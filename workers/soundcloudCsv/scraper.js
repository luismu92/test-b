/**
 * Important note here: When using `page.evaluate()` to execute JS in
 * the context of the headless browser, you either have to wrap your function
 * in a template string, or use basic/more supported JS methods since this
 * file gets transpiled using Babel and the outputted code will be unrecognizable
 * in the browser context. I've replaced things like Array.from with the older
 * and probably more well known `[].slice.call` to get around certain limitations.
 * To use more advanced JS features, I try to get the basic amount of data
 * from `page.evalulate()` and format the data after it's returned from the
 * context of the browser
 */
import '../../am-shared/utils/setEnv';
import puppeteer from 'puppeteer';
import {
    sendEmail,
    emailList,
    template,
    TYPE_SPARKPOST
} from '../email/helpers';
import SoundcloudCsvEmail from '../../am-desktop/shared/email/SoundcloudCsvEmail';
import { renderToStaticMarkup } from 'react-dom/server';
import React from 'react';

const emailKey = 'Email';
// The only social networks we care about. The keys will
// map to what is output in the csv
const socialProfileMap = {
    [emailKey]: /[^@]+@[^\.]+\..+/i,
    Facebook: /^https?:\/\/(?:www\.)?facebook.com/i,
    Twitter: /^https?:\/\/(?:www\.)?twitter.com/i,
    Instagram: /^https?:\/\/(?:www\.)?instagram.com/i,
    YouTube: /^https?:\/\/(?:www\.)?youtube.com/i,
    Spotify: /^https?:\/\/(?:www|open\.)?spotify.com/i,
    Audiomack: /^https?:\/\/(?:www\.)?audiomack.com/i,
    Snapchat: /^https?:\/\/(?:www\.)?snapchat.com/i
};

export default async function main(data) {
    const puppeteerConfig = {
        headless: true,
        ...(data.puppeteerConfig || {})
    };
    const browser = await puppeteer.launch(puppeteerConfig);
    const userNickname = data.link.split('/')[3];
    const likedUserNames = await getUsernamesFromUserLikesPage(
        data.link,
        browser
    );
    const result = [];

    for (const name of likedUserNames) {
        const user = await getUserDataAndTrackInfo(name, browser);

        result.push(user);
    }

    await browser.close();

    const csv = objectsToCsv(result);

    if (data.debug) {
        return csv;
    }

    await sendCsvEmail({
        filename: 'blah.csv',
        email: data.email,
        name: userNickname,
        csv: csv
    });

    return null;
}

// Needs to be in a string because babel is transpiling code
// in our environment
// https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#code-transpilation-issues
function scrollDownPage(page) {
    page.evaluate(`async () => {
        await new Promise((resolve, reject) => {
            try {
                let countScroll = 0;
                const maxScroll = Number.MAX_SAFE_INTEGER;
                let lastScroll = 0;
                const interval = window.setInterval(() => {
                    window.scrollBy(0, 500);
                    const scrollTop = document.documentElement.scrollTop;
                    if (
                        countScroll > 300 ||
                        scrollTop === maxScroll ||
                        scrollTop === lastScroll
                    ) {
                        window.clearInterval(interval);
                        resolve();
                    } else {
                        lastScroll = scrollTop;
                        countScroll = countScroll + 1;
                    }
                }, 500);
            } catch (err) {
                reject(err.toString());
            }
        });
    }`);
}

async function getNewPage(browser) {
    const page = await browser.newPage();

    await page.setViewport({ width: 1920, height: 926 });

    return page;
}

async function getUsernamesFromUserLikesPage(userName, browser) {
    const soundcloudLikes = `${userName}/likes`;
    const page = await getNewPage(browser);

    await page.goto(soundcloudLikes, {
        waitUntil: ['domcontentloaded', 'networkidle2']
    });
    await scrollDownPage(page);

    const likedNames = await page.evaluate(() => {
        const likesElms = document.querySelectorAll('li.soundList__item');
        const likes = [];
        likesElms.forEach((likeElement) => {
            try {
                const nameHref = likeElement
                    .querySelector('a.soundTitle__username')
                    .getAttribute('href');
                if (nameHref) {
                    likes.push(nameHref.substr(1));
                }
            } catch (exception) {
                console.log(exception);
            }
        });

        return likes;
    });

    const uniqueValues = Array.from(new Set(likedNames));
    return uniqueValues;
}

function getNumber(potentialNumber) {
    return parseInt(potentialNumber, 10) || 0;
}

async function getUserDataAndTrackInfo(userName, browser) {
    const soundcloudBase = 'https://soundcloud.com';
    const userUrl = `${soundcloudBase}/${userName}/tracks`;
    const page = await getNewPage(browser);

    await page.goto(userUrl, {
        waitUntil: ['domcontentloaded', 'networkidle2']
    });
    await scrollDownPage(page);

    const [userTracksStats, socialLinks, following] = await Promise.all([
        getUserTracksStats(page),
        getUserSocialProfiles(page),
        page.evaluate(() => {
            const infoElms = document.querySelectorAll('a.infoStats__statLink');
            return infoElms[0].getAttribute('title').replace(/[^-0-9]/gim, '');
        })
    ]);

    let allCounter = 0;
    let playCount = 0;
    let engagement = 0;

    userTracksStats.forEach((track) => {
        playCount = getNumber(playCount) + getNumber(track.plays);
        allCounter =
            getNumber(allCounter) +
            getNumber(track.like) +
            getNumber(track.repost) +
            getNumber(track.plays);
    });

    if (allCounter > 0) {
        engagement = (getNumber(allCounter) / getNumber(following)).toFixed(2);
    }

    const uploadTime = userTracksStats[0].time;

    return {
        Username: userName,
        Followers: following,
        Engagement: engagement,
        Plays: playCount,
        'Last Upload Date': uploadTime,
        ...socialLinks
    };
}

function extractLinkFromSoundcloudShortener(href = '') {
    if (href.includes('&token')) {
        return decodeURIComponent(href.split('url=')[1].split('&token')[0]);
    }

    if (href.includes('@')) {
        return href.split('mailto:')[1];
    }

    return href;
}

async function getUserSocialProfiles(page) {
    // Makes sure everyone has at least an empty state within the csv
    const defaultLinks = Object.keys(socialProfileMap).reduce(
        (acc, socialPlatform) => {
            acc[socialPlatform] = '';

            if (socialPlatform === emailKey) {
                acc[socialPlatform] = [];
            }
            return acc;
        },
        {}
    );
    const profiles = await page.evaluate(() => {
        const mailLinks = [].slice
            .call(
                document.querySelectorAll(
                    '.infoStats__description a[href^=mailto]'
                )
            )
            .map((a) => a.href);
        const userElms = document.querySelectorAll('li.web-profiles__item');
        return [].slice
            .call(userElms)
            .reduce((acc, userElement) => {
                let profileLink = '';

                try {
                    profileLink = userElement
                        .querySelector('a.web-profile')
                        .getAttribute('href');
                } catch (exception) {
                    console.log(exception);
                }

                if (profileLink) {
                    acc.push(profileLink);
                }

                return acc;
            }, [])
            .concat(mailLinks);
    });

    const socialLinks = profiles.reduce((allLinks, profileLink) => {
        const url = extractLinkFromSoundcloudShortener(profileLink);

        Object.keys(socialProfileMap).some((socialPlatform) => {
            const regex = socialProfileMap[socialPlatform];

            if (url.match(regex)) {
                if (socialPlatform === emailKey) {
                    allLinks[socialPlatform].push(url);
                } else {
                    allLinks[socialPlatform] = url;
                }
                return true;
            }

            return false;
        });

        return allLinks;
    }, defaultLinks);

    socialLinks[emailKey] = socialLinks[emailKey].join('\n');
    return socialLinks;
}

async function getUserTracksStats(page) {
    const tracksData = await page.evaluate(() => {
        const trackElements = document.querySelectorAll('li.soundList__item');

        return [].slice.call(trackElements).map((trackElement) => {
            const time =
                trackElement
                    .querySelector('time.relativeTime')
                    .getAttribute('datetime') || 0;
            const reposts = trackElement
                .querySelector('button.sc-button-repost')
                .innerText.replace(/[^-0-9]/gim, '');
            const plays = trackElement
                .querySelector('li.sc-ministats-item')
                .getAttribute('title')
                .replace(/[^-0-9]/gim, '');
            const likes = trackElement.querySelector('button.sc-button-like')
                .innerText;

            return { time, reposts, plays, likes };
        });
    });

    if (!tracksData.length) {
        return [{ time: 0, repost: '-', plays: '-', like: '-' }];
    }

    const tracks = tracksData.reduce((acc, trackData) => {
        const { time, reposts, plays, likes } = trackData;
        const dateEnd = new Date().setFullYear(new Date().getFullYear() - 1);
        const dateTime = Date.parse(time);

        if (dateEnd > dateTime) {
            return acc;
        }

        const trackJson = {
            time: time.split('T')[0],
            reposts,
            plays,
            likes: '-'
        };

        if (likes) {
            let likeCount = likes;

            if (likeCount.includes('K')) {
                likeCount = parseFloat(likeCount.slice(0, -1));
                likeCount = likeCount * 1000;
            } else if (likeCount.includes('M')) {
                likeCount = parseFloat(likeCount.slice(0, -1));
                likeCount = likeCount * 1000000;
            }

            trackJson.likes = likeCount;
        }

        acc.push(trackJson);

        return acc;
    }, []);

    return tracks;
}

function getKeys(obj, prefix = '') {
    if (typeof obj === 'undefined' || obj === null) {
        return [];
    }
    return [
        ...Object.keys(obj).map((key) => `${prefix}${key}`),
        ...Object.entries(obj).reduce((acc, [key, value]) => {
            if (typeof value === 'object') {
                return [...acc, ...getKeys(value, `${prefix}${key}.`)];
            }
            return acc;
        }, [])
    ];
}

function flatObject(obj, prefix = '') {
    if (typeof obj === 'undefined' || obj === null) {
        return {};
    }
    return Object.entries(obj).reduce((acc, [key, value]) => {
        if (typeof value === 'object') {
            return { ...acc, ...flatObject(value, `${prefix}${key}.`) };
        }
        return { ...acc, [`${prefix}${key}`]: value };
    }, {});
}

function escapeCsvValue(cell) {
    if (cell.replace(/ /g, '').match(/[\s,"]/)) {
        return `"${cell.replace(/"/g, '""')}"`;
    }
    return cell;
}

function objectsToCsv(arrayOfObjects) {
    // collect all available keys
    const keys = new Set(
        arrayOfObjects.reduce((acc, item) => [...acc, ...getKeys(item)], [])
    );
    // for each object create all keys
    const values = arrayOfObjects.map((item) => {
        const fo = flatObject(item);
        const val = Array.from(keys).map((key) =>
            key in fo ? escapeCsvValue(`${fo[key]}`) : ''
        );
        return val.join(',');
    });
    return `${Array.from(keys).join(',')}\n${values.join('\n')}`;
}

// eslint-disable-next-line no-unused-vars
// uploading a file to AWS and sending an email with a link to the file
async function sendCsvEmail(data, debug = false) {
    const { filename, csv } = data;
    const { email } = data;

    if (debug) {
        return renderToStaticMarkup(<SoundcloudCsvEmail {...data} />);
    }

    const html = template(
        renderToStaticMarkup(<SoundcloudCsvEmail {...data} />)
    );

    const csvAttachment = {
        name: filename,
        type: 'text/csv',
        data: Buffer.from(csv).toString('base64')
    };
    const attachments = [csvAttachment];

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: SoundcloudCsvEmail.subject(data),
        campaign: SoundcloudCsvEmail.key(data),
        from: emailList.noReply,
        html: html,
        attachments: attachments
    });
}
