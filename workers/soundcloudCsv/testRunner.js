/**
 * To test this worker, run:
 *
 * NODE_PATH=$NODE_PATH:./am-shared node workers/soundcloudCsv/testRunnerIndex.js
 */

import 'utils/setEnv';

import fs from 'fs';
import path from 'path';
import { promisify } from 'util';

import scraper from './scraper';

const writeFilePromise = promisify(fs.writeFile);

(async function() {
    try {
        console.log('Starting scrape…');
        const csv = await scraper({
            // Add this env variable in your personal env file
            email: process.env.TESTING_EMAIL_TO,
            link: 'https://soundcloud.com/iamcrveu',
            debug: true,
            puppeteerConfig: {
                // headless: false
                // slowMo: 100
            }
        });
        const outputPath = path.join(__dirname, `${Date.now()}.csv`);

        await writeFilePromise(outputPath, csv, 'utf-8');

        console.log(`Wrote file: ${outputPath}`);
        process.exit(0);
    } catch (err) {
        console.log(err);
        process.exit(1);
    }
})();
