/* global global */

// import fs from "fs";
import '../../am-shared/utils/setEnv';
import amqp from 'amqplib';

import scraper from './scraper';

function getConnection() {
    return amqp.connect({
        hostname: process.env.RABBITMQ_HOST,
        port: process.env.RABBITMQ_PORT,
        username: process.env.RABBITMQ_USER,
        password: process.env.RABBITMQ_PASS
    });
}

async function main() {
    try {
        const queueName = 'soundcloud_csv_queue';
        const connection = await getConnection();
        connection.on('error', (err) => {
            global.console.log('Error from amqp: ', err);
        });
        process.once('SIGTERM', function(sig) {
            connection.close();

            global.console.error('AMQP shutdown via SIGTERM:');
            global.console.trace();
            global.console.error(sig);
            process.exit(0);
        });

        process.once('uncaughtException', function(err) {
            connection.close();

            global.console.error('AMQP shutdown via uncaughtException:');
            global.console.trace();
            global.console.error(err);
            process.exit(1);
        });

        const channel = await connection.createChannel();
        await channel.assertQueue(queueName);
        channel.prefetch(100);
        channel.consume(queueName, (msg) => {
            if (msg === null) {
                return;
            }

            const json = JSON.parse(msg.content.toString('utf-8'));

            if (process.env.NODE_ENV === 'development') {
                global.console.log('Message received', json);
            }

            scraper(json)
                .then(() => {
                    return channel.ack(msg);
                })
                .catch((err) => {
                    global.console.log('SoundCloud scrape error:', err);
                    return channel.nack(msg);
                });
        });

        global.console.log(
            `> 🗺️👷  Running SoundCloud CSV worker on ${
                process.env.RABBITMQ_HOST
            }:${process.env.RABBITMQ_PORT}…`
        );
    } catch (err) {
        global.console.log(err);
        process.exit(1);
    }
}

main();
