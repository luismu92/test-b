/* global global */
/**
 * To test this locally add something like SITEMAP_OUTPUT_PATH=/Users/sean/Downloads/sitemap to your personal env file and run:
 * DEBUG=sitemap* NODE_ENV=development NODE_PATH=$NODE_PATH:./am-shared node workers/sitemap/index.js --run
 */
require('../../am-shared/utils/setEnv');

// https://www.quora.com/If-I-have-a-website-with-millions-of-unique-pages-should-I-submit-a-partial-sitemap-to-Google

import fs from 'fs';
import util from 'util';
import path from 'path';
import Redis from 'ioredis';
import debug from 'debug';
import amqp from 'amqplib';
import rimraf from 'rimraf';

import Sitemap from './models/Sitemap';

const exec = util.promisify(require('child_process').exec);
const log = debug('sitemap:generate');
const runOnlyMode = process.argv[2] === '--run';

process.on('warning', function(warning) {
    global.console.log(warning);
});

process.on('exit', function(code) {
    global.console.log('Exit with code', code);
});

function getRedisClient() {
    log('Getting redis client');

    return new Promise((resolve, reject) => {
        let redisClient;

        if (process.env.REDIS_CLUSTER_ENABLED === 'true') {
            const hosts = process.env.REDIS_CLUSTER_HOSTS.split(',').map(
                (uri) => {
                    const [host, port] = uri.split(':');

                    return {
                        port: parseInt(port, 10),
                        host
                    };
                }
            );

            redisClient = new Redis.Cluster(hosts);
        } else {
            redisClient = new Redis(process.env.REDIS_MAIN_URL);
        }

        redisClient.on('connect', () => {
            log('Redis client ready');
            resolve(redisClient);
        });

        redisClient.on('error', (err) => {
            log('Redis client error');

            if (redisClient) {
                redisClient.quit();
            }

            redisClient = null;
            reject(err);
        });
    });
}

function getConnection() {
    return amqp.connect({
        hostname: process.env.RABBITMQ_HOST,
        port: process.env.RABBITMQ_PORT,
        username: process.env.RABBITMQ_USER,
        password: process.env.RABBITMQ_PASS
    });
}

function runLog() {
    // if (!runOnlyMode) {
    //     return;
    // }

    global.console.log(...arguments);
}

async function generate() {
    const outputPath = process.env.SITEMAP_OUTPUT_PATH;

    if (!outputPath) {
        throw new Error(
            'No SITEMAP_OUTPUT_PATH environment variable found. Aborting.'
        );
    }

    // eslint-disable-next-line no-sync
    if (!fs.existsSync(outputPath)) {
        // eslint-disable-next-line no-sync
        fs.mkdirSync(outputPath);
    }

    // Remove the old sitemaps
    rimraf.sync(path.join(outputPath, '*')); // eslint-disable-line no-sync

    runLog(new Date().toString(), 'Sitemap generation started');

    const redisClient = await getRedisClient();

    log('Creating sitemap partials');

    const partials = [
        new Sitemap(Sitemap.TYPE_USER, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_MUSIC, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_PLAYLIST, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_PLAYLIST_CATEGORY, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_MISC, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_BLOG_POST, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_BLOG_TAG, outputPath, redisClient),
        new Sitemap(Sitemap.TYPE_BLOG_PAGE, outputPath, redisClient)
    ];

    await Promise.all(
        partials.map((partial) => {
            runLog(`Generating sitemap for partial type: ${partial.type}`);
            return partial.generate();
        })
    );

    log('Creating sitemap index');

    await Sitemap.createIndex(partials, outputPath);

    runLog(new Date().toString(), 'Sitemap generation complete');

    runLog('');
    runLog(new Date().toString(), 'Uploading sitemap files');
    const upload = await exec(
        `/usr/bin/aws s3 sync --content-type "application/octet-stream" --content-disposition "attachment" --delete ${outputPath} s3://audiomack.sitemaps/sitemaps`
    );

    if (upload.stderr) {
        runLog(upload.stderr);
    } else {
        runLog(new Date().toString(), 'Uploading sitemap files complete');
    }
    runLog('');
    runLog(new Date().toString(), 'Pinging Google and Bing');

    const sitemapUrl = 'https://audiomack.com/sitemaps/sitemap-index.xml.gz';
    const [googleCurl, bingCurl] = await Promise.all([
        exec(
            `/usr/bin/curl http://www.google.com/webmasters/sitemaps/ping?sitemap=${sitemapUrl}`
        ),
        exec(`/usr/bin/curl http://www.bing.com/ping?sitemap=${sitemapUrl}`)
    ]);

    runLog(googleCurl.stdout);
    runLog(bingCurl.stdout);
    runLog('');
    runLog(new Date().toString(), 'Sitemap script complete!');
}

async function main() {
    try {
        if (runOnlyMode) {
            await generate();
            process.exit(0);
        }

        const queueName = 'sitemap_queue';
        const connection = await getConnection();

        connection.on('error', (err) => {
            global.console.log('Error from amqp: ', err);
        });

        process.once('SIGTERM', function(sig) {
            connection.close();

            global.console.error('AMQP shutdown via SIGTERM:');
            global.console.trace();
            global.console.error(sig);
            process.exit(0);
        });

        process.once('uncaughtException', function(err) {
            connection.close();

            global.console.error('AMQP shutdown via uncaughtException:');
            global.console.trace();
            global.console.error(err);
            process.exit(1);
        });

        const channel = await connection.createChannel();
        await channel.assertQueue(queueName);

        channel.consume(queueName, (msg) => {
            runLog('Received message', msg);

            // const json = JSON.parse(message.data.toString('utf-8'));

            generate()
                .then(() => {
                    runLog('Acking msg');
                    return channel.ack(msg);
                })
                .catch((err) => {
                    global.console.log('Sitemap generation error:', err);

                    return channel.nack(msg);
                });
        });

        global.console.log(
            `> 🗺️👷  Running sitemap worker on ${process.env.RABBITMQ_HOST}:${
                process.env.RABBITMQ_PORT
            }…`
        );
    } catch (err) {
        global.console.log('');
        global.console.log(new Date().toString(), 'Sitemap generation error');
        global.console.log(err);
        process.exit(1);
    }
}

main();
