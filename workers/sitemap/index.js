require('../../am-shared/utils/setEnv');
require('@babel/register')({
    plugins: [
        ['universal-import', { babelServer: true }]
    ]
});
require('./worker');
