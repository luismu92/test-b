import fs from 'fs';

// Edited from
// https://gist.github.com/tristanlins/6585391
export function getW3CString(date = new Date()) {
    const year = date.getFullYear();
    let month = date.getMonth();

    month++;
    if (month < 10) {
        month = `0${month}`;
    }
    let day = date.getDate();

    if (day < 10) {
        day = `0${day}`;
    }
    let hours = date.getHours();

    if (hours < 10) {
        hours = `0${hours}`;
    }
    let minutes = date.getMinutes();

    if (minutes < 10) {
        minutes = `0${minutes}`;
    }
    let seconds = date.getSeconds();

    if (seconds < 10) {
        seconds = `0${seconds}`;
    }
    const offset = -date.getTimezoneOffset();
    let offsetHours = Math.abs(Math.floor(offset / 60));
    let offsetMinutes = Math.abs(offset) - offsetHours * 60;

    if (offsetHours < 10) {
        offsetHours = `0${offsetHours}`;
    }
    if (offsetMinutes < 10) {
        offsetMinutes = `0${offsetMinutes}`;
    }
    let offsetSign = '+';

    if (offset < 0) {
        offsetSign = '-';
    }
    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}${offsetSign}${offsetHours}:${offsetMinutes}`;
}

export class AssertionError {
    constructor(message) {
        this.message = message;
        Object.freeze(this);
    }
}

export function assert(condition, message, trace = false) {
    if (!condition) {
        message = message || 'Assertion failed';
        if (trace) {
            console.trace();
        }
        throw new AssertionError(message);
    }
}

export function writeFile(outputPath, contents) {
    return new Promise((resolve, reject) => {
        fs.writeFile(outputPath, contents, 'utf8', (err) => {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
}

export async function batchWithTotalNumber(
    totalAmount,
    promiseFn,
    batchCount = 5,
    cursor = 1
) {
    const includeLastNumber = 1;
    const amountToProcess = Math.min(
        totalAmount - cursor + includeLastNumber,
        batchCount
    );

    if (amountToProcess <= 0) {
        return null;
    }

    let promise = Promise.resolve();

    for (let i = 0; i < amountToProcess; i++) {
        promise = promise.then(() => {
            return promiseFn(cursor + i);
        });
    }

    await promise;

    return batchWithTotalNumber(
        totalAmount,
        promiseFn,
        batchCount,
        cursor + batchCount
    );
}

export function isVerified(value) {
    return ['yes', 'authenticated', 'tastemaker'].includes(value);
}
