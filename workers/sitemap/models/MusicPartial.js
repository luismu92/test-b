import debug from 'debug';
import Sitemap from './Sitemap';
import SitemapPartial from './SitemapPartial';
import { assert, isVerified } from '../utils';

const log = debug('sitemap:MusicPartial');

export default class MusicPartial extends SitemapPartial {
    static writeData(redis, writeLine) {
        function getArtistSlug(artistId) {
            return new Promise((resolve, reject) => {
                redis.hmget(
                    `artist:${artistId}`,
                    'url_slug',
                    'verified',
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve({
                            slug: reply[0],
                            verified: isVerified(reply[1])
                        });
                    }
                );
            });
        }

        function getMusicItem(musicId) {
            return new Promise((resolve, reject) => {
                redis.hmget(
                    `music:${musicId}`,
                    'url_slug',
                    'updated',
                    'type',
                    'uploader',
                    'private',
                    'released',
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        // Ignore private music
                        if (reply[4] === 'yes') {
                            resolve(null);
                            return;
                        }

                        // Ignore future released music
                        const released = parseInt(reply[5], 10) * 1000;

                        if (released > Date.now()) {
                            resolve(null);
                            return;
                        }

                        getArtistSlug(reply[3])
                            .then(({ slug, verified }) => {
                                if (!verified) {
                                    resolve(null);
                                    return;
                                }

                                resolve({
                                    musicSlug: reply[0],
                                    updated: parseInt(reply[1], 10),
                                    type: reply[2],
                                    artistSlug: slug
                                });
                                return;
                            })
                            .catch(reject);
                    }
                );
            });
        }

        return new Promise((resolve, reject) => {
            function scan(cursor = '0') {
                redis.zscan(
                    'music_releases',
                    cursor,
                    'COUNT',
                    Sitemap.MAX_SCAN_COUNT,
                    async (err, reply) => {
                        // eslint-disable-line space-before-function-paren
                        if (err) {
                            reject(err);
                            return;
                        }

                        const nextCursor = reply[0];
                        const matchedKeys = reply[1];
                        const promises = [];

                        for (
                            let i = 0, len = matchedKeys.length;
                            i < len;
                            i += 2
                        ) {
                            const musicId = parseInt(matchedKeys[i], 10);
                            // const released = parseInt(matchedKeys[i + 1], 10);

                            promises.push(getMusicItem(musicId));
                        }

                        const musicItems = await Promise.all(promises);
                        const writes = musicItems.reduce((promise, item) => {
                            if (!item) {
                                return promise;
                            }

                            const { updated, ...rest } = item;

                            try {
                                const partialInstance = new MusicPartial(
                                    updated,
                                    rest
                                );

                                log(
                                    `Creating new instance\n ${JSON.stringify(
                                        item,
                                        null,
                                        4
                                    )}`
                                );

                                return promise.then(() => {
                                    return writeLine(partialInstance);
                                });
                            } catch (e) {
                                log(
                                    `Error creating new instance from item\n ${JSON.stringify(
                                        item,
                                        null,
                                        4
                                    )}`
                                );
                                return promise;
                            }
                        }, Promise.resolve());

                        await writes;

                        if (nextCursor === '0') {
                            resolve();
                            return;
                        }

                        scan(nextCursor);
                    }
                );
            }

            log('Starting scan');
            scan();
        });
    }

    constructor(lastModified, musicItem) {
        super(lastModified);

        this.musicItem = musicItem;

        assert(
            typeof musicItem.musicSlug === 'string',
            'Must pass an object with a musicSlug for `MusicPartial`'
        );
        assert(
            typeof musicItem.artistSlug === 'string',
            'Must pass an object with a artistSlug for `MusicPartial`'
        );
        assert(
            typeof musicItem.type === 'string',
            'Must pass an object with a type for `MusicPartial`'
        );
    }

    getUrl() {
        const { type, artistSlug, musicSlug } = this.musicItem;

        return `${process.env.AM_URL}/${type}/${artistSlug}/${musicSlug}`;
    }

    getAppUrls() {
        const { type, artistSlug, musicSlug } = this.musicItem;

        return [
            `ios-app://${
                process.env.IOS_APP_ID
            }/audiomack/${type}/${artistSlug}/${musicSlug}`,
            `android-app://${
                process.env.ANDROID_APP_PACKAGE
            }/audiomack/${type}/${artistSlug}/${musicSlug}`
        ];
    }
}
