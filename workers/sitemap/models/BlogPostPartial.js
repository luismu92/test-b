import debug from 'debug';

import { getBlogPostUrl } from 'utils/blog';
import api from 'api/index';

import SitemapPartial from './SitemapPartial';
import { assert } from '../utils';

const log = debug('sitemap:BlogPostPartial');

export default class BlogPostPartial extends SitemapPartial {
    static writeData(redis, writeLine) {
        const limit = 20;

        return new Promise((resolve, reject) => {
            async function scan(page = 1) {
                try {
                    const { posts, meta } = await api.world.posts({
                        page,
                        limit
                    });
                    const { pagination } = meta;

                    if (page > pagination.pages) {
                        resolve();
                        return;
                    }

                    log(
                        `Writing blog posts page ${page} of ${pagination.pages}`
                    );

                    for (const post of posts) {
                        const { updated_at: updated, ...rest } = post;
                        const partialInstance = new BlogPostPartial(
                            new Date(updated).getTime() / 1000,
                            rest
                        );

                        log(
                            `Creating new instance\n ${JSON.stringify(
                                {
                                    ...post,
                                    // Ignore this in the logs so we dont have a bunch of
                                    // bs in the logs
                                    html: 'stripped'
                                },
                                null,
                                4
                            )}`
                        );

                        await writeLine(partialInstance);
                    }

                    await scan(page + 1);
                } catch (err) {
                    reject(err);
                }
            }

            log('Starting blog post scan');
            scan();
        });
    }

    constructor(lastModified, post) {
        super(lastModified);

        this.post = post;

        assert(
            typeof post.slug === 'string',
            'Must pass an object with a slug for `BlogPostPartial`'
        );
        assert(
            typeof post.id === 'string',
            'Must pass an object with an id for `BlogPostPartial`'
        );
    }

    getUrl() {
        return getBlogPostUrl(this.post, {
            host: process.env.AM_URL
        });
    }

    // App urls dont exist yet
    // getAppUrls() {
    //     const { type, artistSlug, musicSlug } = this.musicItem;

    //     return [
    //         `ios-app://${process.env.IOS_APP_ID}/audiomack/${type}/${artistSlug}/${musicSlug}`,
    //         `android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack/${type}/${artistSlug}/${musicSlug}`
    //     ];
    // }
}
