import { assert } from '../utils';

/**
 * Base class for querying data for a sitemap child ie songs or albums
 */
export default class SitemapPartial {
    static async writeData() {
        throw new Error('Must implement static writeData() function');
    }

    /**
     * @param  {Number} lastModified Time in seconds
     */
    constructor(lastModified = 0) {
        this.lastModified = parseInt(lastModified, 10);

        assert(
            !isNaN(this.lastModified),
            '`lastModified` needs to be a number.'
        );
    }

    getUrl() {
        throw new Error('Must implement getUrl() function');
    }

    getLastModified() {
        return this.lastModified * 1000;
    }

    getAppUrls() {
        return [];
    }
}
