import debug from 'debug';
import Sitemap from './Sitemap';
import SitemapPartial from './SitemapPartial';
import { assert, isVerified } from '../utils';

const log = debug('sitemap:PlaylistPartial');

export default class PlaylistPartial extends SitemapPartial {
    static writeData(redis, writeLine) {
        function getArtistSlug(artistId) {
            return new Promise((resolve, reject) => {
                redis.hmget(
                    `artist:${artistId}`,
                    'url_slug',
                    'verified',
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve({
                            slug: reply[0],
                            verified: isVerified(reply[1])
                        });
                    }
                );
            });
        }

        function getPlaylist(playlistId) {
            return new Promise((resolve, reject) => {
                redis.hmget(
                    `playlist:${playlistId}`,
                    'url_slug',
                    'updated',
                    'artist_id',
                    'private',
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        // Ignore private playlists
                        if (reply[3] === 'yes') {
                            resolve(null);
                            return;
                        }

                        getArtistSlug(reply[2])
                            .then(({ slug, verified }) => {
                                if (!verified) {
                                    resolve(null);
                                    return;
                                }

                                resolve({
                                    musicSlug: reply[0],
                                    updated: parseInt(reply[1], 10),
                                    artistSlug: slug
                                });
                                return;
                            })
                            .catch(reject);
                    }
                );
            });
        }

        return new Promise((resolve, reject) => {
            function scan(cursor = '0') {
                redis.zscan(
                    'playlists',
                    cursor,
                    'COUNT',
                    Sitemap.MAX_SCAN_COUNT,
                    async (err, reply) => {
                        // eslint-disable-line space-before-function-paren
                        if (err) {
                            reject(err);
                            return;
                        }

                        const nextCursor = reply[0];
                        const matchedKeys = reply[1];
                        const promises = [];

                        for (
                            let i = 0, len = matchedKeys.length;
                            i < len;
                            i += 2
                        ) {
                            const playlistId = parseInt(matchedKeys[i], 10);
                            // const created = parseInt(matchedKeys[i + 1], 10);

                            promises.push(getPlaylist(playlistId));
                        }

                        const musicItems = await Promise.all(promises);
                        const writes = musicItems.reduce(
                            (promise, playlist) => {
                                if (!playlist) {
                                    return promise;
                                }

                                const { updated, ...rest } = playlist;

                                try {
                                    log(
                                        `Creating new instance\n ${JSON.stringify(
                                            playlist,
                                            null,
                                            4
                                        )}`
                                    );

                                    const partialInstance = new PlaylistPartial(
                                        updated,
                                        rest
                                    );

                                    return promise.then(() => {
                                        return writeLine(partialInstance);
                                    });
                                } catch (e) {
                                    log(
                                        `Error creating new instance from item\n ${JSON.stringify(
                                            playlist,
                                            null,
                                            4
                                        )}`
                                    );
                                    return promise;
                                }
                            },
                            Promise.resolve()
                        );

                        await writes;

                        if (nextCursor === '0') {
                            resolve();
                            return;
                        }

                        scan(nextCursor);
                    }
                );
            }

            log('Starting scan');
            scan();
        });
    }

    constructor(lastModified, playlist) {
        super(lastModified);

        this.playlist = playlist;

        assert(
            typeof playlist.musicSlug === 'string',
            'Must pass an object with a musicSlug for `MusicPartial`'
        );
        assert(
            typeof playlist.artistSlug === 'string',
            'Must pass an object with a artistSlug for `MusicPartial`'
        );
    }

    getUrl() {
        const { artistSlug, musicSlug } = this.playlist;

        return `${process.env.AM_URL}/playlist/${artistSlug}/${musicSlug}`;
    }

    getAppUrls() {
        const { artistSlug, musicSlug } = this.playlist;

        return [
            `ios-app://${
                process.env.IOS_APP_ID
            }/audiomack/playlist/${artistSlug}/${musicSlug}`,
            `android-app://${
                process.env.ANDROID_APP_PACKAGE
            }/audiomack/playlist/${artistSlug}/${musicSlug}`
        ];
    }
}
