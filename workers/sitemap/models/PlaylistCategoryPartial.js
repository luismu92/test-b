import debug from 'debug';

import api from 'api/index';

import SitemapPartial from './SitemapPartial';
import { assert } from '../utils';

const log = debug('sitemap:PlaylistCategoryPartial');

export default class PlaylistCategoryPartial extends SitemapPartial {
    static writeData(redis, writeLine) {
        return new Promise((resolve, reject) => {
            async function run() {
                try {
                    const tag = '';
                    const limit = 999;
                    const featured = true;
                    const response = await api.playlist.playlistTags(
                        tag,
                        limit,
                        featured
                    );
                    const categories = response.results.categories;

                    for (const category of categories) {
                        const partialInstance = new PlaylistCategoryPartial(
                            Date.now() / 1000,
                            category.url_slug
                        );

                        log(
                            `Creating new instance\n ${JSON.stringify(
                                category,
                                null,
                                4
                            )}`
                        );

                        await writeLine(partialInstance);
                    }

                    resolve();
                } catch (err) {
                    reject(err);
                }
            }

            log('Starting playlist category scan');
            run();
        });
    }

    constructor(lastModified, category) {
        super(lastModified);

        this.category = category;

        assert(
            typeof category === 'string',
            `Must pass a category string to PlaylistCategoryPartial but got: ${JSON.stringify(
                category,
                null,
                4
            )}`
        );
    }

    getUrl() {
        return `${process.env.AM_URL}/playlists/browse/${this.category}`;
    }

    // App urls dont exist yet
    // getAppUrls() {
    //     const { type, artistSlug, musicSlug } = this.musicItem;

    //     return [
    //         `ios-app://${process.env.IOS_APP_ID}/audiomack/${type}/${artistSlug}/${musicSlug}`,
    //         `android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack/${type}/${artistSlug}/${musicSlug}`
    //     ];
    // }
}
