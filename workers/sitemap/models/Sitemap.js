/* global global */

import fs from 'fs';
import path from 'path';
import zlib from 'zlib';
import debug from 'debug';

import { pad } from 'utils/index';

import { getW3CString, assert, writeFile } from '../utils';
import SitemapPartial from './SitemapPartial';
import UserPartial from './UserPartial';
import MusicPartial from './MusicPartial';
import PlaylistPartial from './PlaylistPartial';
import PlaylistCategoryPartial from './PlaylistCategoryPartial';
import MiscPartial from './MiscPartial';
import BlogPostPartial from './BlogPostPartial';
import BlogTagPartial from './BlogTagPartial';
import BlogPagePartial from './BlogPagePartial';

const log = debug('sitemap:index');

export default class Sitemap {
    static MAX_SCAN_COUNT = 1000;

    // Songs and albums
    static TYPE_MUSIC = 'music';
    static TYPE_USER = 'user';
    static TYPE_PLAYLIST = 'playlist';
    static TYPE_PLAYLIST_CATEGORY = 'playlist_category';
    static TYPE_MISC = 'misc';
    static TYPE_BLOG_POST = 'blog_post';
    static TYPE_BLOG_TAG = 'blog_tag';
    static TYPE_BLOG_PAGE = 'blog_page';

    static createIndex(partialInstances = [], outputPath) {
        const output = path.join(outputPath, 'sitemap-index.xml.gz');
        const partials = partialInstances
            .map((sitemap) => {
                return Array(sitemap.pages)
                    .fill(0)
                    .map((x, i) => {
                        return `<sitemap><loc>${sitemap.getUrl(
                            i + 1
                        )}</loc><lastmod>${sitemap.getLastModified()}</lastmod></sitemap>`;
                    })
                    .join('\n');
            })
            .join('\n');
        const content = `<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${partials}
</sitemapindex>`;

        return new Promise((resolve, reject) => {
            zlib.gzip(content, (err, buf) => {
                if (err) {
                    reject(err);
                    return;
                }

                writeFile(output, buf)
                    .then(resolve)
                    .catch(reject);
            });
        });
    }

    constructor(
        type,
        outputPath,
        redis,
        {
            gzip = true,
            // Google has a limit of 50000 urls or 10MB uncompressed per sitemap
            maxUrls = 50000
        } = {}
    ) {
        this.type = type;
        this.pages = 1;
        this.currentPartialLine = 1;
        this.gzip = gzip;
        this.maxUrls = maxUrls;
        this.lastModified = getW3CString();
        this.outputPath = outputPath;
        this.redis = redis;
        this.writeStream = this.openWriteStream();
    }

    getUrl(page) {
        return `${process.env.AM_URL}/sitemaps/${this.getSitemapName(page)}`;
    }

    getSitemapName(page) {
        const extension = this.gzip ? '.xml.gz' : '.xml';

        return `sitemap.${this.type}.${pad(page, 4)}${extension}`;
    }

    getLastModified() {
        return this.lastModified;
    }

    openWriteStream() {
        const outputPath = path.join(
            this.outputPath,
            this.getSitemapName(this.pages)
        );

        log(`Opening write stream @ ${outputPath} of type "${this.type}"`);

        const writeStream = fs.createWriteStream(outputPath);

        let stream = writeStream;

        if (this.gzip) {
            stream = zlib.createGzip();
            stream.pipe(writeStream);
        }

        stream.setMaxListeners(Sitemap.MAX_SCAN_COUNT);
        stream.once('error', (err) => {
            global.console.error(
                `Stream error for partial type: ${this.type}`,
                err
            );
        });

        const header = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">\n`;

        stream.write(header);

        return stream;
    }

    closeWriteStream() {
        log(`Write stream for type "${this.type}" closing`);

        const footer = '</urlset>';

        return new Promise((resolve, reject) => {
            this.writeStream.once('finish', () => {
                log(`Write stream finished for type "${this.type}"`);

                this.writeStream = null;
                resolve();
            });

            this.writeStream.once('error', (err) => {
                global.console.error(
                    `Write stream close error for type "${this.type}"`
                );
                this.writeStream = null;
                reject(err);
            });

            this.write(footer)
                .then(() => {
                    this.writeStream.end();
                    return;
                })
                .catch(reject);
        });
    }

    write(data) {
        return new Promise((resolve) => {
            if (!this.writeStream.write(data)) {
                this.writeStream.once('drain', () => {
                    resolve();
                });
                return;
            }

            resolve();
        });
    }

    writeLine = async (partialInstance) => {
        // eslint-disable-line space-before-function-paren
        assert(
            partialInstance instanceof SitemapPartial,
            'writeLine only accepts instances of `SitemapPartial`'
        );

        if (this.currentPartialLine > this.maxUrls) {
            this.pages += 1;
            this.currentPartialLine = 1;
            await this.closeWriteStream();
            this.writeStream = this.openWriteStream();
        }

        const lastModified = new Date(partialInstance.getLastModified());
        const date = getW3CString(lastModified);
        const appUrls = partialInstance
            .getAppUrls()
            .map((url) => {
                return `<xhtml:link rel="alternate" href="${url}" />`;
            })
            .join('');
        const line = `<url><loc>${partialInstance.getUrl()}</loc><lastmod>${date}</lastmod>${appUrls}</url>\n`;

        this.currentPartialLine += 1;

        await this.write(line);
    };

    generate() {
        let promise = Promise.resolve();

        switch (this.type) {
            case Sitemap.TYPE_USER: {
                promise = UserPartial.writeData(this.redis, this.writeLine);
                break;
            }

            case Sitemap.TYPE_MUSIC: {
                promise = MusicPartial.writeData(this.redis, this.writeLine);
                break;
            }

            case Sitemap.TYPE_PLAYLIST: {
                promise = PlaylistPartial.writeData(this.redis, this.writeLine);
                break;
            }

            case Sitemap.TYPE_PLAYLIST_CATEGORY: {
                promise = PlaylistCategoryPartial.writeData(
                    this.redis,
                    this.writeLine
                );
                break;
            }

            case Sitemap.TYPE_BLOG_POST: {
                promise = BlogPostPartial.writeData(this.redis, this.writeLine);
                break;
            }

            case Sitemap.TYPE_BLOG_TAG: {
                promise = BlogTagPartial.writeData(this.redis, this.writeLine);
                break;
            }

            case Sitemap.TYPE_BLOG_PAGE: {
                promise = BlogPagePartial.writeData(this.redis, this.writeLine);
                break;
            }

            case Sitemap.TYPE_MISC: {
                promise = MiscPartial.writeData(this.redis, this.writeLine);
                break;
            }

            default: {
                console.warn('No generate handler found for type:', this.type);
                return promise;
            }
        }

        return promise.then(() => {
            // If the partial file doesnt end up writing anything
            // The result file ends up being corrupted. I'm sure there's some
            // event or something that fires that can remove the need for this
            // hack but im not sure what to look for yet. This fixes that in the
            // meantime. Also this is super edge casey since there will probably
            // never be a case like this on production.
            return this.write('').then(() => {
                return this.closeWriteStream();
            });
        });
    }
}
