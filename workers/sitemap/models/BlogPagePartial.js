import debug from 'debug';

import { getBlogPageUrl } from 'utils/blog';
import api from 'api/index';

import SitemapPartial from './SitemapPartial';
import { assert } from '../utils';

const log = debug('sitemap:BlogPagePartial');

export default class BlogPagePartial extends SitemapPartial {
    static writeData(redis, writeLine) {
        return new Promise((resolve, reject) => {
            async function run() {
                try {
                    const { pages } = await api.world.pages({
                        limit: 'all'
                    });

                    for (const page of pages) {
                        const partialInstance = new BlogPagePartial(
                            new Date(page.updated_at).getTime() / 1000,
                            page
                        );

                        log(
                            `Creating new instance\n ${JSON.stringify(
                                page,
                                null,
                                4
                            )}`
                        );

                        await writeLine(partialInstance);
                    }

                    resolve();
                } catch (err) {
                    reject(err);
                }
            }

            log('Starting blog page scan');
            run();
        });
    }

    constructor(lastModified, page) {
        super(lastModified);

        this.page = page;

        assert(
            typeof page.slug === 'string',
            'Must pass an object with a slug for `BlogPagePartial`'
        );
        assert(
            typeof page.updated_at === 'string',
            'Must pass an object with an updated_at for `BlogPagePartial`'
        );
    }

    getUrl() {
        return getBlogPageUrl(this.page, {
            host: process.env.AM_URL
        });
    }

    // App urls dont exist yet
    // getAppUrls() {
    //     const { type, artistSlug, musicSlug } = this.musicItem;

    //     return [
    //         `ios-app://${process.env.IOS_APP_ID}/audiomack/${type}/${artistSlug}/${musicSlug}`,
    //         `android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack/${type}/${artistSlug}/${musicSlug}`
    //     ];
    // }
}
