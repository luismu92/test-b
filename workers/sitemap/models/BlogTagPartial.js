import debug from 'debug';

import { getBlogTagUrl } from 'utils/blog';
import api from 'api/index';

import SitemapPartial from './SitemapPartial';
import { assert } from '../utils';

const log = debug('sitemap:BlogTagPartial');

export default class BlogTagPartial extends SitemapPartial {
    static writeData(redis, writeLine) {
        return new Promise((resolve, reject) => {
            async function run() {
                try {
                    const { tags } = await api.world.tags({
                        limit: 'all',
                        filter: 'visibility:public'
                    });

                    for (const tag of tags) {
                        const partialInstance = new BlogTagPartial(
                            Date.now() / 1000,
                            tag
                        );

                        log(
                            `Creating new instance\n ${JSON.stringify(
                                tag,
                                null,
                                4
                            )}`
                        );

                        await writeLine(partialInstance);
                    }

                    resolve();
                } catch (err) {
                    reject(err);
                }
            }

            log('Starting blog tag scan');
            run();
        });
    }

    constructor(lastModified, tag) {
        super(lastModified);

        this.tag = tag;

        assert(
            typeof tag.slug === 'string',
            'Must pass an object with a slug for `BlogTagPartial`'
        );
        assert(
            typeof tag.id === 'string',
            'Must pass an object with an id for `BlogTagPartial`'
        );
    }

    getUrl() {
        return getBlogTagUrl(this.tag, {
            host: process.env.AM_URL
        });
    }

    // App urls dont exist yet
    // getAppUrls() {
    //     const { type, artistSlug, musicSlug } = this.musicItem;

    //     return [
    //         `ios-app://${process.env.IOS_APP_ID}/audiomack/${type}/${artistSlug}/${musicSlug}`,
    //         `android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack/${type}/${artistSlug}/${musicSlug}`
    //     ];
    // }
}
