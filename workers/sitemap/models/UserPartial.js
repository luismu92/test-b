import debug from 'debug';

import { isSeoWorthyArtistProfile } from 'utils/index';

import Sitemap from './Sitemap';
import SitemapPartial from './SitemapPartial';
import { assert, batchWithTotalNumber, isVerified } from '../utils';

const log = debug('sitemap:UserPartial');

export default class UserPartial extends SitemapPartial {
    static async writeData(redis, writeLine) {
        function getArtistCount() {
            return new Promise((resolve, reject) => {
                redis.get('global:next_artist', (err, reply) => {
                    // eslint-disable-line space-before-function-paren
                    if (err) {
                        reject(err);
                        return;
                    }

                    resolve(parseInt(reply, 10));
                });
            });
        }

        async function getUploadCount(artistId) {
            const musicUploads = new Promise((resolve, reject) => {
                redis.zcount(
                    `music_releases:artist:${artistId}`,
                    0,
                    Date.now(),
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve(parseInt(reply, 10));
                    }
                );
            });
            const repostsCount = new Promise((resolve, reject) => {
                redis.zcard(
                    `music_reposts:artist:${artistId}`,
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve(parseInt(reply, 10));
                    }
                );
            });

            const values = await Promise.all([musicUploads, repostsCount]);

            return values[0] - values[1];
        }

        function getValuesFromArtist(artistId) {
            return new Promise((resolve, reject) => {
                redis.hmget(
                    `artist:${artistId}`,
                    'url_slug',
                    'updated',
                    'created',
                    'verified',
                    (err, reply) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve({
                            slug: reply[0],
                            updated: parseInt(reply[1], 10),
                            created: parseInt(reply[2], 10),
                            verified: isVerified(reply[3])
                        });
                    }
                );
            });
        }

        const totalArtists = await getArtistCount();

        return batchWithTotalNumber(
            totalArtists,
            async (artistId) => {
                // eslint-disable-line space-before-function-paren
                const uploadCount = await getUploadCount(artistId);

                if (isSeoWorthyArtistProfile(uploadCount)) {
                    const item = await getValuesFromArtist(artistId);
                    const { updated, slug, created, verified } = item;

                    try {
                        const creation = created * 1000;
                        const oneDay = 1000 * 60 * 60 * 24;

                        // Only add users in sitemap that are at least a day old
                        if (creation + oneDay > Date.now()) {
                            return null;
                        }

                        // Ignore unverified users
                        if (!verified) {
                            return null;
                        }

                        const partialInstance = new UserPartial(updated, slug);

                        log(
                            `Creating new instance\n ${JSON.stringify(
                                item,
                                null,
                                4
                            )}`
                        );

                        return writeLine(partialInstance);
                    } catch (e) {
                        log(
                            `Error creating new instance from item\n ${JSON.stringify(
                                item,
                                null,
                                4
                            )}`
                        );
                    }
                }

                return null;
            },
            Sitemap.MAX_SCAN_COUNT
        );
    }

    constructor(lastModified, urlSlug) {
        super(lastModified);

        this.urlSlug = urlSlug;

        assert(
            typeof urlSlug === 'string',
            'Must pass a url slug for `UserPartial`'
        );
    }

    getUrl() {
        return `${process.env.AM_URL}/artist/${this.urlSlug}`;
    }

    getAppUrls() {
        return [
            `ios-app://${process.env.IOS_APP_ID}/audiomack/artist/${
                this.urlSlug
            }`,
            `android-app://${
                process.env.ANDROID_APP_PACKAGE
            }/audiomack/artist/${this.urlSlug}`
        ];
    }
}
