import debug from 'debug';
import SitemapPartial from './SitemapPartial';
import { assert } from '../utils';
import { routeConfig as mobileRoutes } from '../../../am-mobile/shared/routes';
import { routeConfig as desktopRoutes } from '../../../am-desktop/shared/routes';

const log = debug('sitemap:MiscPartial');

export default class MiscPartial extends SitemapPartial {
    static async writeData(redis, writeLine) {
        const writtenPaths = [];
        const writes = mobileRoutes.concat(desktopRoutes).filter((route) => {
            if (route.sitemap === true && route.path.includes('/:')) {
                console.warn('Route', route.path, 'contains a splat in the url. Either make the sitemap property an absolute url or remove the sitemap key');
                return false;
            }

            if (typeof route.sitemap === 'string' && route.sitemap.includes('/:')) {
                console.warn('Route', route.path, 'with sitemap value of', route.sitemap, 'contains a splat in the url. Either make the sitemap property an absolute url or remove the sitemap key');
                return false;
            }

            return route.sitemap;
        }).reduce((promise, route) => {
            const path = typeof route.sitemap === 'string' ? route.sitemap : route.path;

            if (writtenPaths.includes(path)) {
                return promise;
            }

            writtenPaths.push(path);

            const dateInSeconds = Date.now() / 1000;

            try {
                const partial = new MiscPartial(dateInSeconds, path);

                log(`Creating new instance\n ${JSON.stringify({ dateInSeconds, path }, null, 4)}`);

                return promise.then(() => {
                    return writeLine(partial);
                });
            }
            catch (e) {
                log(`Error creating new instance from item\n ${JSON.stringify({ dateInSeconds, path }, null, 4)}`);
                return promise;
            }
        }, Promise.resolve());

        await writes;
    }

    constructor(lastModified, path) {
        super(lastModified);

        this.path = path;

        assert(typeof path === 'string', 'Must a url path for `MiscPartial`');
    }

    getUrl() {
        return `${process.env.AM_URL}${this.path}`;
    }

    // getAppUrls() {
    //     return [
    //         `ios-app://${process.env.IOS_APP_ID}/audiomack/artist/${this.urlSlug}`,
    //         `android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack/artist/${this.urlSlug}`
    //     ];
    // }
}
