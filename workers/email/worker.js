/* global global */

require('../../am-shared/utils/setEnv');

import amqp from 'amqplib';
import { processMessage } from './helpers';
import { EmailShouldNackException } from 'utils/errors';

function getConnection() {
    return amqp.connect({
        hostname: process.env.RABBITMQ_HOST,
        port: process.env.RABBITMQ_PORT,
        username: process.env.RABBITMQ_USER,
        password: process.env.RABBITMQ_PASS
    });
}

function hasSparkpostErrorCode(err, code) {
    return (
        err.name === 'SparkPostError' &&
        err.errors &&
        err.errors[0] &&
        err.errors[0].code === code
    );
}

async function main() {
    try {
        const queueName = 'email_queue';
        const connection = await getConnection();

        connection.on('error', (err) => {
            global.console.log('Error from amqp: ', err);
        });

        process.once('SIGTERM', function(sig) {
            connection.close();

            global.console.error('AMQP shutdown via SIGTERM:');
            global.console.trace();
            global.console.error(sig);
            process.exit(0);
        });

        process.once('uncaughtException', function(err) {
            connection.close();

            global.console.error('AMQP shutdown via uncaughtException:');
            global.console.trace();
            global.console.error(err);
            process.exit(1);
        });

        const channel = await connection.createChannel();
        await channel.assertQueue(queueName);

        channel.prefetch(5);
        channel.consume(queueName, (msg) => {
            if (msg === null) {
                return;
            }

            const json = JSON.parse(msg.content.toString('utf-8'));

            if (process.env.NODE_ENV === 'development') {
                global.console.log('Message received', json);
            }

            const emailKey = `email:${json.type}`;

            processMessage({
                ...json,
                emailKey
            })
                .then(() => {
                    return channel.ack(msg);
                })
                .catch((err) => {
                    global.console.log('Email process error:', err, json);

                    // Force pm2 to restart the worker when this error happens
                    if ((err.message || {}).code === 'ETIMEDOUT') {
                        global.console.log(
                            'Exiting worker because of ETIMEDOUT'
                        );
                        process.exit(1);
                    }

                    // Ignore msg if data.email is falsy
                    if (
                        !json.email ||
                        // Invalid email code
                        hasSparkpostErrorCode(err, 5002) ||
                        err instanceof EmailShouldNackException
                    ) {
                        const allUpTo = false;
                        const requeue = false;

                        return channel.nack(msg, allUpTo, requeue);
                    }

                    return channel.nack(msg);
                });
        });

        global.console.log(
            `> 📧👷  Running email worker on ${process.env.RABBITMQ_HOST}:${
                process.env.RABBITMQ_PORT
            }…`
        );
    } catch (err) {
        global.console.log(err);
        process.exit(1);
    }
}

main();
