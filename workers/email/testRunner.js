import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import useragent from 'express-useragent';
import api, { setConfig } from 'api/index';

import { buildDynamicImage, getFeaturing } from 'utils/index';
import { processMessage } from './helpers';
import errorHandler from '../../server/handlers/error';
import { generateImage as trendingMusic } from '../../server/handlers/trending';
// import trendingMusic from './emails/trendingMusic';
import { getImageData as statBenchmark } from './emails/statBenchmark';

const app = express();

app.disable('x-powered-by');

app.use(bodyParser.json({ type: 'application/csp-report' }));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(useragent.express());

app.use((req, res, next) => {
    if (!process.env.CUSTOM_GRAPHIC_URL) {
        next(
            new Error(`CUSTOM_GRAPHIC_URL environment variable not found.

Please add a production url in your personal .env file to use this test runner.

(ie. CUSTOM_GRAPHIC_URL=https://audiomack.com/album/mo3badazz/osama)
`)
        );
        return;
    }

    // Set to production urls
    setConfig({
        baseUrl: 'https://api.audiomack.com/v1',
        consumerKey: 'audiomack-js',
        consumerSecret: 'f3ac5b086f3eab260520d8e3049561e6'
    });

    next();
});

// Sending test emails with your own data
app.post('/send', async function(req, res) {
    const { data } = req.body;

    try {
        await processMessage(data);
        res.status(200).json({
            message: `Email sent to ${process.env.TESTING_EMAIL_TO ||
                data.email}!`
        });
    } catch (err) {
        res.status(500).json({
            message: err
        });
    }
});

app.get('/send', function(req, res) {
    // const placeholder = {
    //     email: 'your@email.com',
    //     type: 'amp-partner-approval',
    //     name: '박카스{{{{{{  강남출장안마 HIGH CLASS'
    // };

    const placeholder = {
        email: 'your@email.com',
        type: 'dmca-takedown',
        uploader: 'dteiddir',
        artist: '박카스{{{{{{  강남출장안마 HIGH CLASS',
        title: '박카스{{{{{{  강남출장안마 HIGH CLASS',
        link: '/album/dteiddir/bagkaseu-gangnamchuljang-anma-high-class'
    };

    res.set('Content-Type', 'text/html');
    res.send(`<html>
<head>
    <title>Email tester</title>
</head>
<body style="font-family: sans-serif; text-align: center;">
<p>Enter queue json data:</p>
<small style="display: block; color: #aaa; max-width: 400px;margin: 0 auto;">Note: You can avoid adding the email key below by adding "TESTING_EMAIL_TO=your@email.com" to your personal env file to send to yourself.</small>
<form id="form" method="post" action="/send">
<textarea id="textarea" placeholder='${JSON.stringify(
        placeholder,
        null,
        4
    )}' style="width: 500px; height: 200px; margin: 15px 0;" name="data"></textarea>
<p id="error" style="color: #990000"></p>
<p id="success" style="color: green"></p>
<input type="submit" value="Send email" id="submit" />
</form>
<script type="text/javascript">
    const textarea = document.getElementById('textarea');
    const submit = document.getElementById('submit');
    const success = document.getElementById('success');
    const error = document.getElementById('error');
    const form = document.getElementById('form');

    form.addEventListener('submit', function(e) {
        e.preventDefault();

        error.textContent = '';
        success.textContent = '';
        let json;

        try {
            json = JSON.parse(textarea.value);
        }
        catch (err) {
            try {
                // Try falling back to eval if keys aren't
                // surrounded by quotes for valid json
                json = eval('data = ' + textarea.value)
            }
            catch (err2) {
                error.textContent = err.message;
                return;
            }
        }

        submit.setAttribute('disabled', true);

        fetch('/send', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({ data: json })
        }).then((res) => {
            return Promise.all([
                res.status,
                res.json()
            ])
        }).then(([status, json]) => {
            if (status >= 400) {
                if (typeof json.message === 'string') {
                    throw new Error(json.message);
                }

                throw new Error(JSON.stringify(json.message, null, 4));
            }
            textarea.value = '';
            success.textContent = json.message;
            submit.removeAttribute('disabled');
        }).catch((err) => {
            submit.removeAttribute('disabled');
            error.textContent = err.message;
        });
    });
</script>
</body>
</html>`);
});

// Trending email
app.get('/trending', async function(req, res) {
    const imageData = await trendingMusic(process.env.CUSTOM_GRAPHIC_URL);

    res.set('Content-Type', 'image/png');
    res.send(imageData);
});

// Stat Benchmark
app.get('/benchmark', async function(req, res) {
    const url = process.env.CUSTOM_GRAPHIC_URL;
    const number = '1 Million';
    const stat = 'Streams';

    const parts = url.split('/');
    const musicSlug = parts.pop();
    const artistSlug = parts.pop();
    const type = parts.pop();

    console.log(
        'Querying artist',
        `"${artistSlug}"`,
        'with music slug',
        `"${musicSlug}"`,
        `and music type: ${type}`
    );

    const { results: item } = await api.music[type](artistSlug, musicSlug);

    const artist = item.artist;
    const title = item.title;
    const featuring = getFeaturing(item);
    const image = item.image_base || item.image;
    const thumbnail = buildDynamicImage(image, {
        width: 260,
        height: 260,
        blur: 20,
        max: true
    });
    const uploaderSlug = 'test';
    const imageData = await statBenchmark(
        {
            artist,
            title,
            uploaderSlug,
            featuring,
            number,
            stat
        },
        image,
        thumbnail
    );

    res.set('Content-Type', 'image/png');
    res.send(imageData);
});

app.get('*', (req, res) => {
    res.end(`<html>
<head></head>
<body>
    <p><a href="/trending">Trending Image</a></p>
    <p><a href="/benchmark">Stat Benchmark</a></p>
    <p><a href="/send">Send a test email with queue data</a></p>
</body>
</html>`);
});

app.use(errorHandler);

const port = 3003;

app.listen(port, () => {
    console.log(`Running on port ${port}`);
});
