import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailList,
    emailReplyToList
} from '../helpers';
import ForgotPasswordEmail from '../../../am-desktop/shared/email/ForgotPasswordEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<ForgotPasswordEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.accountRecovery,
        replyTo: emailReplyToList.support,
        subject: ForgotPasswordEmail.subject(data),
        campaign: ForgotPasswordEmail.key(data),
        html
    });
}
