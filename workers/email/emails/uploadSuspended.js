import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailList,
    emailReplyToList
} from '../helpers';
import UploadSuspendedEmail from '../../../am-desktop/shared/email/UploadSuspendedEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<UploadSuspendedEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.suspend,
        replyTo: emailReplyToList.dmca,
        subject: UploadSuspendedEmail.subject(data),
        campaign: UploadSuspendedEmail.key(data),
        html
    });
}
