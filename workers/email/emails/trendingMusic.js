import util from 'util';
import path from 'path';
import fs from 'fs';
import React from 'react';
import { createCanvas, registerFont, loadImage } from 'canvas';
import { canvasRGBA as blurImage } from 'stackblur-canvas';
import { renderToStaticMarkup } from 'react-dom/server';

import { buildDynamicImage } from 'utils/index';
import { EmailShouldNackException } from 'utils/errors';

import {
    sendEmail,
    emailList,
    template,
    TYPE_SPARKPOST,
    getAndAssertAllImagesExist
} from '../helpers';
import TrendingMusicEmail from '../../../am-desktop/shared/email/TrendingMusicEmail';

const size = 1024;

const logoWidth = Math.round(size * 0.29);
const logoLeft = Math.round((size - logoWidth) / 2);
const logoTop = Math.round(size * 0.05);
// natural height / width
const logoHeight = Math.round((96 / 700) * logoWidth);

const artworkTop = Math.round(size * 0.21);
const artworkSize = Math.round(size * 0.61);
const artworkLeft = Math.round((size - artworkSize) / 2);

const textLeft = Math.round(size * 0.07);

const trendingText = 'Now Trending';
const trendingTextTop = Math.round(size * 0.172);
const trendingTextSize = Math.round((size * 38) / 510);

const titleTop = Math.round(size * 0.9);
const titleTextSize = Math.round((size * 18) / 510);
const titleWithFeaturingMaxWidth = Math.round(size * 0.9);
const titleMaxWidth = Math.round(size * 0.8);

const artistTop = Math.round(size * 0.94);
const artistTextSize = Math.round((size * 18) / 510);

const trendingIcon = path.join(__dirname, '../assets/trending-icon.png');
const trendingIconLeft = Math.round(size * 0.88);
const trendingIconTop = Math.round(size * 0.885);
const trendingIconWidth = Math.round(size * 0.06);
const trendingIconHeight = Math.round((trendingIconWidth * 25) / 30);

// 447x438
const colorBaseBackground = '#f9f9f9';
const colorBorder = 'rgba(0, 0, 0, 0.1)';
const colorOrange = '#ffa200';
const colorGray = '#777777';
const colorBlack = '#2a2a2a';

const logoWhite = path.join(__dirname, '../assets/am-logo-gradient.png');
const fontMedium = 'Roboto Medium';
const fontBlack = 'Roboto Black';
[
    {
        family: fontMedium,
        path: path.join(__dirname, '../assets/Roboto-Medium.ttf')
    },
    {
        family: fontBlack,
        path: path.join(__dirname, '../assets/Roboto-Black.ttf')
    }
].forEach(({ family, path: fontPath }) => {
    registerFont(fontPath, {
        family: family
    });
});

const exec = util.promisify(require('child_process').exec);
const writeFile = util.promisify(fs.writeFile);

function drawText(ctx, text, x, y, options = {}) {
    const letters = text.split('');
    const letterSpacing = -0.1;
    let lastX = x;

    if (options.shadowBlur && options.shadowColor && options.textColor) {
        ctx.fillStyle = 'transparent';
        ctx.shadowBlur = options.shadowBlur;
        ctx.shadowColor = options.shadowColor;

        letters.forEach((char) => {
            const { width: charWidth } = ctx.measureText(char);
            const offset = charWidth * letterSpacing;

            ctx.fillText(char, lastX, y);

            lastX += charWidth + offset;
        });

        lastX = x;
        ctx.fillStyle = options.textColor;
    }

    return letters.reduce((totalWidth, char) => {
        const { width: charWidth } = ctx.measureText(char);
        const offset = charWidth * letterSpacing;

        ctx.fillText(char, lastX, y);

        lastX += charWidth + offset;

        return totalWidth + charWidth + offset;
    }, 0);
}

function drawTrending(ctx, x = 0) {
    ctx.save();
    ctx.font = `${trendingTextSize}px "${fontBlack}"`;
    ctx.fillStyle = '#ffffff';

    const trendingWidth = drawText(
        ctx,
        trendingText.toUpperCase(),
        x,
        trendingTextTop
    );
    ctx.restore();

    return trendingWidth;
}

function drawTitle(ctx, title, x = 0, y = 0) {
    const uppercaseTitle = title.toUpperCase();

    ctx.save();
    ctx.font = `${titleTextSize}px "${fontBlack}"`;
    ctx.fillStyle = colorBlack;

    const width = drawText(ctx, uppercaseTitle, x, y);

    ctx.restore();

    return width;
}

export function getImageData(metadata, image, thumb) {
    return Promise.all([
        loadImage(image),
        loadImage(thumb),
        loadImage(logoWhite),
        loadImage(trendingIcon)
    ]).then((values) => {
        const { artist, title, featuring } = metadata;

        return new Promise((resolve) => {
            const [img, thumbnail, whiteLogo, trendingImage] = values;
            const canvas = createCanvas(size, size);
            const ctx = canvas.getContext('2d');

            // Draw text to get widths
            const trendingWidth = drawTrending(ctx);
            let uppercaseTitle = title.toUpperCase();
            const titleWidth = drawTitle(
                ctx,
                uppercaseTitle,
                textLeft,
                titleTop
            );

            ctx.drawImage(thumbnail, 0, 0, size, size);

            // Start with blurred thumbnail as background
            blurImage(canvas, 0, 0, size, size, 80);

            // Draw 40% black overlay
            ctx.save();
            ctx.globalAlpha = 0.4;
            ctx.fillStyle = '#000000';
            ctx.fillRect(0, 0, size, size);
            ctx.globalAlpha = 1;
            ctx.restore();

            // Draw solid light gray angled background
            ctx.save();
            ctx.fillStyle = colorBaseBackground;
            ctx.beginPath();
            ctx.moveTo(0, Math.round(size * 0.48));
            ctx.lineTo(size, Math.round(size * 0.62));
            ctx.lineTo(size, size);
            ctx.lineTo(0, size);
            ctx.fill();
            ctx.restore();

            // Draw logo
            ctx.drawImage(whiteLogo, logoLeft, logoTop, logoWidth, logoHeight);

            // Draw artwork
            ctx.save();
            ctx.shadowBlur = 20;
            ctx.shadowColor = 'rgba(0, 0, 0, 0.6)';
            ctx.drawImage(
                img,
                artworkLeft,
                artworkTop,
                artworkSize,
                artworkSize
            );
            ctx.restore();

            // Draw now trending
            const trendingLeft = (size - trendingWidth) / 2;

            drawTrending(ctx, trendingLeft);

            // Draw title and maybe featuring
            const titleCharWidth = titleWidth / uppercaseTitle.length;
            let titleWasTrimmed = false;

            console.log(titleWidth, titleMaxWidth, titleCharWidth);

            if (titleWidth > titleMaxWidth) {
                const totalChars = Math.floor(titleMaxWidth / titleCharWidth);

                titleWasTrimmed = true;

                uppercaseTitle = `${uppercaseTitle.substr(0, totalChars)}…`;
            }

            drawTitle(ctx, uppercaseTitle, textLeft, titleTop);

            let trimmedFeaturing = featuring ? `feat. ${featuring.trim()}` : '';

            if (trimmedFeaturing && !titleWasTrimmed) {
                const charsLeft = Math.floor(
                    (titleWithFeaturingMaxWidth - titleWidth) / titleCharWidth
                );

                if (trimmedFeaturing.length > charsLeft) {
                    trimmedFeaturing = `${trimmedFeaturing.substr(
                        0,
                        charsLeft
                    )}…`;
                }

                ctx.font = `${titleTextSize}px "${fontMedium}"`;
                ctx.fillStyle = colorOrange;
                drawText(
                    ctx,
                    trimmedFeaturing,
                    textLeft + titleWidth + titleCharWidth / 2,
                    titleTop
                );
            }

            ctx.restore();

            // Draw artist
            ctx.save();
            ctx.font = `${artistTextSize}px "${fontBlack}"`;
            ctx.fillStyle = colorGray;
            drawText(ctx, artist, textLeft, artistTop);
            ctx.restore();

            // Draw trending icon
            ctx.drawImage(
                trendingImage,
                trendingIconLeft,
                trendingIconTop,
                trendingIconWidth,
                trendingIconHeight
            );

            // // Draw gradient overlays
            // ctx.save();
            // const gradient1 = ctx.createLinearGradient(0, 0, size, size);
            // const gradient2 = ctx.createLinearGradient(0, 0, size, 0);

            // gradient1.addColorStop(0, 'rgba(0, 0, 0, 0)');
            // gradient1.addColorStop(1, 'rgba(0, 0, 0, 0.4)');
            // ctx.fillStyle = gradient1;
            // ctx.fillRect(0, 0, size, size);

            // gradient2.addColorStop(0, 'rgba(0, 0, 0, 0.15)');
            // gradient2.addColorStop(1, 'rgba(0, 0, 0, 0)');
            // ctx.fillStyle = gradient2;
            // ctx.fillRect(0, 0, size, size);

            // ctx.restore();

            // // Draw title
            // ctx.save();

            // // 7 characters should be about 140px size
            // const titleSize = clamp((7 / title.length) * 140, 30, 140);

            // ctx.font = `${titleSize}px "${fontMedium}"`;
            // drawText(ctx, title.toUpperCase(), titleLeft, titleTop, {
            //     shadowBlur: 20,
            //     shadowColor: 'rgba(0, 0, 0, 0.2)',
            //     textColor: titleColor
            // });
            // ctx.restore();

            // Top it off with the border
            ctx.beginPath();
            ctx.rect(0, 0, size, size);
            ctx.lineWidth = 2;
            ctx.strokeStyle = colorBorder;
            ctx.stroke();

            resolve(canvas.toBuffer());
        });
    });
}

export default async function handler(
    data,
    { htmlOnly = false, imageDataOnly = false } = {}
) {
    const { musicArtist, musicTitle, featuring, uploaderSlug, musicId } = data;
    let { musicImage, musicBaseImage } = data;

    if (!uploaderSlug) {
        throw new EmailShouldNackException('No artist slug found.');
    }

    if (!musicArtist) {
        throw new EmailShouldNackException('No artist name found.');
    }

    // The base image might not be available on certain items so
    // we can fall back to the regular 275x275 image if thats the case
    musicBaseImage = musicBaseImage || musicImage;

    const checkedImages = await getAndAssertAllImagesExist(
        [musicImage, musicBaseImage],
        {
            errorMessage: `Trending email missing one or more images for: ${musicTitle} by ${musicArtist}`
        }
    );

    musicImage = checkedImages[0];
    musicBaseImage = checkedImages[1];

    const thumbnail = buildDynamicImage(musicBaseImage || musicImage, {
        width: 260,
        height: 260,
        max: true
    });

    let imageData = null;
    try {
        imageData = await getImageData(
            { artist: musicArtist, title: musicTitle, featuring },
            musicBaseImage,
            thumbnail
        );
    } catch (error) {
        throw new EmailShouldNackException(
            `Failed to generate image ${error.message}`
        );
    }

    if (imageDataOnly) {
        return imageData;
    }

    const fileName = `trending-${Date.now()}.png`;
    const artworkPath = `/tmp/${uploaderSlug}-${fileName}`;
    let bucketPath = `/trending/${uploaderSlug}`;

    if (musicId) {
        bucketPath += `/${musicId}`;
    }

    await writeFile(artworkPath, imageData);

    const upload = await exec(
        `${process.env.AWS_BIN_PATH} s3 cp ${artworkPath} s3://${
            process.env.STATIC_BUCKET
        }${bucketPath}/${fileName}`
    );

    if (upload.stderr) {
        if (process.env.NODE_ENV === 'development') {
            throw new Error(upload.stderr);
        }

        throw new Error('Error uploading image');
    }

    const { email } = data;
    const newData = {
        ...data,
        musicImage: `${process.env.STATIC_URL}${bucketPath}/${fileName}`
    };

    if (htmlOnly) {
        return renderToStaticMarkup(<TrendingMusicEmail {...newData} />);
    }

    const html = template(
        renderToStaticMarkup(<TrendingMusicEmail {...newData} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: TrendingMusicEmail.subject(data),
        campaign: TrendingMusicEmail.key(data),
        from: emailList.trending,
        html
    });
}
