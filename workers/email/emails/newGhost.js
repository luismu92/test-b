import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, emailList, template, TYPE_SPARKPOST } from '../helpers';
import NewGhostEmail from '../../../am-desktop/shared/email/NewGhostEmail';

export default function handler(data) {
    const {
        amp_partner_name: ampPartnerName,
        account_name: accountName,
        amp_partner_url: ampPartnerUrl,
        account_url: accountUrl
    } = data;
    const props = {
        ampPartnerName,
        ampPartnerUrl,
        accountName,
        accountUrl
    };
    const html = template(renderToStaticMarkup(<NewGhostEmail {...props} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: emailList.ampPartner,
        subject: NewGhostEmail.subject(props),
        campaign: NewGhostEmail.key(props),
        html,
        from: emailList.ampPartner
    });
}
