import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    emailList,
    emailReplyToList,
    template,
    TYPE_SPARKPOST
} from '../helpers';
import ClaimGhostEmail from '../../../am-desktop/shared/email/ClaimGhostEmail';

export default function handler(data) {
    const { email } = data;
    const props = {
        ...data,
        ee: encodeURIComponent(Buffer.from(email).toString('base64'))
    };
    const html = template(renderToStaticMarkup(<ClaimGhostEmail {...props} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: ClaimGhostEmail.subject(props),
        campaign: ClaimGhostEmail.key(props),
        html,
        from: emailList.support,
        replyTo: emailReplyToList.support
    });
}
