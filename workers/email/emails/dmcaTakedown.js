import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailList,
    emailReplyToList
} from '../helpers';
import DmcaTakedownEmail from '../../../am-desktop/shared/email/DmcaTakedownEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<DmcaTakedownEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.dmca,
        replyTo: emailReplyToList.dmca,
        subject: DmcaTakedownEmail.subject(data),
        campaign: DmcaTakedownEmail.key(data),
        html
    });
}
