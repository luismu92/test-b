import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST, emailList } from '../helpers';
import SubscriptionAccountHoldEmail from '../../../am-desktop/shared/email/SubscriptionAccountHoldEmail';

export default function handler(data) {
    const { email, replyTo } = data;
    const html = template(
        renderToStaticMarkup(<SubscriptionAccountHoldEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.support,
        replyTo: replyTo,
        subject: SubscriptionAccountHoldEmail.subject(data),
        campaign: SubscriptionAccountHoldEmail.key(data),
        html
    });
}
