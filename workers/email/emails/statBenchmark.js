import util from 'util';
import path from 'path';
import fs from 'fs';
import React from 'react';
import { createCanvas, registerFont, loadImage } from 'canvas';
import { canvasRGBA as blurImage } from 'stackblur-canvas';
import { renderToStaticMarkup } from 'react-dom/server';

import { buildDynamicImage } from 'utils/index';
import { EmailShouldNackException } from 'utils/errors';

import {
    sendEmail,
    emailList,
    template,
    TYPE_SPARKPOST,
    getAndAssertAllImagesExist
} from '../helpers';
import StatBenchmarkEmail from '../../../am-desktop/shared/email/StatBenchmarkEmail';

const width = 1024;
const height = 1282;

const numberTop = Math.round(height * 0.2);

const statTop = Math.round(height * 0.26);

const logoWidth = Math.round(width * 0.305);
const logoLeft = Math.round((width - logoWidth) / 2);
const logoTop = Math.round(height * 0.035);
// natural height / width
const logoHeight = Math.round((94 / 687) * logoWidth);

const artworkTop = Math.round(height * 0.275);
const artworkSize = Math.round(width * 0.7);
const artworkLeft = Math.round((width - artworkSize) / 2);

// const trendingText = 'Now Trending';
// const trendingTextTop = Math.round(size * 0.172);
// const trendingTextSize = Math.round((size * 38) / 510);

const titleTop = Math.round(height * 0.905);
const titleTextSize = Math.round(width * 0.049);
const titleWithFeaturingMaxWidth = Math.round(width * 0.7);

const artistTop = Math.round(height * 0.946);
const artistTextSize = Math.round(width * 0.038);

// 447x438
const colorBaseBackground = '#f9f9f9';
const colorBorder = 'rgba(0, 0, 0, 0.1)';
const colorOrange = '#ffa200';
const colorLightOrange = '#fdb800';
const colorDarkOrange = '#e6890f';
const colorGray = '#777777';
const colorBlack = '#000000';

const logoGradient = path.join(__dirname, '../assets/am-logo-gradient2.png');
const robotoMedium = 'Roboto Medium';
const robotoBlack = 'Roboto Black';
const gilroyBlack = 'Gilroy Black';
[
    {
        family: robotoMedium,
        path: path.join(__dirname, '../assets/Roboto-Medium.ttf')
    },
    {
        family: gilroyBlack,
        path: path.join(__dirname, '../assets/Gilroy-Black.ttf')
    },
    {
        family: robotoBlack,
        path: path.join(__dirname, '../assets/Roboto-Black.ttf')
    }
].forEach(({ family, path: fontPath }) => {
    registerFont(fontPath, {
        family: family
    });
});

const exec = util.promisify(require('child_process').exec);
const writeFile = util.promisify(fs.writeFile);

function drawText(ctx, text, x, y, options = {}) {
    const letters = text.split('');
    const letterSpacing = options.letterSpacing || 0;
    let lastX = x;

    if (options.shadows && options.shadows.length && options.textColor) {
        options.shadows.forEach((shadow) => {
            const { blur, color, offsetY = 0 } = shadow;

            ctx.fillStyle = 'transparent';
            ctx.shadowBlur = blur;
            ctx.shadowColor = color;
            ctx.shadowOffsetY = offsetY;

            letters.forEach((char) => {
                const { width: charWidth } = ctx.measureText(char);
                const offset = charWidth * letterSpacing;

                ctx.fillText(char, lastX, y);

                lastX += charWidth + offset;
            });

            lastX = x;
        });

        ctx.fillStyle = options.textColor;
    }

    return letters.reduce((totalWidth, char) => {
        const { width: charWidth } = ctx.measureText(char);
        const offset = charWidth * letterSpacing;

        ctx.fillText(char, lastX, y);

        lastX += charWidth + offset;

        return totalWidth + charWidth + offset;
    }, 0);
}

function drawNumber(ctx, number, left = 0, top = 0) {
    ctx.save();

    const numberFontSize = Math.round(width * 0.2);
    const text =
        typeof number === 'number'
            ? number.toLocaleString('en')
            : number.toUpperCase();

    ctx.font = `${numberFontSize}px "${gilroyBlack}"`;
    ctx.fillStyle = '#ffffff';

    const textWidth = drawText(ctx, text, left, top, {
        letterSpacing: -0.05
    });

    ctx.restore();

    return textWidth;
}

function drawStat(ctx, stat, left = 0, top = 0) {
    ctx.save();

    const statFontSize = Math.round(width * 0.064);
    const gradient3 = ctx.createLinearGradient(
        0,
        top - statFontSize * 0.6,
        0,
        top
    );

    gradient3.addColorStop(0, colorLightOrange);
    gradient3.addColorStop(1, colorDarkOrange);

    ctx.font = `${statFontSize}px "${gilroyBlack}"`;
    // ctx.fillStyle = colorOrange;
    ctx.fillStyle = gradient3;

    const textWidth = drawText(ctx, stat, left, top, {
        letterSpacing: -0.04,
        shadows: [
            {
                blur: width * 0.08,
                offsetY: height * 0.2,
                color: 'rgba(0, 0, 0, 0.5)'
            },
            {
                blur: width * 0.02,
                offsetY: -height * 0.001,
                color: 'rgba(0, 0, 0, 0.2)'
            }
        ],
        textColor: gradient3
    });

    ctx.restore();

    return textWidth;
}

function drawArtist(ctx, artist, left = 0, top = 0) {
    ctx.save();
    ctx.font = `${artistTextSize}px "${robotoMedium}"`;
    ctx.fillStyle = colorGray;

    const textWidth = drawText(ctx, artist, left, top);

    ctx.restore();

    return textWidth;
}

function drawTitle(ctx, title, left = 0, top = 0) {
    ctx.save();
    ctx.font = `${titleTextSize}px "${robotoBlack}"`;
    ctx.fillStyle = colorBlack;

    const textWidth = drawText(ctx, title, left, top, {
        letterSpacing: -0.01
    });

    ctx.restore();

    return textWidth;
}

function drawTitleAndFeature(ctx, title, featuring, left = 0, top = 0) {
    ctx.save();
    let textWidth = 0;
    const titleUpper = title.toUpperCase();

    ctx.font = `${titleTextSize}px "${robotoBlack}"`;
    ctx.fillStyle = colorBlack;

    const titleWidth = drawTitle(ctx, titleUpper, left, top);

    textWidth += titleWidth;

    let trimmedFeaturing = featuring ? `feat. ${featuring.trim()}` : '';

    if (trimmedFeaturing) {
        const charWidth = titleWidth / titleUpper.length;
        const charsLeft = Math.round(
            (titleWithFeaturingMaxWidth - titleWidth) / charWidth
        );

        if (trimmedFeaturing.length > charsLeft) {
            trimmedFeaturing = `${trimmedFeaturing.substr(0, charsLeft)}…`;
        }

        ctx.font = `${titleTextSize}px "${robotoBlack}"`;
        ctx.fillStyle = colorOrange;
        const padding = charWidth / 2;
        const featureWidth = drawText(
            ctx,
            trimmedFeaturing.toUpperCase(),
            left + titleWidth + padding,
            top,
            {
                letterSpacing: -0.01
            }
        );

        textWidth += featureWidth;
    }

    ctx.restore();

    return textWidth;
}

export function getImageData(metadata, image, thumb) {
    return Promise.all([
        loadImage(image),
        loadImage(thumb),
        loadImage(logoGradient)
    ]).then((values) => {
        const { artist, title, featuring, number, stat } = metadata;

        return new Promise((resolve) => {
            const [img, thumbnail, whiteLogo] = values;
            const canvas = createCanvas(width, height);
            const ctx = canvas.getContext('2d');

            // Draw number/stat to get width
            const numberWidth = drawNumber(ctx, number);
            const statWidth = drawStat(ctx, stat.toUpperCase());
            const artistWidth = drawArtist(ctx, artist);

            let formattedTitle = title;
            let formattedFeaturing = featuring;
            const tempTitleWidth = drawTitle(ctx, title.toUpperCase());

            if (tempTitleWidth > titleWithFeaturingMaxWidth) {
                const titleCharWidth = tempTitleWidth / title.length;
                const totalChars = Math.floor(
                    titleWithFeaturingMaxWidth / titleCharWidth
                );

                formattedFeaturing = '';
                formattedTitle = `${formattedTitle.substr(0, totalChars)}…`;
            }

            const titleWidth = drawTitleAndFeature(
                ctx,
                formattedTitle,
                formattedFeaturing
            );
            const lowAngleHeight = Math.round(height * 0.48);

            ctx.drawImage(thumbnail, 0, 0, width, height);

            // Start with blurred thumbnail as background
            blurImage(canvas, 0, 0, width, height, 80);

            // Draw low opacity black overlay
            ctx.save();

            const radialGradient = ctx.createRadialGradient(
                width / 2,
                lowAngleHeight / 2,
                0,
                width / 2,
                lowAngleHeight / 2,
                width / 2
            );

            // Add colors
            radialGradient.addColorStop(0, 'rgba(0, 0, 0, 0.05)');
            radialGradient.addColorStop(1, 'rgba(0, 0, 0, 0.5)');

            // Fill with gradient
            ctx.fillStyle = radialGradient;
            ctx.fillRect(0, 0, width, lowAngleHeight);
            ctx.restore();

            // Draw top gradient
            ctx.save();
            const topHeight = Math.round(height * 0.27);
            const gradient3 = ctx.createLinearGradient(0, 0, 0, topHeight);
            // gradient3.addColorStop(0, 'rgba(0, 0, 0, 0.4)');
            gradient3.addColorStop(0, 'rgba(0, 0, 0, 0.4)');
            gradient3.addColorStop(1, 'rgba(0, 0, 0, 0.0)');
            ctx.fillStyle = gradient3;
            ctx.fillRect(0, 0, width, topHeight);
            ctx.restore();

            // Draw solid light gray angled background
            ctx.save();
            ctx.fillStyle = colorBaseBackground;
            ctx.beginPath();
            ctx.moveTo(0, Math.round(height * 0.385));
            ctx.lineTo(width, lowAngleHeight);
            ctx.lineTo(width, height);
            ctx.lineTo(0, height);
            ctx.fill();
            ctx.restore();

            // Draw number
            const numberLeft = (width - numberWidth) / 2;
            drawNumber(ctx, number, numberLeft, numberTop);

            // Draw stat
            const statLeft = (width - statWidth) / 2;
            drawStat(ctx, stat.toUpperCase(), statLeft, statTop);

            // Draw logo
            ctx.drawImage(whiteLogo, logoLeft, logoTop, logoWidth, logoHeight);

            // Draw artwork
            ctx.save();
            ctx.shadowBlur = 40;
            ctx.shadowOffsetY = height * 0.02;
            ctx.shadowColor = 'rgba(0, 0, 0, 0.34)';
            ctx.drawImage(
                img,
                artworkLeft,
                artworkTop,
                artworkSize,
                artworkSize
            );
            ctx.restore();

            // Draw title and maybe featuring
            drawTitleAndFeature(
                ctx,
                formattedTitle,
                formattedFeaturing,
                (width - titleWidth) / 2,
                titleTop
            );

            // Draw artist
            drawArtist(ctx, artist, (width - artistWidth) / 2, artistTop);

            // Top it off with the border
            ctx.beginPath();
            ctx.rect(0, 0, width, height);
            ctx.lineWidth = 2;
            ctx.strokeStyle = colorBorder;
            ctx.stroke();

            resolve(canvas.toBuffer());
        });
    });
}

export default async function handler(
    data,
    { htmlOnly = false, imageDataOnly = false } = {}
) {
    const {
        artist,
        title,
        number,
        featuring,
        stat,
        uploaderSlug,
        musicId
    } = data;
    let { musicImage, musicBaseImage } = data;

    if (!uploaderSlug) {
        throw new EmailShouldNackException('No uploaderSlug passed.');
    }

    if (!number) {
        throw new EmailShouldNackException('No number passed.');
    }

    if (!stat) {
        throw new EmailShouldNackException('No stat passed.');
    }

    if (!artist) {
        throw new EmailShouldNackException('No artist passed.');
    }

    if (!title) {
        throw new EmailShouldNackException('No title passed.');
    }

    // The base image might not be available on certain items so
    // we can fall back to the regular 275x275 image if thats the case
    musicBaseImage = musicBaseImage || musicImage;

    const checkedImages = await getAndAssertAllImagesExist(
        [musicImage, musicBaseImage],
        {
            errorMessage: `Stat benchmark email missing one or more images for: ${title} by ${artist}`
        }
    );

    musicImage = checkedImages[0];
    musicBaseImage = checkedImages[1];

    const thumbnail = buildDynamicImage(musicBaseImage || musicImage, {
        width: 260,
        height: 260,
        blur: 20,
        max: true
    });

    let imageData = null;
    try {
        imageData = await getImageData(
            { artist, title, featuring, number, stat },
            musicBaseImage,
            thumbnail
        );
    } catch (error) {
        throw new EmailShouldNackException(
            `Failed to generate image ${error.message}`
        );
    }

    if (imageDataOnly) {
        return imageData;
    }

    const fileName = `benchmark-${Date.now()}.png`;
    const artworkPath = `/tmp/${uploaderSlug}-${fileName}`;
    let bucketPath = `/benchmark/${uploaderSlug}`;

    if (musicId) {
        bucketPath += `/${musicId}`;
    }

    await writeFile(artworkPath, imageData);

    const upload = await exec(
        `${process.env.AWS_BIN_PATH} s3 cp ${artworkPath} s3://${
            process.env.STATIC_BUCKET
        }${bucketPath}/${fileName}`
    );

    if (upload.stderr) {
        if (process.env.NODE_ENV === 'development') {
            throw new Error(upload.stderr);
        }

        throw new Error('Error uploading image');
    }

    const { email } = data;
    const newData = {
        ...data,
        musicImage: `${process.env.STATIC_URL}${bucketPath}/${fileName}`
    };

    if (htmlOnly) {
        return renderToStaticMarkup(<StatBenchmarkEmail {...newData} />);
    }

    const html = template(
        renderToStaticMarkup(<StatBenchmarkEmail {...newData} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: StatBenchmarkEmail.subject(data),
        campaign: StatBenchmarkEmail.key(data),
        from: emailList.trending,
        html
    });
}
