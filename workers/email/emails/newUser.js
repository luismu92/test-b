import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST } from '../helpers';
import NewUserEmail from '../../../am-desktop/shared/email/NewUserEmail';

export default function handler(data) {
    const { email } = data;

    const html = template(renderToStaticMarkup(<NewUserEmail {...data} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: NewUserEmail.subject(data),
        campaign: NewUserEmail.key(data),
        html
    });
}
