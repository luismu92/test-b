import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailList,
    emailReplyToList
} from '../helpers';
import UploadSuspendedAdminEmail from '../../../am-desktop/shared/email/UploadSuspendedAdminEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<UploadSuspendedAdminEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.suspend,
        replyTo: emailReplyToList.dmca,
        subject: UploadSuspendedAdminEmail.subject(data),
        campaign: UploadSuspendedAdminEmail.key(data),
        html
    });
}
