import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST, emailList } from '../helpers';
import SupportEmail from '../../../am-desktop/shared/email/SupportEmail';

export default function handler(data) {
    const { replyTo, email } = data;
    const html = template(renderToStaticMarkup(<SupportEmail {...data} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.support,
        replyTo: replyTo,
        subject: SupportEmail.subject(data),
        campaign: SupportEmail.key(data),
        html
    });
}
