import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailReplyToList
} from '../helpers';
import TranscoderErrorEmail from '../../../am-desktop/shared/email/TranscoderErrorEmail';

export default function handler(props) {
    const { email } = props;
    const html = template(
        renderToStaticMarkup(<TranscoderErrorEmail {...props} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailReplyToList.transcode,
        subject: TranscoderErrorEmail.subject(props),
        campaign: TranscoderErrorEmail.key(props),
        html
    });
}
