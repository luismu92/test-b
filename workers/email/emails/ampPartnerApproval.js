import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    emailList,
    emailReplyToList,
    template,
    TYPE_SPARKPOST
} from '../helpers';
import AmpPartnerApprovalEmail from '../../../am-desktop/shared/email/AmpPartnerApprovalEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<AmpPartnerApprovalEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: AmpPartnerApprovalEmail.subject(data),
        campaign: AmpPartnerApprovalEmail.key(data),
        html,
        from: emailList.ampPartner,
        replyTo: emailReplyToList.support
    });
}
