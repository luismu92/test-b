import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailList,
    emailReplyToList
} from '../helpers';
import PasswordChangedEmail from '../../../am-desktop/shared/email/PasswordChangedEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<PasswordChangedEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.noReply,
        replyTo: emailReplyToList.support,
        subject: PasswordChangedEmail.subject(data),
        campaign: PasswordChangedEmail.key(data),
        html
    });
}
