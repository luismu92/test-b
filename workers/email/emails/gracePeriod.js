import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST, emailList } from '../helpers';
import SubscriptionGracePeriodEmail from '../../../am-desktop/shared/email/SubscriptionGracePeriodEmail';

export default function handler(data) {
    const { email, replyTo } = data;
    const html = template(
        renderToStaticMarkup(<SubscriptionGracePeriodEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.support,
        replyTo: replyTo,
        subject: SubscriptionGracePeriodEmail.subject(data),
        campaign: SubscriptionGracePeriodEmail.key(data),
        html
    });
}
