import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailList,
    emailReplyToList
} from '../helpers';
import SuggestedTagAcceptEmail from '../../../am-desktop/shared/email/SuggestedTagAcceptEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<SuggestedTagAcceptEmail {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.support,
        replyTo: emailReplyToList.support,
        subject: SuggestedTagAcceptEmail.subject(data),
        campaign: SuggestedTagAcceptEmail.key(data),
        html
    });
}
