import newUser from './newUser';
import claimGhost from './claimGhost';
import newGhost from './newGhost';
import ampPartnerApproval from './ampPartnerApproval';
import forgotPassword from './forgotPassword';
import dmcaTakedown from './dmcaTakedown';
import uploadSuspended from './uploadSuspended';
import uploadSuspendedAdmin from './uploadSuspendedAdmin';
import dmcaNotice from './dmcaNotice';
import supportEmail from './supportEmail';
import supportEmailResponse from './supportEmailResponse';
import accountDeletion from './accountDeletion';
import transcoderError from './transcoderError';
import apiRequest from './apiRequest';
import trendingMusic from './trendingMusic';
import passwordChanged from './passwordChanged';
import optInVerification from './optInVerification';
import weeklyReport from './weeklyReport';
import trendingPlaylistVerification from './trendingPlaylistVerification';
import accountHold from './accountHold';
import gracePeriod from './gracePeriod';
import statBenchmark from './statBenchmark';
import verifiedCreator from './verifiedCreator';
import declinedCreator from './declinedCreator';
import suggestedTagAccept from './suggestedTagAccept';

import NewUserEmail from '../../../am-desktop/shared/email/NewUserEmail';
import ClaimGhostEmail from '../../../am-desktop/shared/email/ClaimGhostEmail';
import NewGhostEmail from '../../../am-desktop/shared/email/NewGhostEmail';
import AmpPartnerApprovalEmail from '../../../am-desktop/shared/email/AmpPartnerApprovalEmail';
import ForgotPasswordEmail from '../../../am-desktop/shared/email/ForgotPasswordEmail';
import DmcaTakedownEmail from '../../../am-desktop/shared/email/DmcaTakedownEmail';
import UploadSuspendedEmail from '../../../am-desktop/shared/email/UploadSuspendedEmail';
import UploadSuspendedAdminEmail from '../../../am-desktop/shared/email/UploadSuspendedAdminEmail';
import DmcaNotice from '../../../am-desktop/shared/email/DmcaNotice';
import SupportEmail from '../../../am-desktop/shared/email/SupportEmail';
import SupportEmailResponse from '../../../am-desktop/shared/email/SupportEmailResponse';
import AccountDeletion from '../../../am-desktop/shared/email/AccountDeletion';
import TrendingMusicEmail from '../../../am-desktop/shared/email/TrendingMusicEmail';
import ApiRequestEmail from '../../../am-desktop/shared/email/ApiRequestEmail';
import TranscoderErrorEmail from '../../../am-desktop/shared/email/TranscoderErrorEmail';
import PasswordChangedEmail from '../../../am-desktop/shared/email/PasswordChangedEmail';
import OptInEmail from '../../../am-desktop/shared/email/OptInEmail';
import WeeklyReportEmail from '../../../am-desktop/shared/email/WeeklyReportEmail';
import AddToVerifiedPlaylist from '../../../am-desktop/shared/email/AddToVerifiedPlaylist';
import SubscriptionAccountHoldEmail from '../../../am-desktop/shared/email/SubscriptionAccountHoldEmail';
import SubscriptionGracePeriodEmail from '../../../am-desktop/shared/email/SubscriptionGracePeriodEmail';
import StatBenchmarkEmail from '../../../am-desktop/shared/email/StatBenchmarkEmail';
import VerifiedCreatorEmail from '../../../am-desktop/shared/email/VerifiedCreatorEmail';
import AuthenticatedDeniedEmail from '../../../am-desktop/shared/email/AuthenticatedDeniedEmail';
import SuggestedTagAcceptEmail from '../../../am-desktop/shared/email/SuggestedTagAcceptEmail';

export default [
    {
        component: NewUserEmail,
        handler: newUser,
        requiredFields: ['hash']
    },
    {
        component: NewUserEmail,
        handler: newUser,
        requiredFields: [],
        extraProps: {
            isVerifiedAlready: true
        }
    },
    {
        component: NewUserEmail,
        handler: newUser,
        requiredFields: [],
        extraProps: {
            isResendEmail: true
        }
    },
    {
        component: ClaimGhostEmail,
        handler: claimGhost,
        requiredFields: ['url_slug', 'code']
    },
    {
        component: NewGhostEmail,
        handler: newGhost,
        requiredFields: [
            'amp_partner_name',
            'account_name',
            'amp_partner_url',
            'account_url'
        ]
    },
    {
        component: AmpPartnerApprovalEmail,
        handler: ampPartnerApproval,
        requiredFields: []
    },
    {
        component: ForgotPasswordEmail,
        handler: forgotPassword,
        requiredFields: ['token']
    },
    {
        component: DmcaTakedownEmail,
        handler: dmcaTakedown,
        requiredFields: ['link']
    },
    {
        component: UploadSuspendedEmail,
        handler: uploadSuspended,
        requiredFields: ['link']
    },
    {
        component: UploadSuspendedAdminEmail,
        handler: uploadSuspendedAdmin,
        requiredFields: ['link']
    },
    {
        component: DmcaNotice,
        handler: dmcaNotice,
        requiredFields: ['message', 'url']
    },
    {
        component: SupportEmail,
        handler: supportEmail,
        requiredFields: ['replyTo', 'subject']
    },
    {
        component: SupportEmailResponse,
        handler: supportEmailResponse,
        requiredFields: ['fromEmail']
    },
    {
        component: AccountDeletion,
        handler: accountDeletion,
        requiredFields: []
    },
    {
        component: TrendingMusicEmail,
        handler: trendingMusic,
        requiredFields: ['name', 'musicTitle', 'musicUrl']
    },
    {
        component: TranscoderErrorEmail,
        handler: transcoderError,
        requiredFields: []
    },
    {
        component: ApiRequestEmail,
        handler: apiRequest,
        requiredFields: []
    },
    {
        component: PasswordChangedEmail,
        handler: passwordChanged,
        requiredFields: []
    },
    {
        component: OptInEmail,
        handler: optInVerification,
        requiredFields: ['confirmUrl']
    },
    {
        component: WeeklyReportEmail,
        handler: weeklyReport,
        requiredFields: []
    },
    {
        component: AddToVerifiedPlaylist,
        handler: trendingPlaylistVerification,
        requiredFields: []
    },
    {
        component: SubscriptionAccountHoldEmail,
        handler: accountHold,
        requiredFields: []
    },
    {
        component: SubscriptionGracePeriodEmail,
        handler: gracePeriod,
        requiredFields: []
    },
    {
        component: StatBenchmarkEmail,
        handler: statBenchmark,
        requiredFields: []
    },
    {
        component: VerifiedCreatorEmail,
        handler: verifiedCreator,
        requiredFields: []
    },
    {
        component: AuthenticatedDeniedEmail,
        handler: declinedCreator,
        requiredFields: []
    },
    {
        component: SuggestedTagAcceptEmail,
        handler: suggestedTagAccept,
        requiredFields: ['tag', 'musictype', 'title', 'url']
    }
].reduce((acc, config) => {
    const props = config.extraProps || {};

    if (typeof config.component.key !== 'function') {
        throw new Error(
            `Component ${
                config.component
            } must have a static key() function that returns a string.`
        );
    }

    const key = config.component.key(props);

    if (typeof key !== 'string') {
        throw new Error(
            `Component ${
                config.component
            } must return a string from the static key() function.`
        );
    }

    if (!!acc[key]) {
        throw new Error(`Config already exists for key: ${key}\n${acc[key]}`);
    }

    acc[key] = config;

    return acc;
}, {});
