import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST_ANNOUNCEMENTS,
    TYPE_MAILTRAP,
    emailList,
    emailReplyToList
} from '../helpers';
import WeeklyReportEmail from '../../../am-desktop/shared/email/WeeklyReportEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<WeeklyReportEmail {...data} />)
    );
    const mailOptions = {
        to: email,
        from: emailList.noReply,
        replyTo: emailReplyToList.support,
        subject: WeeklyReportEmail.subject(data),
        campaign: WeeklyReportEmail.key(data),
        html
    };

    if (process.env.NODE_ENV === 'development') {
        return sendEmail(TYPE_MAILTRAP, mailOptions);
    }

    return sendEmail(TYPE_SPARKPOST_ANNOUNCEMENTS, mailOptions);
}
