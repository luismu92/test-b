import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, emailList, template, TYPE_SPARKPOST } from '../helpers';
import AddToVerifiedPlaylist from '../../../am-desktop/shared/email/AddToVerifiedPlaylist';

export default function handler(data) {
    const { email } = data;
    const html = template(
        renderToStaticMarkup(<AddToVerifiedPlaylist {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        subject: AddToVerifiedPlaylist.subject(data),
        campaign: AddToVerifiedPlaylist.key(data),
        from: emailList.trending,
        html
    });
}
