import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailReplyToList
} from '../helpers';
import AccountDeletion from '../../../am-desktop/shared/email/AccountDeletion';

export default function handler(props) {
    const { email, artistName, replyTo } = props;
    const html = template(renderToStaticMarkup(<AccountDeletion {...props} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: {
            ...emailReplyToList.support,
            name: artistName
        },
        replyTo: replyTo,
        subject: AccountDeletion.subject(props),
        campaign: AccountDeletion.key(props),
        html
    });
}
