import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST, emailList } from '../helpers';
import AuthenticatedDeniedEmail from '../../../am-desktop/shared/email/AuthenticatedDeniedEmail';

export default function handler(data) {
    const {
        email: email,
        replyTo,
        profile_image: profileImage,
        url_slug: urlSlug
    } = data;

    const props = {
        profileImage,
        urlSlug
    };
    const html = template(
        renderToStaticMarkup(<AuthenticatedDeniedEmail {...props} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.creator,
        replyTo: replyTo,
        subject: AuthenticatedDeniedEmail.subject(data),
        campaign: AuthenticatedDeniedEmail.key(data),
        html
    });
}
