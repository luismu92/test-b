import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST, emailList } from '../helpers';
import DmcaNotice from '../../../am-desktop/shared/email/DmcaNotice';

export default function handler(data) {
    const { email, replyTo } = data;
    const html = template(renderToStaticMarkup(<DmcaNotice {...data} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.dmca,
        replyTo: replyTo,
        subject: DmcaNotice.subject(data),
        campaign: DmcaNotice.key(data),
        html
    });
}
