import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST,
    emailReplyToList
} from '../helpers';
import ApiRequestEmail from '../../../am-desktop/shared/email/ApiRequestEmail';

export default function handler(props) {
    const { email, replyTo } = props;
    const html = template(renderToStaticMarkup(<ApiRequestEmail {...props} />));

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailReplyToList.api,
        replyTo: replyTo,
        subject: ApiRequestEmail.subject(props),
        campaign: ApiRequestEmail.key(props),
        html
    });
}
