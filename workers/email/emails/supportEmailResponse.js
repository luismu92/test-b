import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST } from '../helpers';
import SupportEmailResponse from '../../../am-desktop/shared/email/SupportEmailResponse';

export default function handler(data) {
    const { fromEmail, email, subject } = data;
    const html = template(
        renderToStaticMarkup(<SupportEmailResponse {...data} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: {
            // Sparkpost domains need to be from email.audiomack.com
            email: fromEmail.replace('@audiomack.com', '@email.audiomack.com'),
            name: subject
        },
        replyTo: fromEmail,
        subject: SupportEmailResponse.subject(data),
        campaign: SupportEmailResponse.key(data),
        html
    });
}
