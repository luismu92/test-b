import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import {
    sendEmail,
    template,
    TYPE_SPARKPOST_ANNOUNCEMENTS,
    TYPE_MAILTRAP,
    emailList,
    emailReplyToList
} from '../helpers';
import OptInEmail from '../../../am-desktop/shared/email/OptInEmail';

export default function handler(data) {
    const { email } = data;
    const html = template(renderToStaticMarkup(<OptInEmail {...data} />));
    const mailOptions = {
        to: email,
        from: emailList.noReply,
        replyTo: emailReplyToList.support,
        subject: OptInEmail.subject(data),
        campaign: OptInEmail.key(data),
        html
    };

    if (process.env.NODE_ENV === 'development') {
        return sendEmail(TYPE_MAILTRAP, mailOptions);
    }

    return sendEmail(TYPE_SPARKPOST_ANNOUNCEMENTS, mailOptions);
}
