import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sendEmail, template, TYPE_SPARKPOST, emailList } from '../helpers';
import VerifiedCreatorEmail from '../../../am-desktop/shared/email/VerifiedCreatorEmail';

export default function handler(data) {
    const {
        email: email,
        replyTo,
        name: name,
        profile_image: profileImage,
        url_slug: urlSlug
    } = data;

    const props = {
        name,
        email,
        profileImage,
        urlSlug
    };
    const html = template(
        renderToStaticMarkup(<VerifiedCreatorEmail {...props} />)
    );

    return sendEmail(TYPE_SPARKPOST, {
        to: email,
        from: emailList.creator,
        replyTo: replyTo,
        subject: VerifiedCreatorEmail.subject(data),
        campaign: VerifiedCreatorEmail.key(data),
        html
    });
}
