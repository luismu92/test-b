import htmlToText from 'html-to-text';
import sendgrid from '@sendgrid/mail';
import SparkPost from 'sparkpost';
import nodemailer from 'nodemailer';
import { headRequest } from 'utils/index';
import { EmailShouldNackException } from 'utils/errors';
import emailMap from './emails/index';

export const SPARKPOST_IP_POOL_TRANSACTIONAL = 'am_transactional';
export const SPARKPOST_IP_POOL_ANNOUNCEMENTS = 'announcements';

export const TYPE_MAILTRAP = 'mailtrap';
export const TYPE_SENDGRID = 'sendgrid';
export const TYPE_SPARKPOST = 'sparkpost';
export const TYPE_SPARKPOST_ANNOUNCEMENTS = 'announcements';

const sparkpost = new SparkPost(process.env.SPARKPOST_API_KEY, {
    debug: process.env.NODE_ENV === 'development'
});

sendgrid.setApiKey(process.env.SENDGRID_API_KEY);

export const emailList = {
    noReply: {
        email: 'no-reply@email.audiomack.com',
        name: process.env.AM_NAME
    },
    accountRecovery: {
        email: 'passwordrecovery@email.audiomack.com',
        name: `${process.env.AM_NAME} Account Recovery`
    },
    support: {
        email: 'support@email.audiomack.com',
        name: `${process.env.AM_NAME} Support`
    },
    ampPartner: {
        email: 'amp@email.audiomack.com',
        name: `${process.env.AM_NAME} AMP Partner Notifier`
    },
    dmca: {
        email: 'dmca@email.audiomack.com',
        name: `${process.env.AM_NAME} DMCA Notifier`
    },
    suspend: {
        email: 'dmca@email.audiomack.com',
        name: `${process.env.AM_NAME} Suspension Notifier`
    },
    transcode: {
        email: 'support@email.audiomack.com',
        name: `${process.env.AM_NAME} Audio Notifier`
    },
    api: {
        email: 'support@email.audiomack.com',
        name: 'API Access Request'
    },
    trending: {
        email: 'no-reply@email.audiomack.com',
        name: 'Trending Notification'
    },
    creator: {
        email: 'creator@email.audiomack.com',
        name: `${process.env.AM_NAME} Creators Program`
    }
};

export const emailReplyToList = {
    support: 'support@audiomack.com',
    dmca: 'dmca@audiomack.com',
    creator: 'creator@audiomack.com'
};

export function template(html) {
    return `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
            </head>
            <body style="margin: 0; padding: 0; background:#f4f4f4">
                ${html}
            </body>
        </html>
    `;
}

function escapeCurlyBracesForSparkpost(text) {
    return text
        .replace(/{|}/g, (match) => {
            if (match === '{') {
                return '{{ opening_single_curly() }}';
            }

            return '{{ closing_single_curly() }}';
        })
        .trim();
}

function formatFromKey(fromKey) {
    if (fromKey && fromKey.name && typeof fromKey.name === 'string') {
        return {
            ...fromKey,
            name: escapeCurlyBracesForSparkpost(fromKey.name)
        };
    }

    return fromKey;
}

const sparkPostTransMissionsOptions = (ipPool) => ({
    to,
    html,
    subject,
    from: fromKey = emailList.noReply,
    replyTo,
    campaign,
    attachments
} = {}) => {
    return {
        campaign_id: campaign,
        options: {
            sandbox: false,
            ip_pool: ipPool
        },
        content: {
            from: formatFromKey(fromKey),
            reply_to: replyTo,
            subject: escapeCurlyBracesForSparkpost(subject),
            html: escapeCurlyBracesForSparkpost(html),
            text: escapeCurlyBracesForSparkpost(htmlToText.fromString(html)),
            attachments: attachments
        },
        recipients: [{ address: to }]
    };
};

const _sendEmailWithSparkPost = (transmissionsOptions) => {
    return sparkpost.transmissions.send(transmissionsOptions);
};

function _sendEmailWithSendGrid({
    to,
    html,
    subject,
    from: fromKey = emailList.noReply,
    replyTo
} = {}) {
    return sendgrid.send({
        to: to,
        from: fromKey,
        replyTo: replyTo,
        subject: subject,
        text: htmlToText.fromString(html),
        html: html
    });
}

async function _sendWithMailTrap({ to, html, subject, replyTo } = {}) {
    if (process.env.NODE_ENV !== 'development') {
        throw new Error(
            `Can not use mailtrap in environement: ${process.env.NODE_ENV}`
        );
    }

    const transporter = nodemailer.createTransport({
        host: process.env.MAILTRAP_HOST,
        port: process.env.MAILTRAP_PORT,
        auth: {
            user: process.env.MAILTRAP_USER,
            pass: process.env.MAILTRAP_PASS
        }
    });

    const info = await transporter.sendMail({
        from: emailList.noReply,
        to: to,
        replyTo: replyTo,
        subject: subject,
        text: htmlToText.fromString(html),
        html: html
    });

    console.log('Message sent: %s', info.messageId);
}

export function sendEmail(provider, mailOptions) {
    if (provider === TYPE_SPARKPOST) {
        return _sendEmailWithSparkPost(
            sparkPostTransMissionsOptions(SPARKPOST_IP_POOL_TRANSACTIONAL)(
                mailOptions
            )
        );
    }

    if (provider === TYPE_SPARKPOST_ANNOUNCEMENTS) {
        return _sendEmailWithSparkPost(
            sparkPostTransMissionsOptions(SPARKPOST_IP_POOL_ANNOUNCEMENTS)(
                mailOptions
            )
        );
    }

    if (provider === TYPE_MAILTRAP) {
        return _sendWithMailTrap(mailOptions).catch(console.error);
    }

    return _sendEmailWithSendGrid(mailOptions);
}

/**
 * Takes image url arguments and either returns them back if they return
 * a 200 status code. If one image fails, they are replaced by the `defaultImage`.
 *
 * @param  {Array} images array of images to check
 * @param  {Object} options
 * @return {Array}
 * @throws {EmailShouldNackException} If all images and the defaultImage doesn't exist
 */
export async function getAndAssertAllImagesExist(
    images,
    {
        defaultImage = 'https://assets.audiomack.com/default-email-image.jpg',
        errorMessage = 'getAndAssertAllImagesExistError - Missing one or more images'
    } = {}
) {
    const imageResponseCodes = await Promise.all(
        images.map((i) => headRequest(i).catch(() => 404))
    );
    const allImagesExist = imageResponseCodes.every((status) => status === 200);

    if (!allImagesExist) {
        const defaultMusicImageStatus = await headRequest(defaultImage).catch(
            () => {
                return 404;
            }
        );

        if (defaultMusicImageStatus !== 200) {
            throw new EmailShouldNackException(errorMessage);
        }

        return images.map(() => defaultImage);
    }

    return images;
}

export function processMessage({ type, ...rest } = {}) {
    if (!emailMap[type]) {
        return Promise.reject(`Invalid email type: ${type}`);
    }

    const { handler, requiredFields, extraProps = {} } = emailMap[type];
    const isValid = requiredFields.every((field) => {
        return !!rest[field];
    });

    if (!isValid) {
        return Promise.reject(
            `Missing one or more required fields for type: ${type}. Data: ${JSON.stringify(
                rest
            )}. Required fields: ${JSON.stringify(requiredFields)}`
        );
    }

    if (process.env.NODE_ENV !== 'production' && process.env.TESTING_EMAIL_TO) {
        rest.email = process.env.TESTING_EMAIL_TO;
    }

    const email = rest.email;

    if (!email) {
        return Promise.reject(
            `No email provided for type: ${type}. Data: ${JSON.stringify(rest)}`
        );
    }

    return handler({
        ...extraProps,
        ...rest
    });
}
