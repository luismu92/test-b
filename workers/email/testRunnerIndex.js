// Use this to test out the canvas workers instead of restarting
// the main app every time a change is made
//
// Run:
// NODE_PATH=$NODE_PATH:./am-shared node_modules/.bin/nodemon --inspect=3004 workers/email/testRunnerIndex.js
require('../../am-shared/utils/setEnv');
require('@babel/register');
require('./testRunner');
