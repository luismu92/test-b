/* global workbox, caches, Response */
// fallback URLs
const FALLBACK_HTML_URL = '/static/offline-mobile.html';
const FALLBACK_IMAGE_URL = '/static/offline.svg';

// Cache the Google Fonts stylesheets with a stale while revalidate strategy.
workbox.routing.registerRoute(
    /^https:\/\/fonts\.googleapis\.com/,
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'google-fonts-stylesheets'
    })
);

// Cache the Google Fonts webfont files with a cache first strategy for 1 year.
workbox.routing.registerRoute(
    /^https:\/\/fonts\.gstatic\.com/,
    new workbox.strategies.CacheFirst({
        cacheName: 'google-fonts-webfonts',
        plugins: [
            new workbox.cacheableResponse.Plugin({
                statuses: [0, 200]
            }),
            new workbox.expiration.Plugin({
                maxAgeSeconds: 60 * 60 * 24 * 365
            })
        ]
    })
);

workbox.precaching.precacheAndRoute([
    { url: '/am-shell' }
], {});

workbox.routing.registerNavigationRoute(workbox.precaching.getCacheKeyForURL('/am-shell'));

workbox.routing.registerRoute(
    /\.(?:js|css)$/,
    new workbox.strategies.StaleWhileRevalidate(),
);

workbox.routing.registerRoute(
    /^https:\/\/dcf.aws.audiomack.com\/v1\//i,
    // /^https:\/\/api.audiomack.com\/v1\//i,
    new workbox.strategies.NetworkFirst({
        cacheName: 'audiomack-api-v1'
    })
);

// This "catch" handler is triggered when any of the other routes fail to
// generate a response.
// https://developers.google.com/web/tools/workbox/guides/advanced-recipes#provide_a_fallback_response_to_a_route
workbox.routing.setCatchHandler((obj) => {
    const { event, request, url } = obj; // eslint-disable-line no-unused-vars

    // Use event, request, and url to figure out how to respond.
    // One approach would be to use request.destination, see
    // https://medium.com/dev-channel/service-worker-caching-strategies-based-on-request-types-57411dd7652c
    switch ((request || {}).destination) {
        case 'document':
            return caches.match(FALLBACK_HTML_URL);

        case 'image':
            return caches.match(FALLBACK_IMAGE_URL);

        default:
            // If we don't have a fallback, just return an error response.
            return Response.error();
    }
});

// Use a stale-while-revalidate strategy for all other requests.
workbox.routing.setDefaultHandler(new workbox.strategies.StaleWhileRevalidate());
