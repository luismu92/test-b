#use base docker image this is centos
FROM 130635258951.dkr.ecr.us-east-1.amazonaws.com/base
ARG NODE_ENV=production
ENV NODE_ENV ${NODE_ENV}

ARG AM_URL=https://audiomack.com
ENV AM_URL ${AM_URL}

ARG API_URL=https://audiomack.com/v1
ENV API_URL ${API_URL}

ARG AM_DOMAIN=audiomack.com
ENV AM_DOMAIN ${AM_DOMAIN}

ARG API_CONSUMER_KEY=audiomack-js
ENV API_CONSUMER_KEY ${API_CONSUMER_KEY}

ARG API_CONSUMER_SECRET=d414055457e991807398b0a61bc15b00
ENV API_CONSUMER_SECRET ${API_CONSUMER_SECRET}

ARG FACEBOOK_APP_ID=1482202045124554
ENV FACEBOOK_APP_ID ${FACEBOOK_APP_ID}

ARG INSTAGRAM_APP_ID=56ff0104ada145aa81fa59358814bd12
ENV INSTAGRAM_APP_ID ${INSTAGRAM_APP_ID}

ARG INSTAGRAM_REDIRECT_URI=http://localhost:3000/edit/profile
ENV INSTAGRAM_REDIRECT_URI ${INSTAGRAM_REDIRECT_URI}

ARG GOOGLE_AUTH_ID=122326890670-fnq113nf7ks4ogtpjpc7bsndm8eu8n9b.apps.googleusercontent.com
ENV GOOGLE_AUTH_ID ${GOOGLE_AUTH_ID}

ARG API_VERSION=v1
ENV API_VERSION ${API_VERSION}

ARG GIT_COMMIT
ENV GIT_COMMIT ${GIT_COMMIT}

ARG REDIS_MAIN_URL
ENV REDIS_MAIN_URL ${REDIS_MAIN_URL}

ARG NEW_RELIC_KEY
ENV NEW_RELIC_KEY ${NEW_RELIC_KEY}

ARG NEW_RELIC_APP_ID
ENV NEW_RELIC_APP_ID ${NEW_RELIC_APP_ID}

ARG NEW_RELIC_ADMIN_KEY
ENV NEW_RELIC_ADMIN_KEY ${NEW_RELIC_ADMIN_KEY}

ARG NEW_RELIC_LICENSE
ENV NEW_RELIC_LICENSE ${NEW_RELIC_LICENSE}

RUN yum update -y && yum install -y epel-release

#install all libraries
RUN yum install gcc gcc-c++ git which wget awscli -y
RUN yum install npm -y

# Install nvm with node and npm
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 6.9.1

RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

RUN wget https://dl.yarnpkg.com/rpm/yarn.repo -O /etc/yum.repos.d/yarn.repo
RUN yum install yarn -y
RUN npm install pm2 -g

EXPOSE 3000

# Cache node module folder if nothing in package.json has changed
# http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js/
ADD ./package.json /tmp/package.json
ADD ./yarn.lock /tmp/yarn.lock
RUN cd /tmp && NODE_ENV=development yarn
RUN npm rebuild node-sass
RUN mkdir -p /var/www/audiomack-js && cp -a /tmp/node_modules /var/www/audiomack-js

ADD . /var/www/audiomack-js
WORKDIR /var/www/audiomack-js

RUN npm run build

RUN aws s3 cp /var/www/audiomack-js/public/static/dist/ s3://audiomack.webhost/${NODE_ENV}/static/dist/ --recursive --exclude "*.map" --cache-control max-age=31536000

RUN npm run publish-sourcemaps

RUN curl -X POST "https://api.newrelic.com/v2/applications/$NEW_RELIC_APP_ID/deployments.json" \
    -H "X-Api-Key:$NEW_RELIC_ADMIN_KEY" -i \
    -H "Content-Type: application/json" \
    -d "{ \"deployment\": { \"revision\": \"$GIT_COMMIT\" } }"

CMD pm2-docker start --auto-exit --env ${NODE_ENV} audiomack.config.js
