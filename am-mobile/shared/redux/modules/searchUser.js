import api from 'api/index';
import { suffix } from 'utils/index';

const PREFIX = 'am/searchUser/';

export const QUERY_TYPE_FAVORITES = 'favorites';
export const QUERY_TYPE_PLAYLISTS = 'playlists';
export const QUERY_TYPE_UPLOADS = 'uploads';

const USER_SEARCH = `${PREFIX}USER_SEARCH`;
const USER_SEARCH_INPUT = `${PREFIX}USER_SEARCH_INPUT`;
const USER_SET_QUERY_TYPE = `${PREFIX}USER_SET_QUERY_TYPE`;
const USER_SET_SEARCHING_STATE = `${PREFIX}USER_SET_SEARCHING_STATE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;

const defaultState = {
    activeType: QUERY_TYPE_FAVORITES,
    list: [],
    page: 1,
    onLastPage: false,
    query: '',
    loading: false,
    searchActive: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(USER_SEARCH, 'REQUEST'):
            return Object.assign({}, state, {
                loading: true
            });

        case suffix(USER_SEARCH, 'FAILURE'):
            return Object.assign({}, state, {
                loading: false
            });

        case USER_SEARCH: {
            let newData = action.resolved.results
                ? action.resolved.results[action.show]
                : [];
            let onLastPage = false;

            if (state.activeType === action.activeType && state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (
                state.page !== 1 &&
                !action.resolved.results[action.show].length
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                loading: false,
                onLastPage,
                list: newData,
                searchActive: false,
                query: action.query
            });
        }

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case USER_SEARCH_INPUT:
            return Object.assign({}, state, {
                query: action.query
            });

        case USER_SET_SEARCHING_STATE:
            return Object.assign({}, state, {
                searchActive: action.bool
            });

        case USER_SET_QUERY_TYPE:
            return Object.assign({}, state, {
                activeType: action.queryType
            });

        default:
            return state;
    }
}

export function getSearch(query = '', options = {}) {
    return (dispatch, getState) => {
        const state = getState().searchUser;
        const { token, secret } = getState().currentUser;
        const q = query || state.query;

        const {
            limit = 20,
            page = state.page,
            type = state.activeType
        } = options;

        return dispatch({
            type: USER_SEARCH,
            query: q,
            show: type,
            page,
            limit,
            activeType: type,
            promise: api.search.getUser(q, token, secret, {
                show: type,
                page,
                limit
            })
        });
    };
}

export function searchInput(query = '') {
    return {
        type: USER_SEARCH_INPUT,
        query
    };
}

export function setQueryType(queryType = defaultState.activeType) {
    return {
        type: USER_SET_QUERY_TYPE,
        queryType
    };
}

export function setSearchingState(bool) {
    return {
        type: USER_SET_SEARCHING_STATE,
        bool
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}
