export { default } from 'redux/modules/artist/favorites';

// Export all action dispatchers from the common reducer
export {
    getArtistFavorites,
    setPage,
    nextPage,
    reset
} from 'redux/modules/artist/favorites';
