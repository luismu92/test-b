export { default } from 'redux/modules/artist/uploads';

// Export all action dispatchers from the common reducer
export {
    getArtistUploads,
    setPage,
    nextPage,
    reset
} from 'redux/modules/artist/uploads';
