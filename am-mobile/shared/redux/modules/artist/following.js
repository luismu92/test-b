export { default } from 'redux/modules/artist/following';

// Export all action dispatchers from the common reducer
export {
    getArtistFollowing,
    setPage,
    nextPage,
    reset
} from 'redux/modules/artist/following';
