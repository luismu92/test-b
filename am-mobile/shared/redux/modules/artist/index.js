export { default } from 'redux/modules/artist/index';

export { getArtist } from 'redux/modules/artist/index';
