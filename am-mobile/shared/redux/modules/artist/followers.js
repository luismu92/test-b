export { default } from 'redux/modules/artist/followers';

// Export all action dispatchers from the common reducer
export {
    getArtistFollowers,
    setPage,
    nextPage,
    reset
} from 'redux/modules/artist/followers';
