import commonReducer, { PREFIX } from 'redux/modules/playlist';
import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const DESELECT_PLAYLIST = `${PREFIX}/DESELECT_PLAYLIST`;
const SELECT_PLAYLIST = `${PREFIX}/SELECT_PLAYLIST`;
const SAVE_SELECTION = `${PREFIX}/SAVE_SELECTION`;

const defaultState = {
    selectedPlaylists: []
};

export default function reducer(state = defaultState, action) {
    const partialState = {
        // Make sure we get both default states
        ...state,
        ...commonReducer(undefined, action),

        // Run common reducer
        ...commonReducer(state, action)
    };

    switch (action.type) {
        case SELECT_PLAYLIST: {
            const selectedPlaylists = state.selectedPlaylists;

            if (selectedPlaylists.indexOf(action.playlistId) === -1) {
                selectedPlaylists.push(action.playlistId);
            }

            return {
                ...partialState,
                selectedPlaylists
            };
        }

        case DESELECT_PLAYLIST: {
            const selectedPlaylists = state.selectedPlaylists;
            const index = selectedPlaylists.indexOf(action.playlistId);

            selectedPlaylists.splice(index, 1);

            return {
                ...partialState,
                selectedPlaylists
            };
        }

        case requestSuffix(SAVE_SELECTION): {
            return {
                ...partialState,
                loading: true,
                errors: []
            };
        }

        case failSuffix(SAVE_SELECTION): {
            return {
                ...partialState,
                loading: false
            };
        }

        case SAVE_SELECTION: {
            return {
                ...partialState,
                selectedPlaylists: [],
                songToAdd: null,
                songToAddCallback: null,
                loading: false
            };
        }

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    CREATE_PLAYLIST,
    ADD_SONG_TO_PLAYLIST,
    DELETE_SONG_FROM_PLAYLIST,
    DELETE_PLAYLIST,
    FAVORITE_PLAYLIST,
    UNFAVORITE_PLAYLIST,
    getInfo,
    getInfoWithSlugs,
    addSong,
    clearAddSong,
    reorderTracks,
    addSongToPlaylist,
    deleteSong,
    createPlaylist,
    savePlaylistDetails,
    deletePlaylist,
    favoritePlaylist,
    unfavoritePlaylist,
    getFeatured,
    reset
} from 'redux/modules/playlist';

export function saveSelection() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const {
            songToAdd,
            selectedPlaylists,
            songToAddCallback
        } = getState().playlist;
        const promises = selectedPlaylists.map((playlistId) => {
            return api.playlist.addSong(
                playlistId,
                songToAdd.id,
                token,
                secret
            );
        });

        return dispatch({
            type: SAVE_SELECTION,
            promise: Promise.all(promises).then(() => {
                if (typeof songToAddCallback === 'function') {
                    return songToAddCallback();
                }

                return null;
            })
        });
    };
}

export function selectPlaylist(playlistId) {
    return {
        type: SELECT_PLAYLIST,
        playlistId
    };
}

export function deselectPlaylist(playlistId) {
    return {
        type: DESELECT_PLAYLIST,
        playlistId
    };
}
