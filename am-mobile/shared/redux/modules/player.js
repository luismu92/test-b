import commonReducer, { PREFIX } from 'redux/modules/player';

const PLAYER_VISIBLITY = `${PREFIX}PLAYER_VISIBLITY`;

// Export reducer specific to desktop using the common reducer to take
// care of shared actions between desktop and mobile

export default function reducer(state, action) {
    const partialState = commonReducer(state, action);

    switch (action.type) {
        case PLAYER_VISIBLITY:
            return {
                ...partialState,
                hidden: action.hidden
            };

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    editQueue,
    shuffleTracks,
    songLoaded,
    play,
    stop,
    ended,
    pause,
    seek,
    mute,
    unmute,
    setVolume,
    setRepeat,
    next,
    prev,
    timeUpdate,
    refreshCurrentPlaylistItem,
    loadMusicFromStorage
} from 'redux/modules/player';

export function showPlayer() {
    if (typeof document !== 'undefined') {
        document.body.classList.remove('player-hidden');
    }

    return {
        type: PLAYER_VISIBLITY,
        hidden: false
    };
}

export function hidePlayer() {
    if (typeof document !== 'undefined') {
        document.body.classList.add('player-hidden');
    }

    return {
        type: PLAYER_VISIBLITY,
        hidden: true
    };
}
