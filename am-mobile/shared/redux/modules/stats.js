export { default } from 'redux/modules/stats';

// Export all action dispatchers from the common reducer
export {
    TRACK_PLAY,
    reset,
    trackPlay,
    getStatsToken,
    setSection,
    setEnvironment,
    trackAction,
    setReferer
} from 'redux/modules/stats';
