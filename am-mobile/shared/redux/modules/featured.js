import api from 'api/index';

const PREFIX = 'am/featured/';

export const GET_FEATURED_SONG_INFO = `${PREFIX}GET_FEATURED_SONG_INFO`;

const defaultState = {
    loading: false,
    song: null,
    album: null,
    data: []
};

export default function featuredReducer(state = defaultState, action) {
    switch (action.type) {
        case GET_FEATURED_SONG_INFO: {
            return Object.assign({}, state, {
                song: action.resolved.results,
                loading: false
            });
        }

        default:
            return state;
    }
}

export function getSongInfoById(id, options = {}) {
    return {
        type: GET_FEATURED_SONG_INFO,
        promise: api.music.songById(id, options).then((results) => {
            results.results.isFeatured = true;
            return results;
        })
    };
}
