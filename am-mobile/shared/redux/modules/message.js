const SHOW_MESSAGE = 'am/message/SHOW_MESSAGE';
const HIDE_MESSAGE = 'am/message/HIDE_MESSAGE';

const initialState = {
    visible: false,
    message: ''
};

let messageTimer = null;

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SHOW_MESSAGE:
            return Object.assign({}, state, {
                visible: true,
                message: action.message
            });

        case HIDE_MESSAGE:
            return Object.assign({}, state, {
                visible: false
            });

        default:
            return state;
    }
}

export function showMessage(message) {
    return (dispatch) => {
        clearTimeout(messageTimer);
        messageTimer = setTimeout(() => dispatch(hideMessage()), 3000);

        return dispatch({
            type: SHOW_MESSAGE,
            message
        });
    };
}

export function hideMessage() {
    return {
        type: HIDE_MESSAGE
    };
}
