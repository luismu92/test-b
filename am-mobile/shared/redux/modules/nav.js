export const MOVE_ACTIVE_MARKER = 'am/nav/MOVE_ACTIVE_MARKER';
export const SET_ACTIVE_MARKER = 'am/nav/SET_ACTIVE_MARKER';

export const NAV_MARKER_FEED = 'feed';
export const NAV_MARKER_PLAYLIST = 'playlist';
export const NAV_MARKER_TRENDING = 'trending';
export const NAV_MARKER_SEARCH = 'search';
export const NAV_MARKER_PROFILE = 'profile';

const defaultState = {
    width: 0,
    transformX: 0,
    activeMarker: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case MOVE_ACTIVE_MARKER: {
            const { width, transformX } = action;

            return Object.assign({}, state, {
                width,
                transformX
            });
        }

        case SET_ACTIVE_MARKER:
            return Object.assign({}, state, {
                activeMarker: action.marker
            });

        default:
            return state;
    }
}

export function moveActiveMarker({ width, transformX }) {
    return {
        type: MOVE_ACTIVE_MARKER,
        width,
        transformX
    };
}

export function setActiveMarker(marker = null) {
    return {
        type: SET_ACTIVE_MARKER,
        marker
    };
}
