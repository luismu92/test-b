export { default } from 'redux/modules/admin';

// Export all action dispatchers from the common reducer
export {
    SUSPEND_MUSIC,
    UNSUSPEND_MUSIC,
    BUMP_MUSIC,
    LOGIN_AS_USER,
    LOGOUT_OF_USER,
    VERIFY_ARTIST,
    setMasqueradeValue,
    logoutOfUser,
    loginAsUser,
    suspendMusic,
    unsuspendMusic,
    takedownMusic,
    restoreMusic,
    bumpMusic,
    censorArtwork,
    trendMusic,
    excludeStats,
    clearStats,
    verifyArtist,
    verifyArtistEmail,
    getMusicSource,
    getMusicRecognition,
    refreshMusicRecognition,
    repairMusicUrl
} from 'redux/modules/admin';
