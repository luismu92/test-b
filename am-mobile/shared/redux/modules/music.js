import api from 'api/index';
import { suffix, strToNumber } from 'utils/index';

import {
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_PLAYLIST,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    PLAYLIST_TYPE_BROWSE,
    GENRE_TYPE_ALL,
    CHART_TYPE_TOTAL,
    CHART_TYPE_DAILY,
    CHART_TYPE_WEEKLY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY
} from 'constants/index';

import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from './user';
import { DELETE_PLAYLIST } from './playlist';
import { TRACK_PLAY } from './stats';
import { BUMP_MUSIC } from './admin';

const PREFIX = 'am/music/';

const FETCH_MUSIC = `${PREFIX}FETCH_MUSIC`;
const SET_CONTEXT = `${PREFIX}SET_CONTEXT`;
const SET_GENRE = `${PREFIX}SET_GENRE`;
const SET_TIME_PERIOD = `${PREFIX}SET_TIME_PERIOD`;
const SET_PLAYLIST_CONTEXT = `${PREFIX}SET_PLAYLIST_CONTEXT`;
const GET_USER_PLAYLISTS = `${PREFIX}GET_USER_PLAYLISTS`;
const GET_ARTIST_FAVORITES = `${PREFIX}GET_ARTIST_FAVORITES`;
const GET_ALBUM_INFO = `${PREFIX}GET_ALBUM_INFO`;
const GET_SONG_INFO = `${PREFIX}GET_SONG_INFO`;
const SET_SONG_PAGE_SONG = `${PREFIX}SET_SONG_PAGE_SONG`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const PREV_PAGE = `${PREFIX}PREV_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const RESET_SONG = `${PREFIX}RESET_SONG`;

const defaultState = {
    loading: false,
    activePlaylistContext: PLAYLIST_TYPE_BROWSE,
    activeContext: COLLECTION_TYPE_TRENDING,
    activeGenre: GENRE_TYPE_ALL,
    activeTimePeriod: CHART_TYPE_WEEKLY,
    currentDataType: null,
    errors: [],
    song: null,
    album: null,
    page: 1,
    onLastPage: false,
    showNumbers: false,
    data: []
};

// eslint-disable-next-line complexity
export default function musicReducer(state = defaultState, action) {
    // eslint-disable-line complexity
    switch (action.type) {
        case suffix(FETCH_MUSIC, 'REQUEST'):
        case suffix(GET_USER_PLAYLISTS, 'REQUEST'):
        case suffix(GET_ARTIST_FAVORITES, 'REQUEST'):
            return Object.assign({}, state, {
                loading: true
            });

        case suffix(FETCH_MUSIC, 'FAILURE'):
        case suffix(GET_USER_PLAYLISTS, 'FAILURE'):
        case suffix(GET_ARTIST_FAVORITES, 'FAILURE'):
            return Object.assign({}, state, {
                loading: false
            });

        case suffix(GET_SONG_INFO, 'FAILURE'):
        case suffix(GET_ALBUM_INFO, 'FAILURE'):
            return Object.assign({}, state, {
                errors: [action.error],
                loading: false
            });

        case FETCH_MUSIC:
        case GET_USER_PLAYLISTS:
        case GET_ARTIST_FAVORITES: {
            let newData = action.resolved.results;
            let onLastPage = state.onLastPage;

            if (state.currentDataType === action.type && state.page !== 1) {
                newData = state.data.concat(newData);
            }

            if (state.page !== 1 && !action.resolved.results.length) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                data: newData,
                currentDataType: action.type,
                loading: false,
                showNumbers: action.showNumbers,
                onLastPage
            });
        }

        case CLEAR_LIST:
            return Object.assign({}, state, {
                data: []
            });

        case SET_CONTEXT: {
            let page = state.page;
            let onLastPage = state.onLastPage;

            if (state.activeContext !== action.context) {
                page = 1;
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeContext: action.context,
                page,
                onLastPage
            });
        }

        case SET_PLAYLIST_CONTEXT:
            return Object.assign({}, state, {
                activePlaylistContext: action.context
            });

        case SET_GENRE: {
            let page = state.page;
            let onLastPage = state.onLastPage;

            if (state.activeGenre !== action.genre) {
                page = 1;
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeGenre: action.genre,
                page,
                onLastPage
            });
        }

        case SET_TIME_PERIOD: {
            let page = state.page;
            let onLastPage = state.onLastPage;

            if (state.activeTimePeriod !== action.timePeriod) {
                page = 1;
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeTimePeriod: action.timePeriod,
                page,
                onLastPage
            });
        }

        case GET_ALBUM_INFO: {
            const album = action.resolved.results;

            if (typeof album.tracks === 'undefined') {
                album.tracks = [];
            }

            return Object.assign({}, state, {
                album,
                loading: false
            });
        }

        case GET_SONG_INFO:
            return Object.assign({}, state, {
                song: action.resolved.results,
                loading: false
            });

        case DELETE_PLAYLIST:
            if (state.activeContext !== COLLECTION_TYPE_PLAYLIST) {
                return state;
            }

            return Object.assign({}, state, {
                data: state.data.filter((obj) => obj.id !== action.id)
            });

        case SET_SONG_PAGE_SONG:
            return Object.assign({}, state, {
                song: action.song
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case PREV_PAGE:
            return Object.assign({}, state, {
                page: Math.max(1, state.page - 1)
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case suffix(UNFAVORITE_ITEM, 'FAILURE'):
        case suffix(FAVORITE_ITEM, 'REQUEST'): {
            let song = state.song;
            let album = state.album;

            if (song) {
                song = { ...song, stats: { ...song.stats } };
                song.stats['favorites-raw'] += 1;
            }

            if (album) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['favorites-raw'] += 1;
            }

            return Object.assign({}, state, {
                data: state.data.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === strToNumber(action.id)) {
                        newObj.stats['favorites-raw'] += 1;
                    }

                    return newObj;
                }),
                song,
                album
            });
        }

        case suffix(FAVORITE_ITEM, 'FAILURE'):
        case suffix(UNFAVORITE_ITEM, 'REQUEST'): {
            const song = state.song;
            const album = state.album;

            if (song) {
                song.stats['favorites-raw'] -= 1;
            }

            if (album) {
                album.stats['favorites-raw'] -= 1;
            }

            return Object.assign({}, state, {
                data: state.data.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === strToNumber(action.id)) {
                        newObj.stats['favorites-raw'] -= 1;
                    }

                    return newObj;
                }),
                song,
                album
            });
        }

        case suffix(UNREPOST_ITEM, 'FAILURE'):
        case suffix(REPOST_ITEM, 'REQUEST'): {
            let song = state.song;
            let album = state.album;

            if (song) {
                song = { ...song, stats: { ...song.stats } };
                song.stats['reposts-raw'] += 1;
            }

            if (album) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['reposts-raw'] += 1;
            }

            return Object.assign({}, state, {
                data: state.data.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === strToNumber(action.id)) {
                        newObj.stats['reposts-raw'] += 1;
                    }

                    return newObj;
                }),
                song,
                album
            });
        }

        case suffix(REPOST_ITEM, 'FAILURE'):
        case suffix(UNREPOST_ITEM, 'REQUEST'): {
            let song = state.song;
            let album = state.album;

            if (song) {
                song = { ...song, stats: { ...song.stats } };
                song.stats['reposts-raw'] -= 1;
            }

            if (album) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['reposts-raw'] -= 1;
            }

            return Object.assign({}, state, {
                data: state.data.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === strToNumber(action.id)) {
                        newObj.stats['reposts-raw'] -= 1;
                    }

                    return newObj;
                }),
                song,
                album
            });
        }

        case TRACK_PLAY: {
            let album = state.album;

            if (
                album &&
                action.options.album_id &&
                album.id === action.options.album_id
            ) {
                // Only update if this is the first track play call, ie not the 30 second track call
                if (!action.time) {
                    album = {
                        ...state.album,
                        stats: {
                            ...state.album.stats,
                            'plays-raw': state.album.stats['plays-raw'] + 1
                        }
                    };
                }
            }

            return Object.assign({}, state, {
                album,
                data: state.data.map((obj) => {
                    const newObj = {
                        ...obj
                    };
                    const itemId = action.id;
                    const albumId = action.options.album_id;

                    if (
                        (obj.type === 'album' &&
                            typeof albumId === 'number' &&
                            obj.id === albumId) ||
                        (obj.type !== 'album' && obj.id === itemId)
                    ) {
                        // Only update if this is the first track play call, ie not the 30 second track call
                        if (!action.options.time) {
                            newObj.stats['plays-raw'] += 1;
                        }
                    }

                    return newObj;
                })
            });
        }

        case BUMP_MUSIC: {
            return Object.assign({}, state, {
                data: [action.item].concat(
                    state.data.filter((item) => item.id !== action.item.id)
                )
            });
        }

        case suffix(BUMP_MUSIC, 'FAILURE'): {
            return Object.assign({}, state, {
                data: [action.item].concat(
                    state.data.filter((item) => item.id !== action.item.id)
                )
            });
        }

        case RESET_SONG:
            return {
                ...state,
                song: defaultState.song
            };

        default:
            return state;
    }
}

export function fetchSongList(options = {}) {
    return (dispatch, getState) => {
        const state = getState().music;

        const {
            context = state.activeContext,
            genre = state.activeGenre,
            timePeriod: activeTimePeriod = state.activeTimePeriod,
            page = state.page
        } = options;
        const limit = 10;

        let promise;
        let showNumbers = false;
        let playerFetcher;

        switch (context) {
            case COLLECTION_TYPE_TRENDING:
                promise = api.music.trending(genre, page);
                playerFetcher = (p) =>
                    api.music.trending(genre, p).then((data) => {
                        return data.results;
                    });
                break;

            case COLLECTION_TYPE_RECENTLY_ADDED:
                promise = api.music.recent(genre, page);
                playerFetcher = (p) =>
                    api.music.recent(genre, p).then((data) => {
                        return data.results;
                    });
                break;

            case COLLECTION_TYPE_SONG:
            case COLLECTION_TYPE_ALBUM:
                showNumbers = true;
                promise = api.chart.get(
                    context,
                    activeTimePeriod,
                    genre,
                    page,
                    limit
                );
                playerFetcher = (p) =>
                    api.chart
                        .get(context, activeTimePeriod, genre, p, limit)
                        .then((data) => {
                            return data.results;
                        });
                break;

            default:
                throw new Error(`Context (${context}) not accounted for.`);
        }

        return dispatch({
            type: FETCH_MUSIC,
            showNumbers,
            promise,
            playerFetcher,
            page
        });
    };
}

export function setPlaylistContext(context) {
    return {
        type: SET_PLAYLIST_CONTEXT,
        context
    };
}
export function setContext(context) {
    return {
        type: SET_CONTEXT,
        context
    };
}

export function setGenre(genre = '') {
    return {
        type: SET_GENRE,
        genre
    };
}

export function setTimePeriod(timePeriod = CHART_TYPE_WEEKLY) {
    // Map what comes from the url to the actual API param
    const map = {
        day: CHART_TYPE_DAILY,
        week: CHART_TYPE_WEEKLY,
        month: CHART_TYPE_MONTHLY,
        year: CHART_TYPE_YEARLY,
        '': CHART_TYPE_TOTAL
    };

    return {
        type: SET_TIME_PERIOD,
        timePeriod: map[timePeriod] || timePeriod
    };
}

// Moving these here instead of in artists/user reducers since playlist
// page needs to display user shit and general browse shit. Not the cleanest
// but will be the path of least resistance at the moment.
export function getUserPlaylists(slug) {
    return (dispatch, getState) => {
        const currentUser = getState().currentUser;
        let promise;

        if ((currentUser.isLoggedIn && currentUser.profile.url_slug) || slug) {
            promise = api.artist.playlists(
                slug || currentUser.profile.url_slug
            );
        } else {
            promise = Promise.reject('No user or slug provided');
        }

        return dispatch({
            type: GET_USER_PLAYLISTS,
            promise
        });
    };
}

export function getArtistFavorites(slug, options = {}) {
    return (dispatch, getState) => {
        const state = getState().music;
        const { limit = 20, page = state.page, show } = options;

        return dispatch({
            type: GET_ARTIST_FAVORITES,
            page,
            limit,
            promise: api.artist.favorites(slug, page, limit, show)
        });
    };
}

export function getAlbumInfo(artistSlug, albumSlug, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_ALBUM_INFO,
            force404OnError: /^\/album\//,
            promise: api.music.album(artistSlug, albumSlug, {
                token,
                secret,
                ...options
            })
        });
    };
}

export function getAlbumInfoById(id, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_ALBUM_INFO,
            force404OnError: /^\/album\//,
            promise: api.music.albumById(id, { token, secret, ...options })
        });
    };
}

export function getSongInfo(artistSlug, songSlug, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_SONG_INFO,
            force404OnError: /^\/song\//,
            promise: api.music.song(artistSlug, songSlug, {
                token,
                secret,
                ...options
            })
        });
    };
}

export function getSongInfoById(id, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_SONG_INFO,
            force404OnError: /^\/song\//,
            promise: api.music.songById(id, { token, secret, ...options })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: parseInt(page, 10)
    };
}

export function setSongForSongPage(song) {
    return {
        type: SET_SONG_PAGE_SONG,
        song
    };
}

export function prevPage() {
    return {
        type: PREV_PAGE
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}

export function resetSong() {
    return {
        type: RESET_SONG
    };
}
