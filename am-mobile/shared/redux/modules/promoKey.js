export { default } from 'redux/modules/promoKey';

// Export all action dispatchers from the common reducer
export { getPromoKeys, reset } from 'redux/modules/promoKey';
