const PREFIX = 'am/global/';

export const HIDE_HEADER = `${PREFIX}HIDE_HEADER`;
export const SHOW_HEADER = `${PREFIX}SHOW_HEADER`;
export const SET_GLOBAL_BACKGROUND = `${PREFIX}SET_GLOBAL_BACKGROUND`;
export const SET_BANNER_DEEP_LINK = `${PREFIX}SET_BANNER_DEEP_LINK`;

const defaultState = {
    headerHidden: false,
    background: null,
    deepLink: null,
    deepLinkData: {}
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case HIDE_HEADER:
            return Object.assign({}, state, {
                headerHidden: true
            });

        case SHOW_HEADER:
            return Object.assign({}, state, {
                headerHidden: false
            });

        case SET_GLOBAL_BACKGROUND:
            return Object.assign({}, state, {
                background: action.image
            });

        case SET_BANNER_DEEP_LINK:
            return {
                ...state,
                deepLink: action.link ? action.link : defaultState.deepLink,
                deepLinkData: action.extraData
                    ? action.extraData
                    : defaultState.deepLinkData
            };

        default:
            return state;
    }
}

export function hideHeader() {
    return {
        type: HIDE_HEADER
    };
}

export function showHeader() {
    return {
        type: SHOW_HEADER
    };
}

export function setGlobalBackground(image) {
    return {
        type: SET_GLOBAL_BACKGROUND,
        image
    };
}

export function setBannerDeepLink(link, extraData) {
    return {
        type: SET_BANNER_DEEP_LINK,
        link,
        extraData
    };
}
