/* global test, expect, describe */
import { combineReducers } from 'redux';
import { configureStore } from 'redux/configureStore';

import playerReducer from '../player';
import * as actions from '../player';

function getDefaultStore(initialState) {
    const state = {
        stats: {},
        currentUser: {},
        ad: {},
        router: {
            location: {
                key: 'adsfafsdf'
            }
        },
        player: {
            ...playerReducer(undefined, { type: 'gimmeDefaultState' }),
            ...playerReducer(initialState, { type: 'gimmeDefaultState' })
        }
    };

    return configureStore(
        state,
        combineReducers({
            player: playerReducer,
            // The player reducer requires reading properties on
            // these other reducers for some actions so we just
            // return a regular object for those.
            router: () => state.router,
            ad: () => state.ad,
            currentUser: () => state.currentUser,
            stats: () => state.stats
        })
    );
}

describe('#showPlayer', () => {
    test.only('it should set hidden to false', async function() {
        const store = getDefaultStore();

        store.dispatch(actions.showPlayer());

        const nextState = store.getState().player;

        expect(nextState.hidden).toBe(false);
    });
});

describe('#hidePlayer', () => {
    test.only('it should set hidden to true', async function() {
        const store = getDefaultStore();

        store.dispatch(actions.hidePlayer());

        const nextState = store.getState().player;

        expect(nextState.hidden).toBe(true);
    });
});
