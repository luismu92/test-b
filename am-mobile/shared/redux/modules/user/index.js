import commonReducer from 'redux/modules/user/index';

import { GET_USER_FEED } from './feed';

export default function reducer(state, action) {
    const partialState = commonReducer(state, action);

    switch (action.type) {
        case GET_USER_FEED:
            return {
                ...partialState,
                profile: {
                    ...partialState.profile,
                    new_feed_items: 0
                }
            };

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    LOG_IN,
    SAVE_USER_DETAILS,
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM,
    FOLLOW_ARTIST,
    UNFOLLOW_ARTIST,
    getTwitterRequestToken,
    identityCheck,
    logIn,
    logInFacebook,
    logInGoogle,
    logInTwitter,
    logInApple,
    claimArtist,
    register,
    verifyHash,
    verifyForgotPasswordToken,
    recoverAccount,
    registerWithCaptcha,
    forgotpw,
    clearErrors,
    saveUserDetails,
    updatePassword,
    updateSlug,
    updateEmail,
    deleteAccount,
    logOut,
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction,
    dequeueAction,
    resendEmail,
    follow,
    unfollow,
    emailUnsubscribe
} from 'redux/modules/user/index';
