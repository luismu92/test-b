import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

import { ADD_SONG_TO_PLAYLIST, DELETE_SONG_FROM_PLAYLIST } from '../playlist';

const PREFIX = 'am/user/playlists/';
const GET_PLAYLISTS = `${PREFIX}GET_PLAYLISTS`;
const GET_PLAYLISTS_WITH_SONG_ID = `${PREFIX}GET_PLAYLISTS_WITH_SONG_ID`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    errors: [],
    page: 1,
    onLastPage: false,
    loading: false,
    list: [],
    loadingSongIds: false,
    songIdToPlaylistIds: {}
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PLAYLISTS):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case failSuffix(GET_PLAYLISTS):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case GET_PLAYLISTS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (state.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                state.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                list: newData,
                page: action.page,
                onLastPage,
                loading: false
            });
        }

        case requestSuffix(GET_PLAYLISTS_WITH_SONG_ID):
            return {
                ...state,
                loadingSongIds: true
            };

        case failSuffix(GET_PLAYLISTS_WITH_SONG_ID):
            return {
                ...state,
                loadingSongIds: false
            };

        case GET_PLAYLISTS_WITH_SONG_ID:
            return {
                ...state,
                loadingSongIds: false,
                songIdToPlaylistIds: {
                    ...state.songIdToPlaylistIds,
                    [action.musicId]: action.resolved.results.map((p) => p.id)
                }
            };

        case requestSuffix(ADD_SONG_TO_PLAYLIST): {
            const newArray = Array.from(
                state.songIdToPlaylistIds[action.song.id] || []
            );

            newArray.push(action.playlistId);

            return {
                ...state,
                songIdToPlaylistIds: {
                    ...state.songIdToPlaylistIds,
                    [action.song.id]: newArray
                }
            };
        }

        case failSuffix(ADD_SONG_TO_PLAYLIST): {
            const ids = Array.from(
                state.songIdToPlaylistIds[action.song.id] || []
            );
            const newArray = ids.filter((id) => id !== action.playlistId);

            return {
                ...state,
                songIdToPlaylistIds: {
                    ...state.songIdToPlaylistIds,
                    [action.song.id]: newArray
                }
            };
        }

        case ADD_SONG_TO_PLAYLIST:
            return {
                ...state,
                list: state.list.map((playlist) => {
                    if (playlist.id === action.playlistId) {
                        return {
                            ...playlist,
                            track_count: playlist.track_count + 1
                        };
                    }

                    return playlist;
                })
            };

        case requestSuffix(DELETE_SONG_FROM_PLAYLIST): {
            const ids = Array.from(
                state.songIdToPlaylistIds[action.songId] || []
            );
            const newArray = ids.filter((id) => id !== action.playlistId);

            return {
                ...state,
                songIdToPlaylistIds: {
                    ...state.songIdToPlaylistIds,
                    [action.songId]: newArray
                }
            };
        }

        case failSuffix(DELETE_SONG_FROM_PLAYLIST): {
            const newArray = Array.from(
                state.songIdToPlaylistIds[action.songId] || []
            );

            newArray.push(action.playlistId);

            return {
                ...state,
                songIdToPlaylistIds: {
                    ...state.songIdToPlaylistIds,
                    [action.songId]: newArray
                }
            };
        }

        case DELETE_SONG_FROM_PLAYLIST:
            return {
                ...state,
                list: state.list.map((playlist) => {
                    if (playlist.id === action.playlistId) {
                        return {
                            ...playlist,
                            track_count: playlist.track_count - 1
                        };
                    }

                    return playlist;
                })
            };

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getPlaylists(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().currentUserPlaylists;
        const { limit = 20, page = state.page } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_PLAYLISTS,
            page: finalPage,
            limit: finalLimit,
            promise: api.user.playlists(token, secret, {
                page: finalPage,
                limit: finalLimit
            })
        });
    };
}

export function getPlaylistsWithSongId(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_PLAYLISTS_WITH_SONG_ID,
            musicId,
            promise: api.user.playlists(token, secret, {
                limit: 100,
                musicId
            })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
