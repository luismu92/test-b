export { default } from 'redux/modules/user/feed';

// Export all action dispatchers from the common reducer
export {
    GET_USER_FEED,
    setUploadsOnlyState,
    getUserFeed,
    setPage,
    nextPage,
    reset
} from 'redux/modules/user/feed';
