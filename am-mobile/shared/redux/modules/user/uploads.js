import api from 'api/index';
import { suffix } from 'utils/index';

const PREFIX = 'am/artist/uploads/';

const GET_USER_UPLOADS = `${PREFIX}GET_USER_UPLOADS`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    list: [],
    page: 1,
    show: 'all',
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(GET_USER_UPLOADS, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(GET_USER_UPLOADS, 'FAILURE'):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case GET_USER_UPLOADS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (action.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                action.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                list: newData,
                page: action.page,
                onLastPage,
                loading: false
            });
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getUserUploads(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().currentUserUploads;
        const { limit, page = state.page } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_USER_UPLOADS,
            page: finalPage,
            limit: finalLimit,
            promise: api.user.uploads({
                token,
                secret,
                page: finalPage,
                limit: finalLimit
            })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
