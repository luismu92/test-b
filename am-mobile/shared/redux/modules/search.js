import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { /* getSongInfoById, */ GET_FEATURED_SONG_INFO } from './featured';

const PREFIX = 'am/search/';

export const QUERY_TYPE_MUSIC = 'music';
export const QUERY_TYPE_SONGS = 'songs';
export const QUERY_TYPE_ALBUMS = 'albums';
export const QUERY_TYPE_ARTISTS = 'artists';

export const CONTEXT_TYPE_RELEVANCE = 'relevance';
export const CONTEXT_TYPE_RECENT = 'recent';
export const CONTEXT_TYPE_POPULAR = 'popular';

const SEARCH = `${PREFIX}SEARCH`;
const SEARCH_SUGGEST = `${PREFIX}SEARCH_SUGGEST`;
const SEARCH_INPUT = `${PREFIX}SEARCH_INPUT`;
const SET_QUERY_CONTEXT = `${PREFIX}SET_QUERY_CONTEXT`;
const SET_QUERY_TYPE = `${PREFIX}SET_QUERY_TYPE`;
const SET_SEARCHING_STATE = `${PREFIX}SET_SEARCHING_STATE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;

const defaultState = {
    activeType: QUERY_TYPE_MUSIC,
    activeContext: CONTEXT_TYPE_POPULAR,
    suggestions: [],
    results: [],
    query: '',
    onLastPage: false,
    page: 1,
    loading: false,
    searchActive: false,
    verifiedArtist: null,
    verifiedTastemaker: null,
    verifiedPlaylist: null,
    tastemakerPlaylist: null,
    related: null
};

const cache = {
    suggestions: {},
    results: {}
};

const CACHE_LENGTH = 1000 * 60; // cache for a minute

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case GET_FEATURED_SONG_INFO: {
            const newData = Array.from(state.results);

            newData.splice(2, 0, action.resolved.results);
            return {
                ...state,
                results: newData
            };
        }
        case requestSuffix(SEARCH):
            return {
                ...state,
                loading: true
            };

        case failSuffix(SEARCH):
            return {
                ...state,
                loading: false
            };

        case SEARCH: {
            let newData = action.resolved.results;
            let onLastPage = false;
            let related = false;

            if (
                state.activeType === action.activeType &&
                state.activeContext === action.activeContext &&
                state.page !== 1
            ) {
                newData = state.results.concat(newData);
            }

            if (state.page !== 1 && !action.resolved.results.length) {
                onLastPage = true;
            }

            if (action.resolved.related) {
                related = true;
            }

            cache.results[action.cacheKey] = {
                ...action.resolved,
                lastUpdated: Date.now()
            };

            let verifiedArtist =
                action.resolved.verified_artist || defaultState.verifiedArtist;
            let verifiedTastemaker =
                action.resolved.tastemaker_artist ||
                defaultState.verifiedTastemaker;
            const verifiedPlaylist =
                action.resolved.verified_playlist ||
                defaultState.verifiedPlaylist;
            const tastemakerPlaylist =
                action.resolved.tastemaker_playlist ||
                defaultState.tastemakerPlaylist;

            // Subsequent pages don't have the verified key
            if (action.page > 1) {
                verifiedArtist = state.verifiedArtist;
                verifiedTastemaker = state.verifiedTastemaker;
            }

            return {
                ...state,
                results: newData || [],
                loading: false,
                searchActive: false,
                query: action.query,
                onLastPage,
                verifiedArtist,
                verifiedTastemaker,
                verifiedPlaylist,
                tastemakerPlaylist,
                related
            };
        }

        case SET_PAGE:
            return {
                ...state,
                page: action.page
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case SEARCH_INPUT:
            return {
                ...state,
                query: action.query
            };

        case SET_SEARCHING_STATE:
            return {
                ...state,
                searchActive: action.bool
            };

        case SEARCH_SUGGEST: {
            const suggestions = action.resolved.results;

            cache.suggestions[action.cacheKey] = suggestions;

            return {
                ...state,
                suggestions: suggestions || []
            };
        }

        case SET_QUERY_TYPE:
            return {
                ...state,
                activeType: action.queryType
            };

        case SET_QUERY_CONTEXT:
            return {
                ...state,
                activeContext: action.context
            };

        case CLEAR_LIST:
            return {
                ...state,
                results: [],
                verifiedArtist: null,
                verifiedTastemaker: null,
                verifiedPlaylist: null,
                tastemakerPlaylist: null,
                related: null
            };

        default:
            return state;
    }
}

function argRequired() {
    throw new Error('Arg required');
}

function getCacheKey({
    query = argRequired(),
    activeContext = argRequired(),
    activeType = argRequired(),
    page,
    limit
}) {
    return `${query}:${activeContext}:${activeType}:${page}:${limit}`;
}

export function getSearch(query = '', options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().search;
        const {
            limit = 20,
            page = state.page,
            type = state.activeType,
            context = state.activeContext
        } = options;
        const q = query || state.query;
        const key = getCacheKey({
            query: q,
            activeContext: context,
            activeType: type,
            page,
            limit
        });
        let promise;

        if (
            cache.results[key] &&
            Date.now() - cache.results[key].lastUpdated < CACHE_LENGTH
        ) {
            promise = Promise.resolve(cache.results[key]);
        } else {
            promise = api.search.get(q, {
                type,
                context,
                page,
                limit,
                token,
                secret
            });
        } // Disable for now

        /* promise = promise.then((results) => {
            if (page === 1) {
                dispatch(getSongInfoById(3277672));
            }
            return results;
        }); */ return dispatch(
            {
                type: SEARCH,
                cacheKey: key,
                query: q,
                activeContext: context,
                activeType: type,
                page,
                limit,
                promise
            }
        );
    };
}

export function searchInput(query = '') {
    return {
        type: SEARCH_INPUT,
        query
    };
}

export function getSuggestions(query = '') {
    const action = {
        type: SEARCH_SUGGEST,
        query
    };

    return (dispatch, getState) => {
        const state = getState().search;
        const { activeContext, activeType } = state;
        const key = getCacheKey({ query, activeContext, activeType });

        action.cacheKey = key;
        if (cache.suggestions[key]) {
            action.promise = Promise.resolve({
                results: cache.suggestions[key]
            });
            return dispatch(action);
        }

        action.promise = api.search.suggest(query);
        return dispatch(action);
    };
}

export function setQueryType(queryType = defaultState.activeType) {
    return {
        type: SET_QUERY_TYPE,
        queryType
    };
}

export function setQueryContext(context = defaultState.activeContext) {
    return {
        type: SET_QUERY_CONTEXT,
        context
    };
}
export function setSearchingState(bool) {
    return {
        type: SET_SEARCHING_STATE,
        bool
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
