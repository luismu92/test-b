export const MODAL_TYPE_ALBUM_INFO = 'album-info';
export const MODAL_TYPE_SONG_INFO = 'song-info';
export const MODAL_TYPE_ARTIST_INFO = 'artist-info';
export const MODAL_TYPE_PLAYLIST_INFO = 'playlist-info';
export const MODAL_TYPE_ADD_TO_PLAYLIST = 'add-to-playlist';
export const MODAL_TYPE_APP_DOWNLOAD = 'app-download';
export const MODAL_TYPE_CONFIRM = 'confirm';
export const MODAL_TYPE_TRENDING = 'trending';
export const MODAL_TYPE_COPY_URL = 'copy-url';
export const MODAL_TYPE_VERIFY_EMAIL = 'verify-email';
export const MODAL_TYPE_GHOST_CLAIM = 'ghost-claim';
export const MODAL_TYPE_VERIFY_PASSWORD_TOKEN = 'verify-password-token';
export const MODAL_TYPE_INPUT_FORM = 'input-form';

export { default } from 'redux/modules/modal';

export { showModal, hideModal } from 'redux/modules/modal';
