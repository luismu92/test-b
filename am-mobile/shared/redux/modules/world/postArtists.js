export { default } from 'redux/modules/world/postArtists';

// Export all action dispatchers from the common reducer
export { getArtistBySlug } from 'redux/modules/world/postArtists';
