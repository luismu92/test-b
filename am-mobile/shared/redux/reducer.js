import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { history } from 'redux/configureStore';

import artist from './modules/artist/index';
import artistFavorites from './modules/artist/favorites';
import artistFollowers from './modules/artist/followers';
import artistFollowing from './modules/artist/following';
import artistFeed from './modules/artist/feed';
import artistPlaylists from './modules/artist/playlists';
import artistUploads from './modules/artist/uploads';
import artistBrowse from './modules/artist/browse';
import artistPinned from './modules/artist/pinned';

import admin from './modules/admin';
import ad from './modules/ad';

import currentUser from './modules/user/index';
import currentUserFollowing from './modules/user/following';
import currentUserFeed from './modules/user/feed';
import currentUserUploads from './modules/user/uploads';
import currentUserPlaylists from './modules/user/playlists';
import currentUserNotifications from './modules/user/notifications';
import currentUserPremium from './modules/user/premium';
import currentUserNotificationSettings from './modules/user/notificationSettings';

import browsePlaylist from './modules/music/browsePlaylist';

import email from './modules/email';
import modal from './modules/modal';
import playlist from './modules/playlist';
import music from './modules/music';
import player from './modules/player';
import search from './modules/search';
import searchUser from './modules/searchUser';
import message from './modules/message';
import globalReducer from './modules/global';
import nav from './modules/nav';
import drawer from './modules/drawer';
import stats from './modules/stats';
import promoKey from './modules/promoKey';
import featured from './modules/featured';
import comment from './modules/comment';

import worldFeatured from './modules/world/featured';
import worldPage from './modules/world/page';
import worldPost from './modules/world/post';
import worldSettings from './modules/world/settings';
import worldPostArtists from './modules/world/postArtists';

export default combineReducers({
    artist,
    artistFavorites,
    artistFollowers,
    artistFollowing,
    artistFeed,
    artistPlaylists,
    artistUploads,
    artistBrowse,
    artistPinned,

    admin,
    ad,

    currentUser,
    currentUserFollowing,
    currentUserUploads,
    currentUserFeed,
    currentUserNotifications,
    currentUserNotificationSettings,
    currentUserPlaylists,
    currentUserPremium,

    browsePlaylist,

    email,
    modal,
    playlist,
    music,
    player,
    search,
    featured,
    searchUser,
    message,
    nav,
    drawer,
    global: globalReducer,
    router: connectRouter(history),
    stats,
    promoKey,
    comment,

    worldFeatured,
    worldPage,
    worldPost,
    worldSettings,
    worldPostArtists
});
