import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getBranchUrl } from 'utils/index';

import AmLogo from './icons/am-logo-color';

export default class Header extends Component {
    static propTypes = {
        hidden: PropTypes.bool,
        deepLink: PropTypes.string,
        deepLinkData: PropTypes.object
    };

    static defaultProps = {
        deepLink: 'audiomack://'
    };

    render() {
        const { hidden, deepLink, deepLinkData } = this.props;

        if (hidden) {
            return null;
        }

        const branchUrl = getBranchUrl(process.env.BRANCH_URL, {
            deeplinkPath: deepLink,
            channel: 'Mobile Web',
            campaign: 'Mobile Header Link',
            ...deepLinkData
        });

        return (
            <header className="main-header u-d-flex u-d-flex--align-center u-d-flex--justify-between">
                <div className="branch-journeys-top u-pos-absolute" />
                <Link
                    className="main-header__logo"
                    to="/"
                    aria-label="Audiomack Home"
                >
                    <AmLogo />
                </Link>
                <ul className="app-badges main-header__app-badges u-text-center u-spacing-right-15">
                    <li className="u-d-inline-block">
                        <a
                            href={branchUrl}
                            target="_blank"
                            rel="nofollow noopener"
                        >
                            <img
                                src="/static/images/shared/app-badges/app-store-badge-85x25.png"
                                srcSet="/static/images/shared/app-badges/app-store-badge-85x25@2x.png 2x"
                                alt="Audiomack on the App Store"
                            />
                        </a>
                    </li>
                    <li className="u-d-inline-block u-spacing-left-10">
                        <a
                            href={branchUrl}
                            target="_blank"
                            rel="nofollow noopener"
                        >
                            <img
                                src="/static/images/shared/app-badges/google-play-badge-85x25.png"
                                srcSet="/static/images/shared/app-badges/google-play-badge-85x25@2x.png 2x"
                                alt="Audiomack on Google Play"
                            />
                        </a>
                    </li>
                </ul>
            </header>
        );
    }
}
