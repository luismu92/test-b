import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import FollowIcon from './icons/follow-user';
import FollowingIcon from './icons/following-user';
import DotsLoader from './loaders/DotsLoader';

export default class FollowButton extends Component {
    static propTypes = {
        artist: PropTypes.object.isRequired,
        onClick: PropTypes.func.isRequired,
        loading: PropTypes.bool.isRequired,
        currentUser: PropTypes.object,
        className: PropTypes.string
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFollowClick = () => {
        this.props.onClick(true, this.props.artist);
    };

    handleUnfollowClick = () => {
        this.props.onClick(false, this.props.artist);
    };

    render() {
        const { currentUser, artist, loading, className } = this.props;

        if (currentUser.isLoggedIn && currentUser.profile.id === artist.id) {
            return null;
        }

        const following =
            currentUser.profile &&
            currentUser.profile.following &&
            currentUser.profile.following.indexOf(artist.id) !== -1;
        const loader = <DotsLoader size={2} className="follow-loading" />;
        const buttonClass = classnames('button button--follow follow-button', {
            'follow-button--following': following,
            'follow-button--loading': loading,
            [className]: className
        });
        let icon = <FollowIcon title="Tap to follow" />;

        if (following) {
            icon = <FollowingIcon title="Tap to unfollow" />;
        }

        const fn = following
            ? this.handleUnfollowClick
            : this.handleFollowClick;

        const inner = loading ? loader : icon;

        return (
            <button className={buttonClass} onClick={fn} disabled={loading}>
                {inner}
            </button>
        );
    }
}
