import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getNormalizedQueueTrack } from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';

import {
    getInfoWithSlugs,
    deleteSong,
    reorderTracks
} from '../redux/modules/playlist';
import requireAuth from '../hoc/requireAuth';
import PlaylistReorderList from '../list/PlaylistReorderList';

class PlaylistReorderContainer extends Component {
    static propTypes = {
        global: PropTypes.object,
        modal: PropTypes.object,
        currentUser: PropTypes.object,
        playlist: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handlePlaylistItemDelete = (e) => {
        e.stopPropagation();
        const { dispatch, playlist } = this.props;
        const button = e.currentTarget;
        const songId = button.getAttribute('data-music-id');

        dispatch(deleteSong(playlist.data.id, songId));
    };

    handleListItemSwitch = (fromIndex, toIndex) => {
        const { playlist, dispatch } = this.props;
        // ImmutableJS in the reducers would fix the need for this
        const tracks = JSON.parse(JSON.stringify(playlist.data.tracks));

        const fromTrack = tracks[fromIndex];

        tracks.splice(fromIndex, 1);
        tracks.splice(toIndex, 0, fromTrack);

        const changedOrder = tracks.some((track, i) => {
            return this.props.playlist.data.tracks[i].id !== track.id;
        });

        if (changedOrder) {
            dispatch(reorderTracks(playlist.data.id, tracks));
        }
    };

    handleCloseButton = () => {
        const { playlist, currentUser } = this.props;

        this.props.history.push(
            `/playlist/${currentUser.profile.url_slug}/${
                playlist.data.url_slug
            }`
        );
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const { playlist } = this.props;

        if (!playlist.data) {
            return null;
        }

        const listProps = {
            songs: {
                data: playlist.data.tracks.map((t, i) =>
                    getNormalizedQueueTrack(playlist.data, i)
                )
            },
            onPlaylistItemDelete: this.handlePlaylistItemDelete,
            onListItemSwitch: this.handleListItemSwitch,
            onCloseButton: this.handleCloseButton
        };

        return (
            <div>
                <PlaylistReorderList {...listProps} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        global: state.global,
        modal: state.modal,
        currentUser: state.currentUser,
        playlist: state.playlist,
        player: state.player
    };
}

export default requireAuth(
    connect(mapStateToProps)(
        connectDataFetchers(PlaylistReorderContainer, [
            (params, query, props) => {
                if (props.currentUser.isLoggedIn) {
                    return getInfoWithSlugs(
                        props.currentUser.profile.url_slug,
                        params.playlistSlug
                    );
                }

                return null;
            }
        ])
    )
);
