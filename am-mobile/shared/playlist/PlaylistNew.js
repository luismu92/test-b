import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { allGenresMap as genreMap } from 'constants/index';
import Avatar from '../Avatar';
import DotsLoader from '../loaders/DotsLoader';

// @todo make components out of save button, editable avatar and
// editable fields. They are being used all over and a lot of the stuff
// here is copied from PlaylistEditContainer

export default class PlaylistNew extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        defaultThumb: PropTypes.string,
        editFieldValues: PropTypes.object,
        onEditFieldInput: PropTypes.func,
        onSaveButtonClick: PropTypes.func
    };

    renderAvatar(image, cachedFields) {
        const klass = classnames('avatar-wrap edit-mode', {});

        if (cachedFields.image) {
            image = cachedFields.image;
        }

        return (
            <div className={klass}>
                <input
                    type="file"
                    name="image"
                    className="avatar-wrap__file-input"
                    accept="image/*"
                    onChange={this.props.onEditFieldInput}
                />
                <i className="glyphicons camera" />
                <Avatar
                    image={image}
                    size={130}
                    className="edit-mode"
                    rounded={false}
                />
            </div>
        );
    }

    renderSaveButton(loading, editFieldValues) {
        let text = 'Save';

        if (loading) {
            text = <DotsLoader active={loading} />;
        }

        return (
            <div className="save-container">
                <button
                    className="button button--lg"
                    disabled={
                        loading ||
                        (!editFieldValues.genre || !editFieldValues.title)
                    }
                    onClick={this.props.onSaveButtonClick}
                >
                    {text}
                </button>
            </div>
        );
    }

    render() {
        const { loading, defaultThumb, editFieldValues } = this.props;
        const options = Object.keys(genreMap).map((key, k) => {
            return (
                <option value={key} key={`${k}`}>
                    {genreMap[key]}
                </option>
            );
        });

        return (
            <div className="profile-container">
                <header>
                    {this.renderAvatar(defaultThumb, editFieldValues)}
                </header>
                <table className="profile-table">
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="title">Name</label>
                            </td>
                            <td>
                                <input
                                    maxLength={65}
                                    type="text"
                                    id="name"
                                    defaultValue={editFieldValues.name}
                                    placeholder="Name"
                                    onChange={this.props.onEditFieldInput}
                                    name="title"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="genre">Genre</label>
                            </td>
                            <td>
                                <div>
                                    {/* eslint-disable-next-line jsx-a11y/no-onchange */}
                                    <select
                                        name="genre"
                                        onChange={this.props.onEditFieldInput}
                                        value={editFieldValues.genre}
                                        id="genre"
                                    >
                                        {options}
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="permissions">Permissions</label>
                            </td>
                            <td>
                                <div className="privacy-radios">
                                    <label className="privacy-radios__label">
                                        <span>Public</span>
                                        <input
                                            type="radio"
                                            name="permissions"
                                            value="no"
                                            checked={
                                                editFieldValues.permissions ===
                                                    'no' ||
                                                !editFieldValues.permissions
                                            }
                                            onChange={
                                                this.props.onEditFieldInput
                                            }
                                        />
                                    </label>
                                    <label className="privacy-radios__label">
                                        <span>Private</span>
                                        <input
                                            type="radio"
                                            name="permissions"
                                            value="yes"
                                            checked={
                                                editFieldValues.permissions ===
                                                'yes'
                                            }
                                            onChange={
                                                this.props.onEditFieldInput
                                            }
                                        />
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                {this.renderSaveButton(loading, editFieldValues)}
            </div>
        );
    }
}
