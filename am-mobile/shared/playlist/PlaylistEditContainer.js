import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { previewFile } from 'utils/index';

import connectDataFetchers from 'lib/connectDataFetchers';
import { allGenresMap as genreMap } from '../../../am-shared/constants/index';
import {
    getInfoWithSlugs,
    savePlaylistDetails,
    deletePlaylist
} from '../redux/modules/playlist';
import { showMessage } from '../redux/modules/message';

import Avatar from '../Avatar';
import DotsLoader from '../loaders/DotsLoader';
import requireAuth from '../hoc/requireAuth';

const maxLengthRules = {
    name: 65,
    label: 65,
    hometown: 65,
    url: 80,
    bio: 1200
};

const fields = {
    title: {
        label: 'Name'
    },
    genre: {
        label: 'Genre'
    },
    private: {
        label: 'Permissions'
    }
};

//  can be username e.g. 'glenscott' or full url e.g. https://twitter.com/glenscott
//  must be full URL to Facebook profile e.g. https://www.facebook.com/glen.scott.716
//  can be username e.g. glenscott or full URL to Instagram profile e.g. https://www.instagram.com/glenscott/
// genre must be one of 'rap', 'electronic', 'dancehall', 'rock', 'pop', 'podcast', 'latin', 'jazz', 'country', 'world', 'classical', 'gospel', 'acapella' or 'other'
// image base-64 encoded JPEG or PNG image. Max file size 2Mb.

class PlaylistEditContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        playlist: PropTypes.object,
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editFieldValues: {}
        };
    }

    componentDidMount() {
        if (!this.props.currentUser.isLoggedIn) {
            this.props.history.replace('/');
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSaveButtonClick = () => {
        const { dispatch, playlist, currentUser, history } = this.props;
        const data = {
            ...this.state.editFieldValues
        };

        dispatch(savePlaylistDetails(playlist.data.id, data))
            .then(() => {
                return history.push(
                    `/playlist/${currentUser.profile.url_slug}/${
                        playlist.data.url_slug
                    }`
                );
            })
            .catch((err) => {
                console.log(err);

                dispatch(
                    showMessage(
                        'There was an error saving your playlist details.'
                    )
                );
            });
    };

    handlePlaylistDeleteClick = () => {
        const { dispatch, playlist } = this.props;

        if (confirm('Are you sure you want to delete this playlist?')) {
            dispatch(deletePlaylist(playlist.data.id));
            this.props.history.push('/playlists/mine');
        }
    };

    handleEditFieldInput = (e) => {
        const { name, value } = e.currentTarget;

        if (name === 'image') {
            previewFile(e)
                .then((result) => {
                    this.setState((prevState) => {
                        return {
                            ...prevState,
                            editFieldValues: {
                                ...prevState.editFieldValues,
                                [name]: result
                            }
                        };
                    });
                    return;
                })
                .catch((err) => console.log(err));
            return;
        }

        this.setState((prevState) => {
            return {
                ...prevState,
                editFieldValues: {
                    ...prevState.editFieldValues,
                    [name]: value
                }
            };
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    // eslint-disable-next-line max-params
    renderInputs(
        playlistData,
        field,
        maxLength,
        id,
        cachedFields,
        placeholder
    ) {
        return (
            <input
                maxLength={maxLength}
                type="text"
                id={id}
                defaultValue={cachedFields[field] || playlistData[field]}
                placeholder={placeholder}
                onChange={this.handleEditFieldInput}
                name={field}
            />
        );
    }

    renderRows(playlist, fieldObj, cachedFields) {
        const { errors, data } = playlist;

        return Object.keys(fieldObj).map((field, i) => {
            const placeholder = fieldObj[field].placeholder;
            const label = fieldObj[field].label;
            let errorMessages = (errors || {})[field];
            let text;
            const id = `input-${i}`;

            if (errorMessages) {
                errorMessages = Object.keys(errorMessages).map((key, k) => {
                    return (
                        <p className="error" key={k}>
                            {errorMessages[key]}
                        </p>
                    );
                });
            }

            text = (
                <div>
                    {this.renderInputs(
                        data,
                        field,
                        maxLengthRules[field],
                        id,
                        cachedFields,
                        placeholder
                    )}
                    {errorMessages}
                </div>
            );

            if (field === 'genre') {
                const options = Object.keys(genreMap).map((key, k) => {
                    return (
                        <option value={key} key={`${k}`}>
                            {genreMap[key]}
                        </option>
                    );
                });

                text = (
                    <div key={i}>
                        {/* eslint-disable-next-line jsx-a11y/no-onchange */}
                        <select
                            name={field}
                            onChange={this.handleEditFieldInput}
                            value={cachedFields.genre || data.genre}
                            id={id}
                        >
                            {options}
                        </select>
                        {errorMessages}
                    </div>
                );
            }

            if (field === 'private') {
                const checkedValue = cachedFields[field]
                    ? cachedFields[field]
                    : data.private;

                text = (
                    <div className="privacy-radios">
                        <label className="privacy-radios__label">
                            <span>Public</span>
                            <input
                                type="radio"
                                name={field}
                                value="no"
                                checked={checkedValue === 'no'}
                                onChange={this.handleEditFieldInput}
                            />
                        </label>
                        <label className="privacy-radios__label">
                            <span>Private</span>
                            <input
                                type="radio"
                                name={field}
                                value="yes"
                                checked={checkedValue === 'yes'}
                                onChange={this.handleEditFieldInput}
                            />
                        </label>
                        {errorMessages}
                    </div>
                );
            }

            return (
                <tr key={i}>
                    <td>
                        <label htmlFor={id}>{label}</label>
                    </td>
                    <td>{text}</td>
                </tr>
            );
        });
    }

    renderAvatar(image, cachedFields) {
        const klass = classnames('avatar-wrap edit-mode', {});

        if (cachedFields.image) {
            image = cachedFields.image;
        }

        return (
            <div className={klass}>
                <input
                    type="file"
                    name="image"
                    className="avatar-wrap__file-input"
                    accept="image/*"
                    onChange={this.handleEditFieldInput}
                />
                <i className="glyphicons camera" />
                <Avatar
                    image={image}
                    size={130}
                    className="edit-mode"
                    rounded={false}
                />
            </div>
        );
    }

    renderSaveButton(loading) {
        let text = 'Save';

        if (loading) {
            text = <DotsLoader active={loading} />;
        }

        return (
            <div className="save-container">
                <button
                    className="button button--lg"
                    disabled={loading}
                    onClick={this.handleSaveButtonClick}
                >
                    {text}
                </button>
            </div>
        );
    }

    render() {
        const { playlist } = this.props;
        const cachedFields = this.state.editFieldValues;

        // If there's no data, then the request from using the currently
        // logged in user's data at the bottom of the file failed.
        if (!playlist.data) {
            return (
                <div className="profile-container">
                    <p>You don't have permission to view this page.</p>
                </div>
            );
        }

        return (
            <div className="profile-container">
                <header>
                    {this.renderAvatar(playlist.data.image, cachedFields)}
                </header>
                <table className="profile-table">
                    <tbody>
                        {this.renderRows(playlist, fields, cachedFields)}
                    </tbody>
                </table>
                <p className="text-right delete-playlist">
                    <button onClick={this.handlePlaylistDeleteClick}>
                        Delete playlist
                    </button>
                </p>
                <p className="error text-center">
                    {playlist.error ? playlist.error : null}
                </p>
                {this.renderSaveButton(playlist.loading)}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        playlist: state.playlist
    };
}

export default requireAuth(
    connect(mapStateToProps)(
        connectDataFetchers(PlaylistEditContainer, [
            (params, query, props) => {
                if (!props.currentUser.isLoggedIn) {
                    return null;
                }

                return getInfoWithSlugs(
                    props.currentUser.profile.url_slug,
                    params.playlistSlug
                );
            }
        ])
    )
);
