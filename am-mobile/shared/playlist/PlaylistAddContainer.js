import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import connectDataFetchers from 'lib/connectDataFetchers';

import { getPlaylists } from '../redux/modules/user/playlists';
import { addSongToPlaylist, deleteSong } from '../redux/modules/playlist';
import { showMessage } from '../redux/modules/message';
import { hideModal, MODAL_TYPE_ADD_TO_PLAYLIST } from '../redux/modules/modal';

import requireAuth from '../hoc/requireAuth';
import AndroidLoader from '../loaders/AndroidLoader';

class PlaylistAddContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        currentUserPlaylists: PropTypes.object,
        playlist: PropTypes.object,
        history: PropTypes.object,
        dispatch: PropTypes.func,
        onClose: PropTypes.func
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleNewPlaylistClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal(MODAL_TYPE_ADD_TO_PLAYLIST));
        this.props.history.replace('/manage/playlists/new');
    };

    handleSaveClick = () => {
        this.props.onClose();
    };

    handlePlaylistButtonClick = (e) => {
        const { dispatch, playlist } = this.props;
        const button = e.currentTarget;
        const checked = button.getAttribute('data-checked') === 'true';
        const playlistId = parseInt(button.getAttribute('data-id'), 10);
        const songId = playlist.songToAdd.id;

        let artist = playlist.songToAdd.artist;

        if (typeof artist !== 'string') {
            artist = playlist.songToAdd.artist.name;
        }

        const item = `${artist} - ${playlist.songToAdd.title}`;

        if (!checked) {
            dispatch(addSongToPlaylist(playlistId, playlist.songToAdd))
                .then(() => {
                    dispatch(showMessage(`${item} was added to your playlist`));
                    return;
                })
                .catch(() => {
                    dispatch(
                        showMessage(
                            `There was an error adding "${item}" to your playlist`
                        )
                    );
                });
            return;
        }

        dispatch(deleteSong(playlistId, songId))
            .then(() => {
                dispatch(showMessage(`${item} was removed from your playlist`));
                return;
            })
            .catch(() => {
                dispatch(
                    showMessage(
                        `There was an error removing "${item}" from your playlist`
                    )
                );
            });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    listPlaylists(currentUserPlaylists, playlistModule) {
        const { list, songIdToPlaylistIds } = currentUserPlaylists;
        const { songToAdd } = playlistModule;
        const selectedPlaylistIds = songIdToPlaylistIds[songToAdd.id] || [];

        let addItemText = 'Create New Playlist';

        if (songToAdd) {
            addItemText = `Create new playlist with ${songToAdd.title}`;
        }

        const addItem = (
            <div className="list-item" key={0} role="listitem">
                <button
                    className="list-item__inner clearfix add-new-button"
                    onClick={this.handleNewPlaylistClick}
                >
                    <div className="list-item__image list-item__image--placeholder">
                        <i className="glyphicons plus-sign" />
                    </div>
                    <div className="list-item__details list-item__details--padded">
                        <div className="details-inner">
                            <h2 className="list-item__details__title u-fs-13 u-fw-700 u-ls-n-05">
                                {addItemText}
                            </h2>
                            <p>Tap to create a new playlist</p>
                        </div>
                    </div>
                </button>
            </div>
        );

        if (!songToAdd) {
            return [addItem];
        }

        return [addItem].concat(
            list.map((playlist, i) => {
                const checked = selectedPlaylistIds.indexOf(playlist.id) !== -1;
                const buttonClass = classnames('list-item__action-button', {
                    'follow-button': checked,
                    'follow-button--active': checked,
                    'orange-check': checked
                });

                const iconClass = classnames('glyphicons icon', {
                    'plus-sign': !checked,
                    ok: checked
                });

                return (
                    <div className="list-item" key={i + 1} role="listitem">
                        <div className="list-item__inner clearfix">
                            <div className="list-item__image">
                                <img
                                    src={playlist.image}
                                    alt={playlist.title}
                                />
                            </div>
                            <div className="list-item__details list-item__details--padded">
                                <div className="details-inner">
                                    <h2 className="list-item__details__title u-fs-13 u-fw-700 u-ls-n-05">
                                        {playlist.title}
                                    </h2>
                                    <p>
                                        {playlist.track_count}{' '}
                                        {playlist.track_count === 1
                                            ? 'song'
                                            : 'songs'}
                                    </p>
                                </div>
                                <button
                                    className={buttonClass}
                                    onClick={this.handlePlaylistButtonClick}
                                    data-checked={checked}
                                    data-id={playlist.id}
                                >
                                    <i className={iconClass} />
                                </button>
                            </div>
                        </div>
                    </div>
                );
            })
        );
    }

    render() {
        const { currentUserPlaylists, playlist } = this.props;
        // const tracks = user.playlists;

        let saveButton;
        let loader;

        if (currentUserPlaylists.loading) {
            loader = <AndroidLoader size={34} className="list__loader" />;
        }

        if (playlist.songToAdd) {
            saveButton = (
                <button
                    className="button button--lg u-fs-14 u-fw-700 u-tt-uppercase"
                    onClick={this.handleSaveClick}
                >
                    Done
                </button>
            );
        }

        return (
            <div className="list list--bordered list--playlist" role="list">
                {this.listPlaylists(currentUserPlaylists, playlist)}
                {loader}
                <div className="save-container">{saveButton}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        currentUserPlaylists: state.currentUserPlaylists,
        playlist: state.playlist
    };
}

export default requireAuth(
    connect(mapStateToProps)(
        connectDataFetchers(PlaylistAddContainer, [() => getPlaylists()])
    )
);
