import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import analytics, { eventCategory, eventAction } from 'utils/analytics';

import { createPlaylist } from '../redux/modules/playlist';
import { showMessage } from '../redux/modules/message';

import requireAuth from '../hoc/requireAuth';
import PlaylistNew from '../playlist/PlaylistNew';

class PlaylistNewContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        history: PropTypes.object,
        playlist: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editFieldValues: {
                genre: props.playlist.songToAdd
                    ? props.playlist.songToAdd.genre
                    : ''
            }
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSaveButtonClick = () => {
        const { dispatch, playlist } = this.props;
        const data = {
            ...this.state.editFieldValues
        };

        let message = 'Your new playlist was successfully created';

        if (playlist.songToAdd) {
            data.id = playlist.songToAdd.id;
            message = `"${
                playlist.songToAdd.title
            }" was added to your new playlist`;
        }

        dispatch(createPlaylist(data))
            .then((action) => {
                if (action.error) {
                    return null;
                }

                analytics.track(
                    eventCategory.playlist,
                    {
                        eventAction: eventAction.createPlaylist
                    },
                    ['ga', 'fb']
                );

                dispatch(showMessage(message));

                return this.props.history.goBack();
            })
            .catch((err) => {
                console.log(err);

                dispatch(
                    showMessage('There was an error creating your new playlist')
                );
            });
    };

    handleEditFieldInput = (e) => {
        const { name, value } = e.currentTarget;

        if (name === 'image') {
            this.previewFile(e)
                .then((result) => {
                    this.setState((prevState) => {
                        return {
                            ...prevState,
                            editFieldValues: {
                                ...prevState.editFieldValues,
                                [name]: result
                            }
                        };
                    });

                    return;
                })
                .catch((err) => console.log(err));
            return;
        }

        this.setState((prevState) => {
            return {
                ...prevState,
                editFieldValues: {
                    ...prevState.editFieldValues,
                    [name]: value
                }
            };
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    previewFile(e) {
        const file = e.target.files[0];
        const reader = new FileReader();

        return new Promise((resolve) => {
            reader.addEventListener(
                'load',
                () => {
                    resolve(reader.result);
                },
                false
            );

            reader.readAsDataURL(file);
        });
    }

    render() {
        const { playlist } = this.props;
        const defaultThumb = playlist.songToAdd
            ? playlist.songToAdd.image
            : null;

        return (
            <PlaylistNew
                loading={playlist.loading}
                defaultThumb={defaultThumb}
                editFieldValues={this.state.editFieldValues}
                onEditFieldInput={this.handleEditFieldInput}
                onSaveButtonClick={this.handleSaveButtonClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        playlist: state.playlist
    };
}

export default requireAuth(connect(mapStateToProps)(PlaylistNewContainer));
