import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { connect } from 'react-redux';
import classnames from 'classnames';
import MusicPageMeta from 'components/MusicPageMeta';
import Truncate from 'components/Truncate';
import { parse } from 'query-string';
import { DEFAULT_DATE_FORMAT } from 'constants/index';
import {
    isCurrentMusicItem,
    getQueueIndexForSong,
    buildDynamicImage
} from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';

import { getInfoWithSlugs, deletePlaylist } from '../redux/modules/playlist';
import {
    showHeader,
    setGlobalBackground,
    setBannerDeepLink
} from '../redux/modules/global';
import {
    MODAL_TYPE_PLAYLIST_INFO,
    showModal,
    hideModal
} from '../redux/modules/modal';
import { addItems, showDrawer, hideDrawer } from '../redux/modules/drawer';
import { play, pause, editQueue } from '../redux/modules/player';
import { KIND_PLAYLIST } from 'constants/comment';
import { reset } from '../redux/modules/promoKey';

import ListenOnAppButton from '../components/ListenOnAppButton';
import AndroidLoader from '../loaders/AndroidLoader';
import Avatar from '../Avatar';
import NotFound from '../NotFound';
import AlbumList from '../list/AlbumList';
import ActionSheetContainer from '../ActionSheetContainer';
// import PlaylistInfoModal from '../modal/PlaylistInfoModal';
import CommentsWrapper from '../components/CommentsWrapper';

import CogIcon from '../icons/cog';
import PlayIcon from '../icons/play';
import PauseIcon from '../icons/pause';

class PlaylistContainer extends Component {
    static propTypes = {
        modal: PropTypes.object,
        currentUser: PropTypes.object,
        match: PropTypes.object,
        location: PropTypes.object,
        history: PropTypes.object,
        playlist: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.setSiteWrap();
        this.setDeepLink(this.props.playlist.data);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, currentUser, playlist } = this.props;
        const { params: prevParams } = prevProps.match;
        const { params: currentParams } = this.props.match;

        const changedArtistSlug =
            prevParams.artistId !== currentParams.artistId &&
            currentParams.artistId;
        const changedPlaylistSlug =
            prevParams.playlistSlug !== currentParams.playlistSlug &&
            currentParams.playlistSlug;
        const changedUserToken =
            currentUser.token &&
            prevProps.currentUser.token !== currentUser.token;
        const prevPlaylist = prevProps.playlist.data || {};
        const currentPlaylist = playlist.data || {};

        if (prevPlaylist.id !== currentPlaylist.id && playlist.data) {
            this.setSiteWrap();
            this.setDeepLink(playlist.data);
        }

        if (changedArtistSlug || changedPlaylistSlug || changedUserToken) {
            dispatch(
                getInfoWithSlugs(
                    currentParams.artistId,
                    currentParams.playlistSlug
                )
            );
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(showHeader());
        dispatch(setGlobalBackground(null));

        dispatch(hideModal());
        dispatch(reset());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleActionClick = (action, e) => {
        const { dispatch, playlist } = this.props;

        switch (action) {
            case 'info':
                this.handleInfoOpen();
                break;

            case 'edit-playlist': {
                const items = this.getPlaylistEditItems(playlist);

                dispatch(addItems(items));
                dispatch(showDrawer());
                break;
            }

            case 'play':
                this.handlePlayClick(e);
                break;

            default:
                break;
        }
    };

    handlePlayClick = (e) => {
        const { playlist, dispatch, player } = this.props;

        // if trackIndex gets set then we've clicked
        // a track list item. may want to separate
        // the track list item event handler out later
        const buttonIndex = parseInt(
            e.currentTarget.getAttribute('data-index'),
            10
        );
        const currentSong = player.currentSong;
        const musicItem = playlist.data;
        const clickedOnTrack = !isNaN(buttonIndex);
        const shouldCountTrack = !clickedOnTrack;
        const isAlreadyCurrentSong = isCurrentMusicItem(
            currentSong,
            playlist.data,
            shouldCountTrack
        );

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        let queueIndex = getQueueIndexForSong(musicItem, player.queue, {
            trackIndex: buttonIndex,
            currentQueueIndex: player.queueIndex
        });

        if (queueIndex !== -1) {
            dispatch(play(queueIndex));
            return;
        }

        const { queue: newQueue } = dispatch(editQueue(musicItem));

        queueIndex = getQueueIndexForSong(musicItem, newQueue, {
            trackIndex: buttonIndex
        });

        dispatch(play(queueIndex));
    };

    handleInfoOpen = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_PLAYLIST_INFO));
    };

    handleInfoClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setDeepLink(playlistData) {
        if (!playlistData) {
            return;
        }

        const {
            artist: { url_slug: artistSlug },
            url_slug: urlSlug
        } = playlistData;

        const link = `audiomack://playlist/${artistSlug}/${urlSlug}`;

        this.props.dispatch(
            setBannerDeepLink(link, {
                playlist: playlistData.id,
                key: parse(this.props.location.search).key
            })
        );
    }

    getPlaylistEditItems(playlist) {
        const { dispatch, history } = this.props;

        return [
            {
                text: 'Reorder/Remove tracks',
                action: 'reorder-remove-playlist-tracks',
                href: `/manage/playlists/reorder/${playlist.data.url_slug}`
            },
            {
                text: 'Edit playlist details',
                action: 'edit-playlist',
                href: `/manage/playlists/edit/${playlist.data.url_slug}`
            },
            {
                text: 'Share playlist',
                action: 'share-playlist'
            },
            {
                text: 'Delete playlist',
                action: 'delete-playlist',
                handler: () => {
                    if (
                        confirm(
                            'Are you sure you want to delete this playlist?'
                        )
                    ) {
                        dispatch(hideDrawer());
                        dispatch(deletePlaylist(playlist.data.id))
                            .then(() => {
                                return history.push('/playlists/mine');
                            })
                            .catch((err) => console.log(err));
                    }
                }
            }
        ];
    }

    setSiteWrap() {
        const { dispatch, playlist } = this.props;

        if (!playlist.data) {
            return;
        }

        const image = playlist.data.image_base || playlist.data.image;

        dispatch(setGlobalBackground(image));
    }

    render() {
        const {
            playlist,
            player,
            dispatch,
            currentUser,
            /* modal, router, */ location
        } = this.props;

        const isCurrentSong = isCurrentMusicItem(
            player.currentSong,
            playlist.data
        );

        if (playlist.loading) {
            return (
                <div className="text-center">
                    <AndroidLoader className="music-feed__loader" />
                </div>
            );
        }

        if (!playlist.data) {
            return <NotFound errors={playlist.errors} type="playlist" />;
        }

        const isOwner =
            currentUser.isLoggedIn &&
            currentUser.profile.id === playlist.data.artist_id;

        const imageSize = 140;
        const artwork = buildDynamicImage(
            playlist.data.image_base || playlist.data.image,
            {
                width: imageSize,
                height: imageSize,
                max: true
            }
        );

        const retinaArtwork = buildDynamicImage(
            playlist.data.image_base || playlist.data.image,
            {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            }
        );
        const srcSet = `${retinaArtwork} 2x`;

        const avatarProps = {
            image: artwork,
            srcSet: srcSet,
            size: imageSize,
            rounded: false,
            center: true,
            stack: true
        };

        const listProps = {
            dispatch,
            player,
            location,
            currentUser,
            songs: {
                // Build out structure that complies with what the player needs
                data: playlist.data.tracks.map((track) => {
                    track.trackType = track.type;
                    return track;
                })
            },
            album: playlist.data,
            onItemClick: this.handlePlayClick
        };

        const actions = [
            'favorite',
            'comments',
            'share'
            // { icon: 'info-sign', action: 'info' }
        ];

        if (isOwner) {
            actions.push({
                icon: [<CogIcon key="cog" />],
                action: 'edit-playlist'
            });
        }

        const updated = moment(playlist.data.updated * 1000).format(
            DEFAULT_DATE_FORMAT
        );

        let showPaused = false;

        if (!player.paused && isCurrentSong) {
            showPaused = true;
        }

        const playButtonClass = classnames('avatar__play-button', {
            'avatar__play-button--paused': showPaused
        });

        const playButton = (
            <button className={playButtonClass} onClick={this.handlePlayClick}>
                <PlayIcon
                    title="Play"
                    hidden={showPaused}
                    className="song-play__play"
                />
                <PauseIcon
                    title="Pause"
                    hidden={!showPaused}
                    className="song-play__pause"
                />
            </button>
        );

        return (
            <div className="album-container u-padding-top-20">
                <MusicPageMeta
                    musicItem={playlist.data}
                    currentUser={currentUser}
                    location={location}
                />
                <header className="u-pos-relative music-header">
                    <div className="avatar-wrap u-pos-relative">
                        <Avatar {...avatarProps} />
                        {playButton}
                    </div>
                    <Truncate
                        tagName="h1"
                        className="album-container__title u-fs-18 u-ls-n-05 u-fw-700 u-lh-15 u-spacing-top-10"
                        lines={2}
                        text={playlist.data.title}
                    />
                    <p className="u-fs-16 u-lh-14 u-ls-n-04 u-fw-600 u-text-orange">
                        Last Updated: {updated}
                    </p>
                    <p className="u-fs-14 u-ls-n-04 u-lh-15">
                        Number of songs{' '}
                        <strong>{playlist.data.track_count}</strong>
                    </p>
                    <ActionSheetContainer
                        noFlex={false}
                        currentUser={currentUser}
                        dispatch={dispatch}
                        actionItem={playlist.data}
                        actions={actions}
                        onActionItemClick={this.handleActionClick}
                        className="u-spacing-top-15 u-spacing-bottom-15"
                    />
                    <ListenOnAppButton musicItem={playlist.data} />
                </header>

                <AlbumList {...listProps} />
                {/* <PlaylistInfoModal
                    active={modal.type === MODAL_TYPE_PLAYLIST_INFO}
                    router={router}
                    onClose={this.handleInfoClose}
                    playlist={playlist.data}
                /> */}
                <div className="column small-24">
                    <CommentsWrapper
                        item={playlist.data}
                        kind={KIND_PLAYLIST}
                        id={playlist.data.id}
                        className="u-spacing-top-30 u-padding-x-20"
                        total={playlist.data.stats.comments}
                        isMobile
                        history={this.props.history}
                        singleCommentUuid={
                            parse(this.props.location.search).comment
                        }
                        singleCommentThread={
                            parse(this.props.location.search).thread
                        }
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        currentUser: state.currentUser,
        playlist: state.playlist,
        player: state.player
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(PlaylistContainer, [
            (params) => getInfoWithSlugs(params.artistId, params.playlistSlug)
        ])
    )
);
