import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
    getTwitterShareLink,
    getFacebookShareLink,
    getMusicUrl,
    copyToClipboard,
    nativeShare,
    ownsMusic
} from 'utils/index';
import {
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction
} from './redux/modules/user';
import {
    favoritePlaylist,
    unfavoritePlaylist,
    addSong
} from './redux/modules/playlist';
import { getPlaylistsWithSongId } from './redux/modules/user/playlists';
import { addItems, showDrawer, hideDrawer } from './redux/modules/drawer';
import { showMessage } from './redux/modules/message';
import {
    showModal,
    MODAL_TYPE_ADD_TO_PLAYLIST,
    MODAL_TYPE_COPY_URL,
    MODAL_TYPE_APP_DOWNLOAD
} from './redux/modules/modal';
import { bumpMusic } from './redux/modules/admin';

import TwitterIcon from './icons/twitter-logo';
import FacebookIcon from './icons/facebook-letter-logo';
import LinkIcon from './icons/link';
import ActionSheet from './ActionSheet';
import { getPromoKeys } from './redux/modules/promoKey';

class ActionSheetContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        actionItem: PropTypes.object.isRequired,
        actions: PropTypes.array.isRequired,
        history: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        player: PropTypes.object.isRequired,
        className: PropTypes.string,
        noFlex: PropTypes.bool,
        hideStats: PropTypes.bool,
        hideLabel: PropTypes.bool,
        onActionItemClick: PropTypes.func,
        activeGenre: PropTypes.string,
        promoKey: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            nativeShareSupported: false
        };
    }

    componentDidMount() {
        const { dispatch, currentUser, actionItem } = this.props;

        this.setNativeShareSupport();

        const ownMusic = ownsMusic(currentUser, actionItem);

        if (ownMusic) {
            dispatch(getPromoKeys(actionItem.id));
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleActionClick = (e) => {
        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        const {
            currentUser,
            dispatch,
            actionItem,
            history,
            activeGenre
        } = this.props;

        switch (action) {
            case 'favorite': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.favoriteItem(actionItem, newlyLoggedInUser);

                if (!currentUser.isLoggedIn) {
                    dispatch(queueAction(queuedAction));
                    history.push('/login');
                    return;
                }

                queuedAction();
                break;
            }

            case 'repost': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.reupItem(actionItem, newlyLoggedInUser);

                if (!currentUser.isLoggedIn) {
                    dispatch(queueAction(queuedAction));
                    history.push('/login');
                    return;
                }

                queuedAction();
                break;
            }

            case 'playlist': {
                const queuedAction = () => this.playlistItem(actionItem);

                if (!currentUser.isLoggedIn) {
                    dispatch(queueAction(queuedAction));
                    history.push('/login');
                    return;
                }

                queuedAction();
                break;
            }

            case 'share': {
                if (this.state.nativeShareSupported) {
                    nativeShare(actionItem);
                } else {
                    dispatch(addItems(this.getShareItems(actionItem)));
                    dispatch(showDrawer());
                }
                break;
            }

            case 'download': {
                dispatch(
                    showModal(MODAL_TYPE_APP_DOWNLOAD, {
                        item: actionItem
                    })
                );
                break;
            }

            case 'comments': {
                this.handleScrollToComments();
                break;
            }

            case 'bump':
                this.bumpItem(actionItem, activeGenre);
                break;

            default:
                break;
        }

        this.props.onActionItemClick(action, e);
    };

    handleGetPromoLinks = () => {
        const { dispatch } = this.props;

        dispatch(addItems(this.getPromoLinks()));
        dispatch(showDrawer());
    };

    handleCopyUrlClick = (item) => {
        const { dispatch } = this.props;
        const url = getMusicUrl(item, {
            host: process.env.AM_URL
        });
        const successful = copyToClipboard(url);

        if (successful) {
            dispatch(showMessage('URL was copied to clipboard!'));
        } else {
            dispatch(
                showModal(MODAL_TYPE_COPY_URL, {
                    url: url
                })
            );
        }

        dispatch(hideDrawer());
    };

    handleCopyPromoClick = (key) => {
        const { dispatch, actionItem } = this.props;

        const url = `${getMusicUrl(actionItem, {
            host: process.env.AM_URL
        })}?key=${key}`;

        const successful = copyToClipboard(url);

        if (successful) {
            dispatch(showMessage('Private Key URL was copied to clipboard'));
        } else {
            dispatch(
                showModal(MODAL_TYPE_COPY_URL, {
                    url: url
                })
            );
        }

        dispatch(hideDrawer());
    };

    handleScrollToComments = () => {
        const comments = document.getElementById('comments');

        comments.scrollIntoView({ behavior: 'smooth' });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getShareItems(actionItem) {
        const { promoKey, currentUser } = this.props;
        const ownMusic = ownsMusic(currentUser, actionItem);

        const shareItems = [
            {
                text: [
                    <FacebookIcon className="drawer__list-item-icon" key="f" />,
                    'Share on Facebook'
                ],
                action: 'share-facebook',
                href: getFacebookShareLink(
                    process.env.AM_URL,
                    actionItem,
                    process.env.FACEBOOK_APP_ID
                )
            },
            {
                text: [
                    <TwitterIcon className="drawer__list-item-icon" key="t" />,
                    'Share on Twitter'
                ],
                action: 'share-twitter',
                href: getTwitterShareLink(process.env.AM_URL, actionItem)
            },
            {
                text: [
                    <LinkIcon className="drawer__list-item-icon" key="c" />,
                    'Copy URL'
                ],
                action: 'copy-url',
                handler: () => this.handleCopyUrlClick(actionItem)
            }
        ];

        if (ownMusic && promoKey.list.length) {
            shareItems.push({
                text: [
                    <LinkIcon className="drawer__list-item-icon" key="p" />,
                    'Copy Private URL'
                ],
                action: 'promo-links',
                handler: () => this.handleGetPromoLinks()
            });
        }

        return shareItems;
    }

    getPromoLinks() {
        const { promoKey } = this.props;
        const keyArray = [];

        promoKey.list.map((obj) => {
            keyArray.push({
                text: obj.key,
                action: 'copy-promo-link',
                handler: () => this.handleCopyPromoClick(obj.key)
            });
        });

        return keyArray;
    }

    setNativeShareSupport() {
        if (typeof window.navigator.share !== 'undefined') {
            this.setState({
                nativeShareSupported: true
            });
        }
    }

    favoriteItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const addMessage = `'${item.title}' was added to your favorites`;
        const removeMessage = `'${item.title}' was removed from your favorites`;
        const user = newlyLoggedInUser || currentUser.profile;
        let message;

        if (item.type === 'playlist') {
            // Make sure we don't let a newly logged in user to undo an item
            if (
                newlyLoggedInUser ||
                user.favorite_playlists.indexOf(item.id) === -1
            ) {
                dispatch(favoritePlaylist(item.id));
                message = addMessage;
            } else {
                message = removeMessage;
                dispatch(unfavoritePlaylist(item.id));
            }
        }
        // Make sure we don't let a newly logged in user to undo an item
        else if (
            newlyLoggedInUser ||
            user.favorite_music.indexOf(item.id) === -1
        ) {
            message = addMessage;
            dispatch(favorite(item.id));
        } else {
            message = removeMessage;
            dispatch(unfavorite(item.id));
        }

        dispatch(showMessage(message));
    }

    reupItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        let message;

        // Make sure we're comparing the same types
        const itemId = item.id;

        // Make sure we don't let a newly logged in user to undo an item
        if (newlyLoggedInUser || user.reups.indexOf(itemId) === -1) {
            message = `'${
                item.title
            }' was Re-upped and shared with your followers`;
            dispatch(repost(itemId));
        } else {
            dispatch(unrepost(itemId));
        }

        if (message) {
            dispatch(showMessage(message));
        }
    }

    playlistItem(item) {
        const { dispatch } = this.props;

        dispatch(addSong(item));
        dispatch(getPlaylistsWithSongId(item.id));
        dispatch(showModal(MODAL_TYPE_ADD_TO_PLAYLIST));
    }

    bumpItem(item, genre) {
        const { dispatch } = this.props;

        // When the API endpoint is installed this code should dispatch to the API
        //  To attempt to bump the track to the top of the trending chart it's on
        // And upon successful return from the API, physically move it to the top
        // If the top 20 songs are on the page, otherwise remove it from the page?
        const result = dispatch(bumpMusic(item, genre));

        return result;
    }

    render() {
        return (
            <ActionSheet
                actionItem={this.props.actionItem}
                player={this.props.player}
                actions={this.props.actions}
                currentUser={this.props.currentUser}
                className={this.props.className}
                noFlex={this.props.noFlex}
                hideStats={this.props.hideStats}
                hideLabel={this.props.hideLabel}
                onActionClick={this.handleActionClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player,
        promoKey: state.promoKey
    };
}

export default withRouter(connect(mapStateToProps)(ActionSheetContainer));
