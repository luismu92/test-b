import React, { Component } from 'react';
import PropTypes from 'prop-types';

import analytics from 'utils/analytics';

export default class ErrorBoundary extends Component {
    static propTypes = {
        children: PropTypes.object.isRequired,
        boundaryName: PropTypes.string.isRequired,
        errorMessage: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error, info) {
        console.error(this.props.boundaryName, error, info);
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        analytics.error(error);
    }

    render() {
        if (this.state.hasError) {
            if (this.props.errorMessage) {
                return <p>{this.props.errorMessage}</p>;
            }

            return null;
        }
        return this.props.children;
    }
}
