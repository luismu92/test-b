import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// Import admin functionality here when it's created

import {
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_TRENDING,
    showModal,
    hideModal
} from './redux/modules/modal';
import {
    getSongInfo,
    getAlbumInfo,
    getSongInfoById,
    getAlbumInfoById
} from './redux/modules/music';
import { addItems, showDrawer, hideDrawer } from './redux/modules/drawer';
import {
    suspendMusic,
    unsuspendMusic,
    takedownMusic,
    restoreMusic,
    trendMusic,
    excludeStats
} from './redux/modules/admin';
import { showMessage } from './redux/modules/message';

import CogIcon from './icons/cog';
class AdminActionsContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        item: PropTypes.object.isRequired,
        onActionItemClick: PropTypes.func
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleActionClick = (e) => {
        e.preventDefault();

        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        const { item } = this.props;

        switch (action) {
            case 'trending': {
                this.trendItem(item);
                break;
            }
            case 'edit': {
                this.editItem(item);
                break;
            }
            case 'takedown': {
                this.takedownItem();
                break;
            }
            case 'restore': {
                this.restoreItem();
                break;
            }
            case 'suspend': {
                this.suspendItem();
                break;
            }
            case 'unsuspend': {
                this.unsuspendItem();
                break;
            }
            case 'featured': {
                this.featuredItem();
                break;
            }
            case 'streaming': {
                this.streamingItem(item);
                break;
            }
            case 'exclude': {
                this.excludeStats(item);
                break;
            }

            default:
                break;
        }

        this.props.onActionItemClick(action, e);

        this.props.dispatch(hideDrawer());
    };

    handleTrendingToggle = (genre, trend) => {
        const { dispatch, item } = this.props;
        const trendingTypeText = trend ? 'trended' : 'un-trended';

        dispatch(trendMusic(item.id, genre, trend))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.trending !== 'undefined') {
                    dispatch(
                        showMessage(
                            `You have ${trendingTypeText} ${item.artist} - ${
                                item.title
                            } in ${genre}`
                        )
                    );
                }

                if (item.type === 'album') {
                    dispatch(
                        getAlbumInfo(item.uploader.url_slug, item.url_slug)
                    );
                } else {
                    dispatch(
                        getSongInfo(item.uploader.url_slug, item.url_slug)
                    );
                }
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_TRENDING));
                dispatch(
                    showMessage(
                        `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    )
                );
            });
    };

    handleAdminActionClick = () => {
        const { dispatch } = this.props;

        dispatch(addItems(this.getAdminActions()));
        dispatch(showDrawer());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getAdminActions() {
        const { item } = this.props;
        const actions = [
            {
                text: 'Trending Controls',
                action: 'trending',
                handler: this.handleActionClick
            },
            {
                text: 'Admin Edit',
                action: 'edit',
                handler: this.handleActionClick
            }
        ];

        if (item.status === 'takedown') {
            actions.push({
                text: 'Restore Music',
                action: 'restore',
                handler: this.handleActionClick
            });
        } else if (item.status === 'suspended') {
            actions.push({
                text: 'Unsuspend Music',
                action: 'unsuspend',
                handler: this.handleActionClick
            });
        } else {
            actions.push(
                {
                    text: 'Suspend Music',
                    action: 'suspend',
                    handler: this.handleActionClick
                },
                {
                    text: 'Takedown Music',
                    action: 'takedown',
                    handler: this.handleActionClick
                }
            );
        }

        return actions;
    }

    trendItem(item) {
        const { dispatch } = this.props;

        const id = item.id;
        const type = item.type;
        const trendGenres = item.featured_genres;

        dispatch(
            showModal(MODAL_TYPE_TRENDING, {
                title: `Trend ${type}`,
                trendData: {
                    id,
                    trendGenres,
                    handleCheckboxToggle: this.handleTrendingToggle
                }
            })
        );
    }

    editItem(item) {
        window.location = `/admin/${item.type}s/edit/${
            item.uploader.url_slug
        }/${item.url_slug}`;
    }

    takedownItem() {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Takedown?',
                message: 'Are you sure you want to takedown this item?',
                handleConfirm: () => this.doTakedown()
            })
        );
    }

    doTakedown() {
        const { dispatch, item } = this.props;

        dispatch(takedownMusic(item.id))
            .then((action) => {
                const json = action.resolved;

                if (json.status === 'takedown') {
                    if (item.type === 'song') {
                        dispatch(
                            getSongInfo(item.uploader.url_slug, item.url_slug)
                        );
                    } else if (item.type === 'album') {
                        dispatch(
                            getAlbumInfo(item.uploader.url_slug, item.url_slug)
                        );
                    }
                }

                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                return;
            })
            .catch(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
            });
    }

    restoreItem() {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Restore?',
                message: 'Are you sure you want to restore this item?',
                handleConfirm: () => this.doRestore()
            })
        );
    }

    doRestore() {
        const { dispatch, item } = this.props;

        dispatch(restoreMusic(item.id))
            .then((action) => {
                const json = action.resolved;

                if (json.status === 'complete') {
                    if (item.type === 'song') {
                        dispatch(
                            getSongInfo(item.uploader.url_slug, item.url_slug)
                        );
                    } else if (item.type === 'album') {
                        dispatch(
                            getAlbumInfo(item.uploader.url_slug, item.url_slug)
                        );
                    }

                    dispatch(hideModal(MODAL_TYPE_CONFIRM));
                } else {
                    dispatch(hideModal(MODAL_TYPE_CONFIRM));
                }
                return;
            })
            .catch(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
            });
    }

    suspendItem() {
        const { dispatch, item } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `Suspend ${item.type}?`,
                message: `Are you sure you want to suspend the ${item.type} ${
                    item.title
                }?`,
                handleConfirm: () => this.doSuspend()
            })
        );
    }

    doSuspend = () => {
        const { dispatch, item } = this.props;

        dispatch(suspendMusic(item.id))
            .then(() => {
                if (item.type === 'song') {
                    dispatch(getSongInfoById(item.id));
                } else if (item.type === 'album') {
                    dispatch(getAlbumInfoById(item.id));
                }

                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                return;
            })
            .catch(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
            });
    };

    unsuspendItem() {
        const { dispatch, item } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `Un-suspend ${item.type}?`,
                message: `Are you sure you want to un-suspend the ${
                    item.type
                } ${item.title}?`,
                handleConfirm: () => this.doUnsuspend()
            })
        );
    }

    doUnsuspend = () => {
        const { dispatch, item } = this.props;

        dispatch(unsuspendMusic(item.id))
            .then(() => {
                if (item.type === 'song') {
                    dispatch(getSongInfoById(item.id));
                } else if (item.type === 'album') {
                    dispatch(getAlbumInfoById(item.id));
                }

                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                return;
            })
            .catch(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
            });
    };

    featuredItem() {
        const { dispatch, item } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `Feature ${item.type}?`,
                message: `Are you sure you want to feature the ${item.type} ${
                    item.title
                }?`,
                handleConfirm: () => this.doFeature()
            })
        );
    }

    streamingItem(item) {
        console.warn(item, 'Open Streaming Item Controls here');
    }

    excludeStats() {
        const { dispatch, item } = this.props;

        console.warn(item, 'Exclude the item from our charts');

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure?',
                message: `Are you sure you want to exclude this ${
                    item.type
                } from stats rankings?`,
                handleConfirm: () => this.doExcludeStats()
            })
        );
    }

    doExcludeStats() {
        const { dispatch, item } = this.props;

        dispatch(excludeStats(item.id))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.success !== 'undefined') {
                    dispatch(hideModal());
                }
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                dispatch(showMessage('There was a problem excluding stats'));
                console.log(error);
            });
    }

    render() {
        return (
            <button
                className="music-admin-button"
                onClick={this.handleAdminActionClick}
            >
                <CogIcon />
            </button>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        admin: state.admin
    };
}

export default withRouter(connect(mapStateToProps)(AdminActionsContainer));
