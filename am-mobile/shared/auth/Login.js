import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { AUTH_PROVIDER } from 'constants/index';

import AndroidLoader from 'components/loaders/AndroidLoader';

import FacebookIcon from 'icons/facebook-letter-logo';
import TwitterIcon from 'icons/twitter-logo-new';
import GoogleIcon from 'icons/google';
import AppleIcon from 'icons/apple-logo';
import ArrowNextIcon from 'icons/arrow-next';
import PasswordHide from '../icons/eye-hide';
import PasswordView from '../icons/eye-view';

import SocialButton from '../buttons/SocialButton';

import styles from './Auth.module.scss';

const networks = {
    facebook: {
        provider: AUTH_PROVIDER.FACEBOOK,
        icon: <FacebookIcon />
    },
    twitter: {
        provider: AUTH_PROVIDER.TWITTER,
        icon: <TwitterIcon />
    },
    google: {
        provider: AUTH_PROVIDER.GOOGLE,
        icon: <GoogleIcon className="u-no-fill" />
    },
    apple: {
        provider: AUTH_PROVIDER.APPLE,
        icon: <AppleIcon />
    }
};

export default class Login extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        onSocialAuth: PropTypes.func,
        onSocialAuthWithEmail: PropTypes.func,
        onEmailSubmit: PropTypes.func,
        onLoginSubmit: PropTypes.func,
        emailStatus: PropTypes.string,
        userEmail: PropTypes.string,
        userPassword: PropTypes.string,
        showPassword: PropTypes.bool,
        loading: PropTypes.bool,
        onPasswordToggle: PropTypes.func,
        socialEmailError: PropTypes.bool
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getAuthErrors(errors) {
        return errors.reduce(
            (acc, error) => {
                const description = error.description || '';

                // Seems like this error code doesn't change when getting different errors
                if (error.errorcode === 1037) {
                    acc.facebook =
                        'Cannot retrieve necessary details from Facebook. Please make sure you have an email address set in your Facebook account and try again.';
                } else if (description.toLowerCase().indexOf('email') !== -1) {
                    acc.email = description;
                } else if (
                    description.toLowerCase().indexOf('password') !== -1
                ) {
                    acc.password = description;
                } else if (
                    description.toLowerCase().indexOf('connected') !== -1
                ) {
                    acc.password = description;
                }

                if (error.errorcode === 1056) {
                    acc.email =
                        'There is already a registered user with this social network.';
                }

                if (error.errorcode === 1057) {
                    acc.email =
                        'There is already a registered user with this social network email.';
                }

                return acc;
            },
            {
                email: '',
                password: '',
                facebook: ''
            }
        );
    }

    renderAuthError(errors = []) {
        const hasFacebookError = errors.some((error) => {
            return error.errorcode === 1037;
        });

        if (hasFacebookError) {
            return this.renderError({
                description:
                    'Cannot retrieve necessary details from Facebook. Please make sure you have an email address set in your Facebook account and try again.'
            });
        }

        return null;
    }

    renderError(error, key) {
        if (!error) {
            return null;
        }

        const errorMessage = error.description ? error.description : error;

        return (
            <p className={styles.error} key={key}>
                {errorMessage}
            </p>
        );
    }

    renderAuthButton(network) {
        if (!network || !networks[network]) {
            return null;
        }

        if (this.props.loading) {
            return (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        const { provider, icon } = networks[network];

        return (
            <div className={styles.button}>
                <SocialButton
                    network={network}
                    provider={provider}
                    onClick={this.props.onSocialAuth}
                    icon={icon}
                />
                {this.renderAuthError(this.props.currentUser.errors)}
            </div>
        );
    }

    renderSocialButtons() {
        if (this.props.loading) {
            return (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }
        return (
            <div className={styles.socialAuth}>
                {this.renderAuthButton('google')}
                {this.renderAuthButton('twitter')}
                {this.renderAuthButton('facebook')}
                {this.renderAuthButton('apple')}
            </div>
        );
    }

    renderSocialsAndEmailInput() {
        if (
            this.props.emailStatus === 'isTaken' ||
            this.props.socialEmailError
        ) {
            return null;
        }

        if (this.props.loading) {
            return (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        const { currentUser } = this.props;
        const formErrors = this.getAuthErrors(this.props.currentUser.errors);
        const formButtonLoading =
            currentUser.loading || currentUser.checkEmail.loading;

        let loginValidationError;

        if (this.props.emailStatus === 'notTaken') {
            loginValidationError = (
                <p className={styles.validationError}>
                    We don't have an account that matches that email.{' '}
                    <Link
                        to={{
                            pathname: '/join',
                            state: {
                                userEmail: this.props.userEmail
                            }
                        }}
                    >
                        Make a new account for free.
                    </Link>
                </p>
            );
        }

        let passwordIcon = <PasswordView />;
        let inputType = 'password';

        if (this.props.showPassword) {
            passwordIcon = <PasswordHide />;
            inputType = 'text';
        }

        const hasPassword = !!this.props.userPassword;
        const passwordStyle = {};
        let submittingText = 'Logging in…';

        if (!hasPassword || currentUser.checkEmail.loading) {
            passwordStyle.height = 0;
            passwordStyle.overflow = 'hidden';
            submittingText = 'Verifying email…';
        }

        return (
            <Fragment>
                {this.renderSocialButtons()}
                <div className={styles.formContainer}>
                    <h2 className={styles.title}>
                        Or sign up / sign in with your email
                    </h2>
                    {loginValidationError}
                    <form onSubmit={this.props.onEmailSubmit}>
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            name="email"
                            data-testid="email"
                            placeholder="Enter your email"
                            autoComplete="username"
                            autoCapitalize="none"
                        />
                        {this.renderError(formErrors.email)}
                        <div style={passwordStyle} className={styles.inputWrap}>
                            <input
                                type={inputType}
                                key="password"
                                name="password"
                                data-testid="hidden-password"
                                autoComplete="current-password"
                                autoCapitalize="none"
                                placeholder="Enter your password"
                            />
                            <button
                                type="button"
                                tabIndex="-1"
                                className={styles.eye}
                                onClick={this.props.onPasswordToggle}
                            >
                                {passwordIcon}
                            </button>
                            {this.renderError(formErrors.password)}
                        </div>
                        <button
                            type="submit"
                            disabled={formButtonLoading}
                            data-testid="continueToLogin"
                            key="continueToLogin"
                            className={styles.submit}
                        >
                            {formButtonLoading ? submittingText : 'Continue'}
                            <span className={styles.submitIcon}>
                                <ArrowNextIcon />
                            </span>
                        </button>

                        <p className={styles.terms}>
                            By signing up you agree to our
                            <br />
                            <a href="/about/terms-of-service" target="_blank">
                                Terms&nbsp;of&nbsp;Service
                            </a>{' '}
                            and{' '}
                            <a href="/about/privacy-policy" target="_blank">
                                Privacy&nbsp;Policy
                            </a>
                            .
                        </p>
                    </form>
                </div>
            </Fragment>
        );
    }

    renderFullLoginForm() {
        if (
            this.props.emailStatus !== 'isTaken' ||
            this.props.socialEmailError
        ) {
            return null;
        }

        const formErrors = this.getAuthErrors(this.props.currentUser.errors);

        let passwordIcon = <PasswordView />;
        let inputType = 'password';

        if (this.props.showPassword) {
            passwordIcon = <PasswordHide />;
            inputType = 'text';
        }

        return (
            <Fragment>
                <div className={styles.formContainer}>
                    <form onSubmit={this.props.onLoginSubmit}>
                        <div className={styles.inputWrap}>
                            <label htmlFor="email">Email</label>
                            <input
                                type="email"
                                name="email"
                                data-testid="email"
                                placeholder="Enter your email"
                                autoComplete="username"
                                autoCapitalize="none"
                                defaultValue={
                                    this.props.userEmail
                                        ? this.props.userEmail
                                        : ''
                                }
                            />
                            {this.renderError(formErrors.email)}
                        </div>
                        <div className={styles.inputWrap}>
                            <label htmlFor="password">Password</label>
                            <input
                                type={inputType}
                                key="password"
                                name="password"
                                data-testid="password"
                                autoComplete="current-password"
                                autoCapitalize="none"
                                placeholder="Enter your password"
                                required
                            />
                            <button
                                type="button"
                                tabIndex="-1"
                                className={styles.eye}
                                onClick={this.props.onPasswordToggle}
                            >
                                {passwordIcon}
                            </button>
                            {this.renderError(formErrors.password)}
                        </div>
                        <p className={styles.forgot}>
                            <Link
                                to={{
                                    pathname: '/forgot-password',
                                    state: {
                                        userEmail: this.props.userEmail
                                    }
                                }}
                            >
                                Forgot Your Password?
                            </Link>
                        </p>

                        <button
                            type="submit"
                            disabled={this.props.currentUser.loading}
                            data-testid="loginSubmit"
                            key="loginSubmit"
                            className={styles.submit}
                        >
                            {this.props.currentUser.loading
                                ? 'Logging in...'
                                : 'Sign in'}
                            <span className={styles.submitIcon}>
                                <ArrowNextIcon />
                            </span>
                        </button>
                    </form>
                </div>
            </Fragment>
        );
    }

    renderEmailForSocialPanel() {
        const {
            onSocialAuthWithEmail,
            emailStatus,
            currentUser,
            socialEmailError,
            loading
        } = this.props;

        const formErrors = this.getAuthErrors(this.props.currentUser.errors);

        if (!socialEmailError) {
            return null;
        }

        let loginValidationError;

        if (emailStatus === 'notTaken') {
            loginValidationError = (
                <p className={styles.validationError}>
                    An account has already been created with this email address.
                </p>
            );
        }

        let submittingText = 'Logging in…';

        if (currentUser.checkEmail.loading) {
            submittingText = 'Verifying email…';
        }

        const formButtonLoading =
            currentUser.loading || currentUser.checkEmail.loading || loading;

        return (
            <Fragment>
                <div className={styles.formContainer}>
                    <p className={styles.validationError}>
                        Your social account does not have an associated email,
                        please provide one to complete account creation.
                    </p>
                    {loginValidationError}
                    <form onSubmit={onSocialAuthWithEmail}>
                        <input
                            key="email"
                            type="email"
                            name="email"
                            data-testid="email"
                            placeholder="Enter your email"
                            autoComplete="username"
                            autoCapitalize="none"
                        />
                        {this.renderError(formErrors.email)}
                        <button
                            type="submit"
                            disabled={formButtonLoading}
                            data-testid="socialEmailRegistration"
                            className={styles.submit}
                        >
                            {formButtonLoading ? submittingText : 'Continue'}
                            <span className={styles.submitIcon}>
                                <ArrowNextIcon />
                            </span>
                        </button>
                    </form>
                </div>
            </Fragment>
        );
    }

    render() {
        return (
            <Fragment>
                {this.renderSocialsAndEmailInput()}
                {this.renderFullLoginForm()}
                {this.renderEmailForSocialPanel()}
            </Fragment>
        );
    }
}
