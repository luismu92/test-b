import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Recaptcha from 'react-google-recaptcha/lib/recaptcha';

import PasswordHide from '../icons/eye-hide';
import PasswordView from '../icons/eye-view';
import ArrowNextIcon from 'icons/arrow-next';

import styles from './Auth.module.scss';

export default class Signup extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        captchaLoaded: PropTypes.bool,
        showPassword: PropTypes.bool,
        onSignupSubmit: PropTypes.func,
        grecaptcha: PropTypes.object,
        location: PropTypes.object,
        onPasswordToggle: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            loading: null,
            userEmail: (props.location.state || {}).userEmail
        };
    }

    handleCaptchaChange = (response) => {
        window.__captchaResponse = response;
    };

    renderError(error, key) {
        if (!error) {
            return null;
        }

        const errorMessage = error.description ? error.description : error;

        return (
            <p className={styles.error} key={key}>
                {errorMessage}
            </p>
        );
    }

    render() {
        const { currentUser } = this.props;
        const { errors } = currentUser;
        const { userEmail } = this.state;
        const errorObj =
            errors && errors[0] && errors[0].errors ? errors[0].errors : {};
        const messages = Object.keys(errorObj).reduce((obj, key) => {
            const keyObj = errors[0].errors[key];
            const keyObjKey = Object.keys(keyObj)[0];
            const newObj = obj;

            newObj[key] = {
                description: keyObj[keyObjKey]
            };

            return newObj;
        }, {});

        let captcha;

        if (this.props.captchaLoaded) {
            captcha = (
                <Recaptcha
                    className={styles.captcha}
                    sitekey={process.env.CAPTCHA_SITEKEY}
                    onChange={this.handleCaptchaChange}
                    grecaptcha={this.props.grecaptcha}
                />
            );
        }

        let passwordIcon = <PasswordView />;
        let inputType = 'password';

        if (this.props.showPassword) {
            passwordIcon = <PasswordHide />;
            inputType = 'text';
        }

        return (
            <form
                className={styles.formContainer}
                onSubmit={this.props.onSignupSubmit}
            >
                <div className={styles.inputWrap}>
                    <label htmlFor="email">Enter an email address</label>
                    <input
                        type="email"
                        key="email"
                        placeholder="Email Address"
                        data-testid="email"
                        autoComplete="username"
                        autoCapitalize="none"
                        required
                        name="email"
                        defaultValue={userEmail ? userEmail : ''}
                    />
                    {this.renderError(messages.email)}
                </div>
                <div className={styles.inputWrap}>
                    <label htmlFor="username">Enter a username</label>
                    <input
                        type="text"
                        placeholder="Username"
                        data-testid="username"
                        required
                        name="username"
                    />
                    {this.renderError(messages.artist_name)}
                </div>
                <div className={styles.inputWrap}>
                    <label htmlFor="password">Create a password</label>
                    <input
                        type={inputType}
                        key="password"
                        placeholder="Password"
                        data-testid="password"
                        autoComplete="new-password"
                        autoCapitalize="none"
                        required
                        name="password"
                    />
                    <button
                        className={styles.eye}
                        onClick={this.props.onPasswordToggle}
                    >
                        {passwordIcon}
                    </button>
                    {this.renderError(messages.password)}
                </div>
                <div className={styles.inputWrap}>
                    <p className={styles.terms}>
                        By signing up you agree to our{' '}
                        <a href="/about/terms-of-service" target="_blank">
                            Terms of Service
                        </a>{' '}
                        and{' '}
                        <a href="/about/privacy-policy" target="_blank">
                            Privacy Policy
                        </a>
                        .
                    </p>
                </div>
                {captcha}
                <button
                    type="submit"
                    disabled={currentUser.loading}
                    data-testid="registerSubmit"
                    className={styles.submit}
                >
                    {currentUser.loading ? 'Submitting...' : 'Sign up'}
                    <span className={styles.submitIcon}>
                        <ArrowNextIcon />
                    </span>
                </button>
            </form>
        );
    }
}
