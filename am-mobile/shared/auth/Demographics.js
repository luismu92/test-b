import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Select from '../Select';
import styles from './Auth.module.scss';
import { genders } from 'constants/index';

export default class Demographics extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        onDemographicsSubmit: PropTypes.func,
        defaultBirthday: PropTypes.object,
        minBirthday: PropTypes.object,
        maxBirthday: PropTypes.object
    };

    render() {
        const {
            currentUser,
            onDemographicsSubmit,
            defaultBirthday,
            minBirthday,
            maxBirthday
        } = this.props;

        return (
            <form
                className={styles.formContainer}
                onSubmit={onDemographicsSubmit}
            >
                <div className={styles.inputWrap}>
                    <label htmlFor="email">When is your birthday?</label>
                    <input
                        type="date"
                        name="birthday"
                        data-testid="birthday"
                        autoComplete="bday"
                        defaultValue={defaultBirthday.format('YYYY-MM-DD')}
                        min={minBirthday.format('YYYY-MM-DD')}
                        max={maxBirthday.format('YYYY-MM-DD')}
                        required
                    />
                </div>
                <div className={styles.inputWrap}>
                    <label htmlFor="gender">What is your gender?</label>
                    <Select
                        name="gender"
                        defaultValue=""
                        selectProps={{
                            'data-testid': 'gender',
                            required: true
                        }}
                        options={[
                            {
                                value: '',
                                text: 'Choose a gender',
                                disabled: true
                            },
                            ...Object.keys(genders).map((g) => {
                                return {
                                    value: g,
                                    text: genders[g]
                                };
                            })
                        ]}
                    />
                </div>
                <button
                    type="submit"
                    disabled={currentUser.loading}
                    data-testid="demographicsSubmit"
                    className={styles.submit}
                >
                    {currentUser.loading ? 'Submitting...' : 'Finish'}
                </button>
            </form>
        );
    }
}
