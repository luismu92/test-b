import React from 'react';
import PropTypes from 'prop-types';

import ChevronLeft from 'icons/chevron-left';

import styles from './BackButton.module.scss';

export default function BackButton({ onClick }) {
    return (
        <button className={styles.button} onClick={onClick}>
            <ChevronLeft />
        </button>
    );
}

BackButton.propTypes = {
    onClick: PropTypes.func
};
