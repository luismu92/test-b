import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import { AUTH_PROVIDER } from 'constants/index';

import AuthPageMeta from 'components/AuthPageMeta';
import { loadScript, loadFacebookSdk, loadSignInWithApple } from 'utils/index';
import analytics from 'utils/analytics';

import {
    identityCheck,
    logIn,
    logInFacebook,
    logInGoogle,
    logInTwitter,
    logInApple,
    register,
    registerWithCaptcha,
    dequeueAction,
    forgotpw,
    getTwitterRequestToken,
    saveUserDetails
} from '../redux/modules/user';
import { showHeader, hideHeader } from '../redux/modules/global';
import { showMessage } from '../redux/modules/message';

import AmMark from 'icons/am-logo-mark';

import Login from './Login';
import Signup from './Signup';
import ForgotPassword from './ForgotPassword';
import Demographics from './Demographics';
import BackButton from './BackButton';

import styles from './AuthPageContainer.module.scss';
import moment from 'moment';

class AuthPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            captchaLoaded: false,
            showPassword: false,
            loading: false,
            twitterToken: null,
            twitterSecret: null,
            emailStatus: null,
            userEmail: null,
            userPassword: null,
            socialLoginError: false,
            lastSocialLogin: null,
            collectDemographicInfo: false,
            queuedRedirect: null
        };
        const shoudldRedirect = props.currentUser.isLoggedIn;

        if (shoudldRedirect) {
            props.history.replace({
                pathname: '/'
            });
        }
    }

    componentDidMount() {
        const { dispatch } = this.props;

        if (!window.__UA__.isHeadless) {
            loadScript(
                'https://www.google.com/recaptcha/api.js',
                'recaptcha-api'
            )
                .then(() => {
                    this.getCaptchaReference();
                    return;
                })
                .catch((err) => {
                    console.log(err);
                    analytics.error(err);
                });
        }

        loadFacebookSdk({ login: false });

        dispatch(hideHeader());

        this.setTwitterToken();

        this._mounted = true;
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        this.setState({
            userPassword: null
        });

        dispatch(showHeader());

        this.clearState();

        clearTimeout(this._timer);
        this._timer = null;
        this._mounted = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoginSubmit = (e) => {
        e.preventDefault();

        const { dispatch, currentUser, history } = this.props;

        const form = e.currentTarget;
        const email = form.email.value;
        const password = form.password.value;

        dispatch(logIn(email, password))
            .then((action) => {
                const redirectTo =
                    parse(window.location.search).redirectTo || '';
                let goTo = decodeURIComponent(redirectTo) || '/';

                if (goTo.match(/^\/login/)) {
                    goTo = '/';
                }

                history.replace(goTo);

                if (currentUser.queuedLoginAction) {
                    currentUser.queuedLoginAction(action.resolved);
                    dispatch(dequeueAction());
                }
                return;
            })
            .catch((err) => {
                // Remove social logins errors if exists
                this.setState({
                    socialLoginError: false,
                    lastSocialLogin: null
                });
                console.log(err);
            });
        return;
    };

    handleSigninSubmit = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;

        const form = e.currentTarget;
        const email = form.email.value;
        const username = form.username.value;
        const password = form.password.value;
        const captcha = window.__captchaResponse;

        let actionObj;

        if (window.__UA__.isHeadless) {
            actionObj = register(email, username, password, password);
        } else {
            actionObj = registerWithCaptcha(
                email,
                username,
                [password, password],
                captcha
            );
        }

        dispatch(actionObj)
            .then(() => {
                const redirectTo =
                    parse(window.location.search).redirectTo || '';
                let goTo = decodeURIComponent(redirectTo) || '/';

                if (goTo.match(/^\/login|\/join/)) {
                    goTo = '/';
                }

                this.setState({
                    collectDemographicInfo: true,
                    queuedRedirect: goTo
                });

                return;
            })
            .catch(() => {
                if (window.grecaptcha) {
                    window.grecaptcha.reset();
                }
                return;
            });
        return;
    };

    handleDemographicsSubmit = (e) => {
        e.preventDefault();

        const { dispatch, currentUser, history } = this.props;
        const { queuedRedirect } = this.state;

        const form = e.currentTarget;
        const birthday = form.birthday.value;
        const gender = form.gender.value;

        const actionObj = saveUserDetails({
            birthday,
            gender
        });

        dispatch(actionObj)
            .then((action) => {
                if (queuedRedirect) {
                    history.replace(queuedRedirect);
                }

                if (currentUser.queuedLoginAction) {
                    currentUser.queuedLoginAction(action.resolved);
                    dispatch(dequeueAction());
                }

                return;
            })
            .catch((err) => {
                console.error(err);
            });

        return;
    };

    handlePasswordResetSubmit = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;

        const form = e.currentTarget;
        const email = form.email.value;

        dispatch(forgotpw(email))
            .then(() => {
                dispatch(
                    showMessage(
                        `Password reset instructions have been sent to ${email}`
                    )
                );
                return;
            })
            .catch((err) => {
                const errorObj = err && err.errors ? err.errors : {};
                const messages = Object.keys(errorObj).reduce((obj, key) => {
                    const keyObj = err.errors[key];
                    const keyObjKey = Object.keys(keyObj)[0];
                    const newObj = obj;

                    newObj[key] = {
                        description: keyObj[keyObjKey]
                    };

                    return newObj;
                }, {});

                dispatch(showMessage(`${messages.email.description}`));
            });
        return;
    };

    handleSocialAuth = (e) => {
        const provider = e.currentTarget.getAttribute('data-provider');

        let promise;

        switch (provider) {
            case AUTH_PROVIDER.FACEBOOK: {
                promise = this.doFacebookLogin();
                break;
            }
            case AUTH_PROVIDER.TWITTER: {
                promise = this.doTwitterLogin();
                break;
            }
            case AUTH_PROVIDER.GOOGLE: {
                promise = this.doGoogleLogin();
                break;
            }
            case AUTH_PROVIDER.APPLE: {
                promise = this.doAppleLogin();
                break;
            }

            default:
                return;
        }

        promise
            .then((action) => {
                if (!this.state.socialLoginError) {
                    this.finishLogin(action);
                }
                return;
            })
            .finally(() => {
                this.hideLoader();
            });
    };

    handleEmailSubmit = (e) => {
        e.preventDefault();

        const { dispatch, currentUser } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;
        const pwrd = form.password.value;

        this.setState({
            emailStatus: null,
            userPassword: pwrd
        });

        // If we have both values, we're probably using a password
        // manager in which case we can just submit everything.
        if (!!email && !!pwrd && !currentUser.loading) {
            dispatch(logIn(email, pwrd))
                .then((action) => {
                    const redirectTo =
                        parse(window.location.search).redirectTo || '';
                    let goTo = decodeURIComponent(redirectTo) || '/';

                    if (goTo.match(/^\/login/)) {
                        goTo = '/';
                    }

                    this.props.history.replace(goTo);

                    if (currentUser.queuedLoginAction) {
                        currentUser.queuedLoginAction(action.resolved);
                        dispatch(dequeueAction());
                    }
                    return;
                })
                .catch((err) => {
                    console.error(err);
                });
            return;
        }

        dispatch(identityCheck(email, 'email'))
            .then((data) => {
                const isTaken = data.resolved.data.email.taken;

                if (email.length && isTaken) {
                    this.setState({
                        emailStatus: 'isTaken',
                        userEmail: email
                    });
                } else {
                    this.setState({
                        emailStatus: 'notTaken',
                        userEmail: email
                    });
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handlePasswordToggle = (e) => {
        e.preventDefault();

        this.setState({
            showPassword: !this.state.showPassword
        });
    };

    // Check if provided email do no exist in DB
    handleSocialAuthEmailValidation = (e) => {
        e.preventDefault();
        const { dispatch } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;

        dispatch(identityCheck(email, 'email'))
            .then((data) => {
                const isTaken = data.resolved.data.email.taken;

                if (email.length && isTaken) {
                    this.setState({
                        emailStatus: 'notTaken',
                        userEmail: email
                    });
                } else {
                    // If the email is valid, do the social login again with the last token
                    this.handleSocialAuthWithEmail(email);
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleSocialAuthWithEmail = (email) => {
        const { lastSocialLogin } = this.state;
        let promise;

        switch (lastSocialLogin) {
            case 'facebook': {
                promise = this.doFacebookLogin(email);
                break;
            }

            case 'twitter': {
                promise = this.doTwitterLogin(email);
                break;
            }

            default:
                return;
        }

        promise
            .then((action) => {
                this.finishLogin(action);
                return;
            })
            .finally(() => {
                this.hideLoader();
            });
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBackButtonClick = () => {
        const { location, history } = this.props;

        if (location.pathname.match(/^\/login/)) {
            this.setState({
                emailStatus: null,
                userPassword: null
            });
            return;
        }

        history.replace('/login');
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setTwitterToken() {
        this.props
            .dispatch(getTwitterRequestToken())
            .then((result) => {
                this.setState({
                    twitterToken: result.resolved.token,
                    twitterSecret: result.resolved.secret
                });

                this.hideLoader();
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    }

    getCaptchaReference() {
        this._timer = setTimeout(() => {
            if (!window.grecaptcha) {
                this.getCaptchaReference();
                return;
            }

            this._grecaptcha = window.grecaptcha;
            this.setState({
                captchaLoaded: true
            });
        }, 1000);
    }

    getPageFromPath(path) {
        if (path === '/login') {
            return 'login';
        }

        if (path === '/forgot-password') {
            return 'forgot';
        }

        return 'join';
    }

    hideLoader() {
        if (!this._mounted) {
            return;
        }

        this.setState({ loading: false });
    }

    doFacebookLogin(email) {
        const { dispatch } = this.props;
        const userEmail = email ? email : null;

        return new loadFacebookSdk().then(({ login }) => {
            const fbToken = login.authResponse.accessToken;
            const fbUserId = login.authResponse.userID;

            // Idk what this is but it doesnt seem to be used in the API
            const fbUsername = 'username';

            return dispatch(
                logInFacebook(fbToken, fbUserId, fbUsername, userEmail)
            ).catch(() => {
                const {
                    currentUser: { errors }
                } = this.props;

                // Error code for empty email in social token
                if (errors.length > 0 && errors[0].errorcode === 1052) {
                    this.setState({
                        lastSocialLogin: 'facebook',
                        socialLoginError: true
                    });
                }
            });
        });
    }

    doTwitterLogin = (email) => {
        const { dispatch } = this.props;
        const userEmail = email ? email : null;

        return dispatch(
            logInTwitter(
                this.state.twitterToken,
                this.state.twitterSecret,
                userEmail
            )
        ).catch(() => {
            const {
                currentUser: { errors }
            } = this.props;

            // Error code for empty email in social token
            if (errors.length > 0 && errors[0].errorcode === 1052) {
                this.setState({
                    lastSocialLogin: 'twitter',
                    socialLoginError: true
                });
                // Get new twitter tokens
                this.setTwitterToken();
            }
        });
    };

    finishLogin(action) {
        const { dispatch, history, currentUser } = this.props;

        history.replace(`/artist/${action.resolved.user.url_slug}`);
        if (currentUser.queuedLoginAction) {
            currentUser.queuedLoginAction(action.resolved);
            dispatch(dequeueAction());
        }
    }

    doGoogleLogin(preferredEmail) {
        return loadScript(
            'https://apis.google.com/js/platform.js',
            'google-auth'
        ).then(() => {
            return this.initGoogleShit(preferredEmail);
        });
    }

    initGoogleShit(email) {
        return new Promise((resolve, reject) => {
            window.gapi.load('auth2', () => {
                window.gapi.auth2
                    .init({
                        client_id: process.env.GOOGLE_AUTH_ID
                    })
                    .then(() => {
                        resolve();
                        return;
                    })
                    .catch(reject);
            });
        })
            .then(() => {
                const auth2 = window.gapi.auth2.getAuthInstance();

                if (auth2.isSignedIn.get()) {
                    // Check if currently signed in user is the same as intended.
                    const googleUser = auth2.currentUser.get();

                    if (googleUser.getBasicProfile().getEmail() === email) {
                        return Promise.resolve(googleUser);
                    }
                }

                // If the user is not signed in with expected account, let sign in.
                return auth2.signIn({
                    // Set `login_hint` to specify an intended user account,
                    // otherwise user selection dialog will popup.
                    login_hint: email || ''
                });
            })
            .then((googleUser) => {
                // Now user is successfully authenticated with Google.
                // Send ID Token to the server to authenticate with our server.
                // const form = new FormData();
                const token = googleUser.getAuthResponse().id_token;

                return this.props.dispatch(logInGoogle(token));
            });
    }

    doAppleLogin() {
        return loadSignInWithApple()
            .then(() => window.AppleID.auth.signIn())
            .then((data) => {
                return this.props
                    .dispatch(logInApple(data.authorization.id_token))
                    .then((action) => {
                        this.finishLogin(action);
                        return;
                    });
            })
            .catch((error) => {
                throw error;
            });
    }

    clearState() {
        const { history } = this.props;

        console.log('State clearing...');

        history.replace({
            state: {}
        });
    }

    renderPageTitle(whichPage) {
        const { collectDemographicInfo } = this.state;

        let titleText = 'Create a free account';

        if (whichPage === '/login') {
            titleText = 'Sign in to your account';
        } else if (whichPage === '/forgot-password') {
            titleText = 'Reset your password';
        } else if (whichPage === '/join' && collectDemographicInfo) {
            titleText = 'Tell us about yourself:';
        }

        return (
            <h1 className={styles.headerTitle}>
                <div className={styles.logo}>
                    <AmMark />
                </div>
                {titleText}
            </h1>
        );
    }

    renderBackButton(whichPage) {
        if (
            whichPage === '/login' &&
            (!this.state.userEmail || this.state.emailStatus !== 'isTaken')
        ) {
            return null;
        }

        return <BackButton onClick={this.handleBackButtonClick} />;
    }

    renderPageContent(whichPage) {
        const { collectDemographicInfo } = this.state;

        if (whichPage === '/forgot-password') {
            return (
                <ForgotPassword
                    currentUser={this.props.currentUser}
                    onPasswordResetSubmit={this.handlePasswordResetSubmit}
                    loading={this.state.loading}
                    location={this.props.location}
                />
            );
        }

        if (whichPage === '/join') {
            if (collectDemographicInfo) {
                const defaultDate = moment().subtract(22, 'years');
                const minDate = moment().subtract(100, 'years');
                const maxDate = moment().subtract(13, 'years');

                return (
                    <Demographics
                        currentUser={this.props.currentUser}
                        defaultBirthday={defaultDate}
                        minBirthday={minDate}
                        maxBirthday={maxDate}
                        onDemographicsSubmit={this.handleDemographicsSubmit}
                    />
                );
            }

            return (
                <Signup
                    currentUser={this.props.currentUser}
                    captchaLoaded={this.state.captchaLoaded}
                    showPassword={this.state.showPassword}
                    onSignupSubmit={this.handleSigninSubmit}
                    grecaptcha={this._grecaptcha}
                    location={this.props.location}
                    onPasswordToggle={this.handlePasswordToggle}
                />
            );
        }

        return (
            <Login
                currentUser={this.props.currentUser}
                onSocialAuth={this.handleSocialAuth}
                onEmailSubmit={this.handleEmailSubmit}
                onLoginSubmit={this.handleLoginSubmit}
                emailStatus={this.state.emailStatus}
                userEmail={this.state.userEmail}
                userPassword={this.state.userPassword}
                showPassword={this.state.showPassword}
                loading={this.state.loading}
                onPasswordToggle={this.handlePasswordToggle}
                socialEmailError={this.state.socialLoginError}
                onSocialAuthWithEmail={this.handleSocialAuthEmailValidation}
            />
        );
    }

    render() {
        const { location } = this.props;
        const whichPage = location.pathname;
        const page = this.getPageFromPath(whichPage);

        return (
            <div className={styles.container}>
                <AuthPageMeta page={page} />
                <header className={styles.header}>
                    {this.renderBackButton(whichPage)}
                    {this.renderPageTitle(whichPage)}
                </header>
                {this.renderPageContent(whichPage)}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(AuthPageContainer));
