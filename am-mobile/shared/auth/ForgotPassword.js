import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ArrowNextIcon from 'icons/arrow-next';

import styles from './Auth.module.scss';

export default class ForgotPassword extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        onPasswordResetSubmit: PropTypes.func,
        loading: PropTypes.bool,
        location: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            userEmail: null
        };
    }

    componentDidMount() {
        if (this.props.location.state) {
            // eslint-disable-next-line
            this.setState({
                userEmail: this.props.location.state.userEmail
            });
        }
    }

    renderError(error, key) {
        if (!error) {
            return null;
        }

        const errorMessage = error.description ? error.description : error;

        return (
            <p className={styles.error} key={key}>
                {errorMessage}
            </p>
        );
    }

    render() {
        const { userEmail } = this.state;

        return (
            <form
                className={styles.formContainer}
                onSubmit={this.props.onPasswordResetSubmit}
            >
                <label htmlFor="password">Email</label>
                <input
                    type="email"
                    key="email"
                    placeholder="Email Address"
                    autoCapitalize="none"
                    required
                    name="email"
                    defaultValue={userEmail ? userEmail : ''}
                />
                {this.renderError(this.props.currentUser.errors)}
                <div className={styles.inputWrap}>
                    <button
                        type="submit"
                        disabled={this.props.loading}
                        data-testid="forgotSubmit"
                        className={styles.submit}
                    >
                        Email reset link
                        <span className={styles.submitIcon}>
                            <ArrowNextIcon />
                        </span>
                    </button>
                </div>
            </form>
        );
    }
}
