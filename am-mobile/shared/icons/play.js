/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Play extends Component {
    render() {
        return (
            <svg { ...this.props }  width="1692px" height="1692px" viewBox="0 0 1692 1692" version="1.1"><path d="M87,8 C79.6666667,2.66666667 72.6666667,0 66,0 C60,0 55,2.5 51,7.5 C47,12.5 45,19.3333333 45,28 L45,1664 C45,1682.66667 52,1692 66,1692 C72.6666667,1692 79.6666667,1689 87,1683 L1632,877 C1642,868.333333 1647,858.333333 1647,847 C1647,834.333333 1641.66667,823.666667 1631,815 L87,8 Z"/></svg>
        );
    }
}
