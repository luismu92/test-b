import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { ucfirst } from 'utils/index';

export default class NotFound extends Component {
    static propTypes = {
        type: PropTypes.string
    };

    render() {
        const { type } = this.props;
        let message;

        switch (type) {
            case 'artist':
            case 'playlist':
            case 'album':
            case 'song':
                message = <p>{ucfirst(type)} not found</p>;
                break;

            default: {
                message = <p>Page not found</p>;
                break;
            }
        }

        return (
            <Fragment>
                <Helmet>
                    <meta name="robots" content="noindex, nofollow" />
                </Helmet>
                <div className="outer-padding text-center">
                    <h1>404</h1>
                    {message}
                    <p>
                        Discover your next favorite song on our{' '}
                        <Link to="/songs/week">Top Songs</Link>,{' '}
                        <Link to="/albums/week">Top Albums</Link>, or{' '}
                        <Link to="/playlists/browse">Top Playlists Charts</Link>
                        !
                    </p>
                    <ul className="alert-group alert-group--centered">
                        <li>
                            <a
                                href="https://itunes.apple.com/us/app/audiomack/id921765888?ls=1&amp;mt=8"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                <span className="app-badge app-badge--apple" />
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://play.google.com/store/apps/details?id=com.audiomack"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                <span
                                    className="app-badge app-badge--google"
                                    data-reactid="99"
                                />
                            </a>
                        </li>
                    </ul>
                </div>
            </Fragment>
        );
    }
}
