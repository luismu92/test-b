import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import {
    getSearch,
    setQueryType,
    nextPage,
    QUERY_TYPE_FAVORITES,
    QUERY_TYPE_PLAYLISTS,
    QUERY_TYPE_UPLOADS
} from '../redux/modules/searchUser';
import { setQueryContext } from '../redux/modules/search';

import AccountSearchInputContainer from '../search/AccountSearchInputContainer';
import ListContainer from '../list/ListContainer';
import ContextSwitcher from '../widgets/ContextSwitcher';

class AccountSearchContainer extends Component {
    static propTypes = {
        ad: PropTypes.object,
        search: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        match: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextSwitch = (type) => {
        const { dispatch } = this.props;

        dispatch(setQueryType(type));
        dispatch(getSearch());
    };

    handleSubNavChange = (value) => {
        const { dispatch } = this.props;

        dispatch(setQueryContext(value));
        dispatch(getSearch());
    };

    handleNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(getSearch());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getSubNavItems() {
        const items = [];

        return items;
    }

    getContextItems(query, activeType) {
        return [
            {
                text: 'Favorites',
                href: `/account/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_FAVORITES}`,
                value: QUERY_TYPE_FAVORITES,
                active: activeType === QUERY_TYPE_FAVORITES
            },
            {
                text: 'Playlists',
                href: `/account/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_PLAYLISTS}`,
                value: QUERY_TYPE_PLAYLISTS,
                active: activeType === QUERY_TYPE_PLAYLISTS
            },
            {
                text: 'Uploads',
                href: `/account/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_UPLOADS}`,
                value: QUERY_TYPE_UPLOADS,
                active: activeType === QUERY_TYPE_UPLOADS
            }
        ];
    }

    renderEmptyState() {
        return (
            <div className="empty-state">
                <p>No results. Try another search.</p>
            </div>
        );
    }

    renderList(props) {
        const {
            activeType,
            query,
            loading,
            list,
            searchActive,
            onLastPage
        } = props.search;

        const songObj = {
            data: list || [],
            onLastPage,
            loading
        };

        if (!list.length && !loading && !query) {
            return (
                <div className="empty-state">
                    <i className="glyphicons search" />
                    <p>Search for songs & albums…</p>
                </div>
            );
        }

        // If the search dropdown is showing, hide the context switcher
        // and song list
        const style = {};

        if (searchActive) {
            style.display = 'none';
        }

        return (
            <div className="list-view" style={style}>
                <ContextSwitcher
                    items={this.getContextItems(query, activeType)}
                    subnavItems={this.getSubNavItems(query, activeType)}
                    onContextSwitch={this.handleContextSwitch}
                    onSubNavChange={this.handleSubNavChange}
                />
                <ListContainer
                    location={props.location}
                    songs={songObj}
                    showEmptyMessage={false}
                    onNextPage={this.handleNextPage}
                    emptyState={this.renderEmptyState()}
                />
            </div>
        );
    }

    render() {
        return (
            <div className="search-page">
                <AccountSearchInputContainer
                    match={this.props.match}
                    location={this.props.location}
                    inputPlaceholder="Search your songs, albums and playlists"
                />
                {this.renderList(this.props)}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        search: state.searchUser,
        ad: state.ad,
        player: state.player
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(AccountSearchContainer, [
        (params, query) => setQueryType(query.show),
        (params, query) => getSearch(query.q)
    ])
);
