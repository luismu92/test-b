import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { connect } from 'react-redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import { getNotifications } from './redux/modules/user/notifications';
import { setActiveMarker, moveActiveMarker } from './redux/modules/nav';

import NavBar from './NavBar';

class NavBarContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        nav: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserNotifications: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.moveMarker(this.props.nav.activeMarker);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, currentUser } = this.props;

        if (this.props.currentUser.isLoggedIn) {
            if (
                !prevProps.currentUser.isLoggedIn ||
                (prevProps.currentUser.isLoggedIn &&
                    currentUser.profile.id !== prevProps.currentUser.profile.id)
            ) {
                dispatch(getNotifications());
            }
        }

        if (this.props.nav.activeMarker !== prevProps.nav.activeMarker) {
            this.moveMarker(this.props.nav.activeMarker);
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleNavClick = (e) => {
        const { dispatch } = this.props;
        const link = e.currentTarget;
        const marker = link.getAttribute('data-marker');

        dispatch(setActiveMarker(marker));
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    moveMarker(destinationMarker) {
        const { dispatch, nav } = this.props;
        const { width, transformX } = nav;

        if (!this._nav) {
            return;
        }

        const links = Array.from(findDOMNode(this._nav).querySelectorAll('a'));
        const found = links.some((link) => {
            const activeLink =
                link.getAttribute('data-marker') === destinationMarker;

            if (activeLink) {
                const w = link.clientWidth;
                const tx = link.offsetLeft;

                if (width !== w || tx !== transformX) {
                    dispatch(moveActiveMarker({ width: w, transformX: tx }));
                }

                return true;
            }

            return false;
        });

        if (!found) {
            // Here we center the marker wherever you left off so it grows out from
            // the center if you go back to that page
            dispatch(
                moveActiveMarker({
                    width: 0,
                    transformX: transformX + width / 2
                })
            );
        }
    }

    filterNotifications(notifications = []) {
        return notifications.reduce(
            (all, notification) => {
                switch (notification.verb) {
                    case 'playlistfavorite':
                        all.playlistNotifications.push(notification);
                        break;

                    case 'favorite':
                    case 'reup':
                    case 'upload':
                        all.musicNotifications.push(notification);
                        break;

                    case 'playlisted':
                    case 'follow':
                        all.profileNotifications.push(notification);
                        break;

                    default:
                        break;
                }

                return all;
            },
            {
                profileNotifications: [],
                playlistNotifications: [],
                musicNotifications: []
            }
        );
    }

    render() {
        const { currentUser, currentUserNotifications } = this.props;
        const {
            playlistNotifications = [],
            musicNotifications = []
        } = this.filterNotifications(currentUserNotifications.list);

        // if (currentUser.isLoggedIn) {
        //     feedUrl = `/artist/${currentUser.profile.url_slug}/feed`;
        // }

        let feedNotifications = 0;

        if (currentUser.isLoggedIn) {
            feedNotifications = currentUser.profile.new_feed_items;
        }

        return (
            <NavBar
                ref={(e) => (this._nav = e)}
                feedNotifications={feedNotifications}
                playlistNotifications={playlistNotifications.length}
                musicNotifications={musicNotifications.length}
                currentUser={currentUser}
                navPosition={this.props.nav}
                onNavClick={this.handleNavClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        nav: state.nav,
        currentUser: state.currentUser,
        currentUserNotifications: state.currentUserNotifications
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(NavBarContainer, [
        (params, query, props) => {
            if (props.currentUser.isLoggedIn) {
                return getNotifications();
            }

            return null;
        }
    ])
);
