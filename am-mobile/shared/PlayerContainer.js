import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { getMusicUrl, getUploader, buildDynamicImage } from 'utils/index';
import storage, { STORAGE_MUSIC_HISTORY } from 'utils/storage';
import PlayerAudioContainer from 'components/player/PlayerAudioContainer';

import { play, pause, loadMusicFromStorage } from './redux/modules/player';
import {
    queueAction,
    favorite,
    unfavorite,
    repost,
    unrepost
} from './redux/modules/user';
import { addSong } from './redux/modules/playlist';
import { getPlaylistsWithSongId } from './redux/modules/user/playlists';
import { showMessage } from './redux/modules/message';
import { follow, unfollow } from './redux/modules/user/index';
import { showModal, MODAL_TYPE_ADD_TO_PLAYLIST } from './redux/modules/modal';

import HeartIcon from '../../am-shared/icons/heart';
import PlusIcon from './icons/plus';
import RetweetIcon from './icons/retweet';
import FollowIcon from './icons/follow-user';
import FollowingIcon from './icons/following-user';
import VerticalDots from './icons/vertical-dots';
import DotsLoader from './loaders/DotsLoader';
import Drawer from './Drawer';

// @todo refactor the player actions with the action handlers in ActionSheet
// along with its queuedAction functionality.
class PlayerContainer extends Component {
    static propTypes = {
        history: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        // @todo move this state to redux store
        this.state = {
            actionsOverlayOpen: false,
            scrolled: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.loadQueueHistory();
        window.addEventListener('scroll', this.handleScroll, false);
    }

    componentDidUpdate() {
        this.maybeAddBodyClass();
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll, false);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleScroll = () => {
        if (window.pageYOffset > 0) {
            if (!this.state.scrolled) {
                this.setState({ scrolled: true });
            }
        } else if (this.state.scrolled) {
            this.setState({ scrolled: false });
        }
    };

    handleActionsOverlayToggle = () => {
        this.setState({
            actionsOverlayOpen: !this.state.actionsOverlayOpen
        });
    };

    handlePauseClick = () => {
        this.props.dispatch(pause());
    };

    handlePlayClick = () => {
        this.props.dispatch(play());
    };

    handleFavoriteClick = () => {
        this.setState({
            actionsOverlayOpen: false
        });

        const { dispatch, currentUser, player, history } = this.props;
        const itemId = player.currentSong.id;
        const queuedAction = (newlyLoggedInUser) =>
            this.favoriteItem(itemId, newlyLoggedInUser);

        if (!currentUser.isLoggedIn) {
            dispatch(queueAction(queuedAction));
            history.push('/login');
            return;
        }

        queuedAction();
    };

    handlePlaylistClick = () => {
        this.setState({
            actionsOverlayOpen: false
        });

        const { dispatch, currentUser, history } = this.props;
        const queuedAction = () =>
            this.playlistItem(this.props.player.currentSong);

        if (!currentUser.isLoggedIn) {
            dispatch(queueAction(queuedAction));
            history.push('/login');
            return;
        }

        queuedAction();
    };

    handleReupClick = () => {
        this.setState({
            actionsOverlayOpen: false
        });

        const { dispatch, currentUser, player, history } = this.props;
        const itemId = player.currentSong.id;
        const queuedAction = (newlyLoggedInUser) =>
            this.reupItem(itemId, newlyLoggedInUser);

        if (!currentUser.isLoggedIn) {
            dispatch(queueAction(queuedAction));
            history.push('/login');
            return;
        }

        queuedAction();
    };

    handleFollowClick = (artist, shouldFollow) => {
        this.setState({
            actionsOverlayOpen: false
        });

        const { dispatch, currentUser, history } = this.props;
        const queuedAction = (newlyLoggedInUser) =>
            this.followArtist(artist, shouldFollow, newlyLoggedInUser);

        if (!currentUser.isLoggedIn) {
            dispatch(queueAction(queuedAction));
            history.push('/login');
            return;
        }

        queuedAction();
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getUrlFromMusicItem(musicItem) {
        if (musicItem.parentDetails) {
            return getMusicUrl(musicItem.parentDetails);
        }

        return getMusicUrl(musicItem);
    }

    getPlayerActionItems() {
        const { currentUser, player } = this.props;

        if (!player.currentSong) {
            return null;
        }

        const buttonClass = 'mobile-player-actions__button';
        const activeClass = 'mobile-player-actions__button--active';
        const isFavorited =
            currentUser.isLoggedIn &&
            currentUser.profile.favorite_music &&
            currentUser.profile.favorite_music.indexOf(
                player.currentSong.id
            ) !== -1;
        const favoriteClass = classnames(buttonClass, {
            [activeClass]: isFavorited
        });
        const isReupped =
            currentUser.isLoggedIn &&
            currentUser.profile.reups &&
            currentUser.profile.reups.indexOf(player.currentSong.id) !== -1;
        const reupClass = classnames(buttonClass, {
            [activeClass]: isReupped
        });

        const items = [
            {
                text: (
                    <Fragment>
                        <HeartIcon className="drawer__list-item-icon drawer__list-item-icon--fav" />{' '}
                        {isFavorited ? 'Remove Favorite' : 'Favorite'}
                    </Fragment>
                ),
                className: favoriteClass,
                handler: this.handleFavoriteClick
            }
        ];

        items.push({
            text: (
                <Fragment>
                    <PlusIcon className="drawer__list-item-icon" /> Add to
                    Playlist
                </Fragment>
            ),
            className: 'mobile-player-actions__button',
            handler: this.handlePlaylistClick
        });

        items.push({
            text: (
                <Fragment>
                    <RetweetIcon className="drawer__list-item-icon" />{' '}
                    {isReupped ? 'Remove Re-Up' : 'Re-Up'}
                </Fragment>
            ),
            className: reupClass,
            handler: this.handleReupClick
        });

        const currentItem =
            player.currentSong.parentDetails || player.currentSong;
        const songArtist = getUploader(currentItem);

        if (
            !currentUser.isLoggedIn ||
            (currentUser.isLoggedIn &&
                currentUser.profile.following &&
                currentUser.profile.id !== songArtist.id)
        ) {
            const following =
                currentUser.isLoggedIn &&
                currentUser.profile.following.indexOf(songArtist.id) !== -1;
            const followingClass = classnames(buttonClass, {
                [activeClass]: following
            });
            let text;

            if (following) {
                text = (
                    <Fragment>
                        <FollowingIcon
                            className="drawer__list-item-icon"
                            title="Tap to unfollow"
                        />{' '}
                        Unfollow {songArtist.name}
                    </Fragment>
                );
            } else {
                text = (
                    <Fragment>
                        <FollowIcon
                            className="drawer__list-item-icon"
                            title="Tap to follow"
                        />{' '}
                        Follow {songArtist.name}
                    </Fragment>
                );
            }

            items.push({
                text,
                className: followingClass,
                // @todo figure out a better way to do this without creating a new function each time render is called
                handler: () => this.handleFollowClick(songArtist, !following)
            });
        }

        return items;
    }

    maybeAddBodyClass() {
        const klass = 'has-active-player';

        if (this.props.player.currentSong) {
            if (!document.body.classList.contains(klass)) {
                document.body.classList.add(klass);
            }
            return;
        }

        if (document.body.classList.contains(klass)) {
            document.body.classList.remove(klass);
        }
    }

    loadQueueHistory() {
        const { dispatch } = this.props;
        const json = true;
        const history = storage.get(STORAGE_MUSIC_HISTORY, json);

        if (history) {
            dispatch(loadMusicFromStorage(history));
        }
    }

    favoriteItem(itemId, newlyLoggedInUser) {
        const { dispatch, player, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;

        // Dont allow a newly logged in user to undo on an item
        if (newlyLoggedInUser || user.favorite_music.indexOf(itemId) === -1) {
            dispatch(favorite(itemId))
                .then(() => {
                    dispatch(
                        showMessage(
                            `'${
                                player.currentSong.title
                            }' was added to your favorites`
                        )
                    );
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    dispatch(
                        showMessage(
                            `There was a problem adding '${
                                player.currentSong.title
                            }' to your favorites`
                        )
                    );
                });
        } else {
            dispatch(unfavorite(itemId))
                .then(() => {
                    dispatch(
                        showMessage(
                            `'${
                                player.currentSong.title
                            }' was removed from your favorites`
                        )
                    );
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    dispatch(
                        showMessage(
                            `There was a problem removing '${
                                player.currentSong.title
                            }' from your favorites`
                        )
                    );
                });
        }
    }

    reupItem(itemId, newlyLoggedInUser) {
        const { dispatch, currentUser, player } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;

        // Dont allow a newly logged in user to undo on an item
        if (newlyLoggedInUser || user.reups.indexOf(itemId) === -1) {
            dispatch(repost(itemId))
                .then(() => {
                    dispatch(
                        showMessage(
                            `'${
                                player.currentSong.title
                            }' was re-upped and shared with your followers`
                        )
                    );
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    dispatch(
                        showMessage(
                            `There was a problem adding '${
                                player.currentSong.title
                            }' to your reups`
                        )
                    );
                });
        } else {
            dispatch(unrepost(itemId))
                .then(() => {
                    dispatch(
                        showMessage(
                            `'${
                                player.currentSong.title
                            }' was Re-upped and shared with your followers`
                        )
                    );
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    dispatch(
                        showMessage(
                            `There was a problem removing '${
                                player.currentSong.title
                            }' from your reups`
                        )
                    );
                });
        }
    }

    followArtist(artist, shouldFollow, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;

        if (user.id === artist.id) {
            dispatch(showMessage('You cannot follow yourself'));
        }
        // Dont allow a newly logged in user to undo on an item
        else if (newlyLoggedInUser || shouldFollow) {
            dispatch(follow(artist))
                .then(() => {
                    dispatch(
                        showMessage(`You are now following ${artist.name}`)
                    );
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    dispatch(
                        showMessage(
                            `There was a problem following ${artist.name}`
                        )
                    );
                });
        } else {
            dispatch(unfollow(artist))
                .then(() => {
                    dispatch(showMessage(`You unfollowed ${artist.name}`));
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    dispatch(
                        showMessage(
                            `There was a problem unfollowing ${artist.name}`
                        )
                    );
                });
        }
    }

    playlistItem(song) {
        const { dispatch } = this.props;

        dispatch(addSong(song));
        dispatch(getPlaylistsWithSongId(song.id));
        dispatch(showModal(MODAL_TYPE_ADD_TO_PLAYLIST));
    }

    renderActionsOverlay(items, open) {
        const klass = classnames('mobile-player-actions', {
            'mobile-player-actions--active': open
        });

        return (
            <Drawer
                visible
                items={items}
                onDrawerItemClick={this.handleDrawerItemClick}
                displayOverlay={false}
                addCloseButtonItem={false}
                className={klass}
            />
        );
    }

    renderInterface() {
        const {
            currentSong,
            paused,
            currentTime,
            duration,
            hidden
        } = this.props.player;

        if (!currentSong) {
            return null;
        }
        const playButtonClass = classnames('glyphicons', {
            pause: !paused,
            play: paused
        });
        const playerClass = classnames('mobile-player', {
            higher: this.state.scrolled
        });
        const playButtonAction = paused
            ? this.handlePlayClick
            : this.handlePauseClick;
        const loaderStyles = {
            left: '0px',
            top: '-3px',
            display: 'inline-block'
        };

        const elapsed = (currentTime / duration) * 100;
        const artist = currentSong.artist;
        const title = currentSong.title;
        const url = this.getUrlFromMusicItem(currentSong);

        let style = {};

        if (hidden) {
            style = {
                display: 'none'
            };
        }

        const imageSize = 36;
        const artwork = buildDynamicImage(
            currentSong.image_base || currentSong.image,
            {
                width: imageSize,
                height: imageSize,
                max: true
            }
        );

        const retinaArtwork = buildDynamicImage(
            currentSong.image_base || currentSong.image,
            {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            }
        );
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        return (
            <div className={playerClass} style={style}>
                <div className="mobile-player__progress">
                    <div
                        className="mobile-player__progress-inner"
                        role="progressbar"
                        aria-label="played"
                        aria-valuemin="0"
                        aria-valuemax={duration}
                        aria-valuenow={currentTime}
                        aria-valuetext={`${Math.round(
                            currentTime
                        )} seconds played`}
                        style={{ width: `${elapsed}%` }}
                    />
                </div>
                <div className="mobile-player-inner u-padding-10">
                    <div className="mobile-player-inner__artwork">
                        <img src={artwork} srcSet={srcSet} alt="" />
                    </div>
                    <Link
                        to={url}
                        className="mobile-player-metadata u-padding-left-10 u-padding-top-5"
                    >
                        <span className="artist u-fs-12 u-ls-n-05 u-lh-12">
                            {artist}
                        </span>
                        <span className="title u-fs-12 u-ls-n-05 u-lh-12">
                            {title}
                        </span>
                    </Link>
                    <div className="mobile-player-inner__button">
                        <button
                            className="mobile-player-inner__play-button play-button playlist mobile-player-inner__play-button--pulse"
                            onClick={playButtonAction}
                        >
                            <i className={playButtonClass} />
                            <DotsLoader
                                active={false}
                                containerStyles={loaderStyles}
                            />
                        </button>
                    </div>
                    <button
                        className="vertical-dots mobile-player__vertical-dots"
                        onClick={this.handleActionsOverlayToggle}
                    >
                        <VerticalDots />
                    </button>
                </div>
                {this.renderActionsOverlay(
                    this.getPlayerActionItems(),
                    this.state.actionsOverlayOpen
                )}
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <PlayerAudioContainer />
                {this.renderInterface()}
            </Fragment>
        );
    }
}

export default withRouter(
    connect((state) => {
        return {
            player: state.player,
            currentUser: state.currentUser
        };
    })(PlayerContainer)
);
