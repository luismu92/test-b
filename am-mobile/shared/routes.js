/* eslint react/no-multi-comp: 0, react/prop-types: 0 */
import React from 'react';
import universal from 'react-universal-component';
import { Switch, Route, Redirect } from 'react-router-dom';
import {
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_RECENTLY_ADDED,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    GENRE_TYPE_ALL,
    timeMap,
    liveGenres,
    liveGenresExcludedFromCharts
} from 'constants/index';
import { SECTIONS } from 'constants/stats/section';
import analytics from 'utils/analytics';

function noop() {
    return null;
}

function DefaultError({ error }) {
    if (process.env.NODE_ENV === 'development') {
        return (
            <div>
                Error: <p>{error.message}</p>
                <p>{error.stack}</p>
            </div>
        );
    }

    analytics.error(error);

    return null;
}

const universalComponentOptions = {
    loading: noop,
    error: DefaultError
};
const UniversalComponent = universal(
    (props) => import(`./${props.page}`),
    universalComponentOptions
);

function renderComponent(props) {
    if (props.isServer) {
        const Component = universal(
            import(`./${props.page}`),
            universalComponentOptions
        );

        return Component.preload(props).then(() => {
            return {
                Component,
                props
            };
        });
    }

    return <UniversalComponent {...props} />;
}

// If we're here that probably means we have failed to load a chunk.
// i.e. something like /static/dist/0.44lfadsf9430fasf.js doesnt exist anymore
//
// This can happen by the following steps:
// 1. A user loads the site
// 2. A deploy happens where new chunks are generated in the new docker container
// 3. A user tries to navigate to another page where the initially loaded javascript
// will try to load the associated javascript chunk for the new page. Spoiler
// alert: it probably doesnt exist anymore since the new docker container has newly
// build chunks
// function onRouteError(err, store, callback, is404) {
//     console.error(err);
//     if (!is404) {
//         const message = 'Audiomack has been updated, please refresh your browser';

//         store.dispatch(showMessage(message));
//         setTimeout(() => {
//             store.dispatch(hideMessage());
//         }, 3000);
//     }

//     return callback(err);
// }

function getArtistPageHeaderTitle(props) {
    const { currentUser, artist } = props;

    if (
        currentUser.isLoggedIn &&
        artist.profile &&
        artist.profile.id === currentUser.profile.id
    ) {
        return 'My Account';
    }

    if (artist.profile) {
        return artist.profile.name;
    }

    return '';
}

function getMusicPageHeaderTitle(props) {
    const { song } = props.songs;

    if (song && song.title) {
        return song.title;
    }

    return 'Song';
}

const timePeriodRegex = `(${Object.values(timeMap)
    .filter(Boolean)
    .join('|')})`;
const genreChartPages = liveGenres.reduce((acc, genre) => {
    const genreSlug = genre === GENRE_TYPE_ALL ? '' : `/${genre}`;
    const genreRoutes = [
        {
            path: `${genreSlug}/${COLLECTION_TYPE_TRENDING}/:pageString(page)?/:page(\\d+)?`,
            sitemap: `${genreSlug}/${COLLECTION_TYPE_TRENDING}`,
            context: COLLECTION_TYPE_TRENDING,
            section: SECTIONS.trending
        },
        liveGenresExcludedFromCharts.includes(genre)
            ? null
            : {
                  path: `${genreSlug}/${COLLECTION_TYPE_SONG}/:timePeriod?/:pageString(page)?/:page(\\d+)?`,
                  sitemap: `${genreSlug}/${COLLECTION_TYPE_SONG}`,
                  context: COLLECTION_TYPE_SONG,
                  section: SECTIONS.trendingSongs
              },
        liveGenresExcludedFromCharts.includes(genre)
            ? null
            : {
                  path: `${genreSlug}/${COLLECTION_TYPE_ALBUM}/:timePeriod?/:pageString(page)?/:page(\\d+)?`,
                  sitemap: `${genreSlug}/${COLLECTION_TYPE_ALBUM}`,
                  context: COLLECTION_TYPE_ALBUM,
                  section: SECTIONS.trendingAlbums
              },
        {
            path: `${genreSlug}/${COLLECTION_TYPE_RECENTLY_ADDED}/:timePeriod${timePeriodRegex}?/:pageString(page)?/:page(\\d+)?`,
            sitemap: `${genreSlug}/${COLLECTION_TYPE_RECENTLY_ADDED}`,
            context: COLLECTION_TYPE_RECENTLY_ADDED,
            section: SECTIONS.recentlyAdded
        }
    ]
        .filter(Boolean)
        .map(({ path, context, sitemap, section }) => {
            return {
                path,
                sitemap,
                render(props) {
                    const route = {
                        title: 'Browse New Music',
                        context: context,
                        genre: genre,
                        section: section
                    };
                    const allProps = {
                        ...props,
                        route,
                        page: 'browse/BrowseContainer'
                    };

                    return renderComponent(allProps);
                }
            };
        });

    acc = acc.concat(genreRoutes);

    return acc;
}, []);

// You can test your routes here
// https://forbeslindesay.github.io/express-route-tester/

export const routeConfig = [
    /* **** Browsing **** */
    /* Default */
    {
        sitemap: true,
        exact: true,
        path: '/',
        render(props) {
            const route = {
                title: 'Browse New Music',
                genre: GENRE_TYPE_ALL,
                context: COLLECTION_TYPE_TRENDING,
                section: SECTIONS.homePage
            };
            const allProps = {
                ...props,
                route,
                page: 'browse/BrowseContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/am-shell',
        render(props) {
            const route = {
                title: 'Audiomack'
            };
            const allProps = {
                ...props,
                page: 'Shell',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Playlists */
    {
        sitemap: '/playlists/browse',
        path: '/playlists/browse/:urlSlug?',
        render(props) {
            const route = {
                title: 'Playlists',
                section: SECTIONS.newPlaylists
            };
            const allProps = {
                ...props,
                route,
                page: 'browse/BrowsePlaylistsContainer'
            };

            return renderComponent(allProps);
        }
    },

    // Redirect old recent urls before parsing for new ones
    { from: '/recent/date/:date', to: '/recent' },
    ...genreChartPages,

    /* Podcasts
    { path: '/podcast' component: PodcastTrendingContainer} title: 'Podcasts'{ path: 'trending' />
    },
    { path: '/podcast/songs/week' component: PodcastTopPodcastsContainer} title: 'Podcasts' />
    { path: '/podcast/recent' component: PodcastRecentContainer} title: 'Podcasts' />
    */

    // Redirect joe's old handle because he's special
    {
        from: '/song/fuckjoevango/:context(feed|suggested-follows)',
        to: '/song/joevango/:context(feed|suggested-follows)'
    },
    {
        path: '/artist/:artistId/:context(feed)',
        sitemap: '/feed',
        render(props) {
            const route = {
                title: 'My Feed',
                section: SECTIONS.feed
            };
            const allProps = {
                ...props,
                route,
                page: 'user/FeedContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/artists/:context(popular)',
        sitemap: '/artists/popular',
        render(props) {
            const route = {
                title: "Browse Audiomack's Top Artists",
                section: SECTIONS.popularArtists
            };
            const allProps = {
                ...props,
                page: 'user/FeedContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    // Redirect old /suggested-follows URL
    {
        from: '/artist/:artistId/:context(suggested-follows)',
        to: '/artists/popular'
    },
    {
        exact: true,
        path: '/search',
        render(props) {
            const route = {
                title: 'Search',
                section: SECTIONS.search
            };
            const allProps = {
                ...props,
                route,
                page: 'search/SearchContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/login',
        render(props) {
            const route = {
                activeTab: 'login',
                title: 'Sign in',
                section: SECTIONS.login
            };
            const allProps = {
                ...props,
                route,
                page: 'auth/AuthPageContainer'
            };

            return renderComponent(allProps);
        }
    },

    {
        exact: true,
        path: '/join',
        render(props) {
            const route = {
                activeTab: 'join',
                title: 'Sign up',
                section: SECTIONS.signUp
            };
            const allProps = {
                ...props,
                route,
                page: 'auth/AuthPageContainer'
            };

            return renderComponent(allProps);
        }
    },

    {
        exact: true,
        path: '/forgot-password',
        render(props) {
            const route = {
                activeTab: 'login',
                title: 'Password Recovery',
                section: SECTIONS.forgotPassword
            };
            const allProps = {
                ...props,
                route,
                page: 'auth/AuthPageContainer'
            };

            return renderComponent(allProps);
        }
    },

    /* World */
    {
        exact: true,
        sitemap: true,
        path: '/world',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/MainPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/world/post/:slug',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/PostPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/world/tag/:slug',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/MainPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/world/:page',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/MainPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/artist/:artistId/claim/:code/:ee',
        render(props) {
            const route = {
                title: getArtistPageHeaderTitle,
                section: SECTIONS.claimArtist
            };
            const allProps = {
                ...props,
                route,
                page: 'artist/ArtistContainer'
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    {
        from: '/artist/fuckjoevango/:context?/:pageString(page)?/:page(\\d+)?',
        to: '/artist/joevango/:context?/:pageString(page)?/:page(\\d+)?'
    },
    {
        path:
            '/artist/:artistId/:context(uploads|favorites|playlists|following|followers)?/:pageString(page)?/:page(\\d+)?',
        render(props) {
            const route = {
                title: getArtistPageHeaderTitle,
                section: SECTIONS.artist
            };
            const allProps = {
                ...props,
                route,
                page: 'artist/ArtistContainer'
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/edit/profile/:tab?',
        render(props) {
            const route = {
                title: 'Edit Account',
                section: SECTIONS.editProfile
            };
            const allProps = {
                ...props,
                route,
                page: 'user/ProfileEditContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/manage/playlists/new',
        render(props) {
            const route = {
                title: 'Create new playlist',
                section: SECTIONS.newPlaylists
            };
            const allProps = {
                ...props,
                route,
                page: 'playlist/PlaylistNewContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/manage/playlists/edit/:playlistSlug',
        render(props) {
            const route = {
                title: 'Edit playlist details',
                section: SECTIONS.editMusic
            };
            const allProps = {
                ...props,
                route,
                page: 'playlist/PlaylistEditContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/manage/playlists/reorder/:playlistSlug',
        render(props) {
            const route = {
                title: 'Reorder/Remove Tracks',
                section: SECTIONS.editMusic
            };
            const allProps = {
                ...props,
                route,
                page: 'playlist/PlaylistReorderContainer'
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    { from: '/song/fuckjoevango/:songSlug', to: '/song/joevango/:songSlug' },
    {
        path: '/song/:artistId/:songSlug',
        render(props) {
            const route = {
                title: getMusicPageHeaderTitle,
                section: SECTIONS.playSong
            };
            const allProps = {
                ...props,
                route,
                page: 'song/SongPageContainer'
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    {
        from: '/album/fuckjoevango/:albumSlug',
        to: '/album/joevango/:albumSlug'
    },
    {
        path: '/album/:artistId/:albumSlug',
        render(props) {
            const route = {
                title: getMusicPageHeaderTitle,
                section: SECTIONS.playAlbum
            };
            const allProps = {
                ...props,
                route,
                page: 'album/AlbumContainer'
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    {
        from: '/playlist/fuckjoevango/:playlistSlug',
        to: '/playlist/joevango/:playlistSlug'
    },
    {
        path: '/playlist/:artistId/:playlistSlug',
        render(props) {
            const route = {
                title: getMusicPageHeaderTitle,
                section: SECTIONS.playlist
            };
            const allProps = {
                ...props,
                route,
                page: 'playlist/PlaylistContainer'
            };

            return renderComponent(allProps);
        }
    },
    // {
    //     exact: true,
    //     sitemap: true,
    //     path: '/about',
    //     render(props) {
    //         const route = {
    //             title: 'Create a Free Account',
    //             section: SECTIONS.about
    //         };
    //         const allProps = {
    //             ...props,
    //             page: 'about/AboutPageContainer',
    //             route
    //         };

    //         return renderComponent(allProps);
    //     }
    // },
    {
        exact: true,
        sitemap: true,
        path: '/about/terms-of-service',
        render(props) {
            const route = {
                title: 'Terms of Service',
                section: SECTIONS.tos
            };
            const allProps = {
                ...props,
                route,
                page: 'about/TosContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/about/privacy-policy',
        render(props) {
            const route = {
                title: 'Privacy Policy',
                section: SECTIONS.privacyPolicy
            };
            const allProps = {
                ...props,
                route,
                page: 'about/PrivacyContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: '/contact-us',
        path: '/contact-us/:initialForm?',
        render(props) {
            const route = {
                title: 'Contact Us',
                section: SECTIONS.contactUs
            };
            const allProps = {
                ...props,
                route,
                page: 'about/ContactPageContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/about/legal',
        render(props) {
            const route = {
                title: 'Legal & DMCA',
                section: SECTIONS.legal
            };
            const allProps = {
                ...props,
                route,
                page: 'about/LegalPageContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/account/search',
        render(props) {
            const route = {
                title: 'Search My Account',
                section: SECTIONS.accountSearch
            };
            const allProps = {
                ...props,
                route,
                page: 'account/AccountSearchContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/notifications/:pageString(page)?/:page(\\d+)?',
        render(props) {
            const route = {
                title: 'Your Notifications',
                section: SECTIONS.notifications
            };
            const allProps = {
                ...props,
                route,
                page: 'user/NotificationContainer'
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/email/unsubscribe/:hash',
        render(props) {
            const route = {
                title: 'Unsubscribe From Email',
                section: SECTIONS.emailUnsubscribe
            };
            const allProps = {
                ...props,
                page: 'user/EmailUnsubscribePageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Catch all redirects */

    { from: '/feed', to: '/feed/timeline' },

    { from: '/rap/playlists', to: '/playlists/browse' },
    { from: '/electronic/playlists', to: '/playlists/browse' },
    { from: '/afrobeats/playlists', to: '/playlists/browse' },
    { from: '/afropop/playlists', to: '/playlists/browse' },
    { from: '/dancehall/playlists', to: '/playlists/browse' },
    { from: '/pop/playlists', to: '/playlists/browse' },
    { from: '/podcast/playlists', to: '/playlists/browse' },
    { from: '/playlist', to: '/playlists/browse' },

    { from: '/trending', to: '/trending-now' },
    { from: '/rap', to: '/rap/trending-now' },
    { from: '/electronic', to: '/electronic/trending-now' },
    { from: '/afrobeats', to: '/afrobeats/trending-now' },
    { from: '/afropop', to: '/afrobeats/trending-now' },
    { from: '/dancehall', to: '/dancehall/trending-now' },
    { from: '/pop', to: '/pop/trending-now' },
    { from: '/podcast', to: '/podcast/trending-now' },

    { from: '/manage/playlists/new', to: '/manage/playlists/add-to-playlist' },
    { from: '/account/create', to: '/about' },
    { from: '/manage/albums/create', to: '/manage/albums/upload' },
    { from: '/privacy-policy', to: '/about/privacy-policy' },
    { from: '/about/terms', to: '/about/terms-of-service' },
    { from: '/about/privacy', to: '/about/privacy-policy' },

    /* Catch all */
    {
        exact: true,
        path: '*',
        render(props) {
            const route = {
                status: 404
            };
            const allProps = {
                ...props,
                route,
                page: 'NotFound'
            };

            if (props.staticContext) {
                props.staticContext.status = route.status;
            }

            return renderComponent(allProps);
        }
    }
];

export default () => {
    const AppContainer = require('./AppContainer').default;

    return (
        <AppContainer>
            <Switch>
                {routeConfig.map((config, i) => {
                    if (config.to && config.from) {
                        return <Redirect key={i} {...config} />;
                    }

                    if (config.sitemap === true && config.path.includes(':')) {
                        throw new Error(`You have specified "sitemap: true" but the path "${
                            config.path
                        }"
contains a dynamic url param. This will force the exact path, including the dynamic
param, to be included in the sitemap which is not useful to crawlers. Firstly, remove
"sitemap: true" for the path ${
                            config.path
                        }. Secondly, if you think these dynamic
paths should to be included in the sitemap, check out the sitemap worker and add
your custom partial to take care of these dynamic routes. The last option is just to specify an exact string for the "sitemap" key. An example of this was done for the /trending-now path that has optional timePeriod/genre params.`);
                    }

                    return <Route key={i} {...config} />;
                })}
            </Switch>
        </AppContainer>
    );
};
