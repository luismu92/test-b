import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import styles from './SocialButton.module.scss';

export default class SocialButton extends Component {
    static propTypes = {
        network: PropTypes.string,
        provider: PropTypes.string,
        onClick: PropTypes.func,
        icon: PropTypes.element
    };

    render() {
        const { icon, network } = this.props;

        const klass = classnames({
            [styles.facebook]: network === 'facebook',
            [styles.twitter]: network === 'twitter',
            [styles.google]: network === 'google',
            [styles.apple]: network === 'apple'
        });

        let buttonIcon;

        if (icon) {
            buttonIcon = <span className={styles.icon}>{icon}</span>;
        }

        return (
            <button
                className={klass}
                data-provider={this.props.provider}
                title={`Continue with ${network}`}
                onClick={this.props.onClick}
            >
                {buttonIcon}
            </button>
        );
    }
}
