import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ActionSheetContainer from '../ActionSheetContainer';

import CartIcon from '../icons/shopping-cart';

export default class SongActionSheet extends Component {
    static propTypes = {
        actionItem: PropTypes.object.isRequired,
        onActionItemClick: PropTypes.func
    };

    render() {
        const { actionItem, onActionItemClick } = this.props;
        const actions = [
            'playlist',
            'repost',
            'favorite',
            'comments',
            'share',
            'download'
        ];

        if (actionItem.buy_link) {
            actions.push({
                icon: [<CartIcon key="buy" />],
                href: actionItem.buy_link,
                action: 'buy'
            });
        }

        return (
            <ActionSheetContainer
                noFlex={false}
                actions={actions}
                actionItem={actionItem}
                className="u-spacing-top-15 u-spacing-bottom-15"
                onActionItemClick={onActionItemClick}
            />
        );
    }
}
