import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import { nextPage, fetchSongList } from '../redux/modules/music';

import MusicPageMeta from 'components/MusicPageMeta';
import { isCurrentMusicItem, getQueueIndexForSong } from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';
import { hideDrawer } from '../redux/modules/drawer';

import {
    getSongInfo,
    setSongForSongPage,
    resetSong
} from '../redux/modules/music';
import {
    hidePlayer,
    showPlayer,
    pause,
    editQueue,
    play
} from '../redux/modules/player';
import {
    setGlobalBackground,
    setBannerDeepLink
} from '../redux/modules/global';
import {
    MODAL_TYPE_SONG_INFO,
    showModal,
    hideModal
} from '../redux/modules/modal';
import { trackAction } from '../redux/modules/stats';
import { getArtistUploads } from '../redux/modules/artist/uploads';
import { reset } from '../redux/modules/promoKey';

import NotFound from '../NotFound';
import SongPage from './SongPage';

class SongPageContainer extends Component {
    static propTypes = {
        modal: PropTypes.object,
        song: PropTypes.object,
        match: PropTypes.object,
        currentUser: PropTypes.object,
        artistUploads: PropTypes.object,
        player: PropTypes.object,
        history: PropTypes.object,
        dispatch: PropTypes.func,
        location: PropTypes.object,
        errors: PropTypes.array,
        statsToken: PropTypes.string,
        songs: PropTypes.object
    };

    constructor(props) {
        super(props);

        const query = parse(props.location.search);

        this._query = {
            key: query.key
        };
    }

    componentDidMount() {
        const { song, dispatch, player } = this.props;
        const currentSong = player.currentSong;

        this.setSiteWrap(song);

        if (song && currentSong && song.id === currentSong.id) {
            dispatch(hidePlayer());
        }

        // This can happen if we just refresh a song page
        if (!currentSong && song) {
            const queue = [song];

            dispatch(
                editQueue(queue, {
                    append: false
                })
            );
        }

        this.setDeepLink(song);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, history, location, player } = this.props;
        const { params: prevParams } = prevProps.match;
        const { params: currentParams } = this.props.match;
        const prevPageSong = prevProps.song || {};
        const currentPageSong = this.props.song || {};
        const currentPlayerSong = this.props.player.currentSong || {};

        const changedArtist =
            prevParams.artistId !== currentParams.artistId &&
            currentParams.artistId;
        const changedSong =
            prevParams.songSlug !== currentParams.songSlug &&
            currentParams.songSlug;
        const changedSongId = prevPageSong.id !== currentPageSong.id;
        const changedUserToken =
            this.props.currentUser.token &&
            prevProps.currentUser.token !== this.props.currentUser.token;
        const songExists = this.props.song && this.props.player.currentSong;

        if (
            !player.hidden &&
            currentPlayerSong.id === currentPageSong.id &&
            songExists
        ) {
            dispatch(hidePlayer());
        } else if (
            player.hidden &&
            currentPlayerSong.id !== currentPageSong.id &&
            songExists
        ) {
            dispatch(showPlayer());
        }

        if (changedSongId && this.props.song) {
            dispatch(setSongForSongPage(this.props.song));

            const newPath = `/song/${currentPageSong.uploader.url_slug}/${
                currentPageSong.url_slug
            }${location.search}`;

            if (prevPageSong !== null && newPath !== location.pathname) {
                history.replace(newPath);
            }
            this.setSiteWrap(currentPageSong);
            this.setDeepLink(this.props.song);
        }

        if (changedArtist || changedSong || changedUserToken) {
            const options = {
                key: parse(location.search).key,
                token: this.props.currentUser.token,
                secret: this.props.currentUser.secret
            };

            dispatch(
                getSongInfo(
                    currentParams.artistId,
                    currentParams.songSlug,
                    options
                )
            );
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(showPlayer());
        dispatch(setGlobalBackground(null));
        dispatch(hideModal());
        dispatch(hideDrawer());
        dispatch(resetSong());
        dispatch(reset());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handlePlayClick = () => {
        const { song, dispatch, player, artistUploads } = this.props;
        const currentSong = player.currentSong;
        const isCurrentSong = isCurrentMusicItem(currentSong, song);

        if (isCurrentSong) {
            if (!player.paused) {
                dispatch(pause());
            } else {
                dispatch(play());
            }
            return;
        }

        const queueIndex = getQueueIndexForSong(song, player.queue, {
            currentQueueIndex: player.queueIndex
        });

        if (queueIndex !== -1) {
            dispatch(play(queueIndex));
            return;
        }

        const filteredArtistUploads = artistUploads.list.filter((item) => {
            return item.id !== song.id;
        });
        const queue = [song].concat(filteredArtistUploads);
        const { queue: newQueue } = dispatch(editQueue(queue));
        const newIndex = getQueueIndexForSong(song, newQueue);

        dispatch(play(newIndex));
    };

    handleActionItemClick = (action) => {
        const { dispatch, statsToken, song } = this.props;

        switch (action) {
            case 'info':
                dispatch(showModal(MODAL_TYPE_SONG_INFO));
                break;

            case 'play':
                this.handlePlayClick();
                break;

            case 'download':
                dispatch(trackAction(statsToken, 'dl', song.id));
                break;

            default:
                break;
        }
    };

    handleInfoOpen = () => {
        this.handleActionItemClick('info');
    };

    handleInfoClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(fetchSongList());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setDeepLink(song) {
        if (!song) {
            return;
        }

        const link = `audiomack://song/${song.uploader.url_slug}/${
            song.url_slug
        }`;

        this.props.dispatch(
            setBannerDeepLink(link, {
                song: song.id,
                key: parse(this.props.location.search).key
            })
        );
    }

    setSiteWrap(song) {
        const { dispatch } = this.props;

        if (!song) {
            return;
        }

        const notSuspended =
            song.status !== 'suspended' &&
            song.status !== 'takedown' &&
            song.status !== 'unplayable';
        const image = notSuspended ? song.image_base || song.image : null;

        dispatch(setGlobalBackground(image));
    }

    render() {
        const {
            song,
            player,
            modal,
            history,
            location,
            errors,
            currentUser,
            artistUploads
        } = this.props;
        const { currentSong, paused, currentTime, duration } = player;
        const isCurrentSong = isCurrentMusicItem(currentSong, song);

        if (!song) {
            if (errors.length) {
                return <NotFound errors={errors} type="song" />;
            }

            return null;
        }

        const realDuration = song.duration || duration;

        return (
            <Fragment>
                <MusicPageMeta
                    musicItem={song}
                    currentUser={currentUser}
                    location={location}
                />
                <SongPage
                    song={song}
                    paused={paused}
                    duration={realDuration}
                    history={history}
                    location={location}
                    currentTime={currentTime}
                    currentUser={currentUser}
                    isCurrentSong={isCurrentSong}
                    onInfoOpen={this.handleInfoOpen}
                    onInfoClose={this.handleInfoClose}
                    onPlayClick={this.handlePlayClick}
                    artistUploads={artistUploads}
                    onActionItemClick={this.handleActionItemClick}
                    infoModalActive={modal.type === MODAL_TYPE_SONG_INFO}
                    songs={this.props.songs}
                    onNextPage={this.handleNextPage}
                    songKey={this._query.key}
                    singleCommentUuid={
                        parse(this.props.location.search).comment
                    }
                    singleCommentThread={
                        parse(this.props.location.search).thread
                    }
                />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        song: state.music.song,
        player: state.player,
        currentUser: state.currentUser,
        errors: state.music.errors,
        statsToken: state.stats.statsToken,
        artistUploads: state.artistUploads,
        songs: state.music
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(SongPageContainer, [
            (params, query, props) => {
                const { currentUser } = props;
                const { token, secret } = currentUser;
                const options = {};

                options.key = query.key;
                options.token = token;
                options.secret = secret;

                return getSongInfo(params.artistId, params.songSlug, options);
            },
            (params) => getArtistUploads(params.artistId, { limit: 10 }),
            () => fetchSongList()
        ])
    )
);
