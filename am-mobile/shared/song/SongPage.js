import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import { parse } from 'query-string';

import Truncate from 'components/Truncate';
import WaveformContainer from 'components/WaveformContainer';
import {
    convertSecondsToTimecode,
    getUploader,
    buildDynamicImage
} from 'utils/index';
import { renderFeaturingLinks } from 'utils/index';

import SongInfoModal from '../modal/SongInfoModal';
import Avatar from '../Avatar';
import SongActionSheet from '../song/SongActionSheet';
import AdminActionsContainer from '../AdminActionsContainer';
import ListContainer from '../list/ListContainer';

import PlayIcon from '../icons/play';
import PauseIcon from '../icons/pause';
import InfoIcon from 'icons/info';
import FollowButtonContainer from '../FollowButtonContainer';

import ListenOnAppButton from '../components/ListenOnAppButton';
import SongVideo from '../song/SongVideo.js';
import Verified from 'components/Verified';
import CommentsWrapper from '../components/CommentsWrapper';
import { KIND_SONG } from 'constants/comment';
import GeoNotice from '../components/GeoNotice';

export default class SongPage extends Component {
    static propTypes = {
        paused: PropTypes.bool,
        currentTime: PropTypes.number,
        duration: PropTypes.number,
        infoModalActive: PropTypes.bool,
        history: PropTypes.object,
        location: PropTypes.object,
        artistUploads: PropTypes.object,
        song: PropTypes.object,
        onPlayClick: PropTypes.func,
        onInfoOpen: PropTypes.func,
        onInfoClose: PropTypes.func,
        onActionItemClick: PropTypes.func,
        currentUser: PropTypes.object,
        isCurrentSong: PropTypes.bool,
        songs: PropTypes.object,
        onNextPage: PropTypes.func,
        songKey: PropTypes.string,
        singleCommentUuid: PropTypes.string,
        singleCommentThread: PropTypes.string
    };

    static defaultProps = {
        onInfoClose() {},
        onPlayClick() {}
    };

    getWaveformData(song) {
        let data = [];

        try {
            data = JSON.parse(song.volume_data);
        } catch (e) {
            return data;
        }

        return data;
    }

    displayAdminActions(currentUser, song) {
        if (!currentUser.isLoggedIn || !currentUser.isAdmin) {
            return null;
        }

        return (
            <AdminActionsContainer
                item={song}
                onActionItemClick={this.props.onActionItemClick}
            />
        );
    }

    renderEmptyState() {
        return (
            <div className="empty-state">
                <p>This artist has no other uploads.</p>
            </div>
        );
    }

    renderGeoNotice() {
        const { song } = this.props;
        const { geo_restricted: geoRestricted } = song;

        if (!geoRestricted) {
            return null;
        }

        return (
            <GeoNotice withBackdrop>
                The rightsholder has not made this content available in your
                country.
            </GeoNotice>
        );
    }

    renderSongHeader() {
        const {
            song,
            currentTime,
            paused,
            duration,
            isCurrentSong,
            currentUser,
            location,
            songKey
        } = this.props;

        if (
            song.status === 'takedown' ||
            song.status === 'suspended' ||
            song.status === 'unplayable'
        ) {
            return null;
        }

        const { geo_restricted: geoRestricted } = song;

        const size = 180;
        const retina = buildDynamicImage(song.image_base || song.image, {
            width: size * 2,
            height: size * 2,
            max: true
        });
        const avatarProps = {
            image: buildDynamicImage(song.image_base || song.image, {
                width: size,
                height: size,
                max: true
            }),
            srcSet: `${retina} 2x`,
            size: size,
            rounded: false,
            center: true
        };

        let featuring;

        if (song.featuring) {
            featuring = (
                <p className="u-fs-16 u-fw-600 u-ls-n-04 u-lh-14 u-text-shadow">
                    {renderFeaturingLinks(song.featuring)}
                </p>
            );
        }

        const verified = <Verified user={song.uploader} size={15} />;

        const elapsedDisplay = convertSecondsToTimecode(
            isCurrentSong ? currentTime : 0
        );
        const durationDisplay = convertSecondsToTimecode(duration || 0);

        let showPaused = false;

        if (!paused && isCurrentSong) {
            showPaused = true;
        }

        const waveformClass = classnames(
            'waveform-wrap song-waveform-wrap u-d-flex u-d-flex--align-center',
            'u-spacing-bottom-20 u-padding-x-10',
            {
                'song-waveorm-wrap__geo-restricted': geoRestricted
            }
        );

        const waveformPlayButtonClass = classnames(
            'song-play waveform-play-button',
            {
                'song-play--paused': showPaused
            }
        );

        const avatarPlayButtonClass = classnames('avatar__play-button', {
            'avatar__play-button--paused': showPaused
        });

        const canvasProps = {
            role: 'progressbar',
            'aria-label': 'played',
            'aria-valuemin': '0',
            'aria-valuemax': duration,
            'aria-valuenow': currentTime,
            'aria-valuetext': `${Math.round(currentTime)} seconds played`
        };

        // @todo Merge these into one
        let waveformPlayButton = (
            <button
                className={waveformPlayButtonClass}
                onClick={this.props.onPlayClick}
            >
                <PlayIcon
                    title={`Play ${song.title}`}
                    hidden={showPaused}
                    className="song-play__play"
                />
                <PauseIcon
                    title={`Pause ${song.title}`}
                    hidden={!showPaused}
                    className="song-play__pause"
                />
            </button>
        );

        let avatarPlayButton = (
            <button
                className={avatarPlayButtonClass}
                onClick={this.props.onPlayClick}
            >
                <PlayIcon
                    title={`Play ${song.title}`}
                    hidden={showPaused}
                    className="song-play__play"
                />
                <PauseIcon
                    title={`Pause ${song.title}`}
                    hidden={!showPaused}
                    className="song-play__pause"
                />
            </button>
        );

        const songActionSheet = !geoRestricted && (
            <SongActionSheet
                actionItem={song}
                onActionItemClick={this.props.onActionItemClick}
            />
        );

        let waveform = (
            <div className={waveformClass}>
                {waveformPlayButton}
                <div className="song-waveform-container waveform waveform--adjacent-button u-pos-relative">
                    <span className="waveform__time waveform__time--elapsed u-fs-12 u-fw-700 u-ls-n-05 u-pos-absolute">
                        {elapsedDisplay}
                    </span>
                    <WaveformContainer
                        musicItem={song}
                        canvasProps={canvasProps}
                        color="#3a3a38"
                    />
                    <span className="waveform__time waveform__time--duration u-fs-12 u-fw-700 u-ls-n-05 u-pos-absolute">
                        {durationDisplay}
                    </span>
                </div>
                {this.renderGeoNotice()}
            </div>
        );

        const followButton = (
            <FollowButtonContainer
                artist={song.uploader}
                className="song-container__follow-icon"
                currentUser={currentUser}
            />
        );

        if (song.released > Math.floor(Date.now() / 1000) && !songKey) {
            waveformPlayButton = null;
            avatarPlayButton = null;
            waveform = null;
        }

        return (
            <header className="u-pos-relative music-header music-header--bordered u-spacing-bottom-20">
                <button
                    className="music-header__info"
                    onClick={this.props.onInfoOpen}
                    data-action="info"
                >
                    <InfoIcon />
                </button>
                <div className="avatar-wrap u-pos-relative">
                    <Avatar {...avatarProps} />
                    {avatarPlayButton}
                </div>
                <h1>
                    <span className="song-container__artist u-fs-18 u-ls-n-05 u-lh-15 u-text-shadow u-spacing-top-10 u-d-block">
                        {song.artist}
                    </span>
                    <Truncate
                        tagName="span"
                        className="song-container__title u-d-block u-fs-18 u-ls-n-05 u-fw-700 u-lh-15 u-spacing-top-10"
                        lines={2}
                        text={song.title}
                    />
                </h1>
                {featuring}
                <p className="song-container__uploader u-spacing-top-5 u-fs-14 u-lh-16 u-ls-n-04 u-text-shadow">
                    By{' '}
                    <Link to={`/artist/${song.uploader.url_slug}`}>
                        {song.uploader.name}
                    </Link>{' '}
                    {verified} {followButton}
                </p>

                {songActionSheet}
                {waveform}
                {!geoRestricted && (
                    <ListenOnAppButton
                        musicItem={song}
                        keyPassword={parse(location.search).key}
                    />
                )}
            </header>
        );
    }

    render() {
        const {
            infoModalActive,
            song,
            currentUser,
            songs,
            onNextPage,
            location,
            history,
            artistUploads,
            songKey,
            singleCommentUuid,
            singleCommentThread
        } = this.props;

        const takenDown =
            song.status === 'takedown' || song.status === 'suspended';

        let unavailableText;

        if (song.released > Math.floor(Date.now() / 1000) && !songKey) {
            unavailableText = (
                <div className="u-text-center u-spacing-bottom-15">
                    <span className="alert alert--not-released alert--dark">
                        This song has not yet been released.
                    </span>
                </div>
            );
        }

        if (takenDown) {
            unavailableText = (
                <p className="outer-padding no-tracks-found u-fw-600 u-padding-20 u-spacing-x-20 u-spacing-y-40 u-text-center">
                    This song has been removed due to a DMCA Complaint. Discover
                    your next favorite song on our{' '}
                    <Link to="/songs/week">Top Songs</Link>,{' '}
                    <Link to="/albums/week">Top Albums</Link> or{' '}
                    <Link to="/playlists/browse">Top Playlists Charts!</Link>
                </p>
            );
        }

        if (song.status === 'unplayable') {
            unavailableText = (
                <p className="outer-padding no-tracks-found u-fw-600 u-padding-20 u-spacing-x-20 u-spacing-y-40 u-text-center">
                    Streaming for this song has been Disabled at the source.
                    Discover your next favorite song on our{' '}
                    <Link to="/songs/week">Top Songs</Link>,{' '}
                    <Link to="/albums/week">Top Albums</Link> or{' '}
                    <Link to="/playlists/browse">Top Playlists Charts!</Link>
                </p>
            );
        }

        let moreSongs = {
            data: artistUploads.list.filter((item) => {
                return item.id !== song.id;
            }),
            onLastPage: artistUploads.onLastPage,
            loading: artistUploads.loading
        };

        if (takenDown) {
            moreSongs = songs;
        }

        let moreFrom;

        if (moreSongs.data.length) {
            moreFrom = (
                <h5 className="u-fs-13 u-fw-800 u-ls-n-06 u-tt-uppercase u-spacing-bottom-10 u-padding-x-10">
                    More from {getUploader(song).name}
                </h5>
            );
        }

        if (takenDown) {
            moreFrom = (
                <h5 className="u-fs-13 u-fw-800 u-ls-n-06 u-tt-uppercase u-spacing-bottom-10 u-padding-x-10">
                    Trending now on Audiomack
                </h5>
            );
        }

        return (
            <div className="song-container u-padding-top-20 u-pos-relative">
                {this.displayAdminActions(currentUser, song)}
                {this.renderSongHeader()}
                {unavailableText}

                <SongVideo song={song} />

                {moreFrom}
                <ListContainer
                    location={location}
                    songs={moreSongs}
                    onNextPage={takenDown ? onNextPage : null}
                    emptyState={this.renderEmptyState()}
                    className="u-padding-0"
                />
                <SongInfoModal
                    history={history}
                    active={infoModalActive}
                    onClose={this.props.onInfoClose}
                    song={song}
                />
                <div className="column small-24">
                    <CommentsWrapper
                        item={song}
                        kind={KIND_SONG}
                        id={song.id}
                        className="u-spacing-top-30 u-padding-x-20"
                        total={song.stats.comments}
                        isMobile
                        history={this.props.history}
                        singleCommentUuid={singleCommentUuid}
                        singleCommentThread={singleCommentThread}
                    />
                </div>
            </div>
        );
    }
}
