import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Cookie from 'js-cookie';
import { parse } from 'query-string';

import ArtistPageMeta from 'components/ArtistPageMeta';
import connectDataFetchers from 'lib/connectDataFetchers';
import { cookies } from 'constants/index';

import { setActiveMarker, NAV_MARKER_PROFILE } from '../redux/modules/nav';
import { follow, unfollow } from '../redux/modules/user/index';
import {
    getPlaylists,
    nextPage as nextUserPlaylistsPage
} from '../redux/modules/user/playlists';
import {
    getUserFeed,
    nextPage as nextUserFeedPage,
    reset as resetUserFeedPage
} from '../redux/modules/user/feed';
import { showModal, MODAL_TYPE_GHOST_CLAIM } from '../redux/modules/modal';
import { setBannerDeepLink } from '../redux/modules/global';
// import { getUserUploads, nextPage as nextUserUploadsPage, reset as resetUserUploadsPage } from '../redux/modules/user/uploads';
import { setLastDisplayTime } from '../redux/modules/ad';
import { getArtist } from '../redux/modules/artist/index';
import {
    getArtistFeed,
    nextPage as nextFeedPage,
    reset as resetFeedPage
} from '../redux/modules/artist/feed';
import {
    getArtistUploads,
    nextPage as nextUploadsPage,
    reset as resetUploadsPage
} from '../redux/modules/artist/uploads';
import {
    getArtistFavorites,
    nextPage as nextFavoritesPage,
    reset as resetFavoritesPage
} from '../redux/modules/artist/favorites';
import {
    getArtistPlaylists,
    nextPage as nextPlaylistsPage,
    reset as resetPlaylistsPage
} from '../redux/modules/artist/playlists';
import {
    getArtistFollowers,
    nextPage as nextFollowersPage,
    reset as resetFollowersPage
} from '../redux/modules/artist/followers';
import {
    getArtistFollowing,
    nextPage as nextFollowingPage,
    reset as resetFollowingPage
} from '../redux/modules/artist/following';
import { addItems, showDrawer } from '../redux/modules/drawer';
import { getPinned } from '../redux/modules/artist/pinned';

import Avatar from '../Avatar';
import ContextSwitcher from '../widgets/ContextSwitcher';
import ListContainer from '../list/ListContainer';
import UserList from '../list/UserList';
import ArtistInfoModal from '../modal/ArtistInfoModal';
import FollowButtonContainer from '../FollowButtonContainer';
import NotificationIcon from '../NotificationIcon';
import InfoIcon from '../icons/info';
import SearchIcon from '../icons/search';
import SettingsIcon from '../icons/cog';
import InboxOutIcon from '../icons/inbox-out';
import NotFound from '../NotFound';
import Verified from '../../../am-shared/components/Verified';

const DEFAULT_CONTEXT = 'uploads';

function makeContextRequest(params, props) {
    const { currentUser } = props;
    const { artistId } = params;
    const ownsPage =
        currentUser.isLoggedIn && currentUser.profile.url_slug === artistId;
    const page = parseInt(params.page, 10) || 1;
    // default to uploads since its first in the context switcher list
    const context = params.context || DEFAULT_CONTEXT;

    let fn = () => {}; //eslint-disable-line

    switch (context) {
        case 'uploads':
            fn = () => getArtistUploads(artistId, { page });

            /* if (ownsPage) {
                fn = () => getUserUploads({ page });
            } */
            break;

        case 'favorites':
            fn = () => getArtistFavorites(artistId, { page });
            break;

        case 'playlists':
            fn = () => getArtistPlaylists(artistId, { page });

            if (ownsPage) {
                fn = () => getPlaylists({ page });
            }
            break;

        case 'followers':
            fn = () => getArtistFollowers(artistId, { page });
            break;

        case 'following':
            fn = () => getArtistFollowing(artistId, { page });
            break;

        case 'feed':
            fn = () => getArtistFeed(artistId, { page });

            if (ownsPage) {
                fn = () => getUserFeed({ page });
            }
            break;

        default:
            console.warn('No fn implemented for context:', context);
            break;
    }

    return fn(artistId);
}

class ArtistContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        currentUserFeed: PropTypes.object,
        currentUserNotifications: PropTypes.object,
        currentUserPlaylists: PropTypes.object,
        ad: PropTypes.object,
        match: PropTypes.object,
        location: PropTypes.object,
        artist: PropTypes.object.isRequired,
        artistFeed: PropTypes.object,
        artistUploads: PropTypes.object,
        artistPlaylists: PropTypes.object,
        artistFollowers: PropTypes.object,
        artistFollowing: PropTypes.object,
        artistFavorites: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            showingInfo: false,
            GhostClaimModal: null
        };
    }

    componentDidMount() {
        const { dispatch, ad, artist, match } = this.props;

        // When landing on the account page, we allow 12 minutes to go by
        // before displaying an ad
        if (!ad.lastDisplayed && !Cookie.get(cookies.lastAdDisplayTime)) {
            dispatch(setLastDisplayTime(Date.now()));
        }

        const link = this.getDeepLink(this.props);

        dispatch(
            setBannerDeepLink(link, {
                key: parse(this.props.location.search).key
            })
        );

        this.loadGhostClaimModal(match.params.code, match.params.ee, artist);
    }

    componentDidUpdate(prevProps) {
        const { currentUser, dispatch } = this.props;

        if (!prevProps.artist.profile && this.props.artist.profile) {
            this.loadGhostClaimModal(
                this.props.match.params.code,
                this.props.match.params.ee,
                this.props.artist
            );
        }

        if (
            prevProps.match.params.artistId !== this.props.match.params.artistId
        ) {
            dispatch(getArtist(this.props.match.params.artistId));
            dispatch(getPinned(this.props.match.params.artistId));
            this.clearArtistLists();
        }

        if (
            prevProps.match.params.context !==
                this.props.match.params.context ||
            currentUser.isLoggedIn !== this.props.currentUser.isLoggedIn ||
            prevProps.match.params.artistId !==
                this.props.match.params.artistId ||
            prevProps.match.params.page !== this.props.match.params.page
        ) {
            const link = this.getDeepLink(this.props);

            dispatch(
                setBannerDeepLink(link, {
                    key: parse(this.props.location.search).key
                })
            );
            dispatch(makeContextRequest(this.props.match.params, this.props));
        }
    }

    componentWillUnmount() {
        this.clearArtistLists();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleInfoClick = () => {
        this.setState({
            showingInfo: true
        });
    };

    handleInfoClose = () => {
        this.setState({
            showingInfo: false
        });
    };

    handleFollowClick = (shouldFollow, artist) => {
        const { dispatch } = this.props;

        if (!artist) {
            console.warn('artist not found in follow button');
            return;
        }

        if (shouldFollow) {
            dispatch(follow(artist));
            return;
        }

        dispatch(unfollow(artist));
    };

    handleEditProfileClick = () => {
        const { dispatch } = this.props;
        const items = this.getAccountItems();

        dispatch(addItems(items));
        dispatch(showDrawer());
    };

    handleNextPage = () => {
        const {
            dispatch,
            match: { params }
        } = this.props;
        const context = params.context;
        const listData = this.getListDataForContext(context);

        if (!listData) {
            return;
        }

        dispatch(listData.nextPage());
        dispatch(listData.fetchContext());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getDeepLink(props) {
        const { context, artistId } = props.match.params;
        let urlSlug;

        switch (context) {
            case 'favorites':
            case 'playlists':
            case 'following':
            case 'followers':
                urlSlug = `/artist/${artistId}/${context}`;
                break;

            default:
                urlSlug = `/artist/${artistId}`;
        }

        const page = props.match.params.page;
        const pageSlug = page ? `/page/${page}` : '';
        let link = `audiomack:/${urlSlug}${pageSlug}`;

        if (props.location.search) {
            link += `?${props.location.search}`;
        }

        return link;
    }

    getContextSwitcherItems(context, artist, currentUser) {
        const urlPrefix = `/artist/${artist.profile.url_slug}/`;
        const contextItems = [
            {
                text: 'Uploads',
                href: `${urlPrefix}uploads`,
                active: context === 'uploads' || !context
            },
            {
                text: 'Favorites',
                href: `${urlPrefix}favorites`,
                active: context === 'favorites'
            },
            {
                text: 'Playlists',
                href: `${urlPrefix}playlists`,
                active: context === 'playlists'
            },
            {
                text: 'Following',
                href: `${urlPrefix}following`,
                active: context === 'following'
            }
        ];

        if (currentUser.isLoggedIn) {
            const ownsPage = artist.profile.id === currentUser.profile.id;

            if (ownsPage || currentUser.profile.is_admin) {
                contextItems.push({
                    text: 'Followers',
                    href: `${urlPrefix}followers`,
                    active: context === 'followers'
                });
            }
        }

        return contextItems;
    }

    getListDataForContext(context) {
        const {
            match: {
                params: { artistId }
            },
            currentUser
        } = this.props;
        const ownsPage =
            currentUser.isLoggedIn && currentUser.profile.url_slug === artistId;
        // default to uploads since its first in the context switcher list
        const paramContext = context || DEFAULT_CONTEXT;

        switch (paramContext) {
            case 'uploads': {
                const { artistUploads /* , currentUserUploads */ } = this.props;

                /* if (ownsPage) {
                    return {
                        data: currentUserUploads.list,
                        fetchContext: () => getUserUploads(),
                        nextPage: nextUserUploadsPage,
                        onLastPage: currentUserUploads.onLastPage,
                        loading: currentUserUploads.loading
                    };
                } */

                return {
                    data: artistUploads.list,
                    page: artistUploads.page,
                    fetchContext: () => getArtistUploads(artistId),
                    nextPage: nextUploadsPage,
                    onLastPage: artistUploads.onLastPage,
                    loading: artistUploads.loading
                };
            }

            case 'favorites': {
                const { artistFavorites } = this.props;

                return {
                    data: artistFavorites.list,
                    page: artistFavorites.page,
                    loading: artistFavorites.loading,
                    onLastPage: artistFavorites.onLastPage,
                    fetchContext: () => getArtistFavorites(artistId),
                    nextPage: nextFavoritesPage
                };
            }
            case 'playlists': {
                const { artistPlaylists, currentUserPlaylists } = this.props;

                if (ownsPage) {
                    return {
                        data: currentUserPlaylists.list,
                        page: currentUserPlaylists.page,
                        loading: currentUserPlaylists.loading,
                        onLastPage: currentUserPlaylists.onLastPage,
                        fetchContext: () => getPlaylists(),
                        nextPage: nextUserPlaylistsPage
                    };
                }

                return {
                    data: artistPlaylists.list,
                    page: artistPlaylists.page,
                    loading: artistPlaylists.loading,
                    onLastPage: artistPlaylists.onLastPage,
                    fetchContext: () => getArtistPlaylists(artistId),
                    nextPage: nextPlaylistsPage
                };
            }

            case 'followers': {
                const { artistFollowers } = this.props;

                return {
                    data: artistFollowers.list,
                    page: artistFollowers.page,
                    loading: artistFollowers.loading,
                    onLastPage: artistFollowers.onLastPage,
                    fetchContext: () => getArtistFollowers(artistId),
                    nextPage: nextFollowersPage
                };
            }

            case 'following': {
                const { artistFollowing } = this.props;

                return {
                    data: artistFollowing.list,
                    page: artistFollowing.page,
                    loading: artistFollowing.loading,
                    onLastPage: artistFollowing.onLastPage,
                    fetchContext: () => getArtistFollowing(artistId),
                    nextPage: nextFollowingPage
                };
            }

            case 'feed': {
                const { artistFeed, currentUserFeed } = this.props;

                if (ownsPage) {
                    return {
                        data: currentUserFeed.list,
                        page: currentUserFeed.page,
                        loading: currentUserFeed.loading,
                        onLastPage: currentUserFeed.onLastPage,
                        fetchContext: () => getUserFeed(),
                        nextPage: nextUserFeedPage
                    };
                }

                return {
                    data: artistFeed.list,
                    page: artistFeed.page,
                    loading: artistFeed.loading,
                    onLastPage: artistFeed.onLastPage,
                    fetchContext: () => getArtistFeed(artistId),
                    nextPage: nextFeedPage
                };
            }

            default:
                console.warn('No props implemented for context:', paramContext);
                return null;
        }
    }

    getListTypeFromContext(context) {
        if (context === 'playlists') {
            return 'playlist';
        }

        if (context === 'following' || context === 'followers') {
            return 'user';
        }

        return 'song';
    }

    getAvatar(image) {
        return <Avatar image={image} size={90} />;
    }

    getFollowerCount(artist) {
        const count = artist.profile.followers_count;
        let text = 'Followers';

        if (count === 1) {
            text = 'Follower';
        }

        return (
            <span>
                {count} {text}
            </span>
        );
    }

    getFollowingCount(artist) {
        const count = artist.profile.following_count;
        const text = 'Following';

        return (
            <span>
                <Link to={`/artist/${artist.profile.url_slug}/following`}>
                    {count}{' '}
                </Link>
                {text}
            </span>
        );
    }

    getAccountItems() {
        return [
            {
                text: 'View Profile Info',
                action: 'view-profile',
                handler: this.handleInfoClick
            },
            {
                text: 'Edit Profile',
                action: 'edit-profile',
                href: '/edit/profile',
                buttonProps: {
                    'data-testid': 'editProfile'
                }
            },
            {
                text: 'Manage Uploads',
                action: 'view-dashboard',
                href: '/dashboard',
                anchor: true
            },
            {
                text: 'Upload Songs',
                action: 'upload-songs',
                href: '/upload?fromMobile=1',
                anchor: true
            },
            {
                text: 'Sign Out',
                action: 'log-out',
                href: '/',
                buttonProps: {
                    'data-testid': 'logoutButton'
                }
            }
        ];
    }

    loadGhostClaimModal(code, encodedEmail, artist) {
        if (!artist.profile || !encodedEmail || !code) {
            return;
        }

        // if (this.state.GhostClaimModal || artist.profile.status !== 'ghost') {
        //     return;
        // }

        const { dispatch } = this.props;

        require.ensure([], (require) => {
            this.setState(
                {
                    GhostClaimModal: require('../modal/GhostClaimModal').default
                },
                () => {
                    dispatch(
                        showModal(MODAL_TYPE_GHOST_CLAIM, {
                            email: window.atob(
                                decodeURIComponent(encodedEmail)
                            ),
                            code,
                            name: artist.profile.name,
                            id: artist.profile.id,
                            urlSlug: artist.profile.url_slug
                        })
                    );
                }
            );
        });
    }

    clearArtistLists() {
        const { dispatch } = this.props;

        [
            resetUserFeedPage,
            // resetUserUploadsPage,
            resetFeedPage,
            resetUploadsPage,
            resetFavoritesPage,
            resetPlaylistsPage,
            resetFollowersPage,
            resetFollowingPage
        ].forEach((fn) => dispatch(fn()));
    }

    renderAvatar(ownsPage, artist) {
        let editProfileButton;

        if (ownsPage) {
            editProfileButton = (
                <button
                    className="artist-actions__action artist-actions__action--settings"
                    onClick={this.handleEditProfileClick}
                    data-testid="settingsButton"
                    aria-label="Your profile settings"
                >
                    <SettingsIcon />
                </button>
            );
        }

        return (
            <div className="avatar-wrap">
                {this.getAvatar(artist.profile.image)}
                {editProfileButton}
            </div>
        );
    }

    renderEmptyState(context = DEFAULT_CONTEXT) {
        let icon;
        let text;

        switch (context) {
            case 'uploads':
                icon = 'inbox-out';
                text = <p className="user-empty">You have no uploads.</p>;
                break;

            case 'favorites':
                icon = 'star';
                text = (
                    <p className="user-empty">
                        You have no favorites.
                        <br />
                        Play a song and hit the star
                        <br />
                        to add songs here.
                    </p>
                );
                break;

            case 'downloads':
                icon = 'inbox';
                text = <p className="user-empty">You have no downloads.</p>;
                break;

            case 'followers':
                icon = 'user';
                text = <p className="user-empty">You have no followers.</p>;
                break;

            case 'following':
                icon = 'user';
                text = (
                    <p className="user-empty">You are not following anyone.</p>
                );
                break;

            case 'playlists':
                icon = 'user';
                text = (
                    <p className="user-empty">
                        You haven’t created any playlists yet
                    </p>
                );
                break;

            default:
                break;
        }

        return (
            <div className="empty-state">
                <i className={`glyphicons ${icon}`} />
                {text}
            </div>
        );
    }

    renderUserActions(ownsPage, currentUser, artist) {
        let settingsOrInfoButton;
        let searchOrActionButton;
        let notificationIcon;

        if (ownsPage) {
            const number = this.props.currentUserNotifications.unseen;

            settingsOrInfoButton = null;

            searchOrActionButton = (
                <Link className="artist-actions__action" to="/account/search">
                    <SearchIcon />
                </Link>
            );

            notificationIcon = (
                <NotificationIcon
                    artist={artist}
                    className="artist-actions__action"
                    currentUser={currentUser}
                    unseen={number}
                />
            );
        } else {
            settingsOrInfoButton = (
                <button
                    className="artist-actions__action"
                    onClick={this.handleInfoClick}
                >
                    <InfoIcon title="Artist Info" />
                </button>
            );

            searchOrActionButton = (
                <FollowButtonContainer
                    artist={artist.profile}
                    className="artist-actions__action"
                    currentUser={currentUser}
                />
            );

            if (!currentUser) {
                searchOrActionButton = null;
            }

            notificationIcon = null;
        }

        return (
            <div className="artist-actions">
                {settingsOrInfoButton}
                {searchOrActionButton}
                {notificationIcon}
            </div>
        );
    }

    render() {
        const {
            artist,
            match: { params },
            currentUser
        } = this.props;

        // Wait until the fetch listed at the bottom of the file
        // grabs the user
        if (!artist.profile) {
            return null;
        }

        const ownsPage =
            currentUser.isLoggedIn &&
            currentUser.profile.id === artist.profile.id;
        const style = {
            backgroundImage: `url(${artist.profile.image})`
        };
        const { context } = params;

        if (context === 'followers' && currentUser.profile) {
            if (!ownsPage && !currentUser.profile.is_admin) {
                return <NotFound />;
            }
        } else if (context === 'followers') {
            return <NotFound />;
        }

        const listType = this.getListTypeFromContext(context);
        const contextItems = this.getContextSwitcherItems(
            context,
            artist,
            currentUser
        );
        const items = this.getListDataForContext(context);

        let itemList;

        if (listType === 'user' && items) {
            itemList = (
                <UserList
                    items={items.data}
                    loading={items.loading}
                    isLastPage={items.onLastPage}
                    onNextPage={this.handleNextPage}
                    emptyState={this.renderEmptyState(context)}
                />
            );
        } else if (items) {
            const shareOverlayProps = {
                hideStats: true
            };

            itemList = (
                <ListContainer
                    location={this.props.location}
                    songs={items}
                    listType={listType}
                    onNextPage={this.handleNextPage}
                    emptyState={this.renderEmptyState(context)}
                    shareOverlayProps={shareOverlayProps}
                />
            );
        }

        const check = <Verified user={artist.profile} size={20} />;

        let { GhostClaimModal } = this.state;

        if (GhostClaimModal) {
            GhostClaimModal = <GhostClaimModal />;
        }

        const uploadPromptButton = (
            <div className="u-spacing-top-20 u-spacing-bottom-10 u-text-center">
                <a
                    href="/upload?fromMobile=1"
                    rel="nofollow noopener"
                    target="_blank"
                    className="button button--has-icon button--slim button--flex"
                >
                    <InboxOutIcon className="button__icon" />
                    <span className="u-fs-12 u-ls-n-05 u-fw-700">
                        Upload your Music
                    </span>
                </a>
            </div>
        );

        return (
            <div className="artist-container">
                <ArtistPageMeta
                    defaultContext={DEFAULT_CONTEXT}
                    artistProfile={artist.profile}
                    artistContext={context || DEFAULT_CONTEXT}
                    page={items ? items.page : undefined}
                />
                <header style={style}>
                    <div className="artist-container__inner-header">
                        {this.renderAvatar(ownsPage, artist)}
                        <div className="artist-meta">
                            <h2 className="artist-meta__name">
                                {artist.profile.name} {check}
                            </h2>

                            <p className="artist-meta__follow">
                                {this.getFollowerCount(artist)}
                                {this.getFollowingCount(artist)}
                            </p>
                        </div>

                        {this.renderUserActions(
                            ownsPage,
                            currentUser.profile,
                            artist
                        )}
                    </div>
                </header>
                <ContextSwitcher items={contextItems} />
                {uploadPromptButton}
                {itemList}
                {GhostClaimModal}
                <ArtistInfoModal
                    active={this.state.showingInfo}
                    onClose={this.handleInfoClose}
                    artist={artist}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser,
        currentUserUploads: state.currentUserUploads,
        currentUserNotifications: state.currentUserNotifications,
        currentUserFeed: state.currentUserFeed,
        currentUserPlaylists: state.currentUserPlaylists,
        artist: state.artist,
        artistFeed: state.artistFeed,
        artistUploads: state.artistUploads,
        artistFavorites: state.artistFavorites,
        artistPlaylists: state.artistPlaylists,
        artistFollowers: state.artistFollowers,
        artistFollowing: state.artistFollowing,
        player: state.player
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(
    connectDataFetchers(ArtistContainer, [
        (params, query, props) => {
            const { artist, currentUser } = props;
            const ownsPage =
                artist.profile &&
                currentUser.isLoggedIn &&
                currentUser.profile.id === artist.profile.id;

            if (!ownsPage) {
                return null;
            }

            return setActiveMarker(NAV_MARKER_PROFILE);
        },
        (params) => getArtist(params.artistId),
        (params) => getPinned(params.artistId),
        (params, query, props) => makeContextRequest(params, props)
    ])
);
