/* global test, expect */

import { routeConfig } from '../routes';

test('should contain all the necessary upload routes', () => {
    const hasUpload = routeConfig.find((route) => route.path === '/');

    expect(hasUpload).toBeTruthy();
    expect(hasUpload.exact).toBe(true);
    expect(hasUpload.sitemap).toBe(true);
});
