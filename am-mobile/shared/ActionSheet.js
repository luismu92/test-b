import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { isCurrentMusicItem, getUploader } from 'utils/index';

import PlayIcon from './icons/play';
import PauseIcon from './icons/pause';
import HeartIcon from '../../am-shared/icons/heart';
import ReupIcon from './icons/retweet';
import PlusIcon from './icons/plus';
import CloseIcon from './icons/close';
import ShareIcon from './icons/share';
import DownloadIcon from 'icons/download';
import CommentIcon from 'icons/comment';

// Allow for easy passing of strings that map to common AM actions
const defaultTypes = {
    play: {
        label: 'Play',
        icon: [<PlayIcon key="play" />],
        action: 'play'
    },
    favorite: {
        label: 'Fav',
        icon: [<HeartIcon key="fav" />],
        action: 'favorite',
        stat: 'favorites-raw',
        ariaLabel: 'Add to favorites'
    },
    repost: {
        label: 'Re-Up',
        icon: [<ReupIcon key="Reup" />],
        action: 'repost',
        stat: 'reposts-raw',
        ariaLabel: 'Re-up'
    },
    playlist: {
        label: 'Playlist',
        icon: [<PlusIcon key="playlist" />],
        action: 'playlist',
        stat: 'playlists-raw',
        ariaLabel: 'Add to a playlist'
    },
    comments: {
        label: 'Comments',
        icon: [<CommentIcon key="comment" />],
        action: 'comments',
        ariaLabel: 'Comment'
    },
    bump: {
        label: 'Bump',
        icon: 'bump',
        action: 'bump'
    },
    share: {
        label: 'Share',
        icon: [<ShareIcon key="share" />],
        action: 'share',
        ariaLabel: 'Share'
    },
    download: {
        label: 'Download',
        icon: [<DownloadIcon key="download" />],
        action: 'download',
        ariaLabel: 'Download'
    },
    close: {
        label: 'Close',
        icon: [<CloseIcon key="close" />],
        action: 'close',
        ariaLabel: 'Close action menu'
    }
};

export default class ActionSheet extends Component {
    static propTypes = {
        actionItem: PropTypes.object.isRequired,
        actions: PropTypes.array.isRequired,
        currentUser: PropTypes.object,
        player: PropTypes.object,
        onActionClick: PropTypes.func,
        className: PropTypes.string,
        noFlex: PropTypes.bool,
        hideStats: PropTypes.bool,
        hideLabel: PropTypes.bool
    };

    static defaultProps = {
        noFlex: true,
        hideStats: true,
        hideLabel: true,
        actions: [],
        onActionClick() {}
    };

    shouldActivateAction(action, item, currentUser) {
        if (!currentUser.isLoggedIn) {
            return false;
        }

        if (action === 'favorite') {
            if (item.type === 'playlist') {
                return (
                    (currentUser.profile.favorite_playlists || []).indexOf(
                        item.id
                    ) !== -1
                );
            }

            return (
                (currentUser.profile.favorite_music || []).indexOf(item.id) !==
                -1
            );
        }

        if (action === 'repost') {
            return (currentUser.profile.reups || []).indexOf(item.id) !== -1;
        }

        return false;
    }

    renderButtons() {
        const {
            actionItem,
            currentUser,
            actions,
            hideStats,
            hideLabel,
            player
        } = this.props;
        const { stats = {} } = actionItem;

        return actions
            .map((action) => {
                return defaultTypes[action] || action;
            })
            .map((button, i) => {
                let action = button.action;
                let buttonIcon = button.icon;

                if (
                    !player.paused &&
                    action === 'play' &&
                    isCurrentMusicItem(player.currentSong, actionItem)
                ) {
                    action = 'pause';
                    label = 'Pause';
                    buttonIcon = [<PauseIcon key="pause" />];
                }

                if (
                    action === 'repost' &&
                    currentUser.isLoggedIn &&
                    actionItem &&
                    currentUser.profile.id === getUploader(actionItem).id
                ) {
                    return null;
                }

                if (
                    actionItem.private === 'yes' &&
                    (action === 'favorite' || action === 'playlist')
                ) {
                    return null;
                }

                let label = (
                    <span className="action-sheet__label">{button.label}</span>
                );

                if (button.stat && !hideStats) {
                    label = (
                        <span className="action-sheet__label">
                            ({stats[button.stat] || 0})
                        </span>
                    );
                }

                if (action === 'close') {
                    label = (
                        <span className="action-sheet__label action-sheet__label--close">
                            Close
                        </span>
                    );
                }

                if (hideLabel) {
                    label = null;
                }

                let buttonProps;

                if (action === 'comments' && actionItem.stats.comments >= 1) {
                    const number =
                        actionItem.stats.comments > 99
                            ? '99+'
                            : actionItem.stats.comments;

                    buttonProps = {
                        'data-count': number
                    };
                }

                const buttonClass = classnames(
                    `action-sheet__button action-sheet__button--${action}`,
                    {
                        'action-sheet__button--active': this.shouldActivateAction(
                            action,
                            actionItem,
                            currentUser
                        ),
                        'action-sheet__button--flex': !this.props.noFlex
                    }
                );

                if (button.href) {
                    if (button.href.match(/^http(s)?:\/\//)) {
                        return (
                            <a
                                href={button.href}
                                className={buttonClass}
                                key={i}
                                data-action={action}
                                onClick={this.props.onActionClick}
                                aria-label={button.ariaLabel || button.label}
                            >
                                {buttonIcon}
                                {label}
                            </a>
                        );
                    }

                    return (
                        <Link
                            to={button.href}
                            className={buttonClass}
                            key={i}
                            data-action={button.action}
                            aria-label={button.ariaLabel || button.label}
                        >
                            {buttonIcon}
                            {label}
                        </Link>
                    );
                }

                return (
                    <button
                        className={buttonClass}
                        key={i}
                        data-action={button.action}
                        onClick={this.props.onActionClick}
                        aria-label={button.ariaLabel || button.label}
                        {...buttonProps}
                    >
                        {buttonIcon}
                        {label}
                    </button>
                );
            });
    }

    render() {
        const klass = classnames('action-sheet', {
            'action-sheet--flex': !this.props.noFlex,
            [this.props.className]: this.props.className
        });

        return <div className={klass}>{this.renderButtons()}</div>;
    }
}
