import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { humanizeNumber } from 'utils/index';

import PlayIcon from 'icons/play';
import HeartIcon from 'icons/heart';
import PlusIcon from 'icons/plus-alt';
import RetweetIcon from 'icons/retweet';

export default class MusicStats extends Component {
    static propTypes = {
        stats: PropTypes.object
    };

    render() {
        const { stats } = this.props;

        if (!stats) {
            return null;
        }

        const plays = stats['plays-raw'];
        const favorites = stats['favorites-raw'];
        const playlists = stats['playlists-raw'];
        const reposts = stats['reposts-raw'];

        // don't show icons if zero counts
        let reups;
        let playlistAdds;

        if (reposts !== 0) {
            reups = (
                <li className="track-stat track-stat--reup">
                    <span className="track-stat__icon track-stat__icon--reup u-text-orange">
                        <RetweetIcon />
                    </span>
                    {humanizeNumber(reposts)}
                </li>
            );
        }

        if (playlists !== 0) {
            playlistAdds = (
                <li className="track-stat track-stat__playlist">
                    <span className="track-stat__icon track-stat__icon--plus u-text-orange">
                        <PlusIcon />
                    </span>
                    {humanizeNumber(playlists)}
                </li>
            );
        }

        return (
            <ul className="track-stats inline-list">
                <li className="track-stat track-stat--plays">
                    <span className="track-stat__icon track-stat__icon--play u-text-orange">
                        <PlayIcon />
                    </span>
                    {humanizeNumber(plays)}
                </li>
                <li className="track-stat track-stat--favs">
                    <span className="track-stat__icon track-stat__icon--heart u-text-orange">
                        <HeartIcon />
                    </span>
                    {humanizeNumber(favorites)}
                </li>
                {reups}
                {playlistAdds}
            </ul>
        );
    }
}
