import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { getMusicUrl, buildDynamicImage } from 'utils/index';

import styles from './PlaylistCard.module.scss';

export default function PlaylistCard({ playlist }) {
    if (!playlist) {
        return null;
    }

    const imageSize = 155;
    const artwork = buildDynamicImage(playlist.image_base || playlist.image, {
        width: imageSize,
        height: imageSize,
        max: true
    });

    const retinaArtwork = buildDynamicImage(
        playlist.image_base || playlist.image,
        {
            width: imageSize * 2,
            height: imageSize * 2,
            max: true
        }
    );
    let srcSet;

    if (retinaArtwork) {
        srcSet = `${retinaArtwork} 2x`;
    }

    return (
        <Fragment>
            <Link className={styles.link} to={getMusicUrl(playlist)}>
                <div className={styles.image}>
                    <img
                        src={artwork}
                        srcSet={srcSet}
                        alt={playlist.title}
                        loading="lazy"
                        style={{ width: '100%' }}
                    />
                </div>
                <h3 className={styles.title}>{playlist.title}</h3>
            </Link>
            <p className={styles.count}>{playlist.track_count} songs</p>
        </Fragment>
    );
}

PlaylistCard.propTypes = {
    playlist: PropTypes.object
};
