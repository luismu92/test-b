import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    setCommentContext,
    getComments,
    deleteComment,
    reportComment,
    banComment
} from '../redux/modules/comment';

import { ucfirst, strToNumber, getMusicUrl } from 'utils/index';
import { addItems, showDrawer, hideDrawer } from '../redux/modules/drawer';
import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import { showMessage } from '../redux/modules/message';

import { ORDER_BY_VOTE, ORDER_BY_DATE, ORDER_BY_OLD } from 'constants/comment';
import CommentsContainer from 'components/CommentsContainer';

class CommentsWrapper extends Component {
    static propTypes = {
        item: PropTypes.object,
        kind: PropTypes.string,
        id: PropTypes.number,
        total: PropTypes.number,
        className: PropTypes.string,
        isMobile: PropTypes.bool,
        comment: PropTypes.object,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object,
        singleCommentUuid: PropTypes.string,
        singleCommentThread: PropTypes.string
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCommentSortClick = () => {
        const { dispatch } = this.props;

        dispatch(addItems(this.getSortItems()));
        dispatch(showDrawer());
    };

    handleSortComments = (e) => {
        const {
            dispatch,
            kind,
            singleCommentThread,
            singleCommentUuid
        } = this.props;
        const { id } = this.props.item;
        const button = e.currentTarget;
        const value = button.getAttribute('data-action');

        dispatch(setCommentContext(kind, id, value));
        dispatch(getComments(singleCommentUuid, singleCommentThread));
        dispatch(hideDrawer());
    };

    handleCommentActionClick = (e) => {
        const { dispatch } = this.props;

        const button = e.currentTarget;
        const uuid = button.getAttribute('data-uuid');
        const thread = button.getAttribute('data-thread');
        const commentId = button.getAttribute('data-comment-id');
        const userId = button.getAttribute('data-user');
        const artistId = button.getAttribute('data-artist');

        dispatch(
            addItems(
                this.getActionItems(uuid, thread, commentId, userId, artistId)
            )
        );
        dispatch(showDrawer());
    };

    handleCommentAction = (e) => {
        e.preventDefault();
        const action = e.currentTarget.getAttribute('data-action');
        const uuid = e.currentTarget.getAttribute('data-uuid');
        const thread = e.currentTarget.getAttribute('data-thread');
        const userId = e.currentTarget.getAttribute('data-user');

        this.props.dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `${ucfirst(action)} ${
                    action === 'ban' ? 'user' : 'comment'
                }`,
                message: `Are you sure you want to ${action} ${
                    action === 'ban' ? 'this user?' : 'this comment?'
                }`,
                handleConfirm: () => {
                    switch (action) {
                        case 'report':
                            this.doCommentReport(uuid, thread);
                            break;
                        case 'ban':
                            this.doBanUser(userId);
                            break;
                        case 'remove':
                            this.doCommentRemove(uuid, thread);
                            break;
                        default:
                            this.props.dispatch(hideModal());
                            break;
                    }
                }
            })
        );
    };

    //////////////////////
    // Internal Methods //
    //////////////////////

    getSortItems() {
        const sortItems = [
            {
                text: 'Top Comments',
                action: ORDER_BY_VOTE,
                handler: this.handleSortComments
            },
            {
                text: 'Newest First',
                action: ORDER_BY_DATE,
                handler: this.handleSortComments
            },
            {
                text: 'Oldest First',
                action: ORDER_BY_OLD,
                handler: this.handleSortComments
            }
        ];

        return sortItems;
    }

    getActionItems(uuid, thread, commentId, userId, artistId) {
        const { currentUser } = this.props;

        const canDelete =
            currentUser.isAdmin ||
            currentUser.profile.id === strToNumber(artistId);
        const canBan =
            currentUser.isAdmin &&
            currentUser.profile.id !== strToNumber(artistId);
        const canReport = currentUser.profile.id !== strToNumber(artistId);

        console.log(currentUser.profile.id, Number());

        const buttonProps = {
            'data-uuid': uuid,
            'data-thread': thread,
            'data-kind': this.props.kind,
            'data-comment-id': commentId,
            'data-user': userId
        };

        const actionItems = [];

        let commentUrl = `${getMusicUrl(this.props.item)}?comment=${uuid}`;
        if (thread !== null && thread !== '') {
            commentUrl += `&thread=${thread}`;
        }
        actionItems.push({
            text: 'Link',
            action: 'share',
            buttonProps: buttonProps,
            href: commentUrl
        });

        if (canDelete) {
            actionItems.push({
                text: 'Remove Comment',
                action: 'remove',
                buttonProps: buttonProps,
                handler: this.handleCommentAction
            });
        }

        if (canBan) {
            actionItems.push({
                text: 'Ban User',
                action: 'ban',
                buttonProps: buttonProps,
                handler: this.handleCommentAction
            });
        }

        if (canReport) {
            actionItems.push({
                text: 'Report Comment',
                action: 'report',
                buttonProps: buttonProps,
                handler: this.handleCommentAction
            });
        }

        return actionItems;
    }

    doCommentReport = (uuid, thread = null) => {
        const { dispatch } = this.props;

        dispatch(reportComment(uuid, thread === '' ? null : thread))
            .then(() => {
                dispatch(showMessage('Comment has been reported'));
                dispatch(hideDrawer());
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doCommentRemove = (uuid, thread = null) => {
        const { dispatch } = this.props;

        dispatch(deleteComment(uuid, thread === '' ? null : thread))
            .then(() => {
                dispatch(showMessage('Comment has been deleted'));
                dispatch(hideDrawer());
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doBanUser = (userId) => {
        const { dispatch } = this.props;

        dispatch(banComment(userId))
            .then(() => {
                dispatch(showMessage('User has been banned from commenting'));
                dispatch(hideDrawer());
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    render() {
        return (
            <CommentsContainer
                item={this.props.item}
                kind={this.props.kind}
                id={this.props.id}
                total={this.props.total}
                className={this.props.className}
                isMobile={this.props.isMobile}
                history={this.props.history}
                onCommentSortClick={this.handleCommentSortClick}
                onCommentActionClick={this.handleCommentActionClick}
                comment={this.props.comment}
                currentUser={this.props.currentUser}
                dispatch={this.props.dispatch}
                singleCommentUuid={this.props.singleCommentUuid}
                singleCommentThread={this.props.singleCommentThread}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        comment: state.comment,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(CommentsWrapper);
