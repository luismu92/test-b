import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class Checkbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: props.isChecked
        };
    }

    state = {
        isChecked: false
    };

    handleCheckboxChange = () => {
        const { onCheckboxChange, label } = this.props;

        this.setState(
            (prevState) => {
                return { isChecked: !prevState.isChecked };
            },
            () => {
                onCheckboxChange(label, this.state.isChecked);
            }
        );
    };

    render() {
        const { label, isFancy } = this.props;
        const { isChecked } = this.state;

        const klass = classnames('', {
            'fancy-checkbox': isFancy
        });

        return (
            <label>
                <input
                    className={klass}
                    type="checkbox"
                    value={label}
                    checked={isChecked}
                    onChange={this.handleCheckboxChange}
                />

                {label}
            </label>
        );
    }
}

Checkbox.propTypes = {
    label: PropTypes.string.isRequired,
    onCheckboxChange: PropTypes.func,
    isChecked: PropTypes.bool,
    isFancy: PropTypes.bool
};

export default Checkbox;
