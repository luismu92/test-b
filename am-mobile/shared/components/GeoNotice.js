import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './GeoNotice.module.scss';

export default class GeoNotice extends Component {
    static propTypes = {
        withBackdrop: PropTypes.bool,
        children: PropTypes.element
    };

    static defaultProps = {
        withBackdrop: false
    };

    render() {
        const { children, withBackdrop } = this.props;
        const wrapperClass = `${styles.geoNotice} ${
            withBackdrop ? styles.withBackdrop : ''
        }`;

        return (
            <div className={wrapperClass}>
                <p className={styles.notice}>{children}</p>
            </div>
        );
    }
}
