import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { getMusicUrl, getBranchUrl } from 'utils/index';

import DownloadIcon from 'icons/download';

export default class ListenOnAppButton extends Component {
    static propTypes = {
        musicItem: PropTypes.object.isRequired,
        keyPassword: PropTypes.string
    };

    render() {
        const { musicItem, keyPassword } = this.props;

        const appDeepLink = getMusicUrl(musicItem, {
            host: 'audiomack:/' // 1 slash since the url will start with a slash
        });
        const branchUrl = getBranchUrl(process.env.BRANCH_URL, {
            deeplinkPath: appDeepLink,
            channel: 'Mobile Web',
            campaign: 'Listen on App',
            feature: musicItem.type,
            [musicItem.type]: musicItem.id,
            key: keyPassword
        });

        return (
            <div className="pill-group pill-group--music">
                <a
                    className="pill pill--condensed pill--has-icon"
                    href={branchUrl}
                >
                    <span className="pill__icon u-spacing-right-5">
                        <DownloadIcon className="u-d-block" />
                    </span>
                    Download on the Audiomack App
                </a>
            </div>
        );
    }
}
