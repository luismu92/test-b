import React, { Component } from 'react';

export default class OuttageHeader extends Component {
    render() {
        const messageStyle = {
            color: 'orange',
            margin: '7px'
        };

        return (
            <header>
                <h3 style={{ ...messageStyle }}>
                    Audiomack is currently offline as we upgrade our server
                    capacity and will be back as soon as possible. Please check
                    back soon and we're sorry for the disruption.
                </h3>
            </header>
        );
    }
}
