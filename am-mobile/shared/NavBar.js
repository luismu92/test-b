import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { renderCappedDisplayCount, buildDynamicImage } from 'utils/index';

import {
    NAV_MARKER_FEED,
    NAV_MARKER_PLAYLIST,
    NAV_MARKER_TRENDING,
    NAV_MARKER_SEARCH,
    NAV_MARKER_PROFILE
} from './redux/modules/nav';
import Avatar from 'components/Avatar';

import AlbumsIcon from './icons/albums';
import FireIcon from './icons/fire';
import PlaylistIcon from './icons/playlist';
import SearchIcon from './icons/search';
import UserIcon from './icons/user';

export default class NavBar extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        feedNotifications: PropTypes.number,
        playlistNotifications: PropTypes.number,
        musicNotifications: PropTypes.number,
        navPosition: PropTypes.object,
        onNavClick: PropTypes.func
    };

    static defaultProps = {
        onNavClick() {}
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    renderProfileLink(currentUser) {
        const { musicNotifications } = this.props;
        let inner;
        let link = '/login';

        if (currentUser.isLoggedIn) {
            const imageSize = 25;
            const artwork = buildDynamicImage(
                currentUser.profile.image_base || currentUser.profile.image,
                {
                    width: imageSize,
                    height: imageSize,
                    max: true
                }
            );

            const retinaArtwork = buildDynamicImage(
                currentUser.profile.image_base || currentUser.profile.image,
                {
                    width: imageSize * 2,
                    height: imageSize * 2,
                    max: true
                }
            );
            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            inner = (
                <span
                    className="nav-bar__button-inner nav-bar__button-inner--avatar"
                    data-count={renderCappedDisplayCount(musicNotifications)}
                >
                    <Avatar
                        image={artwork}
                        srcSet={srcSet}
                        size={imageSize}
                        className="nav-bar__avatar u-overflow-hidden"
                    />
                    <span className="nav-bar__label" data-testid="artistName">
                        My Library
                    </span>
                </span>
            );
            link = `/artist/${currentUser.profile.url_slug}/favorites`;
        } else {
            inner = (
                <span className="nav-bar__button-inner">
                    <UserIcon role="presentation" title="" />
                    <span className="nav-bar__label">My Library</span>
                </span>
            );
        }

        const profileClass = classnames(
            'nav-bar__button nav-bar__button--profile',
            {
                'nav-bar__button--active':
                    this.props.navPosition.activeMarker === 'profile'
            }
        );

        return (
            <Link
                to={link}
                className={profileClass}
                data-marker={NAV_MARKER_PROFILE}
                onClick={this.props.onNavClick}
                data-testid="artistAvatar"
            >
                {inner}
            </Link>
        );
    }

    render() {
        const borderStyle = {
            transform: `translateX(${this.props.navPosition.transformX}px)`,
            width: this.props.navPosition.width
        };
        const {
            feedNotifications,
            playlistNotifications,
            currentUser
        } = this.props;
        let feedUrl = '/artists/popular';

        if (currentUser && currentUser.profile) {
            feedUrl = `/artist/${currentUser.profile.url_slug}/feed`;
        }

        const feedClass = classnames(
            'nav-bar__button nav-bar__button--outline-count',
            {
                'nav-bar__button--active':
                    this.props.navPosition.activeMarker === 'feed'
            }
        );
        const playlistClass = classnames('nav-bar__button', {
            'nav-bar__button--active':
                this.props.navPosition.activeMarker === 'playlist'
        });
        const trendingClass = classnames('nav-bar__button', {
            'nav-bar__button--active':
                this.props.navPosition.activeMarker === 'trending'
        });
        const searchClass = classnames('nav-bar__button', {
            'nav-bar__button--active':
                this.props.navPosition.activeMarker === 'search'
        });

        return (
            <div className="nav-bar-container">
                <nav className="nav-bar">
                    <Link
                        to={feedUrl}
                        className={feedClass}
                        data-marker={NAV_MARKER_FEED}
                        onClick={this.props.onNavClick}
                    >
                        <span
                            className="nav-bar__button-inner"
                            data-count={renderCappedDisplayCount(
                                feedNotifications
                            )}
                        >
                            <AlbumsIcon role="presentation" title="" />
                            <span className="nav-bar__label">Feed</span>
                        </span>
                    </Link>

                    <Link
                        to="/playlists/browse"
                        className={playlistClass}
                        data-marker={NAV_MARKER_PLAYLIST}
                        onClick={this.props.onNavClick}
                    >
                        <span
                            className="nav-bar__button-inner"
                            data-count={renderCappedDisplayCount(
                                playlistNotifications
                            )}
                        >
                            <PlaylistIcon role="presentation" title="" />
                            <span className="nav-bar__label">Playlists</span>
                        </span>
                    </Link>

                    <Link
                        to="/"
                        className={trendingClass}
                        data-marker={NAV_MARKER_TRENDING}
                        onClick={this.props.onNavClick}
                    >
                        <span className="nav-bar__button-inner">
                            <FireIcon role="presentation" title="" />
                            <span className="nav-bar__label">Browse</span>
                        </span>
                    </Link>

                    <Link
                        to="/search"
                        className={searchClass}
                        data-marker={NAV_MARKER_SEARCH}
                        onClick={this.props.onNavClick}
                    >
                        <span className="nav-bar__button-inner">
                            <SearchIcon role="presentation" title="" />
                            <span className="nav-bar__label">Search</span>
                        </span>
                    </Link>

                    {this.renderProfileLink(this.props.currentUser)}
                </nav>
                <span className="nav-bar-border" style={borderStyle} />
            </div>
        );
    }
}
