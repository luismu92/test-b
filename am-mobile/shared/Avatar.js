import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Avatar extends Component {
    static propTypes = {
        style: PropTypes.object,
        image: PropTypes.string,
        srcSet: PropTypes.string,
        className: PropTypes.string,
        size: PropTypes.number,
        center: PropTypes.bool,
        rounded: PropTypes.bool,
        stack: PropTypes.bool,
        onClick: PropTypes.func
    };

    static defaultProps = {
        size: 50,
        center: false,
        rounded: true,
        stack: false
    };

    render() {
        const image =
            this.props.image ||
            'https://d3m79pznqer0b2.cloudfront.net/default-artist-image.jpg';

        const style = {
            width: `${this.props.size}px`,
            height: `${this.props.size}px`,
            ...this.props.style
        };

        if (this.props.center) {
            style.margin = '0 auto';
        }

        if (this.props.rounded) {
            style.borderRadius = '50%';
            style.overflow = 'hidden';
        }

        const klass = classnames('avatar-container u-pos-relative', {
            [this.props.className]: this.props.className,
            'avatar-container--stack': this.props.stack
        });

        return (
            // eslint-disable-next-line jsx-a11y/click-events-have-key-events
            <div
                className={klass}
                style={style}
                onClick={this.props.onClick}
                role={this.props.onClick ? 'button' : null}
            >
                <img src={image} srcSet={this.props.srcSet} alt="" />
            </div>
        );
    }
}
