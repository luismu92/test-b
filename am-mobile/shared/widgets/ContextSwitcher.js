import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

export default class ContextSwitcher extends Component {
    static propTypes = {
        items: PropTypes.array.isRequired,
        onContextSwitch: PropTypes.func,
        onSubNavChange: PropTypes.func,
        onDropdownChange: PropTypes.func,
        subnavItems: PropTypes.array,
        dropdownItems: PropTypes.object,
        activeContext: PropTypes.string
    };

    static defaultProps = {
        onContextSwitch() {},
        onSubNavChange() {}
    };

    componentDidMount() {
        this.scrollActiveContextIntoView();
    }

    componentDidUpdate() {
        this.scrollActiveContextIntoView();
    }

    handleContextSwitch = (e) => {
        const link = e.currentTarget;
        const value = link.getAttribute('data-value');

        if (value !== this.props.activeContext) {
            this.scrollActiveContextIntoView(link);
            this.props.onContextSwitch(value);
        }
    };

    handleSubnavClick = (e) => {
        const link = e.currentTarget;
        const value = link.getAttribute('data-value');

        this.props.onSubNavChange(value);
    };

    handleDropdownChange = (e) => {
        const select = e.currentTarget;
        const key = select.getAttribute('data-key');
        const value = select.value;

        this.props.onDropdownChange(key, value);
    };

    scrollActiveContextIntoView(item) {
        if (!this._contextList) {
            return;
        }

        const width = this._contextList.clientWidth;
        const activeItem =
            item ||
            this._contextList.querySelector(
                '.context-switcher__list-item--active'
            );

        if (!activeItem) {
            console.warn(
                'No active item found in ContextSwitcher. Check to see your items are properly provided'
            );
            return;
        }

        const computed = window.getComputedStyle(activeItem);
        const margin =
            parseInt(computed.marginLeft, 10) +
            parseInt(computed.marginRight, 10);
        const activeItemWidth = activeItem.clientWidth + margin;
        const offset = activeItem.offsetLeft;
        const centerX = width / 2;
        const x = offset - centerX + activeItemWidth / 2;

        this._contextList.scrollLeft = x;
    }

    renderItems(items = []) {
        const lis = items.map((item, i) => {
            const klass = classnames('context-switcher__list-item', {
                'context-switcher__list-item--active': item.active
            });

            let button;

            if (item.href) {
                button = (
                    <Link {...item.buttonProps} to={item.href}>
                        {item.text}
                    </Link>
                );
            } else {
                button = <button {...item.buttonProps}>{item.text}</button>;
            }

            return (
                /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-noninteractive-element-interactions */
                <li
                    className={klass}
                    key={i}
                    data-value={item.value}
                    onClick={this.handleContextSwitch}
                    {...item.listItemProps}
                >
                    {button}
                </li>
            );
        });

        return (
            <ul
                className="context-switcher__list"
                ref={(c) => (this._contextList = c)}
            >
                {lis}
            </ul>
        );
    }

    // @todo make this and navbarcontainer component
    // the same
    renderSubnav(items = []) {
        if (!items.length) {
            return null;
        }

        const lis = items.map((item, i) => {
            const klass = classnames('flex-item', {
                'flex-item--active': item.active
            });

            return (
                /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
                <div
                    className={klass}
                    key={i}
                    onClick={this.handleSubnavClick}
                    data-value={item.value}
                >
                    <Link to={item.href}>{item.text}</Link>
                </div>
            );
        });

        return (
            <div className="nav-bar context-switcher__nav-bar context-switcher__nav-bar--padded-links">
                {lis}
            </div>
        );
    }

    renderDropdowns(items = {}) {
        if (!Object.keys(items).length) {
            return null;
        }

        const selects = Object.keys(items).map((key, i) => {
            const dropdownItems = items[key];
            let selected;

            const options = dropdownItems.map((item, k) => {
                if (item.active) {
                    selected = item.value;
                }

                return (
                    <option value={item.value} key={`${i}${k}`}>
                        {item.text}
                    </option>
                );
            });

            return (
                <label
                    className="select-wrapper u-pos-relative"
                    key={i}
                    aria-label={key}
                >
                    {/* eslint-disable-next-line jsx-a11y/no-onchange */}
                    <select
                        onChange={this.handleDropdownChange}
                        data-key={key}
                        value={selected}
                    >
                        {options}
                    </select>
                    <span className="select-chevron" />
                </label>
            );
        });

        return (
            <div className="nav-bar context-switcher__nav-bar nav-bar--context-switcher">
                {selects}
            </div>
        );
    }

    render() {
        return (
            <div className="context-switcher-wrapper">
                <div className="context-switcher">
                    {this.renderItems(this.props.items)}
                </div>
                {this.renderSubnav(this.props.subnavItems)}
                {this.renderDropdowns(this.props.dropdownItems)}
            </div>
        );
    }
}
