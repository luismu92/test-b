import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import Transition, {
    EXITED,
    ENTERING,
    ENTERED,
    EXITING
} from 'react-transition-group/Transition';

export default class Drawer extends Component {
    static propTypes = {
        items: PropTypes.array,
        onDrawerItemClick: PropTypes.func,
        className: PropTypes.string,
        displayOverlay: PropTypes.bool,
        addCloseButtonItem: PropTypes.bool,
        visible: PropTypes.bool
    };

    static defaultProps = {
        displayOverlay: true,
        addCloseButtonItem: true
    };

    renderItems(items, showCloseButton) {
        const list = items.map((item, i) => {
            const tag = item.href ? Link : 'button';
            // If the link has http or https, that means it's an external link
            const useAnchor =
                (!!item.href && !!item.href.match(/^http(s)?:\/\//)) ||
                item.anchor === true;
            const handler =
                typeof item.handler === 'function'
                    ? item.handler
                    : this.props.onDrawerItemClick;

            const props = {
                onClick: handler,
                'data-action': item.action,
                children: item.text,
                ...(item.buttonProps || {})
            };

            if (useAnchor) {
                props.href = item.href;
                props.target = '_blank';
                props.rel = 'nofollow noopener';
            } else if (item.href) {
                props.to = item.href;
            }

            const tagRender = React.createElement(useAnchor ? 'a' : tag, props);

            const klass = classnames('drawer__list-item', {
                [item.className]: item.className
            });

            return (
                <li key={i} className={klass}>
                    {tagRender}
                </li>
            );
        });

        if (!showCloseButton) {
            return list;
        }

        return list.concat(
            <li key={list.length} className="drawer__close-button">
                <button
                    onClick={this.props.onDrawerItemClick}
                    data-action="drawer-close"
                >
                    Cancel
                </button>
            </li>
        );
    }

    renderOverlay = (state) => {
        const klass = classnames('drawer-overlay', {
            'drawer-overlay--transition-enter': state === ENTERING,
            'drawer-overlay--transition-enter-active': state === ENTERED,
            'drawer-overlay--transition-leave': state === EXITING,
            'drawer-overlay--transition-leave-active': state === EXITED
        });

        return (
            <button
                className={klass}
                onClick={this.props.onDrawerItemClick}
                data-action="drawer-close"
                aria-label="Close action drawer"
            />
        );
    };

    renderOverlayTransition() {
        const { visible, displayOverlay } = this.props;

        if (!displayOverlay) {
            return null;
        }

        return (
            <Transition timeout={110} in={visible} mountOnEnter unmountOnExit>
                {this.renderOverlay}
            </Transition>
        );
    }

    render() {
        const { visible, className } = this.props;
        const klass = classnames('drawer', {
            'drawer--visible': visible
        });
        const containerClass = classnames('drawer-container', {
            [className]: className
        });

        return (
            <div className={containerClass}>
                {this.renderOverlayTransition()}
                <div className={klass}>
                    <ul className="drawer-items">
                        {this.renderItems(
                            this.props.items,
                            this.props.addCloseButtonItem
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}
