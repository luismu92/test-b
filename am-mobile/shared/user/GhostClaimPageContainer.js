import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { claimArtist } from '../redux/modules/user/index';
import { hideModal } from '../redux/modules/modal';

import GhostClaimPage from './GhostClaimPage';

class GhostClaimPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        urlSlug: PropTypes.string.isRequired,
        artist: PropTypes.object,
        history: PropTypes.object,
        id: PropTypes.number,
        code: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired
    };

    handleClaimSubmit = (e) => {
        e.preventDefault();

        const { dispatch, id, code, email } = this.props;
        const form = e.currentTarget;
        const password = form.password.value;
        const options = {
            email,
            code,
            password,
            agree_to_terms: 'yes'
        };

        dispatch(claimArtist(id, options))
            .then(() => {
                this.finishClaim();
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    finishClaim() {
        const { dispatch, history, urlSlug } = this.props;

        dispatch(hideModal());

        history.replace(`/artist/${urlSlug}`);
    }

    render() {
        return (
            <GhostClaimPage
                id={this.props.id}
                email={this.props.email}
                artist={this.props.artist}
                onClaimSubmit={this.handleClaimSubmit}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        artist: state.artist
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(GhostClaimPageContainer);
