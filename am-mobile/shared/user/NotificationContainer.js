import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    getNotifications,
    markNotificationsAsSeen
} from '../redux/modules/user/notifications';

import NotificationList from '../list/NotificationList';

class NotificationContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        currentUserNotifications: PropTypes.object,
        dispatch: PropTypes.func
    };

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getNotifications({ pagingToken: null }))
            .then(() => {
                return dispatch(markNotificationsAsSeen({ all: true }));
            })
            .catch((err) => {
                console.log(err);
            });
    }

    handleNotificationLoadMoreClick = () => {
        const { dispatch, currentUserNotifications } = this.props;
        const { pagingToken } = currentUserNotifications;

        dispatch(getNotifications({ pagingToken }));
    };

    render() {
        const { currentUserNotifications } = this.props;
        const { loading, list, onLastPage } = currentUserNotifications;

        let loadMoreButton;

        if (!loading && !onLastPage && list.length) {
            const loadStyle = { color: 'white' };

            loadMoreButton = (
                <div className="button-group">
                    <button
                        className="load-more pill"
                        style={loadStyle}
                        onClick={this.handleNotificationLoadMoreClick}
                    >
                        + Load More
                    </button>
                </div>
            );
        }

        return (
            <div className="outer-padding">
                <h2>Notifications</h2>
                {loading && list.length === 0 && <h2>Loading...</h2>}
                {!loading && list.length === 0 && <h2>Empty.</h2>}
                {list.length > 0 && (
                    <div style={{ opacity: loading ? 0.5 : 1 }}>
                        <NotificationList notifications={list} />
                        {loadMoreButton}
                    </div>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUserNotifications: state.currentUserNotifications,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(NotificationContainer);
