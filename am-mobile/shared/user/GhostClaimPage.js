import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class GhostClaimPage extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        email: PropTypes.string,
        artist: PropTypes.object,
        onClaimSubmit: PropTypes.func
    };

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="error">{error.message}</p>;
    }

    render() {
        const { onClaimSubmit, artist, id, email } = this.props;

        return (
            <form
                action={`${process.env.API_URL}/artist/${id}/claim`}
                method="PUT"
                onSubmit={onClaimSubmit}
            >
                <div hidden style={{ display: 'none' }}>
                    <input
                        type="email"
                        name="email"
                        tabIndex="-1"
                        value={email}
                        autoComplete="username"
                        autoCapitalize="none"
                        readOnly
                    />
                </div>

                <input
                    type="password"
                    key="password"
                    name="password"
                    data-testid="password"
                    placeholder="Password"
                    autoComplete="current-password"
                    autoCapitalize="none"
                    style={{ margin: '12px 0' }}
                    required
                />

                {this.renderError(artist.claimError)}

                <div className="text-center">
                    <input
                        className="button button--lg"
                        type="submit"
                        data-testid="loginSubmit"
                        value={
                            artist.isClaimingArtist
                                ? 'Finalizing...'
                                : 'Claim account'
                        }
                        disabled={artist.isClaimingArtist}
                    />
                </div>
            </form>
        );
    }
}
