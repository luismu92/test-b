import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { showMessage } from '../redux/modules/message';
import { verifyHash, resendEmail } from '../redux/modules/user/index';
import { hideModal } from '../redux/modules/modal';
import VerifyEmail from './VerifyEmail';

class VerifyEmailContainer extends Component {
    static propTypes = {
        hash: PropTypes.string.isRequired,
        dispatch: PropTypes.func,
        location: PropTypes.object,
        history: PropTypes.object,
        currentUser: PropTypes.object
    };

    componentDidMount() {
        const { history, location, hash, dispatch } = this.props;

        history.replace(location.pathname);

        dispatch(verifyHash(hash))
            .then(() => {
                dispatch(hideModal());
                dispatch(showMessage('Thanks for verifying your email!'));
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleResendEmailClick = () => {
        const { dispatch } = this.props;

        dispatch(resendEmail());
        dispatch(hideModal());
        dispatch(showMessage('An email confirmation has been sent.'));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <VerifyEmail
                hash={this.props.hash}
                isVerifying={this.props.currentUser.verifyHash.loading}
                onResendEmailClick={this.handleResendEmailClick}
                error={this.props.currentUser.verifyHash.error}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(VerifyEmailContainer);
