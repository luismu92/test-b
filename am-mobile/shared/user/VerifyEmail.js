import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import AndroidLoader from '../loaders/AndroidLoader';

export default class VerifyEmail extends Component {
    static propTypes = {
        hash: PropTypes.string.isRequired,
        isVerifying: PropTypes.bool,
        onResendEmailClick: PropTypes.func,
        error: PropTypes.object
    };

    render() {
        if (this.props.isVerifying) {
            return (
                <div className="text-center">
                    <h2>Verifying email…</h2>
                    <AndroidLoader />
                </div>
            );
        }

        if (this.props.error) {
            return (
                <div className="body-text body-text--no-padding">
                    <h2>Email verification error</h2>
                    <p>
                        There was an error verifying your email. This could be
                        because the link has expired or the link is incorrect.
                        Try having the confirmation{' '}
                        <button
                            className="button button--link"
                            onClick={this.props.onResendEmailClick}
                        >
                            email resent
                        </button>
                        .
                    </p>
                    <p>
                        Still having issues?{' '}
                        <Link to="/contact-us">Contact us</Link>.
                    </p>
                </div>
            );
        }

        return (
            <form action="/auth/verify-hash" method="post">
                <input
                    type="text"
                    value={this.props.hash}
                    name="hash"
                    readOnly
                />
                <p className="u-text-center">
                    <input
                        type="submit"
                        className="button button--pill"
                        value="Verify"
                    />
                </p>
            </form>
        );
    }
}
