import React from 'react';
import PropTypes from 'prop-types';

import { settingsLabelsMap } from 'constants/notifications/index';

import ToggleSwitch from 'components/ToggleSwitch';

import styles from './NotificationSettings.module.scss';

export default function NotificationSettings({
    settings,
    onUpdateNotifications
}) {
    const sections = ['email', 'push'].map((section, i) => {
        const toggles = Object.entries(settings).map(([type, value], key) => {
            if (type.includes(section)) {
                const item = {
                    type: type,
                    value: value
                };

                if (Object.keys(settingsLabelsMap).indexOf(item.type) === -1) {
                    return null;
                }

                return (
                    <div className="column small-24 medium-12" key={key}>
                        <div className={styles.setting}>
                            <p>{settingsLabelsMap[item.type]}</p>
                            <ToggleSwitch
                                checked={item.value}
                                label={item.type}
                                name={item.type}
                                id={`toggle-${key}`}
                                onChange={onUpdateNotifications}
                            />
                        </div>
                    </div>
                );
            }

            return null;
        });

        return (
            <div className="row u-spacing-bottom-50" key={`section-${i}`}>
                <h2 className={styles.sectionTitle}>{section} Notifications</h2>
                {toggles}
            </div>
        );
    });

    return <form style={{ padding: '30px 15px' }}>{sections}</form>;
}

NotificationSettings.propTypes = {
    settings: PropTypes.object,
    onUpdateNotifications: PropTypes.func
};
