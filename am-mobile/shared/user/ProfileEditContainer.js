import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import { previewFile, ucfirst } from 'utils/index';
import {
    allGenresMap,
    podcastCategories,
    podcastLanguages
} from 'constants/index';

import Avatar from 'components/Avatar';
import SocialLinkButton from 'buttons/SocialLinkButton';

import {
    saveUserDetails,
    logOut,
    updatePassword,
    updateSlug,
    updateEmail,
    deleteAccount
} from '../redux/modules/user';
import {
    getUserSettings,
    updateUserSettings
} from '../redux/modules/user/notificationSettings';

import { showMessage } from '../redux/modules/message';

import NotificationSettings from './NotificationSettings';

import ContextSwitcher from '../widgets/ContextSwitcher';
import DotsLoader from '../loaders/DotsLoader';
import requireAuth from '../hoc/requireAuth';
import Select from '../Select';

import PasswordHide from '../icons/eye-hide';
import PasswordView from '../icons/eye-view';

const maxLengthRules = {
    name: 65,
    label: 65,
    hometown: 65,
    url: 80,
    bio: 900
};

const fields = {
    name: {
        title: 'Artist Name'
    },
    label: {
        title: 'Label'
    },
    hometown: {
        title: 'Hometown'
    },
    genre: {
        title: 'Genre'
    },
    url: {
        title: 'Website'
    },
    bio: {
        title: 'Short Bio'
    },
    facebook: {
        title: 'Facebook',
        placeholder: 'https://facebook.com/username'
    },
    twitter: {},
    instagram: {}
};

const allTabs = ['basic', 'password', 'email', 'url', 'notifications'];

class ProfileEditContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        history: PropTypes.object,
        match: PropTypes.object,
        notificationSettings: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            activeTab: this.getTab(props.match.params.tab),
            basicFormValues: {},
            updatePassword: {},
            newUrlSlug: props.currentUser.profile.url_slug,
            showPassword: false,
            newEmail: {
                email: '',
                password: ''
            },
            deletePassword: '',
            isSocial:
                props.currentUser.profile.facebook_id ||
                props.currentUser.profile.twitter_id ||
                props.currentUser.profile.google_id ||
                props.currentUser.profile.apple_id
        };

        if (!props.currentUser.isLoggedIn) {
            props.history.replace('/');
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSaveButtonClick = () => {
        const { dispatch } = this.props;

        dispatch(saveUserDetails(this.state.basicFormValues))
            .then(() => {
                return this.goToArtistPage();
            })
            .catch((err) => console.log(err));
    };

    handleEditFieldInput = (e) => {
        const { name, value } = e.currentTarget;

        if (name === 'image') {
            previewFile(e.target.files[0])
                .then((result) => {
                    this.setState((prevState) => {
                        return {
                            basicFormValues: {
                                ...prevState.basicFormValues,
                                [name]: result
                            }
                        };
                    });
                    return;
                })
                .catch((err) => console.log(err));
            return;
        }

        this.setState((prevState) => {
            return {
                basicFormValues: {
                    ...prevState.basicFormValues,
                    [name]: value
                }
            };
        });
    };

    handleDropdownChange = (e) => {
        const menu = e.currentTarget;
        const name = menu.name;
        const value = menu.options[menu.selectedIndex].value;

        this.setState((prevState) => {
            return {
                basicFormValues: {
                    ...prevState.basicFormValues,
                    [name]: value
                }
            };
        });
    };

    handleContextSwitch = (tab) => {
        this.setState({
            activeTab: tab
        });
    };

    handleUpdatePasswordInput = (e) => {
        const { name, value } = e.currentTarget;

        this.setState((prevState) => {
            return {
                ...prevState,
                updatePassword: {
                    ...prevState.updatePassword,
                    [name]: value
                }
            };
        });
    };

    handleUpdateUrlSlugInput = (e) => {
        const { value } = e.currentTarget;

        this.setState((prevState) => {
            return {
                ...prevState,
                newUrlSlug: (value || '').toLowerCase()
            };
        });
    };

    handleUpdateEmailInput = (e) => {
        const { name, value } = e.currentTarget;

        this.setState((prevState) => {
            let finalValue = value;

            if (name === 'email') {
                finalValue = finalValue.toLowerCase();
            }

            return {
                ...prevState,
                newEmail: {
                    ...prevState.newEmail,
                    [name]: finalValue
                }
            };
        });
    };

    handleDeleteFieldInput = (e) => {
        const { value } = e.currentTarget;

        this.setState((prevState) => {
            return {
                ...prevState,
                deletePassword: value || ''
            };
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch } = this.props;

        dispatch(saveUserDetails(this.state.editableFieldValues))
            .then(() => {
                return;
            })
            .catch((err) => console.log(err));
    };

    handleUpdatePasswordFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, history } = this.props;
        const { current: oldPass, new1: newPass } = this.state.updatePassword;

        dispatch(updatePassword(oldPass, newPass, newPass))
            .then(() => {
                dispatch(
                    showMessage(
                        'Your password has been changed. You can now log in with your new password.'
                    )
                );
                dispatch(logOut());
                history.push('/login');

                return;
            })
            .catch((err) => {
                let message = 'There was an error updating your password.';

                if (err.errorcode === 1004) {
                    message = err.message;
                }

                dispatch(showMessage(message));
                console.log(err);
            });
    };

    handleUpdateUrlSlugFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch } = this.props;

        dispatch(updateSlug(this.state.newUrlSlug))
            .then(() => {
                dispatch(showMessage('Your URL slug has been updated.'));
                return;
            })
            .catch((err) => console.log(err));
    };

    handleUpdateEmailFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, history } = this.props;
        const { email, password } = this.state.newEmail;

        dispatch(updateEmail(email, password))
            .then(() => {
                dispatch(
                    showMessage(
                        'Your email has been updated. You can now log in with your new email.'
                    )
                );

                dispatch(logOut());

                history.push('/login');
                return;
            })
            .catch((err) => console.log(err));
    };

    handleDeleteAccountFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, history } = this.props;

        dispatch(deleteAccount(this.state.deletePassword, this.state.isSocial))
            .then(() => {
                dispatch(showMessage('Your account has been deleted.'));

                dispatch(logOut());

                history.push('/');
                return;
            })
            .catch((err) => console.log(err));
    };

    handleTogglePassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        });
    };

    handleUpdateNotifications = (e) => {
        const { dispatch } = this.props;
        const { name, checked } = e.currentTarget;

        dispatch(updateUserSettings(name, checked)).catch((err) => {
            dispatch(
                showMessage(
                    'There was a problem updating your preferences. Please try again.'
                )
            );
            console.log(err);
        });
    };

    handleSocialLinkSuccess = (action, network) => {
        this.props.dispatch(
            showMessage(`Your ${ucfirst(network)} was successfully linked.`)
        );
    };

    handleSocialLinkError = (err, action, network) => {
        this.props.dispatch(
            showMessage(
                err.message ||
                    `There was an error ${action}ing your ${ucfirst(network)}`
            )
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getTab(initialTab) {
        return allTabs.find((tab) => tab === initialTab) || allTabs[0];
    }

    getAvatar(image) {
        return <Avatar image={image} size={130} className="edit-mode" />;
    }

    getContextSwitcherItems(activeTab) {
        const contextItems = [
            {
                text: 'Basic Information',
                active: activeTab === 'basic' || !activeTab,
                href: '/edit/profile/basic',
                value: 'basic'
            },
            {
                text: 'Change Password',
                active: activeTab === 'password',
                href: '/edit/profile/password',
                value: 'password'
            },
            {
                text: 'Update Email',
                active: activeTab === 'email',
                href: '/edit/profile/email',
                value: 'email'
            },
            {
                text: 'Update Artist URL',
                active: activeTab === 'url',
                href: '/edit/profile/url',
                value: 'url'
            },
            {
                text: 'Notifications',
                active: activeTab === 'notifications',
                href: '/edit/profile/notifications',
                value: 'notifications'
            },
            {
                text: 'Delete Account',
                active: activeTab === 'delete',
                value: 'delete',
                buttonProps: {
                    'data-testid': 'deleteAccount',
                    style: { color: 'red' }
                }
            }
        ];

        return contextItems;
    }

    goToArtistPage() {
        const { currentUser } = this.props;

        return this.props.history.push(
            `/artist/${currentUser.profile.url_slug}`
        );
    }

    renderInputs(profile, field, maxLength, basicFormValues, placeholder) {
        // eslint-disable-line
        if (field === 'bio') {
            return (
                <textarea
                    maxLength={maxLength}
                    defaultValue={profile[field] || basicFormValues[field]}
                    name={field}
                    placeholder={placeholder}
                    onChange={this.handleEditFieldInput}
                />
            );
        }

        return (
            <input
                maxLength={maxLength}
                type="text"
                spellCheck={false}
                defaultValue={profile[field] || basicFormValues[field]}
                placeholder={placeholder}
                onChange={this.handleEditFieldInput}
                name={field}
            />
        );
    }

    renderTbody({ profile, errors }, fieldObj, basicFormValues) {
        const fieldKeys = Object.keys(fieldObj);
        const rows = fieldKeys.map((field, i) => {
            const placeholder = fieldObj[field].placeholder;

            const errorObj =
                errors && errors[0] && errors[0].errors ? errors[0].errors : {};
            let errorMessages = errorObj[field];

            if (errorMessages) {
                errorMessages = Object.keys(errorMessages).map((key, k) => {
                    return (
                        <p className="error u-spacing-top-10" key={k}>
                            {errorMessages[key]}
                        </p>
                    );
                });
            }

            const isSocial = ['instagram', 'twitter'].includes(field);
            let text = (
                <Fragment>
                    {this.renderInputs(
                        profile,
                        field,
                        maxLengthRules[field],
                        basicFormValues,
                        placeholder
                    )}
                    {errorMessages}
                </Fragment>
            );
            let title = fieldObj[field].title;

            if (field === 'bio') {
                let currentLength = 0;

                if (basicFormValues[field]) {
                    currentLength = basicFormValues[field].length;
                }

                title = (
                    <span>
                        {title}
                        <br />
                        <small className="subline">
                            <em>
                                {maxLengthRules[field] - currentLength}{' '}
                                characters left
                            </em>
                        </small>
                    </span>
                );
            } else if (field === 'genre') {
                const genreOptions = Object.keys(allGenresMap).map((genre) => {
                    return { value: genre, text: allGenresMap[genre] };
                });
                const genreProps = {
                    value:
                        typeof basicFormValues.genre !== 'undefined'
                            ? basicFormValues.genre
                            : profile.genre || ''
                };

                text = (
                    <Select
                        options={genreOptions}
                        name="genre"
                        selectProps={genreProps}
                        onChange={this.handleDropdownChange}
                    />
                );
            } else if (isSocial) {
                text = (
                    <SocialLinkButton
                        network={field}
                        onSocialLink={this.handleSocialLinkSuccess}
                        onSocialLinkError={this.handleSocialLinkError}
                    />
                );
            }

            return (
                <tr key={i}>
                    {!isSocial && <td>{title}</td>}
                    <td colSpan={isSocial ? 2 : null}>{text}</td>
                </tr>
            );
        });

        let podcastRow;
        const currentGenre = basicFormValues.genre || profile.genre;

        if (currentGenre === 'podcast') {
            const podcastOptions = [
                { value: '', text: 'Choose category' }
            ].concat(
                podcastCategories.map(({ category }) => {
                    return { value: category, text: category };
                })
            );
            const podcastProps = {
                value:
                    typeof basicFormValues.podcast_category !== 'undefined'
                        ? basicFormValues.podcast_category
                        : profile.podcast_category || ''
            };

            const podcastLangOptions = Object.keys(podcastLanguages).map(
                (langCode) => {
                    const text = podcastLanguages[langCode];

                    return { value: langCode, text: text };
                }
            );

            const podcastLanguageProps = {
                value:
                    typeof basicFormValues.podcast_language !== 'undefined'
                        ? basicFormValues.podcast_language
                        : profile.podcast_language || 'en'
            };

            podcastRow = (
                <Fragment key="podcastOptions">
                    <tr key="podcastRow">
                        <td>Podcast Category</td>
                        <td>
                            <Select
                                options={podcastOptions}
                                name="podcast_category"
                                selectProps={podcastProps}
                                onChange={this.handleDropdownChange}
                            />
                        </td>
                    </tr>
                    <tr key="podcastLang">
                        <td>Podcast Language</td>
                        <td>
                            <Select
                                options={podcastLangOptions}
                                name="podcast_language"
                                selectProps={podcastLanguageProps}
                                onChange={this.handleDropdownChange}
                            />
                        </td>
                    </tr>
                </Fragment>
            );
        }

        const index = fieldKeys.indexOf('genre');

        rows.splice(index + 1, 0, podcastRow);

        return (
            <tbody>
                {rows}
                <tr>
                    <td colSpan="2">
                        {this.renderSaveButton(this.props.currentUser)}
                    </td>
                </tr>
            </tbody>
        );
    }

    renderAvatar({ profile }, basicFormValues) {
        let value = profile.image;

        if (basicFormValues.image) {
            value = basicFormValues.image;
        }

        return (
            <div className="avatar-wrap edit-mode">
                <input
                    type="file"
                    name="image"
                    className="avatar-wrap__file-input"
                    accept="image/*"
                    onChange={this.handleEditFieldInput}
                />
                <i className="glyphicons camera" />
                {this.getAvatar(value)}
            </div>
        );
    }

    renderSaveButton({ loading }) {
        let text = 'Save';

        if (loading) {
            text = <DotsLoader active={loading} />;
        }

        return (
            <div className="save-container">
                <button
                    className="button button--lg"
                    disabled={loading}
                    onClick={this.handleSaveButtonClick}
                >
                    {text}
                </button>
            </div>
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="auth__error">{error.description}</p>;
    }

    renderNewPasswordForm() {
        const { currentUser } = this.props;
        const { loading } = currentUser.updatePassword;
        let updatePasswordText = 'Change password';

        if (loading) {
            updatePasswordText = 'Updating…';
        }

        let passwordIcon = <PasswordView />;
        let inputType = 'password';

        if (this.state.showPassword) {
            passwordIcon = <PasswordHide />;
            inputType = 'text';
        }

        return (
            <form onSubmit={this.handleUpdatePasswordFormSubmit}>
                <h3 className="profile-table-header">Change password</h3>
                <table className="profile-table">
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="current">
                                    Current password
                                </label>
                            </td>
                            <td className="u-pos-relative">
                                <input
                                    id="current"
                                    name="current"
                                    type={inputType}
                                    required
                                    onChange={this.handleUpdatePasswordInput}
                                    autoCapitalize="none"
                                />
                                <button
                                    className="form-container__toggle-password"
                                    onClick={this.handleTogglePassword}
                                    tabIndex="-1"
                                >
                                    {passwordIcon}
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="new1">New password</label>
                            </td>
                            <td className="u-pos-relative">
                                <input
                                    id="new1"
                                    name="new1"
                                    type={inputType}
                                    required
                                    onChange={this.handleUpdatePasswordInput}
                                />
                                <button
                                    className="form-container__toggle-password"
                                    onClick={this.handleTogglePassword}
                                    tabIndex="-1"
                                >
                                    {passwordIcon}
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <div className="save-container">
                                    <button
                                        type="submit"
                                        name="submit"
                                        className="button button--lg"
                                        disabled={loading}
                                    >
                                        {updatePasswordText}
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }

    renderNewEmailForm() {
        const { currentUser } = this.props;
        const { newEmail } = this.state;

        const { loading, error } = currentUser.updateEmail;
        let updateEmailText = 'Update email';

        if (loading) {
            updateEmailText = 'Updating…';
        }

        let messages = {};

        if (error) {
            messages = this.getErrorMessagesObject(error.errors);
        }

        let passwordIcon = <PasswordView />;
        let inputType = 'password';

        if (this.state.showPassword) {
            passwordIcon = <PasswordHide />;
            inputType = 'text';
        }

        return (
            <form onSubmit={this.handleUpdateEmailFormSubmit}>
                <h3 className="profile-table-header">Update email</h3>
                <table className="profile-table">
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="currentEmail">
                                    Current email
                                </label>
                            </td>
                            <td>
                                <input
                                    id="currentEmail"
                                    type="email"
                                    value={currentUser.profile.email}
                                    name="email"
                                    key="currentEmail"
                                    autoCapitalize="none"
                                    autoComplete="off"
                                    disabled
                                    readOnly
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="newEmail">New email</label>
                            </td>
                            <td>
                                <input
                                    id="newEmail"
                                    type="email"
                                    value={newEmail.email || ''}
                                    name="email"
                                    required
                                    autoCapitalize="none"
                                    onChange={this.handleUpdateEmailInput}
                                />
                                {this.renderError(messages.email)}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="newEmailPassword">
                                    Current password
                                </label>
                            </td>
                            <td className="u-pos-relative">
                                <input
                                    id="newEmailPassword"
                                    type={inputType}
                                    value={newEmail.password || ''}
                                    name="password"
                                    autoCapitalize="none"
                                    required
                                    onChange={this.handleUpdateEmailInput}
                                />
                                <button
                                    className="form-container__toggle-password"
                                    onClick={this.handleTogglePassword}
                                    tabIndex="-1"
                                >
                                    {passwordIcon}
                                </button>
                                {this.renderError(messages.password)}
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <div className="save-container">
                                    <button
                                        type="submit"
                                        name="submit"
                                        className="button button--lg"
                                        disabled={loading}
                                    >
                                        {updateEmailText}
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }

    renderNewSlugForm() {
        const { currentUser } = this.props;
        const { newUrlSlug } = this.state;
        let updateSlugText = 'Update slug';

        const { loading } = currentUser.updateSlug;

        if (loading) {
            updateSlugText = 'Updating…';
        }

        return (
            <form onSubmit={this.handleUpdateUrlSlugFormSubmit}>
                <h3 className="profile-table-header">Update slug</h3>
                <table className="profile-table">
                    <tbody>
                        <tr>
                            <td>New slug</td>
                            <td>
                                <input
                                    type="text"
                                    value={newUrlSlug || ''}
                                    pattern="^[a-zA-Z0-9\-]*$"
                                    name="url_slug"
                                    required
                                    onChange={this.handleUpdateUrlSlugInput}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <div className="save-container">
                                    <button
                                        type="submit"
                                        name="submit"
                                        className="button button--lg"
                                        disabled={loading}
                                    >
                                        {updateSlugText}
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }

    renderNotificationsForm() {
        const { notificationSettings } = this.props;
        const { settings } = notificationSettings;

        if (!settings) {
            return null;
        }

        return (
            <NotificationSettings
                settings={settings}
                onUpdateNotifications={this.handleUpdateNotifications}
            />
        );
    }

    renderDeleteForm() {
        const { currentUser } = this.props;
        const { deletePassword } = this.state;

        let deleteAccountText = 'Delete account';

        const { loading, error } = currentUser.deleteAccount;

        if (loading) {
            deleteAccountText = 'Deleting…';
        }

        let message = {};

        if (error) {
            message = {
                description: error.message
            };
        }

        let passwordInput = (
            <Fragment>
                <td>
                    <label htmlFor="delete-password">
                        Enter your password to delete your account.
                    </label>
                </td>
                <td>
                    <input
                        id="delete-password"
                        type="password"
                        autoComplete="current-password"
                        autoCapitalize="none"
                        data-testid="deletePassword"
                        required
                        value={deletePassword}
                        name="delete_password"
                        onChange={this.handleDeleteFieldInput}
                    />
                    {this.renderError(message)}
                </td>
            </Fragment>
        );

        if (this.state.isSocial) {
            passwordInput = null;
        }

        return (
            <form onSubmit={this.handleDeleteAccountFormSubmit}>
                <h3 className="profile-table-header">Delete account</h3>
                <table className="profile-table">
                    <tbody>
                        <tr>{passwordInput}</tr>
                        <tr>
                            <td colSpan="2">
                                <div className="save-container">
                                    <button
                                        type="submit"
                                        name="submit"
                                        className="button button--danger button--lg"
                                        disabled={loading}
                                        data-testid="deletePasswordSubmit"
                                    >
                                        {deleteAccountText}
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }

    renderBasicForm() {
        const { currentUser } = this.props;

        return (
            <div>
                <header>
                    {this.renderAvatar(currentUser, this.state.basicFormValues)}
                    <Link
                        to={`/artist/${currentUser.profile.url_slug}`}
                        className="edit-profile-link"
                    >
                        Cancel
                    </Link>
                </header>
                <table className="profile-table">
                    {this.renderTbody(
                        currentUser,
                        fields,
                        this.state.basicFormValues
                    )}
                </table>
            </div>
        );
    }

    renderContent(active) {
        switch (active) {
            case 'basic':
                return this.renderBasicForm();

            case 'password':
                return this.renderNewPasswordForm();

            case 'email':
                return this.renderNewEmailForm();

            case 'url':
                return this.renderNewSlugForm();

            case 'delete':
                return this.renderDeleteForm();

            case 'notifications':
                return this.renderNotificationsForm();

            default:
                return null;
        }
    }

    render() {
        const { currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            return null;
        }

        const contextItems = this.getContextSwitcherItems(this.state.activeTab);

        return (
            <div className="profile-container">
                <ContextSwitcher
                    items={contextItems}
                    onContextSwitch={this.handleContextSwitch}
                />
                {this.renderContent(this.state.activeTab)}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        notificationSettings: state.currentUserNotificationSettings
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(connectDataFetchers(ProfileEditContainer, [() => getUserSettings()]));
