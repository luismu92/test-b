import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { parse } from 'query-string';

import connectDataFetchers from 'lib/connectDataFetchers';
import BrowseArtistMeta from 'components/BrowseArtistMeta';

import { setBannerDeepLink } from '../redux/modules/global';
import { setActiveMarker, NAV_MARKER_FEED } from '../redux/modules/nav';
import {
    fetchArtistList,
    nextPage as nextArtistBrowsePage,
    reset as resetArtistBrowsePage
} from '../redux/modules/artist/browse';
import {
    nextPage as nextUserFollowingPage,
    reset as resetUserFollowingPage
} from '../redux/modules/user/following';
import {
    getUserFeed,
    nextPage as nextUserFeedPage,
    reset as resetUserFeedPage
} from '../redux/modules/user/feed';

import ListContainer from '../list/ListContainer';
import UserList from '../list/UserList';
import ContextSwitcher from '../widgets/ContextSwitcher';

// Kinda hacky because we dont want all /artist/:part
// urls to render this component. For example the
// artist profile page needs to render that specific
// component. Because of that we need to basically
// read the url and decide which request to make.
const urlContextMap = {
    feed: getUserFeed,
    popular: fetchArtistList
};

const contextListTypeMap = {
    feed: 'song',
    popular: 'user'
};

class FeedContainer extends Component {
    static propTypes = {
        location: PropTypes.object,
        match: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserFeed: PropTypes.object,
        currentUserFollowing: PropTypes.object,
        artistBrowse: PropTypes.object,
        dispatch: PropTypes.func
    };

    componentDidMount() {
        const { dispatch } = this.props;
        const link = this.getDeepLink(this.props);

        dispatch(
            setBannerDeepLink(link, {
                key: parse(this.props.location.search).key
            })
        );
    }

    componentDidUpdate(prevProps) {
        const { dispatch, match } = this.props;

        if (prevProps.match.params.context !== match.params.context) {
            const fn = urlContextMap[match.params.context];

            if (fn) {
                const link = this.getDeepLink(this.props);

                dispatch(fn());
                dispatch(
                    setBannerDeepLink(link, {
                        key: parse(this.props.location.search).key
                    })
                );
            }
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleNextPage = () => {
        const { dispatch, match } = this.props;
        const params = match.params;
        const fn = urlContextMap[params.context];

        switch (params.context) {
            case 'feed': {
                dispatch(nextUserFeedPage());
                dispatch(fn());
                break;
            }

            case 'popular': {
                dispatch(nextArtistBrowsePage());
                dispatch(fn());
                break;
            }

            case 'following': {
                dispatch(nextUserFollowingPage());
                dispatch(fn());
                break;
            }

            default:
                break;
        }
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getDeepLink(props) {
        const context = props.match.params.context;
        let urlSlug;

        if (!props.currentUser.isLoggedIn) {
            return null;
        }

        switch (context) {
            case 'timeline':
                urlSlug = `/artist/${props.currentUser.profile.url_slug}/feed`;
                break;

            case 'popular':
                urlSlug = '/popular';
                break;

            case 'following':
                urlSlug = `/artist/${
                    props.currentUser.profile.url_slug
                }/following`;
                break;

            default:
                return null;
        }

        const page = props.match.params.page;
        const pageSlug = page ? `/page/${page}` : '';

        return `audiomack:/${urlSlug}${pageSlug}`;
    }

    getCurrentPathFromParams(path, params) {
        return Object.keys(params).reduce((url, key) => {
            return url.replace(`:${key}`, params[key]);
        }, path);
    }

    getContextFromUrl(url) {
        return url
            .split('/')
            .filter(Boolean)
            .pop();
    }

    getList(context) {
        const {
            currentUser,
            currentUserFeed,
            currentUserFollowing,
            artistBrowse,
            location
        } = this.props;
        const listType = contextListTypeMap[context];

        if (listType === 'user') {
            let list = artistBrowse.list;
            let loading = artistBrowse.loading;
            let onLastPage = artistBrowse.onLastPage;

            if (context === 'following') {
                list = currentUserFollowing.list;
                loading = currentUserFollowing.loading;
                onLastPage = currentUserFollowing.onLastPage;
            }

            return (
                <UserList
                    items={list}
                    loading={loading}
                    isLastPage={onLastPage}
                    emptyState={this.renderEmptyState(currentUser, context)}
                    onNextPage={this.handleNextPage}
                />
            );
        }

        const songs = {
            data: currentUserFeed.list,
            loading: currentUserFeed.loading
        };

        return (
            <ListContainer
                location={location}
                songs={songs}
                listType={listType}
                emptyState={this.renderEmptyState(currentUser, context)}
                onNextPage={this.handleNextPage}
            />
        );
    }

    clearLists() {
        const { dispatch } = this.props;

        [
            resetArtistBrowsePage,
            resetUserFollowingPage,
            resetUserFeedPage
        ].forEach((fn) => dispatch(fn()));
    }

    renderEmptyState(currentUser, context) {
        let link;
        let text;

        if (!currentUser.isLoggedIn) {
            link = (
                <Link className="pill" to="/join">
                    Login / Signup
                </Link>
            );
        }

        switch (context) {
            case 'popular':
                text = 'You have no suggested followers at this time';
                break;

            case 'following':
                text = 'You are not following anyone';

                if (!currentUser.isLoggedIn) {
                    text = 'Log in to see who you follow';
                }
                break;

            default:
                text = 'Your feed is empty';
                if (!currentUser.isLoggedIn) {
                    text = 'Log in to see your feed';
                }
                break;
        }

        return (
            <div className="empty-state">
                <i className="glyphicons albums" />
                <p>{text}</p>
                <div className="pill-group">{link}</div>
            </div>
        );
    }

    renderHelmet(currentPath) {
        switch (currentPath) {
            case '/artists/popular':
                return <BrowseArtistMeta mobile />;

            default:
                return null;
        }
    }

    render() {
        const {
            location,
            currentUser,
            match: { params }
        } = this.props;
        const currentPath = location.pathname;

        let feedUrl = '/login';
        if (currentUser.isLoggedIn) {
            feedUrl = `/artist/${currentUser.profile.url_slug}/feed`;
        }

        const contextItems = [
            {
                text: 'Feed',
                href: feedUrl,
                active: currentPath === feedUrl
            },
            {
                text: 'Suggested Follows',
                href: '/artists/popular',
                active: currentPath === '/artists/popular'
            }
        ];

        const context = params.context;
        const list = this.getList(context);

        return (
            <div className="list-view">
                {this.renderHelmet(currentPath)}
                <ContextSwitcher items={contextItems} />
                {list}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        artist: state.artist,
        artistBrowse: state.artistBrowse,
        currentUser: state.currentUser,
        currentUserFeed: state.currentUserFeed,
        currentUserFollowing: state.currentUserFollowing,
        player: state.player,
        ad: state.ad
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(FeedContainer, [
        () => setActiveMarker(NAV_MARKER_FEED),
        (params) => {
            if (!urlContextMap[params.context]) {
                console.warn(
                    params.context,
                    'doesnt have an associated fetch fn'
                );
                return () => {};
            }

            return urlContextMap[params.context]();
        }
    ])
);
