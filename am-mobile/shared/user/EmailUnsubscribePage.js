import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AndroidLoader from '../loaders/AndroidLoader';

export default class EmailUnsubscribePage extends Component {
    static propTypes = {
        isUnsubsribing: PropTypes.bool,
        error: PropTypes.object,
        type: PropTypes.string
    };

    render() {
        let content = (
            <div className="u-text-center">
                <p>
                    All set. We've successfully removed your email address from
                    our {this.props.type} list.
                </p>
            </div>
        );

        if (this.props.isUnsubsribing) {
            content = (
                <div className="u-text-center">
                    <p>Unsubscribing your email…</p>
                    <AndroidLoader />
                </div>
            );
        }

        if (this.props.error) {
            content = (
                <div className="u-text-center">
                    <p>Failed to unsubscribe your email.</p>
                    <p>Sorry, there was an error unsubscribing your email.</p>
                </div>
            );
        }

        return (
            <div className="content-page">
                <div className="auth__bottom body-text">{content}</div>
            </div>
        );
    }
}
