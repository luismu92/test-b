import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import AndroidLoader from '../loaders/AndroidLoader';

export default class AccountRecovery extends Component {
    static propTypes = {
        token: PropTypes.string.isRequired,
        isRecoveringAccount: PropTypes.bool,
        recoveryError: PropTypes.string,
        isVerifyingToken: PropTypes.bool,
        isValidToken: PropTypes.bool,
        onFormSubmit: PropTypes.func
    };

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="error">{error}</p>;
    }

    render() {
        if (this.props.isVerifyingToken) {
            return (
                <div className="text-center">
                    <h2>Verifying password link…</h2>
                    <AndroidLoader />
                </div>
            );
        }

        if (!this.props.isValidToken) {
            return (
                <div className="body-text body-text--no-padding">
                    <h2>Password reset link error</h2>
                    <p>
                        There was an error validating the password reset link.
                        This probably happened because the link has expired. Try
                        using the{' '}
                        <Link to="/forgot-password">forgot password form</Link>{' '}
                        again to generate a new link.
                    </p>
                    <p>
                        Still having issues?{' '}
                        <Link to="/contact-us">Contact us</Link>.
                    </p>
                </div>
            );
        }

        return (
            <form
                action="/auth/recover-account"
                method="post"
                onSubmit={this.props.onFormSubmit}
            >
                <p>Enter a new password to get started.</p>
                <input
                    type="hidden"
                    value={this.props.token}
                    name="token"
                    readOnly
                />
                <p>
                    <label>
                        New password:
                        <input
                            type="password"
                            name="password"
                            autoComplete="new-password"
                            autoCapitalize="none"
                            defaultValue=""
                            required
                        />
                    </label>
                </p>
                {this.renderError(this.props.recoveryError)}
                <p className="text-center">
                    <input
                        type="submit"
                        className="button button--lg form-container__submit"
                        disabled={this.props.isRecoveringAccount}
                        value={
                            this.props.isRecoveringAccount
                                ? 'Updating password…'
                                : 'Set new password'
                        }
                    />
                </p>
            </form>
        );
    }
}
