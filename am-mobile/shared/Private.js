import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Private extends Component {
    static propTypes = {
        type: PropTypes.string,
        errors: PropTypes.array
    };

    render() {
        const message = (
            <div>
                <p>This {this.props.type} is not available</p>
            </div>
        );

        return message;
    }
}
