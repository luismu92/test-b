import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import Cookie from 'js-cookie';
import { parse } from 'query-string';

import GlobalMeta from 'components/GlobalMeta';
import analytics from 'utils/analytics';
import connectDataFetchers from 'lib/connectDataFetchers';
import { uuid, getDynamicImageProps } from 'utils/index';
import { cookies } from 'constants/index';
import { ENVIRONMENT_MOBILE } from 'constants/stats/environment';

import {
    MODAL_TYPE_APP_DOWNLOAD,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_TRENDING,
    MODAL_TYPE_COPY_URL,
    MODAL_TYPE_ADD_TO_PLAYLIST,
    MODAL_TYPE_VERIFY_EMAIL,
    MODAL_TYPE_VERIFY_PASSWORD_TOKEN,
    MODAL_TYPE_INPUT_FORM,
    hideModal
} from './redux/modules/modal';
import { setBannerDeepLink } from './redux/modules/global';
import { logOut } from './redux/modules/user/index';
import { hideDrawer } from './redux/modules/drawer';
import {
    setEnvironment,
    getStatsToken,
    setReferer
} from './redux/modules/stats';
import { setLastDisplayTime } from './redux/modules/ad';
import { showMessage } from './redux/modules/message';
import { showModal } from './redux/modules/modal';
import { NAV_MARKER_TRENDING } from './redux/modules/nav';

import Header from './Header';
import OuttageHeader from './OuttageHeader';
import PlayerContainer from './PlayerContainer';
import AppDownloadModal from './modal/AppDownloadModal';
import ConfirmModal from './modal/ConfirmModal';
import TrendingModal from './modal/TrendingModal';
import CopyUrlModal from './modal/CopyUrlModal';
import AddToPlaylistModal from './modal/AddToPlaylistModal';
import InputFormModal from './modal/InputFormModal';
import VideoAdContainer from './ad/VideoAdContainer';
import Drawer from './Drawer';
import NavBarContainer from './NavBarContainer';
import Footer from './Footer';
import BannerAd from './ad/BannerAd';
import Message from './Message';
import BrowseContextContainer from './browse/BrowseContextContainer';

class AppContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        children: PropTypes.object,
        currentUser: PropTypes.object,
        modal: PropTypes.object,
        drawer: PropTypes.object,
        player: PropTypes.object,
        history: PropTypes.object,
        match: PropTypes.object,
        songs: PropTypes.object,
        global: PropTypes.object,
        location: PropTypes.object,
        message: PropTypes.object,
        environment: PropTypes.object,
        section: PropTypes.object,
        nav: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            menuActive: false,
            VerifyEmailModal: null,
            VerifyPasswordTokenModal: null
        };
    }

    componentDidMount() {
        const { dispatch, location } = this.props;
        const query = parse(location.search);

        dispatch(getStatsToken(uuid()));
        dispatch(setEnvironment(ENVIRONMENT_MOBILE));

        if (query.referer) {
            const hourExpiration = 1000 * 60 * 60;
            dispatch(
                setReferer(decodeURIComponent(query.referer), hourExpiration)
            );
        }

        this.maybeLoadVerifyEmailModal(query.verifyHash);
        this.maybeLoadVerifyPasswordTokenModal(query.verifyPasswordToken);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, location } = this.props;
        const currentPathname = location.pathname;
        const prevPathname = prevProps.location.pathname;

        if (currentPathname !== prevPathname) {
            dispatch(setBannerDeepLink(null));
            this.handleLocationChange(this.props);
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleDrawerItemClick = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        switch (action) {
            case 'log-out':
                dispatch(logOut());
                break;

            default:
                break;
        }

        dispatch(hideDrawer());
    };

    handleModalClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleLocationChange = (nextProps) => {
        if (nextProps.history.action !== 'POP') {
            window.scrollTo(0, 0);
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getHeaderBarTitle() {
        return 'Browse';
    }

    maybeLoadVerifyEmailModal(hash) {
        const { currentUser, dispatch } = this.props;

        if (window.location.hash === '#verifyHashSuccess') {
            dispatch(showMessage('Thanks for verifying your email!'));

            window.location.hash = '';
        }

        if (
            !hash ||
            (currentUser.isLoggedIn && currentUser.profile.verified_email)
        ) {
            return;
        }

        new Promise((resolve) => {
            require.ensure([], (require) => {
                const VerifyEmailModal = require('./modal/VerifyEmailModal')
                    .default;

                resolve({
                    VerifyEmailModal
                });
            });
        })
            .then(({ VerifyEmailModal }) => {
                this.setState(
                    {
                        VerifyEmailModal
                    },
                    () => {
                        this.props.dispatch(
                            showModal(MODAL_TYPE_VERIFY_EMAIL, { hash })
                        );
                    }
                );
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    maybeLoadVerifyPasswordTokenModal(token) {
        const { dispatch, currentUser } = this.props;

        if (currentUser.isLoggedIn && !token) {
            return;
        }

        if (window.location.hash === '#recoverAccountSuccess') {
            dispatch(
                showMessage(
                    'Password updated! Please login using your new password.'
                )
            );

            window.location.hash = '';
        }

        if (!token) {
            return;
        }

        new Promise((resolve) => {
            require.ensure([], (require) => {
                const VerifyPasswordTokenModal = require('./modal/VerifyPasswordTokenModal')
                    .default;

                resolve({
                    VerifyPasswordTokenModal
                });
            });
        })
            .then(({ VerifyPasswordTokenModal }) => {
                this.setState(
                    {
                        VerifyPasswordTokenModal
                    },
                    () => {
                        dispatch(
                            showModal(MODAL_TYPE_VERIFY_PASSWORD_TOKEN, {
                                token
                            })
                        );
                    }
                );
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    render() {
        const { location, player } = this.props;
        const {
            headerHidden,
            background,
            deepLink,
            deepLinkData
        } = this.props.global;
        const { activeMarker } = this.props.nav;
        const { currentSong, hidden: playerHidden } = player;
        const klass = classnames('mobile-wrap', {
            'mobile-wrap--padding': !headerHidden
        });

        let artworkBg;

        if (background) {
            const size = 160;
            const [image, srcSet] = getDynamicImageProps(background, {
                size: size,
                max: true,
                blur: 20
            });

            artworkBg = (
                <div className="artwork-bg">
                    <img src={image} srcSet={srcSet} alt="" />
                </div>
            );
        }

        const showOuttageHeader = false;
        let outtageHeader;

        if (showOuttageHeader) {
            outtageHeader = <OuttageHeader />;
        }

        let { VerifyEmailModal, VerifyPasswordTokenModal } = this.state;

        if (VerifyEmailModal) {
            VerifyEmailModal = (
                <VerifyEmailModal onClose={this.handleModalClose} />
            );
        }

        if (VerifyPasswordTokenModal) {
            VerifyPasswordTokenModal = (
                <VerifyPasswordTokenModal onClose={this.handleModalClose} />
            );
        }

        let contextSwitcher;
        if (
            activeMarker === NAV_MARKER_TRENDING &&
            !this.props.global.background
        ) {
            contextSwitcher = (
                <BrowseContextContainer
                    songs={this.props.songs}
                    dispatch={this.props.dispatch}
                />
            );
        }

        return (
            <div className={klass}>
                {artworkBg}
                <GlobalMeta location={location} />
                <Header
                    onMenuToggle={this.handleMenuOpen}
                    loading={this.props.songs.loading}
                    title={this.getHeaderBarTitle(this.props)}
                    deepLink={deepLink}
                    deepLinkData={deepLinkData}
                />
                {outtageHeader}
                <div className="main-content">
                    {contextSwitcher}
                    {this.props.children}
                </div>
                <BannerAd
                    bottom={currentSong && !playerHidden ? 114 : 60}
                    currentUser={this.props.currentUser}
                    location={this.props.location}
                />
                <PlayerContainer />
                <VideoAdContainer location={this.props.location} />
                <Drawer
                    items={this.props.drawer.items}
                    visible={this.props.drawer.visible}
                    onDrawerItemClick={this.handleDrawerItemClick}
                />
                <Message
                    visible={this.props.message.visible}
                    message={this.props.message.message}
                />
                <NavBarContainer
                    match={this.props.match}
                    location={this.props.location}
                    history={this.props.history}
                />
                <AppDownloadModal
                    onClose={this.handleModalClose}
                    active={this.props.modal.type === MODAL_TYPE_APP_DOWNLOAD}
                    item={this.props.modal.extraData.item}
                    keyPassword={this.props.location.key}
                />
                <ConfirmModal
                    onClose={this.handleModalClose}
                    active={this.props.modal.type === MODAL_TYPE_CONFIRM}
                    title={this.props.modal.extraData.title}
                    message={this.props.modal.extraData.message}
                    onConfirm={this.props.modal.extraData.handleConfirm}
                    onCancel={this.handleModalClose}
                />
                <TrendingModal
                    onClose={this.handleModalClose}
                    active={this.props.modal.type === MODAL_TYPE_TRENDING}
                    title={this.props.modal.extraData.title}
                    trendData={this.props.modal.extraData.trendData || {}}
                />
                <CopyUrlModal
                    onClose={this.handleModalClose}
                    active={this.props.modal.type === MODAL_TYPE_COPY_URL}
                    url={this.props.modal.extraData.url}
                />
                <AddToPlaylistModal
                    onClose={this.handleModalClose}
                    active={
                        this.props.modal.type === MODAL_TYPE_ADD_TO_PLAYLIST
                    }
                />
                <InputFormModal
                    onClose={this.handleModalClose}
                    active={this.props.modal.type === MODAL_TYPE_INPUT_FORM}
                    message={this.props.modal.extraData.message}
                    onSubmit={this.props.modal.extraData.handleSubmit}
                    submitButtonText={
                        this.props.modal.extraData.submitButtonText
                    }
                    inputType={this.props.modal.extraData.inputType}
                    inputPlaceholder={
                        this.props.modal.extraData.inputPlaceholder
                    }
                />
                {VerifyEmailModal}
                {VerifyPasswordTokenModal}
                <Footer
                    deepLink={deepLink}
                    deepLinkData={deepLinkData}
                    location={location}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        songs: state.music,
        modal: state.modal,
        search: state.search,
        player: state.player,
        currentUser: state.currentUser,
        global: state.global,
        artist: state.artist,
        drawer: state.drawer,
        message: state.message,
        section: state.section,
        nav: state.nav
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(AppContainer, [
            () => {
                if (!process.env.BROWSER) {
                    return null;
                }

                const lastAdDisplayTime = Cookie.get(cookies.lastAdDisplayTime);

                if (!lastAdDisplayTime) {
                    return null;
                }

                return setLastDisplayTime(lastAdDisplayTime);
            }
        ])
    )
);
