import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Modal from './Modal';
import { MODAL_TYPE_GHOST_CLAIM } from '../redux/modules/modal';

import GhostClaimPageContainer from '../user/GhostClaimPageContainer';

import styles from './Modal.module.scss';

class VerifyEmailModal extends Component {
    static propTypes = {
        modal: PropTypes.object.isRequired,
        onClose: PropTypes.func
    };

    render() {
        const { modal } = this.props;

        return (
            <Modal
                className={styles.light}
                isOpen={modal.type === MODAL_TYPE_GHOST_CLAIM}
                onClose={this.props.onClose}
                showCloseButton={false}
                showOverlay
            >
                <p>
                    To log in to your account and use all the features of
                    Audiomack, please create a password.
                </p>
                <GhostClaimPageContainer
                    email={modal.extraData.email}
                    code={modal.extraData.code}
                    id={modal.extraData.id}
                    urlSlug={modal.extraData.urlSlug}
                />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(VerifyEmailModal);
