import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Modal from './Modal';
import { MODAL_TYPE_VERIFY_PASSWORD_TOKEN } from '../redux/modules/modal';

import AccountRecoveryContainer from '../user/AccountRecoveryContainer';

import styles from './Modal.module.scss';

class VerifyPasswordTokenModal extends Component {
    static propTypes = {
        modal: PropTypes.object.isRequired,
        onClose: PropTypes.func
    };

    render() {
        const { modal } = this.props;

        return (
            <Modal
                className={styles.light}
                isOpen={modal.type === MODAL_TYPE_VERIFY_PASSWORD_TOKEN}
                onClose={this.props.onClose}
                showCloseButton={false}
                showOverlay
            >
                <AccountRecoveryContainer token={modal.extraData.token} />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(VerifyPasswordTokenModal);
