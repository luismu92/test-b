import React, { Component, Fragment } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Transition, {
    EXITED,
    ENTERING,
    ENTERED,
    EXITING
} from 'react-transition-group/Transition';

import CloseIcon from '../../../am-shared/icons/close-thin';

import styles from './Modal.module.scss';

export default class Modal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        isOpen: PropTypes.bool,
        showCloseButton: PropTypes.bool,
        showOverlay: PropTypes.bool,
        title: PropTypes.string,
        className: PropTypes.string,
        overlayTransitionEnterTimeout: PropTypes.number,
        overlayTransitionLeaveTimeout: PropTypes.number,
        modalTransitionEnterTimeout: PropTypes.number,
        modalTransitionLeaveTimeout: PropTypes.number,
        children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    };

    static defaultProps = {
        isOpen: false,
        showOverlay: true,
        showCloseButton: true,
        overlayTransitionEnterTimeout: 100,
        overlayTransitionLeaveTimeout: 200,
        modalTransitionEnterTimeout: 300,
        modalTransitionLeaveTimeout: 100
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.addEventListener('keydown', this.handleKeyDown);
    }

    componentDidUpdate(prevProps) {
        if (this.props.isOpen && !prevProps.isOpen) {
            this.setOpenClass();
            return;
        }

        this.setCloseClass();
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyDown);
    }

    handleKeyDown = (e) => {
        const ESC = 27;

        if (e.keyCode === ESC) {
            this.close();
        }
    };

    handleOverlayClick = (e) => {
        this.close(e);
    };

    getModalRoot() {
        const modalRootId = 'am-modal';
        let modalRoot = document.getElementById(modalRootId);

        if (!modalRoot) {
            modalRoot = document.createElement('div');

            modalRoot.id = modalRootId;
            modalRoot.className = styles.modalRoot;
            document.body.appendChild(modalRoot);
        }

        return modalRoot;
    }

    setOpenClass() {
        document.body.classList.add('modal-open');
    }

    setCloseClass() {
        document.body.classList.remove('modal-open');
    }

    open() {
        this.setOpenClass();
    }

    close(e) {
        this.setCloseClass();
        this.props.onClose(e);
    }

    renderModal = (state) => {
        let header = null;
        let closeButton = null;

        if (this.props.title) {
            header = (
                <header className={styles.header}>
                    <h4 className={styles.headerTitle}>{this.props.title}</h4>
                </header>
            );
        }

        if (this.props.showCloseButton) {
            closeButton = (
                <button
                    className={classnames(styles.close, 'close')}
                    onClick={this.handleOverlayClick}
                >
                    <CloseIcon className="u-d-block" />
                </button>
            );
        }

        const klass = classnames(styles.modal, 'modal', {
            [this.props.className]: this.props.className,
            [styles.modalLeaveActive]: state === EXITED,
            [styles.modalEnter]: state === ENTERING,
            [styles.modalEnterActive]: state === ENTERED,
            [styles.modalLeave]: state === EXITING
        });

        return createPortal(
            <div className={klass} key="modal">
                {closeButton}
                {header}
                <div className={classnames(styles.content, 'content')}>
                    {this.props.children}
                </div>
            </div>,
            this.getModalRoot()
        );
    };

    renderModalOverlay = (state) => {
        const klass = classnames(styles.overlay, {
            [styles.overlayLeaveActive]: state === EXITED,
            [styles.overlayEnter]: state === ENTERING,
            [styles.overlayEnterActive]: state === ENTERED,
            [styles.overlayLeave]: state === EXITING
        });

        return createPortal(
            /* eslint-disable-next-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
            <div className={klass} onClick={this.handleOverlayClick} key="o" />,
            this.getModalRoot()
        );
    };

    renderOverlay() {
        if (!this.props.showOverlay) {
            return null;
        }

        const transitionProps = {
            in: this.props.isOpen,
            mountOnEnter: true,
            unmountOnExit: true,
            timeout: {
                enter: this.props.overlayTransitionEnterTimeout,
                exit: this.props.overlayTransitionLeaveTimeout
            }
        };

        return (
            <Transition {...transitionProps}>
                {this.renderModalOverlay}
            </Transition>
        );
    }

    render() {
        const transitionProps = {
            in: this.props.isOpen,
            mountOnEnter: true,
            unmountOnExit: true,
            timeout: {
                enter: this.props.modalTransitionEnterTimeout,
                exit: this.props.modalTransitionLeaveTimeout
            }
        };

        return (
            <Fragment>
                {this.renderOverlay()}
                <Transition {...transitionProps}>{this.renderModal}</Transition>
            </Fragment>
        );
    }
}
