import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Avatar from '../Avatar';
import FacebookIcon from '../icons/facebook-letter-logo';
import InstagramIcon from '../icons/instagram';
import TwitterIcon from '../icons/twitter-logo';
import GlobeIcon from 'icons/globe-wire';

import Modal from './Modal';
import Verified from 'components/Verified';

import styles from './ArtistInfoModal.module.scss';

export default class ArtistInfoModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool,
        artist: PropTypes.object
    };

    static defaultProps = {
        active: false
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    //////////////////////
    // Internal methods //
    //////////////////////

    getFollowerCount(artist) {
        const count = artist.profile.followers_count;
        let text = 'Followers';

        if (count === 1) {
            text = 'Follower';
        }

        return (
            <span>
                <span className="u-text-orange u-margin-0">{count}</span> {text}
            </span>
        );
    }

    getFollowingCount(artist) {
        const count = artist.profile.following_count;
        const text = 'Following';

        return (
            <span>
                <span className="u-text-orange u-margin-0">{count}</span> {text}
            </span>
        );
    }

    getAvatar(image) {
        const center = true;

        return <Avatar image={image} size={150} rounded center={center} />;
    }

    renderAvatar(artist) {
        return (
            <div className="avatar-wrap">
                {this.getAvatar(artist.profile.image)}
            </div>
        );
    }

    renderTableBody(artist) {
        const keyToLabel = {
            url: 'Website',
            genre: 'Genre',
            label: 'Label',
            hometown: 'Hometown',
            bio: 'Bio'
        };

        const rows = Object.keys(keyToLabel)
            .map((key, i) => {
                let info = artist.profile[key];
                const lineClass = classnames('artist-info-table--line', {
                    bio: key === 'bio'
                });

                if (!info) {
                    return null;
                }

                if (key === 'url') {
                    info = (
                        <a href={info} target="_blank" rel="nofollow noopener">
                            {info}
                        </a>
                    );
                }

                return (
                    <div className={lineClass} key={i}>
                        <p className="artist-info-table--label">
                            {keyToLabel[key]}
                        </p>
                        <p className="artist-info-table--info">{info}</p>
                    </div>
                );
            })
            .filter(Boolean);

        return <div className="artist-info-table">{rows}</div>;
    }

    renderSocialNetworks(artist) {
        let url;
        let twitter;
        let facebook;
        let instagram;

        if (artist.profile.url) {
            url = (
                <a
                    className=""
                    href={artist.profile.url}
                    rel="nofollow noopener"
                    target="_blank"
                >
                    <GlobeIcon />
                </a>
            );
        }

        if (artist.profile.twitter) {
            twitter = (
                <a
                    className=""
                    href={`https://twitter.com/${artist.profile.twitter}`}
                    rel="nofollow noopener"
                    target="_blank"
                >
                    <TwitterIcon />
                </a>
            );
        }

        if (artist.profile.facebook) {
            facebook = (
                <a
                    className=""
                    href={artist.profile.facebook}
                    rel="nofollow noopener"
                    target="_blank"
                >
                    <FacebookIcon />
                </a>
            );
        }

        if (artist.profile.instagram) {
            instagram = (
                <a
                    className=""
                    href={`https://instagram.com/${artist.profile.instagram}`}
                    rel="nofollow noopener"
                    target="_blank"
                >
                    <InstagramIcon />
                </a>
            );
        }
        return (
            <div className="artist-networks">
                {url}
                {twitter}
                {facebook}
                {instagram}
            </div>
        );
    }

    render() {
        const { artist } = this.props;

        const check = <Verified user={artist.profile} size={20} />;

        return (
            <Modal
                className={classnames(styles.modal, 'artist-info')}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay={false}
            >
                {this.renderAvatar(artist)}
                <div className={styles.content}>
                    <h2 className="artist-meta__name">
                        {artist.profile.name} {check}
                    </h2>
                    <p className="artist-meta__follow">
                        {this.getFollowerCount(artist)}
                        {this.getFollowingCount(artist)}
                    </p>
                </div>

                {this.renderSocialNetworks(artist)}

                <div className="artist-info-table">
                    {this.renderTableBody(artist)}
                </div>
            </Modal>
        );
    }
}
