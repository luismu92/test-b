import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './ConfirmModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

export default class ConfirmModal extends Component {
    static propTypes = {
        active: PropTypes.bool,
        onClose: PropTypes.func,
        onConfirm: PropTypes.func,
        onCancel: PropTypes.func,
        title: PropTypes.string,
        confirmButtonText: PropTypes.string,
        message: PropTypes.string,
        confirmButtonProps: PropTypes.object
    };

    static defaultProps = {
        confirmButtonText: 'OK',
        confirmButtonProps: {}
    };

    render() {
        const {
            title,
            message,
            onConfirm,
            onCancel,
            confirmButtonText,
            confirmButtonProps
        } = this.props;

        return (
            <Modal
                className={classnames(styles.base.takedown, styles.base.dark)}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay
            >
                <div className={styles.variant.content}>
                    <h1
                        className={classnames(
                            styles.base.title,
                            styles.variant.title,
                            'u-ls-n-01',
                            'u-spacing-bottom-5'
                        )}
                    >
                        {title}
                    </h1>
                    <p className="u-spacing-bottom-20">{message}</p>
                    <div className={`button-group ${styles.base.buttonGroup}`}>
                        <button
                            className={classnames(
                                'button',
                                styles.base.button,
                                styles.variant.button
                            )}
                            onClick={onConfirm}
                            type="button"
                            {...confirmButtonProps}
                        >
                            {confirmButtonText}
                        </button>
                        <button
                            className={classnames(
                                'button',
                                styles.base.buttonCancel,
                                styles.variant.button
                            )}
                            onClick={onCancel}
                        >
                            Cancel
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}
