import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import moment from 'moment';

import { DEFAULT_DATE_FORMAT } from 'constants/index';

import Avatar from '../Avatar';
import MusicStats from '../components/MusicStats';

import Modal from './Modal';
import styles from './SongInfoModal.module.scss';

export default class SongInfoModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool,
        song: PropTypes.object,
        history: PropTypes.object
    };

    static defaultProps = {
        active: false
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    // Since portals are rendered at the bottom of the DOM, we will get an
    // error when trying to use a Link component that is outside of a Router
    // component. This is a workaround for now
    handleLinkClick = (e) => {
        e.preventDefault();

        this.props.history.push(e.target.href);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    renderAddedDate(song) {
        const date = moment(song.released * 1000).format(DEFAULT_DATE_FORMAT);

        return (
            <small>
                Added on {date} by{' '}
                <a
                    href={`/artist/${song.uploader.url_slug}`}
                    onClick={this.handleLinkClick}
                >
                    {song.uploader.name}
                </a>
            </small>
        );
    }

    renderRanks(song) {
        return (
            <div className="rankings">
                <div className="rankings__rank">
                    <p className="rankings__title">Today</p>
                    <p className="rankings__value">
                        {song.stats.rankings.daily}
                    </p>
                </div>
                <div className="rankings__rank">
                    <p className="rankings__title">Week</p>
                    <p className="rankings__value">
                        {song.stats.rankings.weekly}
                    </p>
                </div>
                <div className="rankings__rank">
                    <p className="rankings__title">Month</p>
                    <p className="rankings__value">
                        {song.stats.rankings.monthly}
                    </p>
                </div>
                <div className="rankings__rank">
                    <p className="rankings__title">Year</p>
                    <p className="rankings__value">
                        {song.stats.rankings.yearly}
                    </p>
                </div>
            </div>
        );
    }

    renderFeaturing(song) {
        if (!song.featuring) {
            return null;
        }

        return <p>Feat. {song.featuring}</p>;
    }

    renderAvatar(song) {
        const props = {
            center: true,
            image: song.image,
            rounded: false,
            size: 150
        };

        return (
            <div className="avatar-wrap">
                <Avatar {...props} />
            </div>
        );
    }

    renderTableBody(artist) {
        const keyToLabel = {
            album: 'Album',
            producer: 'Producer',
            genre: 'Genre',
            description: 'Description'
        };

        const rows = Object.keys(keyToLabel)
            .map((key, i) => {
                let info = artist[key];

                if (!info) {
                    return null;
                }

                if (key === 'genre') {
                    info = <Link to={`/${info}/songs/week`}>{info}</Link>;
                }

                if (key === 'url') {
                    info = (
                        <a href={info} target="_blank" rel="nofollow">
                            {info}
                        </a>
                    );
                }

                return (
                    <div className={`artist-info-table--line ${key}`} key={i}>
                        <p className="artist-info-table--label">
                            {keyToLabel[key]}:
                        </p>
                        <p className="artist-info-table--info">{info}</p>
                    </div>
                );
            })
            .filter(Boolean);

        return <div className="artist-info-table">{rows}</div>;
    }

    render() {
        const { song } = this.props;

        return (
            <Modal
                className={classnames(styles.modal, 'song-info')}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay={false}
            >
                {this.renderAvatar(song)}
                <div className={styles.content}>
                    <p className="artist-meta__follow">{song.artist}</p>
                    <h2 className="artist-meta__name">{song.title}</h2>
                    {this.renderFeaturing(song)}
                    {this.renderAddedDate(song)}
                    <MusicStats stats={song.stats} />
                </div>

                {this.renderRanks(song)}
                {this.renderTableBody(song)}
            </Modal>
        );
    }
}
