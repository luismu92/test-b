import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './InputFormModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

export default class InputFormModal extends Component {
    static propTypes = {
        active: PropTypes.bool,
        onClose: PropTypes.func,
        onSubmit: PropTypes.func,
        inputType: PropTypes.string,
        inputPlaceholder: PropTypes.string,
        submitButtonText: PropTypes.string,
        message: PropTypes.string
    };

    static defaultProps = {
        submitButtonText: 'Submit',
        inputType: 'text',
        inputPlaceholder: ''
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const { onSubmit } = this.props;
        const form = e.currentTarget;
        const inputValue = form.inputValue.value;

        onSubmit(inputValue);
    };

    render() {
        const {
            message,
            submitButtonText,
            inputType,
            inputPlaceholder
        } = this.props;

        return (
            <Modal
                className={classnames(styles.base.takedown, styles.base.dark)}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showCloseButton={true}
                showOverlay
            >
                <form
                    className={classnames(
                        styles.variant.content,
                        'column small-24 u-padding-0'
                    )}
                    onSubmit={this.handleFormSubmit}
                >
                    <p className={styles.variant.message}>{message}</p>
                    <input
                        type={inputType}
                        name="inputValue"
                        className={styles.variant.input}
                        placeholder={inputPlaceholder}
                        autoCapitalize="none"
                    />
                    <button
                        type="submit"
                        className={classnames(
                            'button',
                            styles.base.button,
                            styles.variant.button
                        )}
                    >
                        {submitButtonText}
                    </button>
                </form>
            </Modal>
        );
    }
}
