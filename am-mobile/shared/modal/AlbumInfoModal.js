import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import Avatar from '../Avatar';
import MusicStats from '../components/MusicStats';

import Modal from './Modal';
import styles from './ArtistInfoModal.module.scss';

export default class AlbumInfoModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool,
        album: PropTypes.object,
        history: PropTypes.object
    };

    static defaultProps = {
        active: false
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    // Since portals are rendered at the bottom of the DOM, we will get an
    // error when trying to use a Link component that is outside of a Router
    // component. This is a workaround for now
    handleLinkClick = (e) => {
        e.preventDefault();

        this.props.history.push(e.target.href);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getAvatar(image) {
        const center = true;

        return <Avatar image={image} size={150} center={center} />;
    }

    renderAvatar(image) {
        return <div className="avatar-wrap">{this.getAvatar(image)}</div>;
    }

    renderRanks(rankings) {
        if (!rankings) {
            return null;
        }

        return (
            <div className="rankings">
                <div className="rankings__rank">
                    <p className="rankings__title">Today</p>
                    <p className="rankings__value">{rankings.daily}</p>
                </div>
                <div className="rankings__rank">
                    <p className="rankings__title">Week</p>
                    <p className="rankings__value">{rankings.weekly}</p>
                </div>
                <div className="rankings__rank">
                    <p className="rankings__title">Month</p>
                    <p className="rankings__value">{rankings.monthly}</p>
                </div>
                <div className="rankings__rank">
                    <p className="rankings__title">Year</p>
                    <p className="rankings__value">{rankings.yearly}</p>
                </div>
            </div>
        );
    }

    renderTableBody(album) {
        const keyToLabel = {
            genre: 'Genre',
            label: 'Label',
            hometown: 'Hometown',
            bio: 'Bio'
        };

        const rows = Object.keys(keyToLabel)
            .map((key, i) => {
                let info = album[key];

                if (!info) {
                    return null;
                }

                if (key === 'genre') {
                    info = <Link to={`${key}/albums/week`}>{info}</Link>;
                }

                return (
                    <div className={`artist-info-table--line ${key}`} key={i}>
                        <p className="artist-info-table--label">
                            {keyToLabel[key]}
                        </p>
                        <p className="artist-info-table--info">{info}</p>
                    </div>
                );
            })
            .filter(Boolean);

        let description;

        if (album.description) {
            description = (
                <div className="artist-info-table--line">
                    <p className="artist-info-table--label">
                        Album Description
                    </p>
                    <p className="artist-info-table--info">
                        {album.description}
                    </p>
                </div>
            );
        }

        return (
            <div className="artist-info-table">
                {description}
                {rows}
            </div>
        );
    }

    render() {
        const { album } = this.props;
        let check;

        if (album.uploader.verified) {
            check = <span className="glyphicons ok orange-check" />;
        }

        return (
            <Modal
                className={classnames(styles.modal, 'artist-info')}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay={false}
            >
                {this.renderAvatar(album.image)}
                <div className="text-center">
                    <p>{album.artist}</p>
                    <h2 className="artist-meta__name">{album.title}</h2>
                    <small className="album-container__uploader">
                        Added by{' '}
                        <a
                            href={`/artist/${album.uploader.url_slug}`}
                            onClick={this.handleLinkClick}
                        >
                            {album.uploader.name} {check}
                        </a>
                    </small>
                    <MusicStats stats={album.stats} />
                    {this.renderRanks(album.stats.rankings)}
                </div>
                {this.renderTableBody(album)}
            </Modal>
        );
    }
}
