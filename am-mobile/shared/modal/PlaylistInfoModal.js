import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Avatar from '../Avatar';

import Modal from './Modal';
import styles from './SongInfoModal.module.scss';

export default class PlaylistInfoModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool,
        playlist: PropTypes.object,
        history: PropTypes.object
    };

    static defaultProps = {
        active: false
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLinkClick = (e) => {
        e.preventDefault();

        this.props.history.push(e.target.href);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getAvatar(image) {
        const center = true;

        return <Avatar image={image} size={150} center={center} />;
    }

    renderAvatar(image) {
        return <div className="avatar-wrap">{this.getAvatar(image)}</div>;
    }

    renderTableBody(playlist) {
        const keyToLabel = {
            genre: 'Genre',
            label: 'Label',
            hometown: 'Hometown',
            bio: 'Bio'
        };

        const rows = Object.keys(keyToLabel)
            .map((key, i) => {
                const info = playlist[key];

                if (!info) {
                    return null;
                }

                return (
                    <tr className="artist-info-table--line" key={i}>
                        <td className="artist-info-table--label">
                            {keyToLabel[key]}
                        </td>
                        <td className="artist-info-table--info">{info}</td>
                    </tr>
                );
            })
            .filter(Boolean);

        return <tbody>{rows}</tbody>;
    }

    render() {
        const { playlist } = this.props;
        let check;

        if (playlist.artist.verified) {
            check = <span className="glyphicons ok orange-check" />;
        }

        return (
            <Modal
                className={classnames(styles.modal, 'playlist-info')}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay={false}
            >
                {this.renderAvatar(playlist.image)}
                <div className={styles.content}>
                    <h2 className="artist-meta__name">{playlist.title}</h2>
                    <small className="album-container__uploader">
                        Playlist creator:{' '}
                        <a
                            href={`/artist/${playlist.artist.url_slug}`}
                            onClick={this.handleLinkClick}
                        >
                            {playlist.artist.name} {check}
                        </a>
                    </small>
                </div>

                <table className="artist-info-table">
                    {this.renderTableBody(playlist)}
                </table>
            </Modal>
        );
    }
}
