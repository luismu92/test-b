import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Modal from './Modal';
import classnames from 'classnames';

import { getBranchUrl, getMusicUrl } from 'utils/index';

import AmLogo from '../icons/am-logo-full';

import styles from './AppDownloadModal.module.scss';

export default class AppDownloadModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool,
        item: PropTypes.object,
        keyPassword: PropTypes.string
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const { item, keyPassword } = this.props;

        if (!item) {
            return null;
        }

        const appDeepLink = getMusicUrl(item, {
            host: 'audiomack:/'
        });

        const branchUrl = getBranchUrl(process.env.BRANCH_URL, {
            deeplinkPath: appDeepLink,
            channel: 'Mobile Web',
            campaign: 'Download on App',
            feature: item.type,
            [item.type]: item.id,
            key: keyPassword
        });

        return (
            <Modal
                className={classnames(styles.modal, 'app-download')}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay={false}
            >
                <div className="flex-breaker">
                    <div className={styles.top}>
                        <div className={styles.amAppIcon}>
                            <img
                                src="/static/images/am-app-icon-100x100.png"
                                srcSet="/static/images/am-app-icon-200x200.png 2x"
                                alt="Audiomack app"
                            />
                        </div>
                        <AmLogo className={styles.topLogo} />
                    </div>
                    <div className={styles.body}>
                        <h5
                            className={classnames(
                                styles.bodyTitle,
                                'u-spacing-bottom-10'
                            )}
                        >
                            Get the app
                        </h5>
                        <p>
                            Download any song or album for offline for FREE on
                            the Audiomack app.
                        </p>
                        <div className="button-group">
                            <a
                                className="pill pill--condensed"
                                href={branchUrl}
                            >
                                Let's go!
                            </a>
                            <button
                                onClick={this.props.onClose}
                                className="u-text-white u-d-block u-spacing-top-25 u-spacing-x-auto u-fw-700"
                            >
                                No thanks!
                            </button>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}
