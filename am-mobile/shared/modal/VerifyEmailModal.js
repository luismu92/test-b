import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Modal from './Modal';
import { MODAL_TYPE_VERIFY_EMAIL } from '../redux/modules/modal';

import VerifyEmailContainer from '../user/VerifyEmailContainer';

import styles from './Modal.module.scss';

class VerifyEmailModal extends Component {
    static propTypes = {
        modal: PropTypes.object.isRequired,
        onClose: PropTypes.func
    };

    render() {
        const { modal } = this.props;

        return (
            <Modal
                className={styles.light}
                isOpen={modal.type === MODAL_TYPE_VERIFY_EMAIL}
                onClose={this.props.onClose}
                showCloseButton={false}
                showOverlay
            >
                <VerifyEmailContainer hash={modal.extraData.hash} />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(VerifyEmailModal);
