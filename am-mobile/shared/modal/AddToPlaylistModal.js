import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Modal from './Modal';
import PlaylistAddContainer from '../playlist/PlaylistAddContainer';

import baseStyles from './Modal.module.scss';
import variantStyles from './AddToPlaylistModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

export default class AddToPlaylistModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool
    };

    static defaultProps = {
        active: false
    };

    render() {
        return (
            <Modal
                className={classnames(
                    styles.base.pageOverlay,
                    styles.variant.modal,
                    'add-to-playlist'
                )}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay={false}
            >
                <PlaylistAddContainer onClose={this.props.onClose} />
            </Modal>
        );
    }
}
