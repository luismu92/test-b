import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './CopyUrlModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

export default class CopyUrlModal extends Component {
    static propTypes = {
        active: PropTypes.bool,
        onClose: PropTypes.func,
        url: PropTypes.string
    };

    handleTextAreaFocus = () => {
        if (!this._textarea) {
            return;
        }

        this._textarea.focus();
        this._textarea.select();
    };

    render() {
        const { url } = this.props;

        return (
            <Modal
                classNames={classnames(
                    styles.base.takedown,
                    styles.base.light,
                    'copy-url'
                )}
                isOpen={this.props.active}
                onClose={this.props.onClose}
                showOverlay
            >
                <div>
                    <h1 className={styles.base.title}>Copy URL</h1>
                    <textarea
                        value={url}
                        readOnly
                        ref={(e) => (this._textarea = e)}
                        onClick={this.handleTextAreaFocus}
                    />
                </div>
            </Modal>
        );
    }
}
