import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Modal from './Modal';
import Checkbox from '../components/Checkbox';

import baseStyles from './Modal.module.scss';
import variantStyles from './TrendingModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

export default class TrendingModal extends Component {
    static propTypes = {
        active: PropTypes.bool,
        onClose: PropTypes.func,
        title: PropTypes.string,
        trendData: PropTypes.object
    };

    createCheckbox = (label, index) => {
        const key = `${index}-${label}`;

        return (
            <div className={styles.base.checkbox} key={key}>
                <Checkbox
                    isFancy
                    label={label}
                    onCheckboxChange={this.props.trendData.handleCheckboxToggle}
                    key={label}
                    isChecked={this.props.trendData.trendGenres[label]}
                />
            </div>
        );
    };

    createTrendingCheckboxes = (items) => {
        return Object.keys(items).map(this.createCheckbox);
    };

    render() {
        const { title, onClose, trendData } = this.props;

        return (
            <Modal
                className={classnames(
                    styles.base.dark,
                    styles.variant.modal,
                    'trending'
                )}
                isOpen={this.props.active}
                onClose={this.props.onClose}
            >
                <div>
                    <h1
                        className={classnames(
                            styles.base.title,
                            styles.variant.title,
                            'u-ls-n-01',
                            'u-spacing-bottom-20'
                        )}
                    >
                        {title}
                    </h1>
                    <div
                        className={classnames(
                            'button-group',
                            styles.base.buttonGroup
                        )}
                    >
                        <div className={styles.base.checkboxWrap}>
                            {this.createTrendingCheckboxes(
                                trendData.trendGenres || {}
                            )}
                        </div>
                        <div
                            className={classnames(
                                styles.base.buttonGroup,
                                'u-spacing-top-20',
                                'u-padding-x-30'
                            )}
                        >
                            <button
                                className={classnames(
                                    'button',
                                    styles.base.button,
                                    styles.variant.button
                                )}
                                onClick={onClose}
                            >
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}
