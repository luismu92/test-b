import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import { parse } from 'query-string';

import {
    searchInput,
    getSearch,
    setSearchingState
} from '../redux/modules/searchUser';

// import DotsLoader from '../loaders/DotsLoader';

class AccountSearchInputContainer extends Component {
    static propTypes = {
        searchUser: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func,
        inputPlaceholder: PropTypes.string,
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.props.dispatch(searchInput(parse(props.location.query).q || ''));
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();
        const input = e.currentTarget.querySelector('input');
        const query = input.value;

        input.blur();

        this.goToSearchPage(query);
    };

    handleSearchChange = (event) => {
        const { dispatch } = this.props;

        dispatch(searchInput(event.currentTarget.value));
    };

    handleClearSearchClick = () => {
        const { dispatch } = this.props;

        dispatch(searchInput(''));
        dispatch(setSearchingState(false));
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getItemValue = (item) => {
        return item;
    };

    goToSearchPage = (query) => {
        if (!query) {
            return;
        }

        const { dispatch, searchUser } = this.props;

        dispatch(searchInput(query));
        dispatch(getSearch(query));
        dispatch(setSearchingState(false));

        this.props.history.push(
            `/account/search?q=${encodeURIComponent(query)}&show=${
                searchUser.activeType
            }`
        );
    };

    renderMenu = (items, value, style) => {
        const { searchUser } = this.props;

        if (!searchUser.suggestions.length) {
            return <div />;
        }

        return (
            <div
                className="top-search__autocomplete"
                style={{ ...style }}
                children={items}
            />
        );
    };

    render() {
        const { searchUser, inputPlaceholder } = this.props;
        const removeClass = classnames(
            'glyphicons remove-circle top-search__removeIcon',
            {
                'top-search__removeIcon--visible': searchUser.query
            }
        );

        const icon = (
            /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
            <i className={removeClass} onClick={this.handleClearSearchClick} />
        );

        // if (searchUser.loading) {
        //     const containerStyles = {
        //         position: 'absolute',
        //         right: '5px',
        //         top: '50%',
        //         transform: 'translateY(-50%)'
        //     };

        //     icon = (
        //         <DotsLoader color="#c0c0c2" containerStyles={containerStyles} />
        //     );
        // }
        const inputValue = searchUser.query || '';

        return (
            <form
                className="top-search"
                action="/account/search"
                method="get"
                onSubmit={this.handleFormSubmit}
                autoComplete="off"
                ref={(f) => (this._form = f)}
            >
                <i className="glyphicons search top-search__searchIcon" />
                <input
                    className="top-search__input"
                    name="q"
                    placeholder={inputPlaceholder}
                    value={inputValue}
                    onChange={this.handleSearchChange}
                />
                {icon}
            </form>
        );
    }
}

function mapStateToProps(state) {
    return {
        searchUser: state.searchUser
    };
}

export default withRouter(
    connect(mapStateToProps)(AccountSearchInputContainer)
);
