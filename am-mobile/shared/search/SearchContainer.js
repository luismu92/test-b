import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import SearchPageMeta from 'components/SearchPageMeta';

import AmSearch from '../icons/search';
import ListContainer from '../list/ListContainer';
import UserListItem from '../list/UserListItem';
import UserList from '../list/UserList';

import { setActiveMarker, NAV_MARKER_SEARCH } from '../redux/modules/nav';
import {
    getSearch,
    setQueryType,
    setQueryContext,
    nextPage,
    setPage,
    clearList,
    QUERY_TYPE_MUSIC,
    QUERY_TYPE_SONGS,
    QUERY_TYPE_ALBUMS,
    QUERY_TYPE_ARTISTS,
    CONTEXT_TYPE_RELEVANCE,
    CONTEXT_TYPE_RECENT,
    CONTEXT_TYPE_POPULAR
} from '../redux/modules/search';

import ContextSwitcher from '../widgets/ContextSwitcher';
import SearchInputContainer from './SearchInputContainer';
import ListItemContainer from '../list/ListItemContainer';

class SearchContainer extends Component {
    static propTypes = {
        search: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        match: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        ad: PropTypes.object,
        loadingFollow: PropTypes.object,
        featuredSong: PropTypes.object,
        currentUser: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextSwitch = (type) => {
        const { dispatch } = this.props;

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setQueryType(type));
        dispatch(getSearch());
    };

    handleSubNavChange = (value) => {
        const { dispatch } = this.props;

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setQueryContext(value));
        dispatch(getSearch());
    };

    handleNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(getSearch());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getSubNavItems(query, activeType, activeContext) {
        const items = [
            {
                text: 'Most Popular',
                value: CONTEXT_TYPE_POPULAR,
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${activeType}&sort=${CONTEXT_TYPE_POPULAR}`,
                active: activeContext === CONTEXT_TYPE_POPULAR
            },
            {
                text: 'Most Relevant',
                value: CONTEXT_TYPE_RELEVANCE,
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${activeType}&sort=${CONTEXT_TYPE_RELEVANCE}`,
                active: activeContext === CONTEXT_TYPE_RELEVANCE
            },
            {
                text: 'Most Recent',
                value: CONTEXT_TYPE_RECENT,
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${activeType}&sort=${CONTEXT_TYPE_RECENT}`,
                active: activeContext === CONTEXT_TYPE_RECENT
            }
        ];

        return items;
    }

    getContextItems(query, activeType, activeContext) {
        return [
            {
                text: 'All Music',
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_MUSIC}&sort=${activeContext}`,
                value: QUERY_TYPE_MUSIC,
                active: activeType === QUERY_TYPE_MUSIC
            },
            {
                text: 'Songs',
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_SONGS}&sort=${activeContext}`,
                value: QUERY_TYPE_SONGS,
                active: activeType === QUERY_TYPE_SONGS
            },
            {
                text: 'Albums',
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_ALBUMS}&sort=${activeContext}`,
                value: QUERY_TYPE_ALBUMS,
                active: activeType === QUERY_TYPE_ALBUMS
            },
            {
                text: 'Accounts',
                href: `/search?q=${encodeURIComponent(
                    query
                )}&show=${QUERY_TYPE_ARTISTS}&sort=${activeContext}`,
                value: QUERY_TYPE_ARTISTS,
                active: activeType === QUERY_TYPE_ARTISTS
            }
            // {
            //     text: 'Playlists',
            //     href: `/${prefix}recent`,
            //     value: MusicActions.COLLECTION_TYPE_RECENTLY_ADDED,
            //     active: activeContext === MusicActions.COLLECTION_TYPE_RECENTLY_ADDED
            // }
        ];
    }

    renderEmptyState() {
        return (
            <div className="empty-state">
                <p>No results. Try another search.</p>
            </div>
        );
    }

    renderReplacementText() {
        const { search } = this.props;
        const { related, query } = search;

        if (!related) {
            return null;
        }

        return (
            <div className="row u-text-center u-spacing-y-50 u-padding-x-30">
                <p className="u-fs-20 u-lh-16 u-ls-n-09">
                    <strong>
                        We don't have music matching{' '}
                        <span className="u-text-orange">"{query}"</span>
                    </strong>{' '}
                    but we have selected some great music from similar artists
                    you might enjoy. Discover something new!
                </p>
            </div>
        );
    }

    renderList(props) {
        const {
            activeType,
            activeContext,
            query,
            loading,
            results,
            searchActive,
            onLastPage,
            verifiedArtist,
            verifiedTastemaker,
            verifiedPlaylist,
            tastemakerPlaylist
        } = props.search;

        const songObj = {
            data: results.filter((result) => !result.geo_restricted),
            onLastPage,
            loading
        };

        if (!results.length && !loading && !query) {
            return (
                <div className="empty-state">
                    <AmSearch />
                    <p>Search for songs & albums…</p>
                </div>
            );
        }

        // If the search dropdown is showing, hide the context switcher
        // and song list
        const style = {};

        if (searchActive) {
            style.display = 'none';
        }

        let list = (
            <ListContainer
                location={props.location}
                songs={songObj}
                showEmptyMessage={false}
                emptyState={this.renderEmptyState()}
                onNextPage={this.handleNextPage}
                featuredSong={this.props.featuredSong}
                showAd={true}
            />
        );

        if (activeType === 'artists') {
            list = (
                <UserList
                    items={results}
                    emptyState={this.renderEmptyState()}
                    onNextPage={this.handleNextPage}
                    isLastPage={onLastPage}
                />
            );
        }

        let artistResult;
        const featuredArtist = verifiedArtist || verifiedTastemaker;

        if (featuredArtist) {
            artistResult = (
                <Fragment>
                    <p className="list-header">Verified Account</p>
                    <UserListItem key="verifiedArtist" item={featuredArtist} />
                </Fragment>
            );
        }

        let playlistResult;
        const featuredPlaylist = verifiedPlaylist || tastemakerPlaylist;

        if (featuredPlaylist) {
            playlistResult = (
                <Fragment>
                    <p className="list-header">Verified Playlist</p>
                    <ListItemContainer
                        item={featuredPlaylist}
                        key="featuredPlaylist"
                        listType="playlist"
                        currentUser={this.props.currentUser}
                    />
                </Fragment>
            );
        }

        const hasFeatured =
            verifiedArtist ||
            verifiedTastemaker ||
            verifiedPlaylist ||
            tastemakerPlaylist;

        return (
            <div className="list-view" style={style}>
                <ContextSwitcher
                    items={this.getContextItems(
                        query,
                        activeType,
                        activeContext
                    )}
                    subnavItems={this.getSubNavItems(
                        query,
                        activeType,
                        activeContext
                    )}
                    onContextSwitch={this.handleContextSwitch}
                    onSubNavChange={this.handleSubNavChange}
                />
                {this.renderReplacementText()}
                {artistResult}
                {playlistResult}
                <Fragment>
                    {hasFeatured && <p className="list-header">Results</p>}
                    {list}
                </Fragment>
            </div>
        );
    }

    render() {
        const { query, activeType, activeContext, page } = this.props.search;

        return (
            <div className="search-page">
                <SearchPageMeta
                    query={query}
                    type={activeType}
                    page={page}
                    context={activeContext}
                />
                <SearchInputContainer
                    match={this.props.match}
                    history={this.props.history}
                    location={this.props.location}
                    inputPlaceholder="Search for artists, songs, albums!"
                />
                {this.renderList(this.props)}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        search: state.search,
        player: state.player,
        ad: state.ad,
        loadingFollow: state.artist.loadingFollow,
        featuredSong: state.featured.song,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(SearchContainer, [
        () => setActiveMarker(NAV_MARKER_SEARCH),
        (params, query) => setQueryType(query.show),
        (params, query) => setQueryContext(query.sort),
        (params, query) => getSearch(query.q)
    ])
);
