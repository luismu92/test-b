import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Autocomplete from 'react-autocomplete';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import { parse } from 'query-string';

import connectDataFetchers from 'lib/connectDataFetchers';

import {
    searchInput,
    getSuggestions,
    getSearch,
    setSearchingState,
    setQueryType,
    setQueryContext
} from '../redux/modules/search';

import DotsLoader from '../loaders/DotsLoader';

class SearchInputContainer extends Component {
    static propTypes = {
        search: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func,
        inputPlaceholder: PropTypes.string,
        account: PropTypes.bool
    };

    static defaultProps = {
        account: false
    };

    constructor(props) {
        super(props);

        this._queryFromPageLoad = parse(props.location.search).q;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleInputFocus = () => {
        const { dispatch, search } = this.props;

        dispatch(setSearchingState(!!search.suggestions.length));
    };

    handleInputBlur = () => {
        const { dispatch, search } = this.props;

        dispatch(
            setSearchingState(
                !(search.suggestions.length || search.results.length)
            )
        );
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const input = e.currentTarget.querySelector('input');
        const query = input.value;
        const { dispatch, history } = this.props;

        if (query === '') {
            alert('You must enter a search term');
            return;
        }

        input.blur();

        dispatch(setQueryType());
        dispatch(setQueryContext());
        dispatch(getSearch(query));
        dispatch(setSearchingState(false));

        history.push(`/search?q=${encodeURIComponent(query)}`);
    };

    handleSearchChange = (event, query) => {
        const { dispatch, account } = this.props;

        this._queryFromPageLoad = '';

        dispatch(searchInput(query));

        if (account) {
            console.log('No autocomplete for account search');
            return;
        }

        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(() => {
            const SPHINX_SEARCH_MIN_LENGTH = 2;

            if (!query || query.length < SPHINX_SEARCH_MIN_LENGTH) {
                return;
            }

            dispatch(getSuggestions(query))
                .then((action) => {
                    const suggestions = action.resolved.results;

                    if (!suggestions) {
                        return null;
                    }

                    return dispatch(setSearchingState(!!suggestions.length));
                })
                .catch((err) => console.log(err));
        }, 200);
    };

    handleSearchSelect = (query) => {
        this.goToSearchPage(query);
    };

    handleClearSearchClick = () => {
        const { dispatch } = this.props;

        dispatch(searchInput(''));
        dispatch(setSearchingState(false));
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getItemValue = (item) => {
        return item;
    };

    goToSearchPage = (query) => {
        if (!query) {
            return;
        }

        const { dispatch, history } = this.props;

        dispatch(searchInput(query));
        dispatch(getSearch(query));
        dispatch(setSearchingState(false));

        history.push(`/search?q=${encodeURIComponent(query)}`);
    };

    renderSearchItem = (item, isHighlighted) => {
        const index = this.props.search.suggestions.indexOf(item);
        const klass = classnames('top-search-result', {
            'top-search-result--highlighted': isHighlighted
        });

        return (
            <div className={klass} key={`s${index}`}>
                {item}
            </div>
        );
    };

    renderMenu = (items, value, style) => {
        const { search } = this.props;

        if (!search.suggestions.length) {
            return <div />;
        }

        return (
            <div
                className="top-search__autocomplete"
                style={{ ...style }}
                children={items}
            />
        );
    };

    render() {
        const inputProps = {
            className: 'top-search__input',
            name: 'q',
            placeholder: this.props.inputPlaceholder,
            minLength: 2
        };
        const wrapperProps = {
            style: {
                display: 'block'
            }
        };

        const removeClass = classnames(
            'glyphicons remove-circle top-search__removeIcon',
            {
                'top-search__removeIcon--visible': this.props.search.query
            }
        );

        let icon = (
            /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
            <i className={removeClass} onClick={this.handleClearSearchClick} />
        );

        if (this.props.search.loading) {
            const containerStyles = {
                position: 'absolute',
                right: '5px',
                top: '50%',
                transform: 'translateY(-50%)'
            };

            icon = (
                <DotsLoader color="#c0c0c2" containerStyles={containerStyles} />
            );
        }

        return (
            <form
                className="top-search"
                action="/search"
                method="get"
                onSubmit={this.handleFormSubmit}
                autoComplete="off"
            >
                <i className="glyphicons search top-search__searchIcon" />
                <Autocomplete
                    autoHighlight={false}
                    wrapperProps={wrapperProps}
                    inputProps={inputProps}
                    value={this.props.search.query || this._queryFromPageLoad}
                    items={this.props.search.suggestions}
                    getItemValue={this.getItemValue}
                    onSelect={this.handleSearchSelect}
                    onChange={this.handleSearchChange}
                    renderMenu={this.renderMenu}
                    renderItem={this.renderSearchItem}
                />
                {icon}
            </form>
        );
    }
}

function mapStateToProps(state) {
    return {
        search: state.search
    };
}

export default withRouter(
    connect(mapStateToProps)(connectDataFetchers(SearchInputContainer))
);
