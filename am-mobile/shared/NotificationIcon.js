import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { renderCappedDisplayCount } from 'utils/index';

import BellIcon from './icons/bell';

export default class NotificationIcon extends Component {
    static propTypes = {
        artist: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        className: PropTypes.string,
        unseen: PropTypes.number
    };

    render() {
        const { unseen, className } = this.props;

        const buttonClass = classnames('notification-bell', {
            [className]: className
        });

        return (
            <Link
                className={buttonClass}
                to="/notifications"
                data-jewel={renderCappedDisplayCount(unseen)}
                aria-label="Your notifications"
            >
                <BellIcon />
            </Link>
        );
    }
}
