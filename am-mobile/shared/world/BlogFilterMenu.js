import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { getBlogPageUrl } from 'utils/blog';

import CloseIcon from 'icons/close-alt';
import CheckMark from 'icons/check-mark';

import styles from './BlogFilterMenu.module.scss';

export default function BlogFilterMenu({
    worldPage,
    overlayActive,
    onCloseOverlayClick,
    activeTag
}) {
    if (!overlayActive) {
        return null;
    }

    const allPostsLink = (
        /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-noninteractive-element-interactions */
        <li
            className={styles.menuItem}
            key="menuItem-main"
            data-active={!activeTag}
            onClick={onCloseOverlayClick}
        >
            <Link to="/world">
                <span className={styles.optionCheck}>
                    <CheckMark />
                </span>
                All Posts
            </Link>
        </li>
    );

    const listItems = worldPage.list.map((item, i) => {
        const klass = classnames(styles.menuItem, {
            active: activeTag === item.slug
        });

        return (
            /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-noninteractive-element-interactions */
            <li
                className={klass}
                key={`menuItem-${i}`}
                data-active={activeTag === item.slug}
                onClick={onCloseOverlayClick}
            >
                <Link to={getBlogPageUrl(item)}>
                    <span className={styles.optionCheck}>
                        <CheckMark />
                    </span>
                    {item.title}
                </Link>
            </li>
        );
    });

    listItems.unshift(allPostsLink);

    return (
        <div className={styles.menu}>
            <header className={styles.header}>
                <h3 className={styles.heading}>Filter</h3>
                <button className={styles.close} onClick={onCloseOverlayClick}>
                    <CloseIcon />
                </button>
            </header>
            <ul className={styles.options}>{listItems}</ul>
        </div>
    );
}

BlogFilterMenu.propTypes = {
    worldPage: PropTypes.object,
    overlayActive: PropTypes.bool,
    onCloseOverlayClick: PropTypes.func,
    activeTag: PropTypes.string
};
