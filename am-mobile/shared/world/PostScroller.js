import React from 'react';
import PropTypes from 'prop-types';
import FeedPost from './FeedPost';

function PostScroller({ worldFeatured }) {
    return worldFeatured.list.map((post, i) => {
        return <FeedPost key={`post-${i}`} post={post} />;
    });
}

PostScroller.propTypes = {
    worldFeatured: PropTypes.object
};

export default PostScroller;
