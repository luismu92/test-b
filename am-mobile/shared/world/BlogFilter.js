import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { blogSlugToTitle } from 'constants/index';

import BlogFilterMenu from './BlogFilterMenu';
import FilterIcon from 'icons/filter';

import styles from './BlogFilter.module.scss';

export default function BlogFilter({ worldPage, match, tag }) {
    const [overlayActive, toggleOverlay] = useState(false);

    const label = blogSlugToTitle[match.params.page] || 'All Posts';

    function handleOverlayToggle() {
        toggleOverlay(!overlayActive);
    }

    let tagName;
    if (tag) {
        tagName = <p className={styles.tag}>#{tag.name}</p>;
    }

    return (
        <div className={styles.container}>
            <div className={styles.buttonWrap}>
                <button
                    className={styles.button}
                    onClick={handleOverlayToggle} //eslint-disable-line
                >
                    <span className={styles.icon}>
                        <FilterIcon />
                    </span>
                    <span className={styles.label}>{label}</span>
                </button>
                {tagName}
            </div>
            <BlogFilterMenu
                worldPage={worldPage}
                overlayActive={overlayActive}
                onCloseOverlayClick={handleOverlayToggle}
                activeTag={match.params.page}
            />
        </div>
    );
}

BlogFilter.propTypes = {
    worldPage: PropTypes.object,
    match: PropTypes.object,
    tag: PropTypes.object
};
