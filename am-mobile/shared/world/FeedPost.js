/* eslint-disable react/no-multi-comp */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getDynamicImageProps } from 'utils/index';
import { getFeaturedImage, getBlogPostUrl } from 'utils/blog';
import { truncate } from 'utils/string';

import styles from './FeedPost.module.scss';

export default function FeedPost({ post }) {
    if (!post) {
        return null;
    }

    const postLink = getBlogPostUrl(post);

    const getFeatureImage = (image) => {
        return getDynamicImageProps(image, {
            width: 355,
            height: 180
        });
    };

    const [featureImage, featureImageSrcSet] = getFeatureImage(
        getFeaturedImage(post)
    );

    let excerpt = post.custom_excerpt || post.excerpt;
    let excerptTitle = null;
    const maxExcerptLength = 100;

    if (maxExcerptLength) {
        const newExcerpt = truncate(excerpt, maxExcerptLength);

        if (newExcerpt !== excerpt) {
            excerptTitle = excerpt;
        }

        excerpt = newExcerpt;
    }

    return (
        <div className={styles.post}>
            <div className={styles.image}>
                <Link to={postLink}>
                    <img
                        src={featureImage}
                        srcSet={featureImageSrcSet}
                        alt={post.title}
                        loading="lazy"
                        style={{
                            width: '100%'
                        }}
                    />
                </Link>
            </div>
            <div className={styles.content}>
                <h3 className={styles.title}>
                    <Link to={postLink}>{post.title}</Link>
                </h3>
                <div className={styles.excerpt}>
                    <p aria-label={excerptTitle}>{excerpt}</p>
                </div>
            </div>
        </div>
    );
}

FeedPost.propTypes = {
    post: PropTypes.object
};
