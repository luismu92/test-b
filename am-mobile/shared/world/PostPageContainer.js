import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';

import connectDataFetchers from 'lib/connectDataFetchers';
import hideHeaderForComponent from 'hoc/hideHeaderForComponent';

import { getBySlug } from '../redux/modules/world/post';
import { addItems, showDrawer, hideDrawer } from '../redux/modules/drawer';

import { copyToClipboard } from 'utils/index';
import { getBlogPostUrl } from 'utils/blog';

import { showMessage } from '../redux/modules/message';

import { showModal, MODAL_TYPE_COPY_URL } from '../redux/modules/modal';

import PostPage from './PostPage';
import WorldHeader from './WorldHeader';

import TwitterIcon from '../icons/twitter-logo';
import FacebookIcon from '../icons/facebook-letter-logo';
import LinkIcon from '../icons/link';

class PostPageContainer extends Component {
    static propTypes = {
        worldPost: PropTypes.object,
        worldPostArtists: PropTypes.object,
        history: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            aboutModalActive: false
        };
    }

    componentDidMount() {
        document.body.classList.add('context-switcher-hidden');
    }

    componentWillUnmount() {
        document.body.classList.remove('context-switcher-hidden');
    }

    handleAddedToQueue = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const title = button.getAttribute('data-title');

        dispatch(showMessage(`${title} was added to your queue`));
    };

    handleShareClick = () => {
        const { worldPost, dispatch } = this.props;
        if (typeof window.navigator.share !== 'undefined') {
            window.navigator.share({
                title: worldPost.info.title,
                url: getBlogPostUrl(worldPost.info, {
                    host: process.env.AM_URL
                })
            });
        } else {
            dispatch(addItems(this.getShareItems()));
            dispatch(showDrawer());
        }
    };

    handleCopyUrlClick = (item) => {
        const { dispatch } = this.props;
        const url = getBlogPostUrl(item.info, {
            host: process.env.AM_URL
        });
        const successful = copyToClipboard(url);

        if (successful) {
            dispatch(showMessage('URL was copied to clipboard!'));
        } else {
            dispatch(
                showModal(MODAL_TYPE_COPY_URL, {
                    url: url
                })
            );
        }

        dispatch(hideDrawer());
    };

    getShareItems() {
        const { worldPost } = this.props;
        // https://facebook.com/sharer/sharer.php?u=http://dev.audiomack.com/song/glen-scott/vimeo-tester
        // https://twitter.com/share?url=http://dev.audiomack.com/song/glen-scott/vimeo-tester&text=Glen%20Scott%20-%20Vimeo%20Tester%20via%20@glenscott
        return [
            {
                text: [
                    <FacebookIcon
                        className="drawer__list-item-icon u-brand-color"
                        key="f"
                    />,
                    'Share on Facebook'
                ],
                action: 'share-facebook',
                href: `https://www.facebook.com/dialog/feed?app_id=${
                    process.env.FACEBOOK_APP_ID
                }&link=${getBlogPostUrl(worldPost.info, {
                    host: process.env.AM_URL
                })}${
                    worldPost.info.feature_image
                }&name=Now%20Reading&caption=%20&description=${
                    worldPost.info.title
                }`
            },
            {
                text: [
                    <TwitterIcon
                        className="drawer__list-item-icon u-brand-color"
                        key="t"
                    />,
                    'Share on Twitter'
                ],
                action: 'share-twitter',
                href: `https://twitter.com/share?url=${getBlogPostUrl(
                    worldPost.info,
                    {
                        host: process.env.AM_URL
                    }
                )}&text=${worldPost.info.title}`
            },
            {
                text: [
                    <LinkIcon
                        className="drawer__list-item-icon u-brand-color"
                        key="c"
                    />,
                    'Copy URL'
                ],
                action: 'copy-url',
                handler: () => this.handleCopyUrlClick(worldPost)
            }
        ];
    }

    render() {
        return (
            <div style={{ paddingTop: 48 }}>
                <WorldHeader onShareClick={this.handleShareClick} />
                <PostPage
                    worldPost={this.props.worldPost}
                    worldPostArtists={this.props.worldPostArtists}
                    history={this.props.history}
                    onAddedToQueue={this.handleAddedToQueue}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        worldPost: state.worldPost,
        worldPostArtists: state.worldPostArtists
    };
}

export default compose(
    withRouter,
    hideHeaderForComponent,
    connect(mapStateToProps)
)(
    connectDataFetchers(PostPageContainer, [
        (params) =>
            getBySlug(params.slug, {
                include: 'authors,tags',
                formats: ['html', 'mobiledoc']
            })
    ])
);
