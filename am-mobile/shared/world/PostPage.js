import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import md5 from 'blueimp-md5';

import MobiledocRenderer from 'utils/MobiledocRenderer';
import { buildDynamicImage } from 'utils/index';
import { getBlogPostUrl, getFeaturedImage } from 'utils/blog';
import BlogPostMeta from 'components/BlogPostMeta';

import Tag from 'components/blog/Tag';

import NotFound from '../NotFound';
import SocialShareButton from 'buttons/SocialShareButton';
import PostAuthor from 'world/PostAuthor';

import EmbedReplacementContainer from 'world/EmbedReplacementContainer';
import styles from './PostPage.module.scss';

export default class PostPage extends Component {
    static propTypes = {
        location: PropTypes.object,
        worldPost: PropTypes.object,
        worldPostArtists: PropTypes.object,
        history: PropTypes.object,
        onAddedToQueue: PropTypes.func
    };

    shouldComponentUpdate(nextProps) {
        const currentInfo = this.props.worldPost.info;
        const nextInfo = nextProps.worldPost.info;

        if (!currentInfo || !nextInfo) {
            return true;
        }

        const currentMobileDoc = currentInfo.mobiledoc;
        const nextMobileDoc = nextInfo.mobiledoc;

        return md5(currentMobileDoc) !== md5(nextMobileDoc);
    }

    renderImageCards() {
        const feature = getFeaturedImage(this.props.worldPost.info);

        return {
            name: 'image',
            render({ /* env, options, */ payload }) {
                const imageUrl = payload.src;
                // Sometimes the feature image is suffixed with an incremented number
                // ie:
                // http://localhost:2368/content/images/downloaded_images/Meet-Dylan-The-Gypsy--the-Prolific-NYC-Tastemaker-with-Proud-DC-Roots/1-yRCZBIPMTjgL1aWFELT7-w-1.jpeg
                //
                // vs
                //
                // http://localhost:2368/content/images/downloaded_images/Meet-Dylan-The-Gypsy--the-Prolific-NYC-Tastemaker-with-Proud-DC-Roots/1-yRCZBIPMTjgL1aWFELT7-w.jpegs
                const regex = /-\d+.(jpeg|jpg|gif|png|webp)$/i;
                const replacer = (match, ext) => {
                    return `.${ext}`;
                };
                const normalizedImage = imageUrl.replace(regex, replacer);
                const normalizedFeature = feature.replace(regex, replacer);
                const isSameAsFeatureImage =
                    normalizedImage === normalizedFeature;

                if (isSameAsFeatureImage) {
                    return null;
                }

                return <img src={imageUrl} key={payload.key} alt="" />;
            }
        };
    }

    renderHTMLCards() {
        const { onAddedToQueue } = this.props;

        return {
            name: 'html',
            render({ /* env, options, */ payload }) {
                const audiomackEmbedMatch = payload.html.match(
                    /<iframe.*src="https:\/\/(?:www.)?audiomack.com\/embed\/(.+?)\/(.+?)\/(.+?)"/
                );

                if (audiomackEmbedMatch) {
                    const [, type, artist, slug] = audiomackEmbedMatch;

                    return (
                        <EmbedReplacementContainer
                            key={`${type}:${artist}:${slug}`}
                            type={type}
                            artist={artist}
                            slug={slug}
                            onAddedToQueue={onAddedToQueue}
                        />
                    );
                }

                const youtubeEmbedMatch = payload.html.match(
                    /<iframe.*src="https:\/\/(?:www.)?youtube.com\/embed\/([\w\-]+)(\S+)?"/
                );

                if (youtubeEmbedMatch) {
                    return (
                        <div key={payload.key}>
                            <div
                                className={styles.videoContainer}
                                dangerouslySetInnerHTML={{
                                    __html: payload.html
                                }}
                            />
                        </div>
                    );
                }

                // @todo
                // We probably want to not just blindly inject html into the
                // page so lets try to only render html if they pass some check
                // ie audiomack.com embeds
                return (
                    <div key={payload.key}>
                        <div
                            dangerouslySetInnerHTML={{ __html: payload.html }}
                        />
                        {payload.caption ? <p>{payload.caption}</p> : null}
                    </div>
                );
            }
        };
    }

    renderEmbedCards() {
        const { onAddedToQueue } = this.props;

        return {
            name: 'embed',
            render({ /* env, options, */ payload }) {
                const audiomackEmbedMatch = payload.html.match(
                    /<iframe.*src="https:\/\/(?:www.)?audiomack.com\/embed\/(.+?)\/(.+?)\/(.+?)"/
                );

                if (audiomackEmbedMatch) {
                    const [, type, artist, slug] = audiomackEmbedMatch;

                    return (
                        <EmbedReplacementContainer
                            key={`${type}:${artist}:${slug}`}
                            type={type}
                            artist={artist}
                            slug={slug}
                            onAddedToQueue={onAddedToQueue}
                        />
                    );
                }

                const youtubeEmbedMatch = payload.html.match(
                    /<iframe.*src="https:\/\/(?:www.)?youtube.com\/embed\/([\w\-]+)(\S+)?"/
                );

                if (youtubeEmbedMatch) {
                    return (
                        <div key={payload.key}>
                            <div
                                className={styles.videoContainer}
                                dangerouslySetInnerHTML={{
                                    __html: payload.html
                                }}
                            />
                            {payload.caption ? <p>{payload.caption}</p> : null}
                        </div>
                    );
                }

                // @todo
                // We probably want to not just blindly inject html into the
                // page so lets try to only render html if they pass some check
                // ie audiomack.com embeds
                return (
                    <div key={payload.key}>
                        <div
                            dangerouslySetInnerHTML={{ __html: payload.html }}
                        />
                        {payload.caption ? <p>{payload.caption}</p> : null}
                    </div>
                );
            }
        };
    }

    renderLinks() {
        return {
            name: 'a',
            render(props) {
                const href = props.href || '';
                // const current
                // const pageUrl = process.env.AM_URL;
                const pageRegex = /^https?:\/\/(?:www.)?audiomack.com/;
                if (href.match(pageRegex)) {
                    const path = href.replace(pageRegex, '');
                    const popover =
                        path.indexOf('/artist/') === 0 ? true : null;

                    return <Link {...props} to={path} data-popover={popover} />;
                }

                return (
                    // eslint-disable-next-line jsx-a11y/anchor-has-content
                    <a
                        {...props}
                        href={href}
                        target="_blank"
                        rel="nofollow noopener"
                    />
                );
            }
        };
    }

    renderDoc(mobiledocString) {
        const options = {
            atoms: [],
            cards: [
                this.renderImageCards(),
                this.renderHTMLCards(),
                this.renderEmbedCards()
            ],
            markups: [this.renderLinks()]
        };

        const doc = new MobiledocRenderer(JSON.parse(mobiledocString), options);

        return doc.render();
    }

    renderShareButtons() {
        const { worldPost } = this.props;
        const url = getBlogPostUrl(worldPost.info, {
            host: process.env.AM_URL
        });

        const buttons = [
            {
                network: SocialShareButton.NETWORK_TWITTER,
                url: url,
                text: worldPost.info.title
            },
            {
                network: SocialShareButton.NETWORK_FACEBOOK,
                url: url,
                text: worldPost.info.title,
                image: getFeaturedImage(worldPost.info)
            }
        ].map((network, i) => {
            return (
                <SocialShareButton
                    key={i}
                    network={network.network}
                    url={network.url}
                    shareText={network.text}
                    shareImage={network.image}
                    hideLabel
                />
            );
        });

        return <div className={styles.shareButtonFooter}>{buttons}</div>;
    }

    render() {
        const { worldPost } = this.props;

        if (!worldPost.info) {
            if (worldPost.error) {
                return (
                    <NotFound
                        errors={worldPost.error.errors}
                        type="world-post"
                    />
                );
            }

            return null;
        }

        const tags = worldPost.info.tags.filter((obj) => {
            return obj.visibility !== 'internal';
        });
        const tagEls = tags.map((obj, i) => {
            return <Tag tag={obj} key={i} size="large" />;
        });

        const authorBlock = (
            <PostAuthor
                post={worldPost.info}
                worldPostArtists={this.props.worldPostArtists}
                variant="mobile"
                style={{
                    marginBottom: 25
                }}
            />
        );

        const featuredImageWidth = 375;
        const featuredImageHeight = 200;

        const featuredImage = buildDynamicImage(
            getFeaturedImage(worldPost.info),
            {
                width: featuredImageWidth,
                height: featuredImageHeight,
                max: true
            }
        );

        const featuredImageRetina = buildDynamicImage(
            getFeaturedImage(worldPost.info),
            {
                width: featuredImageWidth * 2,
                height: featuredImageHeight * 2,
                max: true
            }
        );

        return (
            <Fragment>
                <BlogPostMeta post={worldPost.info} />
                <img
                    className={styles.image}
                    src={featuredImage}
                    srcSet={featuredImageRetina}
                    alt=""
                />
                <div className={styles.contentWrap}>
                    <header className={styles.header}>
                        <Tag
                            tag={tags[0]}
                            size="small"
                            style={{
                                marginBottom: 10
                            }}
                        />
                        <h1 className={styles.title}>{worldPost.info.title}</h1>
                    </header>
                    <div className={styles.content}>
                        {this.renderDoc(worldPost.info.mobiledoc)}
                    </div>
                    <div className={styles.meta}>
                        {authorBlock}
                        {tagEls}
                    </div>
                    {this.renderShareButtons()}
                </div>
            </Fragment>
        );
    }
}
