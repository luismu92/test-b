import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import BlogFilter from './BlogFilter';
import PostScrollerContainer from './PostScrollerContainer';
import BlogPostLandingMeta from 'components/BlogPostLandingMeta';

import styles from './MainPage.module.scss';

export default class MainPage extends Component {
    static propTypes = {
        worldPage: PropTypes.object,
        worldSettings: PropTypes.object,
        worldFeatured: PropTypes.object,
        match: PropTypes.object
    };

    render() {
        const { match } = this.props;
        let foundTag;

        // For tag page
        if (match.params.slug) {
            this.props.worldFeatured.list.some((post) => {
                return post.tags.some((tag) => {
                    const matched = tag.slug === match.params.slug;

                    if (matched) {
                        foundTag = tag;
                    }

                    return matched;
                });
            });
        }

        return (
            <Fragment>
                <BlogPostLandingMeta
                    settings={this.props.worldSettings.info}
                    match={this.props.match}
                />
                <div className={styles.wrap}>
                    <div>
                        <BlogFilter
                            worldPage={this.props.worldPage}
                            match={this.props.match}
                            tag={foundTag}
                        />
                        <PostScrollerContainer match={this.props.match} />
                    </div>
                </div>
            </Fragment>
        );
    }
}
