import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import WorldLogoColor from 'components/WorldLogoColor';

import SendIcon from 'icons/send-icon';
import ChevronLeft from 'icons/chevron-left';

import styles from './WorldHeader.module.scss';

export default function WorldHeader({ onShareClick }) {
    return (
        <header className={styles.header}>
            <Link className={styles.back} to="/world">
                <ChevronLeft />
            </Link>
            <Link className={styles.logo} to="/world">
                <WorldLogoColor className="u-no-fill" />
            </Link>
            <button className={styles.share} onClick={onShareClick}>
                <SendIcon />
            </button>
        </header>
    );
}

WorldHeader.propTypes = {
    onShareClick: PropTypes.func
};
