import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { passiveOption, debounce } from 'utils/index';

import { getFeaturedPosts, setFilter } from '../redux/modules/world/featured';
import AndroidLoader from '../loaders/AndroidLoader';
import PostScroller from './PostScroller';

const SCROLL_THRESHOLD = 500;

class PostScrollerContainer extends Component {
    static propTypes = {
        worldFeatured: PropTypes.object,
        worldPage: PropTypes.object,
        match: PropTypes.object,
        dispatch: PropTypes.func
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const option = passiveOption();

        window.addEventListener('scroll', this.handleWindowScroll, option);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, worldPage } = this.props;
        const currentPage = this.props.match.params.page;

        if (prevProps.match.params.page !== currentPage) {
            const foundPage = worldPage.list.find((page) => {
                return page.slug === currentPage;
            });

            let slug;

            if (foundPage) {
                slug = foundPage.tags[0].slug;
            }

            dispatch(setFilter('tag', slug));

            dispatch(
                getFeaturedPosts({
                    page: 1
                })
            );
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleWindowScroll, false);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowScroll = debounce(() => {
        const { dispatch, worldFeatured } = this.props;
        const { loading, pagination } = worldFeatured;
        const { page, pages, next } = pagination;
        const onLastPage = page >= pages;

        if (loading || onLastPage) {
            return;
        }

        if (
            document.body.scrollHeight -
                (window.innerHeight + window.pageYOffset) <
            SCROLL_THRESHOLD
        ) {
            dispatch(
                getFeaturedPosts({
                    page: next
                })
            );
        }
    }, 200);

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { loading } = this.props.worldFeatured;
        let loader;

        if (loading) {
            loader = (
                <div className="column small-24">
                    <div className="u-padding-40 u-text-center">
                        <AndroidLoader />
                    </div>
                </div>
            );
        }
        return (
            <div className="row" style={{ padding: '0 10px' }}>
                <PostScroller worldFeatured={this.props.worldFeatured} />
                {loader}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        worldFeatured: state.worldFeatured,
        worldPage: state.worldPage
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(PostScrollerContainer);
