import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

export default function requireAuth(Component, redirectTo = '/login') {
    class RequireAuthWrapper extends React.Component {
        static displayName = `RequireAuthWrapper[${Component.displayName}]`;

        static propTypes = {
            dispatch: PropTypes.func.isRequired,
            currentUser: PropTypes.object.isRequired,
            history: PropTypes.object.isRequired,
            location: PropTypes.object.isRequired
        };

        componentDidMount() {
            this.checkAuth();
        }

        componentDidUpdate(prevProps) {
            if (prevProps.location !== this.props.location) {
                this.checkAuth(this.props);
            }
        }

        checkAuth(props = this.props) {
            const { currentUser, history, location } = props;

            if (!currentUser.isLoggedIn) {
                history.replace({
                    pathname: redirectTo,
                    state: {
                        nextPathname: location.pathname
                    }
                });
            }
        }

        render() {
            if (!this.props.currentUser.isLoggedIn) {
                return null;
            }

            return <Component {...this.props} />;
        }
    }

    function mapStateToProps(state) {
        return {
            currentUser: state.currentUser
        };
    }

    return withRouter(connect(mapStateToProps)(RequireAuthWrapper));
}
