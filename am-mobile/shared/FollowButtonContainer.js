import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { follow, unfollow, queueAction } from './redux/modules/user/index';
import { showMessage } from './redux/modules/message';

import FollowButton from './FollowButton';

class FollowButtonContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        history: PropTypes.object,
        currentUser: PropTypes.object,
        className: PropTypes.string,
        artist: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            loading: false
        };
    }

    componentDidUpdate(prevProps) {
        // Follow or unfollow happened
        if (
            prevProps.currentUser.profile &&
            prevProps.currentUser.profile.following &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.following &&
            prevProps.currentUser.profile.following.length !==
                this.props.currentUser.profile.following.length
        ) {
            // eslint-disable-next-line
            this.setState({
                loading: false
            });
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFollowClick = (shouldFollow, artist) => {
        const { dispatch, currentUser } = this.props;

        const queuedAction = () => {
            this.setState({
                loading: true
            });

            if (shouldFollow) {
                dispatch(follow(artist)).catch((error) => {
                    this.setState({
                        loading: false
                    });
                    dispatch(showMessage(error.message));
                    return;
                });
                return;
            }
            dispatch(unfollow(artist));
        };

        if (!currentUser.isLoggedIn) {
            dispatch(queueAction(queuedAction));
            this.openLogin();
            return;
        }

        queuedAction();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    openLogin() {
        const { history } = this.props;

        history.push('/login');
    }

    render() {
        const { artist, className, currentUser } = this.props;

        return (
            <FollowButton
                className={className}
                loading={this.state.loading}
                currentUser={currentUser}
                onClick={this.handleFollowClick}
                artist={artist}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(FollowButtonContainer));
