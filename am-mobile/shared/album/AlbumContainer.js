import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { parse } from 'query-string';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';
import MusicPageMeta from 'components/MusicPageMeta';
import Truncate from 'components/Truncate';

import { nextPage, fetchSongList } from '../redux/modules/music';

import {
    isCurrentMusicItem,
    getUploader,
    getQueueIndexForSong,
    getNormalizedQueueTrack,
    buildDynamicImage
} from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';

import { KIND_ALBUM } from 'constants/comment';

import { getAlbumInfo } from '../redux/modules/music';
import {
    setGlobalBackground,
    setBannerDeepLink
} from '../redux/modules/global';
import {
    MODAL_TYPE_ALBUM_INFO,
    showModal,
    hideModal
} from '../redux/modules/modal';
import { play, pause, editQueue } from '../redux/modules/player';
import { getArtistUploads } from '../redux/modules/artist/uploads';
import { hideDrawer } from '../redux/modules/drawer';
import { reset } from '../redux/modules/promoKey';

import PlayIcon from '../icons/play';
import PauseIcon from '../icons/pause';
import Avatar from '../Avatar';
import ListContainer from '../list/ListContainer';
import AlbumList from '../list/AlbumList';
import AdminActionsContainer from '../AdminActionsContainer';
import ActionSheetContainer from '../ActionSheetContainer';
import NotFound from '../NotFound';
import AlbumInfoModal from '../modal/AlbumInfoModal';
import ListenOnAppButton from '../components/ListenOnAppButton';
import CommentsWrapper from '../components/CommentsWrapper';
import GeoNotice from '../components/GeoNotice';

import InfoIcon from 'icons/info';
import Verified from 'components/Verified';

const LOCK_POSITION = 130;

class AlbumContainer extends Component {
    static propTypes = {
        modal: PropTypes.object,
        currentUser: PropTypes.object,
        artistUploads: PropTypes.object,
        match: PropTypes.object,
        history: PropTypes.object,
        album: PropTypes.object,
        location: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        errors: PropTypes.array,
        songs: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            scroll: 0
        };

        const query = parse(props.location.search);

        this._query = {
            key: query.key
        };
    }

    componentDidMount() {
        this.setSiteWrap();

        window.addEventListener('scroll', this.handleWindowScroll, false);

        this.setDeepLink(this.props.album);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, album, match, location, currentUser } = this.props;
        const { params: prevParams } = prevProps.match;
        const { params: currentParams } = match;
        const changedArtistSlug =
            prevParams.artistId !== currentParams.artistId &&
            currentParams.artistId;
        const changedAlbumSlug =
            prevParams.albumSlug !== currentParams.albumSlug &&
            currentParams.albumSlug;
        const changedUserToken =
            currentUser.token &&
            prevProps.currentUser.token !== currentUser.token;
        const prevAlbum = prevProps.album || {};
        const currentAlbum = album || {};

        if (prevAlbum.id !== currentAlbum.id && album) {
            this.setSiteWrap();
            this.setDeepLink(album);
        }

        if (changedArtistSlug || changedAlbumSlug || changedUserToken) {
            const options = {
                key: parse(location.search).key,
                token: currentUser.token,
                secret: currentUser.secret
            };

            dispatch(
                getAlbumInfo(
                    currentParams.artistId,
                    currentParams.albumSlug,
                    options
                )
            );
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(setGlobalBackground(null));

        window.removeEventListener('scroll', this.handleWindowScroll, false);

        dispatch(hideModal());
        dispatch(hideDrawer());
        dispatch(reset());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleActionClick = (action, e) => {
        switch (action) {
            case 'info':
                this.handleInfoOpen();
                break;

            case 'play':
                this.handlePlayClick(e);
                break;

            default:
                break;
        }
    };

    handleWindowScroll = () => {
        const scroll = Math.min(window.pageYOffset, LOCK_POSITION);

        if (this.state.scroll !== LOCK_POSITION || scroll < LOCK_POSITION) {
            this.setState({
                scroll
            });
        }
    };

    handleInfoOpen = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_ALBUM_INFO));
    };

    handleInfoClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handlePlayClick = (e) => {
        const { album, dispatch, player, artistUploads } = this.props;

        // if trackIndex gets set then we've clicked
        // a track list item. may want to separate
        // the track list item event handler out later
        const buttonIndex = parseInt(
            e.currentTarget.getAttribute('data-index'),
            10
        );
        const currentSong = player.currentSong;
        const clickedOnTrack = !isNaN(buttonIndex);
        const shouldCountTrack = !clickedOnTrack;
        const isAlreadyCurrentSong = isCurrentMusicItem(
            currentSong,
            album,
            shouldCountTrack
        );

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        let queueIndex = getQueueIndexForSong(album, player.queue, {
            trackIndex: buttonIndex,
            currentQueueIndex: player.queueIndex
        });

        if (queueIndex !== -1) {
            dispatch(play(queueIndex));
            return;
        }
        const filteredArtistUploads = artistUploads.list.filter((item) => {
            return item.id !== album.id;
        });
        const queue = [album].concat(filteredArtistUploads);
        const { queue: newQueue } = dispatch(editQueue(queue));

        queueIndex = getQueueIndexForSong(album, newQueue, {
            trackIndex: buttonIndex
        });

        dispatch(play(queueIndex));
    };

    handleNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(fetchSongList());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setDeepLink(album) {
        if (!album) {
            return;
        }

        const link = `audiomack://album/${album.uploader.url_slug}/${
            album.url_slug
        }`;

        this.props.dispatch(
            setBannerDeepLink(link, {
                album: album.id,
                key: parse(this.props.location.search).key
            })
        );
    }

    setSiteWrap() {
        const { dispatch, album } = this.props;

        if (!album) {
            return;
        }

        const notSuspended =
            album.status !== 'suspended' && album.status !== 'takedown';
        const image = notSuspended ? album.image_base || album.image : null;

        dispatch(setGlobalBackground(image));
    }

    displayAdminActions(currentUser, song) {
        if (!currentUser.isLoggedIn) {
            return false;
        }

        if (!currentUser.profile.is_admin) {
            return false;
        }

        if (currentUser.profile.is_admin === true) {
            return (
                <AdminActionsContainer
                    item={song}
                    onActionItemClick={this.handleActionClick}
                />
            );
        }

        return false;
    }

    renderEmptyState() {
        return (
            <div className="empty-state">
                <p>This artist has no other uploads.</p>
            </div>
        );
    }

    renderAlbumHeader() {
        const { album, player, location } = this.props;
        const isCurrentSong = isCurrentMusicItem(player.currentSong, album);
        const { geo_restricted: geoRestricted } = album;
        const isNotReleased =
            album &&
            album.released > Math.floor(Date.now() / 1000) &&
            !this._query.key;

        if (album.status === 'takedown' || album.status === 'suspended') {
            return null;
        }

        const size = 180;
        const retina = buildDynamicImage(album.image_base || album.image, {
            width: size * 2,
            height: size * 2,
            max: true
        });
        const avatarProps = {
            image: buildDynamicImage(album.image_base || album.image, {
                width: size,
                height: size,
                max: true
            }),
            srcSet: `${retina} 2x`,
            size: size,
            rounded: false,
            center: true,
            stack: true
        };

        const actions = ['favorite', 'repost', 'comments', 'share', 'download'];

        let actionSheet = (
            <ActionSheetContainer
                noFlex={false}
                actionItem={album}
                actions={actions}
                onActionItemClick={this.handleActionClick}
                className="u-spacing-top-15 u-spacing-bottom-15"
            />
        );

        if (geoRestricted) {
            actionSheet = null;
        }

        let showPaused = false;

        if (!player.paused && isCurrentSong) {
            showPaused = true;
        }

        const playButtonClass = classnames('avatar__play-button', {
            'avatar__play-button--paused': showPaused
        });

        let playButton = (
            <button className={playButtonClass} onClick={this.handlePlayClick}>
                <PlayIcon
                    title="Play"
                    hidden={showPaused}
                    className="song-play__play"
                />
                <PauseIcon
                    title="Pause"
                    hidden={!showPaused}
                    className="song-play__pause"
                />
            </button>
        );

        if (!album.tracks.length > 0 || isNotReleased || geoRestricted) {
            playButton = null;
        }

        const verified = <Verified user={album.uploader} size={15} />;

        return (
            <header className="u-pos-relative music-header">
                <button
                    className="music-header__info"
                    onClick={this.handleInfoOpen}
                    data-action="info"
                >
                    <InfoIcon />
                </button>
                <div className="avatar-wrap u-pos-relative">
                    <Avatar {...avatarProps} />
                    {playButton}
                </div>
                <h1>
                    <span className="album-container__artist u-spacing-top-10 u-d-block">
                        {album.artist}
                    </span>
                    <Truncate
                        tagName="span"
                        className="album-container__title u-d-block"
                        lines={2}
                        text={album.title}
                    />
                </h1>
                <small className="album-container__uploader">
                    Added by{' '}
                    <Link to={`/artist/${album.uploader.url_slug}`}>
                        {album.uploader.name}
                    </Link>{' '}
                    {verified}
                </small>

                {actionSheet}

                {!geoRestricted && (
                    <ListenOnAppButton
                        musicItem={album}
                        keyPassword={parse(location.search).key}
                    />
                )}
            </header>
        );
    }

    renderGeoNotice(albumList) {
        const { album } = this.props;
        const { geo_restricted: geoRestricted } = album;

        if (!geoRestricted) {
            return albumList;
        }

        return (
            <div className="u-pos-relative u-spacing-top-20">
                {albumList}
                <GeoNotice withBackdrop>
                    The rightsholder has not made this content available in your
                    country.
                </GeoNotice>
            </div>
        );
    }

    render() {
        const {
            album,
            player,
            dispatch,
            currentUser,
            modal,
            history,
            errors,
            location,
            artistUploads,
            songs
        } = this.props;

        if (!album) {
            if (errors.length) {
                return <NotFound errors={errors} type="album" />;
            }

            return null;
        }

        const listProps = {
            dispatch,
            player,
            currentUser,
            songs: {
                data: album.tracks.map((t, i) =>
                    getNormalizedQueueTrack(album, i)
                )
            },
            onItemClick: this.handlePlayClick,
            album,
            location: location,
            albumKey: this._query.key
        };

        const takenDown =
            album.status === 'takedown' || album.status === 'suspended';

        let albumList = <AlbumList {...listProps} />;

        if (takenDown) {
            albumList = (
                <p className="outer-padding no-tracks-found u-fw-600 u-padding-20 u-spacing-x-20 u-spacing-y-40 u-text-center">
                    This album has been removed due to a DMCA Complaint.
                    Discover your next favorite song on our{' '}
                    <Link to="/songs/week">Top Songs</Link>,{' '}
                    <Link to="/albums/week">Top Albums</Link> or{' '}
                    <Link to="/playlists/browse">Top Playlists Charts!</Link>
                </p>
            );
        }

        let moreSongs = {
            data: artistUploads.list.filter((item) => {
                return item.id !== album.id;
            }),
            onLastPage: artistUploads.onLastPage,
            loading: artistUploads.loading
        };

        if (takenDown) {
            moreSongs = songs;
        }

        let moreFrom = (
            <h5 className="u-fs-13 u-fw-800 u-ls-n-06 u-tt-uppercase u-spacing-bottom-10 u-padding-x-10">
                More from {getUploader(album).name}
            </h5>
        );

        if (takenDown) {
            moreFrom = (
                <h5 className="u-fs-13 u-fw-800 u-ls-n-06 u-tt-uppercase u-spacing-bottom-10 u-padding-x-10">
                    Trending on Audiomack
                </h5>
            );
        }

        return (
            <div className="album-container u-padding-top-20 u-pos-relative">
                <MusicPageMeta
                    musicItem={album}
                    currentUser={currentUser}
                    location={location}
                />

                {this.displayAdminActions(currentUser, album)}
                {this.renderAlbumHeader()}

                {this.renderGeoNotice(albumList)}

                <div>
                    {moreFrom}
                    <ListContainer
                        location={location}
                        songs={moreSongs}
                        onNextPage={takenDown ? this.handleNextPage : null}
                        emptyState={this.renderEmptyState()}
                    />
                </div>

                <AlbumInfoModal
                    active={modal.type === MODAL_TYPE_ALBUM_INFO}
                    history={history}
                    onClose={this.handleInfoClose}
                    album={album}
                />

                <div className="column small-24">
                    <CommentsWrapper
                        item={album}
                        kind={KIND_ALBUM}
                        id={album.id}
                        className="u-spacing-top-30 u-padding-x-20"
                        total={album.stats.comments}
                        isMobile
                        history={this.props.history}
                        singleCommentUuid={
                            parse(this.props.location.search).comment
                        }
                        singleCommentThread={
                            parse(this.props.location.search).thread
                        }
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        currentUser: state.currentUser,
        album: state.music.album,
        player: state.player,
        artistUploads: state.artistUploads,
        errors: state.music.errors,
        songs: state.music
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(AlbumContainer, [
            (params, query, props) => {
                const options = {};
                const { currentUser } = props;
                const { token, secret } = currentUser;

                options.key = query.key;
                options.token = token;
                options.secret = secret;

                return getAlbumInfo(params.artistId, params.albumSlug, options);
            },
            (params) => getArtistUploads(params.artistId, { limit: 10 }),
            () => fetchSongList()
        ])
    )
);
