import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class AndroidLoader extends Component {
    static propTypes = {
        className: PropTypes.string,
        color: PropTypes.string,
        strokeWidth: PropTypes.number,
        size: PropTypes.number
    };

    static defaultProps = {
        strokeWidth: 3,
        size: 54,
        color: '#ffa200'
    };

    render() {
        const scale = this.props.size / AndroidLoader.defaultProps.size;
        const size = Math.round(AndroidLoader.defaultProps.size * scale);
        const containerStyle = {
            width: `${size}px`,
            height: `${size}px`
        };

        const circleStyle = {
            transform: `scale(${scale})`
        };

        const style = {
            width: `${AndroidLoader.defaultProps.size}px`,
            height: `${AndroidLoader.defaultProps.size}px`,
            top: -(AndroidLoader.defaultProps.size - this.props.size) / 2,
            left: -(AndroidLoader.defaultProps.size - this.props.size) / 2
        };

        const klass = classnames('loader-container', {
            [this.props.className]: this.props.className
        });

        return (
            <div className={klass} style={containerStyle}>
                <svg className="loader-android" style={style}>
                    <circle
                        style={circleStyle}
                        className="path"
                        stroke={this.props.color}
                        cx="27"
                        cy="27"
                        r="20"
                        fill="none"
                        strokeWidth="4"
                        strokeMiterlimit="10"
                    />
                </svg>
            </div>
        );
    }
}
