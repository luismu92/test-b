import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './Message.module.scss';

export default class Message extends Component {
    static propTypes = {
        message: PropTypes.string,
        visible: PropTypes.bool
    };

    render() {
        const klass = classnames(styles.message, {
            [styles.visible]: this.props.visible
        });

        return <div className={klass}>{this.props.message}</div>;
    }
}
