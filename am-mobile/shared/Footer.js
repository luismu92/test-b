import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import AmLogoMark from './icons/am-logo-mark';
import TwitterIcon from './icons/twitter-logo';
import FacebookIcon from './icons/facebook-block-icon';
import InstagramIcon from './icons/instagram';
import TwitchIcon from 'icons/twitch';
import AppStoreBadge from './icons/app-store';
import GooglePlayBadge from './icons/google-play';

import { getBranchUrl } from 'utils/index';
export default class Footer extends Component {
    static propTypes = {
        deepLink: PropTypes.string,
        deepLinkData: PropTypes.object,
        location: PropTypes.object
    };

    static defaultProps = {
        deepLink: 'audiomack://'
    };

    render() {
        const { deepLink, deepLinkData, location } = this.props;

        const branchUrl = getBranchUrl(process.env.BRANCH_URL, {
            deeplinkPath: deepLink,
            channel: 'Mobile Web',
            campaign: 'Mobile Footer Link',
            ...deepLinkData
        });

        return (
            <footer className="site-footer u-text-white">
                <div className="site-footer__top u-padding-x-20 u-padding-top-20 u-padding-bottom-30">
                    <div>
                        <AmLogoMark className="site-footer__logo" />
                        <p className="site-footer__copyright u-ls-n-03 u-fs-11 u-spacing-top-10 u-text-center">
                            Copyright &copy; {new Date().getFullYear()}. All
                            rights reserved
                        </p>
                    </div>
                    <div className="site-footer__nav u-spacing-top-30">
                        <div className="site-footer__nav-col u-padding-x-10">
                            <h3 className="u-fs-18 u-fw-700 u-spacing-bottom-20">
                                Browse
                            </h3>
                            <ul>
                                <li>
                                    <Link to="/">Home</Link>
                                </li>
                                <li>
                                    <Link to="/trending-now">Trending</Link>
                                </li>
                                <li>
                                    <Link to="/songs/week">Top Songs</Link>
                                </li>
                                <li>
                                    <Link to="/albums/week">Top Albums</Link>
                                </li>
                                <li>
                                    <Link to="/playlists/browse">
                                        Top Playlists
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/artists/popular">
                                        Accounts to Follow
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/recent">Recently Added</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="site-footer__nav-col u-padding-x-10">
                            <h3 className="u-fs-18 u-fw-700 u-spacing-bottom-20">
                                About Us
                            </h3>
                            <ul>
                                <li>
                                    <a
                                        href="https://audiomack.zendesk.com"
                                        rel="nofollow noopener"
                                        target="_blank"
                                    >
                                        Support
                                    </a>
                                </li>
                                <li>
                                    <Link to="/about">About Us</Link>
                                </li>
                                <li>
                                    <a
                                        href="https://audiomack.com/world"
                                        rel="nofollow noopener"
                                        target="_blank"
                                    >
                                        Blog
                                    </a>
                                </li>
                                <li>
                                    <Link to="/about/legal">
                                        Legal &amp; DMCA
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/about/privacy-policy">
                                        Privacy Policy
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/about/privacy-policy#accessing-updating">
                                        Do not sell my information
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/about/terms-of-service">
                                        Terms of Service
                                    </Link>
                                </li>
                                <li>
                                    <a
                                        href="https://styleguide.audiomack.com"
                                        rel="nofollow noopener"
                                        target="_blank"
                                    >
                                        Styleguide
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="site-footer__bottom u-padding-x-20 u-padding-top-30 u-padding-bottom-40">
                    <div className="site-footer__social u-d-flex u-d-flex--align-center u-d-flex--justify-center u-spacing-bottom-30">
                        <h4 className="u-fs-18 u-fw-700 u-spacing-right-30">
                            Follow Us
                        </h4>
                        <a
                            href="https://twitter.com/audiomack"
                            target="_blank"
                            rel="nofollow noopener"
                            className="site-footer__share-button  site-footer__share-button--twitter u-d-inline-block"
                        >
                            <TwitterIcon title="Audiomack on Twitter" />
                        </a>
                        <a
                            href="https://www.facebook.com/audiomack"
                            target="_blank"
                            rel="nofollow noopener"
                            className="site-footer__share-button u-d-inline-block"
                        >
                            <FacebookIcon title="Audiomack on Facebook" />
                        </a>
                        <a
                            href="https://www.instagram.com/audiomack"
                            target="_blank"
                            rel="nofollow noopener"
                            className="site-footer__share-button u-d-inline-block"
                        >
                            <InstagramIcon title="Audiomack on Instagram" />
                        </a>
                        <a
                            href="https://twitch.tv/audiomack"
                            target="_blank"
                            rel="nofollow noopener"
                            className="site-footer__share-button u-d-inline-block"
                        >
                            <TwitchIcon />
                        </a>
                    </div>
                    <ul className="app-badges site-footer__app-badges u-text-center u-d-flex u-d-flex--align-center">
                        <li className="u-d-inline-block">
                            <a
                                href={branchUrl}
                                target="_blank"
                                rel="nofollow noopener"
                                aria-label="Audiomack on iOS"
                            >
                                <AppStoreBadge />
                            </a>
                        </li>
                        <li className="u-d-inline-block u-spacing-left-10">
                            <a
                                href={branchUrl}
                                target="_blank"
                                rel="nofollow noopener"
                                aria-label="Audiomack on Google Play"
                            >
                                <GooglePlayBadge />
                            </a>
                        </li>
                    </ul>
                    <p className="u-spacing-top-20 u-fs-14 u-fw-600 u-text-center">
                        <a href={`${location.pathname}?showDesktop=true`}>
                            Switch to desktop version
                        </a>
                    </p>
                </div>
            </footer>
        );
    }
}
