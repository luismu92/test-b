import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import {
    timeMap,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_RECENTLY_ADDED,
    CHART_TYPE_TOTAL,
    PLAYLIST_TYPE_BROWSE,
    PLAYLIST_TYPE_MINE,
    PLAYLIST_TYPE_FAVORITES
} from 'constants/index';

import {
    setPlaylistContext,
    setContext,
    setGenre,
    setTimePeriod,
    clearList
} from '../redux/modules/music';

import BrowseContext from './BrowseContext';

class BrowseContextContainer extends Component {
    static propTypes = {
        songs: PropTypes.object,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextSwitch = (context) => {
        const { dispatch } = this.props;
        const playlistTypes = [
            PLAYLIST_TYPE_BROWSE,
            PLAYLIST_TYPE_MINE,
            PLAYLIST_TYPE_FAVORITES
        ];

        dispatch(clearList());

        if (playlistTypes.indexOf(context) !== -1) {
            dispatch(setPlaylistContext(context));
            return;
        }

        dispatch(setContext(context));
    };

    handleDropdownChange = (key, value) => {
        const { dispatch, songs } = this.props;
        const { activeGenre, activeTimePeriod, activeContext } = songs;

        switch (key) {
            case 'genre': {
                dispatch(setGenre(value));
                let suffix = activeTimePeriod
                    ? `/${timeMap[activeTimePeriod]}`
                    : '';

                // Trending & default recent lists dont have weekly, monthly, etc
                if (
                    activeContext === COLLECTION_TYPE_TRENDING ||
                    activeContext === COLLECTION_TYPE_RECENTLY_ADDED ||
                    activeContext === ''
                ) {
                    suffix = '';
                }

                const genre = value ? `/${value}` : '';
                const url = `${genre}/${activeContext}${suffix}`;

                this.props.history.push(url);
                break;
            }

            case 'timePeriod': {
                let period = timeMap[value] || value;

                if (!value) {
                    period = CHART_TYPE_TOTAL;
                }

                dispatch(setTimePeriod(period));
                const prefix = activeGenre ? `/${activeGenre}` : '';
                const suffix = period ? `/${period}` : '';

                this.props.history.push(`${prefix}/${activeContext}${suffix}`);
                break;
            }

            default:
                throw new Error(`${key} not accounted for in switch case`);
        }
    };

    render() {
        return (
            <BrowseContext
                songs={this.props.songs}
                currentUser={this.props.currentUser}
                onContextSwitch={this.handleContextSwitch}
                onDropdownChange={this.handleDropdownChange}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(BrowseContextContainer));
