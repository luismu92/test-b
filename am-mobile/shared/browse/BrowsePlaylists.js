import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import AndroidLoader from '../loaders/AndroidLoader';

import ContextSwitcher from '../widgets/ContextSwitcher';
import PlaylistCard from '../components/PlaylistCard';

import Button from 'buttons/Button';

import styles from './BrowsePlaylists.module.scss';

export default class BrowsePlaylists extends Component {
    static propTypes = {
        contextItems: PropTypes.array,
        onContextSwitch: PropTypes.func,
        onLoadMoreClick: PropTypes.func,
        playlists: PropTypes.object
    };

    renderPlaylists(list) {
        if (!list || !list.length) {
            return null;
        }

        const playlists = list.map((playlist, i) => {
            return (
                <div key={`playlist-${i}`}>
                    <PlaylistCard playlist={playlist} />
                </div>
            );
        });

        return <div className={styles.grid}>{playlists}</div>;
    }

    renderLoadMoreButton(playlists) {
        const { onLoadMoreClick } = this.props;
        const { page, onLastPage } = playlists;

        if (onLastPage) {
            return null;
        }

        const buttonProps = {
            'data-page': page,
            onClick: onLoadMoreClick
        };

        const buttonStyle = {
            fontSize: '1.4rem'
        };

        return (
            <div className={styles.buttonWrap}>
                <Button
                    text="Load More"
                    width={200}
                    height={50}
                    style={buttonStyle}
                    props={buttonProps}
                />
            </div>
        );
    }

    render() {
        const { playlists } = this.props;
        const { list, loading } = playlists;

        let emptyState;
        if (!list.length && !loading) {
            emptyState = (
                <p className="u-text-center">
                    Sorry, there are no playlists to display.
                </p>
            );
        }

        let loader;
        if (loading) {
            loader = (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <Fragment>
                <ContextSwitcher
                    items={this.props.contextItems}
                    onContextSwitch={this.props.onContextSwitch}
                />
                <div style={{ padding: '20px 15px' }}>
                    {emptyState}
                    {loader}
                    {this.renderPlaylists(list)}
                    {this.renderLoadMoreButton(playlists)}
                </div>
            </Fragment>
        );
    }
}
