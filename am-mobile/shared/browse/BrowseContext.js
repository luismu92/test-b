import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    allGenresMap,
    timeMap,
    liveGenres,
    liveGenresExcludedFromCharts,
    CHART_TYPE_WEEKLY,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    COLLECTION_TYPE_PLAYLIST,
    COLLECTION_TYPE_BLOG,
    GENRE_TYPE_ALL,
    CHART_TYPE_DAILY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY,
    CHART_TYPE_TOTAL
} from 'constants/index';

import ContextSwitcher from '../widgets/ContextSwitcher';

export default class BrowseContext extends Component {
    static propTypes = {
        songs: PropTypes.object,
        currentUser: PropTypes.object,
        onContextSwitch: PropTypes.func,
        onDropdownChange: PropTypes.func
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getContextItems(activeContext, genre = '', timePeriod = CHART_TYPE_WEEKLY) {
        let prefix = '';
        let suffix = '';

        if (genre) {
            prefix = `${genre}/`;
        }

        if (timePeriod) {
            suffix = `/${timeMap[timePeriod]}`;
        }

        return [
            {
                text: 'World',
                href: '/world',
                value: COLLECTION_TYPE_BLOG,
                active: activeContext === COLLECTION_TYPE_BLOG
            },
            {
                text: 'Trending',
                href: `/${prefix}${COLLECTION_TYPE_TRENDING}`,
                value: COLLECTION_TYPE_TRENDING,
                active:
                    activeContext === '' ||
                    activeContext === COLLECTION_TYPE_TRENDING
            },
            {
                text: 'Top Songs',
                href: `/${prefix}${COLLECTION_TYPE_SONG}${suffix}`,
                value: COLLECTION_TYPE_SONG,
                active: activeContext === COLLECTION_TYPE_SONG
            },
            {
                text: 'Top Albums',
                href: `/${prefix}${COLLECTION_TYPE_ALBUM}${suffix}`,
                value: COLLECTION_TYPE_ALBUM,
                active: activeContext === COLLECTION_TYPE_ALBUM
            },
            {
                text: 'Recently Added',
                href: `/${prefix}${COLLECTION_TYPE_RECENTLY_ADDED}`,
                value: COLLECTION_TYPE_RECENTLY_ADDED,
                active: activeContext === COLLECTION_TYPE_RECENTLY_ADDED
            }
        ];
    }

    getDropdownItems(activeContext, activeGenre, activeTimePeriod) {
        const dropdownItems = {
            genre: liveGenres
                .map((liveGenre) => {
                    const item = {
                        text: allGenresMap[liveGenre],
                        value: liveGenre,
                        active: activeGenre === liveGenre
                    };

                    if (liveGenre === GENRE_TYPE_ALL) {
                        item.text = 'All Genres';
                    }

                    return item;
                })
                .filter(({ value: genre }) => {
                    const onlyInTrending = liveGenresExcludedFromCharts.includes(
                        genre
                    );
                    // Remove podcast in case of top albums/songs but keep in recent
                    if (
                        activeContext === COLLECTION_TYPE_ALBUM ||
                        activeContext === COLLECTION_TYPE_SONG
                    ) {
                        return !onlyInTrending;
                    }

                    return true;
                })
        };

        const collectionsThatNeedTime = [
            COLLECTION_TYPE_SONG,
            COLLECTION_TYPE_ALBUM,
            COLLECTION_TYPE_PLAYLIST
        ];

        if (collectionsThatNeedTime.indexOf(activeContext) !== -1) {
            dropdownItems.timePeriod = [
                {
                    text: 'Today',
                    value: CHART_TYPE_DAILY,
                    active: CHART_TYPE_DAILY === activeTimePeriod
                },
                {
                    text: 'This Week',
                    value: CHART_TYPE_WEEKLY,
                    active: CHART_TYPE_WEEKLY === activeTimePeriod
                },
                {
                    text: 'This Month',
                    value: CHART_TYPE_MONTHLY,
                    active: CHART_TYPE_MONTHLY === activeTimePeriod
                },
                {
                    text: 'This Year',
                    value: CHART_TYPE_YEARLY,
                    active: CHART_TYPE_YEARLY === activeTimePeriod
                },
                {
                    text: 'All Time',
                    value: CHART_TYPE_TOTAL,
                    active: CHART_TYPE_TOTAL === activeTimePeriod
                }
            ];
        }

        return dropdownItems;
    }

    render() {
        const { songs, currentUser } = this.props;
        const { activeContext, activeGenre, activeTimePeriod } = songs;

        let dropdownItems = {};
        // Only show dropdown options if we're not on a blog page
        if (activeContext !== COLLECTION_TYPE_BLOG) {
            dropdownItems = this.getDropdownItems(
                activeContext,
                activeGenre,
                activeTimePeriod
            );
        }

        return (
            <ContextSwitcher
                items={this.getContextItems(
                    activeContext,
                    activeGenre,
                    activeTimePeriod,
                    currentUser
                )}
                dropdownItems={dropdownItems}
                activeContext={activeContext}
                onContextSwitch={this.props.onContextSwitch}
                onDropdownChange={this.props.onDropdownChange}
            />
        );
    }
}
