import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import BrowsePageMeta from 'components/BrowsePageMeta';
import Cookie from 'js-cookie';
import { parse } from 'query-string';

import connectDataFetchers from 'lib/connectDataFetchers';
import { cookies } from 'constants/index';

import {
    setContext,
    nextPage,
    setGenre,
    setTimePeriod,
    fetchSongList,
    clearList
} from '../redux/modules/music';

import { setBannerDeepLink } from '../redux/modules/global';
import { setLastDisplayTime } from '../redux/modules/ad';
import { setActiveMarker, NAV_MARKER_TRENDING } from '../redux/modules/nav';

import Browse from './Browse';

class BrowseContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        route: PropTypes.object,
        ad: PropTypes.object,
        songs: PropTypes.object,
        player: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        actionCreators: PropTypes.array,
        dispatch: PropTypes.func,
        history: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const { dispatch, ad } = this.props;

        // When landing on the home page, we allow 12 minutes to go by
        // before displaying an ad
        if (!ad.lastDisplayed && !Cookie.get(cookies.lastAdDisplayTime)) {
            dispatch(setLastDisplayTime(Date.now()));
        }

        const link = this.getDeepLink(this.props);

        dispatch(
            setBannerDeepLink(link, {
                key: parse(this.props.location.search).key
            })
        );
    }

    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            const link = this.getDeepLink(this.props);

            this.props.dispatch(
                setBannerDeepLink(link, {
                    key: parse(this.props.location.search).key
                })
            );
        }

        this.refetchIfNecessary(prevProps, this.props);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(fetchSongList());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getDeepLink(props) {
        const { page, timePeriod } = props.match.params;
        const { genre, context } = props.route;
        const genreSlug = genre ? `/${genre}` : '';
        // Deep link uses trending instead of trending-now
        const contextSlug = (context ? `/${context}` : '').replace(
            '/trending-now',
            '/trending'
        );
        const timePeriodSlug = timePeriod ? `/${timePeriod}` : '';
        const pageSlug = page ? `/page/${page}` : '';

        return `audiomack:/${genreSlug}${contextSlug}${timePeriodSlug}${pageSlug}`;
    }

    refetchIfNecessary(currentProps, nextProps) {
        const changedGenre = currentProps.route.genre !== nextProps.route.genre;
        const changedContext =
            currentProps.route.context !== nextProps.route.context;
        const changedTime =
            currentProps.match.params.timePeriod !==
            nextProps.match.params.timePeriod;

        const { dispatch } = currentProps;

        if (changedGenre || changedContext || changedTime) {
            if (changedContext) {
                dispatch(setContext(nextProps.route.context));
            }

            dispatch(clearList());
            dispatch(fetchSongList());
        }
    }

    render() {
        const { songs } = this.props;
        const {
            activeContext,
            activeGenre,
            activeTimePeriod,
            page,
            onLastPage
        } = songs;

        return (
            <Fragment>
                <BrowsePageMeta
                    page={page}
                    onLastPage={onLastPage}
                    activeGenre={activeGenre}
                    activeContext={activeContext}
                    activeTimePeriod={activeTimePeriod}
                />
                <Browse {...this.props} onNextPage={this.handleNextPage} />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser,
        songs: state.music,
        player: state.player
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(BrowseContainer, [
            () => setActiveMarker(NAV_MARKER_TRENDING),
            (params, query, props) => {
                if (!props.route) {
                    return null;
                }

                return setContext(props.route.context);
            },
            (params, query, props) => {
                if (!props.route) {
                    return null;
                }

                return setGenre(props.route.genre);
            },
            (params) => setTimePeriod(params.timePeriod),
            () => fetchSongList()
        ])
    )
);
