import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ListContainer from '../list/ListContainer';

export default class Browse extends Component {
    static propTypes = {
        songs: PropTypes.object,
        location: PropTypes.object,
        onNextPage: PropTypes.func
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const { songs, location, onNextPage } = this.props;

        const filteredSongs = {
            ...songs,
            data: songs.data.filter((song) => !song.geo_restricted)
        };

        return (
            <div className="list-view">
                <ListContainer
                    location={location}
                    onNextPage={onNextPage}
                    songs={filteredSongs}
                />
            </div>
        );
    }
}
