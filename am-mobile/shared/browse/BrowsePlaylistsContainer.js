import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import connectDataFetchers from 'lib/connectDataFetchers';

import BrowsePlaylistMeta from 'components/BrowsePlaylistMeta';

import { setBannerDeepLink } from '../redux/modules/global';
import { setActiveMarker, NAV_MARKER_PLAYLIST } from '../redux/modules/nav';
import { setPlaylistContext } from '../redux/modules/music';

import {
    clearList,
    nextPage,
    fetchTaggedPlaylists,
    fetchAllPlaylistTags
} from '../redux/modules/music/browsePlaylist';

import BrowsePlaylists from './BrowsePlaylists';

// Default to Verified series tag until we have "most popular" endpoint setup
const DEFAULT_PLAYLIST_SLUG = 'verified-series';

class BrowsePlaylistsContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        songs: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func,
        match: PropTypes.object,
        browsePlaylist: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.props.dispatch(
            setBannerDeepLink('audiomack://playlists', {
                key: parse(this.props.location.search).key
            })
        );
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(clearList());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextSwitch = (context) => {
        const { dispatch } = this.props;

        dispatch(setPlaylistContext(context));
    };

    handleLoadMoreClick = (e) => {
        const { dispatch, browsePlaylist } = this.props;
        const button = e.currentTarget;
        const tag = browsePlaylist.tag;
        const page = parseInt(button.getAttribute('data-page'), 10) || 1;
        const limit = 20;

        dispatch(nextPage());
        dispatch(fetchTaggedPlaylists(tag, limit, page + 1));
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getContextItems(activePlaylistContext) {
        const {
            browsePlaylist: { tags: playlistTags }
        } = this.props;

        const tags = playlistTags.map((tag, i) => {
            return {
                key: `tag-${i}`,
                text: tag.title,
                href: `/playlists/browse/${tag.url_slug}`,
                value: tag.url_slug,
                active: activePlaylistContext === tag.url_slug
            };
        });

        return tags;
    }

    refetchIfNecessary(currentProps, nextProps) {
        const { dispatch } = this.props;
        const nextTag = nextProps.match.params.urlSlug;
        const changedPage = currentProps.match.params.urlSlug !== nextTag;

        if (changedPage) {
            let newTag = nextTag;

            // If you go from a tag page, back to /playlists/browse it kills the page.
            // Set some fallbacks here to avoid that
            if (!nextTag) {
                newTag = DEFAULT_PLAYLIST_SLUG;
                dispatch(setPlaylistContext(DEFAULT_PLAYLIST_SLUG));
            }

            dispatch(clearList());
            dispatch(fetchTaggedPlaylists(newTag, 20));
        }
    }

    render() {
        const { songs, currentUser, browsePlaylist } = this.props;
        const { activePlaylistContext } = songs;

        return (
            <div>
                <BrowsePlaylistMeta
                    urlSlug={
                        this.props.match.params.urlSlug || DEFAULT_PLAYLIST_SLUG
                    }
                    category={browsePlaylist.title}
                />
                <BrowsePlaylists
                    contextItems={this.getContextItems(
                        activePlaylistContext,
                        currentUser
                    )}
                    onContextSwitch={this.handleContextSwitch}
                    onLoadMoreClick={this.handleLoadMoreClick}
                    playlists={browsePlaylist}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        songs: state.music,
        ad: state.ad,
        playlist: state.playlist,
        currentUser: state.currentUser,
        browsePlaylist: state.browsePlaylist
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(BrowsePlaylistsContainer, [
        () => setActiveMarker(NAV_MARKER_PLAYLIST),
        (params) => setPlaylistContext(params.urlSlug || DEFAULT_PLAYLIST_SLUG),
        () => fetchAllPlaylistTags(),
        (params) =>
            fetchTaggedPlaylists(params.urlSlug || DEFAULT_PLAYLIST_SLUG, 20)
    ])
);
