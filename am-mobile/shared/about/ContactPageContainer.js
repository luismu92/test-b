import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { cleanParameters } from 'utils/index';
import { showMessage } from '../redux/modules/message';

import ContactPage from './ContactPage';
import { sendContactForm, sendApiForm } from '../redux/modules/email';

class ContactPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        history: PropTypes.object,
        match: PropTypes.object,
        email: PropTypes.object,
        onContactLinkClick: PropTypes.func
    };

    constructor(props) {
        super(props);

        const formType = this.getInitialForm(props.match.params.initialForm);

        this.state = {
            ...this.getInputStateForForm(formType),
            platform: '',
            issue: ''
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleDropdownChange = (e) => {
        const menu = e.currentTarget;
        const menuName = menu.name;
        const value = menu.options[menu.selectedIndex].value;

        switch (menuName) {
            case 'type':
                this.setState(this.getInputStateForForm(value));
                break;

            case 'platform':
            case 'issue':
                this.setState({
                    [menuName]: value
                });
                break;

            default:
                break;
        }
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        const {
            urlInputHidden,
            platformHidden,
            issueInputHidden,
            nameInputHidden,
            phoneInputHidden,
            companyInputHidden,
            platform,
            issue,
            currentForm
        } = this.state;
        const { dispatch } = this.props;
        const form = e.currentTarget;
        const data = cleanParameters({
            type: currentForm,
            emailAddress: form.email.value,
            description: form.message.value,
            platform: platformHidden ? null : platform,
            url: urlInputHidden ? null : form.url.value,
            name: nameInputHidden ? null : form.name.value,
            phone: phoneInputHidden ? null : form.phone.value,
            issue: issueInputHidden ? null : issue,
            company: companyInputHidden ? null : form.company.value
        });

        let action;

        if (data.type !== '6') {
            action = sendContactForm(data);
        } else {
            action = sendApiForm(data);
        }

        dispatch(action)
            .then(() => {
                const message =
                    'Your message has been sent and someone will be in touch soon.';

                dispatch(showMessage(message));

                this.props.history.goBack();
                return;
            })
            .catch((err) => console.log(err));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getInputStateForForm(formType = '') {
        const linkValues = ['0', '1'];
        const nameValues = ['2', '6'];

        const formValues = {
            urlInputHidden: linkValues.indexOf(formType) === -1,
            urlInputRequired: formType === '1',
            platformHidden: formType !== '0',
            issueInputHidden: formType !== '0',
            nameInputHidden: nameValues.indexOf(formType) === -1,
            phoneInputHidden: formType !== '2',
            companyInputHidden: formType !== '6',
            currentForm: formType
        };

        return formValues;
    }

    getInitialForm(formName) {
        switch (formName) {
            case 'api':
                return '6';

            default:
                return '';
        }
    }

    render() {
        const { email } = this.props;

        return (
            <ContactPage
                onFormSubmit={this.handleFormSubmit}
                onContactLinkClick={this.props.onContactLinkClick}
                onDropdownChange={this.handleDropdownChange}
                email={email}
                urlInputHidden={this.state.urlInputHidden}
                urlInputRequired={this.state.urlInputRequired}
                platformHidden={this.state.platformHidden}
                issueInputHidden={this.state.issueInputHidden}
                nameInputHidden={this.state.nameInputHidden}
                phoneInputHidden={this.state.phoneInputHidden}
                companyInputHidden={this.state.companyInputHidden}
                currentForm={this.state.currentForm}
                issue={this.state.issue}
                platform={this.state.platform}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        email: state.email
    };
}

export default withRouter(connect(mapStateToProps)(ContactPageContainer));
