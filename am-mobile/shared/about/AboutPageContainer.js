import React, { Fragment } from 'react';

export default function AboutPageContainer() {
    const currentDate = new Date().getFullYear();
    const features = [
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    <span className="u-text-orange">
                        Share your music with millions
                    </span>{' '}
                    of highly engaged listeners
                </h3>
            ),
            copy: (
                <p>
                    Millions of fans use Audiomack daily to discover the hottest
                    trending music anywhere. Upload your music and submit it for
                    Trending consideration (completely free) to get the chance
                    to be placed in front of a massive new audience.
                </p>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    Powerful{' '}
                    <span className="u-text-orange">Artist Dashboard</span> and
                    Stats
                </h3>
            ),
            copy: (
                <p>
                    Audiomack gives you an advanced Artist Dashboard with
                    in-depth stats on how your content is being consumed,
                    completely free - no premium account needed. See engagement
                    and granular data on how fans are interacting with your
                    releases.
                </p>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    Beautiful and functional{' '}
                    <span className="u-text-orange">Embeddable Players</span>
                </h3>
            ),
            copy: (
                <p>
                    Every upload on Audiomack creates a free, customizable embed
                    player which can be placed on any 3rd party blog, platform,
                    or site. Use embeds to let your music go wide, then funnel
                    fans to your highest value platforms (merch, ticket sales,
                    etc) using the built-in Buy button.
                </p>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    Plan your{' '}
                    <span className="u-text-orange">
                        releases with confidence.
                    </span>
                </h3>
            ),
            copy: (
                <p>
                    A hosting platform is only useful if it's there when you
                    need it most - on release day. Tired of having your press
                    release cycles ruined by unreliable hosting? Audiomack has
                    served billions of streams with 99.9% uptime over the last
                    12 months. Plan and release with confidence using Audiomack.
                </p>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    More than hosting -{' '}
                    <span className="u-text-orange">
                        a free Marketing platform
                    </span>
                </h3>
            ),
            copy: (
                <Fragment>
                    <p>
                        We firmly believe your music should live everywhere, not
                        just on Audiomack. All Audiomack uploads are compatible
                        with Hype Machine, and creators can embed a YouTube
                        video on every Song and Album page.
                    </p>
                </Fragment>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    <span className="u-text-orange">Monetize your audio</span>{' '}
                    on Audiomack
                </h3>
            ),
            copy: (
                <Fragment>
                    <p>
                        Our AMP monetization program is currently in use by
                        thousands of creators, earning artists a competitive
                        per-stream rate on their Audiomack uploads.
                    </p>
                    <p>
                        AMP is currently in advanced beta and will roll out to
                        all Audiomack creators in {currentDate + 1}.
                    </p>
                </Fragment>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    Unlimited everything -{' '}
                    <span className="u-text-orange">for Free</span>
                </h3>
            ),
            copy: (
                <p>
                    It's {new Date().getFullYear()} - no creator should be
                    paying $15 or more a month for storage space. Audiomack
                    gives you all the advanced features you need to manage your
                    career - including unlimited hosting, advanced stats,
                    powerful scheduling and content sharing tools, and
                    monetization - completely free.
                </p>
            )
        },
        {
            title: (
                <h3 className="create-feature__title u-fw-700 u-lh-12">
                    iOS and Android apps{' '}
                    <span className="u-text-orange">for Free</span>
                </h3>
            ),
            copy: (
                <p>
                    Downloaded over a million times in less than six months
                    since launch, our cross platform apps allow you to discover,
                    favorite, and share music on the go.
                </p>
            )
        }
    ].map((feature, i) => {
        return (
            <div key={`feature-${i}`} style={{ marginBottom: 50 }}>
                <h3 style={{ marginBottom: '0.8em' }}>{feature.title}</h3>
                {feature.copy}
            </div>
        );
    });

    return (
        <div className="content-page">
            <div className="body-text">
                <h2 className="auth__title content-page__title">
                    About Audiomack
                </h2>
                {features}
            </div>
        </div>
    );
}
