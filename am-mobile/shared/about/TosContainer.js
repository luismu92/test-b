import React, { Component } from 'react';

import Helmet from 'react-helmet';

import TosContent from 'components/TosContent';

class TosContainer extends Component {
    render() {
        const title = 'Terms of Service';
        const canonical = `${process.env.AM_URL}/about/termsofservice`;

        return (
            <div className="privacy-page" style={{ paddingTop: 0 }}>
                <Helmet>
                    <title>{title}</title>
                    <link rel="canonical" href={canonical} />
                </Helmet>
                <TosContent />
            </div>
        );
    }
}

export default TosContainer;
