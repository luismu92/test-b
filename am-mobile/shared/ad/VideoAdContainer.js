import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { showMessage } from '../redux/modules/message';

import VideoAdContainer from 'components/ad/VideoAdContainer';

class VideoAdContainerWrapper extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAdStarted = () => {
        const message = 'Advertisement: Your content will begin shortly';

        this.props.dispatch(showMessage(message));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <VideoAdContainer
                onAdStarted={this.handleAdStarted}
                location={this.props.location}
                isMobile
                showSkipButton
            />
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(VideoAdContainerWrapper);
