import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { shouldNotShowAd } from 'utils/index';
import FreestarContainer from 'components/ad/FreestarContainer';
import GooglePublisherTag from 'components/ad/GooglePublisherTag';
import ImaSDK from 'components/ad/ImaSDK';
import PageAd from 'components/ad/PageAd';

export default class BannerAd extends Component {
    static propTypes = {
        bottom: PropTypes.number,
        active: PropTypes.bool,
        currentUser: PropTypes.object,
        location: PropTypes.object,
        showAlternate: PropTypes.bool
    };

    static defaultProps = {
        active: true
    };

    componentWillUnmount() {
        clearTimeout(this._googleRefreshTimer);
        this._googleRefreshTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleImaLoaded = () => {
        console.log('ima loaded');
    };

    // interstitial
    handlePageAdLoaded = () => {
        console.log('page ad loaded');

        (window.adsbygoogle || []).push({
            google_ad_client: 'ca-pub-3858157454086512',
            enable_page_level_ads: true
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const {
            location,
            bottom,
            active,
            currentUser,
            showAlternate
        } = this.props;

        const style = {
            bottom: bottom
        };
        const isMobile = true;
        const disabled = shouldNotShowAd(
            location.pathname,
            currentUser,
            isMobile
        );

        if (!active) {
            return null;
        }
        const isUploader =
            currentUser.isLoggedIn &&
            currentUser.profile.upload_count_excluding_reups > 0;
        const extraData = {
            uploader: isUploader ? 'Yes' : 'No'
        };

        let content;

        if (showAlternate) {
            let freestarData;

            // Freestar wants us to do this a little custom
            if (isUploader) {
                freestarData = {
                    Category: 'uploader'
                };
            }

            content = (
                <FreestarContainer
                    location={location}
                    type="leaderboard"
                    isMobile
                    disabled={disabled}
                    slotSuffix="banner"
                    extraData={freestarData}
                />
            );
        } else {
            const disableAds = true;
            // This ad is rendering outside of the container at the moment
            // so I'm disabling it entirely
            if (disableAds) {
                return null;
            }
            // This placeholder doesnt exist
            // const placeholder = '/static/images/desktop/am-partner-placeholder-728x90.jpg';
            const slot = {
                adUnitPath: '/72735579/HTTPS-Mobile-300x250-Adhesive',
                sizes: [[320, 50]],
                divId: 'div-gpt-ad-1554925487383-0'
            };
            const props = {
                slot,
                disabled
            };

            props.targetingKey = 'uploader';
            props.targetingValue = extraData.uploader;
            // props.placeholder = placeholder;

            content = <GooglePublisherTag {...props} />;
        }

        if (disabled) {
            return content;
        }

        return (
            <div className="banner-ad" style={style}>
                <ImaSDK onScriptLoaded={this.handleImaLoaded} isMobile />
                <PageAd onScriptLoaded={this.handlePageAdLoaded} isMobile />
                {content}
            </div>
        );
    }
}
