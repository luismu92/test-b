import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { shouldNotShowAd } from 'utils/index';
import FreestarContainer from 'components/ad/FreestarContainer';
import GooglePublisherTag from 'components/ad/GooglePublisherTag';

export default class InContentAd extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        // A unique key usually an index within a list
        adCount: PropTypes.number.isRequired,
        showAlternate: PropTypes.bool
    };

    render() {
        const { location, currentUser, showAlternate, adCount } = this.props;
        const extraData = {
            isUploader:
                currentUser.isLoggedIn &&
                currentUser.profile.upload_count_excluding_reups > 0
        };
        const isMobile = true;
        const disabled = shouldNotShowAd(
            location.pathname,
            currentUser,
            isMobile
        );

        if (showAlternate) {
            let freestarData;

            // Freestar wants us to do this a little custom
            if (extraData.isUploader) {
                freestarData = {
                    Category: 'uploader'
                };
            }

            return (
                <FreestarContainer
                    location={location}
                    adSize="incontent"
                    slotSuffix={`incontent${adCount}`}
                    disabled={disabled}
                    isMobile={isMobile}
                    extraData={freestarData}
                />
            );
        }

        const placeholder =
            '/static/images/desktop/am-partner-placeholder-300x250.jpg';
        const slot = {
            adUnitPath: '/72735579/HTTPS-Mobile-300x250-InContent',
            sizes: [[300, 250]],
            // These were the original content sizes from complex but they dont
            // exist on this gpt tag... yet.
            // sizes: [[300, 250], [300, 600], [100, 100]],
            divId: `div-gpt-ad-1554928514194-${adCount}`
        };
        const props = {
            slot,
            disabled,
            targetingKey: 'uploader',
            targetingValue: extraData.isUploader ? 'Yes' : 'No',
            placeholder: placeholder
        };

        return <GooglePublisherTag {...props} />;
    }
}
