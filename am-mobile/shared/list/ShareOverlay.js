import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import ActionSheetContainer from '../ActionSheetContainer';

export default class ShareOverlay extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired,
        item: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        actions: PropTypes.array.isRequired,
        onClose: PropTypes.func,
        activeGenre: PropTypes.string
    };

    static defaultProps = {
        onClose() {}
    };

    handleActionItemClick = (action) => {
        if (action === 'close') {
            this.props.onClose();
        }
    };

    render() {
        const { open, actions, currentUser, item } = this.props;
        const klass = classnames('share-overlay', {
            'share-overlay--active': open
        });

        return (
            <div className={klass}>
                <ActionSheetContainer
                    currentUser={currentUser}
                    actionItem={item}
                    actions={actions}
                    noFlex={false}
                    hideStats={false}
                    hideLabel={false}
                    onActionItemClick={this.handleActionItemClick}
                    className="action-sheet--overlay"
                    activeGenre={this.props.activeGenre}
                    {...this.props}
                />
            </div>
        );
    }
}
