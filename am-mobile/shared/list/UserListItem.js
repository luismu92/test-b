import React from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';

import { humanizeNumber } from 'utils/index';

import Avatar from '../Avatar';
import FollowButtonContainer from '../FollowButtonContainer';
import Verified from 'components/Verified';

import styles from './UserListItem.module.scss';

export default class UserListItem extends React.Component {
    static propTypes = {
        item: PropTypes.object.isRequired
    };

    render() {
        const { item } = this.props;

        let followers;

        if (item.followers_count) {
            followers = (
                <p className={styles.count}>
                    {humanizeNumber(item.followers_count)}{' '}
                    {item.followers_count === 1 ? 'follower' : 'followers'}
                </p>
            );
        }

        const check = (
            <Verified user={item} size={15} style={{ marginLeft: 5 }} />
        );

        return (
            <div className={styles.item} role="listitem">
                <div className={styles.inner}>
                    <div className={styles.image}>
                        <Link to={`/artist/${item.url_slug}`}>
                            <Avatar image={item.image} size={60} />
                        </Link>
                    </div>
                    <div>
                        <h2 className={styles.title}>
                            <Link to={`/artist/${item.url_slug}`}>
                                {item.name}
                                {check}
                            </Link>
                        </h2>
                        {followers}

                        <FollowButtonContainer
                            artist={item}
                            className="list-item__action-button"
                        />
                    </div>
                </div>
            </div>
        );
    }
}
