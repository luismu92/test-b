import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { COLLECTION_TYPE_TRENDING } from 'constants/index';
import {
    getTwitterShareLink,
    getFacebookShareLink,
    getMusicUrl,
    copyToClipboard,
    nativeShare,
    getUploader
} from 'utils/index';
import {
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction
} from '../redux/modules/user';
import { getPlaylistsWithSongId } from '../redux/modules/user/playlists';
import { addItems, showDrawer, hideDrawer } from '../redux/modules/drawer';
import { showMessage } from '../redux/modules/message';
import {
    favoritePlaylist,
    unfavoritePlaylist,
    addSong
} from '../redux/modules/playlist';
import {
    showModal,
    MODAL_TYPE_ADD_TO_PLAYLIST,
    MODAL_TYPE_COPY_URL,
    MODAL_TYPE_APP_DOWNLOAD
} from '../redux/modules/modal';
import { bumpMusic } from '../redux/modules/admin';

import ListItem from './ListItem';

import TwitterIcon from '../icons/twitter-logo';
import FacebookIcon from '../icons/facebook-letter-logo';
import LinkIcon from '../icons/link';

class ListItemContainer extends React.Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        onItemClick: PropTypes.func,
        onArtworkClick: PropTypes.func,
        history: PropTypes.object.isRequired,
        onDetailsClick: PropTypes.func,
        active: PropTypes.bool,
        listType: PropTypes.string,
        paused: PropTypes.bool,
        showNumbers: PropTypes.bool,
        displayViewSong: PropTypes.bool,
        index: PropTypes.number,
        activeContext: PropTypes.string,
        activeGenre: PropTypes.string,
        dispatch: PropTypes.func
    };

    static defaultProps = {
        displayViewSong: false,
        onOverlayActionClick() {},
        onArtworkClick() {},
        onDetailsClick() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            nativeShareSupported: false
        };
    }

    componentDidMount() {
        this.setNativeShareSupport();
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(hideDrawer());
    }

    handleActionClick = (e) => {
        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        const {
            currentUser,
            dispatch,
            item,
            history,
            activeGenre
        } = this.props;

        switch (action) {
            case 'favorite': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.favoriteItem(item, newlyLoggedInUser);

                if (!currentUser.isLoggedIn) {
                    dispatch(queueAction(queuedAction));
                    history.push('/login');
                    dispatch(hideDrawer());
                    return;
                }

                queuedAction();
                break;
            }

            case 'repost': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.reupItem(item, newlyLoggedInUser);

                if (!currentUser.isLoggedIn) {
                    dispatch(queueAction(queuedAction));
                    history.push('/login');
                    dispatch(hideDrawer());
                    return;
                }

                queuedAction();
                break;
            }

            case 'playlist': {
                const queuedAction = () => this.playlistItem(item);

                if (!currentUser.isLoggedIn) {
                    dispatch(queueAction(queuedAction));
                    history.push('/login');
                    dispatch(hideDrawer());
                    return;
                }

                queuedAction();
                dispatch(hideDrawer());
                break;
            }

            case 'share': {
                if (this.state.nativeShareSupported) {
                    nativeShare(item);
                } else {
                    dispatch(addItems(this.getShareItems(item)));
                    dispatch(showDrawer());
                }
                break;
            }

            case 'download':
                break;

            case 'bump':
                this.bumpItem(item, activeGenre);
                break;

            default:
                break;
        }
    };

    handleActionDrawerClick = () => {
        const { dispatch } = this.props;

        dispatch(addItems(this.getActionItems()));
        dispatch(showDrawer());
    };

    handleCopyUrlClick = (item) => {
        const { dispatch } = this.props;
        const url = getMusicUrl(item, {
            host: process.env.AM_URL
        });
        const successful = copyToClipboard(url);

        if (successful) {
            dispatch(showMessage('URL was copied to clipboard!'));
        } else {
            dispatch(
                showModal(MODAL_TYPE_COPY_URL, {
                    url: url
                })
            );
        }

        dispatch(hideDrawer());
    };

    handleDownloadClick = () => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_APP_DOWNLOAD, {
                item: this.props.item
            })
        );
    };

    getActionItems() {
        const { item, listType, currentUser, displayViewSong } = this.props;
        const isAlbum = listType === 'album';
        const isPlaylist = listType === 'playlist';
        let favLabel = 'Favorite';

        if (
            currentUser.isLoggedIn &&
            currentUser.profile.favorite_music.indexOf(item.id) !== -1
        ) {
            favLabel = 'Unfavorite';
        }

        let actions = [
            {
                text: favLabel,
                action: 'favorite',
                handler: this.handleActionClick
            },
            {
                text: 'Share',
                action: 'share',
                handler: this.handleActionClick
            }
        ];

        if (isAlbum) {
            actions = [
                {
                    text: favLabel,
                    action: 'favorite',
                    handler: this.handleActionClick
                },
                {
                    text: 'Add to Playlist',
                    action: 'playlist',
                    handler: this.handleActionClick
                }
            ];
        }

        if (item.type === 'song') {
            actions = [
                {
                    text: favLabel,
                    action: 'favorite',
                    handler: this.handleActionClick
                },
                {
                    text: 'Share',
                    action: 'share',
                    handler: this.handleActionClick
                },
                {
                    text: 'Add to Playlist',
                    action: 'playlist',
                    handler: this.handleActionClick
                }
            ];

            if (
                currentUser.isLoggedIn &&
                getUploader(item.parentDetails || item).id !==
                    currentUser.profile.id
            ) {
                let repostLabel = 'Re-Up';

                if (
                    currentUser.isLoggedIn &&
                    currentUser.profile.reups.indexOf(item.id) !== -1
                ) {
                    repostLabel = 'Undo Re-Up';
                }

                actions.push({
                    text: repostLabel,
                    action: 'repost',
                    handler: this.handleActionClick
                });
            }
        }

        const uploader = isPlaylist
            ? getUploader(item)
            : getUploader(item.parentDetails || item);

        if (displayViewSong && (isAlbum || isPlaylist)) {
            const type = item.type || 'song';

            actions.push({
                text: 'View song',
                href: `/${type}/${uploader.url_slug}/${item.url_slug}`
            });
        }

        if (
            this.props.activeContext === COLLECTION_TYPE_TRENDING &&
            currentUser.isLoggedIn
        ) {
            if (currentUser.isAdmin === true) {
                actions.unshift({
                    text: 'Bump',
                    action: 'bump',
                    handler: this.handleActionClick
                });
            }
        }

        return actions;
    }

    setNativeShareSupport() {
        if (typeof window.navigator.share !== 'undefined') {
            this.setState({
                nativeShareSupported: true
            });
        }
    }

    getShareItems(actionItem) {
        // https://facebook.com/sharer/sharer.php?u=http://dev.audiomack.com/song/glen-scott/vimeo-tester
        // https://twitter.com/share?url=http://dev.audiomack.com/song/glen-scott/vimeo-tester&text=Glen%20Scott%20-%20Vimeo%20Tester%20via%20@glenscott
        return [
            {
                text: [
                    <FacebookIcon className="drawer__list-item-icon" key="f" />,
                    'Share on Facebook'
                ],
                action: 'share-facebook',
                href: getFacebookShareLink(
                    process.env.AM_URL,
                    actionItem,
                    process.env.FACEBOOK_APP_ID
                )
            },
            {
                text: [
                    <TwitterIcon className="drawer__list-item-icon" key="t" />,
                    'Share on Twitter'
                ],
                action: 'share-twitter',
                href: getTwitterShareLink(process.env.AM_URL, actionItem)
            },
            {
                text: [
                    <LinkIcon className="drawer__list-item-icon" key="c" />,
                    'Copy URL'
                ],
                action: 'copy-url',
                handler: () => this.handleCopyUrlClick(actionItem)
            }
        ];
    }

    shouldActivateAction(action, item, currentUser) {
        if (!currentUser.isLoggedIn) {
            return false;
        }

        if (action === 'favorite') {
            if (item.type === 'playlist') {
                return (
                    (currentUser.profile.favorite_playlists || []).indexOf(
                        item.id
                    ) !== -1
                );
            }

            return (
                (currentUser.profile.favorite_music || []).indexOf(item.id) !==
                -1
            );
        }

        if (action === 'repost') {
            return (currentUser.profile.reups || []).indexOf(item.id) !== -1;
        }

        return false;
    }

    favoriteItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const addMessage = `'${item.title}' was added to your favorites`;
        const removeMessage = `'${item.title}' was removed from your favorites`;
        const user = newlyLoggedInUser || currentUser.profile;
        let message;

        if (item.type === 'playlist') {
            // Make sure we don't let a newly logged in user to undo an item
            if (
                newlyLoggedInUser ||
                user.favorite_playlists.indexOf(item.id) === -1
            ) {
                dispatch(favoritePlaylist(item.id));
                message = addMessage;
                dispatch(hideDrawer());
            } else {
                message = removeMessage;
                dispatch(unfavoritePlaylist(item.id));
                dispatch(hideDrawer());
            }
        }
        // Make sure we don't let a newly logged in user to undo an item
        else if (
            newlyLoggedInUser ||
            user.favorite_music.indexOf(item.id) === -1
        ) {
            message = addMessage;
            dispatch(favorite(item.id));
            dispatch(hideDrawer());
        } else {
            message = removeMessage;
            dispatch(unfavorite(item.id));
            dispatch(hideDrawer());
        }

        dispatch(showMessage(message));
    }

    reupItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        let message;

        // Make sure we're comparing the same types
        const itemId = item.id;

        // Make sure we don't let a newly logged in user to undo an item
        if (newlyLoggedInUser || user.reups.indexOf(itemId) === -1) {
            message = `'${
                item.title
            }' was Re-upped and shared with your followers`;
            dispatch(repost(itemId));
            dispatch(hideDrawer());
        } else {
            dispatch(unrepost(itemId));
            dispatch(hideDrawer());
        }

        if (message) {
            dispatch(showMessage(message));
            dispatch(hideDrawer());
        }
    }

    playlistItem(item) {
        const { dispatch } = this.props;

        dispatch(addSong(item));
        dispatch(getPlaylistsWithSongId(item.id));
        dispatch(showModal(MODAL_TYPE_ADD_TO_PLAYLIST));
    }

    bumpItem(item, genre) {
        const { dispatch } = this.props;

        // When the API endpoint is installed this code should dispatch to the API
        //  To attempt to bump the track to the top of the trending chart it's on
        // And upon successful return from the API, physically move it to the top
        // If the top 20 songs are on the page, otherwise remove it from the page?
        const result = dispatch(bumpMusic(item, genre));

        dispatch(hideDrawer());

        return result;
    }

    render() {
        return (
            <ListItem
                active={this.props.active}
                paused={this.props.paused}
                item={this.props.item}
                currentUser={this.props.currentUser}
                index={this.props.index}
                listType={this.props.listType}
                onArtworkClick={this.props.onArtworkClick}
                onDetailsClick={this.props.onDetailsClick}
                onItemClick={this.props.onItemClick}
                onActionDrawerClick={this.handleActionDrawerClick}
                showNumbers={this.props.showNumbers}
                activeContext={this.props.activeContext}
                activeGenre={this.props.activeGenre}
                onDownloadClick={this.handleDownloadClick}
            />
        );
    }
}

function mapStateToProps() {
    return {};
}

export default withRouter(connect(mapStateToProps)(ListItemContainer));
