import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AndroidLoader from '../loaders/AndroidLoader';

import { buildDynamicImage } from 'utils/index';
export default class HorizontalList extends Component {
    static propTypes = {
        title: PropTypes.string,
        done: PropTypes.bool,
        onScroll: PropTypes.func,
        page: PropTypes.number,
        loading: PropTypes.bool,
        data: PropTypes.array
    };

    static defaultProps = {
        onScroll() {}
    };

    render() {
        const items = this.props.data.map((item, i) => {
            const imageSize = 135;
            const artwork = buildDynamicImage(item.image_base || item.image, {
                width: imageSize,
                height: imageSize,
                max: true
            });

            const retinaArtwork = buildDynamicImage(
                item.image_base || item.image,
                {
                    width: imageSize * 2,
                    height: imageSize * 2,
                    max: true
                }
            );

            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            return (
                <li key={i} className="list-item">
                    <Link
                        to={`/playlist/${item.artist.url_slug}/${
                            item.url_slug
                        }`}
                        className="list-item__link"
                    >
                        <img
                            src={artwork}
                            srcSet={srcSet}
                            alt=""
                            loading="lazy"
                        />
                        <p className="list-item__title">{item.title}</p>
                    </Link>
                </li>
            );
        });

        if (this.props.loading) {
            items.push(
                <li key="loader" className="list-item list__loader">
                    <AndroidLoader size={34} />
                </li>
            );
        }

        return (
            <div className="list list--horizontal">
                <p className="list__title">{this.props.title}</p>
                <ul
                    onScroll={this.props.onScroll}
                    data-key={this.props.title}
                    data-page={this.props.page}
                    data-done={this.props.done}
                    data-loading={this.props.loading}
                >
                    {items}
                </ul>
            </div>
        );
    }
}
