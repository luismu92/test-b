import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { isCurrentMusicItem } from 'utils/index';
import {
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_PLAYLIST,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    PLAYLIST_TYPE_BROWSE,
    PLAYLIST_TYPE_MINE,
    PLAYLIST_TYPE_FAVORITES
} from 'constants/index';

// import InContentAd from '../ad/InContentAd';
import AndroidLoader from '../loaders/AndroidLoader';
import ListItemContainer from './ListItemContainer';

import InboxOutIcon from '../icons/inbox-out';

// const AD_SPOT = 10; // where to place an ad in the list

export default class List extends Component {
    static propTypes = {
        artist: PropTypes.object,
        currentUser: PropTypes.object,
        songs: PropTypes.object.isRequired,
        player: PropTypes.object.isRequired,
        shareOverlayProps: PropTypes.object,
        listType: PropTypes.string.isRequired,
        onItemClick: PropTypes.func,
        onNextPage: PropTypes.func,
        onArtworkClick: PropTypes.func,
        onDetailsClick: PropTypes.func,
        dispatch: PropTypes.func,
        loading: PropTypes.bool,
        emptyState: PropTypes.element,
        bordered: PropTypes.bool,
        showEmptyMessage: PropTypes.bool,
        displayViewSong: PropTypes.bool,
        location: PropTypes.object,
        activeContext: PropTypes.string,
        activeGenre: PropTypes.string,
        className: PropTypes.string
    };

    static defaultProps = {
        showEmptyMessage: true,
        activeContext: '',
        bordered: false,
        listType: 'song',
        songs: {
            data: []
        },
        showAd: false
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const {
            songs,
            player,
            showEmptyMessage,
            listType,
            emptyState,
            currentUser,
            className
        } = this.props;
        const currentSong = player.currentSong;

        let loader;

        if (songs.loading) {
            loader = <AndroidLoader size={34} className="list__loader" />;
        }

        if (!songs.data.length && !songs.loading && emptyState) {
            return emptyState;
        }

        if (!songs.data.length && !songs.loading && showEmptyMessage) {
            let emptyText;

            switch (songs.activeContext) {
                case COLLECTION_TYPE_TRENDING:
                    emptyText = 'No trending songs found.';
                    break;

                case COLLECTION_TYPE_SONG:
                    emptyText = 'No songs found';
                    break;

                case COLLECTION_TYPE_PLAYLIST:
                    switch (songs.activePlaylistContext) {
                        case PLAYLIST_TYPE_BROWSE:
                            emptyText =
                                'Something went wrong loading Featured Playlists, please try again';
                            break;

                        case PLAYLIST_TYPE_MINE:
                            emptyText = "You haven't created any playlists yet";
                            break;

                        case PLAYLIST_TYPE_FAVORITES:
                            emptyText = 'You have no favorites';
                            break;

                        default:
                            console.warn(
                                `${
                                    songs.activePlaylistContext
                                } is unaccounted for with its empty message`
                            );
                            break;
                    }
                    break;

                case COLLECTION_TYPE_ALBUM:
                    emptyText = 'No albums found.';
                    break;

                case COLLECTION_TYPE_RECENTLY_ADDED:
                    emptyText = 'No recently added items found';
                    break;

                default:
                    emptyText = 'Nothing found here';
                    console.warn(
                        `${
                            songs.activeContext
                        } is unaccounted for with its empty message`
                    );
                    break;
            }

            return <p className="outer-padding no-tracks-found">{emptyText}</p>;
        }

        const list = songs.data.map((song, index) => {
            const active = isCurrentMusicItem(currentSong, song);

            return (
                <ListItemContainer
                    active={active}
                    paused={player.paused}
                    key={index}
                    item={song}
                    currentUser={currentUser}
                    index={index}
                    listType={listType}
                    onArtworkClick={this.props.onArtworkClick}
                    onDetailsClick={this.props.onDetailsClick}
                    onItemClick={this.props.onItemClick}
                    showNumbers={this.props.songs.showNumbers}
                    shareOverlayProps={this.props.shareOverlayProps}
                    displayViewSong={this.props.displayViewSong}
                    activeContext={songs.activeContext}
                    activeGenre={songs.activeGenre}
                />
            );
        });

        /*
        let incontentIndex = AD_SPOT - 1;

        if (listType !== 'album') {
            while (typeof list[incontentIndex] !== 'undefined') {
                const incontentAd = (
                    <div
                        key={`incontentAd-${incontentIndex}`}
                        className="music-detail list-ad u-clearfix"
                        style={{
                            padding: 0,
                            margin: '0 auto',
                            width: 300,
                            height: 250
                        }}
                    >
                        <InContentAd
                            currentUser={currentUser}
                            location={this.props.location}
                            adCount={incontentIndex}
                        />
                    </div>
                );

                list.splice(incontentIndex, 0, incontentAd);

                incontentIndex += AD_SPOT;
            }
        }
        */

        let uploadButton;

        if (listType === 'artist-uploads') {
            uploadButton = (
                <div className="button-group list__button-group text-center">
                    <a
                        href="/upload?fromMobile=1"
                        className="button button--pill button--in-feed"
                        target="_blank"
                        rel="noopener nofollow"
                    >
                        <span className="button__icon">
                            <InboxOutIcon />
                        </span>
                        Upload Your Music
                    </a>
                </div>
            );
        }

        const klass = classnames(`list list--${listType}`, {
            [className]: className,
            'list--bordered': this.props.bordered
        });

        return (
            <Fragment>
                <div
                    className={klass}
                    role="list"
                    data-testid="browseContainer"
                >
                    {uploadButton}
                    {list}
                    {loader}
                </div>
            </Fragment>
        );
    }
}
