import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ListContainer from './ListContainer';

export default class AlbumList extends Component {
    static propTypes = {
        player: PropTypes.object.isRequired,
        songs: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        album: PropTypes.object.isRequired,
        onItemClick: PropTypes.func,
        location: PropTypes.object,
        albumKey: PropTypes.string
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const shareOverlayProps = {
            hideStats: true,
            actions: ['favorite', 'playlist', 'close']
        };
        const { album, location, songs, albumKey } = this.props;

        // @TODO we should move this check to the server side (e.g. the API could return a future_release timestamp)
        // otherwise it can be easily fooled by altering clock on client
        if (
            album &&
            album.released > Math.floor(Date.now() / 1000) &&
            !albumKey
        ) {
            return (
                <div className="alert-wrap outer-padding text-center u-spacing-bottom-20">
                    <span className="alert alert--not-released alert--dark">
                        This album has not yet been released.
                    </span>
                </div>
            );
        }

        return (
            <ListContainer
                songs={songs}
                location={location}
                listType="album"
                onItemClick={this.props.onItemClick}
                shareOverlayProps={shareOverlayProps}
                className="list--tracklist"
                displayViewSong
            />
        );
    }
}
