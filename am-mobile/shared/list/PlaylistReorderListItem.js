import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { clamp, Support } from 'utils/index';

import AndroidLoader from '../loaders/AndroidLoader';

// A few of the things in here go against react best practices such as
// imperatively touching the DOM and altering attributes on items (even outside)
// of this component. If we ever need another drag/drop reordering list component,
// we can re-evaluate how to best handle this. React DND seemed a bit much
// for this one use case
export default class PlaylistReorderListItem extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        onListItemSwitch: PropTypes.func,
        onPlaylistItemDelete: PropTypes.func
    };

    static defaultProps = {
        onListItemSwitch() {},
        onPlaylistItemDelete() {}
    };

    constructor(props) {
        super(props);

        // Doesnt really make sense to have this in the global store
        // so this is breaking the container/component rules a bit. Sue me.
        this.state = {
            deleteActive: false,
            translateY: 0,
            isDragging: false
        };
    }

    componentWillUnmount() {
        this.cleanDragEvents();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleDeleteOpenClick = () => {
        this.setState({
            deleteActive: true
        });
    };

    handleDeleteClose = () => {
        if (!this.state.deleteActive) {
            return;
        }

        this.setState({
            deleteActive: false
        });
    };

    handleDelete = () => {
        this.setState({
            deleteActive: false
        });
    };

    handleDragStart = (e) => {
        this._startingY = e.pageY;

        if (Support.touch && e.touches) {
            this._startingY = e.touches[0].pageY;
        }

        this.calculateDragInfoForTarget(e.currentTarget);

        this.addDragEvents();
    };

    handleDrag = (e) => {
        e.preventDefault();
        let y = e.pageY;

        if (Support.touch) {
            y = e.touches[0].pageY;
        }

        const delta = y - this._startingY;

        const { translates, fromIndex, toIndex } = this.calculateTranslates(
            this._currentListItemIndex,
            delta,
            this._breakpoints,
            this._itemHeights
        );

        this._fromIndex = fromIndex;
        this._toIndex = toIndex;

        this._allListItems.forEach((item, i) => {
            const translate = translates[i];

            // Ignore the item we are dragging
            if (i !== this._currentListItemIndex) {
                if (translate) {
                    item.style.transform = `translateY(${translate}px) translateZ(0)`;
                    return;
                }

                item.style.transform = null;
            }
        });

        this.setState({
            translateY: clamp(delta, this._minTranslate, this._maxTranslate),
            isDragging: true
        });
    };

    handleDragEnd = () => {
        this.setState({
            translateY: 0,
            isDragging: false
        });

        if (!isNaN(this._fromIndex) && !isNaN(this._toIndex)) {
            this.props.onListItemSwitch(this._fromIndex, this._toIndex);
        }

        this.cleanDragEvents();
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getArtistName(song) {
        return song.artist.name || song.artist;
    }

    findBreakpointIndexForDragDelta(delta) {
        let index = 0;

        for (let i = 0; i < this._breakpoints.length; i++) {
            const breakpoint = this._breakpoints[i];

            if (delta > breakpoint) {
                index = i;
                continue;
            }

            break;
        }

        return index;
    }

    calculateTranslates(currentIndex, delta, breakpoints = [], heights = []) {
        // Make equal length array of nulls
        const translates = breakpoints.map(() => null);
        const switchThreshold = 0.5;
        const fromIndex = currentIndex;
        let toIndex = currentIndex;

        // Look forward
        if (delta > 0) {
            for (let i = currentIndex; i < breakpoints.length; i++) {
                const breakpoint = breakpoints[i];
                const height = heights[i];
                const threshold = switchThreshold * height;

                if (delta < breakpoint - threshold) {
                    break;
                }

                toIndex = i;
                translates[i] = height * -1;
            }
        }

        // Look backward
        if (delta < 0) {
            for (let i = currentIndex; i >= 0; i--) {
                const breakpoint = breakpoints[i];
                const height = heights[i];
                const threshold = switchThreshold * height;

                if (delta > breakpoint + threshold) {
                    break;
                }

                toIndex = i;
                translates[i] = height;
            }
        }

        return {
            translates,
            fromIndex,
            toIndex
        };
    }

    calculateDragInfoForTarget(target) {
        this._allListItems = Array.from(
            document.querySelectorAll('.list-item')
        );
        this._currentListItem = this._allListItems[0];
        this._currentListItemIndex = 0;
        this._otherListItems = this._allListItems.filter((item, i) => {
            if (item.contains(target)) {
                this._currentListItem = item;
                this._currentListItemIndex = i;
                return false;
            }

            return true;
        });

        const rects = this._allListItems.map((item) => {
            return item.getBoundingClientRect();
        });

        this._offsets = rects.map((rect) => {
            return rect.top;
        });

        this._itemHeights = rects.map((rect) => {
            return rect.height;
        });

        this._breakpoints = rects.map((rect) => {
            return rect.top - this._offsets[this._currentListItemIndex];
        });

        const firstOffset = this._offsets[0];
        const lastOffset = this._offsets[this._offsets.length - 1];

        this._minTranslate =
            firstOffset - this._offsets[this._currentListItemIndex];
        this._maxTranslate =
            lastOffset - this._offsets[this._currentListItemIndex];
    }

    addDragEvents() {
        const move = Support.touch ? 'touchmove' : 'mousemove';
        const up = Support.touch ? 'touchend' : 'mouseup';

        window.addEventListener(move, this.handleDrag, false);
        window.addEventListener(up, this.handleDragEnd, false);
    }

    cleanDragEvents() {
        const move = Support.touch ? 'touchmove' : 'mousemove';
        const up = Support.touch ? 'touchend' : 'mouseup';

        window.removeEventListener(move, this.handleDrag, false);
        window.removeEventListener(up, this.handleDragEnd, false);

        if (this._allListItems) {
            // Remove altered translates that we imperatively set
            this._allListItems.forEach((item) => {
                item.style.transform = null;
            });
        }

        this._startingY = null;
        this._otherListItems = null;
        this._allListItems = null;
        this._currentListItem = null;
        this._currentListItemIndex = null;
        this._otherListItems = null;
        this._offsets = null;
        this._minTranslate = null;
        this._maxTranslate = null;
        this._itemHeights = null;
        this._breakpoints = null;
        this._fromIndex = null;
        this._toIndex = null;
    }

    renderButtonText(deleting) {
        if (deleting) {
            return (
                <AndroidLoader
                    size={34}
                    className="list__loader"
                    color="white"
                />
            );
        }

        return 'Delete';
    }

    render() {
        const item = this.props.item;

        const containerClass = classnames('list-item track-preview', {
            'list-item--open': this.state.deleteActive,
            'list-item--dragging': this.state.isDragging
        });

        const style = {};

        if (this.state.translateY) {
            style.transform = `translateY(${
                this.state.translateY
            }px) translateZ(0)`;
        }

        return (
            /* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions */
            <div
                className={containerClass}
                role="listitem"
                onClick={this.handleDeleteClose}
                style={style}
            >
                <div className="list-item__inner">
                    <button
                        className="button button--danger button--delete"
                        onClick={this.handleDeleteOpenClick}
                    >
                        <i className="glyphicons minus" />
                    </button>
                    <div className="list-item__image left">
                        <img src={item.image} alt={item.title} />
                    </div>
                    <div className="list-item__details">
                        <h2 className="artist-name">
                            {this.getArtistName(item)}
                        </h2>
                        <h3 className="track-title">{item.title}</h3>
                    </div>
                    {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions */}
                    <div
                        className="list-item__drag-handle"
                        onMouseDown={this.handleDragStart}
                        onTouchStart={this.handleDragStart}
                    >
                        <i className="glyphicons show-lines" />
                    </div>
                </div>
                <button
                    className="button button--danger button--under-list-item"
                    onClick={this.props.onPlaylistItemDelete}
                    disabled={item.deleting}
                    data-music-id={item.id}
                >
                    {this.renderButtonText(item.deleting)}
                </button>
            </div>
        );
    }
}
