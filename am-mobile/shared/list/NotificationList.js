import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { getMusicUrl } from 'utils/index';
import { getTotalsFromPlaylists } from 'utils/notifications';
import { verbDisplayTextMap } from 'constants/notifications/index';

export default class NotificationList extends Component {
    static propTypes = {
        notifications: PropTypes.array
    };

    icon(verb) {
        let icon = <i className="notification-info__icon glyphicons star" />;

        if (verb === 'reup') {
            icon = <i className="notification-info__icon glyphicons retweet" />;
        }

        if (verb === 'playlisted') {
            icon = <i className="notification-info__icon glyphicons plus" />;
        }

        if (verb === 'playlist_updated' || verb === 'playlist_updated_bundle') {
            icon = (
                <i className="notification-info__icon glyphicons playlist" />
            );
        }

        if (verb === 'comment') {
            icon = <i className="notification-info__icon glyphicons plus" />;
        }

        return icon;
    }

    activityTime(createdAt) {
        return createdAt;
    }

    renderPlaylists(playlists) {
        if (!playlists.length) {
            return null;
        }

        return playlists.map((playlist) => {
            return (
                <p key={playlist.playlist_id}>
                    {playlist.artist_name} added {playlist.count} songs to{' '}
                    <Link
                        to={`playlist/${playlist.artist_url_slug}/${
                            playlist.playlist_url_slug
                        }`}
                    >
                        {playlist.playlist_name}
                    </Link>
                </p>
            );
        });
    }

    renderNotificationContent(notification) {
        const {
            actor,
            verb,
            object,
            created_at: created,
            target
        } = notification;
        const targetTitle = (target || {}).title || 'untitled';
        let objectLink = 'you';
        let stats;

        if (object) {
            const objectUrl =
                object.type !== 'artist'
                    ? getMusicUrl(object)
                    : `/artist/${object.url_slug}`;

            if (object.type) {
                objectLink = <Link to={objectUrl}>{object.title}</Link>;
            }
        }

        if (verb === 'playlist_updated') {
            return (
                <Fragment>
                    <div className="notification-image left">
                        <img src={actor.image} alt={actor.name} />
                    </div>
                    <div className="notification-info">
                        <p>
                            <Link to={`/artist/${actor.url_slug}`}>
                                <strong>{actor.name}</strong>
                            </Link>{' '}
                            added "{targetTitle}" to the{' '}
                            <span>"{objectLink}" playlist</span>
                        </p>

                        <p className="meta">
                            <span>{this.icon(verb)}</span>{' '}
                            {moment(this.activityTime(created)).fromNow()}
                        </p>
                    </div>
                </Fragment>
            );
        }

        if (verb === 'playlist_updated_bundle') {
            stats = getTotalsFromPlaylists(
                notification.extra_context.playlists
            );
            return (
                <Fragment>
                    <div className="notification-image left">
                        <img src={actor.image} alt={actor.name} />
                    </div>
                    <div className="notification-info">
                        <p>
                            {stats.totalSongs} songs were added to{' '}
                            {stats.totalPlaylists} playlists you follow:{' '}
                        </p>
                        {this.renderPlaylists(
                            notification.extra_context.playlists
                        )}
                        <p className="meta">
                            <span>{this.icon(verb)}</span>{' '}
                            {moment(this.activityTime(created)).fromNow()}
                        </p>
                    </div>
                </Fragment>
            );
        }

        let verbText = verbDisplayTextMap[verb];
        if (typeof notification.extra_context.comment !== 'undefined') {
            const truncate = (input, length) =>
                input.length > length
                    ? `${input.substring(0, length)}...`
                    : input;
            const parentComment =
                notification.extra_context.parent !== null
                    ? notification.extra_context.parent.replace(/<br>/gi, ' ')
                    : null;

            if (parentComment !== null) {
                verbText = (
                    <span>
                        replied to your comment (
                        <span title={parentComment}>
                            {truncate(parentComment, 10)}
                        </span>
                        ) on
                    </span>
                );
            }
        }

        return (
            <Fragment>
                <div className="notification-image left">
                    <img src={actor.image} alt={actor.name} />
                </div>
                <div className="notification-info">
                    <p>
                        <Link to={`/artist/${actor.url_slug}`}>
                            <strong>{actor.name}</strong>
                        </Link>{' '}
                        {verbText} <span>{objectLink}</span>
                    </p>

                    <p className="meta">
                        <span>{this.icon(verb)}</span>{' '}
                        {moment(this.activityTime(created)).fromNow()}
                    </p>
                </div>
            </Fragment>
        );
    }

    renderNotification(notification, i) {
        const { actor } = notification;

        if (typeof actor !== 'object') {
            return null;
        }

        return (
            <li className="single-notification group" key={i}>
                {this.renderNotificationContent(notification)}
            </li>
        );
    }

    render() {
        return (
            <ul className="notification-feed">
                {' '}
                {this.props.notifications.map(
                    this.renderNotification,
                    this
                )}{' '}
            </ul>
        );
    }
}
