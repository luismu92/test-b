import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import AndroidLoader from '../loaders/AndroidLoader';
import PlaylistReorderListItem from './PlaylistReorderListItem';

import CloseIcon from '../icons/close';

export default class PlaylistReorderList extends Component {
    static propTypes = {
        songs: PropTypes.object.isRequired,
        listType: PropTypes.string.isRequired,
        onListItemSwitch: PropTypes.func,
        onPlaylistItemDelete: PropTypes.func,
        onCloseButton: PropTypes.func,
        loading: PropTypes.bool,
        emptyState: PropTypes.element,
        bordered: PropTypes.bool,
        showEmptyMessage: PropTypes.bool
    };

    static defaultProps = {
        showEmptyMessage: true,
        bordered: true,
        listType: 'playlist-reorder',
        songs: {
            data: []
        }
    };

    render() {
        const {
            songs: tracks,
            showEmptyMessage,
            listType,
            emptyState
        } = this.props;

        let loader;

        if (tracks.loading) {
            loader = <AndroidLoader size={34} className="list__loader" />;
        }

        if (!tracks.data.length && !tracks.loading && emptyState) {
            return emptyState;
        }

        if (!tracks.data.length && !tracks.loading && showEmptyMessage) {
            return (
                <p className="outer-padding no-tracks-found">
                    You haven't created any playlists yet
                </p>
            );
        }

        const list = tracks.data.map((track) => {
            return (
                <PlaylistReorderListItem
                    item={track}
                    key={track.id}
                    onListItemSwitch={this.props.onListItemSwitch}
                    onPlaylistItemDelete={this.props.onPlaylistItemDelete}
                />
            );
        });

        const klass = classnames(`list list--${listType}`, {
            'list--bordered': this.props.bordered
        });

        return (
            <div>
                <div className="clearfix">
                    <button
                        className="playlist-reorder__close"
                        onClick={this.props.onCloseButton}
                    >
                        <CloseIcon className="u-text-icon" />
                    </button>
                </div>
                <div className={klass} role="list">
                    {list}
                    {loader}
                </div>
            </div>
        );
    }
}
