import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import AndroidLoader from '../loaders/AndroidLoader';

import UserListItem from './UserListItem';

const SCROLL_THRESHOLD = 650;

export default class UserList extends Component {
    static propTypes = {
        items: PropTypes.array.isRequired,
        loading: PropTypes.bool,
        emptyState: PropTypes.element,
        bordered: PropTypes.bool,
        isLastPage: PropTypes.bool,
        onNextPage: PropTypes.func,
        showEmptyMessage: PropTypes.bool
    };

    static defaultProps = {
        showEmptyMessage: true,
        bordered: true,
        items: []
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        window.addEventListener('scroll', this.handleWindowScroll, false);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleWindowScroll, false);
        this._scrollTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowScroll = () => {
        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (this.props.loading || this.props.isLastPage) {
                return;
            }

            if (
                document.body.clientHeight -
                    (window.innerHeight + window.pageYOffset) <
                    SCROLL_THRESHOLD &&
                this.props.onNextPage
            ) {
                this.props.onNextPage();
            }
        }, 200);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { items, loading, showEmptyMessage, emptyState } = this.props;

        if (!items.length && !loading) {
            if (emptyState) {
                return emptyState;
            }

            if (showEmptyMessage) {
                return (
                    <p className="outer-padding no-items-found">
                        Nothing found here. Try again later.
                    </p>
                );
            }
        }

        let loader;

        if (loading) {
            loader = <AndroidLoader size={34} className="list__loader" />;
        }

        const list = items.map((item, index) => {
            return <UserListItem key={index} item={item} index={index} />;
        });

        const klass = classnames('list list--user', {
            'list--bordered': this.props.bordered
        });

        return (
            <div className={klass}>
                {list}
                {loader}
            </div>
        );
    }
}
