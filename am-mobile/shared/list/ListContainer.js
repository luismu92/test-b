import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { isCurrentMusicItem, getQueueIndexForSong } from 'utils/index';

import { play, pause, editQueue } from '../redux/modules/player';

import List from './List';

// @todo simplify this component, remove player dependency and anything that doesnt
// make up a "List"

const SCROLL_THRESHOLD = 650;

class ListContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        songs: PropTypes.object.isRequired,
        player: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        shareOverlayProps: PropTypes.object,
        listType: PropTypes.string.isRequired,
        onItemClick: PropTypes.func,
        onNextPage: PropTypes.func,
        dispatch: PropTypes.func,
        loading: PropTypes.bool,
        emptyState: PropTypes.element,
        showEmptyMessage: PropTypes.bool,
        displayViewSong: PropTypes.bool,
        className: PropTypes.string
    };

    static defaultProps = {
        showEmptyMessage: true,
        listType: 'song',
        songs: {
            data: []
        }
    };

    componentDidMount() {
        if (this.props.onNextPage) {
            window.addEventListener('scroll', this.handleWindowScroll, false);
        }
    }

    componentDidUpdate(prevProps) {
        const { dispatch, songs: currentSongs } = this.props;
        const currentContext = currentSongs.activeContext;
        const currentTimePeriod = currentSongs.activeTimePeriod;

        // If page change on same list, update queue
        if (
            currentContext &&
            currentContext === this._activeContext &&
            currentTimePeriod &&
            currentTimePeriod === this._activeTimePeriod &&
            prevProps.songs.data.length !== currentSongs.data.length
        ) {
            dispatch(editQueue(this.props.songs.data));
        }
    }

    componentWillUnmount() {
        if (this.props.onNextPage) {
            window.removeEventListener(
                'scroll',
                this.handleWindowScroll,
                false
            );
        }
        this._scrollTimer = null;
        this._activeContext = null;
        this._activeTimePeriod = null;
    }

    handleArtworkClick = (e) => {
        this.itemClick(e);
    };

    handleDetailsClick = (e) => {
        const isDetailClick = true;

        this.itemClick(e, isDetailClick);
    };

    handleWindowScroll = () => {
        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (this.props.songs.loading || this.props.songs.onLastPage) {
                return;
            }

            if (
                document.body.clientHeight -
                    (window.innerHeight + window.pageYOffset) <
                    SCROLL_THRESHOLD &&
                this.props.onNextPage
            ) {
                this.props.onNextPage();
            }
        }, 200);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    itemClick(event, isDetails = false) {
        const { songs, dispatch, player } = this.props;
        const index = parseInt(
            event.currentTarget.getAttribute('data-song-index'),
            10
        );
        const songList = songs.data;
        const currentSong = player.currentSong;
        const song = songList[index];
        const isAlreadyCurrentSong = isCurrentMusicItem(currentSong, song);

        if (isDetails && currentSong) {
            return;
        }

        this._activeContext = songs.activeContext;
        this._activeTimePeriod = songs.activeTimePeriod;

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        let queueIndex = getQueueIndexForSong(song, player.queue, {
            currentQueueIndex: player.queueIndex
        });

        if (queueIndex !== -1) {
            dispatch(play(queueIndex));
            return;
        }

        const { queue: newQueue } = dispatch(editQueue(songList));

        queueIndex = getQueueIndexForSong(song, newQueue);

        dispatch(play(queueIndex));
    }

    render() {
        const {
            songs,
            player,
            showEmptyMessage,
            listType,
            emptyState,
            currentUser,
            location,
            shareOverlayProps
        } = this.props;

        return (
            <List
                songs={songs}
                player={player}
                location={location}
                showEmptyMessage={showEmptyMessage}
                shareOverlayProps={shareOverlayProps}
                listType={listType}
                emptyState={emptyState}
                currentUser={currentUser}
                onArtworkClick={this.handleArtworkClick}
                onDetailsClick={this.handleDetailsClick}
                onItemClick={this.props.onItemClick}
                className={this.props.className}
                displayViewSong={this.props.displayViewSong}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player
    };
}

export default connect(mapStateToProps)(ListContainer);
