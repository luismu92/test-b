import React from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { COLLECTION_TYPE_TRENDING } from 'constants/index';
import { renderFeaturingLinks, buildDynamicImage } from 'utils/index';

import MusicStats from '../components/MusicStats';

import VolumeIcon from '../icons/volume-high';
import RetweetIcon from '../icons/retweet';
import DownloadIcon from 'icons/download';
import VerticalDots from '../icons/vertical-dots';

import styles from './ListItem.module.scss';

export default class ListItem extends React.Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        onItemClick: PropTypes.func,
        onArtworkClick: PropTypes.func,
        onDetailsClick: PropTypes.func,
        onActionDrawerClick: PropTypes.func,
        active: PropTypes.bool,
        listType: PropTypes.string,
        showNumbers: PropTypes.bool,
        index: PropTypes.number,
        activeContext: PropTypes.string,
        onDownloadClick: PropTypes.func
    };

    static defaultProps = {
        onOverlayActionClick() {},
        onArtworkClick() {},
        onDetailsClick() {}
    };

    getArtistName(song) {
        return song.artist.name || song.artist;
    }

    renderFeaturing(song) {
        const { listType } = this.props;

        if (!song.featuring) {
            return null;
        }

        if (listType === 'album') {
            return (
                <p className="list-item__feat u-d-inline-block">
                    <span>{renderFeaturingLinks(song.featuring)}</span>
                </p>
            );
        }

        return (
            <p className="feat">
                <span>Feat. </span>
                <span className="button-link">{song.featuring}</span>
            </p>
        );
    }

    renderCurrentlyPlaying(active) {
        if (!active) {
            return null;
        }

        return (
            <span className="track-preview__currently-playing">
                <VolumeIcon />
            </span>
        );
    }

    renderNumbers(index, showNumbers) {
        if (isNaN(index)) {
            return null;
        }

        if (!showNumbers) {
            return null;
        }

        return <span className="feed-number">{index + 1}</span>;
    }

    // @todo break these out into different listItem components
    renderItem(listType) {
        const {
            item,
            onArtworkClick,
            onDetailsClick,
            onActionDrawerClick,
            onItemClick,
            index,
            active,
            currentUser,
            activeContext
        } = this.props;

        let actions = ['favorite', 'repost', 'share', 'close'];

        if (item.type === 'song') {
            actions = ['favorite', 'repost', 'playlist', 'share', 'close'];
        }

        if (
            activeContext === COLLECTION_TYPE_TRENDING &&
            currentUser.isLoggedIn
        ) {
            if (currentUser.isAdmin === true) {
                actions.unshift('bump');
            }
        }

        const klass = classnames('list-item', {
            'list-item--playing': active
        });

        let icon = index + 1;

        if (active) {
            icon = <VolumeIcon className="u-text-icon" />;
        }

        let feat;

        if (item.featuring) {
            feat = this.renderFeaturing(item);
        }

        if (listType === 'album') {
            return (
                <div className={klass} role="listitem" data-testid="listItem">
                    <div className="list-item__icon">{icon}</div>
                    <div className="list-item__inner clearfix">
                        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
                        <div
                            className="list-item__details"
                            data-index={index}
                            onClick={onItemClick}
                            role="button"
                            tabIndex="0"
                        >
                            <h2 className="list-item__details__title u-fs-12 u-ls-n-05 u-lh-13 u-fw-700">
                                {item.title} {feat}
                            </h2>
                            <p className="u-fs-12 u-lh-13 u-ls-n-05 u-text-gray7">
                                {item.artist}
                            </p>
                        </div>
                        <button
                            className="vertical-dots track-preview__vertical-dots"
                            onClick={onActionDrawerClick}
                            aria-label="Open action drawer"
                        >
                            <VerticalDots />
                        </button>
                    </div>
                </div>
            );
        }

        if (listType === 'playlist') {
            let slug;

            // Sometimes this is artist? Sometimes its uploader? Glen would know
            if (typeof item.uploader === 'object') {
                slug = item.uploader.url_slug;
            } else if (typeof item.artist === 'object') {
                slug = item.artist.url_slug;
            }

            const url = `/playlist/${slug}/${item.url_slug}`;

            actions = ['favorite', 'share', 'close'];

            if (currentUser && currentUser.url_slug === slug) {
                actions = ['share', 'close'];
            }

            const imageSize = 70;
            const artwork = buildDynamicImage(item.image_base || item.image, {
                width: imageSize,
                height: imageSize,
                max: true
            });

            const retinaArtwork = buildDynamicImage(
                item.image_base || item.image,
                {
                    width: imageSize * 2,
                    height: imageSize * 2,
                    max: true
                }
            );
            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            return (
                <div className={klass} role="listitem" data-testid="listItem">
                    <div className="list-item__icon">{icon}</div>
                    <div className="list-item__inner clearfix">
                        <div className="list-item__image">
                            <Link to={url}>
                                <img
                                    src={artwork}
                                    srcSet={srcSet}
                                    alt={item.title}
                                    loading="lazy"
                                />
                            </Link>
                        </div>
                        <div className="list-item__details list-item__details--padded">
                            <div className="details-inner">
                                <h2 className="list-item__details__title u-fs-12 u-ls-n-05 u-lh-13 u-fw-700">
                                    <Link to={url}>
                                        {item.title} {feat}
                                    </Link>
                                </h2>
                                <p>
                                    {item.track_count}{' '}
                                    {item.track_count === 1 ? 'song' : 'songs'}
                                </p>
                            </div>
                            <button
                                className="vertical-dots track-preview__vertical-dots"
                                onClick={onActionDrawerClick}
                                aria-label="Open action drawer"
                            >
                                <VerticalDots />
                            </button>
                        </div>
                    </div>
                </div>
            );
        }

        const {
            stats,
            uploader,
            url_slug: urlSlug,
            type,
            geo_restricted: geoRestricted
        } = item;
        // const url = `/song/${data.album.title}-${data.song.title}`.replace(/\s/, '');

        // somestimes this seems to be null for some reason
        if (typeof uploader === 'undefined') {
            return null;
        }

        let sponsoredText;

        if (item.isFeatured) {
            sponsoredText = (
                <strong className="u-text-orange">Sponsored Track</strong>
            );
        }

        if (item.repost) {
            sponsoredText = (
                <p className="track-preview__reup-message u-tt-uppercase u-fw-700">
                    <span className="track-preview__reup-icon u-text-orange">
                        <RetweetIcon />
                    </span>
                    {item.repost}
                </p>
            );
        }

        const incompleteAlbum =
            item.type === 'album' && !item.tracks.length > 0;

        const artworkClass = classnames(
            'track-preview__artwork left u-pos-relative',
            {
                'u-album-stack': type === 'album'
            }
        );

        const imageSize = 70;
        const artwork = buildDynamicImage(item.image_base || item.image, {
            width: imageSize,
            height: imageSize,
            max: true
        });

        const retinaArtwork = buildDynamicImage(item.image_base || item.image, {
            width: imageSize * 2,
            height: imageSize * 2,
            max: true
        });
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        let itemArtwork = (
            <button onClick={onArtworkClick} data-song-index={index}>
                <img
                    src={artwork}
                    srcSet={srcSet}
                    alt={item.title}
                    loading="lazy"
                />
                {this.renderCurrentlyPlaying(active)}
            </button>
        );

        if (incompleteAlbum || geoRestricted) {
            itemArtwork = (
                <Link
                    to={`/${type}/${uploader.url_slug}/${urlSlug}`}
                    data-song-index={index}
                >
                    <img
                        src={artwork}
                        srcSet={srcSet}
                        alt={item.title}
                        loading="lazy"
                    />
                    {this.renderCurrentlyPlaying(active)}
                </Link>
            );
        }

        const trackPreviewClass = classnames('track-preview', {
            'sponsored-list-item': item.isFeatured,
            'track-preview--repost': item.repost
        });

        return (
            <div
                className={trackPreviewClass}
                role="listitem"
                data-testid="listItem"
            >
                <div className="track-preview__inner">
                    <div className={artworkClass}>{itemArtwork}</div>
                    <div className="details">
                        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
                        <div
                            className="details-inner"
                            onClick={onDetailsClick}
                            data-song-index={index}
                            role="button"
                            tabIndex="0"
                        >
                            {sponsoredText}
                            <Link
                                to={`/${type}/${uploader.url_slug}/${urlSlug}`}
                            >
                                <h2 className=" artist-name">
                                    {this.getArtistName(item)}
                                </h2>
                                <h3 className="track-title">{item.title}</h3>
                                {this.renderFeaturing(item)}
                            </Link>
                            {!geoRestricted && <MusicStats stats={stats} />}
                            {this.renderNumbers(index, this.props.showNumbers)}
                        </div>
                    </div>
                    {!geoRestricted && (
                        <button
                            className={styles.download}
                            onClick={this.props.onDownloadClick}
                        >
                            <DownloadIcon />
                        </button>
                    )}
                    {!geoRestricted && (
                        <button
                            className="vertical-dots track-preview__vertical-dots"
                            onClick={onActionDrawerClick}
                            aria-label="Open action drawer"
                        >
                            <VerticalDots />
                        </button>
                    )}
                    {geoRestricted && (
                        <div className="track-preview__overlay" />
                    )}
                </div>
            </div>
        );
    }

    render() {
        return this.renderItem(this.props.listType);
    }
}
