// @todo upgrade to core-js3 and use the below
// import 'core-js/stable';
// import 'regenerator-runtime/runtime';

import React from 'react';
import { hydrate, render } from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import branch from 'branch-sdk';
import Cookie from 'js-cookie';

import { Support, waitForBranchMetadata } from 'utils/index';
import analytics from 'utils/analytics';
import { configureStore, history } from 'redux/configureStore';
import { HotRoot } from 'components/Root';
import { setConfig } from 'api/index';
import { cookies } from 'constants/index';

import rootReducer from '../shared/redux/reducer';
import routes, { routeConfig } from '../shared/routes';

const initialState = window.__INITIAL_STATE__;
const store = configureStore(initialState, rootReducer, routeConfig);
const rootNode = document.getElementById('react-view');

history.listen((location) => {
    if (!window.__clientHasBeenRendered) {
        return;
    }

    const { pathname, search } = location;

    window.__lastVisitedLocation = location;

    analytics.pageView(`${pathname}${search}`);
});

function start(appRoutes) {
    const routerProps = {
        history
    };
    const fn = process.env.NODE_ENV === 'development' ? render : hydrate;

    fn(
        <HotRoot
            store={store}
            Router={ConnectedRouter}
            routerProps={routerProps}
            routes={appRoutes}
        />,
        rootNode
    );

    // Setting this flag so we don't fire off a request to fetch data
    // when the page loads since that data will have already been fetched
    // by the server when sending down the HTML with initial state
    //
    // @see connectDataFetchers
    window.__clientHasBeenRendered = true;
}

function polyfillPreloadLinks() {
    document.addEventListener('DOMContentLoaded', function handleDOMloaded() {
        document.removeEventListener('DOMContentLoaded', handleDOMloaded);
        if (Support.preloadSupported()) {
            return;
        }

        Array.from(
            document.querySelectorAll('link[rel="preload"][as="style"]')
        ).forEach((link) => {
            link.rel = 'stylesheet';
        });
    });
}

function initBranch() {
    branch.init(
        process.env.BRANCH_KEY,
        {
            disable_entry_animation: true,
            disable_exit_animation: true
        },
        (err, data) => {
            if (err) {
                analytics.error(err);
                console.error(err);
                return;
            }

            console.log(data);
        }
    );
}

function configureServiceWorker() {
    navigator.serviceWorker
        .register(process.env.SERVICE_WORKER_PATH)
        .then((reg) => {
            // Update the service worker as its possible for a user to
            // stay on the site without reloading for a while
            // https://developers.google.com/web/fundamentals/primers/service-workers/lifecycle#manual_updates
            window.setInterval(() => reg.update(), 1000 * 3600);
            return;
        })
        .catch((err) => {
            console.error(err);
        });
}

function run() {
    // We want to always init branch but also wait for potential
    // branch metadata to show up first
    waitForBranchMetadata()
        .then(() => {
            initBranch();
            return;
        })
        .catch((err) => {
            initBranch();
            console.warn(err);
        });

    const context = 'mobile';

    analytics.release(context, process.env.GIT_COMMIT);
    polyfillPreloadLinks();

    let customApiOptions = {};

    try {
        customApiOptions = JSON.parse(Cookie.get(cookies.apiConfig));
    } catch (e) {} // eslint-disable-line

    setConfig(customApiOptions);

    start(routes);

    window.AM_VERSION = process.env.GIT_COMMIT;
    window.AM_CONTEXT = context;

    if (process.env.NODE_ENV === 'development') {
        window.store = store;
    }

    if (
        process.env.ENABLE_SERVICE_WORKERS === 'true' &&
        'serviceWorker' in navigator
    ) {
        window.addEventListener('load', function registerServiceWorker() {
            configureServiceWorker();

            window.removeEventListener('load', registerServiceWorker);
        });
    }

    const { currentUser } = initialState;

    // Allow admins to enable during runtime if they want
    // Will use for testing
    if (currentUser.isLoggedIn && currentUser.isAdmin) {
        window.configureServiceWorker = configureServiceWorker;
    }
}

run();

if (module.hot) {
    module.hot.accept('../shared/routes', () => {
        const newRoutes = require('../shared/routes').default;

        start(newRoutes);
    });
}
