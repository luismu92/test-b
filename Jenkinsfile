#!/usr/bin/env groovy
/*
import groovy.json.JsonOutput
import java.util.Optional
import hudson.model.Actionable
*/
/* Start pipeline */
properties(
    [
        disableConcurrentBuilds(),
    buildDiscarder(
        logRotator(artifactDaysToKeepStr: '1',
            artifactNumToKeepStr: '10',
            daysToKeepStr: '1',
            numToKeepStr: '10'
            )
        ),
        [$class: 'RebuildSettings', autoRebuild: false, rebuildDisabled: false],
        parameters([
            booleanParam(defaultValue: false, description: 'Create a release package', name: 'RELEASE'),
            [
      $class: 'ChoiceParameter',
      choiceType: 'PT_SINGLE_SELECT',
      description: 'Name of the project',
      filterable: false,
      name: 'PROJECT_NAME',
      script: [
        $class: 'GroovyScript', fallbackScript: [classpath: [], sandbox: false, script: ''],
        script: [
          classpath: [],
          sandbox: false,
          script: 'return ["audiomack-js"]'
        ]
      ]
    ]
    ]),
        pipelineTriggers([])
    ]
)

node('master') {
    timeout(60) {
            wrap([$class: 'BuildUser']) {
                wrap([$class: 'MaskPasswordsBuildWrapper']) {
                    wrap([$class: 'TimestamperBuildWrapper'] ) {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            withCredentials([
                                usernamePassword(credentialsId: 'audiomack-machine-user', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')
                            ]) {
                                def date = new Date().format('yyyy-MM-dd')
                                def release_date = date + '-' + 'release' + '-' + env.BUILD_ID
                                def bucket_name = 'audiomack-artifacts'
                                def bucket_path = ""
                                def project_name = params.PROJECT_NAME
                                def slackNotificationChannel = "devops"

                /* Clean workspace. */
                                step([$class: 'WsCleanup'])

                                /* Checkout code */
                                stage ('Checkout') {
                                checkout(
                                    [
                                    $class: 'GitSCM',
                                    branches: scm.branches,
                                    doGenerateSubmoduleConfigurations: false,
                                    extensions: [[$class: 'CleanCheckout']],
                                    submoduleCfg: [],
                                    userRemoteConfigs:
                                        [[credentialsId: 'audiomack-machine-user',
                                        url: 'https://github.com/audiomack/audiomack-js.git']]
                                    ]
                                    )
                }
                this.setBuildInformation(release_date)
                                // def packer = tool(name: '1.2.1', type: 'biz.neustar.jenkins.plugins.packer.PackerInstallation')
                  def packer = "/usr/bin"
                                if (params.RELEASE) {
                                    bucket_path = "releases/" + params.PROJECT_NAME + "/" + release_date
                                stage('Composer self-update') {
                                    sh "sudo composer self-update"
                    }
                                stage('Composer install') {
                                    sh "sudo composer install --no-dev"
                                    }
                                stage('Creating tar file') {
                                    sh "mkdir -p /tmp/${project_name}/${branch_name}/${build_id}"
                                    sh "tar -zcf /tmp/${project_name}/${branch_name}/${build_id}/${project_name}.tar.gz . --exclude .git"
                                    }
                                stage('Upload release to s3') {
                                    sh "aws s3 cp /tmp/${project_name}/${branch_name}/${build_id}/${project_name}.tar.gz s3://audiomack-artifacts/${bucket_path}/${project_name}.tar.gz --region us-east-1  --quiet"
                    }
                                stage('Creating ami for deployment') {
                    dir('packer') {
                                    sh "pwd"
                                    sh "${packer}/packer build -var release_type=releases -var build_user=${env.BUILD_USER_ID} -var build_url=${env.BUILD_URL} -var ami_version=${release_date} -var ami_name=${params.PROJECT_NAME} packer.json"
                                    }
                   }
                                stage('Generating Release Notes & Create GitHub Release') {
                    dir('packer') {
                    sh "bash releasenotes.sh ${release_date}"
                  }
                 }
                                stage('Cleanup') {
                                    sh "rm -rf /tmp/${project_name}/${branch_name}/${build_id}"
                    }
                                     } else {
                                    def branch_original = env.BRANCH_NAME
                                    def branch_replaced = branch_original.replace("/","-")
                                    bucket_path = "snapshots/" + params.PROJECT_NAME +"/" + branch_replaced
                                stage('Setting dev environment file') {
                                    response = sh returnStdout: true, script: '''#!/bin/bash
                                    touch config/env/.env.devcf
                                    echo "API_URL=https://api-dev.aws.audiomack.com/v1" >config/env/.env.devcf
                                    echo "NODE_ENV=development" >>config/env/.env.devcf
                                    echo "AM_URL=https://audiomack-js-dev.aws.audiomack.com" >>config/env/.env.devcf
                                    echo "AM_DOMAIN=audiomack-js-dev.aws.audiomack.com" >>config/env/.env.devcf
                                    echo "REDIS_MAIN_URL=redis://ip-192-168-2-239.ec2.internal:6379" >> config/env/.env.devcf
                                    echo "PHP_BACKEND_URL=https://dcf-dev.aws.audiomack.com" >> config/env/.env.devcf
                                    echo "AUDIOMACK_DEV=true" >> config/env/.env.devcf
                                    echo "API_CONSUMER_SECRET=d414055457e991807398b0a61bc15b00"  >> config/env/.env.devcf
                                    echo "MUSIC_URL=https://songs-dev.aws.audiomack.com" >>config/env/.env.devcf
                                    echo "ASSETS_URL=https://assets-dev.aws.audiomack.com" >>config/env/.env.devcf
                                    echo "FACEBOOK_APP_ID=1482202045124554"
                                    echo "INSTAGRAM_APP_ID=56ff0104ada145aa81fa59358814bd12"
                                    echo "STATIC_URL=https://s3.amazonaws.com/audiomack.dev.static" >> config/env/.env.devcf
                                    echo "STATIC_BUCKET=audiomack.dev.static" >> config/env/.env.devcf
                                    echo "INSTAGRAM_REDIRECT_URI=https://api-dev.aws.audiomack.com/edit/profile"
                                    echo "GOOGLE_AUTH_ID=122326890670-fnq113nf7ks4ogtpjpc7bsndm8eu8n9b.apps.googleusercontent.com"
                                    echo "RABBITMQ_HOST=rabbit-nlb-32f9cb4aaefc6fe1.elb.us-east-1.amazonaws.com" >>config/env/.env.devcf
                                    echo "SONGTRUST_CLIENT_ID=test_4xWwB7w9rmimxe6kFaL7a9SPcWuSuyec4qOQIVdm" >>config/env/.env.devcf
                                    echo "REDIS_CLUSTER_ENABLED=true"  >>config/env/.env.devcf
                                    echo "RABBITMQ_USER=user"  >>config/env/.env.devcf
                                    echo "RABBITMQ_PASS=kVV3V4tggMuC"  >>config/env/.env.devcf
                                    echo "BRANCH_NAME=${BRANCH_NAME}" >>config/env/.env.devcf'''
                    }
                println(response)

                                stage('Running Yarn') {
                                    sh "NODE_ENV=development yarn  --network-timeout 100000"
                    }
                                stage('Running npm rebuild node-sass') {
                                    sh "npm rebuild node-sass"
                                    }
                                stage('Running npm run lint') {
                                    sh "npm run lint"
                                    }
                                stage('Running npm run test:predeploy') {
                                    sh "npm run test:predeploy"
                                    }
                                stage('Running npm run build') {
                                    sh "npm run build"
                                    }
                                stage('Running npm run publish-sourcemaps') {
                                    sh "npm run publish-sourcemaps"
                                    }
                                stage('Creating tar file') {
                                    sh "cd dist && tar -zcf ../${project_name}.tar.gz . && cd .."
                    }
                               stage('Uploading static to audiomack-dev-host') {
                                    sh 'aws s3 cp ${WORKSPACE}/public/static/dist/ s3://audiomack-dev-webhost/development/static/dist/ --recursive --exclude "*.map" --cache-control max-age=31536000 --no-progress --acl bucket-owner-full-control'
                                    }
                                stage('Upload snapshot to s3') {
                                    sh "aws s3 cp ${project_name}.tar.gz s3://audiomack-artifacts/${bucket_path}/${project_name}.tar.gz --region us-east-1  --quiet"
                    }
                                stage('Cleanup') {
                                    sh "rm -rf ${project_name}.tar.gz"
                                    }
                                  }
                                }
                              }
                }
              }
            }
              }
                }


def setBuildInformation(release_date) {

    def build_type = ''
    if (params.RELEASE) {
      build_type = 'Release'
    }

    def displayName = ''
    if (params.RELEASE) {
    displayName = release_date
    } else {
      displayName = "#${BUILD_NUMBER}"
      }

    currentBuild.displayName = displayName

}
