require('./am-shared/utils/setEnv');

// Allows us to import/require files that are in am-shared folder
// without having to prefix the path.
// i.e. instead of doing:
//
// import connectDataFetchers from '../../whatever/am-shared/lib/connectDataFetchers'
//
// You can do:
//
// import connectDataFetchers from 'lib/connectDataFetchers'

const nodePath = process.env.NODE_PATH
    ? `${process.env.NODE_PATH}:./am-shared`
    : './am-shared';

module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [
        {
            name: 'Audiomack',
            cwd: process.env.PM2_CWD,
            script: 'server/index.js',
            instances: 'max',
            exec_mode: 'cluster',
            log_date_format: 'YYYY-MM-DD HH:mm Z',
            combine_logs: true,
            out_file: process.env.PM2_LOG_OUT_FILE,
            max_memory_restart: process.env.PM2_MAX_MEMORY_RESTART,
            env: Object.assign({}, process.env, {
                NODE_PATH: nodePath
            })
        },
        {
            name: 'email-worker',
            cwd: process.env.PM2_CWD,
            script: 'workers/email/index.js',
            instances: 1,
            exec_mode: 'cluster',
            log_date_format: 'YYYY-MM-DD HH:mm Z',
            combine_logs: true,
            out_file: process.env.PM2_EMAIL_WORKER_OUT,
            max_memory_restart: process.env.PM2_MAX_MEMORY_RESTART,
            env: Object.assign({}, process.env, {
                NODE_PATH: nodePath
            })
        },
        {
            name: 'sitemap-worker',
            cwd: process.env.PM2_CWD,
            script: 'workers/sitemap/index.js',
            instances: 1,
            exec_mode: 'cluster',
            log_date_format: 'YYYY-MM-DD HH:mm Z',
            combine_logs: true,
            out_file: process.env.PM2_SITEMAP_WORKER_OUT,
            max_memory_restart: process.env.PM2_MAX_MEMORY_RESTART,
            env: Object.assign({}, process.env, {
                NODE_PATH: nodePath
            })
        }
    ]
};
