/**
 * Example usage:
 * NODE_PATH=$NODE_PATH:./am-shared node scripts/trending/index.js https://audiomack.com/album/dot-demo/my-brothers-keeper
 */
import fs from 'fs';
import url from 'url';
import mkdirp from 'mkdirp';
import { generateImage } from '../../server/handlers/trending';

async function main() {
    try {
        const amUrl = process.argv[2];
        const imageData = await generateImage(amUrl);
        const outputPath = `/tmp${url.parse(amUrl).pathname}`;

        // eslint-disable-next-line no-sync
        mkdirp.sync(outputPath);

        const outputFile = `${outputPath}/${Date.now()}.png`;

        // eslint-disable-next-line no-sync
        fs.writeFileSync(outputFile, imageData);

        console.log(`Wrote ${outputFile}`);
    } catch (err) {
        console.error(err);
    }
}

main();
