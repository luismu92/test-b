const path = require('path');
const glob = require('glob');
const { publishSourcemap } = require('@newrelic/publish-sourcemap');

require('../../am-shared/utils/setEnv');

function publish(jsPath, sourcemapPath) {
    return new Promise((resolve, reject) => {
        const options = {
            sourcemapPath: sourcemapPath, // from a local file
            // sourcemapUrl: 'https://example.com/sourcefile.js.map', // or from a URL
            javascriptUrl: jsPath,
            applicationId: process.env.NEW_RELIC_APP_ID,
            nrAdminKey: process.env.NEW_RELIC_ADMIN_KEY
            // We dont need this because our JS is hashed every deploy and non-new hashes dont need a new sourcemap generated for them
            // releaseName: sourcemapPath.includes('/desktop/') ? 'desktop' : 'mobile',
            // releaseId: process.env.GIT_COMMIT
        };

        publishSourcemap(options, function(err) {
            if (err) {
                reject({ err, options });
                return;
            }

            resolve();
        });
    });
}

function getMapFiles(jsDir) {
    return new Promise((resolve, reject) => {
        glob(`${jsDir}/**/*.js.map`, function(err, files) {
            if (err) {
                reject(err);
                return;
            }

            resolve(files);
        });
    });
}

function main() {
    const jsDir = path.join(__dirname, '../../public/static/dist');
    let hitLimit = false;

    getMapFiles(jsDir).then((files) => {
        const promises = files.map((sourcemapFile) => {
            const jsFile = sourcemapFile.substr(0, sourcemapFile.length - 4);
            const relativeJSPath = path.relative('./public', jsFile);
            const finalJSPath = `${process.env.AM_URL}/${relativeJSPath}`;

            return publish(finalJSPath, sourcemapFile).catch(({ err, options }) => {
                if (err.status === 409) {
                    return;
                }

                if (err.status === 429) {
                    if (!hitLimit) {
                        console.log('We hit the sourcemap API rate limit');
                    }
                    hitLimit = true;
                    return;
                }

                console.log(err.response.text);
                console.log('with options:');
                console.log(options);
            });
        });

        return Promise.all(promises);
    }).then(() => {
        return process.exit(0);
    }).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}

main();
