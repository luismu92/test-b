// $ node scripts/deploy/webpackBuild/createTestWebpackStats.js
const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const { promisify } = require('util');
const { getFilteredStatJson } = require('./createWebpackStats');
const buildClientConfigs = require('../../../config/webpack/client.development');
const writeFileAsync = promisify(fs.writeFile);

function getStats(app) {
    return new Promise((resolve, reject) => {
        const config = buildClientConfigs([app])[0];

        const compiler = webpack(config);

        compiler.run((err, stats) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(getFilteredStatJson(stats, compiler.name));
        });
    });
}

async function main() {
    try {
        console.log('Creating test stats files…');
        const writes = ['desktop', 'mobile'].map(async (app) => {
            const stats = await getStats(app);
            const outputPath = path.join(
                __dirname,
                `../../../test/helpers/stats-${app}.json`
            );

            await writeFileAsync(outputPath, JSON.stringify([stats]), 'utf8');
            console.log('Wrote to', outputPath);
        });

        await Promise.all(writes);

        console.log('Done.');
        process.exit(0);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}

main();
