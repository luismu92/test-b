// $ DEBUG=webpack-build node scripts/deploy/webpackBuild/index.js
const webpack = require('webpack');
const isDev = process.env.NODE_ENV === 'development';
const configSuffix = isDev ? 'development' : 'production';
const buildClientConfigs = require(`../../../config/webpack/client.${configSuffix}`);
const buildServerConfig = require(`../../../config/webpack/server.${configSuffix}`);

function getFilteredStatJson(statObject, name) {
    const statsJson = statObject.toJson({
        all: false,
        chunks: true,
        chunkGroups: true,
        chunkModules: true,
        modules: true,
        publicPath: true,
        assets: true
    });

    // These are the only keys needed for webpack-flush-chunks
    // https://github.com/faceyspacey/webpack-flush-chunks/blob/3bcf73a158183b2569688f47f3f9bffe8aaabf25/src/flushChunks.js#L13-L30
    const {
        assetsByChunkName,
        namedChunkGroups,
        chunks,
        modules,
        publicPath,

        // Propagate these up to the main bundle
        errors,
        warnings
    } = statsJson;

    return {
        assetsByChunkName,
        namedChunkGroups,
        // Only take the fields that are needed to save hella memory
        chunks: chunks.map((chunk) => {
            return { id: chunk.id, files: chunk.files };
        }),
        // Only take the fields that are needed to save hella memory
        modules: modules.map((mod) => {
            return { id: mod.id, name: mod.name, chunks: mod.chunks };
        }),
        publicPath,

        name,
        errors,
        warnings
    };
}

module.exports = function getStats(app, callback) {
    let config;

    if (app === 'server') {
        config = buildServerConfig();
    } else {
        config = buildClientConfigs([app])[0];
    }

    const compiler = webpack(config);

    compiler.run((err, stats) => {
        if (err) {
            callback(err);
            return;
        }

        callback(null, getFilteredStatJson(stats, compiler.name));
    });
};

module.exports.getFilteredStatJson = getFilteredStatJson;
