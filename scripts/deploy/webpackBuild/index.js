/* eslint-env node */
// $ DEBUG=webpack-build node scripts/deploy/webpackBuild/index.js

const fs = require('fs');
const util = require('util');
const path = require('path');
const numCpus = require('os').cpus().length;
// eslint-disable-next-line no-sync
const execSync = require('child_process').execSync;
const workerFarm = require('worker-farm');
const workers = workerFarm(
    {
        maxConcurrentWorkers: numCpus - 1,
        maxRetries: 0
    },
    require.resolve('./createWebpackStats')
);
const debug = require('debug');
const buildTag = 'webpack-build';
const log = debug(buildTag);
const validApps = require('../../../config/webpack/buildAppConfigs').validApps;
const outputPath = path.join(__dirname, '../../../dist/server/stats.json');
const writeFileAsync = util.promisify(fs.writeFile);

function runWorkers(configs) {
    const work = configs.map((config) => {
        return new Promise((resolve, reject) => {
            workers(config, (err, stats) => {
                if (err) {
                    reject(err);
                    return;
                }

                log(`Bundle for "${stats.name}" compiled.`);

                resolve(stats);
            });
        });
    });

    return Promise.all(work)
        .then((jsonStats) => {
            workerFarm.end(workers);
            return jsonStats;
        })
        .catch((err) => {
            workerFarm.end(workers);
            // https://blog.box.com/how-we-improved-webpack-build-performance-95
            execSync(
                `ps ax | grep "${path.basename(
                    __filename
                )}" | cut -b1-06 | xargs -t kill`
            );
            throw err;
        });
}

function mergeStats(statJSONObjects) {
    return statJSONObjects.reduce(
        (acc, json) => {
            acc.errors = acc.errors.concat(json.errors);
            acc.warnings = acc.warnings.concat(json.warnings);
            acc.children = acc.children.concat(json);

            return acc;
        },
        { errors: [], warnings: [], children: [] }
    );
}

function formatTime(millis) {
    const minutes = Math.floor(millis / 60000);
    const seconds = ((millis % 60000) / 1000).toFixed(0);
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
}

async function main() {
    try {
        const start = Date.now();
        const appBuilds = ['server'].concat(validApps);

        log(
            `Running webpack for ${appBuilds.length} builds: ${appBuilds.join(
                ', '
            )}`
        );
        const jsonStats = await runWorkers(appBuilds);

        log('Merging stats.');
        const statsJson = mergeStats(jsonStats);

        if (statsJson.warnings.length) {
            log(`Warnings:
    ${statsJson.warnings.join('\n')}`);
        }

        if (statsJson.errors.length) {
            log(`Errors:
    ${statsJson.errors.join('\n')}`);
        }

        log(`Writing stats.json to file ${outputPath}`);
        await writeFileAsync(outputPath, JSON.stringify(statsJson));

        log(`Done in ${formatTime(Date.now() - start)}`);
        process.exit(0);
    } catch (err) {
        console.log(err);
        process.exit(1);
    }
}

main();
