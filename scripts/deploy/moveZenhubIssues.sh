#!/bin/bash

echo "Checking for Zenhub issues to move..."

# "id": "5abe9c5df41bfa50c0e46352",
# "name": "Dev QA",

# "id": "5ab3103315ce296c9c1a5812",
# "name": "MAJ User QA",

# "id": "5af194cff8a77e61f90d4274",
# "name": "AM User QA",

# Product Management Workspace
# workspace_id=5cf32d3c1437ff1e943f0b1a

# Dev Team Workspace
workspace_id=5d6810f1d6f05075fcae798f

# In Progress column
inprogress_column=5d6810f1d6f05075fcae7989

# Dev QA column
dev_qa_column=5d6810f1d6f05075fcae798d

# My personal api key, dont make me look bad... -Sean
token=1a9a6fab0add8b1c3883e2d85d0e1a96bbc6858d1e23b291ac17caeb7df9842de2209e622c694dc1

# Repo id for audiomack/audiomack-js
target_repo=49023228

move_issue_to_qa() {
    echo "Moving issue $1 to Dev QA"

    curl -X POST \
        "https://api.zenhub.io/p2/workspaces/$workspace_id/repositories/$target_repo/issues/$1/moves" \
        -H 'Accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "x-authentication-token: $token" \
        -d "{
            \"pipeline_id\": \"$dev_qa_column\",
            \"position\": \"top\"
        }"
}

move_issue_inprogress() {
    echo "Moving issue $1 to In Progress"

    curl -X POST \
        "https://api.zenhub.io/p2/workspaces/$workspace_id/repositories/$target_repo/issues/$1/moves" \
        -H 'Accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "x-authentication-token: $token" \
        -d "{
            \"pipeline_id\": \"$inprogress_column\",
            \"position\": \"top\"
        }"
}

containsElement() {
    local seeking=$1; shift
    local in=1
    for element; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

fixed_issue_regex='(close|closes|closed|fix|fixes|fixed|resolve|resolves|resolved) #([0-9]+)'
# Explicit leading whitespace as we're looking for a commit message
# with the issue number tagged without the potential repo prefix
issue_regex=' #([0-9]+)'
digit_regex='[0-9]+'

# Test a commit message uncommenting below
# commit_message='Do some shit with fix #10022 and also #10023'

fixed_ids=($(echo $commit_message | grep -o -E "$fixed_issue_regex"))

for id in "${fixed_ids[@]}"
do
    if [[ $id =~ $digit_regex ]]
    then
        issue_number="${BASH_REMATCH[0]}"
        move_issue_to_qa $issue_number
    fi
done

# Check for issues that werent in the fix array but were tagged
# and move them to the in progress column
inprogress_ids=($(echo $commit_message | grep -o -E "$issue_regex"))

for id in "${inprogress_ids[@]}"
do
    trimmed=$(echo $id | sed -e 's/^[[:space:]]*//')
    if [[ $trimmed =~ $digit_regex ]] && ! containsElement "$trimmed" "${fixed_ids[@]}"
    then
        issue_number="${BASH_REMATCH[0]}"
        move_issue_inprogress $issue_number
    fi
done

echo "Done with Zenhub!"
