const { listSourcemaps, deleteSourcemap } = require('@newrelic/publish-sourcemap');

require('../../am-shared/utils/setEnv');

function list(page = 1, { limit = 99, offset } = {}) {
    return new Promise((resolve, reject) => {
        const options = {
            limit: limit,
            offset: offset || (page - 1) * limit,
            applicationId: process.env.NEW_RELIC_APP_ID,
            nrAdminKey: process.env.NEW_RELIC_ADMIN_KEY
        };

        listSourcemaps(options, function(err, body) {
            if (err) {
                reject({ err, options });
                return;
            }

            resolve(body);
        });
    });
}

function deleteMap(sourcemapId) {
    return new Promise((resolve, reject) => {
        const options = {
            sourcemapId,
            applicationId: process.env.NEW_RELIC_APP_ID,
            nrAdminKey: process.env.NEW_RELIC_ADMIN_KEY
        };

        deleteSourcemap(options, function(err) {
            if (err) {
                reject({ err, options });
                return;
            }

            resolve();
        });
    });
}

function deleteMaps(options = {}) {
    const {
        page = 1,
        // Just pass a function that returns true to delete all maps
        condition = (map) => !map.releaseName || !map.releaseId
    } = options;

    return list(page).then(({ next, sourcemaps }) => {
        const nullMapPromises = sourcemaps.filter(condition).map((map) => {
            return deleteMap(map.id);
        });

        let promise = Promise.all(nullMapPromises);

        if (sourcemaps.length && next) {
            promise = promise.then(() => {
                return deleteMaps({ page: page + 1, condition });
            });
        }

        return promise;
    });
}


function main() { // eslint-disable-line
    deleteMaps().then(() => {
        return process.exit(0);
    }).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}

main();
