#!/bin/bash

./node_modules/.bin/pm2 delete all
./node_modules/.bin/pm2 kill
./node_modules/.bin/pm2 startOrRestart --env $NODE_ENV audiomack.config.js --update-env
