#! /usr/bin/env node
const path = require('path');
const fs = require('fs');
const glob = require('glob');
const cheerio = require('cheerio');
const debug = require('debug');
const directory = process.argv[2];

const log = debug('SpriteFormatter:');

function readFile(readPath) {
    return new Promise((resolve, reject) => {
        fs.readFile(readPath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(data);
        });
    });
}

function readSvgDirectory(dirPath) {
    return new Promise((resolve, reject) => {
        glob(dirPath, (err, files) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(files);
        });
    });
}

function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, 'utf8', (writeError) => {
            if (writeError) {
                reject(writeError);
                return;
            }

            log('Wrote', filePath);

            resolve();
        });
    });
}

function getFormattedSvgData(data) {
    if (!data.trim()) {
        return '';
    }

    const $ = cheerio.load(data, { xmlMode: true });
    const validSvgAttributes = [
        'width',
        'height',
        'viewbox',
        'version',
        'xmlns'
    ];

    const validPathTags = [
        'path',
        'circle',
        'polygon'
    ];

    const validPathAttributes = [
        'd',
        'cy',
        'cx',
        'points',
        'r'
    ];
    const svg = $('svg');

    const paths = validPathTags.reduce((acc, tag) => {
        const tags = svg.find(tag);

        tags.each((i, el) => {
            acc.push(el);
        });

        return acc;
    }, []);

    // Remove unneeded attributes from SVG
    Object.keys(svg.attr() || {}).forEach((attr) => {
        if (validSvgAttributes.indexOf(attr.toLowerCase()) === -1) {
            svg.removeAttr(attr);
        }
    });

    // Remove unneeded attributes from paths
    const alteredPaths = paths.map((svgPath) => {
        const alteredPath = cheerio.load(svgPath, { xmlMode: true })(svgPath.name);

        Object.keys(alteredPath.attr() || {}).forEach((attr) => {
            if (validPathAttributes.indexOf(attr.toLowerCase()) === -1) {
                alteredPath.removeAttr(attr);
            }
        });

        return alteredPath;
    });

    svg.empty();
    svg.append(alteredPaths);

    // Remove XML declaration
    $.root().empty();
    $.root().append(svg);

    return $.html();
}

function rewriteFile(file) {
    return readFile(file).then(getFormattedSvgData).then((formatted) => {
        return writeFile(file, formatted);
    });
}

function main() {
    const fullDirPath = path.join(`${path.resolve(directory)}/**/*.svg`);

    readSvgDirectory(fullDirPath).then((files) => {
        return Promise.all(files.map(rewriteFile));
    }).then(() => {
        log('Done formatting files!');
        process.exit();
        return;
    }).catch((err) => {
        log(err);
        process.exit(1);
    });
}

main();
