#!/bin/bash

# Commenting out the sprite sheet stuff because IE doesnt support
# SVG sprite sheets

set -e

# MOBILE_SHEET_FILE_HREF=/static/images/am-mobile-sprite-sheet.svg
# MOBILE_SHEET_FILE_PATH="public$MOBILE_SHEET_FILE_HREF"
MOBILE_ICON_DIR=public/static/images/mobile/icons
MOBILE_ICON_COMPONENT_DIR=am-mobile/shared/icons

# node scripts/icons/formatSvgsForSpriteSheet.js $MOBILE_ICON_DIR
# node_modules/.bin/svg-sprite-generate -d $MOBILE_ICON_DIR -o $MOBILE_SHEET_FILE_PATH
node_modules/.bin/ist -i "$MOBILE_ICON_DIR/**/*.svg" -t config/ist/react-icon-class.js -o $MOBILE_ICON_COMPONENT_DIR -c true -e .js




# DESKTOP_SHEET_FILE_HREF=/static/images/am-desktop-sprite-sheet.svg
# DESKTOP_SHEET_FILE_PATH="public$DESKTOP_SHEET_FILE_HREF"
DESKTOP_ICON_DIR=public/static/images/desktop/icons
DESKTOP_ICON_COMPONENT_DIR=am-desktop/shared/icons

# node scripts/icons/formatSvgsForSpriteSheet.js $DESKTOP_ICON_DIR
# node_modules/.bin/svg-sprite-generate -d $DESKTOP_ICON_DIR -o $DESKTOP_SHEET_FILE_PATH
node_modules/.bin/ist -i "$DESKTOP_ICON_DIR/**/*.svg" -t config/ist/react-icon-class.js -o $DESKTOP_ICON_COMPONENT_DIR -c true -e .js



# SHARED_SHEET_FILE_HREF=/static/images/am-shared-sprite-sheet.svg
# SHARED_SHEET_FILE_PATH="public$SHARED_SHEET_FILE_HREF"
SHARED_ICON_DIR=public/static/images/shared/icons
SHARED_ICON_COMPONENT_DIR=am-shared/icons

# node scripts/icons/formatSvgsForSpriteSheet.js $SHARED_ICON_DIR
# node_modules/.bin/svg-sprite-generate -d $SHARED_ICON_DIR -o $SHARED_SHEET_FILE_PATH
node_modules/.bin/ist -i "$SHARED_ICON_DIR/**/*.svg" -t config/ist/react-icon-class.js -o $SHARED_ICON_COMPONENT_DIR -c true -e .js

# Don't want to optimize this svg because its more intricate
node_modules/.bin/ist -i "$SHARED_ICON_DIR/google-logo-color.svg" -t config/ist/react-icon-class.js -o $SHARED_ICON_COMPONENT_DIR -e .js




# CHROMECAST_SHEET_FILE_HREF=/static/images/am-chromecast-sprite-sheet.svg
# CHROMECAST_SHEET_FILE_PATH="public$CHROMECAST_SHEET_FILE_HREF"
CHROMECAST_ICON_DIR=public/static/images/chromecast/icons
CHROMECAST_ICON_COMPONENT_DIR=am-chromecast/shared/icons

# node scripts/icons/formatSvgsForSpriteSheet.js $CHROMECAST_ICON_DIR
# node_modules/.bin/svg-sprite-generate -d $CHROMECAST_ICON_DIR -o $CHROMECAST_SHEET_FILE_PATH
node_modules/.bin/ist -i "$CHROMECAST_ICON_DIR/**/*.svg" -t config/ist/react-icon-class.js -o $CHROMECAST_ICON_COMPONENT_DIR -c true -e .js
