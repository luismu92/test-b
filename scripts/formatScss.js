#! env /usr/local/bin/node

/**
 * This script files a lot of scss rules according to the
 * .stylelintrc file in the root directory. It doesn't fix everything
 * since `stylefmt` can't fix everything but it helps so you
 * dont have to do everything manually. Hopefully we'd only need this
 * file once though. Also it seems I had trouble running this
 * via CLI but it worked in electron. Not sure why but like I
 * mentioned before, we should probably only need this one time.
 */

const fs = require('fs');
const postcss = require('postcss');
const scss = require('postcss-scss');
const stylefmt = require('stylefmt');
const glob = require('glob');

function getFilesFromGlob(pattern, options) {
    return new Promise((resolve, reject) => {
        glob(pattern, options, (err, files) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(files);
        });
    });
}

function readFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve({
                file,
                data
            });
        });
    });
}

function writeFile(file, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, content, 'utf-8', (err) => {
            if (err) {
                reject(err);
                return;
            }
            resolve({
                file
            });
        });
    });
}

function formatScssContent(obj) {
    const data = obj.data;
    const file = obj.file;

    return postcss([
        stylefmt
    ]).process(data, { syntax: scss }).then((results) => {
        return {
            file,
            results
        };
    });
}

function main() {
    getFilesFromGlob('public/scss/**/*.scss').then((files) => {
        console.log('Correcting syntax in:', files);
        return Promise.all(files.map(readFile));
    }).then((fileAndContents) => {
        return Promise.all(fileAndContents.map(formatScssContent));
    }).then((formatted) => {
        const writes = formatted.map((obj) => {
            const file = obj.file;
            const formattedScss = obj.results.css;

            return writeFile(file, formattedScss);
        });

        return Promise.all(writes);
    }).then(() => console.log('Done!')).catch((e) => console.log(e));
}

main();
