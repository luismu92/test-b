#! /bin/bash

set -e

# Clean destination directories
rm -rf public/static/dist/
rm -rf dist/*

./node_modules/.bin/babel am-shared -d dist/am-shared

npm run style

mkdir -p dist/public/static
# Copy favicon
# @todo fix this in build flow

cp public/static/favicons.html dist/public/static/

# Add directory for webpack stats
mkdir -p dist/server
npm run webpack:prod

# Running uglify here instead of using webpack plugin because
# for some reason the webpack plugin fails to compile the server
# with it.
#
# Old issue, but its been a problem before...
# https://github.com/webpack/webpack/issues/537

# Source map is not working for some reason
# Try this again when we upgrade node
# ./node_modules/.bin/uglifyjs dist/server/server.js --output dist/server/server.min.js --compress drop_console=true --source-map "content='dist/server/server.js.map'"
# ./node_modules/.bin/uglifyjs dist/server/server.js --output dist/server/server.min.js --compress drop_console=true

cp server/index.js dist/server

cp -a config dist
cp -a public dist/

#### Build Workers ####

NODE_ENV=production ./node_modules/.bin/webpack --config config/webpack/worker.config.js workers/sitemap/worker.js -o dist/workers/sitemap/index.js

NODE_ENV=production ./node_modules/.bin/webpack --config config/webpack/worker.config.js workers/email/worker.js -o dist/workers/email/index.js
cp -a workers/email/assets dist/workers/email/

cp .env.* dist/
cp audiomack.config.js dist/

# Rename node_modules folder to node_modules2
# and install only prod dependencies to be moved into dist folder
#
# Then move node_modules2 back to node_modules so other scripts after
# this one can use, ie npm run publish-sourcemaps

mv node_modules node_modules2
NODE_ENV=production yarn
mv node_modules dist/
mv node_modules2 node_modules

echo 'Final dist contents:'
ls -la dist

echo 'disk size:'
du -sh dist
