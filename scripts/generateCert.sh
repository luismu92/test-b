#! /bin/bash

openssl req -x509 -newkey rsa:2048 -keyout ../config/ssl/key.pem -out ../config/ssl/cert.pem -days 365
openssl rsa -in ../config/ssl/key.pem -out ../config/ssl/newkey.pem && mv ../config/ssl/newkey.pem ../config/ssl/key.pem
