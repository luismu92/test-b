require('../am-shared/utils/setEnv');

const puppeteer = require('puppeteer');
const chalk = require('chalk');
// const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const { urls, sizes, queryStringVariants, getFileName } = require('../test/helpers/embed');

const outputDir = path.join(__dirname, '../test/e2e/desktop/expectedScreenshots');

async function main() {
    try {
        console.log(chalk.yellow('Generating screenshots of current embed pages'));
        // const port = process.env.PORT || 3000;
        const config = {
            args: []
        };

        // if (process.env.NO_SANDBOX === 'true') {
        // config.args.push('--no-sandbox');
        // }

        // if (process.env.HEADLESS === 'false') {
        // config.headless = false;
        //     config.slowMo = 100;

        // config.args.push(
        //     '--remote-debugging-address=0.0.0.0',
        //     `--remote-debugging-port=${port}`
        // );
        // }

        const browser = await puppeteer.launch(config);
        const page = await browser.newPage();

        mkdirp.sync(outputDir);

        for (const obj of urls) {
            for (const size of sizes) {
                for (const qs of queryStringVariants) {
                    const [width, height] = size;

                    await page.setViewport({ width, height });

                    const fileName = getFileName(obj, size, qs);
                    const filePath = path.join(`${outputDir}/${fileName}.png`);

                    await page.goto(`${obj.url}${qs}`);
                    await page.waitForSelector('[data-testid="playButton"]');
                    await page.screenshot({ path: filePath });

                    console.log(chalk.yellow(`Wrote: ${filePath}`));
                }
            }
        }

        console.log(chalk.green(`Output expected screenshots in ${outputDir}`));
        // fs.writeFileSync(path.join(DIR, 'wsEndpoint'), browser.wsEndpoint()); // eslint-disable-line
        process.exit(0);
    }
    catch (err) {
        console.log(chalk.red(err));
        process.exit(1);
    }
}

main();
