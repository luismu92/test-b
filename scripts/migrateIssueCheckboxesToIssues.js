#! /usr/bin/env node

/**
 * This script is used to migrate an issue with a large amount
 * of checkboxes to individual issues under a project
 */
const octokit = require('@octokit/rest')({
    baseUrl: 'https://api.github.com'
});

octokit.authenticate({
    type: 'token',
    token: process.env.GITHUB_TOKEN
});

/* *** Start: Only need to edit these constants *** */
// Project board title to move to
const PROJECT_TITLE = 'Automated Testing';

// Project column to move new issues to
const PROJECT_COLUMN_NAME = 'To Do';
const ORAGNIZATION = 'audiomack';
const REPOSITORY = 'audiomack-js';

// Issue number to grab checkboxes from
const ISSUE_NUMBER_WITH_CHECKBOXES = 11;

// If you have checkboxes separated in different sections,
// add the markdown for the title type.
const TITLE_PREFIX = '###';
/* *** End: Only need to edit these constants *** */

function getCheckboxesFromIssueBody(body) {
    let currentTitle = '';

    return body.split('\n').map((line) => {
        const trimmed = line.trim();

        if (TITLE_PREFIX) {
            const titleMatch = trimmed.match(new RegExp(`^${TITLE_PREFIX}(.*)`));

            if (titleMatch) {
                currentTitle = titleMatch[1].trim();
            }
        }

        const match = trimmed.match(/^\- \[x?\s?\]/);

        if (!match) {
            return null;
        }

        let issueTitle = line.replace(match[0], '').trim();

        if (currentTitle) {
            issueTitle = `${currentTitle} - ${issueTitle}`;
        }

        return {
            done: match[0].includes('x'),
            title: issueTitle
        };
    }).filter(Boolean);
}

async function main() {
    try {
        // Use to get project ID
        const { data: projectData } = await octokit.projects.getOrgProjects({
            org: ORAGNIZATION
        });
        const project = projectData.find((p) => {
            return p.name.toLowerCase() === PROJECT_TITLE.toLowerCase();
        });

        if (!project) {
            throw new Error(`No project found with name: ${PROJECT_TITLE}`);
        }

        const columnPromise = octokit.projects.getProjectColumns({
            'project_id': project.id
        });
        const issuePromise = octokit.issues.get({
            owner: ORAGNIZATION,
            repo: REPOSITORY,
            number: ISSUE_NUMBER_WITH_CHECKBOXES
        });

        const values = await Promise.all([columnPromise, issuePromise]);
        const [{ data: columnData }, { data: issueData }] = values;
        const issuesToCreate = getCheckboxesFromIssueBody(issueData.body);
        const labels = issueData.labels.map((l) => l.name);
        const assignees = issueData.assignees.map((a) => a.login);

        // Sometimes when a checkbox title had a url in it
        // things would fail. Uncomment the following blocks
        // and set the title variable below appropriately to
        // continue where things left off
        //
        //
        // const errorTitle = 'User Functionality - Sign In (Mobile) https://github.com/audiomack/audiomack-js/commit/517695b43b4da80bbd981b74e1d08497f4350129';
        // let shouldRun = false;

        for (const issue of issuesToCreate) {
            // if (issue.title === errorTitle) {
            //     shouldRun = true;
            //     continue;
            // }

            // if (!shouldRun) {
            //     continue;
            // }

            console.log('Creating issue with', issue);

            const { data: newIssue } = await octokit.issues.create({
                owner: ORAGNIZATION,
                repo: REPOSITORY,
                title: issue.title,
                body: issue.done ? issue.title : undefined,
                labels,
                assignees
            });

            const column = columnData.find((c) => c.name.toLowerCase() === PROJECT_COLUMN_NAME.toLowerCase());

            if (!column) {
                throw new Error(`No column found with name: ${PROJECT_COLUMN_NAME}`);
            }

            await octokit.projects.createProjectCard({
                'column_id': column.id,
                'content_id': newIssue.id,
                'content_type': 'Issue'
            });

            if (issue.done) {
                await octokit.issues.create({
                    owner: ORAGNIZATION,
                    repo: REPOSITORY,
                    number: newIssue.number,
                    state: 'closed'
                });
            }
        }


        console.log('Done!');
    }
    catch (e) {
        console.log(e);
    }
}

main();
