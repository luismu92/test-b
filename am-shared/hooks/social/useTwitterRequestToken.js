import { useState, useEffect, useReducer } from 'react';
import api from 'api/index';

const REQUEST = 'useTwitterRequestToken/REQUEST';
const FAILURE = 'useTwitterRequestToken/FAILURE';
const FETCH = 'useTwitterRequestToken/FETCH';

function useTwitterRequestTokenReducer(state, action) {
    switch (action.type) {
        case REQUEST:
            return {
                ...state,
                loading: true,
                error: false
            };

        case FETCH:
            return {
                ...state,
                loading: false,
                error: false,
                data: {
                    twitterToken: action.twitterToken,
                    twitterId: action.twitterId
                }
            };

        case FAILURE:
            return {
                ...state,
                loading: false,
                error: true
            };

        default:
            throw new Error('Unknown action passed.');
    }
}

// Modeled from https://www.robinwieruch.de/react-hooks-fetch-data
export default function useTwitterRequestToken(currentUser, apiModule = api) {
    const [user, setUser] = useState(currentUser);
    const [state, dispatch] = useReducer(useTwitterRequestTokenReducer, {
        loading: false,
        error: false,
        data: { twitterToken: null, twitterId: null }
    });

    useEffect(() => {
        let didCancel = false;

        async function fetchData() {
            dispatch({ type: REQUEST });
            try {
                const result = await apiModule.user.getTwitterRequestToken();

                if (!didCancel && !user.profile.twitter_id) {
                    dispatch({
                        type: FETCH,
                        twitterToken: result.token,
                        twitterId: result.id
                    });
                }
            } catch (error) {
                if (!didCancel) {
                    dispatch({ type: FAILURE });
                }
            }
        }

        fetchData();

        return () => {
            didCancel = true;
        };
    }, [user, apiModule]);

    return [state, setUser];
}
