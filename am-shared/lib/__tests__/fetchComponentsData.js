/* global test, expect */
import { routeConfig } from '../../../am-desktop/shared/routes';
import fetchComponentsData from '../fetchComponentsData';

const fakeStore = {
    dispatch() {},
    getState() {}
};

test('should return the correct match for a url with a query string', () => {
    const fakeReq = {
        path: '/trending-now',
        url: '/trending-now?playwire=1',
        originalUrl: '/trending-now?playwire=1'
    };
    const result = fetchComponentsData({
        req: fakeReq,
        routeConfig,
        store: fakeStore,
        returnMatch: true
    });

    expect(result.url).toBe('/trending-now');
});
