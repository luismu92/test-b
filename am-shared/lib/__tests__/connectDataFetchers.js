/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow, render } from 'enzyme';

import connectDataFetchers from '../connectDataFetchers';

Enzyme.configure({ adapter: new Adapter() });

test('should not error when excluding match and location as props', () => {
    const div = () => <div>test</div>; // eslint-disable-line
    const Component = connectDataFetchers(div);
    const props = {
        dispatch() {},
        match: {},
        location: {}
    };
    let wrapper = shallow(<Component {...props} />);

    // Make componentDidMount fire
    wrapper.update();

    wrapper = render(<Component {...props} />);

    expect(wrapper.text()).toEqual(expect.stringMatching(/test/));
});
