import React from 'react';
import PropTypes from 'prop-types';
import { parse } from 'query-string';

export default function connectDataFetchers(Component, actionCreators = []) {
    return class DataFetchersWrapper extends React.Component {
        static displayName = `connectDataFetchers(${Component.displayName ||
            Component.name})`;

        static propTypes = {
            dispatch: PropTypes.func.isRequired,
            match: PropTypes.object.isRequired,
            location: PropTypes.object.isRequired
        };

        // eslint-disable-next-line react/sort-comp
        static fetchData(dispatch, params = {}, query = {}, allProps = {}) {
            const promises = actionCreators
                .reduce((acc, actionCreator) => {
                    let action;

                    // If using the dispatch signature, we can assume this
                    // variation is using dispatch manually, in this case
                    // maybe to perform async operations in serial
                    if (actionCreator.length === 4) {
                        const actionOrPromise = actionCreator(
                            params,
                            query,
                            allProps,
                            dispatch
                        );

                        if (typeof actionOrPromise.then === 'function') {
                            acc = acc.concat(actionOrPromise);
                            return acc;
                        }

                        action = actionOrPromise;
                    } else {
                        action = actionCreator(params, query, allProps);
                    }

                    if (!action) {
                        return acc;
                    }

                    let actions = action;

                    if (!Array.isArray(action)) {
                        actions = [action];
                    }

                    acc = acc.concat(actions.map(dispatch));
                    return acc;
                }, [])
                .filter(Boolean);

            return Promise.all(promises);
        }

        componentDidMount() {
            // Update: May 3rd, 2017
            // We are ignoring rendering the full page for users on the server
            // because the API requests required to fetch the full page
            // take too long and make the TTFB super high
            //
            // Don't fire request to fetch data on initial load since
            // it will have already been fetched via the server on page load
            // if (!window.__clientHasBeenRendered) {
            //    // Commenting out this return will cause the client to make the same
            //    // fetches that happen on the server for the initial page load
            //     return;
            // }

            // Crawler's get the full html with fetched data on page load
            if (window.__IS_CRAWLER__ && !window.__clientHasBeenRendered) {
                return;
            }

            const { dispatch } = this.props;
            let { match, location } = this.props;

            if (!match || !location) {
                console.warn(
                    "Without match or location passed as props to a component wrapped with connectDataFetchers, you can expect things to go wrong if you're doing anything in the array of action dispatchers with `params` or `query`."
                );
                match = { params: {} };
                location = { search: '' };
            }

            const query = parse(location.search) || {};

            DataFetchersWrapper.fetchData(
                dispatch,
                match.params,
                query,
                this.props
            );
        }

        render() {
            return (
                <Component {...this.props} actionCreators={actionCreators} />
            );
        }
    };
}
