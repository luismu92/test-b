import { matchPath } from 'react-router';

export default function fetchComponentsData({
    req,
    routeConfig,
    store,
    returnMatch = false
}) {
    let fetchPromise = Promise.resolve([]);
    let matchedRoute;

    routeConfig.some((route) => {
        // Ignore redirect routes
        if (route.from && route.to) {
            return false;
        }

        const match = matchPath(req.path, route);

        if (!match) {
            return false;
        }

        if (returnMatch) {
            matchedRoute = match;
            return true;
        }

        const renderPromise = route.render({ match, isServer: true });

        // Redirect routes wont have a promise returned
        if (typeof renderPromise.then !== 'function') {
            return true;
        }

        fetchPromise = renderPromise.then(({ Component, props }) => {
            // Check fetchData on actual component
            let fetchData = Component.fetchData;

            // Check fetchData on WrappedComponent provided by connectDataFetchers hoc
            if (
                Component.WrappedComponent &&
                Component.WrappedComponent.fetchData
            ) {
                fetchData = Component.WrappedComponent.fetchData;
            }

            if (fetchData) {
                return fetchData(store.dispatch, match.params, req.query, {
                    route: props.route || {},
                    ...store.getState()
                });
            }

            return [];
        });

        return true;
    });

    if (returnMatch) {
        return matchedRoute;
    }

    return fetchPromise;
}
