import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './MusicActionButton.module.scss';

export default function MusicActionButton({
    onClick,
    buttonProps,
    type,
    icon,
    label,
    variant,
    isActive,
    style
}) {
    const iconClass = classnames(styles.icon, {
        [styles[type]]: type
    });

    const klass = classnames(styles.button, {
        [styles[variant]]: variant,
        [styles.active]: isActive
    });

    return (
        <button
            onClick={onClick}
            className={klass}
            style={style || {}}
            {...buttonProps}
        >
            <span className={iconClass}>{icon}</span>
            <span className={styles.label}>{label}</span>
        </button>
    );
}

MusicActionButton.propTypes = {
    onClick: PropTypes.func,
    buttonProps: PropTypes.object,
    type: PropTypes.string,
    icon: PropTypes.element,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    variant: PropTypes.string,
    isActive: PropTypes.bool,
    style: PropTypes.object
};

MusicActionButton.defaultProps = {
    variant: 'mobile'
};
