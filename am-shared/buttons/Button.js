// TODO
// Make desktop and mobile use this component, rather than site specific versions

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './Button.module.scss';

function Button({ width, height, style, href, icon, text, props }) {
    let tag = 'button';

    if (href) {
        tag = Link;
    }

    const buttonProps = {
        className: styles.button,
        style: style || {},
        ...props
    };

    if (width) {
        buttonProps.style.width = `${width}px`;
    }

    if (height) {
        buttonProps.style.height = `${height}px`;
    }

    if (href) {
        buttonProps.to = href;
    }

    let buttonIcon;

    if (icon) {
        buttonIcon = <span className={styles.icon}>{icon}</span>;
    }

    return React.createElement(
        tag,
        buttonProps,
        <Fragment>
            {buttonIcon}
            {text}
        </Fragment>
    );
}

Button.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    href: PropTypes.string,
    icon: PropTypes.element,
    text: PropTypes.string,
    style: PropTypes.object,
    props: PropTypes.object
};

export default Button;
