import React from 'react';
import PropTypes from 'prop-types';

import styles from './LinkButton.module.scss';

export default function LinkButton({ text, props }) {
    return (
        <button className={styles.button} {...props}>
            {text}
        </button>
    );
}

LinkButton.propTypes = {
    text: PropTypes.string,
    props: PropTypes.object
};
