import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import FacebookIcon from 'icons/facebook-letter-logo';
import TwitterIcon from 'icons/twitter-logo-new';

import baseStyles from './Button.module.scss';
import variantStyles from './SocialShareButton.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

function SocialShareButton({ network, url, shareText, shareImage, hideLabel }) {
    if (!url) {
        return null;
    }

    let shareUrl;

    if (network === SocialShareButton.NETWORK_TWITTER) {
        shareUrl = `https://twitter.com/share?url=${url}&text=${shareText}`;
    }

    if (network === SocialShareButton.NETWORK_FACEBOOK) {
        let image = '';

        if (shareImage) {
            image = `&picture=${shareImage}`;
        }
        shareUrl = `https://www.facebook.com/dialog/feed?app_id=${
            process.env.FACEBOOK_APP_ID
        }&link=${url}${image}&name=Now%20Playing&caption=%20&description=${shareText}`;
    }

    const icon =
        network === SocialShareButton.NETWORK_TWITTER ? (
            <TwitterIcon className={styles.base.icon} />
        ) : (
            <FacebookIcon className={styles.base.icon} />
        );

    const klass = classnames([styles.base.button, styles.variant.button], {
        [styles.variant.twitter]: network === SocialShareButton.NETWORK_TWITTER,
        [styles.variant.facebook]:
            network === SocialShareButton.NETWORK_FACEBOOK
    });

    let label;

    if (!hideLabel) {
        label = `Share on ${network}`;
    }

    return (
        <a
            href={shareUrl}
            className={klass}
            target="_blank"
            rel="nofollow noopener"
            data-social-type={network}
        >
            {icon} {label}
        </a>
    );
}

SocialShareButton.propTypes = {
    network: PropTypes.string,
    url: PropTypes.string.isRequired,
    shareText: PropTypes.string,
    shareImage: PropTypes.string,
    hideLabel: PropTypes.bool
};

SocialShareButton.NETWORK_TWITTER = 'twitter';
SocialShareButton.NETWORK_FACEBOOK = 'facebook';

export default SocialShareButton;
