import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import DotsLoader from '../components/loaders/DotsLoader';

import styles from './FollowButton.module.scss';

export default function FollowButton({
    artist,
    onClick,
    currentUser,
    loading,
    showCount
}) {
    const handleFollowClick = () => {
        onClick(true, artist);
    };

    const handleUnfollowClick = () => {
        onClick(false, artist);
    };

    const following =
        currentUser.profile &&
        currentUser.profile.following &&
        currentUser.profile.following.indexOf(artist.id) !== -1;

    const fn = following ? handleUnfollowClick : handleFollowClick;

    const isSameUser =
        currentUser.isLoggedIn && currentUser.profile.id === artist.id;

    const klass = classnames(styles.button, {
        [styles.follow]: !following,
        [styles.following]: following,
        [styles.loading]: loading
    });

    let inner = 'Follow';

    if (following) {
        inner = (
            <span className={styles.text}>
                <span className={styles.rollover}>Following</span>
                <span className={styles.rollover} aria-hidden="true">
                    Unfollow
                </span>
            </span>
        );
    }

    if (loading) {
        inner = <DotsLoader size={3} className={styles.dots} />;
    }

    let count;
    if (showCount) {
        const countNumber = artist.followers_count || 0;

        count = (
            <span className={styles.count}>{countNumber.toLocaleString()}</span>
        );
    }

    const prefix = following ? 'Unfollow' : 'Follow';
    const label = `${prefix} ${artist.name} on Audiomack`;

    return (
        <Fragment>
            <button
                className={klass}
                onClick={fn}
                disabled={isSameUser}
                aria-label={label}
            >
                {inner}
            </button>
            {count}
        </Fragment>
    );
}

FollowButton.defaultProps = {
    onClick() {}
};

FollowButton.propTypes = {
    artist: PropTypes.object,
    onClick: PropTypes.func,
    currentUser: PropTypes.object,
    loading: PropTypes.bool,
    showCount: PropTypes.bool
};
