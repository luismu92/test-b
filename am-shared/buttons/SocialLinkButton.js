import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';

import {
    linkNetwork,
    unlinkNetwork,
    getTwitterRequestToken
} from 'redux/modules/user';
import {
    SOCIAL_NETWORK_LINK_TWITTER,
    SOCIAL_NETWORK_LINK_FACEBOOK,
    SOCIAL_NETWORK_LINK_INSTAGRAM
} from 'constants/user/social';

import InstagramIcon from 'icons/instagram';
import TwitterIcon from 'icons/twitter-logo-new';
import FacebookIcon from 'icons/facebook-letter-logo';

import styles from './SocialLinkButton.module.scss';

export default function SocialLinkButton({
    network,
    className,
    onSocialLink,
    onSocialLinkError
}) {
    const [twitterData, setTwitterData] = useState({
        twitterToken: null,
        twitterSecret: null
    });
    const currentUser = useSelector((state) => state.currentUser);
    const dispatch = useDispatch();

    /////////////
    // Effects //
    /////////////

    useEffect(
        function twitterTokenRequestEffect() {
            async function setTwitterToken() {
                try {
                    if (!currentUser.profile.twitter_id) {
                        const result = await dispatch(getTwitterRequestToken());

                        setTwitterData({
                            twitterToken: result.resolved.token,
                            twitterSecret: result.resolved.secret
                        });
                    }
                } catch (err) {
                    console.log(err);
                }
            }

            if (network === SOCIAL_NETWORK_LINK_TWITTER) {
                setTwitterToken();
            }
        },
        [network, currentUser, dispatch]
    );

    ////////////////////
    // Event handlers //
    ////////////////////

    async function handleSocialLink(e) {
        const button = e.currentTarget;
        const buttonNetwork = button.getAttribute('data-linkage');
        const linkAction = button.getAttribute('data-action');

        try {
            let action;

            switch (buttonNetwork) {
                case SOCIAL_NETWORK_LINK_FACEBOOK:
                    if (linkAction === 'unlink') {
                        action = await dispatch(unlinkNetwork(buttonNetwork));
                        return;
                    }

                    action = await dispatch(linkNetwork(buttonNetwork));

                    break;

                case SOCIAL_NETWORK_LINK_TWITTER: {
                    if (linkAction === 'unlink') {
                        action = await dispatch(unlinkNetwork(buttonNetwork));
                        return;
                    }

                    action = await dispatch(
                        linkNetwork(buttonNetwork, twitterData)
                    );

                    // Reset token/secret in case there was an error and it
                    // needs to be clicked again
                    dispatch(getTwitterRequestToken())
                        .then((result) => {
                            setTwitterData({
                                twitterToken: result.resolved.token,
                                twitterSecret: result.resolved.secret
                            });
                            return;
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                    break;
                }

                case SOCIAL_NETWORK_LINK_INSTAGRAM:
                    if (linkAction === 'unlink') {
                        action = await dispatch(unlinkNetwork(buttonNetwork));
                        return;
                    }
                    action = await dispatch(linkNetwork(buttonNetwork));
                    break;

                default:
                    throw new Error(
                        `Unknown network value on button: ${buttonNetwork}`
                    );
            }

            onSocialLink(linkAction, buttonNetwork, action.resolved);
        } catch (err) {
            onSocialLinkError(err, linkAction, buttonNetwork);
        }
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    function renderIcon() {
        switch (network) {
            case SOCIAL_NETWORK_LINK_INSTAGRAM:
                return <InstagramIcon />;

            case SOCIAL_NETWORK_LINK_TWITTER:
                return <TwitterIcon />;

            case SOCIAL_NETWORK_LINK_FACEBOOK:
                return <FacebookIcon />;

            default:
                return null;
        }
    }

    const klass = classnames(styles.button, styles[network], {
        [className]: className
    });
    const unVerified =
        currentUser.isLoggedIn && !!currentUser.profile[`${network}_id`];
    const alreadyLinked =
        currentUser.isLoggedIn && !!currentUser.profile[`${network}_id`];

    let text = `Link your ${network} account`;
    const dataAction = 'link';

    if (alreadyLinked) {
        text = `${network} linked`;
        // dataAction = 'unlink';
    }

    return (
        <button
            className={klass}
            type="button"
            data-action={dataAction}
            data-linkage={network}
            onClick={handleSocialLink}
            disabled={
                !currentUser.isLoggedIn || unVerified || alreadyLinked
                    ? 'disabled'
                    : ''
            }
        >
            <span className={styles.icon}>{renderIcon()}</span>
            <span className={styles.text}>{text}</span>
        </button>
    );
}

SocialLinkButton.propTypes = {
    network: PropTypes.oneOf([
        SOCIAL_NETWORK_LINK_TWITTER,
        SOCIAL_NETWORK_LINK_FACEBOOK,
        SOCIAL_NETWORK_LINK_INSTAGRAM
    ]),
    className: PropTypes.string,
    onSocialLink: PropTypes.func,
    onSocialLinkError: PropTypes.func
};

SocialLinkButton.defaultProps = {
    onSocialLink() {},
    onSocialLinkError() {}
};
