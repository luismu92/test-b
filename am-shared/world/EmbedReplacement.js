import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    renderFeaturingLinks,
    getTwitterShareLink,
    getFacebookShareLink,
    getArtistName,
    getFeaturing,
    getDynamicImageProps
} from 'utils/index';
import Avatar from 'components/Avatar';

import PlayIcon from 'icons/play';
import QueueIcon from 'icons/queue';
import FacebookIcon from 'icons/facebook-letter-logo';
import TwitterIcon from 'icons/twitter-logo-new';

import styles from './EmbedReplacement.module.scss';

export default class EmbedReplacement extends Component {
    static propTypes = {
        musicItem: PropTypes.object,
        onPlayNowClick: PropTypes.func,
        onAddToQueueClick: PropTypes.func
    };

    componentDidMount() {}

    componentWillUnmount() {}

    ////////////////////
    // Helper methods //
    ////////////////////

    renderFeaturing(featuring) {
        if (!featuring) {
            return null;
        }

        return (
            <strong className={styles.featuring}>
                {renderFeaturingLinks(featuring)}
            </strong>
        );
    }

    render() {
        const { musicItem } = this.props;

        if (!musicItem) {
            return null;
        }

        const avatarSize = 170;
        const [artwork, srcSet] = getDynamicImageProps(
            musicItem.image_base || musicItem.image,
            { size: avatarSize }
        );

        const artistSlug =
            musicItem.type === 'playlist'
                ? musicItem.artist.url_slug
                : musicItem.uploader.url_slug;
        const href = `/${musicItem.type}/${artistSlug}/${musicItem.url_slug}`;

        return (
            <div className={styles.container}>
                <Avatar
                    image={artwork}
                    srcSet={srcSet}
                    className={styles.avatar}
                    size={avatarSize}
                    square
                />
                <div className={styles.content}>
                    <Link className={styles.musicLink} to={href}>
                        <h4 className={styles.heading}>
                            <span>{getArtistName(musicItem)}</span>
                            <strong>{musicItem.title}</strong>
                        </h4>
                    </Link>
                    {this.renderFeaturing(getFeaturing(musicItem.featuring))}

                    <div className={styles.buttons}>
                        <div className={styles.pillButtons}>
                            <button
                                className={styles.button}
                                onClick={this.props.onPlayNowClick}
                                aria-label={`Play ${musicItem.type}`}
                                data-play-start
                            >
                                <PlayIcon className={styles.icon} />
                                Play Now
                            </button>
                            <button
                                className={styles.button}
                                aria-label={`Add ${musicItem.type} to queue"`}
                                onClick={this.props.onAddToQueueClick}
                                data-title={musicItem.title}
                            >
                                <QueueIcon className={styles.icon} />
                                Add to Queue
                            </button>
                        </div>
                        <div className={styles.iconButtons}>
                            <a
                                href={getTwitterShareLink(
                                    process.env.AM_URL,
                                    musicItem
                                )}
                                target="_blank"
                                rel="nofollow noopener"
                                className={classnames(
                                    styles.socialButton,
                                    styles.twitter
                                )}
                                aria-label="Share on Twitter"
                            >
                                <TwitterIcon className={styles.socialIcon} />
                            </a>

                            <a
                                href={getFacebookShareLink(
                                    process.env.AM_URL,
                                    musicItem,
                                    process.env.FACEBOOK_APP_ID
                                )}
                                target="_blank"
                                rel="nofollow noopener"
                                className={classnames(
                                    styles.socialButton,
                                    styles.facebook
                                )}
                                aria-label="Share on Facebook"
                            >
                                <FacebookIcon className={styles.socialIcon} />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
