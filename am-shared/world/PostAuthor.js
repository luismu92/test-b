import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import moment from 'moment';
import { DEFAULT_DATE_FORMAT } from 'constants/index';
import { getDynamicImageProps } from 'utils/index';

import styles from './PostAuthor.module.scss';

export default function PostAuthor({
    post,
    worldPostArtists,
    style,
    variant = ''
}) {
    const author = post.primary_author || {};
    const artistSlug = author.audiomack;

    let authorLink = author.name;

    if (artistSlug) {
        authorLink = <Link to={`/artist/${artistSlug}`}>{author.name}</Link>;
    }

    let image =
        author.profile_image ||
        'https://assets.audiomack.com/default-artist-image.png';

    if (worldPostArtists.artists[artistSlug]) {
        const artist = worldPostArtists.artists[artistSlug];
        image = artist.image_base || artist.image;
    }

    const [artwork, srcSet] = getDynamicImageProps(image, { size: 44 });

    const blockStyle = style || {};

    const klass = classnames(styles.authorBlock, {
        [styles.mobile]: variant === 'mobile'
    });

    return (
        <div className={klass} style={blockStyle}>
            <img
                className={styles.authorImage}
                src={artwork}
                srcSet={srcSet}
                alt=""
            />
            <div className={styles.authorInfo}>
                <p className={styles.authorName}>{authorLink}</p>
                <time className={styles.date} dateTime={post.published_at}>
                    {moment(post.published_at).format(DEFAULT_DATE_FORMAT)}
                </time>
            </div>
        </div>
    );
}

PostAuthor.propTypes = {
    post: PropTypes.object,
    worldPostArtists: PropTypes.object,
    style: PropTypes.object,
    variant: PropTypes.string
};
