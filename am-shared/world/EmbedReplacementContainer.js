import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import analytics from 'utils/analytics';
import api from 'api/index';

import { addQueueItem, next as playerNext, play } from 'redux/modules/player';

import EmbedReplacement from './EmbedReplacement';

class EmbedReplacementContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        player: PropTypes.object,
        type: PropTypes.string,
        artist: PropTypes.string,
        slug: PropTypes.string,
        onAddedToQueue: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            musicItem: null
        };
    }

    componentDidMount() {
        const { type, artist, slug } = this.props;

        this.getMusicItem(type, artist, slug);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handlePlayNowClick = () => {
        const { dispatch, player } = this.props;

        // Instead of erasing current queue for these embeds, we will just
        // add it and call next
        dispatch(
            addQueueItem(this.state.musicItem, {
                atIndex: player.queueIndex + 1
            })
        );
        dispatch(playerNext());
        dispatch(play());
    };

    handleAddToQueueClick = (e) => {
        const { dispatch, onAddedToQueue } = this.props;

        dispatch(addQueueItem(this.state.musicItem));
        onAddedToQueue(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getMusicItem(type, artist, slug) {
        // No need to make a partial state in the global redux store for this
        // so I'm calling here directly
        let promise;

        switch (type) {
            case 'song':
                promise = api.music.song(artist, slug);
                break;

            case 'album':
                promise = api.music.album(artist, slug);
                break;

            case 'playlist':
                promise = api.playlist.getBySlugs(artist, slug);
                break;

            default:
                promise = Promise.reject(
                    `Unknown type provided to embed replacement: ${type}`
                );
                break;
        }

        promise
            .then(({ results }) => {
                this.setState({
                    musicItem: results
                });
                return;
            })
            .catch((err) => {
                analytics.error(err);
            });
    }

    render() {
        return (
            <EmbedReplacement
                onPlayNowClick={this.handlePlayNowClick}
                onAddToQueueClick={this.handleAddToQueueClick}
                musicItem={this.state.musicItem}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player
    };
}

export default connect(mapStateToProps)(EmbedReplacementContainer);
