import { cleanParameters, buildQueryString } from 'utils/index';

export default function api(getConfig, oauthRequest) {
    return {
        revenue(
            token,
            secret,
            {
                month = new Date().getMonth() + 1,
                year = new Date().getFullYear(),
                type = 'monthly',
                csv = false
            } = {}
        ) {
            const url = `${
                getConfig().baseUrl
            }/label/monetization/${type}-revenue`;
            const removeFalsyValues = true;
            const headers = {};
            let getRawResponse = false;

            if (csv) {
                headers.Accept = 'text/csv';
                getRawResponse = true;
            }

            return oauthRequest({
                url,
                body: cleanParameters(
                    {
                        month: month,
                        year: year
                    },
                    removeFalsyValues
                ),
                headers,
                token,
                secret,
                getRawResponse
            });
        },

        summary(
            token,
            secret,
            {
                month = new Date().getMonth() + 1,
                year = new Date().getFullYear(),
                type = 'monthly'
            } = {}
        ) {
            const url = `${
                getConfig().baseUrl
            }/label/monetization/${type}-summary`;
            const removeFalsyValues = true;

            return oauthRequest({
                url,
                body: cleanParameters(
                    {
                        month: month,
                        year: year
                    },
                    removeFalsyValues
                ),
                token,
                secret
            });
        },

        updateLabel(token, secret, options) {
            const body = {
                type: (options.type || '').trim(),
                first_name: (options.first_name || '').trim(),
                last_name: (options.last_name || '').trim(),
                company_name: (options.company_name || '').trim(),
                agreement_confirmed: new Date().toISOString()
            };

            if (options.code) {
                body.code = options.code;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/label`,
                method: 'PATCH',
                body,
                token,
                secret
            });
        },

        labelSignup(token, secret, options) {
            const body = {
                type: (options.type || '').trim(),
                first_name: (options.first_name || '').trim(),
                last_name: (options.last_name || '').trim(),
                company_name: (options.company_name || '').trim(),
                agreement_confirmed: new Date().toISOString()
            };

            if (options.code) {
                body.code = options.code;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/label`,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        associated(
            token,
            secret,
            {
                show = 'all',
                page = 1,
                limit = 20,
                query = null,
                incompletes = null
            }
        ) {
            const queryString = buildQueryString({
                limit,
                show,
                q: query,
                incompletes
            });
            const url = `${
                getConfig().baseUrl
            }/label/associated/page/${page}?${queryString}`;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        paymentInfo(token, secret) {
            const url = `${getConfig().baseUrl}/user/payment-info`;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        validateCode(code) {
            const url = `${getConfig().baseUrl}/label/invite/${code}`;

            return oauthRequest({
                url
            });
        },

        associatedArtists(token, secret, options = {}) {
            const { page, limit } = options;

            const url = `${
                getConfig().baseUrl
            }/label/associated-artists/page/${page}?limit=${limit}`;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        createNewArtist(token, secret, name, options) {
            const body = {
                name: name.trim()
            };

            if (options.bio) {
                body.bio = options.bio;
            }

            if (options.twitterHandle) {
                body.twitter_import = 'yes'; // eslint-disable-line
                body.twitter = options.twitterHandle;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/artist`,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        sendClaimProfileEmail(token, secret, artistId, email) {
            const body = {
                email
            };

            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/label/artist/${artistId}/sendclaimemail`,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        generateCode(token, secret, prefix) {
            const body = {
                prefix
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/amp-codes`,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        getAmpCodes(token, secret, page) {
            const url = `${getConfig().baseUrl}/amp-codes?page=${page}`;

            return oauthRequest({
                url,
                token,
                secret
            });
        }
    };
}
