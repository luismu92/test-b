import { parseApiObject } from 'utils/index';

export default function api(getConfig, oauthRequest) {
    return {
        loginAsUser(id, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/admin_token`,
                method: 'POST',
                body: {
                    user_id: id
                },
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    ...parseApiObject(data)
                };
            });
        },

        bumpMusic(musicId, bumpGenre = '', token, secret) {
            const body = {
                id: musicId,
                genre: bumpGenre
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/trending/bump`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        bumpMusicOneSpot(musicId, bumpGenre = '', token, secret) {
            const body = {
                id: musicId,
                genre: bumpGenre
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/trending/up`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        suspend(id, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/suspend`,
                method: 'PUT',
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    ...parseApiObject(data)
                };
            });
        },

        unsuspend(id, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/suspend`,
                method: 'DELETE',
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    ...parseApiObject(data)
                };
            });
        },

        takedownMusic(musicId, token, secret) {
            const body = {
                id: musicId,
                operation: 'takedown'
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/songs/toggle-dmca`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        restoreMusic(musicId, token, secret) {
            const body = {
                id: musicId,
                operation: 'restore'
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/songs/toggle-dmca`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        censorArtwork(musicId, token, secret) {
            const body = {
                id: musicId
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/songs/censor-artwork`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        trendMusic(musicId, genre, trend, token, secret) {
            const body = {
                id: musicId,
                trending: trend ? 'yes' : 'no',
                genre
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/trending/set`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        excludeStats(musicId, operation, token, secret) {
            const body = {
                id: musicId,
                operation: operation
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/songs/exclude`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        clearStats(musicId, token, secret) {
            const body = {
                id: musicId
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/songs/clear`,
                method: 'POST',
                body: body,
                token: token,
                secret: secret
            });
        },

        verifyArtist(artistId, value, token, secret) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${artistId}/toggle-verified`,
                method: 'POST',
                body: {
                    value
                },
                token,
                secret
            });
        },

        verifyArtistEmail(artistId, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${artistId}/verify-email`,
                method: 'POST',
                token,
                secret
            });
        },

        getMusicSourceUrl(musicId, isStream = false, token, secret) {
            const fileType = isStream ? 'stream' : 'source';

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${musicId}/${fileType}`,
                method: 'GET',
                token,
                secret
            });
        },

        getMusicRecognition(musicId, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${musicId}/recognition`,
                method: 'GET',
                token,
                secret
            });
        },

        refreshMusicRecognition(musicId, token, secret) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/music/${musicId}/recognition-refresh`,
                method: 'GET',
                token,
                secret
            });
        },

        repairMusicUrl(musicId, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${musicId}/repair-url`,
                method: 'POST',
                token,
                secret
            });
        },

        clearUploadErrors(musicId, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${musicId}/clear-error`,
                method: 'POST',
                token,
                secret
            });
        }
    };
}
