import { parseApiObject } from 'utils/index';
import {
    LIMIT_PER_PAGE,
    LIMIT_PER_PAGE_REPORTS,
    ORDER_BY_VOTE,
    KIND_MANAGEMENT_FLAGGED,
    KIND_MANAGEMENT_RECENT,
    KIND_MANAGEMENT_USER
} from 'constants/comment';

function getBaseUrl(getConfig) {
    // This is for temp local
    // baseUrl = 'http://localhost:3331/v1/comments';
    return `${getConfig().baseUrl}/comments`;
}

export default function api(getConfig, oauthRequest) {
    return {
        // eslint-disable-next-line max-params
        get(
            kind,
            id,
            orderBy = ORDER_BY_VOTE,
            page = 1,
            token,
            secret,
            singleCommentUuid,
            singleCommentThread
        ) {
            const limit =
                kind === KIND_MANAGEMENT_FLAGGED
                    ? LIMIT_PER_PAGE_REPORTS
                    : LIMIT_PER_PAGE;

            const offset = limit * (page - 1);
            let url = `${getBaseUrl(
                getConfig
            )}?kind=${kind}&id=${id}&order_by=${orderBy}&limit=${limit}&offset=${offset}`;

            if (kind === KIND_MANAGEMENT_FLAGGED) {
                url = `${getBaseUrl(
                    getConfig
                )}/reports?limit=${limit}&offset=${offset}&order_by=${orderBy}`;
            }

            if (kind === KIND_MANAGEMENT_RECENT) {
                url = `${getBaseUrl(getConfig)}/recent`;
            }

            if (kind === KIND_MANAGEMENT_USER) {
                url = `${getBaseUrl(getConfig)}/user/${id}`;
            }

            if (typeof singleCommentUuid !== 'undefined') {
                const thread =
                    typeof singleCommentThread !== 'undefined'
                        ? `&thread=${singleCommentThread}`
                        : '';
                url = `${getBaseUrl(
                    getConfig
                )}/single?kind=${kind}&id=${id}&uuid=${singleCommentUuid}${thread}`;
            }

            return oauthRequest({
                url,
                method: 'GET',
                token,
                secret
            }).then((data) => {
                return {
                    total: data.total ? data.total : 0,
                    result: data.result.map(parseApiObject)
                };
            });
        },

        post(comment, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}`,
                method: 'POST',
                body: comment, // { kind, id, thread, content }
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }, // Must indicate headers as json otherwise the oauthrequest will be messed up
                token,
                secret
            }).then((data) => {
                return data.result;
            });
        },

        delete(comment, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}`,
                method: 'DELETE',
                body: comment, // { kind, id, thread, uuid }
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }, // Must indicate headers as json otherwise the oauthrequest will be messed up
                token,
                secret
            });
        },

        report(comment, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}/reports`,
                method: 'POST',
                body: comment, // { kind, id, thread, uuid }
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }, // Must indicate headers as json otherwise the oauthrequest will be messed up
                token,
                secret
            });
        },

        unflag(comment, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}/reports`,
                method: 'DELETE',
                body: comment, // { kind, id, thread, uuid }
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }, // Must indicate headers as json otherwise the oauthrequest will be messed up
                token,
                secret
            });
        },

        ban(user, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}/user/${user}`,
                method: 'DELETE',
                token,
                secret
            });
        },

        postVotes(votes, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}/votes`,
                method: 'POST',
                body: votes, // { "kind": "song", "id": "1", "votes": [] }
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }, // Must indicate headers as json otherwise the oauthrequest will be messed up
                token,
                secret
            });
        },

        getUserVoteStatus(kind, id, token, secret) {
            return oauthRequest({
                url: `${getBaseUrl(getConfig)}/votes?kind=${kind}&id=${id}`,
                method: 'GET',
                token,
                secret
            }).then((data) => {
                return data.result.map(parseApiObject);
            });
        }
    };
}
