/**
 * Artist resource of the API client
 *
 * @param  {String} baseUrl base url of the resource
 * @return {Promise}
 */

import { parseApiObject, buildQueryString } from 'utils/index';

export default function api(getConfig, oauthRequest) {
    return {
        get(slug, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: parseApiObject(data.results)
                };
            });
        },

        getById(id, token, secret) {
            let ids = id;

            if (Array.isArray(id)) {
                ids = id.join(',');
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/artist?id=${ids}`,
                token,
                secret
            }).then((data) => {
                if (Array.isArray(id)) {
                    return {
                        ...data,
                        results: data.results.map(parseApiObject)
                    };
                }

                return {
                    ...data,
                    results: parseApiObject(data.results)
                };
            });
        },

        uploads(slug, page = 1, limit = 20) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${slug}/uploads/page/${page}?limit=${limit}`
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        podcasts(slug) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}/podcasts`
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        favorites(slug, page = 1, limit = 20, show = 'all') {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${slug}/favorites/page/${page}?limit=${limit}&show=${show}`
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        playlists(slug, page = 1, limit = 20) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${slug}/playlists/page/${page}?limit=${limit}`
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        following(slug, { page = 1, limit = 20, token, secret } = {}) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${slug}/following?page=${page}&limit=${limit}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        followers(slug, { page = 1, limit = 20, token, secret } = {}) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${slug}/follows?page=${page}&limit=${limit}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        feed(
            slug,
            { page = 1, limit = 20, onlyUploads = false, token, secret } = {}
        ) {
            const qs = buildQueryString({
                limit,
                only_uploads: onlyUploads
            });

            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/artist/${slug}/feed/page/${page}?${qs}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        follow(slug, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}/follow`,
                method: 'PUT',
                token,
                secret
            });
        },

        unfollow(slug, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}/follow`,
                method: 'DELETE',
                token,
                secret
            });
        },

        claim(id, options) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${id}/claim`,
                method: 'PUT',
                body: options
            });
        },

        validateArtist(body, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/validate/artist`,
                method: 'POST',
                body: body,
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                token,
                secret
            });
        },

        getPinned(slug) {
            const url = `${getConfig().baseUrl}/artist/${slug}/pinned`;

            return oauthRequest({
                url
            }).then((data) => {
                return data.map(parseApiObject);
            });
        },

        addPinned(slug, entities = [], token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}/pinned`,
                method: 'POST',
                body: { entities: JSON.stringify(entities) },
                token,
                secret
            }).then((data) => {
                return data.map(parseApiObject);
            });
        },

        savePinned(slug, entities = [], token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}/pinned`,
                method: 'PUT',
                body: { entities: JSON.stringify(entities) },
                token,
                secret
            }).then((data) => {
                return data.map(parseApiObject);
            });
        },

        deletePinned(slug, entities = [], token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${slug}/pinned`,
                method: 'DELETE',
                body: { entities: JSON.stringify(entities) },
                token,
                secret
            }).then((data) => {
                return data.map(parseApiObject);
            });
        }
    };
}
