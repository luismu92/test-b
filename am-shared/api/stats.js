/**
 * Stats resource of the API client
 *
 * @param  {Function} getConfig function to get the current config
 * @return {Promise}
 */

import { parseApiObject, cleanParameters } from 'utils/index';

export default function api(getConfig, oauthRequest) {
    return {
        overall(show, timespan, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/stats`,
                body: cleanParameters({
                    format: 'json',
                    show,
                    timespan
                }),
                token,
                secret
            });
        },

        snapshot(id, start, end, token, secret) {
            const url = `${
                getConfig().baseUrl
            }/analytics/artists/${id}/account?startDate=${start}&endDate=${end}`;
            return oauthRequest({
                url,
                token,
                secret
            });
        },

        // eslint-disable-next-line
        daily(id, attribute, start, end, token, secret) {
            const url = `${
                getConfig().baseUrl
            }/analytics/artists/${id}/account-daily/${attribute}?startDate=${start}&endDate=${end}`;
            return oauthRequest({
                url,
                token,
                secret
            });
        },

        artistPlaySources(token, secret, artistId, options = {}) {
            const { startDate, endDate, limit } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/play-sources`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit
                }),
                token,
                secret
            });
        },

        music({ id, timespan, start, end, country, token, secret }) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/stats`,
                body: cleanParameters({
                    start,
                    end,
                    country,
                    timespan
                }),
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    music: parseApiObject(data.music),
                    timespan: parseInt(data.timespan, 10),
                    total_days: parseInt(data.total_days, 10)
                };
            });
        },

        artistCountryPlays(token, secret, artistId, options = {}) {
            const { startDate, endDate, limit } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/country-plays`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit
                }),
                token,
                secret
            });
        },

        artistCountryStats(token, secret, artistId, options = {}) {
            const { startDate, endDate, limit, page = 1 } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/countries`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        artistCityStats(token, secret, artistId, options = {}) {
            const { startDate, endDate, limit, page = 1 } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/cities`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        artistTopFans(token, secret, artistId, options = {}) {
            const { startDate, endDate, limit, page = 1 } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/top-fans`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        artistTopInfluencers(token, secret, artistId, options = {}) {
            const { startDate, endDate, limit, page = 1 } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/top-influencers`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        musicCountryPlays(token, secret, artistId, musicId, options = {}) {
            const { startDate, endDate, limit } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/country-plays`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit
                }),
                token,
                secret
            });
        },

        musicCountryStats(token, secret, artistId, musicId, options = {}) {
            const { startDate, endDate, limit, page = 1 } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/countries`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        musicCityStats(token, secret, artistId, musicId, options = {}) {
            const { startDate, endDate, limit, page = 1 } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/cities`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        musicSummary(token, secret, artistId, musicId, options = {}) {
            const { startDate, endDate, limit } = options;
            let ids;
            let url = `${getConfig().baseUrl}/analytics/artists/${artistId}`;
            let method = 'get';
            const headers = {};

            if (Array.isArray(musicId)) {
                ids = musicId;
                method = 'post';
                url += '/batch-contents/song';
                headers['Content-Type'] = 'application/json';
            } else {
                url += `/contents/${musicId}`;
            }

            return oauthRequest({
                url,
                method,
                headers,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit,
                    ids
                }),
                token,
                secret
            });
        },

        musicDaily(token, secret, artistId, musicId, options = {}) {
            const { startDate, endDate, limit, attribute = 'play' } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/daily/${attribute}`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit
                }),
                token,
                secret
            });
        },

        musicUrls(token, secret, artistId, musicId, options = {}) {
            const { limit } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/urls`,
                body: cleanParameters({
                    limit
                }),
                token,
                secret
            });
        },

        musicPlaySources(token, secret, artistId, musicId, options = {}) {
            const { startDate, endDate, limit } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/play-sources`,
                body: cleanParameters({
                    startDate,
                    endDate,
                    limit
                }),
                token,
                secret
            });
        },

        musicPlaylistAdds(token, secret, artistId, musicId, options = {}) {
            const { limit, page } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/playlists`,
                body: cleanParameters({
                    limit,
                    page
                }),
                token,
                secret
            });
        },

        musicPromoLinks(token, secret, artistId, musicId, options = {}) {
            const { limit } = options;
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/analytics/artists/${artistId}/contents/${musicId}/promos`,
                body: cleanParameters({
                    limit
                }),
                token,
                secret
            });
        }
    };
}
