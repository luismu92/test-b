/**
 * Playlist resource of the API client
 *
 * @param  {String} baseUrl base url of the resource
 * @return {Promise}
 */

import { allGenresMap } from '../constants/index';

import { parseApiObject, yesBool, cleanParameters } from 'utils/index';

export default function playlist(getConfig, oauthRequest) {
    return {
        create({ title, genre, ...rest }, token, secret) {
            const body = {};

            if (!title || !genre) {
                return Promise.reject(
                    'Must provide title and genre when creating a playlist'
                );
            }

            if (Object.keys(allGenresMap).indexOf(genre) === -1) {
                return Promise.reject(
                    `Genre must be one of the following: ${Object.keys(
                        allGenresMap
                    )
                        .filter(Boolean)
                        .join(', ')}`
                );
            }

            body.title = title;
            body.genre = genre;
            body.private = rest.private === 'yes' ? 'yes' : 'no';

            if (rest.id) {
                body.music_id = rest.id; // eslint-disable-line camelcase
            }

            if (rest.image) {
                body.image = rest.image;
            }

            if (rest.description) {
                body.description = rest.description;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist`,
                method: 'POST',
                body,
                token,
                secret
            }).then((data) => {
                return parseApiObject(data);
            });
        },

        edit(id, metadata, token, secret) {
            const requestKeys = [
                'title',
                'genre',
                'music_id',
                'private',
                'image',
                'image_banner',
                'description',
                'url_slug'
            ];
            const body = Object.keys(metadata).reduce((obj, key) => {
                if (requestKeys.indexOf(key) !== -1) {
                    obj[key] = metadata[key];

                    if (key === 'private') {
                        obj.private = yesBool(metadata.private) ? 'yes' : 'no';
                    }

                    // Just get the base64 part without the "data:image/jpeg;base64,"
                    if (
                        (key === 'image' || key === 'image_banner') &&
                        metadata[key]
                    ) {
                        if (metadata[key].includes(',')) {
                            obj[key] = metadata[key].split(',')[1];
                        }
                    }
                }
                return obj;
            }, {});

            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}`,
                method: 'PUT',
                body: cleanParameters(body),
                token,
                secret
            }).then((data) => {
                return {
                    ...parseApiObject(data)
                };
            });
        },

        delete(id, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}`,
                method: 'DELETE',
                token,
                secret
            });
        },

        privacy(id, isPrivate, token, secret) {
            const body = {
                private: isPrivate
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}`,
                method: 'PATCH',
                body: body,
                token,
                secret
            }).then((data) => {
                return {
                    ...parseApiObject(data)
                };
            });
        },

        addSong(id, songId, token, secret, options = {}) {
            let param = songId;

            if (Array.isArray(songId)) {
                param = songId.join(',');
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}/track`,
                method: 'POST',
                body: cleanParameters({
                    music_id: param,
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        deleteSong(playlistId, songId, token, secret) {
            let param = songId;

            if (Array.isArray(songId)) {
                param = songId.join(',');
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${playlistId}/track`,
                method: 'DELETE',
                body: {
                    music_id: param
                },
                token,
                secret
            });
        },

        tags(tagName, page = 1, limit) {
            let url = `${getConfig().baseUrl}/tags`;

            if (tagName) {
                url = `${url}/${encodeURIComponent(tagName)}`;
            }

            if (page) {
                url = `${url}/page/${page}`;
            }

            if (limit) {
                url = `${url}?${
                    !!tagName ? 'limit' : 'playlist_count'
                }=${limit}`;
            }

            return oauthRequest({
                url
            }).then((data) => {
                if (tagName) {
                    return {
                        ...data,
                        results: data.results.map(parseApiObject)
                    };
                }

                return {
                    ...data,
                    results: data.results.map((obj) => {
                        const title = Object.keys(obj)[0];
                        const list = obj[title] || [];
                        const parsed = {};

                        parsed[title] = list.map(parseApiObject);

                        return parsed;
                    })
                };
            });
        },

        playlistTags(tag, limit = 20, featured, page = 1) {
            let url = `${getConfig().baseUrl}/playlist/categories?page=${page}`;

            if (featured) {
                url = `${url}&featured=yes`;
            }

            if (tag) {
                url = `${url}&slug=${tag}`;
            }

            if (limit) {
                url = `${url}&limit=${limit}`;
            }

            return oauthRequest({
                url
            }).then((data) => {
                if (tag) {
                    return {
                        ...data,
                        results: data.results.playlists,
                        title: data.results.title
                    };
                }

                return data;
            });
        },

        get(id, page, limit) {
            let url = `${getConfig().baseUrl}/playlist/${id}`;

            if (page) {
                url = `${url}/page/${page}`;
            }

            if (limit) {
                url = `${url}?limit=${limit}`;
            }

            return oauthRequest({
                url
            }).then((data) => {
                const formatted = parseApiObject(data.results);

                return {
                    ...data,
                    results: {
                        ...formatted,
                        tracks: (formatted.tracks || []).map(parseApiObject)
                    }
                };
            });
        },

        getTags(playlistId = null, options = {}) {
            let url = `${getConfig().baseUrl}/playlist/tags`;

            if (playlistId) {
                url = `${url}?playlist_id=${playlistId}`;
            }

            const { token, secret } = options;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        setTags(playlistId, tags = [], options = {}) {
            if (!playlistId) {
                return Promise.reject(
                    'Must provide the playlistId when updating the tags'
                );
            }

            tags = JSON.stringify(tags);

            const body = {
                tags,
                playlist_id: playlistId
            };
            const url = `${getConfig().baseUrl}/playlist/addtags`;
            const { token, secret } = options;

            return oauthRequest({
                url,
                method: 'POST',
                body,
                token,
                secret
            }).then((data) => {
                return parseApiObject(data);
            });
        },

        trendPlaylist(playlistId, options = {}) {
            if (!playlistId) {
                return Promise.reject(
                    'Must provide the playlistId when updating the tags'
                );
            }

            const body = {
                id: playlistId
            };

            const url = `${getConfig().baseUrl}/playlist/settrending`;

            return oauthRequest({
                url,
                method: 'POST',
                body,
                token: options.token,
                secret: options.secret
            }).then((data) => {
                return parseApiObject(data);
            });
        },

        getTrendingStatus(playlistId, options = {}) {
            if (!playlistId) {
                return Promise.reject(
                    'Must provide the playlistId when updating the tags'
                );
            }

            const url = `${
                getConfig().baseUrl
            }/playlist/settrending?id=${playlistId}`;

            return oauthRequest({
                url,
                token: options.token,
                secret: options.secret
            });
        },

        id(id, token = null, secret = null) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}`,
                token,
                secret
            }).then((data) => {
                const formatted = parseApiObject(data.results);

                return {
                    ...data,
                    results: {
                        ...formatted,
                        tracks: (formatted.tracks || []).map(parseApiObject)
                    }
                };
            });
        },

        getBySlugs(artistSlug, playlistSlug, token = null, secret = null) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/playlist/${artistSlug}/${playlistSlug}`,
                token,
                secret
            }).then((data) => {
                const formatted = parseApiObject(data.results);

                return {
                    ...data,
                    results: {
                        ...formatted,
                        tracks: (formatted.tracks || []).map(parseApiObject)
                    }
                };
            });
        },

        favorite(id, token, secret, options = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}/favorite`,
                method: 'PUT',
                body: cleanParameters({
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        unfavorite(id, token, secret, options = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/${id}/favorite`,
                method: 'DELETE',
                body: cleanParameters({
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        getMixedPlaylists(ids, options, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/playlist/mixed?ids=${ids.join(
                    ','
                )}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        }
    };
}
