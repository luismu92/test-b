/**
 * Contact Form resource of the API client
 *
 * @param  {String} baseUrl base url of the resource
 * @return {Promise}
 */

export default function email(getConfig, oauthRequest) {
    return {
        contact({
            type,
            emailAddress,
            description,
            issue = '',
            url = '',
            name = '',
            phone = '',
            platform = '',
            token,
            secret
        }) {
            const body = {};

            if (!type || !emailAddress || !description) {
                return Promise.reject('Please fill out all required fields');
            }

            body.type = type;
            body.email = emailAddress;
            body.description = description;
            body.issue = issue;
            body.url = url;
            body.name = name;
            body.phone = phone;
            body.platform = platform;

            return oauthRequest({
                url: `${getConfig().baseUrl}/support_ticket`,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        api({
            emailAddress,
            description,
            name = '',
            company = '',
            token,
            secret
        }) {
            const body = {};

            if (!emailAddress || !description) {
                return Promise.reject('Please fill out all required fields');
            }

            body.type = 'api';
            body.email = emailAddress;
            body.request = description;
            body.emailer = name;
            body.company = company;

            return oauthRequest({
                url: `${getConfig().baseUrl}/support_ticket/api`,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        legal({ name, emailAddress, company, url, message, token, secret }) {
            const body = {};

            if (!name || !emailAddress || !company || !url || !message) {
                return Promise.reject('Please fill out all required fields');
            }

            body.name = name;
            body.email = emailAddress;
            body.company = company;
            body.url = url;
            body.message = message;

            return oauthRequest({
                url: `${getConfig().baseUrl}/dmca_notice`,
                method: 'POST',
                body,
                token,
                secret
            });
        }
    };
}
