/**
 * User resource of the API client
 *
 * @param  {Function} getConfig FUnction to get the current config
 * @return {Promise}
 */

import { parseApiObject, buildQueryString, cleanParameters } from 'utils/index';

export default function api(getConfig, oauthRequest) {
    return {
        get(token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    ...parseApiObject(data)
                };
            });
        },

        save(data, token, secret) {
            const profileKeys = [
                'name',
                'label',
                'hometown',
                'url',
                'bio',
                'twitter',
                'facebook',
                'instagram',
                'youtube',
                'genre',
                'image',
                'image_banner',
                'podcast_category',
                'podcast_language',
                'birthday',
                'gender'
            ];
            const filtered = Object.keys(data).reduce((obj, key) => {
                if (profileKeys.includes(key)) {
                    obj[key] = (data[key] || '').trim();

                    // Just get the base64 part without the "data:image/jpeg;base64,"
                    if (key === 'image' || key === 'image_banner') {
                        if ((data[key] || '').includes(',')) {
                            obj[key] = data[key].split(',')[1] || '';
                        }
                    }
                }
                return obj;
            }, {});

            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                token,
                secret,
                method: 'PUT',
                body: filtered
            }).then((newData) => {
                return parseApiObject(newData);
            });
        },

        updatePassword(oldPassword, newPassword, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                token,
                secret,
                method: 'PATCH',
                body: {
                    old_password: oldPassword,
                    new_password: newPassword
                }
            }).then((newData) => {
                return parseApiObject(newData);
            });
        },

        updateSlug(slug, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                token,
                secret,
                method: 'PATCH',
                body: {
                    url_slug: slug
                }
            }).then((newData) => {
                return parseApiObject(newData);
            });
        },

        updateEmail(email, password, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                token,
                secret,
                method: 'PATCH',
                body: {
                    email,
                    password
                }
            }).then((newData) => {
                return parseApiObject(newData);
            });
        },

        deleteAccount(password, isSocial, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                token,
                secret,
                method: 'DELETE',
                body: cleanParameters({
                    password,
                    isSocial: !!isSocial
                })
            }).then((newData) => {
                return parseApiObject(newData);
            });
        },

        login(email, password) {
            const parameters = {
                x_auth_username: email,
                x_auth_password: password,
                x_auth_mode: 'client_auth'
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/access_token`,
                method: 'POST',
                body: parameters
            });
        },

        identityCheck(value, type) {
            const parameters = cleanParameters({
                [type]: value
            });

            return oauthRequest({
                url: `${getConfig().baseUrl}/identity_check`,
                method: 'GET',
                body: parameters
            });
        },

        loginFacebook(fbToken, fbUserId, fbUsername, userEmail) {
            const parameters = {
                fb_token: fbToken,
                fb_user_id: fbUserId,
                fb_username: fbUsername
            };

            // userEmail is needed just in case the user does not have an associated email in their social network
            if (userEmail) {
                parameters.u_auth_email = userEmail;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/access_token`,
                method: 'POST',
                body: parameters
            });
        },

        loginInstagram(instagramToken, instagramId) {
            const parameters = {
                instagram_token: instagramToken,
                instagram_id: instagramId
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/access_token`,
                method: 'POST',
                body: parameters
            });
        },

        getTwitterRequestToken(options = {}) {
            const { token, secret, isLinkage } = options;

            return oauthRequest({
                url: `${getConfig().baseUrl}/twitter/requesttoken`,
                method: 'POST',
                token,
                secret,
                body: {
                    isLinkage: isLinkage
                }
            });
        },

        logInTwitter(twitterToken, twitterSecret, oauthVerifier, userEmail) {
            const parameters = {
                t_token: twitterToken,
                t_secret: twitterSecret,
                t_oauth_verifier: oauthVerifier
            };

            // userEmail is needed just in case the user does not have an associated email in their social network
            if (userEmail) {
                parameters.u_auth_email = userEmail;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/access_token`,
                method: 'POST',
                body: parameters
            });
        },

        loginGoogle(token, accessToken) {
            const parameters = {
                g_token: token,
                g_access_token: accessToken
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/access_token`,
                method: 'POST',
                body: parameters
            });
        },

        logInApple(idToken) {
            const parameters = {
                id_token: idToken
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/auth/apple`,
                method: 'POST',
                body: parameters
            });
        },

        claim(id, options) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/artist/${id}/claim`,
                method: 'PUT',
                body: options
            });
        },

        register(email, username, password, password2) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/register`,
                method: 'POST',
                body: {
                    email,
                    artist_name: username,
                    password,
                    password2
                }
            });
        },

        registerWithCaptcha(email, username, passwords, captcha) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user`,
                method: 'POST',
                body: {
                    email,
                    artist_name: username,
                    password: passwords[0],
                    password2: passwords[1],
                    'g-recaptcha-response': captcha
                }
            });
        },

        resendEmail(token, secret, { returnPath = null }) {
            const options = {
                url: `${getConfig().baseUrl}/user/email-confirmation`,
                method: 'POST',
                token,
                secret
            };

            if (returnPath) {
                options.body = {
                    encodedReturnPath: encodeURIComponent(returnPath)
                };
            }

            return oauthRequest(options);
        },

        verifyHash(hash, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/email-verify`,
                method: 'POST',
                body: {
                    hash
                },
                token,
                secret
            });
        },

        emailUnsubscribe(hash) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/email/unsubscribe/${hash}`,
                method: 'POST'
            });
        },

        forgot(email) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/forgot-password`,
                method: 'POST',
                body: {
                    email
                }
            });
        },

        verifyForgotPasswordToken(token) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/user/verify-forgot-token?token=${token}`
            });
        },

        recoverAccount(token, password, password2) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/recover-account`,
                method: 'POST',
                body: {
                    token,
                    password,
                    password2
                }
            });
        },

        favorite(id, token, secret, options = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/favorite`,
                method: 'PUT',
                body: cleanParameters({
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        unfavorite(id, token, secret, options = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/favorite`,
                method: 'DELETE',
                body: cleanParameters({
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        repost(id, token, secret, options = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/repost`,
                method: 'PUT',
                body: cleanParameters({
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        unrepost(id, token, secret, options = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/repost`,
                method: 'DELETE',
                body: cleanParameters({
                    section: options.section,
                    environment: options.environment,
                    referer: options.referer
                }),
                token,
                secret
            });
        },

        feed(token, secret, { page = 1, limit = 20, onlyUploads = false }) {
            const query = buildQueryString({
                limit,
                only_uploads: onlyUploads
            });

            return oauthRequest({
                url: `${getConfig().baseUrl}/user/feed/page/${page}?${query}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        follow(token, secret, page = 1) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/follow/page/${page}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        playlists(token, secret, { page = 1, limit = 20, musicId }) {
            const query = buildQueryString({
                limit,
                music_id: musicId
            });

            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/user/playlists/page/${page}?${query}`,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        uploads({
            token,
            secret,
            show = 'all',
            page = 1,
            limit = 20,
            query = null,
            incompletes = null,
            sort = null,
            pinnable = null
        }) {
            let queryString = buildQueryString({
                limit,
                show,
                sort,
                q: query,
                incompletes,
                pinnable: pinnable
            });

            if (show === 'podcast') {
                queryString = buildQueryString({
                    limit,
                    show: 'songs',
                    genre: 'podcast',
                    sort,
                    q: query,
                    incompletes,
                    pinnable: pinnable
                });
            }

            const url = `${
                getConfig().baseUrl
            }/user/uploads/page/${page}?${queryString}`;

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        nativeNotifications(
            token,
            secret,
            { unseen, limit, pagingToken } = {}
        ) {
            const queryString = buildQueryString({
                only_unseen: unseen,
                limit,
                paging_token: pagingToken
            });
            const url = `${
                getConfig().baseUrl
            }/user/native-notifications?${queryString}`;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        markNotificationsAsSeen(token, secret, { all, uids } = {}) {
            const body = cleanParameters({
                for_all: all,
                uids
            });
            const url = `${getConfig().baseUrl}/user/native-notifications/seen`;

            return oauthRequest({
                url,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                body,
                token,
                secret
            });
        },

        getCheckoutSession(token, secret) {
            const url = `${getConfig().baseUrl}/user/premium-checkout`;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        getCheckoutUpdateSession(token, secret) {
            const url = `${getConfig().baseUrl}/user/premium-checkout`;

            return oauthRequest({
                url,
                method: 'PUT',
                token,
                secret
            });
        },

        getSubscription(token, secret) {
            const url = `${getConfig().baseUrl}/user/premium-subscription`;

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        cancelSubscription(token, secret) {
            const url = `${getConfig().baseUrl}/user/premium-subscription`;

            return oauthRequest({
                url,
                method: 'DELETE',
                token,
                secret
            });
        },

        reactivateSubscription(token, secret) {
            const url = `${getConfig().baseUrl}/user/premium-subscription`;

            return oauthRequest({
                url,
                method: 'POST',
                token,
                secret
            });
        },

        linkNetwork(token, secret, data = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/access_token`,
                method: 'POST',
                body: data,
                token,
                secret
            });
        },

        unlinkNetwork(network, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/social/unlink`,
                method: 'POST',
                body: {
                    network: network
                },
                token,
                secret
            });
        },

        deleteExternalRssFeedUrl(token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/rss`,
                method: 'DELETE',
                token,
                secret
            });
        },

        getExternalRssFeedUrl(token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/rss`,
                token,
                secret
            }).then((url) => url);
        },

        setExternalRssFeedUrl(token, secret, data) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/rss`,
                method: 'PUT',
                body: data,
                token,
                secret
            }).then(() => data.url);
        },

        getUserSettings(token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/setting/notification`,
                method: 'GET',
                token,
                secret
            });
        },

        updateUserSettings(token, secret, settings = {}) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/setting/notification`,
                method: 'POST',
                body: {
                    settings: JSON.stringify(settings)
                },
                token,
                secret
            }).then(() => settings);
        }
    };
}
