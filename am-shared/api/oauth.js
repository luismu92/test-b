import { cleanParameters } from 'utils/index';

export default function api(getConfig, oauthRequest, request) {
    return {
        login(requestToken, { accessToken, username, password } = {}) {
            const parameters = cleanParameters({
                username,
                password,
                access_token: accessToken,
                oauth_token: requestToken
            });
            const url = `${getConfig().baseUrl}/authorize`;

            return request(url, {
                method: 'POST',
                body: parameters
            });
        },

        validateToken(token) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/request_token/${token}`
            });
        },

        deleteToken(token) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/request_token/${token}`,
                method: 'DELETE'
            });
        },

        getAuthorizedApps(token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/applications`,
                token,
                secret
            });
        },

        revokeApp(name, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/user/applications/${name}`,
                method: 'DELETE',
                token,
                secret
            });
        }
    };
}
