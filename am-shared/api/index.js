import serializeJS from 'serialize-javascript';
import { stringify } from 'query-string';
import URL from 'url';

import { request, nonce } from 'utils/index';

import user from './user';
import artist from './artist';
import chart from './chart';
import music from './music';
import email from './email';
import playlist from './playlist';
import search from './search';
import featured from './featured';
import admin from './admin';
import upload from './upload';
import stats from './stats';
import external from './external';
import monetization from './monetization';
import oauth from './oauth';
import followlist from './followlist';
import comment from './comment';
import world from './world';
import adminApiRequest from './theBackwoods';

import signature from 'oauth-signature';

let baseUrl = process.env.API_URL;
let consumerKey = process.env.API_CONSUMER_KEY;
let consumerSecret = process.env.API_CONSUMER_SECRET;
let customHeaders = {};

function getOauthRequestParams({
    url,
    method = 'GET',
    body = {},
    headers = {},
    token = null,
    secret = null
} = {}) {
    const query = URL.parse(url, true).query || {};
    const oauthSecret = secret;
    const lowercaseMethod = method.toLowerCase();
    const lowercaseHeaders = Object.keys(headers).reduce((acc, header) => {
        acc[header.toLowerCase()] = headers[header].toLowerCase();

        return acc;
    }, {});
    const oauthParameters = {
        oauth_consumer_key: getConfig().consumerKey,
        oauth_signature_method: 'HMAC-SHA1',
        oauth_timestamp: Math.round(Date.now() / 1000),
        oauth_nonce: nonce(32),
        oauth_version: '1.0'
    };

    if (token) {
        oauthParameters.oauth_token = token; // eslint-disable-line camelcase
        // oauthParameters.oauth_secret = secret; // eslint-disable-line camelcase
    }

    const parameters = {
        ...query
    };
    const isDeleteOrGet = ['get', 'delete'].includes(lowercaseMethod);

    if (
        !lowercaseHeaders['content-type'] ||
        lowercaseHeaders['content-type'].indexOf('application/json') === -1 ||
        isDeleteOrGet
    ) {
        Object.keys(body).forEach((key) => {
            parameters[key] = body[key];
        });
    }
    const qsOptions = { arrayFormat: 'bracket' };
    const allParameters = {
        ...parameters,
        ...oauthParameters
    };
    const urlWithoutQuery = url.split('?')[0];
    const encodedSignature = signature.generate(
        method,
        urlWithoutQuery,
        allParameters,
        getConfig().consumerSecret,
        oauthSecret,
        {
            encodeSignature: true
        }
    );
    let queryString = stringify(
        {
            ...parameters,
            ...oauthParameters
        },
        qsOptions
    );

    let finalUrl = `${urlWithoutQuery}?${queryString}`;

    // GET requests can't have a body
    // DELETE request bodies will usually be ignored http://stackoverflow.com/a/299696/1048847
    if (!isDeleteOrGet) {
        queryString = stringify(oauthParameters, qsOptions);

        finalUrl = `${urlWithoutQuery}?${queryString}`;
        body = {
            ...parameters,
            ...body
        };
    }

    finalUrl += `&oauth_signature=${encodedSignature}`;

    const requestOptions = {
        method,
        credentials: 'omit',
        headers,
        body: Object.keys(body).reduce((acc, key) => {
            acc[key] = body[key];

            // Replace any null or undefined values with an empty string
            // The oauth library replaces these values with an empty string
            // but I'm not sure if the fix should be in the library though or
            // maybe the library should have a method to return the body
            // parameters that were encoded so you know there's no mismatch
            //
            // For now we replace them here.
            if (body[key] === null || typeof body[key] === 'undefined') {
                acc[key] = '';
            }

            return acc;
        }, {})
    };

    return {
        url: finalUrl,
        requestOptions
    };
}

export function oauthRequest({
    url,
    method = 'GET',
    body = {},
    headers = {},
    token,
    secret,
    getRawResponse = false
} = {}) {
    const { url: finalUrl, requestOptions } = getOauthRequestParams({
        url,
        method,
        body,
        headers: {
            ...customHeaders,
            ...headers
        },
        token,
        secret
    });

    const promise = request(finalUrl, requestOptions, getRawResponse);

    if (getRawResponse) {
        return promise;
    }

    return promise.then((data) => {
        return JSON.parse(serializeJS(data, { isJSON: true }));
    });
}

function getConfig() {
    return {
        baseUrl,
        consumerKey,
        consumerSecret,
        customHeaders
    };
}

function setConfig(options = {}) {
    baseUrl = options.baseUrl || baseUrl;
    consumerKey = options.consumerKey || consumerKey;
    consumerSecret = options.consumerSecret || consumerSecret;
    customHeaders = options.customHeaders || customHeaders;
}

const api = Object.freeze({
    user: user(getConfig, oauthRequest),
    artist: artist(getConfig, oauthRequest),
    chart: chart(getConfig, oauthRequest),
    music: music(getConfig, oauthRequest),
    email: email(getConfig, oauthRequest),
    playlist: playlist(getConfig, oauthRequest),
    search: search(getConfig, oauthRequest),
    featured: featured(getConfig, oauthRequest),
    admin: admin(getConfig, oauthRequest),
    stats: stats(getConfig, oauthRequest),
    upload: upload(getConfig, oauthRequest),
    monetization: monetization(getConfig, oauthRequest),
    oauth: oauth(getConfig, oauthRequest, request),
    world: world(request),
    external: external(request),
    followlist: followlist(getConfig, oauthRequest),
    comment: comment(getConfig, oauthRequest),
    adminApiRequest: adminApiRequest(getConfig, oauthRequest)
});

if (typeof window === 'object' && process.env.NODE_ENV === 'development') {
    window.api = api;
}

export { setConfig, getConfig };
export default api;
