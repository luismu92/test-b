function getBaseUrl(getConfig, getAdminEndpoint) {
    return `${getConfig().baseUrl}/admin${getAdminEndpoint}`;
}

export default function api(getConfig, oauthRequest) {
    return {
        get(adminEndpoint, token, secret) {
            return oauthRequest({
                url: getBaseUrl(getConfig, adminEndpoint),
                method: 'GET',
                token,
                secret
            });
        },

        post(adminEndpoint, body, token, secret) {
            return oauthRequest({
                url: getBaseUrl(getConfig, adminEndpoint),
                method: 'POST',
                body,
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                token,
                secret
            });
        },

        delete(adminEndpoint, token, secret) {
            return oauthRequest({
                url: getBaseUrl(getConfig, adminEndpoint),
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                token,
                secret
            });
        },

        put(adminEndpoint, token, secret) {
            return oauthRequest({
                url: getBaseUrl(getConfig, adminEndpoint),
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                token,
                secret
            });
        }
    };
}
