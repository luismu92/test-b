export default function api(request) {
    return {
        blog() {
            return request('/blog');
        },

        instagram(token) {
            return request(
                `https://api.instagram.com/v1/users/self/?access_token=${token}`,
                {
                    credentials: 'omit'
                }
            );
        }
    };
}
