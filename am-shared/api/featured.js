export default function api(getConfig, oauthRequest) {
    return {
        get() {
            const url = `${getConfig().baseUrl}/featured`;

            return oauthRequest({
                url
            });
        }
    };
}
