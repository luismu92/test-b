/**
 * Blog resource of the API client
 */

import { buildQueryString, cleanParameters } from 'utils/index';
import { csvToArray } from 'utils/string';

export default function api(request) {
    const baseUrl = process.env.BLOG_URL;
    const v2Url = `${baseUrl}/ghost/api/v2/content`;
    const key = process.env.BLOG_CONTENT_KEY;
    const requestOptions = {
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'omit'
    };

    return {
        posts({ page = 1, limit = 10, include, filter } = {}) {
            const qs = buildQueryString(
                cleanParameters({
                    page,
                    limit,
                    include,
                    filter,

                    // Key is required
                    key
                })
            );

            return request(`${v2Url}/posts/?${qs}`, requestOptions);
        },

        settings() {
            const qs = buildQueryString(
                cleanParameters({
                    // Key is required
                    key
                })
            );

            return request(`${v2Url}/settings/?${qs}`, requestOptions);
        },

        pages({ page = 1, limit = 10 } = {}) {
            const qs = buildQueryString(
                cleanParameters({
                    page,
                    limit,
                    order: 'updated_at ASC',
                    include: 'tags',

                    // Key is required
                    key
                })
            );

            return request(`${v2Url}/pages/?${qs}`, requestOptions);
        },

        postById(id, { page = 1, limit = 8, include, formats } = {}) {
            const qs = buildQueryString(
                cleanParameters({
                    page,
                    limit,
                    include,
                    formats,

                    // Key is required
                    key
                })
            );

            return request(`${v2Url}/posts/${id}/?${qs}`, requestOptions);
        },

        postBySlug(slug, { page = 1, limit = 8, include, formats } = {}) {
            const qs = buildQueryString(
                cleanParameters({
                    page,
                    limit,
                    include,
                    formats,

                    // Key is required
                    key
                })
            );

            return request(
                `${v2Url}/posts/slug/${slug}/?${qs}`,
                requestOptions
            );
        },

        locations() {
            // @todo add location resource to ghost api with all possible locations
            // along with the posts associated by location
            return request(
                // 'https://s3.amazonaws.com/audiomack.world/static/globe/cities.csv',
                'https://audiomack.world/static/globe/cities.csv',
                {
                    credentials: 'omit'
                }
            ).then((data) => {
                return csvToArray(data).map((obj) => {
                    return {
                        ...obj,
                        latitude: parseFloat(obj.latitude),
                        longitude: parseFloat(obj.longitude)
                    };
                });
            });
        },

        tags({ page = 1, limit = 8, filter } = {}) {
            const qs = buildQueryString(
                cleanParameters({
                    page,
                    limit,
                    filter,

                    // Key is required
                    key
                })
            );

            return request(`${v2Url}/tags?${qs}`, requestOptions);
        },

        postByTagSlug(slug, { page = 1, limit = 8, include, formats } = {}) {
            const qs = buildQueryString(
                cleanParameters({
                    page,
                    limit,
                    include,
                    formats,

                    // Key is required
                    key
                })
            );

            return request(`${v2Url}/tags/slug/${slug}/?${qs}`, requestOptions);
        }
    };
}
