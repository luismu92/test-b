export default function api(getConfig, oauthRequest) {
    return {
        add(artistId, token, secret) {
            const url = `${getConfig().baseUrl}/follow_list`;
            const body = {
                artist_id: artistId
            };

            return oauthRequest({
                url,
                method: 'POST',
                body,
                token,
                secret
            });
        },

        remove(artistId, token, secret) {
            const url = `${getConfig().baseUrl}/follow_list/${artistId}`;

            return oauthRequest({
                url,
                method: 'DELETE',
                token,
                secret
            });
        }
    };
}
