// import { parseApiObject } from 'utils/index';

export default function api(getConfig, oauthRequest) {
    return {
        getSignedUrl(encodedFilename, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/files/upload/${encodedFilename}`,
                token,
                secret
            }).then((data) => {
                return data;
            });
        },

        createEntity({ type, key, artist, title, genre, token, secret }) {
            const getRawResponse = true;
            const body = {
                key: decodeURIComponent(key)
            };

            if (genre) {
                body.genre = genre;
            }

            if (type === 'album') {
                if (!artist || !title) {
                    return Promise.reject('Artist and title are required');
                }

                body.artist = artist;
                body.title = title;
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${type}`,
                method: 'POST',
                body,
                token,
                secret,
                getRawResponse
            })
                .then((response) => {
                    return Promise.all([
                        response.status,
                        response.text(),
                        response.headers,
                        response.redirected,
                        response.url
                    ]);
                })
                .then(([status, text, headers, redirected, redirectUrl]) => {
                    let ret = text;

                    try {
                        ret = JSON.parse(text);
                    } catch (e) {} // eslint-disable-line no-empty

                    if (redirected || (status >= 200 && status < 300)) {
                        const location = headers.get('location') || redirectUrl;

                        if (!location) {
                            throw new Error("Couldn't get song location");
                        }

                        const matches = location.match(/\d+$/);

                        if (!matches) {
                            throw new Error('Something went wrong');
                        }

                        return {
                            id: parseInt(matches[0], 10),
                            alreadyExists: redirected
                        };
                    }

                    throw ret;
                });
        }
    };
}
