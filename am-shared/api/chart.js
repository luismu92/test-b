/**
 * Chart resource of the API client
 *
 * @param  {String} baseUrl base url of the chart resource
 * @return {Promise}
 */

import { parseApiObject } from 'utils/index';

export default function chart(getConfig, oauthRequest) {
    return {
        // /v1/chart/(songs|albums|playlists)/(chart type)
        // chart type must be one of the following:
        // - total
        // - daily
        // - weekly
        // - monthly
        // - yearly
        //
        // Genre-specific chart tracks
        // GET /v1/(genre)/chart/(songs|albums|playlists)/(chart type)
        get(
            collection = 'songs',
            period = 'weekly',
            genre = null,
            page = 1,
            limit = 20
        ) {
            // eslint-disable-line max-params
            let promise;

            if (genre) {
                promise = oauthRequest({
                    url: `${
                        getConfig().baseUrl
                    }/${genre}/chart/${collection}/${period}/page/${page}?limit=${limit}`
                });
            } else {
                promise = oauthRequest({
                    url: `${
                        getConfig().baseUrl
                    }/chart/${collection}/${period}/page/${page}?limit=${limit}`
                });
            }

            promise = promise.then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject).map((result) => {
                        if (!Array.isArray(result.tracks)) {
                            return result;
                        }

                        const newResult = {
                            ...result,
                            tracks: result.tracks.map((track) => {
                                const newTrack = {
                                    ...track,
                                    duration: parseInt(track.duration, 10) || 0
                                };

                                return newTrack;
                            })
                        };

                        return newResult;
                    })
                };
            });

            return promise;
        }
    };
}
