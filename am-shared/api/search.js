/**
 * Search resource of the API client
 *
 * @param  {Function} getConfig function to get the config
 * @return {Promise}
 */

import { parseApiObject, buildQueryString } from 'utils/index';

export default function search(getConfig, oauthRequest) {
    return {
        suggest(query = '') {
            if (!query) {
                return Promise.resolve({});
            }

            return oauthRequest({
                url: `${getConfig().baseUrl}/search_autosuggest?q=${query}`
            });
        },

        get(
            query = '',
            {
                page = 1,
                limit = 20,
                type = null,
                context = null,
                genre = '',
                tag = '',
                verified = false,
                token,
                secret
            }
        ) {
            if (!query && !genre && !tag) {
                console.warn('No query provided');
                return Promise.resolve({});
            }

            const queryString = buildQueryString({
                q: query.trim(),
                page: page,
                limit: limit,
                show: type,
                sort: context,
                genre: genre,
                tag: tag,
                verified: verified ? 'on' : null
            });
            const url = `${getConfig().baseUrl}/search?${queryString}`;

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                const newData = {
                    ...parseApiObject(data),
                    results: data.results.map((result) => {
                        const newResult = {
                            ...result,
                            ...parseApiObject(result)
                        };

                        if (newResult.tracks && newResult.tracks.length) {
                            newResult.tracks = newResult.tracks.map(
                                parseApiObject
                            );
                        }

                        return newResult;
                    })
                };

                if (newData.verified_artist) {
                    newData.verified_artist = parseApiObject(
                        newData.verified_artist
                    ); // eslint-disable-line
                }

                return newData;
            });
        },

        getUser(
            query = '',
            token,
            secret,
            { page = 1, limit = 20, show = 'favorites' }
        ) {
            if (!query) {
                console.warn('No query provided');
                return Promise.resolve({});
            }

            let url = `${
                getConfig().baseUrl
            }/user/search?q=${query}&show=${show}`;

            if (page) {
                url = `${url}&page=${page}`;
            }

            if (limit) {
                url = `${url}&limit=${limit}`;
            }

            return oauthRequest({
                url,
                token,
                secret
            });
        },

        // user suggestions not yet implemented in the API
        suggestUser() {
            return null;
        }
    };
}
