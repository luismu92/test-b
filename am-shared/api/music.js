/**
 * Music resource of the API client
 *
 * @param  {String} baseUrl base url of the music resource
 * @return {Promise}
 */

import { parseApiObject, cleanParameters, yesBool } from 'utils/index';

export default function music(getConfig, oauthRequest) {
    return {
        trending(genre = null, page = 1) {
            let promise;

            if (genre) {
                promise = oauthRequest({
                    url: `${
                        getConfig().baseUrl
                    }/music/${genre}/trending/page/${page}`
                });
            } else {
                promise = oauthRequest({
                    url: `${getConfig().baseUrl}/music/trending/page/${page}`
                });
            }

            promise = promise.then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject).map((result) => {
                        if (!Array.isArray(result.tracks)) {
                            return result;
                        }

                        const newResult = {
                            ...result,
                            tracks: result.tracks.map(parseApiObject)
                        };

                        return newResult;
                    })
                };
            });

            return promise;
        },

        recent(genre = null, page = 1) {
            let promise;

            if (genre) {
                promise = oauthRequest({
                    url: `${
                        getConfig().baseUrl
                    }/music/${genre}/recent/page/${page}`
                });
            } else {
                promise = oauthRequest({
                    url: `${getConfig().baseUrl}/music/recent/page/${page}`
                });
            }

            promise = promise.then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });

            return promise;
        },

        id(id, options = {}) {
            let url = `${getConfig().baseUrl}/music/${id}`;

            const {
                token = options.token,
                secret = options.secret,
                key = options.key
            } = options;

            if (key) {
                url = `${url}?key=${key}`;
            }

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: parseApiObject(data.results)
                };
            });
        },

        update(id, metadata, token, secret) {
            const updateKeys = [
                'artist',
                'title',
                'featuring',
                'producer',
                'genre',
                'image',
                'explicit',

                // metadata
                'album',
                'video',
                'description',
                'admin_description',
                'album_track_only',

                // release
                'private',
                'stream_only',
                'url_slug',
                'buy_title',
                'buy_link',
                'released',

                // monetization
                'video_ad',
                'accounting_code',
                'isrc',
                'upc',

                // Finalizing music
                'status',
                'isSingleTrack',
                'isEditing',

                // Replacing music
                'key'
            ];

            const yesKeys = [
                'private',
                'stream_only',
                'video_ad',
                'isSingleTrack',
                'explicit'
            ];

            const data = Object.keys(metadata).reduce((obj, key) => {
                if (updateKeys.indexOf(key) !== -1) {
                    // Just get the base64 part without the "data:image/jpeg;base64,"
                    if (key === 'image') {
                        obj[key] = metadata[key].split(',')[1] || '';
                        return obj;
                    }

                    if (key === 'key') {
                        obj[key] = decodeURIComponent(metadata[key]);
                        return obj;
                    }

                    if (yesKeys.includes(key)) {
                        const value = metadata[key];

                        obj[key] = yesBool(value) ? 'yes' : 'no';
                        return obj;
                    }

                    obj[key] =
                        typeof metadata[key] === 'string'
                            ? (metadata[key] || '').trim()
                            : metadata[key];
                }

                return obj;
            }, {});

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}`,
                method: 'PATCH',
                body: data,
                token,
                secret
            }).then((results) => {
                return parseApiObject(results);
            });
        },

        delete(id, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}`,
                method: 'DELETE',
                token,
                secret
            });
        },

        deleteTrack(albumId, songId, token, secret) {
            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/music/album/${albumId}/track/${songId}`,
                method: 'DELETE',
                token,
                secret
            });
        },

        privacy(id, isPrivate, token, secret) {
            const body = {
                private: isPrivate
            };

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}`,
                method: 'PATCH',
                body: body,
                token,
                secret
            });
        },

        addTrackToAlbum(id, key, token, secret) {
            const getRawResponse = true;

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/tracks`,
                method: 'POST',
                body: {
                    key: decodeURIComponent(key)
                },
                token,
                secret,
                getRawResponse
            })
                .then((response) => {
                    return Promise.all([
                        response.status,
                        response.text(),
                        response.headers
                    ]);
                })
                .then(([status, text, headers]) => {
                    let ret = text;

                    try {
                        ret = JSON.parse(text);
                    } catch (e) {} // eslint-disable-line no-empty

                    if (status >= 200 && status < 300) {
                        const location = headers.get('location');
                        const retObj = {};

                        if (!location) {
                            throw new Error("Couldn't get song location");
                        }

                        const matches = location.match(/\d+$/);

                        if (!matches) {
                            throw new Error('Something went wrong');
                        }

                        retObj.id = parseInt(matches[0], 10);
                        retObj.artist = ret.results.artist;
                        retObj.title = ret.results.title;

                        return retObj;
                    }

                    throw ret;
                });
        },

        addPreExistingTrackToAlbum(id, trackId, token, secret) {
            const getRawResponse = true;

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/tracks`,
                method: 'POST',
                body: {
                    track_id: decodeURIComponent(trackId)
                },
                token,
                secret,
                getRawResponse
            })
                .then((response) => {
                    return Promise.all([
                        response.status,
                        response.text(),
                        response.headers
                    ]);
                })
                .then(([status, text, headers]) => {
                    let ret = text;

                    try {
                        ret = JSON.parse(text);
                    } catch (e) {} // eslint-disable-line no-empty

                    if (status >= 200 && status < 300) {
                        const location = headers.get('location');
                        const retObj = {};

                        if (!location) {
                            throw new Error("Couldn't get song location");
                        }

                        const matches = location.match(/\d+$/);

                        if (!matches) {
                            throw new Error('Something went wrong');
                        }

                        retObj.id = parseInt(matches[0], 10);
                        retObj.artist = ret.results.artist;
                        retObj.title = ret.results.title;

                        return retObj;
                    }

                    throw ret;
                });
        },

        reorderAlbumTracks(id, tracks = [], token, secret) {
            const trackKeys = ['song_id', 'title'];

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/tracks`,
                method: 'PUT',
                body: {
                    tracks: tracks.map((track) => {
                        return trackKeys.reduce((acc, key) => {
                            acc[key] = track[key];
                            return acc;
                        }, {});
                    })
                },
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                token,
                secret
            });
        },

        album(artistSlug, albumSlug, options = {}) {
            let url = `${
                getConfig().baseUrl
            }/music/album/${artistSlug}/${albumSlug}`;

            const {
                token = options.token,
                secret = options.secret,
                key = options.key
            } = options;

            if (key) {
                url = `${url}?key=${key}`;
            }

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: {
                        ...parseApiObject(data.results),
                        tracks: (data.results.tracks || []).map(parseApiObject)
                    }
                };
            });
        },

        albumById(id, options = {}) {
            let url = `${getConfig().baseUrl}/music/${id}`;

            const {
                token = options.token,
                secret = options.secret,
                key = options.key
            } = options;

            if (key) {
                if (url.indexOf('?') > -1) {
                    url = `${url}&key=${key}`;
                } else {
                    url = `${url}?key=${key}`;
                }
            }

            return oauthRequest({
                url,
                body: {
                    key
                },
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: {
                        ...parseApiObject(data.results),
                        tracks: (data.results.tracks || []).map(parseApiObject)
                    }
                };
            });
        },

        song(artistSlug, songSlug, options = {}) {
            let url = `${
                getConfig().baseUrl
            }/music/song/${artistSlug}/${songSlug}`;

            const {
                token = options.token,
                secret = options.secret,
                key = options.key
            } = options;

            if (key) {
                url = `${url}?key=${key}`;
            }

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: parseApiObject(data.results)
                };
            });
        },

        songById(id, options = {}) {
            const {
                token = options.token,
                secret = options.secret,
                key = options.key
            } = options;

            let url = `${getConfig().baseUrl}/music/${id}`;

            if (key) {
                url = `${url}?key=${key}`;
            }

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: parseApiObject(data.results)
                };
            });
        },

        getPromoKeys(id, token, secret) {
            const url = `${getConfig().baseUrl}/music/${id}/promo_links`;

            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        addPromoKey(id, key, token, secret) {
            const url = `${getConfig().baseUrl}/music/${id}/promo_links`;

            const parameters = cleanParameters({
                key: key
            });

            return oauthRequest({
                url,
                method: 'POST',
                body: parameters,
                token,
                secret
            }).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        },

        deletePromoKey(id, key, token, secret) {
            const url = `${getConfig().baseUrl}/music/${id}/promo_links/${key}`;

            return oauthRequest({
                url,
                method: 'DELETE',
                token,
                secret
            });
        },

        play(id, options = {}) {
            const { token, secret } = options;

            const parameters = cleanParameters({
                session: options.session,
                album_id: options.album_id,
                playlist_id: options.playlist_id,
                hq: options.hq,
                key: options.key,
                time: options.time,
                environment: options.environment,
                section: options.section,
                referer: options.referer
            });

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/${id}/play`,
                method: 'POST',
                body: parameters,
                token,
                secret
            });
        },

        getStatsToken(session) {
            const parameters = cleanParameters({
                device: session,
                music_id: 1
            });

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/stats/token`,
                method: 'POST',
                body: parameters
            });
        },

        recordStats(token, type, musicId, referer = null) {
            const parameters = cleanParameters({
                token,
                type,
                referer
            });

            return oauthRequest({
                url: `${getConfig().baseUrl}/music/stats/${musicId}`,
                method: 'POST',
                body: parameters
            });
        },

        flagUnplayableSong(artistSlug, songSlug) {
            const parameters = cleanParameters({
                type: 'song',
                status: 'unplayable'
            });

            return oauthRequest({
                url: `${
                    getConfig().baseUrl
                }/music/song/${artistSlug}/${songSlug}`,
                method: 'PATCH',
                body: parameters
            });
        },

        getGlobalTags(token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/musictags`,
                token,
                secret
            });
        },

        getGlobalGeoTags(country, state, token, secret) {
            let url = `${getConfig().baseUrl}/musictags/geo`;

            if (country) {
                url += `/${country}`;
            }

            if (country && state) {
                url += `/${state}`;
            }

            // JSON.parse is necessary because the server response is parsed as a string instead of as an array
            return oauthRequest({
                url,
                token,
                secret
            }).then((data) => JSON.parse(data));
        },

        getTags(type, id, token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/musictags/${type}/${id}`,
                token,
                secret
            });
        },

        addTags(type, id, tags = [], token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/musictags/${type}/${id}`,
                method: 'POST',
                body: {
                    tags: JSON.stringify(tags)
                },
                token,
                secret
            });
        },

        deleteTags(type, id, tags = [], token, secret) {
            return oauthRequest({
                url: `${getConfig().baseUrl}/musictags/${type}/${id}`,
                method: 'DELETE',
                body: {
                    tags: JSON.stringify(tags)
                },
                token,
                secret
            });
        },

        suggestTag(type, id, tag, token, secret) {
            const parameters = cleanParameters({
                type,
                id,
                tag
            });

            return oauthRequest({
                url: `${getConfig().baseUrl}/musictags/suggest`,
                method: 'POST',
                body: parameters,
                token,
                secret
            });
        }
    };
}
