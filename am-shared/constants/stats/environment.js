export const ENVIRONMENT_DESKTOP = 'desktop-web';
export const ENVIRONMENT_MOBILE = 'mobile-web';
export const ENVIRONMENT_CHROMECAST = 'chromecast';
export const ENVIRONMENT_EMBED = 'embedded';
