export const fauxData = {
    data: [
        {
            play_song: 1093842,
            geo_country: 'US',
            section: 'Playlist'
        },
        {
            play_song: 876294,
            geo_country: 'GB',
            section: 'Trending'
        },
        {
            play_song: 449201,
            geo_country: 'NG',
            section: 'Charts'
        },
        {
            play_song: 190294,
            geo_country: 'FR',
            section: 'Embeds'
        },
        {
            play_song: 90983,
            geo_country: 'CA',
            section: 'Profile'
        },
        {
            play_song: 49284,
            geo_country: 'AU',
            section: 'My Library'
        }
    ]
};

export const fauxFans = {
    data: [
        {
            fan_artist_id: 37305,
            plays: 33028,
            info: {
                id: 37305,
                name: 'Dave Edwards',
                image_base:
                    'https://assets.audiomack.com/dave-edwards/c3e65610115ff5e95dd508912051ec15a2085a1e6ed026a257ecc408bcbea175.jpeg'
            }
        },
        {
            fan_artist_id: 2529916,
            plays: 4920,
            info: {
                id: 2529916,
                name: 'Joevango',
                image_base:
                    'https://assets.audiomack.com/fuckjoevango/fuckjoevango-275-275-1541091845.jpg'
            }
        },
        {
            fan_artist_id: 7330807,
            plays: 1048,
            info: {
                id: 7330807,
                name: 'Max Escobar',
                image_base:
                    'https://assets.audiomack.com/maxescobar/maxescobar-275-275-1539052327.jpg'
            }
        }
    ]
};

export const fauxInfluencers = {
    data: [
        {
            fan_artist_id: 74790,
            followers: 575508,
            info: {
                id: 74790,
                name: 'Audiomack',
                image_base:
                    'https://assets.audiomack.com/audiomack/audiomack-275-275-1512750112.jpg'
            }
        },
        {
            fan_artist_id: 87283,
            followers: 207361,
            info: {
                id: 87283,
                name: 'Audiomack Electronic',
                image_base:
                    'https://assets.audiomack.com/audiomack-electronic/audiomack-electronic-275-275-1513028172.jpg'
            }
        },
        {
            fan_artist_id: 1439,
            followers: 29614,
            info: {
                id: 1439,
                name: 'DJBooth',
                image_base:
                    'https://assets.audiomack.com/djbooth/djbooth-275-275-1519933831.jpg'
            }
        }
    ]
};
