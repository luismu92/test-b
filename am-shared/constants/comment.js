export const LIMIT_PER_PAGE = 10;
export const LIMIT_PER_PAGE_REPORTS = 25;
export const LIMIT_DISPLAY_CHILDREN = 1;
export const LIMIT_FEATHER = 2;
export const ORDER_BY_DATE = 'date';
export const ORDER_BY_VOTE = 'vote';
export const ORDER_BY_OLD = 'old';

export const KIND_SONG = 'song';
export const KIND_ALBUM = 'album';
export const KIND_PLAYLIST = 'playlist';
export const KIND_PAGE = 'page';
export const KIND_MANAGEMENT_FLAGGED = 'flagged';
export const KIND_MANAGEMENT_RECENT = 'recent';
export const KIND_MANAGEMENT_USER = 'user';
