export const COLLECTION_TYPE_TRENDING = 'trending-now';
export const COLLECTION_TYPE_ARTIST = 'artists';
export const COLLECTION_TYPE_SONG = 'songs';
export const COLLECTION_TYPE_PLAYLIST = 'playlists';
export const COLLECTION_TYPE_ALBUM = 'albums';
export const COLLECTION_TYPE_RECENTLY_ADDED = 'recent';
export const COLLECTION_TYPE_BLOG = 'blog';

export const PLAYLIST_TYPE_BROWSE = 'browse';
export const PLAYLIST_TYPE_MINE = 'mine';
export const PLAYLIST_TYPE_FAVORITES = 'favorites';

export const GENRE_TYPE_ALL = '';
export const GENRE_TYPE_RAP = 'rap';
export const GENRE_TYPE_RNB = 'rnb';
export const GENRE_TYPE_ELECTRONIC = 'electronic';
export const GENRE_TYPE_ROCK = 'rock';
export const GENRE_TYPE_POP = 'pop';
export const GENRE_TYPE_DANCEHALL = 'dancehall';
export const GENRE_TYPE_PODCAST = 'podcast';
export const GENRE_TYPE_LATIN = 'latin';
export const GENRE_TYPE_OTHER = 'other';
export const GENRE_TYPE_AFROBEATS = 'afrobeats';
export const GENRE_TYPE_DJMIX = 'dj-mix';
export const GENRE_TYPE_JAZZ = 'jazz';
export const GENRE_TYPE_COUNTRY = 'country';
export const GENRE_TYPE_CLASSICAL = 'classical';
export const GENRE_TYPE_GOSPEL = 'gospel';
export const GENRE_TYPE_ACAPELLA = 'acapella';
export const GENRE_TYPE_FOLK = 'folk';
export const GENRE_TYPE_INSTRUMENTAL = 'instrumental';
export const GENRE_TYPE_SOCA = 'soca';
export const GENRE_TYPE_KOMPA = 'kompa';

export const CHART_TYPE_TOTAL = 'total';
export const CHART_TYPE_DAILY = 'daily';
export const CHART_TYPE_WEEKLY = 'weekly';
export const CHART_TYPE_MONTHLY = 'monthly';
export const CHART_TYPE_YEARLY = 'yearly';

export const SEARCH_QUERY_TYPE_MUSIC = 'music';
export const SEARCH_QUERY_TYPE_SONGS = 'songs';
export const SEARCH_QUERY_TYPE_ALBUMS = 'albums';
export const SEARCH_QUERY_TYPE_ARTISTS = 'artists';
export const SEARCH_QUERY_TYPE_PLAYLISTS = 'playlists';

export const SEARCH_CONTEXT_TYPE_RELEVANCE = 'relevance';
export const SEARCH_CONTEXT_TYPE_RECENT = 'recent';
export const SEARCH_CONTEXT_TYPE_POPULAR = 'popular';

export const DEFAULT_DATE_FORMAT = 'MMMM D, YYYY';

export const playlistGenreMap = {
    [GENRE_TYPE_ALL]: 'Choose a genre',
    [GENRE_TYPE_RAP]: 'Hip-Hop/Rap',
    [GENRE_TYPE_RNB]: 'R&B',
    [GENRE_TYPE_ELECTRONIC]: 'Electronic',
    [GENRE_TYPE_ROCK]: 'Rock',
    [GENRE_TYPE_POP]: 'Pop',
    [GENRE_TYPE_DANCEHALL]: 'Reggae',
    [GENRE_TYPE_AFROBEATS]: 'Afrobeats',
    [GENRE_TYPE_LATIN]: 'Latin',
    [GENRE_TYPE_FOLK]: 'Folk',
    [GENRE_TYPE_INSTRUMENTAL]: 'Instrumental',
    [GENRE_TYPE_OTHER]: 'Other'
};

export const DEFAULT_PLAYLIST_TAG = 'verified-series';

// Should only use for forms where a user can select from genres
// that may or may not be live on the site yet, ie the upload flow.
export const allGenresMap = {
    [GENRE_TYPE_ALL]: 'Choose a genre',
    [GENRE_TYPE_RAP]: 'Hip-Hop/Rap',
    [GENRE_TYPE_RNB]: 'R&B',
    [GENRE_TYPE_ELECTRONIC]: 'Electronic',
    [GENRE_TYPE_DANCEHALL]: 'Reggae',
    [GENRE_TYPE_POP]: 'Pop',
    [GENRE_TYPE_PODCAST]: 'Podcast',
    [GENRE_TYPE_AFROBEATS]: 'Afrobeats',
    [GENRE_TYPE_DJMIX]: 'DJ Mix',
    [GENRE_TYPE_ROCK]: 'Rock',
    [GENRE_TYPE_LATIN]: 'Latin',
    [GENRE_TYPE_SOCA]: 'Soca',
    [GENRE_TYPE_KOMPA]: 'Kompa',
    [GENRE_TYPE_JAZZ]: 'Jazz/Blues',
    [GENRE_TYPE_COUNTRY]: 'Country',
    [GENRE_TYPE_CLASSICAL]: 'Classical',
    [GENRE_TYPE_GOSPEL]: 'Gospel',
    [GENRE_TYPE_ACAPELLA]: 'Acapella',
    [GENRE_TYPE_FOLK]: 'Folk',
    [GENRE_TYPE_INSTRUMENTAL]: 'Instrumental',
    [GENRE_TYPE_OTHER]: 'Other'
};

export const liveGenres = [
    GENRE_TYPE_ALL,
    GENRE_TYPE_RAP,
    GENRE_TYPE_RNB,
    GENRE_TYPE_LATIN,
    GENRE_TYPE_AFROBEATS,
    GENRE_TYPE_DANCEHALL,
    GENRE_TYPE_ELECTRONIC,
    GENRE_TYPE_POP,
    GENRE_TYPE_ROCK,
    // GENRE_TYPE_FOLK,
    GENRE_TYPE_INSTRUMENTAL,
    GENRE_TYPE_PODCAST
];

export const liveGenresExcludedFromCharts = [
    GENRE_TYPE_ROCK,
    GENRE_TYPE_PODCAST
];

export const timeMap = {
    [CHART_TYPE_DAILY]: 'day',
    [CHART_TYPE_WEEKLY]: 'week',
    [CHART_TYPE_MONTHLY]: 'month',
    [CHART_TYPE_YEARLY]: 'year',
    [CHART_TYPE_TOTAL]: ''
};

export const cookies = {
    lastAdDisplayTime: 'adtime',
    playedAtLeastOneSong: 'adsong',
    otherAccounts: 'ama',
    cookieAlert: 'cookieAlert',
    apiConfig: 'amApiConfig'
};

export const AUTH_PROVIDER = {
    PASSWORD: 'password',
    FACEBOOK: 'https://graph.facebook.com',
    GOOGLE: 'https://accounts.google.com',
    TWITTER: 'https://api.twitter.com',
    INSTAGRAM: 'https://graph.facebook.com',
    APPLE: 'https://appleid.apple.com'
};

export const UPLOAD_STATUS = {
    CANCELED: 0,
    SUBMITTING: 1,
    UPLOADING: 2,
    PROCESSING: 3,
    FAILED: 4,
    COMPLETED: 5,
    EXISTING: 6
};

export const UPLOAD_TYPE = {
    SONG: 'song',
    ALBUM: 'album'
};

export const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

export const states = {
    AL: 'Alabama',
    AK: 'Alaska',
    AZ: 'Arizona',
    AR: 'Arkansas',
    CA: 'California',
    CO: 'Colorado',
    CT: 'Connecticut',
    DC: 'District of Columbia',
    DE: 'Delaware',
    FL: 'Florida',
    GA: 'Georgia',
    HI: 'Hawaii',
    ID: 'Idaho',
    IL: 'Illinois',
    IN: 'Indiana',
    IA: 'Iowa',
    KS: 'Kansas',
    KY: 'Kentucky',
    LA: 'Louisiana',
    ME: 'Maine',
    MD: 'Maryland',
    MA: 'Massachusetts',
    MI: 'Michigan',
    MN: 'Minnesota',
    MS: 'Mississippi',
    MO: 'Missouri',
    MT: 'Montana',
    NE: 'Nebraska',
    NV: 'Nevada',
    NH: 'New Hampshire',
    NJ: 'New Jersey',
    NM: 'New Mexico',
    NY: 'New York',
    NC: 'North Carolina',
    ND: 'North Dakota',
    OH: 'Ohio',
    OK: 'Oklahoma',
    OR: 'Oregon',
    PA: 'Pennsylvania',
    RI: 'Rhode Island',
    SC: 'South Carolina',
    SD: 'South Dakota',
    TN: 'Tennessee',
    TX: 'Texas',
    UT: 'Utah',
    VT: 'Vermont',
    VA: 'Virginia',
    WA: 'Washington',
    WI: 'Wisconsin',
    WV: 'West Virginia',
    WY: 'Wyoming'
};

export const countries = {
    AF: 'Afghanistan',
    AX: 'Aland Islands',
    AL: 'Albania',
    DZ: 'Algeria',
    AS: 'American Samoa',
    AD: 'Andorra',
    AO: 'Angola',
    AI: 'Anguilla',
    AQ: 'Antarctica',
    AG: 'Antigua And Barbuda',
    AR: 'Argentina',
    AM: 'Armenia',
    AW: 'Aruba',
    AU: 'Australia',
    AT: 'Austria',
    AZ: 'Azerbaijan',
    BS: 'Bahamas',
    BH: 'Bahrain',
    BD: 'Bangladesh',
    BB: 'Barbados',
    BY: 'Belarus',
    BE: 'Belgium',
    BZ: 'Belize',
    BJ: 'Benin',
    BM: 'Bermuda',
    BT: 'Bhutan',
    BO: 'Bolivia',
    BA: 'Bosnia And Herzegovina',
    BW: 'Botswana',
    BV: 'Bouvet Island',
    BR: 'Brazil',
    IO: 'British Indian Ocean Territory',
    BN: 'Brunei Darussalam',
    BG: 'Bulgaria',
    BF: 'Burkina Faso',
    BI: 'Burundi',
    KH: 'Cambodia',
    CM: 'Cameroon',
    CA: 'Canada',
    CV: 'Cape Verde',
    KY: 'Cayman Islands',
    CF: 'Central African Republic',
    TD: 'Chad',
    CL: 'Chile',
    CN: 'China',
    CX: 'Christmas Island',
    CC: 'Cocos (Keeling) Islands',
    CO: 'Colombia',
    KM: 'Comoros',
    CG: 'Congo',
    CD: 'Congo, Democratic Republic',
    CK: 'Cook Islands',
    CR: 'Costa Rica',
    CI: "Cote D'Ivoire",
    HR: 'Croatia',
    CU: 'Cuba',
    CY: 'Cyprus',
    CZ: 'Czech Republic',
    DK: 'Denmark',
    DJ: 'Djibouti',
    DM: 'Dominica',
    DO: 'Dominican Republic',
    EC: 'Ecuador',
    EG: 'Egypt',
    SV: 'El Salvador',
    GQ: 'Equatorial Guinea',
    ER: 'Eritrea',
    EE: 'Estonia',
    ET: 'Ethiopia',
    FK: 'Falkland Islands (Malvinas)',
    FO: 'Faroe Islands',
    FJ: 'Fiji',
    FI: 'Finland',
    FR: 'France',
    GF: 'French Guiana',
    PF: 'French Polynesia',
    TF: 'French Southern Territories',
    GA: 'Gabon',
    GM: 'Gambia',
    GE: 'Georgia',
    DE: 'Germany',
    GH: 'Ghana',
    GI: 'Gibraltar',
    GR: 'Greece',
    GL: 'Greenland',
    GD: 'Grenada',
    GP: 'Guadeloupe',
    GU: 'Guam',
    GT: 'Guatemala',
    GG: 'Guernsey',
    GN: 'Guinea',
    GW: 'Guinea-Bissau',
    GY: 'Guyana',
    HT: 'Haiti',
    HM: 'Heard Island & Mcdonald Islands',
    VA: 'Holy See (Vatican City State)',
    HN: 'Honduras',
    HK: 'Hong Kong',
    HU: 'Hungary',
    IS: 'Iceland',
    IN: 'India',
    ID: 'Indonesia',
    IR: 'Iran, Islamic Republic Of',
    IQ: 'Iraq',
    IE: 'Ireland',
    IM: 'Isle Of Man',
    IL: 'Israel',
    IT: 'Italy',
    JM: 'Jamaica',
    JP: 'Japan',
    JE: 'Jersey',
    JO: 'Jordan',
    KZ: 'Kazakhstan',
    KE: 'Kenya',
    KI: 'Kiribati',
    KR: 'Korea',
    KW: 'Kuwait',
    KG: 'Kyrgyzstan',
    LA: "Lao People's Democratic Republic",
    LV: 'Latvia',
    LB: 'Lebanon',
    LS: 'Lesotho',
    LR: 'Liberia',
    LY: 'Libyan Arab Jamahiriya',
    LI: 'Liechtenstein',
    LT: 'Lithuania',
    LU: 'Luxembourg',
    MO: 'Macao',
    MK: 'Macedonia',
    MG: 'Madagascar',
    MW: 'Malawi',
    MY: 'Malaysia',
    MV: 'Maldives',
    ML: 'Mali',
    MT: 'Malta',
    MH: 'Marshall Islands',
    MQ: 'Martinique',
    MR: 'Mauritania',
    MU: 'Mauritius',
    YT: 'Mayotte',
    MX: 'Mexico',
    FM: 'Micronesia, Federated States Of',
    MD: 'Moldova',
    MC: 'Monaco',
    MN: 'Mongolia',
    ME: 'Montenegro',
    MS: 'Montserrat',
    MA: 'Morocco',
    MZ: 'Mozambique',
    MM: 'Myanmar',
    NA: 'Namibia',
    NR: 'Nauru',
    NP: 'Nepal',
    NL: 'Netherlands',
    AN: 'Netherlands Antilles',
    NC: 'New Caledonia',
    NZ: 'New Zealand',
    NI: 'Nicaragua',
    NE: 'Niger',
    NG: 'Nigeria',
    NU: 'Niue',
    NF: 'Norfolk Island',
    MP: 'Northern Mariana Islands',
    NO: 'Norway',
    OM: 'Oman',
    PK: 'Pakistan',
    PW: 'Palau',
    PS: 'Palestinian Territory, Occupied',
    PA: 'Panama',
    PG: 'Papua New Guinea',
    PY: 'Paraguay',
    PE: 'Peru',
    PH: 'Philippines',
    PN: 'Pitcairn',
    PL: 'Poland',
    PT: 'Portugal',
    PR: 'Puerto Rico',
    QA: 'Qatar',
    RE: 'Reunion',
    RO: 'Romania',
    RU: 'Russian Federation',
    RW: 'Rwanda',
    BL: 'Saint Barthelemy',
    SH: 'Saint Helena',
    KN: 'Saint Kitts And Nevis',
    LC: 'Saint Lucia',
    MF: 'Saint Martin',
    PM: 'Saint Pierre And Miquelon',
    VC: 'Saint Vincent And Grenadines',
    WS: 'Samoa',
    SM: 'San Marino',
    ST: 'Sao Tome And Principe',
    SA: 'Saudi Arabia',
    SN: 'Senegal',
    RS: 'Serbia',
    SC: 'Seychelles',
    SL: 'Sierra Leone',
    SG: 'Singapore',
    SK: 'Slovakia',
    SI: 'Slovenia',
    SB: 'Solomon Islands',
    SO: 'Somalia',
    ZA: 'South Africa',
    GS: 'South Georgia And Sandwich Isl.',
    ES: 'Spain',
    LK: 'Sri Lanka',
    SD: 'Sudan',
    SR: 'Suriname',
    SJ: 'Svalbard And Jan Mayen',
    SZ: 'Swaziland',
    SE: 'Sweden',
    CH: 'Switzerland',
    SY: 'Syrian Arab Republic',
    SX: 'Sint Maarten',
    TW: 'Taiwan',
    TJ: 'Tajikistan',
    TZ: 'Tanzania',
    TH: 'Thailand',
    TL: 'Timor-Leste',
    TG: 'Togo',
    TK: 'Tokelau',
    TO: 'Tonga',
    TT: 'Trinidad And Tobago',
    TN: 'Tunisia',
    TR: 'Turkey',
    TM: 'Turkmenistan',
    TC: 'Turks And Caicos Islands',
    TV: 'Tuvalu',
    UG: 'Uganda',
    UA: 'Ukraine',
    AE: 'United Arab Emirates',
    GB: 'United Kingdom',
    US: 'United States',
    UM: 'United States Outlying Islands',
    UY: 'Uruguay',
    UZ: 'Uzbekistan',
    VU: 'Vanuatu',
    VE: 'Venezuela',
    VN: 'Viet Nam',
    VG: 'Virgin Islands, British',
    VI: 'Virgin Islands, U.S.',
    WF: 'Wallis And Futuna',
    EH: 'Western Sahara',
    YE: 'Yemen',
    ZM: 'Zambia',
    ZW: 'Zimbabwe'
};

export const podcastCategories = [
    {
        category: 'Arts',
        subcategories: [
            'Design',
            'Fashion & Beauty',
            'Food',
            'Literature',
            'Performing Arts',
            'Visual Arts'
        ]
    },
    {
        category: 'Business',
        subcategories: [
            'Business News',
            'Careers',
            'Investing',
            'Management & Marketing',
            'Shopping'
        ]
    },
    {
        category: 'Comedy',
        subcategories: []
    },
    {
        category: 'Education',
        subcategories: [
            'Educational Technology',
            'Higher Education',
            'K-12',
            'Language Courses',
            'Training'
        ]
    },
    {
        category: 'Games & Hobbies',
        subcategories: [
            'Automotive',
            'Aviation',
            'Hobbies',
            'Other Games',
            'Video Games'
        ]
    },
    {
        category: 'Government & Organizations',
        subcategories: ['Local', 'National', 'Non-Profit', 'Regional']
    },
    {
        category: 'Health',
        subcategories: [
            'Alternative Health',
            'Fitness & Nutrition',
            'Self-Help',
            'Sexuality'
        ]
    },
    {
        category: 'Kids & Family',
        subcategories: []
    },
    {
        category: 'Music',
        subcategories: []
    },
    {
        category: 'News & Politics',
        subcategories: []
    },
    {
        category: 'Religion & Spirituality',
        subcategories: [
            'Buddhism',
            'Christianity',
            'Hinduism',
            'Islam',
            'Judaism',
            'Other',
            'Spirituality'
        ]
    },
    {
        category: 'Science & Medicine',
        subcategories: ['Medicine', 'Natural Sciences', 'Social Sciences']
    },
    {
        category: 'Society & Culture',
        subcategories: [
            'History',
            'Personal Journals',
            'Philosophy',
            'Places & Travel'
        ]
    },
    {
        category: 'Sports & Recreation',
        subcategories: [
            'Amateur',
            'College & High School',
            'Outdoor',
            'Professional'
        ]
    },
    {
        category: 'Technology',
        subcategories: ['Gadgets', 'Tech News', 'Podcasting', 'Software How-To']
    },
    {
        category: 'TV & Film',
        subcategories: []
    }
];

export const chromecastMessageTypes = {
    idle: 'idle',
    ended: 'ended'
};

export const podcastLanguages = {
    aa: 'Afar',
    ab: 'Abkhazian',
    af: 'Afrikaans',
    ak: 'Akan',
    sq: 'Albanian',
    am: 'Amharic',
    ar: 'Arabic',
    an: 'Aragonese',
    hy: 'Armenian',
    as: 'Assamese',
    av: 'Avaric',
    ae: 'Avestan',
    ay: 'Aymara',
    az: 'Azerbaijani',
    ba: 'Bashkir',
    bm: 'Bambara',
    eu: 'Basque',
    be: 'Belarusian',
    bn: 'Bengali',
    bh: 'Bihari languages',
    bi: 'Bislama',
    bo: 'Tibetan',
    bs: 'Bosnian',
    br: 'Breton',
    bg: 'Bulgarian',
    my: 'Burmese',
    ca: 'Catalan; Valencian',
    cs: 'Czech',
    ch: 'Chamorro',
    ce: 'Chechen',
    zh: 'Chinese',
    cu:
        'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic',
    cv: 'Chuvash',
    kw: 'Cornish',
    co: 'Corsican',
    cr: 'Cree',
    cy: 'Welsh',
    da: 'Danish',
    de: 'German',
    dv: 'Divehi; Dhivehi; Maldivian',
    nl: 'Dutch; Flemish',
    dz: 'Dzongkha',
    el: 'Greek, Modern (1453-)',
    en: 'English',
    eo: 'Esperanto',
    et: 'Estonian',
    ee: 'Ewe',
    fo: 'Faroese',
    fa: 'Persian',
    fj: 'Fijian',
    fi: 'Finnish',
    fr: 'French',
    fy: 'Western Frisian',
    ff: 'Fulah',
    ka: 'Georgian',
    gd: 'Gaelic; Scottish Gaelic',
    ga: 'Irish',
    gl: 'Galician',
    gv: 'Manx',
    gn: 'Guarani',
    gu: 'Gujarati',
    ht: 'Haitian; Haitian Creole',
    ha: 'Hausa',
    he: 'Hebrew',
    hz: 'Herero',
    hi: 'Hindi',
    ho: 'Hiri Motu',
    hr: 'Croatian',
    hu: 'Hungarian',
    ig: 'Igbo',
    is: 'Icelandic',
    io: 'Ido',
    ii: 'Sichuan Yi; Nuosu',
    iu: 'Inuktitut',
    ie: 'Interlingue; Occidental',
    ia: 'Interlingua (International Auxiliary Language Association)',
    id: 'Indonesian',
    ik: 'Inupiaq',
    it: 'Italian',
    jv: 'Javanese',
    ja: 'Japanese',
    kl: 'Kalaallisut; Greenlandic',
    kn: 'Kannada',
    ks: 'Kashmiri',
    kr: 'Kanuri',
    kk: 'Kazakh',
    km: 'Central Khmer',
    ki: 'Kikuyu; Gikuyu',
    rw: 'Kinyarwanda',
    ky: 'Kirghiz; Kyrgyz',
    kv: 'Komi',
    kg: 'Kongo',
    ko: 'Korean',
    kj: 'Kuanyama; Kwanyama',
    ku: 'Kurdish',
    lo: 'Lao',
    la: 'Latin',
    lv: 'Latvian',
    li: 'Limburgan; Limburger; Limburgish',
    ln: 'Lingala',
    lt: 'Lithuanian',
    lb: 'Luxembourgish; Letzeburgesch',
    lu: 'Luba-Katanga',
    lg: 'Ganda',
    mk: 'Macedonian',
    mh: 'Marshallese',
    ml: 'Malayalam',
    mi: 'Maori',
    mr: 'Marathi',
    ms: 'Malay',
    mg: 'Malagasy',
    mt: 'Maltese',
    mn: 'Mongolian',
    na: 'Nauru',
    nv: 'Navajo; Navaho',
    nr: 'Ndebele, South; South Ndebele',
    nd: 'Ndebele, North; North Ndebele',
    ng: 'Ndonga',
    ne: 'Nepali',
    nn: 'Norwegian Nynorsk; Nynorsk, Norwegian',
    nb: 'Bokmål, Norwegian; Norwegian Bokmål',
    no: 'Norwegian',
    ny: 'Chichewa; Chewa; Nyanja',
    oc: 'Occitan (post 1500)',
    oj: 'Ojibwa',
    or: 'Oriya',
    om: 'Oromo',
    os: 'Ossetian; Ossetic',
    pa: 'Panjabi; Punjabi',
    pi: 'Pali',
    pl: 'Polish',
    pt: 'Portuguese',
    ps: 'Pushto; Pashto',
    qu: 'Quechua',
    rm: 'Romansh',
    ro: 'Romanian; Moldavian; Moldovan',
    rn: 'Rundi',
    ru: 'Russian',
    sg: 'Sango',
    sa: 'Sanskrit',
    si: 'Sinhala; Sinhalese',
    sk: 'Slovak',
    sl: 'Slovenian',
    se: 'Northern Sami',
    sm: 'Samoan',
    sn: 'Shona',
    sd: 'Sindhi',
    so: 'Somali',
    st: 'Sotho, Southern',
    es: 'Spanish; Castilian',
    sc: 'Sardinian',
    sr: 'Serbian',
    ss: 'Swati',
    su: 'Sundanese',
    sw: 'Swahili',
    sv: 'Swedish',
    ty: 'Tahitian',
    ta: 'Tamil',
    tt: 'Tatar',
    te: 'Telugu',
    tg: 'Tajik',
    tl: 'Tagalog',
    th: 'Thai',
    ti: 'Tigrinya',
    to: 'Tonga (Tonga Islands)',
    tn: 'Tswana',
    ts: 'Tsonga',
    tk: 'Turkmen',
    tr: 'Turkish',
    tw: 'Twi',
    ug: 'Uighur; Uyghur',
    uk: 'Ukrainian',
    ur: 'Urdu',
    uz: 'Uzbek',
    ve: 'Venda',
    vi: 'Vietnamese',
    vo: 'Volapük',
    wa: 'Walloon',
    wo: 'Wolof',
    xh: 'Xhosa',
    yi: 'Yiddish',
    yo: 'Yoruba',
    za: 'Zhuang; Chuang',
    zu: 'Zulu'
};

export const genders = {
    male: 'Male',
    female: 'Female',
    'non-binary': 'Non-Binary'
};

export const tagSections = {
    adminOnly: 'Admin Only',
    defaultOption: 'Default',
    mood: 'Mood',
    locations: 'Locations',
    subgenre: 'Sub-Genre'
};

export const blogSlugToTitle = {
    features: 'Features',
    videos: 'Videos',
    news: 'News and Updates',
    'for-artists': 'Audiomack for Artists'
};

export const currencySymbols = {
    USD: '$',
    EUR: '€',
    CRC: '₡',
    GBP: '£',
    ILS: '₪',
    INR: '₹',
    JPY: '¥',
    KRW: '₩',
    NGN: '₦',
    PHP: '₱',
    PLN: 'zł',
    PYG: '₲',
    THB: '฿',
    UAH: '₴',
    VND: '₫'
};
