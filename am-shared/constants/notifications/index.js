export const verbDisplayTextMap = {
    favorite: 'favorited',
    playlisted: 'playlisted',
    comment: 'commented on',
    follow: 'followed',
    reup: 'reupped',
    upload: 'uploaded',
    playlistfavorite: 'favorited',
    playlist_updated: 'updated the playlist,',
    playlist_updated_bundle: 'playlists updated,'
};

export const settingsLabelsMap = {
    'email.weekly_summary': 'Weekly Summary',
    // 'email.trending': 'Trending',
    // 'email.benchmark_play_count': 'Play Count Benchmarks',
    // 'push.new_follower': 'New Followers',
    'push.new_music_for_follower': 'New Music'
};
