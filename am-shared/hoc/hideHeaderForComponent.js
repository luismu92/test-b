import React from 'react';
import hoistStatics from 'hoist-non-react-statics';

const timerKey = '__hiddenHeader';

export default function hideHeaderForComponent(Component) {
    class HiddenHeaderComponent extends React.Component {
        static displayName = `HiddenHeaderComponent[${Component.displayName}]`;

        componentDidMount() {
            this._headerWasShowing = !document.body.classList.contains(
                'has-hidden-header'
            );
            document.body.classList.add('has-hidden-header');

            clearTimeout(window[timerKey]);
        }

        componentWillUnmount() {
            // Make sure if we're navigating to a component that also needs
            // the header hidden, we don't show then hide again
            window[timerKey] = setTimeout(() => {
                window.requestAnimationFrame(() => {
                    if (this._headerWasShowing) {
                        document.body.classList.remove('has-hidden-header');
                    }

                    this._headerWasShowing = null;
                });
            }, 100);
        }

        render() {
            return <Component {...this.props} />;
        }
    }

    return hoistStatics(HiddenHeaderComponent, Component);
}
