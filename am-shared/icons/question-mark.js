/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class QuestionMark extends Component {
    render() {
        return (
            <svg { ...this.props }  width="5" height="8" viewBox="0 0 5 8"><path d="M3.12 4.55c0 .068-.055.12-.12.12H1.743c-.033 0-.122-.088-.122-.122v-.5c0-.523.284-.984.866-1.408.046-.032.482-.317.482-.71 0-.326-.245-.554-.597-.554-.5 0-.78.25-.805.73-.004.065-.057.116-.122.116H.123c-.034 0-.066-.014-.09-.038C.012 2.16 0 2.126 0 2.094.07.764.933 0 2.373 0 3.463 0 4.64.592 4.64 1.893c0 .87-.2 1.036-.888 1.47-.474.31-.615.576-.63 1.188zm.24 2.57c0 .135-.11.245-.246.245H1.622c-.135 0-.245-.11-.245-.245V5.65c0-.135.11-.245.245-.245h1.492c.135 0 .245.11.245.245v1.47z" fillRule="evenodd"/></svg>

        );
    }
}
