import { setInfinitePlaybackFetcher } from '../modules/player';

/**
 * Looks for the `playerFetcher` key within an action to enable the player
 * to auto fetch the next page of results when it hits the end of the queue.
 * The `page` key is also required
 */
export default function middleware() {
    return (next) => (action) => {
        const { playerFetcher, page } = action;

        if (!playerFetcher) {
            return next(action);
        }

        if (typeof playerFetcher !== 'function' || playerFetcher.length !== 1) {
            console.warn(
                'The `playerFetcher` key needs to be a function that accepts a single `page` parameter.'
            );
            return next(action);
        }

        if (typeof page !== 'number') {
            console.warn(
                'The `playerFetcher` key was found on the action but the `page` key was not. Skipping.'
            );
            return next(action);
        }

        const fetcher = createInfiniteFetcher(playerFetcher, page);

        next(setInfinitePlaybackFetcher(fetcher));

        return next(action);
    };
}

function createInfiniteFetcher(fetcherFn, initialPage = 1) {
    let currentPage = initialPage;

    return () => {
        currentPage += 1;

        // fetcherFn might need to take care of
        // dynamically getting token/secret each time in case user
        // logs out or in
        return fetcherFn(currentPage);
    };
}
