import { LOCATION_CHANGE } from 'connected-react-router';
import { matchPath } from 'react-router';

import { SET_SECTION } from '../modules/stats';

const pathToSectionCache = {};

/**
 * Sets the "section" context for purposes of tracking
 * certain metrics across the site based on where
 * you're located
 *
 * @param  {Array} routeConfig route config from the routes file
 * @return {[type]}             [description]
 */
export default function sectionMiddleware(routeConfig) {
    return () => (next) => (action) => {
        const { type, ...rest } = action;

        if (type === LOCATION_CHANGE) {
            const { pathname } = action.payload;

            // Call original location change action
            next(action); // eslint-disable-line callback-return

            // Set the section based on location
            if (pathToSectionCache[pathname]) {
                return next({
                    ...rest,
                    section: pathToSectionCache[pathname],
                    type: SET_SECTION
                });
            }

            const routeMatch = routeConfig.find(
                ({ path: routePath, exact, strict }) => {
                    return (
                        routePath &&
                        routePath !== '*' &&
                        matchPath(pathname, {
                            path: routePath,
                            exact,
                            strict
                        })
                    );
                }
            );

            if (routeMatch) {
                const section = routeMatch.render().props.route.section;

                pathToSectionCache[pathname] = section;

                return next({
                    ...rest,
                    section: section,
                    type: SET_SECTION
                });
            }
        }

        return next(action);
    };
}
