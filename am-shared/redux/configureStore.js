import { createStore, applyMiddleware, compose } from 'redux';
import { createMemoryHistory, createBrowserHistory } from 'history';
import { routerMiddleware, routerActions } from 'connected-react-router';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from './middleware/promiseMiddleware';
import sectionMiddleware from './middleware/sectionMiddleware';
import playerFetcherMiddleware from './middleware/playerFetcherMiddleware';

// const history = process.env.NODE_ENV === 'development' ? createHashHistory() : createBrowserHistory();
export const history = process.env.BROWSER
    ? createBrowserHistory()
    : createMemoryHistory();

export function configureStore(initialState, rootReducer, routeConfig) {
    const middlewares = [
        thunkMiddleware,
        promiseMiddleware,
        routerMiddleware(history),
        sectionMiddleware(routeConfig),
        playerFetcherMiddleware
    ];

    if (process.env.NODE_ENV === 'development') {
        if (
            process.env.BROWSER &&
            localStorage.getItem('REDUX_LOGGER') !== 'false'
        ) {
            const { createLogger } = require('redux-logger');
            const logger = createLogger();

            middlewares.push(logger);
        }
    }

    // Redux DevTools Configuration
    const actionCreators = {
        // ...counterActions,
        ...routerActions
    };

    // If Redux DevTools Extension is installed use it, otherwise use Redux compose
    /* eslint-disable no-underscore-dangle */
    const composeEnhancers =
        typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
                  // Options: http://zalmoxisus.github.io/redux-devtools-extension/API/Arguments.html
                  actionCreators
              })
            : compose;

    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(...middlewares))
    );

    return store;
}
