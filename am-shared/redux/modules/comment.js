import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import {
    ORDER_BY_VOTE,
    LIMIT_PER_PAGE,
    KIND_MANAGEMENT_RECENT
} from 'constants/comment';

const PREFIX = 'am/comment/';

export const SET_CONTEXT = `${PREFIX}SET_CONTEXT`;
export const UPDATE_PAGE = `${PREFIX}UPDATE_PAGE`;
export const GET_COMMENTS = `${PREFIX}GET_COMMENTS`;
export const POST_COMMENT = `${PREFIX}POST_COMMENT`;
export const DELETE_COMMENT = `${PREFIX}DELETE_COMMENT`;
export const REPORT_COMMENT = `${PREFIX}REPORT_COMMENT`;
export const BAN_COMMENT = `${PREFIX}BAN_COMMENT`;
export const UNFLAG_COMMENT = `${PREFIX}UNFLAG_COMMENT`;
export const GET_VOTE_STATUS = `${PREFIX}GET_VOTE_STATUS`;
export const POST_VOTES = `${PREFIX}POST_VOTES`;
export const ADD_VOTE = `${PREFIX}ADD_VOTE`;

const defaultState = {
    comments: [],
    kind: null,
    id: null,
    orderBy: ORDER_BY_VOTE,
    singleCommentUuid: null,
    currentPage: 0,
    total: 0,
    nextPage: 1,
    error: null,
    loading: false,
    userVotes: {}, // The vote status for each comments. It is keyed by kind-id.
    userVotesToSync: [] // The votes that need to be synced with server
};

function getKey(kind, id) {
    return `${kind}-${id}`;
}

// eslint-disable-next-line complexity
export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case SET_CONTEXT: {
            return {
                ...state,
                comments: defaultState.comments,
                kind: action.contextKind,
                id: action.contextId,
                orderBy: action.orderBy,
                singleCommentUuid: action.singleCommentUuid,
                currentPage: defaultState.currentPage,
                nextPage: defaultState.nextPage,
                userVotesToSync: []
            };
        }

        case requestSuffix(GET_COMMENTS): {
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };
        }

        case failSuffix(GET_COMMENTS): {
            return {
                ...state,
                error: 'Sorry, but failed to get comments',
                loading: false
            };
        }

        case GET_COMMENTS: {
            const { result, total } = action.resolved;

            // Set page params
            const nextPage =
                result.length < LIMIT_PER_PAGE ? null : state.nextPage + 1;
            const currentPage = state.currentPage + 1;
            const comments = result.filter(
                (comment) => typeof comment.uuid !== 'undefined'
            );

            return {
                ...state,
                error: defaultState.error,
                loading: false,
                comments: state.comments.concat(comments),
                nextPage,
                currentPage,
                total: total
            };
        }

        case requestSuffix(POST_COMMENT): {
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };
        }

        case failSuffix(POST_COMMENT): {
            return {
                ...state,
                error: action.error || 'Sorry, but failed to add your comment',
                loading: false
            };
        }

        case POST_COMMENT: {
            let comments = state.comments;

            if (typeof action.resolved === 'undefined') {
                return {
                    ...state,
                    error:
                        action.error ||
                        'Sorry, but failed to add your comment right now. Please check back later.',
                    loading: false
                };
            }

            if (
                typeof action.resolved.thread !== 'undefined' &&
                action.resolved.thread !== null
            ) {
                comments = comments.map((item) => {
                    if (item.uuid === action.resolved.thread) {
                        if (typeof item.children === 'undefined') {
                            item.children = [];
                        }
                        // Append new child comment to the end
                        return Object.assign(item, {
                            children: [action.resolved].concat(item.children)
                        });
                    }

                    return item;
                });
            } else {
                // Append new thread to the top
                comments = [action.resolved].concat(state.comments);
            }

            return {
                ...state,
                error: defaultState.error,
                loading: false,
                comments
            };
        }

        case requestSuffix(DELETE_COMMENT): {
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };
        }

        case failSuffix(DELETE_COMMENT): {
            return {
                ...state,
                error: action.error || 'Sorry, but failed to delete comment',
                loading: false
            };
        }

        case DELETE_COMMENT: {
            const comments = state.comments.map((item) => {
                return {
                    ...item,
                    deleted: item.uuid === action.uuid ? true : item.deleted,
                    children: (item.children || []).map((child) => {
                        return {
                            ...child,
                            deleted:
                                child.uuid === action.uuid
                                    ? true
                                    : child.deleted
                        };
                    })
                };
            });

            return {
                ...state,
                error: defaultState.error,
                loading: false,
                comments
            };
        }

        case requestSuffix(UNFLAG_COMMENT): {
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };
        }

        case failSuffix(UNFLAG_COMMENT): {
            return {
                ...state,
                error:
                    action.error ||
                    'Sorry, but failed to clear flag for comment',
                loading: false
            };
        }

        case UNFLAG_COMMENT: {
            const comments = state.comments.map((item) => {
                return {
                    ...item,
                    deleted: item.uuid === action.uuid ? true : item.deleted
                };
            });

            return {
                ...state,
                error: defaultState.error,
                loading: false,
                comments
            };
        }

        case requestSuffix(REPORT_COMMENT): {
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };
        }

        case failSuffix(REPORT_COMMENT): {
            return {
                ...state,
                error: action.error || 'Sorry, but failed to report comment',
                loading: false
            };
        }

        case REPORT_COMMENT: {
            return {
                ...state,
                errors: defaultState.error,
                loading: false
            };
        }

        case requestSuffix(BAN_COMMENT): {
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };
        }

        case failSuffix(BAN_COMMENT): {
            return {
                ...state,
                error: action.error || 'Sorry, but failed to ban user',
                loading: false
            };
        }

        case BAN_COMMENT: {
            const bannedUser = parseInt(action.user, 10);
            const comments = state.comments.map((item) => {
                return {
                    ...item,
                    artist: {
                        ...item.artist,
                        comment_banned:
                            item.user_id === bannedUser
                                ? true
                                : item.artist.comment_banned // eslint-disable-line
                    },
                    children: (item.children || []).map((child) => {
                        return {
                            ...child,
                            artist: {
                                ...child.artist,
                                comment_banned:
                                    child.user_id === bannedUser
                                        ? true
                                        : child.artist.comment_banned // eslint-disable-line
                            }
                        };
                    })
                };
            });

            return {
                ...state,
                error: defaultState.error,
                loading: false,
                comments
            };
        }

        case requestSuffix(GET_VOTE_STATUS): {
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };
        }

        case failSuffix(GET_VOTE_STATUS): {
            return {
                ...state,
                loading: false
            };
        }

        case GET_VOTE_STATUS: {
            const { kind, id } = state;
            const key = getKey(kind, id);

            return {
                ...state,
                userVotes: {
                    ...state.userVotes,
                    [key]: action.resolved
                },
                loading: false
            };
        }

        case ADD_VOTE: {
            const { userVotesToSync, userVotes, kind, id, comments } = state;
            const key = getKey(kind, id);
            const newVote = {
                uuid: action.uuid,
                thread: action.thread,
                vote_up: action.voteUp
            }; // eslint-disable-line
            let newUserVotesToSync =
                userVotesToSync !== null ? userVotesToSync : [];
            const newUserVotes = Object.assign({}, userVotes);
            let changeVote = false;
            let addVoteToSync = false;

            if (typeof newUserVotes[key] === 'undefined') {
                newUserVotes[key] = [];
            }

            newUserVotes[key].map((existingVote) => {
                if (existingVote.uuid === newVote.uuid) {
                    changeVote = true;
                }
            });

            if (changeVote) {
                newUserVotesToSync = newUserVotesToSync.map((item) => {
                    if (item.uuid === newVote.uuid) {
                        item.vote_up = newVote.vote_up; // eslint-disable-line
                        addVoteToSync = true;
                    }
                    return item;
                });
                newUserVotes[key] = newUserVotes[key].map((item) => {
                    if (item.uuid === newVote.uuid) {
                        item.vote_up = newVote.vote_up; // eslint-disable-line
                    }
                    return item;
                });
            } else {
                newUserVotes[key].push(newVote);
            }

            if (!addVoteToSync) {
                newUserVotesToSync.push(newVote);
            }

            const changeCount = changeVote ? 2 : 1;
            const changeUpDownCount = changeVote ? 1 : 0;

            // Update comment's vote count
            const newComments = comments.map((item) => {
                if (item.uuid === action.uuid) {
                    item.vote_total = newVote.vote_up
                        ? item.vote_total + changeCount
                        : item.vote_total - changeCount; // eslint-disable-line
                    item.vote_up = newVote.vote_up
                        ? item.vote_up + 1
                        : item.vote_up - changeUpDownCount; // eslint-disable-line
                    item.vote_down = newVote.vote_up
                        ? item.vote_down + 1
                        : item.vote_down - changeUpDownCount; // eslint-disable-line
                }

                if (Array.isArray(item.children) && item.children.length > 0) {
                    const newChildren = item.children.map((childItem) => {
                        if (childItem.uuid === action.uuid) {
                            childItem.vote_total = newVote.vote_up
                                ? childItem.vote_total + changeCount
                                : childItem.vote_total - changeCount; // eslint-disable-line
                            childItem.vote_up = newVote.vote_up
                                ? childItem.vote_up + 1
                                : childItem.vote_up - changeUpDownCount; // eslint-disable-line
                            childItem.vote_down = newVote.vote_up
                                ? childItem.vote_down + 1
                                : childItem.vote_down - changeUpDownCount; // eslint-disable-line
                        }

                        return childItem;
                    });

                    item.children = newChildren;
                }
                return item;
            });

            return {
                ...state,
                comments: newComments,
                userVotes: newUserVotes,
                userVotesToSync: newUserVotesToSync
            };
        }

        case POST_VOTES: {
            return {
                ...state,
                userVotesToSync: []
            };
        }

        default:
            return state;
    }
}

export function setCommentContext(
    kind,
    id,
    orderBy = ORDER_BY_VOTE,
    singleCommentUuid
) {
    return (dispatch, getState) => {
        const state = getState();
        const currentKind = state.comment.kind;
        const currentId = state.comment.id;
        const currentOrderBy = state.comment.orderBy;
        const currentSingleCommentUuid = state.comment.singleCommentUuid;

        // Don't need to reset context
        if (
            currentKind !== KIND_MANAGEMENT_RECENT &&
            currentId === id &&
            kind === currentKind &&
            orderBy === currentOrderBy &&
            singleCommentUuid === currentSingleCommentUuid
        ) {
            return false;
        }

        return dispatch({
            type: SET_CONTEXT,
            contextKind: kind,
            contextId: id,
            orderBy,
            singleCommentUuid
        });
    };
}

export function getComments(singleCommentUuid, singleCommentThread) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { orderBy, kind, id, nextPage } = state.comment;

        return dispatch({
            type: GET_COMMENTS,
            promise: api.comment.get(
                kind,
                id,
                orderBy,
                nextPage,
                token,
                secret,
                singleCommentUuid,
                singleCommentThread
            )
        });
    };
}

export function postComment(content, thread = null) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { kind, id } = state.comment;
        const comment = { kind, id, content, thread };

        return dispatch({
            type: POST_COMMENT,
            promise: api.comment.post(comment, token, secret)
        });
    };
}

export function deleteComment(
    uuid,
    thread = null,
    targetKind = null,
    targetId = null
) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { kind, id } = state.comment;
        const comment = {
            kind: targetKind !== null ? targetKind : kind,
            id: targetId !== null ? targetId : id,
            uuid,
            thread
        };

        return dispatch({
            type: DELETE_COMMENT,
            uuid,
            thread,
            promise: api.comment.delete(comment, token, secret)
        });
    };
}

export function unflagComment(uuid, thread = null, kind, id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const comment = { kind, id, uuid, thread };

        return dispatch({
            type: UNFLAG_COMMENT,
            uuid,
            thread,
            promise: api.comment.unflag(comment, token, secret)
        });
    };
}

export function reportComment(uuid, thread = null) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { kind, id } = state.comment;
        const comment = { kind, id, uuid, thread };

        return dispatch({
            type: REPORT_COMMENT,
            uuid,
            thread,
            promise: api.comment.report(comment, token, secret)
        });
    };
}

export function banComment(user) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: BAN_COMMENT,
            user,
            promise: api.comment.ban(user, token, secret)
        });
    };
}

// Get user's vote status
export function getUserVoteStatus() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { kind, id } = state.comment;

        return dispatch({
            type: GET_VOTE_STATUS,
            promise: api.comment.getUserVoteStatus(kind, id, token, secret)
        });
    };
}

// Sync server with users newest votes
export function postUserVotes() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { userVotesToSync, kind, id } = state.comment;
        const votes = {
            kind,
            id,
            votes: userVotesToSync
        };

        return dispatch({
            type: POST_VOTES,
            promise: api.comment.postVotes(votes, token, secret)
        });
    };
}

// Update state with user's vote status
export function addUserVote(uuid, thread = null, voteUp = true) {
    return (dispatch) => {
        return dispatch({
            type: ADD_VOTE,
            uuid,
            thread,
            voteUp
        });
    };
}
