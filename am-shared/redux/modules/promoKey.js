import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/promoKey/';

const GET_PROMO_KEYS = `${PREFIX}GET_PROMO_KEYS`;
const ADD_PROMO_KEYS = `${PREFIX}ADD_PROMO_KEYS`;
const DELETE_PROMO_KEYS = `${PREFIX}DELETE_PROMO_KEYS`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    loading: false,
    errors: [],
    list: []
};

// Should probably think about using immutableJS in this project
export default function musicReducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PROMO_KEYS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_PROMO_KEYS): {
            return {
                ...state,
                loading: false,
                errors: [action.error]
            };
        }

        case GET_PROMO_KEYS: {
            return {
                ...state,
                loading: false,
                list: action.resolved.results
            };
        }

        case ADD_PROMO_KEYS: {
            return {
                ...state,
                list: action.resolved.results
            };
        }

        case requestSuffix(DELETE_PROMO_KEYS): {
            const newList = Array.from(state.list);

            if (newList[action.index]) {
                newList[action.index].isDeleting = true;
            }

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(DELETE_PROMO_KEYS): {
            const newList = Array.from(state.list);

            if (newList[action.index]) {
                newList[action.index].isDeleting = false;
            }

            return {
                ...state,
                list: newList
            };
        }

        case DELETE_PROMO_KEYS: {
            const newList = state.list.filter((obj, i) => {
                return i !== action.index;
            });

            return {
                ...state,
                list: newList
            };
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getPromoKeys(id) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const promise = api.music.getPromoKeys(id, token, secret);

        return dispatch({
            type: GET_PROMO_KEYS,
            promise
        });
    };
}

export function addPromoKey(id, key) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const promise = api.music.addPromoKey(id, key, token, secret);

        return dispatch({
            type: ADD_PROMO_KEYS,
            promise
        });
    };
}

export function deletePromoKey(id, key, index) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const promise = api.music.deletePromoKey(id, key, token, secret);

        return dispatch({
            type: DELETE_PROMO_KEYS,
            promise,
            index: parseInt(index, 10)
        });
    };
}

export function reset() {
    return {
        type: RESET
    };
}
