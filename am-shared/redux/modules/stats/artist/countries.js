import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/artist/countries/';

const GET_COUNTRY_STATS = `${PREFIX}GET_COUNTRY_STATS`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;

const defaultState = {
    loading: false,
    error: null,
    data: [],
    page: 1,
    onLastPage: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_COUNTRY_STATS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_COUNTRY_STATS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_COUNTRY_STATS: {
            let newData = action.resolved.data;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.data.concat(newData);
            }

            if (
                (state.page !== 1 && !action.resolved.data.length) ||
                action.resolved.data.length < action.limit
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                ...state,
                data: newData,
                loading: false,
                page: state.page,
                onLastPage: onLastPage
            });
        }

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case CLEAR_LIST:
            return {
                ...state,
                data: [],
                loading: false,
                page: 1,
                onLastPage: false
            };

        default:
            return state;
    }
}

export function getCountryStats(artistId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_COUNTRY_STATS,
            promise: api.stats.artistCountryStats(
                token,
                secret,
                artistId,
                options
            )
        });
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
