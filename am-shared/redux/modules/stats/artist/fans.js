import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/stats/artist/fans/';

const GET_FANS = `${PREFIX}GET_FANS`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;

const defaultState = {
    loading: false,
    error: null,
    data: [],
    page: 1,
    onLastPage: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_FANS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_FANS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_FANS: {
            let newData = action.resolved.data;
            let onLastPage = false;

            if (action.page !== 1) {
                newData = state.data.concat(newData);
            }

            if (
                (action.page !== 1 && !action.resolved.data.length) ||
                action.resolved.data.length < action.limit
            ) {
                onLastPage = true;
            }

            return {
                ...state,
                data: newData,
                loading: false,
                page: action.page,
                onLastPage
            };
        }

        case CLEAR_LIST: {
            return defaultState;
        }

        default:
            return state;
    }
}

export function getFans(artistId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const page = options.page || getState().statsArtistFans.page || 1;

        return dispatch({
            type: GET_FANS,
            page,
            promise: api.stats
                .artistTopFans(token, secret, artistId, options)
                .then((response) => {
                    const artistIds = response.data.map(
                        (obj) => obj.fan_artist_id
                    );
                    const artistFetches = api.artist.getById(artistIds);

                    return artistFetches.then(({ results: artists }) => {
                        const artistIdMap = artists.reduce((acc, artist) => {
                            acc[artist.id] = artist;
                            return acc;
                        }, {});

                        return {
                            data: response.data.map((obj) => {
                                return {
                                    ...obj,
                                    info: artistIdMap[obj.fan_artist_id] || null
                                };
                            })
                        };
                    });
                })
        });
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
