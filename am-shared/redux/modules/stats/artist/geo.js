import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/artist/geo/';

const GET_COUNTRY_PLAYS = `${PREFIX}GET_COUNTRY_PLAYS`;

const defaultState = {
    loading: false,
    error: null,
    data: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_COUNTRY_PLAYS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_COUNTRY_PLAYS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_COUNTRY_PLAYS:
            return {
                ...state,
                data: action.resolved.data,
                loading: false
            };

        default:
            return state;
    }
}

export function getCountryPlays(artistId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_COUNTRY_PLAYS,
            promise: api.stats.artistCountryPlays(
                token,
                secret,
                artistId,
                options
            )
        });
    };
}
