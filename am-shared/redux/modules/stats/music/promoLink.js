import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/music/promoLink/';

const GET_PROMO_LINKS = `${PREFIX}GET_PROMO_LINKS`;

const defaultState = {
    loading: false,
    error: null,
    data: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PROMO_LINKS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_PROMO_LINKS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_PROMO_LINKS:
            return {
                ...state,
                data: action.resolved.data,
                loading: false
            };

        default:
            return state;
    }
}

export function getPromoLinks(artistId, musicId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_PROMO_LINKS,
            promise: api.stats.musicPromoLinks(
                token,
                secret,
                artistId,
                musicId,
                options
            )
        });
    };
}
