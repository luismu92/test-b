import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/music/playlistAdds/';

const GET_PLAYLIST_ADDS = `${PREFIX}GET_PLAYLIST_ADDS`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;

const defaultState = {
    loading: false,
    error: null,
    data: [],
    page: 1,
    onLastPage: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PLAYLIST_ADDS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_PLAYLIST_ADDS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_PLAYLIST_ADDS: {
            let newData = action.resolved.data;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.data.concat(newData);
            }

            if (state.page !== 1 && !action.resolved.data.length) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                ...state,
                data: newData,
                loading: false,
                page: state.page,
                onLastPage: onLastPage
            });
        }

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case CLEAR_LIST:
            return {
                ...state,
                data: []
            };

        default:
            return state;
    }
}

export function getPlaylistAdds(artistId, musicId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_PLAYLIST_ADDS,
            promise: api.stats.musicPlaylistAdds(
                token,
                secret,
                artistId,
                musicId,
                options
            )
        });
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
