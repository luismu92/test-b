import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/music/daily/';

const GET_DAILY_STATS = `${PREFIX}GET_DAILY_STATS`;

const defaultState = {
    loading: false,
    error: null,
    data: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_DAILY_STATS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_DAILY_STATS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_DAILY_STATS:
            return {
                ...state,
                data: action.resolved.data,
                loading: false
            };

        default:
            return state;
    }
}

export function getDailyStats(artistId, musicId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_DAILY_STATS,
            promise: api.stats.musicDaily(
                token,
                secret,
                artistId,
                musicId,
                options
            )
        });
    };
}
