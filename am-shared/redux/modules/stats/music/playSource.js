import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { aggregatePlaySources } from 'utils/stats';

export const PREFIX = 'am/stats/music/playSource/';

const GET_PLAY_SOURCES = `${PREFIX}GET_PLAY_SOURCES`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;

const defaultState = {
    loading: false,
    error: null,
    data: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PLAY_SOURCES):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_PLAY_SOURCES):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_PLAY_SOURCES:
            return {
                ...state,
                data: action.resolved.data,
                loading: false
            };

        case CLEAR_LIST:
            return {
                ...state,
                data: []
            };

        default:
            return state;
    }
}

export function getPlaySources(artistId, musicId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_PLAY_SOURCES,
            promise: api.stats
                .musicPlaySources(token, secret, artistId, musicId, options)
                .then((response) => {
                    return {
                        ...response,
                        data: aggregatePlaySources(response.data, 'play')
                    };
                })
        });
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
