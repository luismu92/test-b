import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/music/summary/';

const GET_SUMMARY = `${PREFIX}GET_SUMMARY`;
const GET_MUSIC_TRACK_STATS = `${PREFIX}GET_MUSIC_TRACK_STATS`;

const defaultState = {
    loading: false,
    error: null,
    info: {},
    stats: {},
    tracksLoading: false,
    tracksError: null,
    tracks: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_SUMMARY):
            return {
                ...state,
                tracks: defaultState.tracks,
                loading: true
            };

        case failSuffix(GET_SUMMARY):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_SUMMARY:
            return {
                ...state,
                info: action.resolved.info,
                stats: action.resolved.stats,
                loading: false
            };

        case requestSuffix(GET_MUSIC_TRACK_STATS):
            return {
                ...state,
                tracks: defaultState.tracks,
                tracksLoading: true
            };

        case failSuffix(GET_MUSIC_TRACK_STATS):
            return {
                ...state,
                tracksError: action.error,
                tracksLoading: false
            };

        case GET_MUSIC_TRACK_STATS:
            return {
                ...state,
                tracks: action.resolved.data,
                tracksLoading: false
            };

        default:
            return state;
    }
}

export function getSummary(artistId, musicId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_SUMMARY,
            promise: Promise.all([
                api.music.id(musicId, {
                    token,
                    secret,
                    ...options
                }),
                api.stats.musicSummary(
                    token,
                    secret,
                    artistId,
                    musicId,
                    options
                )
            ]).then((values) => {
                const [infoResponse, statsResponse] = values;
                if (infoResponse.results.type === 'album') {
                    const trackIds = infoResponse.results.tracks.map(
                        (track) => {
                            return track.song_id;
                        }
                    );

                    if (trackIds.length) {
                        dispatch(
                            getMusicTrackStats(artistId, trackIds, options)
                        );
                    }
                }

                return {
                    info: infoResponse.results,
                    stats: statsResponse.data || {}
                };
            })
        });
    };
}

function getMusicTrackStats(artistId, trackIds = [], options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_MUSIC_TRACK_STATS,
            promise: api.stats.musicSummary(
                token,
                secret,
                artistId,
                trackIds,
                options
            )
        });
    };
}
