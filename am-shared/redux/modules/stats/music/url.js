import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/stats/music/url/';

const GET_URLS = `${PREFIX}GET_URLS`;

const defaultState = {
    loading: false,
    error: null,
    data: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_URLS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_URLS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_URLS:
            return {
                ...state,
                data: action.resolved.data,
                loading: false
            };

        default:
            return state;
    }
}

export function getUrls(artistId, musicId, options) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_URLS,
            promise: api.stats.musicUrls(
                token,
                secret,
                artistId,
                musicId,
                options
            )
        });
    };
}
