import api from 'api/index';
import { suffix } from 'utils/index';

const PREFIX = 'am/email/';

const SEND_CONTACT = `${PREFIX}SEND_CONTACT`;
const SEND_API = `${PREFIX}SEND_API`;
const SEND_LEGAL = `${PREFIX}SEND_LEGAL`;

const defaultState = {
    errors: [],
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(SEND_CONTACT, 'REQUEST'): {
            return Object.assign({}, state, {
                loading: true,
                errors: []
            });
        }
        case suffix(SEND_API, 'REQUEST'): {
            return Object.assign({}, state, {
                loading: true,
                errors: []
            });
        }
        case suffix(SEND_LEGAL, 'REQUEST'): {
            return Object.assign({}, state, {
                loading: true,
                errors: []
            });
        }
        case suffix(SEND_CONTACT, 'FAILURE'): {
            return Object.assign({}, state, {
                errors: [action.error],
                loading: false
            });
        }
        case suffix(SEND_API, 'FAILURE'): {
            return Object.assign({}, state, {
                errors: [action.error],
                loading: false
            });
        }
        case suffix(SEND_LEGAL, 'FAILURE'): {
            return Object.assign({}, state, {
                errors: [action.error],
                loading: false
            });
        }

        case SEND_CONTACT: {
            return Object.assign({}, state, {
                errors: [],
                loading: false
            });
        }

        case SEND_API: {
            return Object.assign({}, state, {
                errors: [],
                loading: false
            });
        }

        case SEND_LEGAL: {
            return Object.assign({}, state, {
                errors: [],
                loading: false
            });
        }

        default:
            return state;
    }
}

export function sendContactForm(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: SEND_CONTACT,
            promise: api.email.contact({
                ...options,
                token,
                secret
            })
        });
    };
}

export function sendApiForm(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: SEND_API,
            promise: api.email.api({
                ...options,
                token,
                secret
            })
        });
    };
}

export function sendLegalForm(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: SEND_LEGAL,
            promise: api.email.legal({
                ...options,
                token,
                secret
            })
        });
    };
}
