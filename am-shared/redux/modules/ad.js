import Cookie from 'js-cookie';
import { cookies } from 'constants/index';

export const PREFIX = 'am/ad/';

const AD_SCRIPT_LOADED = `${PREFIX}AD_SCRIPT_LOADED`;
const DELAY_ACTION_FOR_AD = `${PREFIX}DELAY_ACTION_FOR_AD`;
const ACTIVATE_AD = `${PREFIX}ACTIVATE_AD`;
const DEACTIVATE_AD = `${PREFIX}DEACTIVATE_AD`;
const PLAY_QUEUED_ACTION = `${PREFIX}PLAY_QUEUED_ACTION`;
const SET_LAST_DISPLAY_TIME = `${PREFIX}SET_LAST_DISPLAY_TIME`;
const SET_ERROR = `${PREFIX}SET_ERROR`;

const initialState = {
    loaded: false,
    active: false,
    error: false,
    lastDisplayed: 0,
    queuedAction: null
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case AD_SCRIPT_LOADED:
            return Object.assign({}, state, {
                loaded: true
            });

        case DELAY_ACTION_FOR_AD:
            return Object.assign({}, state, {
                queuedAction: action.action
            });

        case ACTIVATE_AD:
            return Object.assign({}, state, {
                active: true
            });

        case PLAY_QUEUED_ACTION:
            return Object.assign({}, state, {
                queuedAction: initialState.queuedAction
            });

        case SET_LAST_DISPLAY_TIME:
            return Object.assign({}, state, {
                lastDisplayed: action.time
            });

        case DEACTIVATE_AD:
            return Object.assign({}, state, {
                active: false,
                lastDisplayed: action.time || Date.now(),
                error: action.error ? action.error : state.error
            });

        case SET_ERROR:
            return {
                ...state,
                error: action.error
            };

        default:
            return state;
    }
}

export function adLoaded() {
    return {
        type: AD_SCRIPT_LOADED
    };
}

export function delayAction(action) {
    return {
        type: DELAY_ACTION_FOR_AD,
        action
    };
}

export function activateAd() {
    return (dispatch, getState) => {
        const { currentUser } = getState();

        if (
            currentUser.isLoggedIn &&
            currentUser.profile.subscription !== null
        ) {
            return null;
        }

        return dispatch({
            type: ACTIVATE_AD
        });
    };
}

export function playQueuedAction() {
    return (dispatch, getState) => {
        const action = getState().ad.queuedAction;

        if (action && typeof action === 'function') {
            action();
        }

        return dispatch({
            type: PLAY_QUEUED_ACTION
        });
    };
}

export function setLastDisplayTime(time = 0) {
    return {
        type: SET_LAST_DISPLAY_TIME,
        time: parseInt(time, 10) || 0
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error
    };
}

export function deactivateAd() {
    return (dispatch) => {
        const lastTime = Date.now();

        dispatch({
            type: DEACTIVATE_AD,
            time: lastTime
        });

        Cookie.set(cookies.lastAdDisplayTime, lastTime, {
            path: '/',
            sameSite: 'strict'
        });

        return dispatch(playQueuedAction());
    };
}
