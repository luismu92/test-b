const SHOW_MODAL = 'am/modal/SHOW_MODAL';
const HIDE_MODAL = 'am/modal/HIDE_MODAL';

export const MODAL_TYPE_AUTH = 'auth';
export const MODAL_TYPE_CONTACT = 'contact';
export const MODAL_TYPE_ADD_TO_PLAYLIST = 'add-to-playlist';
export const MODAL_TYPE_EMBED = 'embed';
export const MODAL_TYPE_VIDEO = 'video';
export const MODAL_TYPE_ADMIN = 'admin';
export const MODAL_TYPE_CONFIRM = 'confirm';
export const MODAL_TYPE_VALIDATE_AGREEMENT = 'validate-agreement';
export const MODAL_TYPE_VALIDATE_ARTIST = 'validate-artist';
export const MODAL_TYPE_ALERT = 'alert';
export const MODAL_TYPE_TRENDING = 'trending';
export const MODAL_TYPE_BLOCK = 'block';
export const MODAL_TYPE_PROMO = 'promo';
export const MODAL_TYPE_TAGS = 'tags';
export const MODAL_TYPE_LABEL_SIGNUP = 'label-signup';
export const MODAL_TYPE_AMP_CODES = 'amp-codes';
export const MODAL_TYPE_DELETE_ACCOUNT = 'delete-account';
export const MODAL_TYPE_VERIFY_HASH = 'verify-hash';
export const MODAL_TYPE_GHOST_CLAIM = 'ghost-claim';
export const MODAL_TYPE_VERIFY_PASSWORD_TOKEN = 'verify-password-token';
export const MODAL_TYPE_CREATE_NEW_ARTIST = 'create-new-associated-artist';
export const MODAL_TYPE_QUEUE_ALERT = 'queue-alert';
export const MODAL_TYPE_SAVE_TO_PLAYLIST = 'save-to-playlist';
export const MODAL_TYPE_QUEUE_DUPLICATE = 'queue-duplicate';
export const MODAL_TYPE_TEXT_ME_APP = 'text-me-app';
export const MODAL_TYPE_ALBUM_INFO = 'album-info';
export const MODAL_TYPE_SONG_INFO = 'song-info';
export const MODAL_TYPE_ARTIST_INFO = 'artist-info';
export const MODAL_TYPE_PLAYLIST_INFO = 'playlist-info';
export const MODAL_TYPE_APP_DOWNLOAD = 'app-download';
export const MODAL_TYPE_COPY_URL = 'copy-url';
export const MODAL_TYPE_VERIFY_EMAIL = 'verify-email';
export const MODAL_TYPE_SEND_CLAIM_PROFILE_EMAIL = 'send-claim-profile-email';
export const MODAL_TYPE_MUSIC_TAGS = 'music-tags';
export const MODAL_TYPE_CHANGE_RELEASE = 'change-release';
export const MODAL_TYPE_CHOOSE_PREVIOUS_SONGS = 'choose-previous-songs';
export const MODAL_TYPE_ABOUT_WORLD = 'about-world';
export const MODAL_TYPE_ALBUM_FINISH_UPLOAD = 'album-finish-upload';
export const MODAL_TYPE_SUGGEST_TAG = 'suggest-tag';

const initialState = {
    type: null,
    extraData: {}
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SHOW_MODAL:
        case HIDE_MODAL:
            return Object.assign({}, state, {
                type: action.modalType,
                extraData: action.extraData
            });

        default:
            return state;
    }
}

export function showModal(modalType = initialState.type, extraData = {}) {
    return {
        type: SHOW_MODAL,
        modalType,
        extraData
    };
}

export function hideModal() {
    return {
        type: HIDE_MODAL,
        modalType: null,
        extraData: {}
    };
}
