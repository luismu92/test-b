import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { ADD_PINNED, DELETE_PINNED } from '../user/pinned';

const PREFIX = 'am/artist/pinned/';

export const GET_PINNED = `${PREFIX}GET_PINNED`;

const defaultState = {
    list: [],
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PINNED): {
            return {
                ...state,
                errors: [],
                loading: true
            };
        }

        case failSuffix(GET_PINNED): {
            return {
                ...state,
                error: action.error,
                loading: false
            };
        }

        case GET_PINNED: {
            return {
                ...state,
                error: defaultState.error,
                list: action.resolved,
                loading: false
            };
        }

        case requestSuffix(DELETE_PINNED):
        case requestSuffix(ADD_PINNED): {
            if (!action.ownsPage) {
                return state;
            }

            return {
                ...state,
                list: action.newList
            };
        }

        case failSuffix(DELETE_PINNED):
        case failSuffix(ADD_PINNED): {
            if (!action.ownsPage) {
                return state;
            }

            return {
                ...state,
                list: action.oldList
            };
        }

        default:
            return state;
    }
}

export function getPinned(artistSlug = false) {
    return (dispatch, getState) => {
        const state = getState();
        const { profile } = state.currentUser;
        const slug = artistSlug !== false ? artistSlug : profile.url_slug;
        const ownsPage = profile && profile.url_slug === slug;

        return dispatch({
            type: GET_PINNED,
            slug,
            ownsPage,
            promise: api.artist.getPinned(slug)
        });
    };
}
