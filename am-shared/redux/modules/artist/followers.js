import api from 'api/index';
import { FOLLOW_ARTIST, UNFOLLOW_ARTIST } from '../user';
import { failSuffix, requestSuffix } from 'utils/index';

const PREFIX = 'am/artist/followers/';
const GET_ARTIST_FOLLOWERS = `${PREFIX}GET_ARTIST_FOLLOWERS`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;
const defaultState = {
    list: [],
    page: 1,
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ARTIST_FOLLOWERS): {
            return {
                ...state,
                errors: [],
                loading: true
            };
        }

        case failSuffix(GET_ARTIST_FOLLOWERS): {
            return {
                ...state,
                errors: state.errors.concat(action.error),
                loading: true
            };
        }

        case SET_PAGE:
            return {
                ...state,
                page: action.page
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case GET_ARTIST_FOLLOWERS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;
            let page = action.page;

            if (state.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                state.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
                page = state.page;
            }

            return {
                ...state,
                list: newData,
                page,
                onLastPage,
                loading: false
            };
        }

        case failSuffix(UNFOLLOW_ARTIST):
        case requestSuffix(FOLLOW_ARTIST): {
            const newList = state.list.map((obj) => {
                const newObj = {
                    ...obj
                };

                if (obj.id === action.artist.id) {
                    newObj.followers_count += 1; // eslint-disable-line
                }

                return newObj;
            });

            if (action.isArtistActive) {
                newList.unshift(action.user);
            }

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(FOLLOW_ARTIST):
        case requestSuffix(UNFOLLOW_ARTIST): {
            const newList = state.list
                .map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.artist.id) {
                        newObj.followers_count -= 1; // eslint-disable-line
                    }

                    return newObj;
                })
                .filter((follower) => {
                    if (
                        action.isArtistActive &&
                        follower.id === action.user.id
                    ) {
                        return false;
                    }

                    return true;
                });

            return {
                ...state,
                list: newList
            };
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getArtistFollowers(slug, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().artistFollowers;
        const { limit = 20, page = state.page } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_ARTIST_FOLLOWERS,
            page: finalPage,
            limit: finalLimit,
            promise: api.artist.followers(slug, {
                page: finalPage,
                limit: finalLimit,
                token,
                secret
            })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
