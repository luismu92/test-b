import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

import { TRACK_PLAY } from '../stats';

const PREFIX = 'am/artist/uploads/';

const GET_ARTIST_UPLOADS = `${PREFIX}GET_ARTIST_UPLOADS`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    list: [],
    page: 1,
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ARTIST_UPLOADS):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case failSuffix(GET_ARTIST_UPLOADS):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case GET_ARTIST_UPLOADS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (action.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                (action.page !== 1 && !results.length) ||
                action.resolved.results.length === 0
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                list: newData,
                page: action.page,
                onLastPage,
                loading: false
            });
        }

        case TRACK_PLAY: {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };
                    const itemId = action.id;
                    const albumId = action.options.album_id;

                    if (
                        (obj.type === 'album' &&
                            typeof albumId === 'number' &&
                            obj.id === albumId) ||
                        (obj.type !== 'album' && obj.id === itemId)
                    ) {
                        // Only update if this is the first track play call, ie not the 30 second track call
                        if (!action.options.time) {
                            newObj.stats['plays-raw'] += 1;
                        }
                    }

                    return newObj;
                })
            });
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getArtistUploads(slug, options = {}) {
    return (dispatch, getState) => {
        const state = getState().artistUploads;
        const { limit, page = state.page } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_ARTIST_UPLOADS,
            page: finalPage,
            limit: finalLimit,
            promise: api.artist.uploads(slug, finalPage, finalLimit),
            playerFetcher: (p) => api.artist.uploads(slug, p, finalLimit)
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
