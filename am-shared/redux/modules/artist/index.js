import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { FOLLOW_ARTIST, UNFOLLOW_ARTIST, SAVE_USER_DETAILS } from '../user';

export const PREFIX = 'am/artist/index/';

const GET_ARTIST = `${PREFIX}GET_ARTIST`;
const defaultState = {
    profile: null,
    loading: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ARTIST): {
            return {
                ...state,
                errors: [],
                loading: true
            };
        }

        case GET_ARTIST:
            return {
                ...state,
                profile: action.resolved.results,
                loading: false
            };

        case failSuffix(GET_ARTIST): {
            return {
                ...state,
                profile: defaultState.profile,
                errors: state.errors.concat(action.error),
                loading: false
            };
        }

        case SAVE_USER_DETAILS:
            if (state.profile && state.profile.id === action.resolved.id) {
                return {
                    ...state,
                    profile: Object.assign({}, state.profile, action.resolved)
                };
            }

            return state;

        case FOLLOW_ARTIST: {
            if (!state.profile) {
                return state;
            }

            const artist = {
                ...state.profile
            };

            if (artist.id === action.artist.id) {
                artist.followers_count += 1; // eslint-disable-line
            }

            if (artist.id === action.user.id) {
                artist.following_count += 1; // eslint-disable-line
            }

            return {
                ...state,
                profile: artist
            };
        }

        case UNFOLLOW_ARTIST: {
            if (!state.profile) {
                return state;
            }

            const artist = {
                ...state.profile
            };

            if (artist.id === action.artist.id) {
                artist.followers_count -= 1; // eslint-disable-line
            }

            if (artist.id === action.user.id) {
                artist.following_count -= 1; // eslint-disable-line
            }

            return {
                ...state,
                profile: artist
            };
        }

        default:
            return state;
    }
}

export function getArtist(slug) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_ARTIST,
            force404OnError: /^\/artist\//,
            promise: api.artist.get(slug, token, secret)
        });
    };
}
