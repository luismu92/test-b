import api from 'api/index';
import { failSuffix, requestSuffix } from 'utils/index';
import { FOLLOW_ARTIST, UNFOLLOW_ARTIST } from '../user/index';

const PREFIX = 'am/artist/following/';

const GET_ARTIST_FOLLOWING = `${PREFIX}GET_ARTIST_FOLLOWING`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    list: [],
    page: 1,
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ARTIST_FOLLOWING, 'REQUEST'):
            return {
                ...state,
                errors: [],
                loading: true
            };

        case failSuffix(GET_ARTIST_FOLLOWING, 'FAILURE'):
            return {
                ...state,
                errors: state.errors.concat(action.error),
                loading: false
            };

        case SET_PAGE:
            return {
                ...state,
                page: action.page
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case failSuffix(UNFOLLOW_ARTIST):
        case requestSuffix(FOLLOW_ARTIST):
            return {
                ...state,
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.artist.id) {
                        newObj.followers_count += 1; // eslint-disable-line
                    }

                    return newObj;
                })
            };

        case failSuffix(FOLLOW_ARTIST):
        case requestSuffix(UNFOLLOW_ARTIST):
            return {
                ...state,
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.artist.id) {
                        newObj.followers_count -= 1; // eslint-disable-line
                    }

                    return newObj;
                })
            };

        case GET_ARTIST_FOLLOWING: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;
            let page = action.page;

            if (action.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (
                action.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                page = state.page;
                onLastPage = true;
            }

            return {
                ...state,
                list: newData,
                loading: false,
                page,
                onLastPage
            };
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getArtistFollowing(slug, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().artistFollowing;
        const { limit = 20, page = state.page } = options;

        return dispatch({
            type: GET_ARTIST_FOLLOWING,
            page,
            limit,
            promise: api.artist.following(slug, { page, limit, token, secret })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
