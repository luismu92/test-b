import api from 'api/index';
import { suffix } from 'utils/index';

const PREFIX = 'am/artist/favorites/';

const GET_ARTIST_FAVORITES = `${PREFIX}GET_ARTIST_FAVORITES`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    list: [],
    page: 1,
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(GET_ARTIST_FAVORITES, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(GET_ARTIST_FAVORITES, 'FAILURE'):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case GET_ARTIST_FAVORITES: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;
            let page = action.page;

            if (action.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                action.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
                page = state.page;
            }

            return Object.assign({}, state, {
                list: newData,
                page,
                onLastPage,
                loading: false
            });
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getArtistFavorites(slug, options = {}) {
    return (dispatch, getState) => {
        const state = getState().artistFavorites;
        const { limit = 20, page = state.page, show } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_ARTIST_FAVORITES,
            page: finalPage,
            limit: finalLimit,
            promise: api.artist.favorites(slug, finalPage, finalLimit, show)
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
