import { parse } from 'query-string';

import api from 'api/index';
import analytics, {
    eventCategory,
    eventAction,
    eventLabel,
    branchEvents
} from 'utils/analytics';
import {
    normalizeMusicItem,
    clamp,
    shuffle,
    requestSuffix,
    failSuffix,
    getFlattendedQueueFromApiItems,
    getNormalizedQueueTrack,
    getUploader,
    getArtistName
} from 'utils';
import oneSecondSong from 'utils/oneSecond';
import PlaySession from 'utils/PlaySession';
import storage, {
    STORAGE_MUSIC_HISTORY,
    STORAGE_ACTIVE_PLAYER_ID
} from 'utils/storage';

import { TRACK_PLAY, trackPlay } from './stats';
import { delayAction, activateAd } from './ad';

export const PREFIX = 'am/player/';

const QUEUE_BACKLOG_AMOUNT = 5;
const QUEUE_HISTORY_AMOUNT = 5;

const EDIT_QUEUE = `${PREFIX}EDIT_QUEUE`;
const SONG_LOADED = `${PREFIX}SONG_LOADED`;
const SET_REPEAT = `${PREFIX}SET_REPEAT`;
const PLAY = `${PREFIX}PLAY`;
const ENDED = `${PREFIX}ENDED`;
const STOP = `${PREFIX}STOP`;
const PAUSE = `${PREFIX}PAUSE`;
const SEEK = `${PREFIX}SEEK`;
const NEXT = `${PREFIX}NEXT`;
const PREV = `${PREFIX}PREV`;
const TIME_UPDATE = `${PREFIX}TIME_UPDATE`;
const SET_MUSIC_ITEM = `${PREFIX}SET_MUSIC_ITEM`;
const SET_VOLUME = `${PREFIX}SET_VOLUME`;
const SHUFFLE_TRACKS = `${PREFIX}SHUFFLE_TRACKS`;
const MUTE = `${PREFIX}MUTE`;
const UNMUTE = `${PREFIX}UNMUTE`;
const REFRESH_CURRENT_PLAYLIST_ITEM = `${PREFIX}REFRESH_CURRENT_PLAYLIST_ITEM`;
const LOAD_MUSIC_FROM_STORAGE = `${PREFIX}LOAD_MUSIC_FROM_STORAGE`;
const CLEAR_QUEUE = `${PREFIX}CLEAR_QUEUE`;
const REMOVE_QUEUE_ITEM = `${PREFIX}REMOVE_QUEUE_ITEM`;
const ADD_QUEUE_ITEM = `${PREFIX}ADD_QUEUE_ITEM`;
const SET_CHROMECAST_STATE = `${PREFIX}SET_CHROMECAST_STATE`;
const SET_CHROMECAST_LOADING = `${PREFIX}SET_CHROMECAST_LOADING`;
const SET_INFINITE_PLAYBACK_FETCHER = `${PREFIX}SET_INFINITE_PLAYBACK_FETCHER`;
const SET_INFINITE_PLAYBACK_ENDED_STATE = `${PREFIX}SET_INFINITE_PLAYBACK_ENDED_STATE`;

const defaultState = {
    queue: [],
    queueHasBeenEdited: false,
    repeat: false,
    placeholderSong: null,
    // @todo rename this to currentMusicItem since it could be an album/playlist
    // and is not always an object with type song.
    currentSong: null,

    // Correlates to the current item in the queue
    queueIndex: 0,

    // Flag for tracking a play for the currentSong
    trackedPlay: false,

    // Flag for tracking a 30 second listen duration for the currentSong
    trackedThirtySecondPlay: false,

    currentTime: 0,
    duration: 0,
    volume: 1,
    muted: false,
    paused: true,
    loading: false,
    ended: false,
    hidden: false,
    seeking: false,
    chromecastActive: false,
    chromecastLoading: false,
    // While browsing around the site, this may be populated with a fetcher
    // function that facilitates infinite playback, however, it is only used
    // when a user explicitly presses play. This allows for a user to press play
    // within some context and browse around the site without having the potentially
    // auto added queue items be from a random context just because they were
    // on that page at some point. Admittedly, this may not be the cleanest
    // implementation though
    infinitePlaybackFetcher: null,
    infinitePlaybackEnded: false
};

const playSession = new PlaySession();

// eslint-disable-next-line complexity
export default function playerReducer(state = defaultState, action) {
    switch (action.type) {
        case PLAY: {
            return {
                ...state,
                paused: false,
                queueIndex:
                    action.queueIndex !== null
                        ? action.queueIndex
                        : state.queueIndex
            };
        }

        // While song is loading, we set the song src to a one second
        // silent mp3 so that on mobile, there is something playing after
        // user interaction. When this request completes, currentSong.streamingUrl
        // will be replaced with the real url.
        case requestSuffix(SET_MUSIC_ITEM):
            return {
                ...state,
                placeholderSong: oneSecondSong
            };

        case SET_VOLUME:
            return {
                ...state,
                volume: action.volume,
                muted: false
            };

        case SET_REPEAT:
            return {
                ...state,
                repeat: action.repeat
            };

        case SET_MUSIC_ITEM: {
            const nextState = Object.assign({}, defaultState, state);
            const musicItem = action.resolved;

            nextState.currentSong = musicItem;
            nextState.placeholderSong = defaultState.placeholderSong;
            nextState.loading = true;
            nextState.trackedPlay = false;
            nextState.trackedThirtySecondPlay = false;
            nextState.ended = false;

            if (!state.currentSong || state.currentSong.id !== musicItem.id) {
                nextState.currentTime = 0;
            }

            return nextState;
        }

        case ENDED:
            return {
                ...state,
                ended: true
            };

        case PAUSE:
            return {
                ...state,
                paused: true
            };

        case STOP:
            return {
                ...state,
                paused: true,
                queueIndex: 0,
                currentTime: 0,
                currentSong: state.queue[0]
            };

        case SEEK:
            return {
                ...state,
                currentTime: action.toTime,
                seeking: true
            };

        case SONG_LOADED:
            return {
                ...state,
                duration: action.song.duration,
                loading: false
            };

        case EDIT_QUEUE: {
            let newQueue = getFlattendedQueueFromApiItems(action.queue);
            let queueHasBeenEdited = state.queueHasBeenEdited;
            let infinitePlaybackEnded = state.infinitePlaybackEnded;

            if (action.atIndex !== null) {
                newQueue = Array.from(state.queue).splice(
                    action.atIndex,
                    0,
                    newQueue
                );
            } else if (action.append) {
                newQueue = Array.from(state.queue).concat(newQueue);
                infinitePlaybackEnded = false;
            } else {
                const lastFivePlayedItems = state.queue
                    .filter((item) => {
                        return !!item.startedPlaying;
                    })
                    .slice(-QUEUE_BACKLOG_AMOUNT);
                const userQueuedTracks = state.queue.filter((item) => {
                    return !!item.userQueuedTrack;
                });

                // Insert user added tracks after the track that
                // was just clicked on to be played
                newQueue.splice(1, 0, ...userQueuedTracks);

                newQueue = lastFivePlayedItems.concat(newQueue);

                queueHasBeenEdited = false;
            }

            const geoFiltered = newQueue.filter((item) => !item.geo_restricted);

            return {
                ...state,
                queue: geoFiltered,
                queueHasBeenEdited,
                infinitePlaybackEnded
            };
        }

        case CLEAR_QUEUE: {
            const queue = state.queue;

            return {
                ...state,
                queueIndex: 0,
                queue: queue.filter((_, i) => {
                    return i === state.queueIndex;
                }),
                infinitePlaybackEnded: true
            };
        }

        case ADD_QUEUE_ITEM: {
            if (!action.item) {
                return state;
            }

            const newQueue = Array.from(state.queue);
            let newQueueItems;

            if (action.item && !isNaN(action.trackIndex)) {
                newQueueItems = [
                    getNormalizedQueueTrack(action.item, action.trackIndex)
                ];
            } else {
                newQueueItems = getFlattendedQueueFromApiItems([action.item]);
            }

            newQueueItems = newQueueItems.map((item) => {
                return {
                    ...item,
                    userQueuedTrack: true
                };
            });

            if (!isNaN(action.atIndex)) {
                newQueue.splice(action.atIndex, 0, ...newQueueItems);
            } else {
                newQueue.push(...newQueueItems);
            }

            const geoFiltered = newQueue.filter((item) => !item.geo_restricted);

            return {
                ...state,
                queue: geoFiltered
            };
        }

        case REMOVE_QUEUE_ITEM: {
            if (
                isNaN(action.index) ||
                action.index < 0 ||
                action.index > state.queue.length - 1
            ) {
                return state;
            }

            const queueIndex = state.queueIndex;
            const newQueue = Array.from(state.queue);

            newQueue.splice(action.index, 1);

            const newState = {
                ...state,
                queueHasBeenEdited: queueIndex < action.index,
                queueIndex: state.queueIndex,
                queue: newQueue
            };

            if (action.index < queueIndex) {
                newState.queueIndex -= 1;
            }

            if (action.index === queueIndex) {
                newState.currentTime = 0;
                newState.currentSong = newState.queue[newState.queueIndex];
            }

            return newState;
        }

        case TIME_UPDATE: {
            let currentSong = state.queue[state.queueIndex] || {};
            let newQueue = state.queue;

            if (!currentSong.startedPlaying) {
                newQueue = Array.from(state.queue);
                currentSong = {
                    ...currentSong,
                    startedPlaying: true,
                    userQueuedTrack: false
                };
            }

            return {
                ...state,
                ended:
                    state.currentTime &&
                    state.duration &&
                    state.currentTime >= state.duration,
                queue: newQueue,
                currentSong,
                currentTime: action.time,
                seeking: false
            };
        }

        case SET_CHROMECAST_STATE:
            return {
                ...state,
                paused: !action.active,
                chromecastActive: action.active,
                chromecastLoading: action.active
                    ? state.chromecastLoading
                    : false
            };

        case SET_CHROMECAST_LOADING:
            return {
                ...state,
                chromecastLoading: action.loading
            };

        case MUTE:
            return {
                ...state,
                muted: true
            };

        case UNMUTE:
            return {
                ...state,
                muted: false
            };

        case NEXT: {
            const nextState = Object.assign({}, defaultState, state);
            const nextQueueIndex = state.queueIndex + 1;

            nextState.queueIndex = nextQueueIndex;
            nextState.currentTime = 0;

            let nextSong = state.queue[nextQueueIndex];

            nextState.currentSong = nextSong;

            if (!nextSong) {
                nextState.queueIndex = 0;
                nextSong = state.queue[0];
                nextState.currentSong = state.queue[state.queueIndex];
            }

            return nextState;
        }

        case PREV: {
            const nextState = Object.assign({}, defaultState, state);
            const currentQueueIndex = state.queueIndex;
            const prevQueueIndex = currentQueueIndex - 1;
            const prevSong =
                state.queue[prevQueueIndex] ||
                state.queue[state.queue.length - 1];
            const currentSong = state.currentSong;

            nextState.queueIndex = prevQueueIndex;
            nextState.currentSong = prevSong;

            if (nextState.queueIndex < 0) {
                nextState.queueIndex = state.queue.length - 1;
            }

            // Go to beginning of song is currentTime is over 4 seconds
            if (state.currentTime > 4) {
                nextState.queueIndex = currentQueueIndex;
                nextState.currentSong = currentSong;
            }

            nextState.currentTime = 0;
            return nextState;
        }

        case SHUFFLE_TRACKS: {
            const indexToShuffleFrom = state.queueIndex + 1;
            const firstQueuePart = state.queue.slice(0, indexToShuffleFrom);
            const secondQueuePart = shuffle(
                state.queue.slice(indexToShuffleFrom)
            );
            const newQueue = firstQueuePart.concat(secondQueuePart);

            return {
                ...state,
                queue: newQueue
            };
        }

        case requestSuffix(TRACK_PLAY): {
            const isSameSong = action.id === state.currentSong.id;
            const newTrackPlay = isSameSong ? true : state.trackedPlay;
            const newThirtySecondPlay =
                isSameSong && action.options.time === 30
                    ? true
                    : state.trackedPlay;

            return {
                ...state,
                trackedPlay: newTrackPlay,
                trackedThirtySecondPlay: newThirtySecondPlay
            };
        }

        case failSuffix(TRACK_PLAY): {
            const isSameSong = action.id === state.currentSong.id;
            const newTrackPlay = isSameSong ? false : state.trackedPlay;
            const newThirtySecondPlay =
                isSameSong && action.options.time === 30
                    ? false
                    : state.trackedPlay;

            return {
                ...state,
                trackedPlay: newTrackPlay,
                trackedThirtySecondPlay: newThirtySecondPlay
            };
        }

        case REFRESH_CURRENT_PLAYLIST_ITEM: {
            const { results } = action.resolved;
            const queueFromItem = getFlattendedQueueFromApiItems([results]);
            const ids = queueFromItem.map((i) => i.id);

            // Replace all queue items from the potential album/playlist we just fetched
            const newQueue = Array.from(state.queue).map((item) => {
                if (ids.includes(item.id)) {
                    const index = ids.indexOf(item.id);

                    return {
                        ...item,
                        ...queueFromItem[index],
                        refreshAttempted: true
                    };
                }

                return item;
            });

            return {
                ...state,
                queue: newQueue
            };
        }

        case LOAD_MUSIC_FROM_STORAGE: {
            const { resolved: musicItems } = action;
            const historyItems = getFlattendedQueueFromApiItems(musicItems).map(
                (item) => {
                    // Keep loaded history items from being cleared out
                    return {
                        ...item,
                        startedPlaying: true
                    };
                }
            );
            const newQueue = historyItems.concat(Array.from(state.queue));

            const newState = {
                ...state,
                currentSong: historyItems[historyItems.length - 1],
                queue: newQueue,
                queueIndex: historyItems.length - 1
            };

            if (!state.paused) {
                newState.currentSong = state.currentSong;
                newState.queueIndex = state.queueIndex + historyItems.length;
            }

            return newState;
        }

        case SET_INFINITE_PLAYBACK_FETCHER:
            return {
                ...state,
                infinitePlaybackFetcher: action.fetcherFn
            };

        case SET_INFINITE_PLAYBACK_ENDED_STATE:
            return {
                ...state,
                infinitePlaybackEnded: action.isEnded
            };

        default:
            return state;
    }
}

function trackPlayCall(dispatch, getState, isThirtySecondPlay = false) {
    const { player, stats, currentUser, router } = getState();
    const { currentSong } = player;
    const locationKey = parse(router.location.search).key;
    const songId = currentSong.id;
    let albumId;
    let playlistId;
    let uploader = getUploader(currentSong);
    let type = currentSong.type;

    if (currentSong.parentDetails) {
        const id = currentSong.parentDetails.id;

        uploader = getUploader(currentSong.parentDetails);
        type = currentSong.parentDetails.type;

        if (type === 'album') {
            albumId = id;
        } else if (type === 'playlist') {
            playlistId = id;
        }
    }

    const options = {
        session: stats.sessionId,
        album_id: albumId,
        playlist_id: playlistId,
        token: currentUser.token,
        secret: currentUser.secret,
        key: locationKey,
        environment: stats.environment,
        section: currentSong.activeSection
    };

    if (isThirtySecondPlay) {
        options.time = 30;
        analytics.trackBranchData({
            [branchEvents.genre30]: currentSong.genre,
            [branchEvents.musicType30]: type,
            [branchEvents.artist30]: getArtistName(currentSong),
            [branchEvents.image30]: currentSong.image,
            [branchEvents.uploader30]: uploader.name,
            [branchEvents.uploaderImage30]: uploader.image,
            [branchEvents.userGenre30]: currentUser.isLoggedIn
                ? currentUser.profile.genre
                : null,
            [branchEvents.userUploads30]: currentUser.isLoggedIn
                ? currentUser.profile.upload_count
                : null
        });
    }

    dispatch(trackPlay(songId, options));
}

function setStorageFromMusicItem(musicItem) {
    const store = {
        id: musicItem.id,
        type: musicItem.type
    };

    if (!isNaN(musicItem.trackIndex)) {
        store.trackIndex = musicItem.trackIndex;
    }

    if (musicItem.parentDetails) {
        store.id = musicItem.parentDetails.id;
        store.type = musicItem.parentDetails.type;
    }

    const json = true;
    const currentStorage = storage.get(STORAGE_MUSIC_HISTORY, json);

    let newStorage = [];

    if (currentStorage) {
        if (Array.isArray(currentStorage)) {
            newStorage = currentStorage;
        } else {
            newStorage.push(currentStorage);
        }
    }

    // Dont double save items
    newStorage = newStorage.filter((item) => {
        if (item.type !== store.type) {
            return true;
        }

        if (item.id !== store.id) {
            return true;
        }

        return item.trackIndex !== store.trackIndex;
    });

    newStorage.push(store);

    storage.set(
        STORAGE_MUSIC_HISTORY,
        newStorage.slice(-QUEUE_HISTORY_AMOUNT),
        json
    );
}

let isFetchingInfininte = false;
// Set within play()
let fetcher;

function maybeFetchNextMusic(dispatch, getState) {
    const state = getState().player;
    const isOnLastSong = state.queueIndex === state.queue.length - 1;

    if (!isOnLastSong || typeof fetcher !== 'function') {
        return;
    }

    if (state.infinitePlaybackEnded || isFetchingInfininte) {
        return;
    }

    isFetchingInfininte = true;

    fetcher()
        .then((results) => {
            if (!Array.isArray(results)) {
                console.warn('Results not an array!', results);
                return;
            }

            if (!results.length) {
                dispatch(setInfinitePlaybackEndedState(true));
                return;
            }

            dispatch(editQueue(results, { append: true }));
            return;
        })
        .catch((err) => {
            dispatch(setInfinitePlaybackEndedState(true));
            console.error(err);
        })
        .finally(() => {
            isFetchingInfininte = false;
        });
}

export function editQueue(queue, { atIndex = null, append = false } = {}) {
    return (dispatch, getState) => {
        let queueArr = queue;

        if (!Array.isArray(queue)) {
            queueArr = [queue];
        }

        queueArr = queueArr.map((result) => {
            let tracks;

            if (result.tracks) {
                tracks = result.tracks.map((track) => {
                    return {
                        ...track,
                        activeSection: getState().stats.section
                    };
                });
            }

            return {
                ...result,
                tracks,
                activeSection: getState().stats.section
            };
        });

        dispatch({
            type: EDIT_QUEUE,
            queue: queueArr,
            append,
            atIndex
        });

        const newState = getState().player;

        return newState;
    };
}

export function removeQueueItem(index = null) {
    return {
        type: REMOVE_QUEUE_ITEM,
        index: index === null ? undefined : parseInt(index, 10)
    };
}

export function addQueueItem(item, { trackIndex = null, atIndex = null } = {}) {
    return (dispatch, getState) => {
        let tracks;

        if (item.tracks) {
            tracks = item.tracks.map((track) => {
                return {
                    ...track,
                    activeSection: getState().stats.section
                };
            });
        }

        const newItem = {
            ...item,
            tracks,
            activeSection: getState().stats.section
        };

        dispatch({
            type: ADD_QUEUE_ITEM,
            item: newItem,
            trackIndex:
                trackIndex === null ? undefined : parseInt(trackIndex, 10),
            atIndex: atIndex === null ? undefined : parseInt(atIndex, 10)
        });

        const newState = getState().player;

        return newState;
    };
}

export function clearQueue() {
    storage.remove(STORAGE_MUSIC_HISTORY);

    return {
        type: CLEAR_QUEUE
    };
}

export function shuffleTracks() {
    return (dispatch, getState) => {
        dispatch({
            type: SHUFFLE_TRACKS
        });

        const newState = getState().player;

        return newState;
    };
}

export function setMusicItem(musicItem) {
    playSession.reset();

    return {
        type: SET_MUSIC_ITEM,
        song: musicItem,
        promise: normalizeMusicItem(musicItem)
    };
}

export function songLoaded(song) {
    return {
        type: SONG_LOADED,
        song
    };
}

export function play(queueIndex = null) {
    return (dispatch, getState) => {
        const { ad, player, stats } = getState();

        if (!player.queue.length) {
            return Promise.resolve();
        }

        const index = queueIndex !== null ? queueIndex : player.queueIndex;
        const currentMusicItem = player.queue[index || 0];

        function playFn() {
            let promise = Promise.resolve();

            if (!currentMusicItem.normalized) {
                promise = promise.then(() => {
                    return dispatch(setMusicItem(currentMusicItem));
                });
            }

            return promise.then(() => {
                playSession.play(player.currentTime);

                if (stats.sessionId) {
                    storage.set(STORAGE_ACTIVE_PLAYER_ID, stats.sessionId);
                }

                // analytics.trackBranchData(
                //     {
                //         action: 'play'
                //     },
                //     {
                //         disable_entry_animation: false,
                //         disable_exit_animation: false
                //     }
                // );

                fetcher = player.infinitePlaybackFetcher;
                maybeFetchNextMusic(dispatch, getState);

                return dispatch({
                    type: PLAY,
                    queueIndex: index
                });
            });
        }

        if (ad.active) {
            return Promise.resolve();
        }

        if (ad.loaded && !ad.error) {
            dispatch({
                type: PAUSE
            });

            dispatch(delayAction(() => playFn()));
            dispatch(activateAd());
            analytics.track(eventCategory.ad, {
                eventAction: eventAction.videoAdPlay
            });

            return Promise.resolve();
        }

        return playFn();
    };
}

export function stop() {
    return {
        type: STOP
    };
}

export function ended() {
    return (dispatch, getState) => {
        const { player, currentUser } = getState();
        const { currentSong } = player;
        let uploader = getUploader(currentSong);
        let type = currentSong.type;

        if (currentSong.parentDetails) {
            uploader = getUploader(currentSong.parentDetails);
            type = currentSong.parentDetails.type;
        }

        analytics.trackBranchData({
            [branchEvents.genreEnded]: currentSong.genre,
            [branchEvents.musicTypeEnded]: type,
            [branchEvents.artistEnded]: getArtistName(currentSong),
            [branchEvents.imageEnded]: currentSong.image,
            [branchEvents.uploaderEnded]: uploader.name,
            [branchEvents.uploaderImageEnded]: uploader.image,
            [branchEvents.userGenreEnded]: currentUser.isLoggedIn
                ? currentUser.profile.genre
                : null,
            [branchEvents.userUploadsEnded]: currentUser.isLoggedIn
                ? currentUser.profile.upload_count
                : null
        });

        return dispatch({
            type: ENDED
        });
    };
}

export function pause() {
    return {
        type: PAUSE
    };
}

export function seek(toTime) {
    playSession.seek(toTime);

    return {
        type: SEEK,
        toTime: clamp(toTime, 0, Infinity)
    };
}

export function mute() {
    return {
        type: MUTE
    };
}

export function unmute() {
    return {
        type: UNMUTE
    };
}

export function setVolume(volume) {
    return {
        type: SET_VOLUME,
        volume: clamp(parseFloat(volume), 0, 1)
    };
}

export function setRepeat(value) {
    return {
        type: SET_REPEAT,
        repeat: value === true
    };
}

export function next() {
    return (dispatch, getState) => {
        const { ad } = getState();

        if (ad.active) {
            return null;
        }

        const ret = dispatch({
            type: NEXT
        });

        const newPlayerState = getState().player;
        const song = newPlayerState.queue[newPlayerState.queueIndex];

        return dispatch(setMusicItem(song)).then(() => {
            playSession.play(newPlayerState.currentTime);
            maybeFetchNextMusic(dispatch, getState);
            return ret;
        });
    };
}

export function prev() {
    return (dispatch, getState) => {
        const { ad, player } = getState();

        if (ad.active) {
            return null;
        }

        const prevSong = player.queue[player.queueIndex];
        const ret = dispatch({
            type: PREV
        });
        const newPlayerState = getState().player;
        const currentSong = newPlayerState.queue[newPlayerState.queueIndex];

        if (prevSong.id === currentSong.id) {
            return dispatch(seek(0));
        }

        return dispatch(setMusicItem(currentSong)).then(() => {
            playSession.play(newPlayerState.currentTime);
            return ret;
        });
    };
}

export function timeUpdate(time) {
    return (dispatch, getState) => {
        const {
            currentSong,
            seeking,
            currentTime,
            chromecastActive
        } = getState().player;

        if (!currentSong) {
            return null;
        }

        if (!currentSong.startedPlaying) {
            setStorageFromMusicItem(currentSong);
        }

        const newTime = seeking ? currentTime : time;
        const ret = dispatch({
            type: TIME_UPDATE,
            time: newTime
        });

        playSession.timeupdate(newTime);

        const newPlayerState = getState().player;
        const currentUser = getState().currentUser;

        if (!newPlayerState.paused && !chromecastActive) {
            if (!newPlayerState.trackedPlay && newTime > 0) {
                trackPlayCall(dispatch, getState);

                let uploader = getUploader(currentSong);
                let type = currentSong.type;

                if (currentSong.parentDetails) {
                    uploader = getUploader(currentSong.parentDetails);
                    type = currentSong.parentDetails.type;
                }

                analytics.trackBranchData({
                    [branchEvents.genreStarted]: currentSong.genre,
                    [branchEvents.musicTypeStarted]: type,
                    [branchEvents.artistStarted]: getArtistName(currentSong),
                    [branchEvents.imageStarted]: currentSong.image,
                    [branchEvents.uploaderStarted]: uploader.name,
                    [branchEvents.uploaderImageStarted]: uploader.image,
                    [branchEvents.userGenreStarted]: currentUser.isLoggedIn
                        ? currentUser.profile.genre
                        : null,
                    [branchEvents.userUploadsStarted]: currentUser.isLoggedIn
                        ? currentUser.profile.upload_count
                        : null
                });

                if (currentUser.isLoggedIn) {
                    analytics.track(eventCategory.player, {
                        eventAction: eventAction.artistPlay,
                        eventLabel: eventLabel.isUploader,
                        eventValue: currentUser.profile.upload_count > 0 ? 1 : 0
                    });

                    if (currentUser.profile.upload_count > 0) {
                        analytics.track(eventCategory.player, {
                            eventAction: eventAction.artistPlay,
                            eventLabel: eventLabel.isDesktopUploader
                        });
                    }
                }
            }

            const playLimit = 30;
            const isThirtySecondPlay = true;

            if (
                !newPlayerState.trackedThirtySecondPlay &&
                playSession.calculateTime() >= playLimit
            ) {
                trackPlayCall(dispatch, getState, isThirtySecondPlay);
            }
        }

        return ret;
    };
}

export function refreshCurrentPlaylistItem() {
    return (dispatch, getState) => {
        const { currentSong } = getState().player;

        let promise;

        const details = currentSong.parentDetails;

        if (details && details.type === 'playlist') {
            promise = api.playlist.get(details.id);
        } else {
            const id = details ? details.id : currentSong.id;

            promise = api.music.id(id);
        }

        return dispatch({
            type: REFRESH_CURRENT_PLAYLIST_ITEM,
            promise: promise
        });
    };
}

export function loadMusicFromStorage(history) {
    let json = history;

    if (!Array.isArray(history)) {
        json = [history];
    }

    let cache = {};
    // Would be better if we got all playlists and song/album items
    // with at max 2 calls. Maybe in the future we only fetch
    // the last 5 tracks instead of whole items
    const promises = json.map((item) => {
        const key = `${item.type}:${item.id}`;
        let promise = cache[key];

        if (item.type === 'playlist') {
            if (!promise) {
                promise = api.playlist.get(item.id);
                cache[key] = promise;
            }

            return promise.then(({ results }) => {
                return getNormalizedQueueTrack(results, item.trackIndex);
            });
        }

        if (!promise) {
            promise = api.music.id(item.id);
            cache[key] = promise;
        }

        return promise.then(({ results }) => {
            if (results.tracks) {
                return getNormalizedQueueTrack(results, item.trackIndex);
            }

            return results;
        });
    });

    return {
        type: LOAD_MUSIC_FROM_STORAGE,
        lastItem: json[json.length - 1],
        promise: Promise.all(promises).then((values) => {
            cache = null;

            return values;
        })
    };
}

export function setChromecastState(active) {
    if (active) {
        playSession.play();
        playSession.stopCollecting();
    } else {
        playSession.startCollecting();
    }

    return {
        type: SET_CHROMECAST_STATE,
        active
    };
}

export function setChromecastLoading(loading) {
    return {
        type: SET_CHROMECAST_LOADING,
        loading
    };
}

export function setInfinitePlaybackFetcher(fetcherFn) {
    return {
        type: SET_INFINITE_PLAYBACK_FETCHER,
        fetcherFn
    };
}

export function setInfinitePlaybackEndedState(isEnded) {
    return {
        type: SET_INFINITE_PLAYBACK_ENDED_STATE,
        isEnded
    };
}
