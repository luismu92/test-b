import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/world/page/';

const GET_PAGES = `${PREFIX}GET_PAGES`;

const defaultState = {
    list: [],
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PAGES):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_PAGES):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_PAGES:
            return {
                ...state,
                list: action.resolved.pages,
                loading: false
            };

        default:
            return state;
    }
}

export function getPages() {
    return {
        type: GET_PAGES,
        promise: api.world.pages()
    };
}
