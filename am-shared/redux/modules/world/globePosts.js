import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { getArtistBySlug } from './postArtists';

const PREFIX = 'am/world/globePosts/';

// Temporary-ish until we can query by location
const GET_ALL_POSTS = `${PREFIX}GET_ALL_POSTS`;

const defaultState = {
    list: [],
    // Using 2 different arrays because we only need to make a request
    // once for all the posts and then filter on the client by city. This
    // is temporary until we add locations as part of the ghost API
    filteredList: [],
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ALL_POSTS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_ALL_POSTS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_ALL_POSTS: {
            return {
                ...state,
                list: action.resolved.posts,
                filteredList: action.resolved.posts.filter((post) => {
                    const { tags } = post;
                    return tags.some((tag) => {
                        const lower = tag.slug
                            .trim()
                            .toLowerCase()
                            .replace(/-/g, ' ');

                        return (
                            lower === action.city.split(',')[0].toLowerCase()
                        );
                    });
                }),
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getAllPosts(city) {
    return (dispatch, getState) => {
        const options = {
            include: ['authors', 'tags'].join(','),
            limit: 'all'
        };
        const currentList = getState().worldGlobePosts.list;

        let promise = Promise.resolve({
            posts: currentList
        });

        if (!currentList.length) {
            promise = api.world.posts(options).then((data) => {
                data.posts.forEach((post) => {
                    const slug = (post.primary_author || {}).audiomack;

                    if (slug) {
                        dispatch(getArtistBySlug(slug));
                    }
                });

                return data;
            });
        }

        return dispatch({
            type: GET_ALL_POSTS,
            page: options.page,
            city,
            promise
        });
    };
}
