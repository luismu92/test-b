import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/world/settings/';

const GET_SETTINGS = `${PREFIX}GET_SETTINGS`;

const defaultState = {
    info: null,
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_SETTINGS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_SETTINGS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_SETTINGS: {
            return {
                ...state,
                list: action.resolved.settings,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getWorldSettings() {
    return {
        type: GET_SETTINGS,
        promise: api.world.settings()
    };
}
