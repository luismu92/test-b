import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/world/postArtists/';

const GET_BY_SLUG = `${PREFIX}GET_BY_SLUG`;

const defaultState = {
    artists: {},
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_BY_SLUG):
            return {
                ...state,
                artists: {
                    ...state.artists,
                    [action.slug]: null
                },
                loading: true
            };

        case failSuffix(GET_BY_SLUG):
            return {
                ...state,
                error: action.error,
                artists: {
                    ...state.artists,
                    [action.slug]: undefined
                },
                loading: false
            };

        case GET_BY_SLUG:
            return {
                ...state,
                artists: {
                    ...state.artists,
                    [action.slug]: action.resolved.results
                },
                loading: false
            };

        default:
            return state;
    }
}

export function getArtistBySlug(slug) {
    return (dispatch, getState) => {
        const postArtists = getState().worldPostArtists.artists;

        if (typeof postArtists[slug] !== 'undefined') {
            return null;
        }

        return dispatch({
            type: GET_BY_SLUG,
            slug,
            promise: api.artist.get(slug)
        });
    };
}
