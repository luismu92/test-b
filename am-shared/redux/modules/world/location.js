import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/world/location/';

const GET_WORLD_LOCATIONS = `${PREFIX}GET_WORLD_LOCATIONS`;

const defaultState = {
    list: [],
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_WORLD_LOCATIONS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_WORLD_LOCATIONS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_WORLD_LOCATIONS: {
            return {
                ...state,
                list: action.resolved,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getWorldLocations() {
    return {
        type: GET_WORLD_LOCATIONS,
        promise: api.world.locations()
    };
}
