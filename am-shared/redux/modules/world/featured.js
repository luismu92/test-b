import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { getArtistBySlug } from './postArtists';

const PREFIX = 'am/world/featured/';

const GET_FEATURED_POSTS = `${PREFIX}GET_FEATURED_POSTS`;
const SET_INCLUDES = `${PREFIX}SET_INCLUDES`;
const SET_FILTER = `${PREFIX}SET_FILTER`;

const defaultState = {
    list: [],
    pagination: {},
    includes: [],
    filter: {},
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_FEATURED_POSTS):
            return {
                ...state,
                loading: true,
                list: action.page === 1 ? defaultState.list : state.list,
                pagination:
                    action.page === 1
                        ? defaultState.pagination
                        : state.pagination
            };

        case failSuffix(GET_FEATURED_POSTS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_FEATURED_POSTS: {
            let list = action.resolved.posts;

            if (action.resolved.meta.pagination.page !== 1) {
                list = state.list.concat(list);
            }

            return {
                ...state,
                list: list,
                pagination: action.resolved.meta.pagination,
                loading: false
            };
        }

        case SET_INCLUDES: {
            let includes = state.includes.concat(action.includes);

            if (action.overwrite) {
                includes = action.includes;
            }

            return {
                ...state,
                includes: includes
            };
        }

        case SET_FILTER: {
            let filter = {
                ...state.filter,
                [action.key]: action.value
            };

            if (action.overwrite) {
                filter = {
                    [action.key]: action.value
                };
            }

            return {
                ...state,
                filter: filter
            };
        }

        default:
            return state;
    }
}

export function getFeaturedPosts(options = {}) {
    return (dispatch, getState) => {
        const { includes, filter } = getState().worldFeatured;
        const opts = {
            include: includes.join(','),
            filter: Object.keys(filter).reduce((str, key) => {
                if (!filter[key]) {
                    return str;
                }

                str += `${key}:${filter[key]}`;
                return str;
            }, ''),
            ...options
        };

        return dispatch({
            type: GET_FEATURED_POSTS,
            page: options.page,
            promise: api.world.posts(opts).then((data) => {
                data.posts.forEach((post) => {
                    const slug = (post.primary_author || {}).audiomack;

                    if (slug) {
                        dispatch(getArtistBySlug(slug));
                    }
                });

                return data;
            })
        });
    };
}

export function setIncludes(includes, overwrite = true) {
    return {
        type: SET_INCLUDES,
        includes,
        overwrite
    };
}

export function setFilter(key, value, overwrite = true) {
    return {
        type: SET_FILTER,
        key,
        value,
        overwrite
    };
}
