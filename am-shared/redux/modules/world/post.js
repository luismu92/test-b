import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/world/post/';

const GET_BY_ID = `${PREFIX}GET_BY_ID`;
const GET_BY_SLUG = `${PREFIX}GET_BY_SLUG`;

const defaultState = {
    info: null,
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_BY_SLUG):
        case requestSuffix(GET_BY_ID):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_BY_SLUG):
        case failSuffix(GET_BY_ID):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_BY_SLUG:
        case GET_BY_ID:
            return {
                ...state,
                info: action.resolved.posts[0],
                loading: false
            };

        default:
            return state;
    }
}

export function getById(id, options) {
    return {
        type: GET_BY_ID,
        promise: api.world.postById(id, options)
    };
}

export function getBySlug(slug, options) {
    return {
        type: GET_BY_SLUG,
        promise: api.world.postBySlug(slug, options)
    };
}
