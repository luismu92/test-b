import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/user/feed/';

export const GET_USER_FEED = `${PREFIX}GET_USER_FEED`;
const SET_UPLOADS_ONLY_STATE = `${PREFIX}SET_UPLOADS_ONLY_STATE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    list: [],
    page: 1,
    loading: false,
    showOnlyUploads: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_USER_FEED):
            return {
                ...state,
                errors: [],
                loading: true
            };

        case failSuffix(GET_USER_FEED):
            return {
                ...state,
                errors: state.errors.concat(action.error),
                loading: false
            };

        case SET_PAGE:
            return {
                ...state,
                page: action.page
            };

        case SET_UPLOADS_ONLY_STATE:
            return {
                ...state,
                showOnlyUploads: action.state
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case GET_USER_FEED: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (state.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                state.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return {
                ...state,
                list: newData,
                showOnlyUploads: action.showOnlyUploads,
                page: action.page,
                onLastPage,
                loading: false
            };
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getUserFeed(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().currentUserFeed;
        const {
            limit = 20,
            page = state.page,
            showOnlyUploads = state.showOnlyUploads
        } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_USER_FEED,
            page: finalPage,
            playerFetcher: (p) =>
                api.user.feed(token, secret, {
                    onlyUploads: showOnlyUploads,
                    page: p,
                    limit: finalLimit
                }),
            limit: finalLimit,
            showOnlyUploads: showOnlyUploads,
            promise: api.user.feed(token, secret, {
                onlyUploads: showOnlyUploads,
                page: finalPage,
                limit: finalLimit
            })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}

export function setUploadsOnlyState(state) {
    return {
        type: SET_UPLOADS_ONLY_STATE,
        state
    };
}
