import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/user/notificationSettings/';

const GET_USER_SETTINGS = `${PREFIX}GET_USER_SETTINGS`;
const UPDATE_USER_SETTINGS = `${PREFIX}UPDATE_USER_SETTINGS`;

const defaultState = {
    settings: {},
    loading: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_USER_SETTINGS):
            return {
                ...state,
                loading: true
            };

        case GET_USER_SETTINGS:
            return {
                ...state,
                loading: false,
                settings: action.resolved
            };

        case failSuffix(GET_USER_SETTINGS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case requestSuffix(UPDATE_USER_SETTINGS):
            return {
                ...state,
                settings: {
                    ...state.settings,
                    [action.setting]: action.value
                },
                loading: true
            };

        case UPDATE_USER_SETTINGS:
            return {
                ...state
            };

        case failSuffix(UPDATE_USER_SETTINGS):
            return {
                ...state,
                settings: {
                    ...state.settings,
                    [action.setting]: !action.value
                },
                error: action.error
            };

        default:
            return state;
    }
}

export function getUserSettings() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_USER_SETTINGS,
            promise: api.user.getUserSettings(token, secret)
        });
    };
}

export function updateUserSettings(setting, value) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const settings = { [setting]: value };

        return dispatch({
            type: UPDATE_USER_SETTINGS,
            setting,
            value,
            promise: api.user.updateUserSettings(token, secret, settings)
        });
    };
}
