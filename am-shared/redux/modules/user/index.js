import api from 'api/index';
import Cookie from 'js-cookie';
import {
    requestSuffix,
    failSuffix,
    strToNumber,
    loadFacebookSdk
} from 'utils/index';
import { getTwitterOauthVerifier, getInstagramAccessToken } from 'utils/social';
import analytics, {
    branchEvents,
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';
import { AUTH_PROVIDER } from 'constants/index';
import {
    SOCIAL_NETWORK_LINK_TWITTER,
    SOCIAL_NETWORK_LINK_FACEBOOK,
    SOCIAL_NETWORK_LINK_INSTAGRAM
} from 'constants/user/social';
import storage, { STORAGE_AUTH_USER } from 'utils/storage';

import { UNFAVORITE_PLAYLIST, FAVORITE_PLAYLIST } from '../playlist';

const PREFIX = 'am/user/index/';
const REGISTER = `${PREFIX}REGISTER`;
const CHECK_EMAIL = `${PREFIX}CHECK_EMAIL`;
const LOG_IN_FACEBOOK = `${PREFIX}LOG_IN_FACEBOOK`;
const LOG_IN_GOOGLE = `${PREFIX}LOG_IN_GOOGLE`;
const LOG_IN_TWITTER = `${PREFIX}LOG_IN_TWITTER`;
const LOG_IN_INSTAGRAM = `${PREFIX}LOG_IN_INSTAGRAM`;
const LOG_IN_APPLE = `${PREFIX}LOG_IN_APPLE`;
const GET_TWITTER_REQUEST_TOKEN = `${PREFIX}GET_TWITTER_REQUEST_TOKEN`;
const FORGOT_PASSWORD = `${PREFIX}FORGOT_PASSWORD`;
const LOG_OUT = `${PREFIX}LOG_OUT`;
const QUEUE_ACTION = `${PREFIX}QUEUE_ACTION`;
const DEQUEUE_ACTION = `${PREFIX}DEQUEUE_ACTION`;
const UPDATE_PASSWORD = `${PREFIX}UPDATE_PASSWORD`;
const UPDATE_URL_SLUG = `${PREFIX}UPDATE_URL_SLUG`;
const UPDATE_EMAIL = `${PREFIX}UPDATE_EMAIL`;
const DELETE_ACCOUNT = `${PREFIX}DELETE_ACCOUNT`;
const RESEND_EMAIL = `${PREFIX}RESEND_EMAIL`;
const VERIFY_HASH = `${PREFIX}VERIFY_HASH`;
const EMAIL_UNSUBSCRIBE = `${PREFIX}EMAIL_UNSUBSCRIBE`;
const VERIFY_FORGOT_PASSWORD_TOKEN = `${PREFIX}VERIFY_FORGOT_PASSWORD_TOKEN`;
const RECOVER_ACCOUNT = `${PREFIX}RECOVER_ACCOUNT`;
const CLEAR_ERRORS = `${PREFIX}CLEAR_ERRORS`;
const CLAIM_ARTIST = `${PREFIX}CLAIM_ARTIST`;
const LINK_NETWORK = `${PREFIX}LINK_NETWORK`;
const UNLINK_NETWORK = `${PREFIX}UNLINK_NETWORK`;
const GET_USER_FROM_TOKEN = `${PREFIX}GET_USER_FROM_TOKEN`;
const VALIDATE_ARTIST = `${PREFIX}VALIDATE_ARTIST`;
const DELETE_EXTERNAL_RSS_FEED_URL = `${PREFIX}DELETE_EXTERNAL_RSS_FEED_URL`;
const GET_EXTERNAL_RSS_FEED_URL = `${PREFIX}GET_EXTERNAL_RSS_FEED_URL`;
const SET_EXTERNAL_RSS_FEED_URL = `${PREFIX}SET_EXTERNAL_RSS_FEED_URL`;
const SET_USER_PREMIUM = `${PREFIX}SET_USER_PREMIUM`;

// These exported consts are used in other reducers
// If your action doesnt need to be used elsewhere,
// add it above
export const LOG_IN = `${PREFIX}LOG_IN`;
export const SAVE_USER_DETAILS = `${PREFIX}SAVE_USER_DETAILS`;
export const FAVORITE_ITEM = `${PREFIX}FAVORITE_ITEM`;
export const UNFAVORITE_ITEM = `${PREFIX}UNFAVORITE_ITEM`;
export const REPOST_ITEM = `${PREFIX}REPOST_ITEM`;
export const UNREPOST_ITEM = `${PREFIX}UNREPOST_ITEM`;
export const FOLLOW_ARTIST = `${PREFIX}FOLLOW_ARTIST`;
export const UNFOLLOW_ARTIST = `${PREFIX}UNFOLLOW_ARTIST`;

const defaultState = {
    profile: null,
    errors: [],
    token: null,
    secret: null,
    loading: false,
    updatePassword: {
        error: null,
        loading: false
    },
    updateEmail: {
        error: null,
        loading: false
    },
    updateSlug: {
        error: null,
        loading: false
    },
    deleteAccount: {
        error: null,
        loading: false
    },
    verifyHash: {
        error: null,
        loading: false
    },
    emailUnsubscribe: {
        error: null,
        loading: false,
        type: null
    },
    claimArtist: {
        error: null,
        loading: false
    },
    checkEmail: {
        error: null,
        loading: false
    },
    externalRssFeedUrl: {
        error: null,
        loading: false
    },
    isLoggedIn: false,
    queuedLoginAction: null,
    isAdmin: false,
    isSuspension: false
};

// eslint-disable-next-line complexity
export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(LOG_IN):
        case requestSuffix(LOG_IN_FACEBOOK):
        case requestSuffix(LOG_IN_GOOGLE):
        case requestSuffix(LOG_IN_TWITTER):
        case requestSuffix(LOG_IN_INSTAGRAM):
        case requestSuffix(REGISTER):
        case requestSuffix(FORGOT_PASSWORD):
        case requestSuffix(SAVE_USER_DETAILS):
            return {
                ...state,
                errors: defaultState.errors,
                loading: true
            };

        case failSuffix(LOG_IN):
        case failSuffix(LOG_IN_FACEBOOK):
        case failSuffix(LOG_IN_GOOGLE):
        case failSuffix(LOG_IN_TWITTER):
        case failSuffix(LOG_IN_INSTAGRAM):
            return {
                ...state,
                errors: [action.error],
                loading: false
            };

        case failSuffix(SAVE_USER_DETAILS): {
            const errors = state.errors.concat(action.error).filter(Boolean);

            return {
                ...state,
                errors,
                loading: false
            };
        }

        case CLEAR_ERRORS:
            return {
                ...state,
                errors: defaultState.errors,
                updatePassword: {
                    ...state.updatePassword,
                    error: defaultState.updatePassword.error
                },
                updateEmail: {
                    ...state.updateEmail,
                    error: defaultState.updateEmail.error
                },
                updateSlug: {
                    ...state.updateSlug,
                    error: defaultState.updateSlug.error
                },
                deleteAccount: {
                    ...state.deleteAccount,
                    error: defaultState.deleteAccount.error
                },
                verifyHash: {
                    ...state.verifyHash,
                    error: defaultState.verifyHash.error
                }
            };

        case failSuffix(REGISTER):
            return {
                ...state,
                errors: [action.error],
                loading: false
            };

        case failSuffix(FORGOT_PASSWORD):
            return {
                ...state,
                errors: [action.error.errors.email],
                loading: false
            };

        case FORGOT_PASSWORD:
            return {
                ...state,
                loading: false
            };

        case FOLLOW_ARTIST:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    following_count: state.profile.following_count + 1,
                    following: Array.from(state.profile.following).concat(
                        action.artist.id
                    )
                }
            };

        case UNFOLLOW_ARTIST:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    following_count: state.profile.following_count - 1,
                    following: Array.from(state.profile.following).filter(
                        (id) => {
                            return id !== action.artist.id;
                        }
                    )
                }
            };

        case requestSuffix(VERIFY_HASH):
            return {
                ...state,
                verifyHash: {
                    ...state.verifyHash,
                    loading: true,
                    error: defaultState.verifyHash.error
                }
            };

        case failSuffix(VERIFY_HASH):
            return {
                ...state,
                verifyHash: {
                    ...state.verifyHash,
                    loading: false,
                    error: action.error
                }
            };

        case VERIFY_HASH: {
            let profile = state.profile;

            if (profile) {
                profile = {
                    ...profile,
                    can_upload: true,
                    verified_email: true
                };
            }

            return {
                ...state,
                verifyHash: {
                    ...state.verifyHash,
                    loading: false
                },
                profile
            };
        }

        case requestSuffix(EMAIL_UNSUBSCRIBE):
            return {
                ...state,
                emailUnsubscribe: {
                    ...state.emailUnsubscribe,
                    loading: true,
                    error: defaultState.emailUnsubscribe.error
                }
            };

        case failSuffix(EMAIL_UNSUBSCRIBE):
            return {
                ...state,
                emailUnsubscribe: {
                    ...state.emailUnsubscribe,
                    loading: false,
                    error: action.error
                }
            };

        case EMAIL_UNSUBSCRIBE: {
            return {
                ...state,
                emailUnsubscribe: {
                    ...state.emailUnsubscribe,
                    loading: false,
                    error: defaultState.emailUnsubscribe.error,
                    type: action.resolved.type
                }
            };
        }

        case LOG_OUT:
            return defaultState;

        case SAVE_USER_DETAILS:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    ...action.resolved
                },
                errors: [],
                loading: false
            };

        case QUEUE_ACTION:
            return {
                ...state,
                queuedLoginAction: action.action
            };

        case DEQUEUE_ACTION:
            return {
                ...state,
                queuedLoginAction: defaultState.queuedLoginAction
            };

        case LOG_IN:
        case LOG_IN_FACEBOOK:
        case LOG_IN_GOOGLE:
        case LOG_IN_TWITTER:
        case LOG_IN_INSTAGRAM:
        case LOG_IN_APPLE:
        case GET_USER_FROM_TOKEN:
        case REGISTER:
            return {
                ...state,
                profile: action.resolved.user,
                errors: [],
                token: action.resolved.token,
                secret: action.resolved.secret,
                loading: false,
                isLoggedIn: true,
                isAdmin: action.resolved.user.is_admin === true,
                isSuspension: action.resolved.user.is_suspension === true
            };

        case requestSuffix(CHECK_EMAIL):
            return {
                ...state,
                checkEmail: {
                    ...state.checkEmail,
                    loading: true,
                    error: defaultState.checkEmail.error
                }
            };

        case failSuffix(CHECK_EMAIL):
            return {
                ...state,
                checkEmail: {
                    ...state.checkEmail,
                    loading: false,
                    error: action.error
                }
            };

        case CHECK_EMAIL:
            return {
                ...state,
                checkEmail: {
                    ...state.checkEmail,
                    loading: false
                },
                errors: state.errors.filter((error) => {
                    const description = error.description || '';
                    const isEmailError =
                        description.toLowerCase().indexOf('email') !== -1;

                    return !(error.errorcode === 1008 && isEmailError);
                })
            };

        // Instead of waiting for the request to finish, we will just proceed
        // with editing the state tree as if the action succeeded on _REQUEST.
        // This makes it appear super fast when favoriting or unfavoriting. We
        // take care of failures appropriately
        case failSuffix(UNFAVORITE_ITEM):
        case requestSuffix(FAVORITE_ITEM): {
            const user = state.profile;

            user.favorite_music.push(action.id);

            return {
                ...state,
                profile: user
            };
        }

        case failSuffix(FAVORITE_ITEM):
        case requestSuffix(UNFAVORITE_ITEM): {
            let found = -1;
            const user = state.profile;

            user.favorite_music.some((id, i) => {
                if (id === action.id) {
                    found = i;
                    return true;
                }

                return false;
            });

            if (found !== -1) {
                user.favorite_music.splice(found, 1);
            }

            return {
                ...state,
                profile: user
            };
        }

        case failSuffix(UNFAVORITE_PLAYLIST):
        case requestSuffix(FAVORITE_PLAYLIST): {
            const user = state.profile;

            user.favorite_playlists.push(action.id);

            return {
                ...state,
                profile: user
            };
        }

        case failSuffix(FAVORITE_PLAYLIST):
        case requestSuffix(UNFAVORITE_PLAYLIST): {
            let found = -1;
            const user = state.profile;

            user.favorite_playlists.some((id, i) => {
                if (id === action.id) {
                    found = i;
                    return true;
                }

                return false;
            });

            if (found !== -1) {
                user.favorite_playlists.splice(found, 1);
            }

            return {
                ...state,
                profile: user
            };
        }

        case failSuffix(UNREPOST_ITEM):
        case requestSuffix(REPOST_ITEM): {
            const user = state.profile;

            user.reups.push(action.id);

            return {
                ...state,
                profile: user
            };
        }

        case failSuffix(REPOST_ITEM):
        case requestSuffix(UNREPOST_ITEM): {
            let found = -1;
            const user = state.profile;

            user.reups.some((id, i) => {
                if (id === action.id) {
                    found = i;
                    return true;
                }

                return false;
            });

            if (found !== -1) {
                user.reups.splice(found, 1);
            }

            return {
                ...state,
                profile: user
            };
        }

        case requestSuffix(UPDATE_PASSWORD):
            return {
                ...state,
                updatePassword: {
                    ...state.updatePassword,
                    loading: true,
                    error: defaultState.updatePassword.error
                }
            };

        case failSuffix(UPDATE_PASSWORD):
            return {
                ...state,
                updatePassword: {
                    ...state.updatePassword,
                    loading: false,
                    error: action.error
                }
            };

        case UPDATE_PASSWORD:
            return {
                ...state,
                updatePassword: {
                    ...state.updatePassword,
                    loading: false,
                    error: defaultState.updatePassword.error
                }
            };

        case requestSuffix(UPDATE_URL_SLUG):
            return {
                ...state,
                updateSlug: {
                    ...state.updateSlug,
                    loading: true,
                    error: defaultState.updateSlug.error
                }
            };

        case failSuffix(UPDATE_URL_SLUG):
            return {
                ...state,
                updateSlug: {
                    ...state.updateSlug,
                    loading: false,
                    error: action.error
                }
            };

        case UPDATE_URL_SLUG:
            return {
                ...state,
                updateSlug: {
                    ...state.updateSlug,
                    loading: false,
                    error: defaultState.updateSlug.error
                },
                profile: {
                    ...state.profile,
                    url_slug: action.resolved.url_slug // eslint-disable-line camelcase
                }
            };

        case requestSuffix(UPDATE_EMAIL):
            return {
                ...state,
                updateEmail: {
                    ...state.updateEmail,
                    loading: true,
                    error: defaultState.updateEmail.error
                }
            };

        case failSuffix(UPDATE_EMAIL):
            return {
                ...state,
                updateEmail: {
                    ...state.updateEmail,
                    loading: false,
                    error: action.error
                }
            };

        case UPDATE_EMAIL:
            return {
                ...state,
                updateEmail: {
                    ...state.updateEmail,
                    loading: false,
                    error: defaultState.updateEmail.error
                }
            };

        case requestSuffix(DELETE_ACCOUNT):
            return {
                ...state,
                deleteAccount: {
                    ...state.deleteAccount,
                    loading: true,
                    error: defaultState.deleteAccount.error
                }
            };

        case failSuffix(DELETE_ACCOUNT):
            return {
                ...state,
                deleteAccount: {
                    ...state.deleteAccount,
                    loading: false,
                    error: action.error
                }
            };

        case DELETE_ACCOUNT:
            return {
                ...state,
                deleteAccount: {
                    ...state.deleteAccount,
                    loading: false,
                    error: defaultState.deleteAccount.error
                }
            };

        case requestSuffix(CLAIM_ARTIST): {
            return {
                ...state,
                claimArtist: {
                    ...state.claimArtist,
                    error: defaultState.claimArtist.error,
                    loading: true
                }
            };
        }

        case CLAIM_ARTIST:
            return {
                ...state,
                claimArtist: {
                    ...state.claimArtist,
                    error: defaultState.claimArtist.error,
                    loading: false
                },
                profile: action.resolved.user,
                errors: [],
                token: action.resolved.token,
                secret: action.resolved.secret,
                isLoggedIn: true,
                isAdmin: action.resolved.user.is_admin === true,
                isSuspension: action.resolved.user.is_suspension === true
            };

        case failSuffix(CLAIM_ARTIST): {
            return {
                ...state,
                claimArtist: {
                    ...state.claimArtist,
                    error: action.error,
                    loading: false
                }
            };
        }

        case requestSuffix(VALIDATE_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(VALIDATE_ARTIST): {
            return {
                ...state,
                loading: false
            };
        }

        case VALIDATE_ARTIST:
            return {
                ...state,
                loading: false,
                profile: {
                    ...state.profile,
                    image: action.resolved.user.image,
                    image_base: action.resolved.user.image_base,
                    image_banner: action.resolved.user.image_banner,
                    verified: action.resolved.verified
                }
            };

        case LINK_NETWORK: {
            if (action.resolved.user_fb_id) {
                return {
                    ...state,
                    profile: {
                        ...state.profile,
                        facebook_id: action.resolved.user_fb_id
                    }
                };
            }

            if (action.resolved.user_twitter_id) {
                return {
                    ...state,
                    profile: {
                        ...state.profile,
                        twitter_id: action.resolved.user_twitter_id
                    }
                };
            }

            if (action.resolved.user_instagram_id) {
                return {
                    ...state,
                    profile: {
                        ...state.profile,
                        instagram_id: action.resolved.user_instagram_id
                    }
                };
            }

            return state;
        }

        case UNLINK_NETWORK:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    [action.key]: ''
                }
            };

        case requestSuffix(DELETE_EXTERNAL_RSS_FEED_URL):
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    error: defaultState.externalRssFeedUrl.error,
                    loading: true
                }
            };

        case DELETE_EXTERNAL_RSS_FEED_URL:
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    error: defaultState.externalRssFeedUrl.error,
                    loading: false
                },
                profile: {
                    ...state.profile,
                    externalRssFeedUrl: ''
                }
            };

        case failSuffix(DELETE_EXTERNAL_RSS_FEED_URL):
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    loading: false,
                    error: action.error
                }
            };

        case requestSuffix(GET_EXTERNAL_RSS_FEED_URL):
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    error: defaultState.externalRssFeedUrl.error,
                    loading: true
                }
            };

        case GET_EXTERNAL_RSS_FEED_URL:
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    error: defaultState.externalRssFeedUrl.error,
                    loading: false
                },
                profile: {
                    ...state.profile,
                    externalRssFeedUrl: action.resolved
                }
            };

        case failSuffix(GET_EXTERNAL_RSS_FEED_URL):
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    loading: false,
                    error: action.error
                }
            };

        case requestSuffix(SET_EXTERNAL_RSS_FEED_URL):
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    error: defaultState.externalRssFeedUrl.error,
                    loading: true
                }
            };

        case SET_EXTERNAL_RSS_FEED_URL:
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    error: defaultState.externalRssFeedUrl.error,
                    loading: false
                },
                profile: {
                    ...state.profile,
                    externalRssFeedUrl: action.resolved
                }
            };

        case failSuffix(SET_EXTERNAL_RSS_FEED_URL):
            return {
                ...state,
                externalRssFeedUrl: {
                    ...state.externalRssFeedUrl,
                    loading: false,
                    error: action.error
                }
            };

        case SET_USER_PREMIUM:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    subscription: action.subscription
                }
            };

        default:
            return state;
    }
}

function cookieUser(cookieDetails = {}) {
    const { token, secret, expiration } = cookieDetails;
    const nowSeconds = Date.now() / 1000;
    const nowExpireSeconds = expiration;
    const hours = (nowExpireSeconds - nowSeconds) / 3600;
    const stringify = true;
    // Convert expires to days
    const expires = hours / 24;

    storage.set(STORAGE_AUTH_USER, cookieDetails, stringify);

    Cookie.set('otoken', token, {
        path: '/',
        expires,
        secure:
            process.env.NODE_ENV !== 'development' &&
            process.env.AUDIOMACK_DEV !== 'true',
        sameSite: 'lax'
    });
    Cookie.set('osecret', secret, {
        path: '/',
        expires,
        secure:
            process.env.NODE_ENV !== 'development' &&
            process.env.AUDIOMACK_DEV !== 'true',
        sameSite: 'lax'
    });
}

function trackLogin(user, provider, cookieDetails = null, authDetails = null) {
    if (typeof window === 'undefined') {
        return;
    }

    const trackers = ['ga'];

    if (provider === AUTH_PROVIDER.FACEBOOK) {
        trackers.push('fb');
    }

    let action;

    switch (provider) {
        case AUTH_PROVIDER.FACEBOOK:
            action = eventAction.facebook;
            break;

        case AUTH_PROVIDER.GOOGLE:
            action = eventAction.google;
            break;

        case AUTH_PROVIDER.TWITTER:
            action = eventAction.twitter;
            break;

        case AUTH_PROVIDER.INSTAGRAM:
            action = eventAction.instagram;
            break;

        default:
            action = eventAction.password;
            break;
    }

    analytics.track(
        eventCategory.auth,
        {
            eventAction: action,
            eventLabel:
                window.AM_CONTEXT === 'desktop'
                    ? eventLabel.desktop
                    : eventLabel.mobile
        },
        trackers
    );
    analytics.identify(user.id);

    if (cookieDetails) {
        cookieUser(cookieDetails);
    }

    switch (provider) {
        case AUTH_PROVIDER.FACEBOOK:
        case AUTH_PROVIDER.GOOGLE:
        case AUTH_PROVIDER.TWITTER: {
            if (window.FederatedCredential && provider) {
                // Create `Credential` object for federation
                const cred = new window.FederatedCredential({
                    id: user.email,
                    name: user.name,
                    iconURL: user.image,
                    provider: provider
                });

                // Store credential information after successful authentication
                navigator.credentials.store(cred);
            }
            break;
        }

        case AUTH_PROVIDER.PASSWORD: {
            const { email, password } = authDetails;

            // This logic doesnt matter if it passes or not and
            // is soley for logging into the legacy dashboard.
            // Therefore we can return the data immediately after
            const form = new FormData();

            form.append('email', email);
            form.append('password', password);

            fetch(`${process.env.PHP_BACKEND_URL}/account/login/modal`, {
                method: 'POST',
                body: form,
                credentials: 'include'
            });

            if (window.PasswordCredential) {
                // Create password credential
                const cred = new window.PasswordCredential({
                    id: email,
                    password: password,
                    name: user.name,
                    iconURL: user.image
                });

                // Store user information as this is registration using id/password
                navigator.credentials.store(cred);
            }
            break;
        }

        default:
            console.error('Provider unaccounted for');
            break;
    }
}

function trackRegistration(user, cookieDetails) {
    analytics.trackBranchData({
        [branchEvents.userRegistration]: user.name
    });

    analytics.track(
        eventCategory.auth,
        {
            eventAction: eventAction.userRegistration,
            eventLabel:
                window.AM_CONTEXT === 'desktop'
                    ? eventLabel.desktop
                    : eventLabel.mobile
        },
        ['ga', 'fb']
    );

    cookieUser(cookieDetails);
}

export function getUserFromToken(token, secret, expiration) {
    return {
        type: GET_USER_FROM_TOKEN,
        promise: api.user.get(token, secret).then((data) => {
            return {
                user: data,
                token: token,
                secret: secret,
                expiration: expiration
            };
        })
    };
}

export function identityCheck(email, type) {
    return {
        type: CHECK_EMAIL,
        promise: api.user.identityCheck(email, type).then((data) => {
            return {
                data
            };
        })
    };
}

export function logIn(email, password, shouldCookieUser = true) {
    return {
        type: LOG_IN,
        promise: api.user
            .login(email, password)
            .then(
                ({
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expiration
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        const cookieDetails = shouldCookieUser
                            ? { token, secret, expiration }
                            : null;
                        const authDetails = { email, password };

                        trackLogin(
                            data,
                            AUTH_PROVIDER.PASSWORD,
                            cookieDetails,
                            authDetails
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

export function logInFacebook(fbToken, fbUserId, fbUsername, userEmail) {
    return {
        type: LOG_IN_FACEBOOK,
        promise: api.user
            .loginFacebook(fbToken, fbUserId, fbUsername, userEmail)
            .then(
                ({
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expiration
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        const cookieDetails = { token, secret, expiration };
                        const authDetails = null;

                        trackLogin(
                            data,
                            AUTH_PROVIDER.FACEBOOK,
                            cookieDetails,
                            authDetails
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

export function logInInstagram(instagramId, instagramToken) {
    return {
        type: LOG_IN_INSTAGRAM,
        promise: api.user
            .logInInstagram(instagramId, instagramToken)
            .then(
                ({
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expiration
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        const cookieDetails = { token, secret, expiration };
                        const authDetails = null;
                        trackLogin(
                            data,
                            AUTH_PROVIDER.INSTAGRAM,
                            cookieDetails,
                            authDetails
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

export function logInGoogle(googleToken, accessToken) {
    return {
        type: LOG_IN_GOOGLE,
        promise: api.user
            .loginGoogle(googleToken, accessToken)
            .then(
                ({
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expiration
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        const cookieDetails = { token, secret, expiration };
                        const authDetails = null;

                        trackLogin(
                            data,
                            AUTH_PROVIDER.GOOGLE,
                            cookieDetails,
                            authDetails
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

function authenticateWithTwitter(twitterToken, twitterSecret, userEmail) {
    return getTwitterOauthVerifier(twitterToken).then((oauthVerifier) => {
        return api.user.logInTwitter(
            twitterToken,
            twitterSecret,
            oauthVerifier,
            userEmail
        );
    });
}

export function authenticateWithInstagram() {
    return getInstagramAccessToken().then((accessToken) => {
        return api.external.instagram(accessToken);
    });
}

export function getTwitterRequestToken() {
    return {
        type: GET_TWITTER_REQUEST_TOKEN,
        promise: api.user.getTwitterRequestToken()
    };
}

export function logInTwitter(twitterToken, twitterSecret, userEmail) {
    return {
        type: LOG_IN_TWITTER,
        // first get request token
        promise: authenticateWithTwitter(
            twitterToken,
            twitterSecret,
            userEmail
        ).then((data) => {
            const {
                oauth_token: token,
                oauth_token_secret: secret,
                oauth_token_expiration: expiration
            } = data;

            return api.user.get(token, secret).then((user) => {
                const cookieDetails = { token, secret, expiration };
                const authDetails = null;

                trackLogin(
                    user,
                    AUTH_PROVIDER.TWITTER,
                    cookieDetails,
                    authDetails
                );

                return {
                    user: user,
                    token: token,
                    secret: secret,
                    expiration: expiration
                };
            });
        })
    };
}

export function logInApple(idToken) {
    return {
        type: LOG_IN_APPLE,
        promise: api.user.logInApple(idToken).then((data) => {
            const {
                oauth_token: token,
                oauth_token_secret: secret,
                oauth_token_expiration: expiration
            } = data;

            return api.user.get(token, secret).then((user) => {
                const cookieDetails = { token, secret, expiration };
                const authDetails = null;

                trackLogin(
                    user,
                    AUTH_PROVIDER.APPLE,
                    cookieDetails,
                    authDetails
                );

                return {
                    user: user,
                    token: token,
                    secret: secret,
                    expiration: expiration
                };
            });
        })
    };
}

export function claimArtist(id, signinOptions = {}) {
    const { email, password } = signinOptions;

    return {
        type: CLAIM_ARTIST,
        promise: api.user
            .claim(id, signinOptions)
            .then(
                ({
                    oauth_token_expiration: expiration,
                    oauth_token: token,
                    oauth_token_secret: secret
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        const cookieDetails = { token, secret, expiration };
                        const authDetails = { email, password };

                        trackLogin(
                            data,
                            AUTH_PROVIDER.PASSWORD,
                            cookieDetails,
                            authDetails
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

export function register(email, username, password, password2) {
    return {
        type: REGISTER,
        promise: api.user
            .register(email, username, password, password2)
            .then(
                ({
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expiration
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        trackRegistration(
                            data,
                            { token, secret, expiration },
                            { email, password }
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

export function registerWithCaptcha(email, username, passwords, captcha) {
    const password = passwords[0];

    return {
        type: REGISTER,
        promise: api.user
            .registerWithCaptcha(
                email,
                username,
                [password, passwords[1]],
                captcha
            )
            .then(
                ({
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expiration
                }) => {
                    return api.user.get(token, secret).then((data) => {
                        trackRegistration(
                            data,
                            { token, secret, expiration },
                            { email, password }
                        );

                        return {
                            user: data,
                            token: token,
                            secret: secret,
                            expiration: expiration
                        };
                    });
                }
            )
    };
}

export function verifyHash(hash) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: VERIFY_HASH,
            promise: api.user.verifyHash(hash, token, secret)
        });
    };
}

export function emailUnsubscribe(hash) {
    return (dispatch) => {
        return dispatch({
            type: EMAIL_UNSUBSCRIBE,
            promise: api.user.emailUnsubscribe(hash)
        });
    };
}

export function verifyForgotPasswordToken(token) {
    return {
        type: VERIFY_FORGOT_PASSWORD_TOKEN,
        promise: api.user.verifyForgotPasswordToken(token)
    };
}

export function recoverAccount(token, password, password2) {
    return {
        type: RECOVER_ACCOUNT,
        promise: api.user.recoverAccount(token, password, password2 || password)
    };
}

export function forgotpw(email) {
    // Add this to the process environments so we don't need the two instances
    Cookie.remove('audiomackdev', {
        path: '/',
        domain: '.dev.audiomack.com'
    });

    Cookie.remove('audiomackdev', {
        path: '/',
        domain: '.aws.audiomack.com'
    });

    Cookie.remove('audiomack', {
        path: '/',
        domain: '.audiomack.com'
    });

    fetch(`${process.env.PHP_BACKEND_URL}/account/logout`, {
        method: 'POST',
        credentials: 'include'
    });

    return {
        type: FORGOT_PASSWORD,
        promise: api.user.forgot(email).then((data) => {
            analytics.track(
                eventCategory.auth,
                {
                    eventAction: eventAction.userForgotPassword,
                    eventLabel:
                        window.AM_CONTEXT === 'desktop'
                            ? eventLabel.desktop
                            : eventLabel.mobile
                },
                ['ga', 'fb']
            );
            deleteCookies();
            return data;
        })
    };
}

export function clearErrors() {
    return {
        type: CLEAR_ERRORS
    };
}

export function saveUserDetails(updatedDetails) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;
        const data = {
            // Name is required in saving so use the current name as default
            ...state.profile,
            ...updatedDetails
        };

        return dispatch({
            type: SAVE_USER_DETAILS,
            promise: api.user.save(data, token, secret)
        });
    };
}

export function updatePassword(oldPass, newPass, newPass2) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        let promise;

        if (newPass && newPass === newPass2) {
            promise = api.user.updatePassword(oldPass, newPass, token, secret);
        } else {
            let message = "Passwords don't match.";

            if (!oldPass || !newPass) {
                message = 'Password fields required.';
            } else if (oldPass === newPass) {
                message = 'Old password matches new password.';
            }

            promise = Promise.reject(message);
        }

        return dispatch({
            type: UPDATE_PASSWORD,
            promise
        });
    };
}

export function updateSlug(slug) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: UPDATE_URL_SLUG,
            promise: api.user.updateSlug(slug, token, secret)
        });
    };
}

export function updateEmail(email, password) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: UPDATE_EMAIL,
            promise: api.user
                .updateEmail(email, password, token, secret)
                .then((data) => {
                    analytics.track(eventCategory.user, {
                        eventAction: eventAction.editProfile,
                        eventLabel: eventLabel.changeEmail
                    });

                    return data;
                })
        });
    };
}

function deleteCookies() {
    const secure = process.env.NODE_ENV !== 'development';

    if (
        window.navigator.credentials &&
        window.navigator.credentials.preventSilentAccess
    ) {
        // Turn on the mediation mode so auto sign-in won't happen
        // until next time user intended to do so.
        window.navigator.credentials.preventSilentAccess();
    }

    analytics.identify(null);
    storage.remove(STORAGE_AUTH_USER);

    Cookie.remove('otoken', {
        path: '/',
        secure
    });
    Cookie.remove('osecret', {
        path: '/',
        secure
    });

    // Remove any admin tokens
    Cookie.remove('adminotoken', {
        path: '/',
        secure
    });
    Cookie.remove('adminosecret', {
        path: '/',
        secure
    });

    fetch(`${process.env.PHP_BACKEND_URL}/account/logout`, {
        method: 'POST',
        credentials: 'include'
    });
}

export function deleteAccount(password, isSocial) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: DELETE_ACCOUNT,
            promise: api.user
                .deleteAccount(password, isSocial, token, secret)
                .then((data) => {
                    analytics.track(eventCategory.user, {
                        eventAction: eventAction.editProfile,
                        eventLabel: eventLabel.deleteAccount
                    });

                    deleteCookies();

                    return data;
                })
        });
    };
}

export function logOut() {
    deleteCookies();

    return {
        type: LOG_OUT
    };
}

export function favorite(itemId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };
        let promise;

        if (token && secret && typeof itemId !== 'undefined') {
            promise = api.user.favorite(itemId, token, secret, opts);
        } else {
            promise = Promise.reject(
                'No valid item id, token or secret provided'
            );
        }

        return dispatch({
            type: FAVORITE_ITEM,
            id: strToNumber(itemId),
            promise
        });
    };
}

export function unfavorite(itemId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };
        let promise;

        if (token && secret && typeof itemId !== 'undefined') {
            promise = api.user.unfavorite(itemId, token, secret, opts);
        } else {
            promise = Promise.reject(
                'No valid item id, token or secret provided'
            );
        }

        return dispatch({
            type: UNFAVORITE_ITEM,
            id: strToNumber(itemId),
            promise
        });
    };
}

export function repost(itemId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };

        let promise;

        if (token && secret && typeof itemId !== 'undefined') {
            promise = api.user.repost(itemId, token, secret, opts);
        } else {
            promise = Promise.reject(
                'No valid item id, token or secret provided'
            );
        }

        return dispatch({
            type: REPOST_ITEM,
            id: strToNumber(itemId),
            promise
        });
    };
}

export function unrepost(itemId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };
        let promise;

        if (token && secret && typeof itemId !== 'undefined') {
            promise = api.user.unrepost(itemId, token, secret, opts);
        } else {
            promise = Promise.reject(
                'No valid item id, token or secret provided'
            );
        }

        return dispatch({
            type: UNREPOST_ITEM,
            id: strToNumber(itemId),
            promise
        });
    };
}

export function queueAction(action) {
    return {
        type: QUEUE_ACTION,
        action
    };
}

export function dequeueAction() {
    return {
        type: DEQUEUE_ACTION
    };
}

export function resendEmail(returnPath) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: RESEND_EMAIL,
            promise: api.user.resendEmail(token, secret, {
                returnPath
            })
        });
    };
}

export function follow(artist) {
    return (dispatch, getState) => {
        const { profile, token, secret } = getState().currentUser;
        const { profile: artistProfile } = getState().artist;

        return dispatch({
            type: FOLLOW_ARTIST,
            user: profile,
            artist,
            // Could happen if we were on the artist page but we dont
            // want to alter artistFollowers reducer otherwise
            isArtistActive: artistProfile && artistProfile.id === artist.id,
            promise: api.artist.follow(artist.url_slug, token, secret)
        });
    };
}

export function unfollow(artist) {
    return (dispatch, getState) => {
        const { profile, token, secret } = getState().currentUser;
        const { profile: artistProfile } = getState().artist;

        return dispatch({
            type: UNFOLLOW_ARTIST,
            user: profile,
            artist,
            // Could happen if we were on the artist page but we dont
            // want to alter artistFollowers reducer otherwise
            isArtistActive: artistProfile && artistProfile.id === artist.id,
            promise: api.artist.unfollow(artist.url_slug, token, secret)
        });
    };
}

export function linkNetwork(network, data = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        let promise;

        switch (network) {
            case SOCIAL_NETWORK_LINK_FACEBOOK: {
                promise = loadFacebookSdk().then(({ login }) => {
                    return api.user.linkNetwork(token, secret, {
                        fb_token: login.authResponse.accessToken
                    });
                });
                break;
            }

            case SOCIAL_NETWORK_LINK_TWITTER: {
                const { twitterToken, twitterSecret } = data;

                promise = getTwitterOauthVerifier(twitterToken).then(
                    (oauthVerifier) => {
                        return api.user.linkNetwork(token, secret, {
                            t_token: twitterToken,
                            t_secret: twitterSecret,
                            t_oauth_verifier: oauthVerifier
                        });
                    }
                );
                break;
            }

            case SOCIAL_NETWORK_LINK_INSTAGRAM: {
                promise = getInstagramAccessToken().then((accessToken) => {
                    return api.user.linkNetwork(token, secret, {
                        instagram_token: accessToken
                    });
                });
                break;
            }

            default:
                promise = Promise.reject(`Network not found: ${network}`);
                break;
        }

        return dispatch({
            type: LINK_NETWORK,
            network,
            profileKey: `${network}_id`,
            promise
        });
    };
}

export function unlinkNetwork(network) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: UNLINK_NETWORK,
            key: `${network}_id`,
            promise: api.user.unlinkNetwork(network, token, secret)
        });
    };
}

export function validateArtist(artistData, socials) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const body = {
            artist: artistData,
            socials: socials
        };

        return dispatch({
            type: VALIDATE_ARTIST,
            promise: api.artist
                .validateArtist(body, token, secret)
                .then((response) => {
                    if (response.verified) {
                        return api.user.get(token, secret).then((data) => {
                            return {
                                user: data,
                                verified: response.verified
                            };
                        });
                    }
                    return response;
                })
        });
    };
}

export function deleteExternalRssFeedUrl() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: DELETE_EXTERNAL_RSS_FEED_URL,
            promise: api.user.deleteExternalRssFeedUrl(token, secret)
        });
    };
}

export function getExternalRssFeedUrl() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_EXTERNAL_RSS_FEED_URL,
            promise: api.user.getExternalRssFeedUrl(token, secret)
        });
    };
}

export function setExternalRssFeedUrl(url) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: SET_EXTERNAL_RSS_FEED_URL,
            promise: api.user.setExternalRssFeedUrl(token, secret, {
                url
            })
        });
    };
}

export function setPremium() {
    return (dispatch) => {
        return dispatch({
            type: SET_USER_PREMIUM,
            subscription: 'Premium1'
        });
    };
}
