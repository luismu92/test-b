import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { MAX_PINS } from 'constants/user/pinned';
import analytics, { eventCategory, eventAction } from 'utils/analytics';

const PREFIX = 'am/user/pinned/';

const SAVE_PINNED = `${PREFIX}SAVE_PINNED`;
const GET_PINNED = `${PREFIX}GET_PINNED`;

export const ADD_PINNED = `${PREFIX}ADD_PINNED`;
export const DELETE_PINNED = `${PREFIX}DELETE_PINNED`;

const defaultState = {
    loading: false,
    list: [],
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PINNED): {
            return {
                ...state,
                errors: [],
                loading: true
            };
        }

        case failSuffix(GET_PINNED): {
            return {
                ...state,
                error: action.error,
                loading: false
            };
        }

        case GET_PINNED: {
            return {
                ...state,
                error: defaultState.error,
                list: action.resolved,
                loading: false
            };
        }

        case requestSuffix(ADD_PINNED):
            return {
                ...state,
                loading: true,
                list: action.newList
            };

        case failSuffix(ADD_PINNED): {
            return {
                ...state,
                loading: false,
                list: action.oldList,
                error: action.error
            };
        }

        case ADD_PINNED: {
            return {
                ...state,
                loading: false
            };
        }

        case requestSuffix(DELETE_PINNED):
            return {
                ...state,
                loading: true,
                list: action.newList
            };

        case failSuffix(DELETE_PINNED): {
            return {
                ...state,
                loading: false,
                list: action.oldList,
                error: action.error
            };
        }

        case DELETE_PINNED: {
            return {
                ...state,
                loading: false
            };
        }

        case requestSuffix(SAVE_PINNED): {
            return {
                ...state,
                loading: true,
                list: action.newItems
            };
        }

        case failSuffix(SAVE_PINNED): {
            return {
                ...state,
                loading: false,
                list: action.oldList,
                error: action.error
            };
        }

        case SAVE_PINNED: {
            return {
                ...state,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getPinned() {
    return (dispatch, getState) => {
        const state = getState();
        const { profile } = state.currentUser;
        const slug = profile.url_slug;

        return dispatch({
            type: GET_PINNED,
            slug,
            promise: api.artist.getPinned(slug)
        });
    };
}

export function addPinned(musicItems, autosave = true) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret, profile, isLoggedIn } = state.currentUser;
        const artistProfile = state.artist.profile;
        const { list } = state.currentUserPinned;
        const slug = profile.url_slug;
        const ownsPage =
            isLoggedIn &&
            artistProfile &&
            profile.url_slug === artistProfile.url_slug;
        let items = musicItems;

        if (!Array.isArray(musicItems)) {
            items = [musicItems];
        }
        const newList = items.concat(list).slice(0, MAX_PINS);
        const entities = newList.map((item) => {
            const id = item.song_id || item.id;

            return { kind: item.type, id: id };
        });
        const promise = autosave
            ? api.artist.addPinned(slug, entities, token, secret)
            : Promise.resolve();

        analytics.track(eventCategory.user, {
            eventAction: eventAction.userAddPinned,
            eventValue: items.length
        });

        return dispatch({
            type: ADD_PINNED,
            slug,
            ownsPage,
            newItems: items,
            newList,
            oldList: list,
            promise: promise
        });
    };
}

export function savePinned(musicItems, autosave = true) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret, profile, isLoggedIn } = state.currentUser;
        const { list } = state.currentUserPinned;
        const artistProfile = state.artist.profile;
        const slug = profile.url_slug;
        const ownsPage =
            isLoggedIn &&
            artistProfile &&
            profile.url_slug === artistProfile.url_slug;
        let items = musicItems;

        if (!Array.isArray(musicItems)) {
            items = [musicItems];
        }

        const entities = items.map((item, i) => {
            const id = item.song_id || item.id;

            return { kind: item.type, id: id, position: i };
        });
        const promise = autosave
            ? api.artist.savePinned(slug, entities, token, secret)
            : Promise.resolve();

        analytics.track(eventCategory.user, {
            eventAction: eventAction.userSavedPinned,
            eventValue: items.length
        });

        return dispatch({
            type: SAVE_PINNED,
            slug,
            ownsPage,
            newItems: items,
            oldList: list,
            promise: promise
        });
    };
}

export function deletePinned(musicItems, autosave = true) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret, profile, isLoggedIn } = state.currentUser;
        const { list } = state.currentUserPinned;
        const artistProfile = state.artist.profile;
        const slug = profile.url_slug;
        const ownsPage =
            isLoggedIn &&
            artistProfile &&
            profile.url_slug === artistProfile.url_slug;
        let items = musicItems;

        if (!Array.isArray(musicItems)) {
            items = [musicItems];
        }

        const entities = items.map((item) => {
            const id = item.song_id || item.id;

            return { kind: item.type, id: id };
        });
        const promise = autosave
            ? api.artist.deletePinned(slug, entities, token, secret)
            : Promise.resolve();
        const deletionIds = items.map((item) => {
            return item.song_id || item.id;
        });

        const newList = list.filter((item) => {
            const id = item.song_id || item.id;

            return !deletionIds.includes(id);
        });

        analytics.track(eventCategory.user, {
            eventAction: eventAction.userDeletePinned,
            eventValue: items.length
        });

        return dispatch({
            type: DELETE_PINNED,
            slug,
            ownsPage,
            newItems: items,
            oldList: list,
            newList,
            promise: promise
        });
    };
}
