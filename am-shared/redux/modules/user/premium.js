import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

export const PREFIX = 'am/user/premium/';

export const GET_CHECKOUT_SESSION = `${PREFIX}GET_CHECKOUT_SESSION`;
export const GET_UPDATE_SESSION = `${PREFIX}GET_UPDATE_SESSION`;
export const GET_SUBSCRIPTION = `${PREFIX}GET_SUBSCRIPTION`;
export const CANCEL_SUBSCRIPTION = `${PREFIX}CANCEL_SUBSCRIPTION`;
export const REACTIVATE_SUBSCRIPTION = `${PREFIX}REACTIVATE_SUBSCRIPTION`;

const defaultState = {
    loading: false,
    subscription: false,
    trial: true,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_CHECKOUT_SESSION):
        case requestSuffix(GET_UPDATE_SESSION):
            return {
                ...state,
                error: null,
                loading: true
            };

        case failSuffix(GET_CHECKOUT_SESSION):
        case failSuffix(GET_UPDATE_SESSION):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case requestSuffix(GET_SUBSCRIPTION):
        case requestSuffix(CANCEL_SUBSCRIPTION):
        case requestSuffix(REACTIVATE_SUBSCRIPTION):
            return {
                ...state,
                error: null,
                loading: true
            };

        case failSuffix(GET_SUBSCRIPTION):
        case failSuffix(CANCEL_SUBSCRIPTION):
        case failSuffix(REACTIVATE_SUBSCRIPTION):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_SUBSCRIPTION:
        case CANCEL_SUBSCRIPTION:
        case REACTIVATE_SUBSCRIPTION:
            return {
                ...state,
                error: null,
                loading: false,
                subscription: action.resolved.current,
                trial:
                    action.resolved.previous === null &&
                    action.resolved.current === null
            };

        default:
            return state;
    }
}

export function getCheckoutSession() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_CHECKOUT_SESSION,
            promise: api.user.getCheckoutSession(token, secret)
        });
    };
}

export function getCheckoutUpdateSession() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_UPDATE_SESSION,
            promise: api.user.getCheckoutUpdateSession(token, secret)
        });
    };
}

export function getSubscription() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_SUBSCRIPTION,
            promise: api.user.getSubscription(token, secret)
        });
    };
}

export function cancelSubscription() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: CANCEL_SUBSCRIPTION,
            promise: api.user.cancelSubscription(token, secret)
        });
    };
}

export function reactivateSubscription() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: REACTIVATE_SUBSCRIPTION,
            promise: api.user.reactivateSubscription(token, secret)
        });
    };
}
