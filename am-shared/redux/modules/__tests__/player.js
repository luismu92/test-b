/* global test, expect, describe */
import { combineReducers } from 'redux';
import { configureStore } from 'redux/configureStore';

import playerReducer from '../player';
import * as actions from '../player';

function getDefaultStore(initialState) {
    const state = {
        stats: {},
        currentUser: {},
        ad: {},
        router: {
            location: {
                key: 'adsfafsdf'
            }
        },
        player: {
            ...playerReducer(undefined, { type: 'gimmeDefaultState' }),
            ...playerReducer(initialState, { type: 'gimmeDefaultState' })
        }
    };

    return configureStore(
        state,
        combineReducers({
            player: playerReducer,
            // The player reducer requires reading properties on
            // these other reducers for some actions so we just
            // return a regular object for those.
            router: () => state.router,
            ad: () => state.ad,
            currentUser: () => state.currentUser,
            stats: () => state.stats
        })
    );
}

test('should return respond to pause', () => {
    const action = actions.pause();

    expect(action).toMatchSnapshot();

    const nextState = playerReducer(undefined, action);

    expect(nextState.paused).toBe(true);
});

describe('#setMusicItem', () => {
    test('it should set currentTime to 0 for a new song', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song.mp3'
            }
        ];
        const initialState = {
            queue,
            currentSong: queue[0],
            queueIndex: 0,
            currentTime: 10,
            paused: true
        };

        const store = getDefaultStore(initialState);
        const action = await store.dispatch(actions.setMusicItem(queue[1]));

        expect(action).toMatchSnapshot();

        const nextState = store.getState().player;

        expect(nextState.currentTime).toBe(0);
        expect(nextState.currentSong).toMatchSnapshot();
    });

    test('it should not set currentTime to 0 for the same song', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song.mp3'
            }
        ];
        const song = queue[0];
        const time = 10;
        const initialState = {
            queue,
            currentSong: song,
            queueIndex: 0,
            currentTime: time,
            paused: false
        };
        const store = getDefaultStore(initialState);
        const action = await store.dispatch(actions.setMusicItem(song));

        expect(action).toMatchSnapshot();

        const nextState = store.getState().player;

        expect(nextState.currentTime).toBe(time);
    });

    test("it should set the currentSong.id to the album's first track.song_id", async function() {
        const queue = [
            {
                title: 1,
                type: 'album',
                id: 1,
                tracks: [
                    {
                        download_url:
                            'https://songs.dev.audiomack.com/albums/t-gut/sound-kit/',
                        duration: 260,
                        song_id: 741,
                        streaming_url:
                            'https://songs.dev.audiomack.com/albums/t-gut/sound-kit/',
                        title: 'Legend (Prod by KE on The Track)',
                        volume_data: '[1,2]'
                    }
                ]
            },
            {
                title: 1,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song2.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 3,
                streaming_url: 'http://some.site/song3.mp3'
            }
        ];
        const song = queue[0];
        const time = 10;
        const initialState = {
            queue,
            currentSong: song,
            queueIndex: 0,
            currentTime: time,
            paused: false
        };
        const store = getDefaultStore(initialState);
        const action = await store.dispatch(actions.setMusicItem(song));

        expect(action).toMatchSnapshot();

        const nextState = store.getState().player;

        expect(nextState.currentSong.id).toMatchSnapshot();
    });
});

test('play() should set paused to false', async function() {
    const queue = [
        { title: 1, type: 'song', id: 1 },
        { title: 2, type: 'song', id: 2 }
    ];
    const initialState = {
        queue,
        currentSong: null,
        queueIndex: 0,
        currentTime: 10,
        paused: true
    };

    const store = getDefaultStore(initialState);

    await store.dispatch(actions.play());

    const nextState = store.getState().player;

    expect(nextState.paused).toBe(false);
});

describe('#next', () => {
    test('it should advance to the next song', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song2.mp3'
            }
        ];
        const initialState = {
            queue,
            currentSong: queue[0],
            queueIndex: 0,
            currentTime: 10
        };
        const store = getDefaultStore(initialState);

        await store.dispatch(actions.next());

        const nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(1);
        expect(nextState.currentTime).toBe(0);
    });

    test('it should return back to the beginning of the songs list if at the end', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song2.mp3'
            }
        ];
        const index = queue.length - 1;
        const initialState = {
            queue,
            currentSong: queue[index],
            queueIndex: index,
            currentTime: 10
        };
        const store = getDefaultStore(initialState);

        await store.dispatch(actions.next());

        const nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(0);
        expect(nextState.currentTime).toBe(0);
    });

    test('it should play through albums in the queue before continuing to the next song', async function() {
        const album = {
            title: 'album title',
            tracks: [
                {
                    title: 'album song 1',
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    streaming_url: 'http://album_song_2'
                }
            ],
            type: 'album'
        };
        const song = {
            title: 'song title',
            streaming_url: 'http://song_title',
            type: 'song'
        };
        const queue = [album, song];
        let queueIndex = 0;
        const initialState = {
            currentSong: album.tracks[0],
            queueIndex: queueIndex,
            currentTime: 10
        };

        const store = getDefaultStore(initialState);

        store.dispatch(actions.editQueue(queue));

        let nextState = store.getState().player;

        // First track in album
        expect(nextState.queueIndex).toBe(queueIndex);

        // Second track in album
        let action = await store.dispatch(actions.next());

        queueIndex += 1;

        nextState = playerReducer(nextState, action);

        expect(nextState.queueIndex).toBe(queueIndex);

        // Next song
        action = await store.dispatch(actions.next());

        queueIndex += 1;

        nextState = playerReducer(nextState, action);

        expect(nextState.currentSong.title).toBe(song.title);
        expect(nextState.queueIndex).toBe(queueIndex);
    });
});

describe('#prev', () => {
    test('it should go to the previous song if currentTime not > 4', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song2.mp3'
            }
        ];
        const index = 1;
        const initialState = {
            queue,
            currentSong: queue[index],
            queueIndex: index,
            currentTime: 4
        };
        const store = getDefaultStore(initialState);

        await store.dispatch(actions.prev());

        const nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(0);
        expect(nextState.currentTime).toBe(0);
    });

    test('it should go back to beginning of current song if currentTime > 4', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song2.mp3'
            }
        ];
        const index = 1;
        const initialState = {
            queue,
            currentSong: queue[index],
            queueIndex: index,
            currentTime: 10
        };

        const store = getDefaultStore(initialState);

        await store.dispatch(actions.prev());

        const nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(index);
        expect(nextState.currentTime).toBe(0);
    });

    test('it should go back to beginning of current album track if currentTime > 4', async function() {
        const queue = [
            {
                id: 1,
                title: 'some album',
                type: 'album',
                tracks: [
                    {
                        title: 1,
                        type: 'song',
                        id: 1,
                        streaming_url: 'http://some.site/song.mp3'
                    },
                    {
                        title: 2,
                        type: 'song',
                        id: 2,
                        streaming_url: 'http://some.site/song2.mp3'
                    }
                ]
            }
        ];
        const index = 0;
        const initialState = {
            queue,
            currentSong: queue[index],
            queueIndex: index,
            currentTime: 10
        };

        const store = getDefaultStore(initialState);

        await store.dispatch(actions.prev());

        const nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(index);
        expect(nextState.currentTime).toBe(0);
    });

    test('it should return back to the end of the songs list if at the beginning', async function() {
        const queue = [
            {
                title: 1,
                type: 'song',
                id: 1,
                streaming_url: 'http://some.site/song.mp3'
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song2.mp3'
            }
        ];
        const index = 0;
        const initialState = {
            queue,
            currentSong: queue[index],
            queueIndex: index,
            currentTime: 0
        };
        const store = getDefaultStore(initialState);

        await store.dispatch(actions.prev());

        const nextState = store.getState().player;

        expect(nextState.currentSong).toBe(queue[queue.length - 1]);
        expect(nextState.currentTime).toBe(0);
    });

    test('it should play back through album tracks before continuing to the prev song', async function() {
        const song1 = {
            title: 'song title 1',
            streaming_url: 'http://song_title',
            type: 'song'
        };
        const album = {
            title: 'album title',
            tracks: [
                {
                    title: 'album song 1',
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    streaming_url: 'http://album_song_2'
                }
            ],
            type: 'album'
        };
        const song2 = {
            title: 'song title 2',
            streaming_url: 'http://song_title2',
            type: 'song'
        };
        const queue = [song1, album, song2];
        let queueIndex = 2;
        const initialState = {
            currentSong: album.tracks[1],
            queueIndex: queueIndex,
            currentTime: 4
        };

        const store = getDefaultStore(initialState);

        store.dispatch(actions.editQueue(queue));

        let nextState = store.getState().player;

        // Starts on second track in album
        expect(nextState.queueIndex).toBe(queueIndex);

        // first track in album
        await store.dispatch(actions.prev());

        queueIndex -= 1;

        nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(queueIndex);

        // song1
        await store.dispatch(actions.prev());

        queueIndex -= 1;

        nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(queueIndex);

        // song2
        await store.dispatch(actions.prev());

        queueIndex -= 1;

        nextState = store.getState().player;

        expect(nextState.queueIndex).toBe(nextState.queue.length - 1);

        // Back to last track of album
        await store.dispatch(actions.prev());

        nextState = store.getState().player;
        expect(nextState.queueIndex).toBe(2);
    });

    test('it should play back through album tracks successfully', async function() {
        const album1 = {
            title: 'album title',
            id: 1,
            tracks: [
                {
                    title: 'album song 1',
                    id: 10,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 11,
                    streaming_url: 'http://album_song_2'
                }
            ],
            type: 'album'
        };
        const album2 = {
            title: 'album title 2',
            id: 2,
            tracks: [
                {
                    title: 'album song 1',
                    id: 12,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 13,
                    streaming_url: 'http://album_song_2'
                },
                {
                    title: 'album song 3',
                    id: 14,
                    streaming_url: 'http://album_song_3'
                }
            ],
            type: 'album'
        };
        let queueIndex = 3;
        const initialState = {
            currentSong: album2.tracks[1],
            queueIndex: queueIndex,
            currentTime: 4
        };

        const store = getDefaultStore(initialState);
        const queue = [album1, album2];

        store.dispatch(actions.editQueue(queue));

        let nextState = store.getState().player;

        // Starts on second track in album2
        expect(nextState.queueIndex).toBe(queueIndex);

        // first track in album2
        await store.dispatch(actions.prev());

        queueIndex -= 1;

        nextState = store.getState().player;

        expect(nextState.currentSong.title).toBe(album2.tracks[0].title);
        expect(nextState.queueIndex).toBe(queueIndex);

        // album1 track 2
        await store.dispatch(actions.prev());

        queueIndex -= 1;

        nextState = store.getState().player;

        expect(nextState.currentSong.title).toBe(album1.tracks[1].title);
        expect(nextState.queueIndex).toBe(queueIndex);

        // album1 track 1
        await store.dispatch(actions.prev());

        queueIndex -= 1;

        nextState = store.getState().player;

        expect(nextState.currentSong.title).toBe(album1.tracks[0].title);
        expect(nextState.queueIndex).toBe(queueIndex);

        // Back to last track of album2
        await store.dispatch(actions.prev());
        nextState = store.getState().player;
        expect(nextState.queueIndex).toBe(nextState.queue.length - 1);
    });
});

describe('#shuffleTracks', () => {
    test('it should return the original state when there is not at least 2 items to shuffle', () => {
        const store = getDefaultStore({
            currentSong: {
                type: 'song',
                id: 1
            }
        });
        const firstState = store.getState().player;
        const nextState = store.dispatch(actions.shuffleTracks());

        expect(nextState).toEqual(firstState);
    });
    test('it only shuffle the tracks after the current active queue index', () => {
        const album1 = {
            title: 'album title',
            id: 1,
            tracks: [
                {
                    title: 'album song 1',
                    id: 10,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 11,
                    streaming_url: 'http://album_song_2'
                }
            ],
            type: 'album'
        };
        const album2 = {
            title: 'album title 2',
            id: 2,
            tracks: [
                {
                    title: 'album song 1',
                    id: 12,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 13,
                    streaming_url: 'http://album_song_2'
                },
                {
                    title: 'album song 3',
                    id: 14,
                    streaming_url: 'http://album_song_3'
                }
            ],
            type: 'album'
        };
        const queueIndex = 2;
        const initialState = {
            currentSong: album1.tracks[1],
            queueIndex: queueIndex,
            currentTime: 4
        };

        const store = getDefaultStore(initialState);
        const queue = [album1, album2];

        store.dispatch(actions.editQueue(queue));
        store.dispatch(actions.shuffleTracks());

        const nextState = store.getState().player;

        // Starts on second track in album2
        expect(nextState.queue[0].id).toBe(album1.tracks[0].id);
        expect(nextState.queue[1].id).toBe(album1.tracks[1].id);
        expect(nextState.queue[2].id).toBe(album2.tracks[0].id);
    });
});

describe('#editQueue', () => {
    test('it should set `infinitePlaybackEnded` to false on append', () => {
        const album1 = {
            title: 'album title',
            id: 1,
            tracks: [
                {
                    title: 'album song 1',
                    id: 10,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 11,
                    streaming_url: 'http://album_song_2'
                }
            ],
            type: 'album'
        };
        const album2 = {
            title: 'album title 2',
            id: 2,
            tracks: [
                {
                    title: 'album song 1',
                    id: 12,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 13,
                    streaming_url: 'http://album_song_2'
                },
                {
                    title: 'album song 3',
                    id: 14,
                    streaming_url: 'http://album_song_3'
                }
            ],
            type: 'album'
        };
        const queueIndex = 2;
        const initialState = {
            queue: [album1, album2],
            currentSong: album1.tracks[1],
            queueIndex: queueIndex,
            currentTime: 4,
            infinitePlaybackEnded: true
        };

        const store = getDefaultStore(initialState);
        const newItems = [
            {
                title: 'another song',
                id: 134,
                streaming_url: 'http://album_song_33'
            }
        ];

        store.dispatch(actions.editQueue(newItems, { append: true }));

        const nextState = store.getState().player;

        expect(nextState.infinitePlaybackEnded).toBe(false);
    });
});

describe('#clearQueue', () => {
    test('it should set `infinitePlaybackEnded` to true', () => {
        const album1 = {
            title: 'album title',
            id: 1,
            tracks: [
                {
                    title: 'album song 1',
                    id: 10,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 11,
                    streaming_url: 'http://album_song_2'
                }
            ],
            type: 'album'
        };
        const album2 = {
            title: 'album title 2',
            id: 2,
            tracks: [
                {
                    title: 'album song 1',
                    id: 12,
                    streaming_url: 'http://album_song_1'
                },
                {
                    title: 'album song 2',
                    id: 13,
                    streaming_url: 'http://album_song_2'
                },
                {
                    title: 'album song 3',
                    id: 14,
                    streaming_url: 'http://album_song_3'
                }
            ],
            type: 'album'
        };
        const queueIndex = 2;
        const initialState = {
            queue: [album1, album2],
            currentSong: album1.tracks[1],
            queueIndex: queueIndex,
            currentTime: 4,
            infinitePlaybackEnded: false
        };

        const store = getDefaultStore(initialState);

        store.dispatch(actions.clearQueue());

        const nextState = store.getState().player;

        expect(nextState.infinitePlaybackEnded).toBe(true);
    });
});

test('setChromecastState(false) should set chromecastActive to false', async function() {
    const queue = [
        { title: 1, type: 'song', id: 1 },
        { title: 2, type: 'song', id: 2 }
    ];
    const initialState = {
        queue,
        currentSong: null,
        queueIndex: 0,
        currentTime: 10,
        chromecastActive: true,
        paused: true
    };

    const store = getDefaultStore(initialState);

    await store.dispatch(actions.setChromecastState(false));

    const nextState = store.getState().player;

    expect(nextState.chromecastActive).toBe(false);
});

test('setChromecastState(true) should set chromecastActive to true', async function() {
    const queue = [
        { title: 1, type: 'song', id: 1 },
        { title: 2, type: 'song', id: 2 }
    ];
    const initialState = {
        queue,
        currentSong: null,
        queueIndex: 0,
        currentTime: 10,
        chromecastActive: false,
        paused: true
    };

    const store = getDefaultStore(initialState);

    await store.dispatch(actions.setChromecastState(true));

    const nextState = store.getState().player;

    expect(nextState.chromecastActive).toBe(true);
});

test('setChromecastState(false) should set chromecastLoading to false', async function() {
    const queue = [
        { title: 1, type: 'song', id: 1 },
        { title: 2, type: 'song', id: 2 }
    ];
    const initialState = {
        queue,
        currentSong: null,
        queueIndex: 0,
        currentTime: 10,
        chromecastLoading: true,
        paused: true
    };

    const store = getDefaultStore(initialState);

    await store.dispatch(actions.setChromecastState(false));

    const nextState = store.getState().player;

    expect(nextState.chromecastLoading).toBe(false);
});
