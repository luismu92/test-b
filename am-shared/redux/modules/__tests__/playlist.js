/* global test, expect, describe */
import { combineReducers } from 'redux';
import { configureStore } from 'redux/configureStore';
import { requestSuffix } from 'utils/index';
import playlistReducer, { ADD_SONG_TO_PLAYLIST } from '../playlist';

function getDefaultStore(initialState) {
    const state = {
        currentUser: {},
        playlist: {
            ...playlistReducer(undefined, { type: 'gimmeDefaultState' }),
            ...playlistReducer(initialState, { type: 'gimmeDefaultState' })
        }
    };

    return configureStore(
        state,
        combineReducers({
            playlist: playlistReducer,
            // The player reducer requires reading properties on
            // these other reducers for some actions so we just
            // return a regular object for those.
            currentUser: () => state.currentUser
        })
    );
}

describe('#addSongToPlaylist', () => {
    test('it should not add the song to the playlist if the current playlist is different than the destination playlist', async function() {
        const initialState = {
            data: {
                id: 24,
                tracks: []
            }
        };
        const song = {
            id: 10389
        };
        const store = getDefaultStore(initialState);
        const currentTrackLength = initialState.data.tracks.length;
        const action = {
            type: requestSuffix(ADD_SONG_TO_PLAYLIST),
            song,
            playlistId: 1
        };
        const nextState = playlistReducer(store.getState().playlist, action);

        expect(nextState.data.tracks.length).toBe(currentTrackLength);
    });

    test('it should add the song to the playlist if the current playlist is the same as the destination playlist', async function() {
        const initialState = {
            data: {
                id: 24,
                tracks: []
            }
        };
        const song = {
            id: 10389
        };
        const store = getDefaultStore(initialState);
        const currentTrackLength = initialState.data.tracks.length;
        const action = {
            type: requestSuffix(ADD_SONG_TO_PLAYLIST),
            song,
            playlistId: initialState.data.id
        };
        const nextState = playlistReducer(store.getState().playlist, action);

        expect(nextState.data.tracks.length).toBe(currentTrackLength + 1);
    });
});
