import api from 'api/index';
import { suffix } from 'utils/index';
import { DELETE_PLAYLIST } from '../playlist';

const PREFIX = 'am/music/browsePlaylist/';

const FETCH_PLAYLISTS = `${PREFIX}FETCH_PLAYLISTS`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const PREV_PAGE = `${PREFIX}PREV_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const FETCH_TAGGED_PLAYLISTS = `${PREFIX}FETCH_TAGGED_PLAYLISTS`;
const FETCH_ALL_PLAYLIST_TAGS = `${PREFIX}FETCH_ALL_PLAYLIST_TAGS`;

const defaultState = {
    loading: false,
    errors: [],
    page: 1,
    onLastPage: false,
    list: [],
    tags: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(FETCH_PLAYLISTS, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(FETCH_PLAYLISTS, 'FAILURE'):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case FETCH_PLAYLISTS: {
            let newData = action.resolved.results;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (!action.resolved.results.length) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                list: newData,
                loading: false,
                onLastPage
            });
        }

        case DELETE_PLAYLIST:
            return Object.assign({}, state, {
                list: state.list.map((playlistObj) => {
                    const list = playlistObj.list;
                    const newList = list.filter((playlist) => {
                        return playlist.id !== action.id;
                    });

                    return {
                        ...playlistObj,
                        list: newList
                    };
                }, {})
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case PREV_PAGE:
            return Object.assign({}, state, {
                page: Math.max(1, state.page - 1)
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case CLEAR_LIST:
            return Object.assign({}, state, {
                list: []
            });

        case suffix(FETCH_TAGGED_PLAYLISTS, 'REQUEST'):
            return Object.assign({}, state, {
                loading: true
            });

        case suffix(FETCH_TAGGED_PLAYLISTS, 'FAILURE'):
            return Object.assign({}, state, {
                loading: false
            });

        case FETCH_TAGGED_PLAYLISTS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (!results || !results.length) {
                onLastPage = true;
            }

            return {
                ...state,
                list: newData,
                loading: false,
                title: action.resolved.title,
                tag: action.tag,
                onLastPage
            };
        }

        case suffix(FETCH_ALL_PLAYLIST_TAGS, 'REQUEST'):
            return Object.assign({}, state, {
                loading: true
            });

        case suffix(FETCH_ALL_PLAYLIST_TAGS, 'FAILURE'):
            return Object.assign({}, state, {
                loading: false
            });

        case FETCH_ALL_PLAYLIST_TAGS: {
            return {
                ...state,
                loading: false,
                tags: action.resolved.results.categories
            };
        }

        default:
            return state;
    }
}

export function fetchPlaylistList(options = {}) {
    return (dispatch, getState) => {
        const state = getState().musicBrowsePlaylist;

        const { page = state.page, limit = 4 } = options;

        const promise = api.playlist.tags('', page, limit).then((data) => {
            return {
                ...data,
                results: data.results.map((item) => {
                    const title = Object.keys(item)[0];

                    return {
                        title,
                        list: item[title],
                        loading: false,
                        onLastPage: item[title].length < limit
                    };
                }, [])
            };
        });

        return dispatch({
            type: FETCH_PLAYLISTS,
            limit,
            page,
            promise
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: parseInt(page, 10)
    };
}

export function prevPage() {
    return {
        type: PREV_PAGE
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}

export function fetchTaggedPlaylists(tag, limit = 20, page) {
    return {
        type: FETCH_TAGGED_PLAYLISTS,
        tag,
        limit,
        promise: api.playlist.playlistTags(tag, limit, false, page)
    };
}

export function fetchAllPlaylistTags() {
    return {
        type: FETCH_ALL_PLAYLIST_TAGS,
        promise: api.playlist.playlistTags('', 999, true)
    };
}
