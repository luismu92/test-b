import api from 'api/index';
import { strToNumber, requestSuffix, failSuffix } from 'utils/index';
import storage, { STORAGE_RECENT_PLAYLIST_ADD } from 'utils/storage';

import { TRACK_PLAY } from './stats';

export const PREFIX = 'am/playlist/';

const GET_PLAYLIST_INFO_BY_SLUG = `${PREFIX}GET_PLAYLIST_INFO_BY_SLUG`;
const GET_PLAYLIST_INFO = `${PREFIX}GET_PLAYLIST_INFO`;
const CLEAR_ADD_SONG = 'am/playlist/CLEAR_ADD_SONG';
const ADD_SONG = `${PREFIX}ADD_SONG`;
const REORDER_TRACKS = `${PREFIX}REORDER_TRACKS`;
const SAVE_PLAYLIST_DETAILS = `${PREFIX}SAVE_PLAYLIST_DETAILS`;
const GET_FEATURED_PLAYLISTS = `${PREFIX}GET_FEATURED_PLAYLISTS`;
const RESET = `${PREFIX}RESET`;

export const CREATE_PLAYLIST = `${PREFIX}CREATE_PLAYLIST`;
export const ADD_SONG_TO_PLAYLIST = `${PREFIX}ADD_SONG_TO_PLAYLIST`;
export const DELETE_SONG_FROM_PLAYLIST = `${PREFIX}DELETE_SONG_FROM_PLAYLIST`;
export const DELETE_PLAYLIST = `${PREFIX}DELETE_PLAYLIST`;
export const FAVORITE_PLAYLIST = `${PREFIX}FAVORITE_PLAYLIST`;
export const UNFAVORITE_PLAYLIST = `${PREFIX}UNFAVORITE_PLAYLIST`;

const defaultState = {
    songToAdd: null,
    songToAddCallback: null,
    loading: false,
    data: null,
    errors: [],
    featured: [],
    featuredPage: 1,
    featuredDone: false
};

// eslint-disable-next-line complexity
export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case CLEAR_ADD_SONG:
            return {
                ...state,
                songToAdd: defaultState.songToAdd
            };

        case ADD_SONG:
            return {
                ...state,
                songToAdd: action.song,
                songToAddCallback: action.addSongCallback
            };

        case SAVE_PLAYLIST_DETAILS: {
            const newState = {
                ...state,
                errors: [],
                loading: false
            };

            if (state.data) {
                newState.data = {
                    ...newState.data,
                    ...action.resolved
                };
            }

            return newState;
        }

        case GET_PLAYLIST_INFO_BY_SLUG:
        case GET_PLAYLIST_INFO: {
            const playlist = action.resolved.results;

            return {
                ...state,
                data: playlist,
                loading: false
            };
        }

        case requestSuffix(SAVE_PLAYLIST_DETAILS):
        case requestSuffix(GET_PLAYLIST_INFO_BY_SLUG):
        case requestSuffix(GET_PLAYLIST_INFO):
        case requestSuffix(CREATE_PLAYLIST): {
            return {
                ...state,
                loading: true,
                errors: []
            };
        }

        case failSuffix(GET_PLAYLIST_INFO_BY_SLUG):
        case failSuffix(GET_PLAYLIST_INFO):
        case failSuffix(CREATE_PLAYLIST): {
            return Object.assign({}, state, {
                errors: [action.error],
                loading: false
            });
        }

        case CREATE_PLAYLIST: {
            return Object.assign({}, state, {
                loading: false
            });
        }

        case failSuffix(SAVE_PLAYLIST_DETAILS): {
            const errors = state.errors.concat(action.error).filter(Boolean);

            return Object.assign({}, state, {
                errors,
                loading: false
            });
        }

        case requestSuffix(DELETE_SONG_FROM_PLAYLIST): {
            if (!state.data) {
                return state;
            }

            let index = -1;
            const newData = {
                ...state.data
            };
            const itemToDelete = newData.tracks.filter((track, i) => {
                if (track.id === action.songId) {
                    index = i;
                    return true;
                }

                return false;
            })[0];

            if (!itemToDelete) {
                return state;
            }

            itemToDelete.deleting = true;
            newData.tracks[index] = itemToDelete;

            return {
                ...state,
                data: newData
            };
        }

        case failSuffix(DELETE_SONG_FROM_PLAYLIST): {
            if (!state.data) {
                return state;
            }

            let index = -1;
            const newData = {
                ...state.data
            };
            const itemToDelete = newData.tracks.filter((track, i) => {
                if (track.id === action.songId) {
                    index = i;
                    return true;
                }

                return false;
            })[0];

            if (!itemToDelete) {
                return state;
            }

            itemToDelete.deleting = false;
            newData.tracks[index] = itemToDelete;

            return {
                ...state,
                data: newData
            };
        }

        case DELETE_SONG_FROM_PLAYLIST: {
            if (!state.data) {
                return state;
            }

            let index = -1;
            const newData = {
                ...state.data
            };
            const itemToDelete = newData.tracks.filter((track, i) => {
                if (track.id === action.songId) {
                    index = i;
                    return true;
                }

                return false;
            })[0];

            if (!itemToDelete) {
                return state;
            }

            newData.tracks.splice(index, 1);

            return {
                ...state,
                data: newData
            };
        }

        case requestSuffix(ADD_SONG_TO_PLAYLIST): {
            if (!state.data || state.data.id !== action.playlistId) {
                return state;
            }

            const newData = {
                ...state.data
            };

            newData.tracks = Array.from(newData.tracks)
                .filter((track) => {
                    return track.id !== action.song.id;
                })
                .concat(action.song);

            return {
                ...state,
                data: newData
            };
        }

        case failSuffix(ADD_SONG_TO_PLAYLIST): {
            if (!state.data) {
                return state;
            }

            const newData = {
                ...state.data
            };

            newData.tracks = newData.tracks.filter((track) => {
                return track.id === action.song.id;
            });

            return {
                ...state,
                data: newData
            };
        }

        case requestSuffix(REORDER_TRACKS):
            return {
                ...state,
                data: {
                    ...state.data,
                    tracks: action.newTracks
                }
            };

        case failSuffix(REORDER_TRACKS):
            return {
                ...state,
                data: {
                    ...state.data,
                    tracks: action.oldTracks
                }
            };

        case requestSuffix(GET_FEATURED_PLAYLISTS): {
            const newState = {
                loading: true
            };

            if (action.playlist) {
                const featured = Array.from(state.featured).map((list) => {
                    const title = list.title;

                    if (title === action.playlist) {
                        list.loading = true;
                    }

                    return list;
                });

                newState.featured = featured;
            }

            return Object.assign({}, state, newState);
        }

        case failSuffix(GET_FEATURED_PLAYLISTS): {
            const newState = {
                loading: false
            };

            if (action.playlist) {
                const featured = Array.from(state.featured).map((list) => {
                    const title = list.title;

                    if (title === action.playlist) {
                        list.loading = false;
                        list.done = true;
                    }

                    return list;
                });

                newState.featured = featured;
            }

            return Object.assign({}, state, newState);
        }

        case GET_FEATURED_PLAYLISTS: {
            let results = action.resolved.results;
            let featuredDone = false;

            // If we're fetching a specific list, just add results
            // to it
            if (action.playlist) {
                results = Array.from(state.featured).map((list) => {
                    const title = list.title;
                    const listResults = list.data;

                    if (title === action.playlist) {
                        list.data = listResults.concat(results);
                        list.loading = false;
                        list.page += 1;
                        list.done = !results.length;
                    }

                    return list;
                });
            }
            // Otherwise re-format the response
            else {
                results = results.map((result) => {
                    const title = Object.keys(result)[0];
                    const list = {
                        title,
                        data: result[title],
                        loading: false,
                        page: 1,
                        done: false
                    };

                    return list;
                });

                // Append new pages to the current featured
                if (action.page > 1) {
                    if (!results.length) {
                        featuredDone = true;
                    }

                    results = Array.from(state.featured).concat(results);
                }
            }

            return Object.assign({}, state, {
                featured: results,
                featuredPage: action.page,
                featuredDone,
                loading: false
            });
        }

        case TRACK_PLAY: {
            if (!state.data) {
                return state;
            }

            const playlist = {
                ...state.data
            };
            const playlistId = action.options.playlist_id;

            if (typeof playlistId === 'number' && playlist.id === playlistId) {
                // Only update if this is the first track play call, ie not the 30 second track call
                if (!action.options.time) {
                    playlist.stats['plays-raw'] += 1;
                }
            }

            return {
                ...state,
                data: playlist
            };
        }

        case failSuffix(UNFAVORITE_PLAYLIST):
        case requestSuffix(FAVORITE_PLAYLIST): {
            if (!state.data) {
                return state;
            }

            const playlist = {
                ...state.data
            };
            const playlistId = action.id;

            if (typeof playlistId === 'number' && playlist.id === playlistId) {
                playlist.stats['favorites-raw'] += 1;
            }

            return {
                ...state,
                data: playlist
            };
        }

        case failSuffix(FAVORITE_PLAYLIST):
        case requestSuffix(UNFAVORITE_PLAYLIST): {
            if (!state.data) {
                return state;
            }

            const playlist = {
                ...state.data
            };
            const playlistId = action.id;

            if (typeof playlistId === 'number' && playlist.id === playlistId) {
                playlist.stats['favorites-raw'] -= 1;
            }

            return {
                ...state,
                data: playlist
            };
        }

        // For now we'll just reset data. We might need to reset
        // everything eventually
        case RESET:
            return Object.assign({}, state, {
                data: defaultState.data
            });

        default:
            return state;
    }
}

export function getInfo(id) {
    return {
        type: GET_PLAYLIST_INFO,
        promise: api.playlist.get(id)
    };
}

export function getInfoWithSlugs(artistSlug, playlistSlug) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_PLAYLIST_INFO_BY_SLUG,
            force404OnError: /^\/playlist\//,
            promise: api.playlist.getBySlugs(
                artistSlug,
                playlistSlug,
                token,
                secret
            )
        });
    };
}

export function addSong(song, addSongCallback = null) {
    return {
        type: ADD_SONG,
        song: {
            ...song,
            id: song.id || song.song_id
        },
        addSongCallback
    };
}

export function clearAddSong() {
    return {
        type: CLEAR_ADD_SONG
    };
}

export function reorderTracks(playlistId, newTracks = []) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const data = {
            ...getState().playlist.data
        };
        const { tracks: oldTracks } = data;
        const ids = newTracks.map((track) => track.id);
        const newData = {
            ...data,
            music_id: ids.join(',')
        };

        return dispatch({
            type: REORDER_TRACKS,
            oldTracks,
            newTracks,
            promise: api.playlist.edit(playlistId, newData, token, secret)
        });
    };
}

function addPlaylistToStorage(id) {
    const parse = true;
    const stringify = true;
    const currentIds = storage.get(STORAGE_RECENT_PLAYLIST_ADD, parse) || [];
    const newIds = [id]
        .concat(currentIds.map((n) => parseInt(n, 10)).filter((n) => n !== id))
        .slice(0, 15);

    storage.set(STORAGE_RECENT_PLAYLIST_ADD, newIds, stringify);
}

export function addSongToPlaylist(playlistId, song, options = {}) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };
        const id = strToNumber(playlistId);
        const songToAdd = {
            ...song,
            id: song.id || song.song_id
        };

        return dispatch({
            type: ADD_SONG_TO_PLAYLIST,
            playlistId: id,
            songId: songToAdd.id,
            song: songToAdd,
            promise: api.playlist
                .addSong(id, songToAdd.id, token, secret, opts)
                .then((data) => {
                    addPlaylistToStorage(id);

                    return data;
                })
        });
    };
}

export function deleteSong(playlistId, songId) {
    let param = strToNumber(songId);

    if (Array.isArray(songId)) {
        param = songId;
    }

    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: DELETE_SONG_FROM_PLAYLIST,
            playlistId: strToNumber(playlistId),
            songId: param,
            promise: api.playlist.deleteSong(playlistId, songId, token, secret)
        });
    };
}

export function createPlaylist(options) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: CREATE_PLAYLIST,
            options,
            promise: api.playlist.create(options, token, secret)
        });
    };
}

export function savePlaylistDetails(id, details) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const merged = {
            ...state.playlist.data,
            ...details
        };

        return dispatch({
            type: SAVE_PLAYLIST_DETAILS,
            promise: api.playlist.edit(id, merged, token, secret)
        });
    };
}

export function deletePlaylist(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: DELETE_PLAYLIST,
            id,
            promise: api.playlist.delete(id, token, secret)
        });
    };
}

export function favoritePlaylist(playlistId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };

        return dispatch({
            type: FAVORITE_PLAYLIST,
            id: playlistId,
            promise: api.playlist.favorite(playlistId, token, secret, opts)
        });
    };
}

export function unfavoritePlaylist(playlistId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { section, environment, referer } = getState().stats;
        const opts = {
            ...options,
            section,
            environment,
            referer
        };

        return dispatch({
            type: UNFAVORITE_PLAYLIST,
            id: playlistId,
            promise: api.playlist.unfavorite(playlistId, token, secret, opts)
        });
    };
}

export function getFeatured(playlist = null, page = 1) {
    let promise;

    if (playlist && playlist.toLowerCase() === 'trending') {
        promise = api.playlist.get('trending', page);
    } else {
        promise = api.playlist.tags(playlist, page);
    }

    return {
        type: GET_FEATURED_PLAYLISTS,
        playlist,
        page,
        promise
    };
}

export function reset() {
    return {
        type: RESET
    };
}
