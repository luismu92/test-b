import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import Cookie from 'js-cookie';

const PREFIX = 'am/admin/';

export const SUSPEND_MUSIC = `${PREFIX}SUSPEND_MUSIC`;
export const UNSUSPEND_MUSIC = `${PREFIX}UNSUSPEND_MUSIC`;
export const BUMP_MUSIC = `${PREFIX}BUMP_MUSIC`;
export const BUMP_MUSIC_ONE_SPOT = `${PREFIX}BUMP_MUSIC_ONE_SPOT`;
export const LOGIN_AS_USER = `${PREFIX}LOGIN_AS_USER`;
export const LOGOUT_OF_USER = `${PREFIX}LOGOUT_OF_USER`;
export const VERIFY_ARTIST = `${PREFIX}VERIFY_ARTIST`;
export const CLEAR_UPLOAD_ERRORS = `${PREFIX}CLEAR_UPLOAD_ERRORS`;
const VERIFY_ARTIST_EMAIL = `${PREFIX}VERIFY_ARTIST_EMAIL`;
const TAKEDOWN_MUSIC = `${PREFIX}TAKEDOWN_MUSIC`;
const SET_MASQUERADE_VALUE = `${PREFIX}SET_MASQUERADE_VALUE`;
const RESTORE_MUSIC = `${PREFIX}RESTORE_MUSIC`;
const CENSOR_ARTWORK = `${PREFIX}CENSOR_ARTWORK`;
const TREND_MUSIC = `${PREFIX}TREND_MUSIC`;
const EXCLUDE_STATS = `${PREFIX}EXCLUDE_STATS`;
const CLEAR_STATS = `${PREFIX}CLEAR_STATS`;
const GET_MUSIC_SOURCE = `${PREFIX}GET_MUSIC_SOURCE`;
const GET_MUSIC_RECOGNITION = `${PREFIX}GET_MUSIC_RECOGNITION`;
const REFRESH_MUSIC_RECOGNITION = `${PREFIX}REFRESH_MUSIC_RECOGNITION`;
const REPAIR_MUSIC_URL = `${PREFIX}REPAIR_MUSIC_URL`;
const GET_GLOBAL_MUSIC_TAGS = `${PREFIX}GET_GLOBAL_MUSIC_TAGS`;
const GET_MUSIC_TAGS = `${PREFIX}GET_MUSIC_TAGS`;
const ADD_MUSIC_TAGS = `${PREFIX}ADD_MUSIC_TAGS`;
const DELETE_MUSIC_TAGS = `${PREFIX}DELETE_MUSIC_TAGS`;

const defaultState = {
    isMasquerading: false,
    tags: {
        list: [],
        isAdding: false,
        loading: false,
        error: null
    },
    recognition: {
        isLoading: false
    }
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case LOGIN_AS_USER:
            return {
                ...state,
                isMasquerading: true
            };

        case LOGOUT_OF_USER:
            return {
                ...state,
                isMasquerading: false
            };

        case SET_MASQUERADE_VALUE:
            return {
                ...state,
                isMasquerading: action.bool
            };

        case requestSuffix(GET_GLOBAL_MUSIC_TAGS):
            return {
                ...state,
                tags: {
                    ...state.tags,
                    loading: true
                }
            };

        case failSuffix(GET_GLOBAL_MUSIC_TAGS):
            return {
                ...state,
                tags: {
                    ...state.tags,
                    loading: false,
                    error: action.error
                }
            };

        case GET_GLOBAL_MUSIC_TAGS:
            return {
                ...state,
                tags: {
                    ...state.tags,
                    list: action.resolved,
                    loading: false
                }
            };

        // This logic is for when we allow arbitrary tags
        // case requestSuffix(ADD_MUSIC_TAGS): {
        //     const normalized = action.newTags.map((t) => t.normalizedKey);

        //     return {
        //         ...state,
        //         tags: {
        //             ...state.tags,
        //             isAdding: true,
        //             // Filter out existing duplicate tags in case we are retrying
        //             list: state.tags.list.filter((tag) => {
        //                 return !normalized.includes(tag);
        //             })
        //         }
        //     };
        // }

        // case failSuffix(ADD_MUSIC_TAGS):
        //     return {
        //         ...state,
        //         tags: {
        //             ...state.tags,
        //             isAdding: false,
        //             error: action.error
        //         }
        //     };

        // case ADD_MUSIC_TAGS: {
        //     const newNormalized = action.resolved.map((t) => t.normalizedKey);
        //     const newList = state.tags.list.filter((tag) => {
        //         return !newNormalized.includes(tag);
        //     });

        //     return {
        //         ...state,
        //         tags: {
        //             ...state.tags,
        //             list: newList.concat(action.resolved),
        //             isAdding: false
        //         }
        //     };
        // }

        // case requestSuffix(DELETE_MUSIC_TAGS): {
        //     const normalized = action.newTags.map((t) => t.normalizedKey);

        //     return {
        //         ...state,
        //         tags: {
        //             ...state.tags,
        //             list: state.tags.list.map((tag) => {
        //                 return {
        //                     ...tag,
        //                     isDeleting: normalized.includes(tag)
        //                 };
        //             })
        //         }
        //     };
        // }

        // case failSuffix(DELETE_MUSIC_TAGS): {
        //     const normalized = action.newTags.map((t) => t.normalizedKey);

        //     return {
        //         ...state,
        //         tags: {
        //             ...state.tags,
        //             list: state.tags.list.map((tag) => {
        //                 const newTag = {
        //                     ...tag
        //                 };

        //                 if (normalized.includes(tag)) {
        //                     newTag.isDeleting = false;
        //                 }

        //                 return newTag;
        //             })
        //         }
        //     };
        // }

        // case DELETE_MUSIC_TAGS: {
        //     const expectedNormalized = action.newTags.map((t) => t.normalizedKey);
        //     const resolvedNormalized = action.resolved.filter((tag) => {
        //         return tag.success;
        //     }).map((tag) => tag.normalizedKey);
        //     const newList = state.tags.list.filter((tag) => {
        //         return !resolvedNormalized.includes(tag);
        //     }).map((tag) => {
        //         const newTag = {
        //             ...tag
        //         };

        //         const isExpected = expectedNormalized.includes(tag.normalizedKey);
        //         const wasSuccess = resolvedNormalized.includes(tag.normalizedKey);

        //         if (isExpected && !wasSuccess) {
        //             newTag.isDeleting = false;
        //         }

        //         return newTag;
        //     });

        //     return {
        //         ...state,
        //         tags: {
        //             ...state.tags,
        //             list: newList
        //         }
        //     };
        // }

        default:
            return state;
    }
}

export function setMasqueradeValue(bool) {
    return {
        type: SET_MASQUERADE_VALUE,
        bool
    };
}

export function logoutOfUser() {
    return (dispatch) => {
        const token = Cookie.get('adminotoken');
        const secret = Cookie.get('adminosecret');
        const secure = process.env.NODE_ENV !== 'development';

        // Set original admin token values
        Cookie.set('otoken', token, {
            path: '/',
            secure,
            sameSite: 'lax'
        });
        Cookie.set('osecret', secret, {
            path: '/',
            secure,
            sameSite: 'lax'
        });

        // Remove our original admin tokens
        Cookie.remove('adminotoken', {
            path: '/',
            secure,
            sameSite: 'strict'
        });
        Cookie.remove('adminosecret', {
            path: '/',
            secure,
            sameSite: 'strict'
        });

        return dispatch({
            type: LOGOUT_OF_USER,
            token,
            secret
        });
    };
}

export function loginAsUser(id) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: LOGIN_AS_USER,
            promise: api.admin
                .loginAsUser(id, token, secret)
                .then((resolved) => {
                    const nowSeconds = Date.now() / 1000;
                    const nowExpireSeconds = resolved.oauth_token_expiration;
                    const hours = (nowExpireSeconds - nowSeconds) / 3600;
                    const expiresInDays = hours / 24;
                    const secure = process.env.NODE_ENV !== 'development';

                    // Set zombie user token
                    Cookie.set('otoken', resolved.oauth_token, {
                        path: '/',
                        expires: expiresInDays,
                        secure,
                        sameSite: 'lax'
                    });
                    Cookie.set('osecret', resolved.oauth_token_secret, {
                        path: '/',
                        expires: expiresInDays,
                        secure,
                        sameSite: 'lax'
                    });

                    // Save our original tokens
                    Cookie.set('adminotoken', resolved.admin_token, {
                        path: '/',
                        expires: expiresInDays,
                        secure,
                        sameSite: 'strict'
                    });
                    Cookie.set('adminosecret', resolved.admin_token_secret, {
                        path: '/',
                        expires: expiresInDays,
                        secure,
                        sameSite: 'strict'
                    });

                    return resolved;
                })
        });
    };
}

export function suspendMusic(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: SUSPEND_MUSIC,
            musicId: musicId,
            promise: api.admin.suspend(musicId, token, secret)
        });
    };
}

export function unsuspendMusic(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: UNSUSPEND_MUSIC,
            musicId: musicId,
            promise: api.admin.unsuspend(musicId, token, secret)
        });
    };
}

export function takedownMusic(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: TAKEDOWN_MUSIC,
            musicId: musicId,
            promise: api.admin.takedownMusic(musicId, token, secret)
        });
    };
}

export function restoreMusic(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: RESTORE_MUSIC,
            musicId: musicId,
            promise: api.admin.restoreMusic(musicId, token, secret)
        });
    };
}

export function bumpMusic(musicItem, bumpGenre) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: BUMP_MUSIC,
            item: musicItem,
            id: musicItem.id,
            genre: bumpGenre,
            promise: api.admin.bumpMusic(musicItem.id, bumpGenre, token, secret)
        });
    };
}

export function bumpMusicOneSpot(musicItem, bumpGenre) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: BUMP_MUSIC_ONE_SPOT,
            item: musicItem,
            id: musicItem.id,
            genre: bumpGenre,
            promise: api.admin.bumpMusicOneSpot(
                musicItem.id,
                bumpGenre,
                token,
                secret
            )
        });
    };
}

export function censorArtwork(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: CENSOR_ARTWORK,
            promise: api.admin.censorArtwork(musicId, token, secret)
        });
    };
}

export function trendMusic(musicId, genre, trend) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: TREND_MUSIC,
            promise: api.admin.trendMusic(musicId, genre, trend, token, secret)
        });
    };
}

export function excludeStats(musicId, operation) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: EXCLUDE_STATS,
            promise: api.admin.excludeStats(musicId, operation, token, secret)
        });
    };
}

export function clearStats(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: CLEAR_STATS,
            promise: api.admin.clearStats(musicId, token, secret)
        });
    };
}

export function verifyArtist(profile, verifiedType) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        let newValue = profile.verified === 'yes' ? 'no' : 'yes';

        if (verifiedType === 'tastemaker') {
            newValue = profile.verified === 'tastemaker' ? 'no' : 'tastemaker';
        }

        return dispatch({
            type: VERIFY_ARTIST,
            verified: verifiedType,
            profile: profile,
            promise: api.admin.verifyArtist(profile.id, newValue, token, secret)
        });
    };
}

export function verifyArtistEmail(profile) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: VERIFY_ARTIST_EMAIL,
            promise: api.admin.verifyArtistEmail(profile.id, token, secret)
        });
    };
}

export function getMusicSource(musicId, isStream = false) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_MUSIC_SOURCE,
            musicId: musicId,
            promise: api.admin.getMusicSourceUrl(
                musicId,
                isStream,
                token,
                secret
            )
        });
    };
}

export function getMusicRecognition(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_MUSIC_RECOGNITION,
            musicId: musicId,
            promise: api.admin.getMusicRecognition(musicId, token, secret)
        });
    };
}

export function refreshMusicRecognition(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: REFRESH_MUSIC_RECOGNITION,
            musicId: musicId,
            promise: api.admin.refreshMusicRecognition(musicId, token, secret)
        });
    };
}

export function repairMusicUrl(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: REPAIR_MUSIC_URL,
            musicId: musicId,
            promise: api.admin.repairMusicUrl(musicId, token, secret)
        });
    };
}

export function clearUploadErrors(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: CLEAR_UPLOAD_ERRORS,
            musicId: musicId,
            promise: api.admin.clearUploadErrors(musicId, token, secret)
        });
    };
}

export function getGlobalMusicTags() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_GLOBAL_MUSIC_TAGS,
            promise: api.music.getGlobalTags(token, secret)
        });
    };
}

export function getMusicTags(musicItem) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;

        return dispatch({
            type: GET_MUSIC_TAGS,
            musicItem: musicItem,
            promise: api.music.getTags(type, id, token, secret)
        });
    };
}

export function addMusicTags(musicItem, tags) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { list } = getState().admin.tags;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;
        const tagsArr = (Array.isArray(tags) ? tags : [tags]).filter(Boolean);

        return dispatch({
            type: ADD_MUSIC_TAGS,
            musicItem: musicItem,
            currentTags: list,
            newTags: tagsArr,
            promise: api.music.addTags(type, id, tagsArr, token, secret)
        });
    };
}

export function deleteMusicTags(musicItem, tags) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { list } = getState().admin.tags;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;
        const tagsArr = (Array.isArray(tags) ? tags : [tags]).filter(Boolean);

        return dispatch({
            type: DELETE_MUSIC_TAGS,
            musicItem: musicItem,
            currentTags: list,
            newTags: tagsArr,
            promise: api.music.deleteTags(type, id, tagsArr, token, secret)
        });
    };
}
