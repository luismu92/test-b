import api from 'api/index';

export const PREFIX = 'am/stats/';

export const TRACK_PLAY = `${PREFIX}TRACK_PLAY`;
export const SET_SECTION = `${PREFIX}SET_SECTION`;
const GET_TOKEN = `${PREFIX}GET_TOKEN`;
const SET_ENVIRONMENT = `${PREFIX}SET_ENVIRONMENT`;
const TRACK_ACTION = `${PREFIX}TRACK_ACTION`;
const SET_REFERER = `${PREFIX}SET_REFERER`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    sessionId: null,
    statsToken: null,
    environment: null,
    referer: null,
    section: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case GET_TOKEN:
            return {
                ...state,
                sessionId: action.sessionId,
                statsToken: action.resolved
            };

        case SET_ENVIRONMENT:
            return {
                ...state,
                environment: action.environment
            };

        case SET_SECTION:
            return {
                ...state,
                section: action.section
            };

        case SET_REFERER:
            return {
                ...state,
                referer: action.referer || defaultState.referer
            };

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function reset() {
    return {
        type: RESET
    };
}

export function trackPlay(id, options = {}) {
    return (dispatch, getState) => {
        const { section, environment, referer } = getState().stats;

        return dispatch({
            type: TRACK_PLAY,
            id,
            options,
            promise: api.music.play(id, {
                ...options,
                section,
                environment,
                referer
            })
        });
    };
}

export function getStatsToken(session) {
    return {
        type: GET_TOKEN,
        promise: api.music.getStatsToken(session),
        sessionId: session
    };
}

export function setEnvironment(environment) {
    return {
        type: SET_ENVIRONMENT,
        environment: environment
    };
}

export function setSection(section) {
    return {
        type: SET_SECTION,
        section
    };
}

let refererExpirationTimer;

export function setReferer(referer, expiration = null) {
    return (dispatch) => {
        if (expiration !== null) {
            clearTimeout(refererExpirationTimer);
            refererExpirationTimer = setTimeout(
                () =>
                    dispatch({
                        type: SET_REFERER,
                        referer: defaultState.referer
                    }),
                expiration
            );
        }

        return dispatch({
            type: SET_REFERER,
            referer
        });
    };
}

export function trackAction(token, type, musicId, referer = null) {
    return {
        type: TRACK_ACTION,
        promise: api.music.recordStats(token, type, musicId, referer)
    };
}
