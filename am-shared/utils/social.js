import { parse } from 'query-string';

import { openPopupWindow, getLocationFromPopup } from 'utils/index';

export function getTwitterOauthVerifier(twitterToken) {
    const popup = openPopupWindow(
        `https://api.twitter.com/oauth/authenticate?oauth_token=${twitterToken}&force_login=false`
    );

    return getLocationFromPopup(popup).then((location) => {
        // after user logs in, a redirect happens to a url with
        // the following query params
        console.log('location from twitter popup', location);
        const query = parse(location.search) || {};
        const oauthVerifier = query.oauth_verifier;

        return oauthVerifier;
    });
}

export function getInstagramAccessToken() {
    const popup = openPopupWindow(
        `https://api.instagram.com/oauth/authorize?client_id=${
            process.env.INSTAGRAM_APP_ID
        }&redirect_uri=${
            process.env.INSTAGRAM_REDIRECT_URI
        }&response_type=code&scope=user_profile`
    );

    return getLocationFromPopup(popup).then((location) => {
        return parse(location.search).code;
    });
}
