export default {
    set(key, value, stringify = false) {
        try {
            if (stringify) {
                localStorage.setItem(key, JSON.stringify(value));
            } else {
                localStorage.setItem(key, value);
            }
            return true;
        } catch (e) {
            return null;
        }
    },

    get(key, parse = false) {
        try {
            if (parse) {
                return JSON.parse(localStorage.getItem(key));
            }

            return localStorage.getItem(key);
        } catch (e) {
            return null;
        }
    },

    remove(key) {
        try {
            localStorage.removeItem(key);
            return true;
        } catch (e) {
            return null;
        }
    }
};

export const STORAGE_KEY_VOLUME = 'am-volume';
export const STORAGE_ACTIVE_EMBED_ID = 'am-activeembed';
export const STORAGE_MUSIC_HISTORY = 'am-history';
export const STORAGE_RECENT_PLAYLIST_ADD = 'am-pladd';
export const STORAGE_ACTIVE_PLAYER_ID = 'am-activeplayer';
export const STORAGE_AUTH_USER = 'am-authuser';
