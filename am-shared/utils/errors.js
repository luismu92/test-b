// http://stackoverflow.com/questions/31089801/extending-error-in-javascript-with-es6-syntax
class ExtendableError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        this.constructor.message = message;
        this.message = message;

        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = new Error(message).stack;
        }
    }
}

// Oembed
export class OembedException extends ExtendableError {}
export class OembedNotImplementedException extends ExtendableError {}
export class OembedNotFoundException extends ExtendableError {}

// Auth
export class AuthenticationFailedException extends ExtendableError {}

// Upload
export class UploadFailedException extends ExtendableError {}

// RSS
export class RssException extends ExtendableError {}

// Email Worker
export class EmailShouldNackException extends ExtendableError {}
