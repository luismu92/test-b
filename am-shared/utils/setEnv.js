/* eslint no-sync: 0 */
const fs = require('fs');
const path = require('path');
const glob = require('glob');
const dotenv = require('dotenv');

function applyEnvVars(envConfig) {
    for (const k in envConfig) {
        if (envConfig.hasOwnProperty(k)) {
            process.env[k] = envConfig[k];
        }
    }
}

function getParsedConfig(file) {
    return dotenv.parse(fs.readFileSync(file));
}

const currentEnv = process.env.NODE_ENV || 'development';

// Apply prod env first
const devConfig = getParsedConfig(
    path.join(__dirname, '../../.env.production')
);

applyEnvVars(devConfig);

// Apply other env (ie development) on top of prod variables
if (['development', 'test'].includes(currentEnv)) {
    const otherConfig = getParsedConfig(
        path.join(__dirname, `../../.env.${currentEnv}`)
    );

    applyEnvVars(otherConfig);
}

// Allow for custom .env.* files in config/env
const files = glob.sync(path.join(__dirname, '../../config/env/.env.*'));

files.forEach((file) => {
    const envConfig = getParsedConfig(file);

    applyEnvVars(envConfig);
});
