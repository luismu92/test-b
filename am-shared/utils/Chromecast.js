import { chromecastMessageTypes } from 'constants/index';
import { getConfig } from 'api/index';

const optionCallbacks = [
    'onAvailablilityChange',
    'onMediaInfoChange',
    'onSessionStart',
    'onSessionStartFailed',
    'onSessionResumed',
    'onSessionEnded',
    'onTimeUpdate',
    'onEnded',
    'onPlaybackStateChange',
    'onVolumeChange'
];

function noop() {}

export default class Chromecast {
    constructor(options = {}) {
        this._options = options;

        optionCallbacks.forEach((cb) => {
            this._options[cb] = options[cb] || noop;
        });

        window.__onGCastApiAvailable = (isAvailable) => {
            if (isAvailable) {
                this._setupInstanceVars();
                this._initializeCastApi();
            }
        };

        this._duration = 0;
    }

    ////////////////////
    // Public methods //
    ////////////////////

    get currentTime() {
        if (!this._player) {
            return 0;
        }

        return this._player.currentTime || 0;
    }

    get paused() {
        if (!this._player) {
            return true;
        }

        return this._player.isPaused;
    }

    get duration() {
        if (!this._player) {
            return true;
        }

        return this._player.duration || this._duration;
    }

    get deviceName() {
        if (!this.isActive()) {
            return null;
        }

        return this.castSession.getCastDevice().friendlyName;
    }

    get castSession() {
        return window.cast.framework.CastContext.getInstance().getCurrentSession();
    }

    // Seems like the player.duration is always 0 so lets make a default
    set duration(value) {
        this._duration = value;
    }

    get currentMediaInfo() {
        return this._currentMediaInfo || null;
    }

    set currentMediaInfo(value) {
        this._currentMediaInfo = value;
    }

    setMedia(
        playerReducer,
        { contentType = 'audio/mp3', token, secret, key } = {}
    ) {
        const currentSong = playerReducer.currentSong;
        const mediaSourceUrl = currentSong.streaming_url;
        const trackIndex = currentSong.trackIndex;

        console.warn('chromecast setMedia', currentSong);
        const {
            MediaInfo,
            StreamType,
            MusicTrackMediaMetadata,
            MetadataType,
            LoadRequest
        } = window.chrome.cast.media;
        const mediaInfo = new MediaInfo(mediaSourceUrl, contentType);

        mediaInfo.streamType = StreamType.BUFFERED;

        const metadata = new MusicTrackMediaMetadata();

        metadata.title = currentSong.title;
        metadata.artist = currentSong.artist;
        metadata.images = [new window.chrome.cast.Image(currentSong.image)];
        metadata.metadataType = MetadataType.MUSIC_TRACK;
        metadata.releaseDate = new Date(
            currentSong.released * 1000
        ).toISOString();
        metadata.songName = currentSong.title;
        metadata.albumName = currentSong.album;

        const { baseUrl, consumerKey, consumerSecret } = getConfig();
        const customData = {
            id: currentSong.id,
            type: currentSong.type,
            apiKey: consumerKey,
            apiSecret: consumerSecret,
            apiUrl: baseUrl,
            // This isnt applicable on the web yet
            // hq,
            key,
            token,
            secret
        };

        if (currentSong.parentDetails) {
            const parentType = currentSong.parentDetails.type;
            const parentId = currentSong.parentDetails.id;

            metadata.trackNumber = trackIndex + 1;

            customData.id = parentId;
            customData.type = parentType;

            // Gonna assume this is okay for playlists too
            metadata.albumName = currentSong.parentDetails.title;
        }

        mediaInfo.metadata = metadata;
        mediaInfo.customData = customData;

        this._player.canSeek = true;
        this._player.canPause = true;
        this._player.canControlVolume = true;

        const request = new LoadRequest(mediaInfo);

        request.autoplay = !playerReducer.paused;
        request.currentTime = playerReducer.currentTime;

        return this.castSession.loadMedia(request);
    }

    sendMessage(type, data = {}) {
        if (!this.castSession) {
            return Promise.reject('Cast session not initialized');
        }

        return this.castSession.sendMessage(process.env.CHROMECAST_URN, {
            ...data,
            type
        });
    }

    play() {
        if (!this._player) {
            return;
        }

        if (!this._player.isPaused) {
            return;
        }

        console.warn('chromecast play');
        this._playerController.playOrPause();
    }

    pause() {
        if (!this._player) {
            return;
        }

        if (this._player.isPaused) {
            return;
        }

        console.warn('chromecast pause');
        this._playerController.playOrPause();
    }

    stop(endSession = false) {
        console.warn('chromecast stop');

        if (this._playerController) {
            this._playerController.stop();
        }

        if (this.castSession && endSession) {
            this.castSession.endSession();
        }
    }

    seek(time) {
        if (!this._player) {
            return;
        }

        console.warn('chromecast seek', time);
        this._player.currentTime = time;
        this._playerController.seek(time);
    }

    volume(volume) {
        if (!this._player) {
            return;
        }

        console.warn('chromecast volume', volume);
        this._player.volumeLevel = volume;
        this._playerController.setVolumeLevel(volume);
    }

    mute() {
        if (!this._player || this._player.isMuted) {
            return;
        }

        console.warn('chromecast mute');
        this._playerController.muteOrUnmute();
    }

    unmute() {
        if (!this._player || !this._player.isMuted) {
            return;
        }

        console.warn('chromecast unmute');
        this._playerController.muteOrUnmute();
    }

    isActive() {
        const session = !!window.cast && !!this.castSession;

        if (!session) {
            return false;
        }

        const currentSessionState = this.castSession.getSessionState();
        const {
            SESSION_STARTED,
            SESSION_RESUMED
        } = window.cast.framework.SessionState;
        const activeStates = [SESSION_STARTED, SESSION_RESUMED];

        return activeStates.includes(currentSessionState);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    _handleCastStateChange = ({ castState }) => {
        const {
            CONNECTED,
            CONNECTING,
            NOT_CONNECTED,
            NO_DEVICES_AVAILABLE
        } = window.cast.framework.CastState;

        console.warn('chromecast cast state', castState);
        switch (castState) {
            case CONNECTED:
            case CONNECTING:
            case NOT_CONNECTED:
                this._options.onAvailablilityChange(true);
                break;

            case NO_DEVICES_AVAILABLE:
                this._options.onAvailablilityChange(false);
                break;

            default:
                break;
        }
    };

    _handleSessionStateChange = ({ sessionState }) => {
        const {
            SESSION_STARTED,
            SESSION_START_FAILED,
            SESSION_RESUMED,
            SESSION_ENDED
        } = window.cast.framework.SessionState;

        // console.log('_handleSessionStateChange');
        console.warn('chromecast session state', sessionState);

        switch (sessionState) {
            case SESSION_STARTED: {
                this.castSession.addMessageListener(
                    process.env.CHROMECAST_URN,
                    this._handleCustomMessage
                );
                this._options.onSessionStart();
                break;
            }

            case SESSION_RESUMED: {
                this._options.onSessionResumed();
                break;
            }

            case SESSION_START_FAILED: {
                this._options.onSessionStartFailed();
                break;
            }

            case SESSION_ENDED: {
                const stopCasting = false;

                this.castSession.endSession(stopCasting);
                this._options.onSessionEnded();

                break;
            }

            default:
                break;
        }
    };

    _handleAnyChange = (e) => {
        // console.warn('chromecast anychange', e);

        switch (e.field) {
            case 'currentTime': {
                if (!this._player.isPaused) {
                    this._options.onTimeUpdate(e.value);
                }
                break;
            }

            case 'duration': {
                this.duration = e.value;
                break;
            }

            case 'isPaused': {
                this._options.onPlaybackStateChange(e.value);
                break;
            }

            case 'mediaInfo': {
                const newValue = e.value || null;
                let oldValue = null;

                if (this.currentMediaInfo) {
                    oldValue = { ...this.currentMediaInfo };
                }

                if (newValue) {
                    this.currentMediaInfo = newValue;
                    this._options.onMediaInfoChange(newValue, oldValue);
                }
                // const { type, id, trackIndex } = e.value.customData;
                // const { type: lastType, id: lastId, trackIndex: lastTrackIndex } = this.lastMediaInfo;
                // const isCurrentTab = type === lastType && id === lastId && trackIndex === lastTrackIndex;

                // if (!isCurrentTab) {
                //     const isPaused = true;

                //     this._options.onSessionEnded();
                //     this._options.onPlaybackStateChange(isPaused);
                // }
                break;
            }

            case 'volume': {
                this._options.onVolumeChange(e.value);
                break;
            }

            default:
                break;
        }
    };

    _handleCustomMessage = (namespace, str) => {
        const message = JSON.parse(str);

        console.warn('chromecast custom message', namespace, message);

        switch (message.type) {
            case chromecastMessageTypes.ended: {
                this._options.onEnded();
                break;
            }

            default:
                break;
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    _setupInstanceVars() {
        const { ANY_CHANGE } = window.cast.framework.RemotePlayerEventType;

        this._player = new window.cast.framework.RemotePlayer();
        this._playerController = new window.cast.framework.RemotePlayerController(
            this._player
        );
        this._playerController.addEventListener(
            ANY_CHANGE,
            this._handleAnyChange
        );

        if (process.env.NODE_ENV === 'development') {
            window.player = this._player;
            window.playerController = this._playerController;
        }
    }

    _initializeCastApi() {
        const instance = window.cast.framework.CastContext.getInstance();

        instance.setOptions({
            autoJoinPolicy: window.chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED,
            receiverApplicationId: process.env.CHROMECAST_RECEIVER_ID
        });

        const {
            CAST_STATE_CHANGED,
            SESSION_STATE_CHANGED
        } = window.cast.framework.CastContextEventType;

        instance.addEventListener(
            CAST_STATE_CHANGED,
            this._handleCastStateChange
        );
        instance.addEventListener(
            SESSION_STATE_CHANGED,
            this._handleSessionStateChange
        );

        this._handleCastStateChange({
            castState: instance.getCastState()
        });
    }
}
