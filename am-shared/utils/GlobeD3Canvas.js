import * as d3 from 'd3';
import classnames from 'classnames';
import { feature } from 'topojson-client';
import { debounce, round, passiveOption } from 'utils/index';

const tooltipHeight = 50;
const tooltipTopMargin = 20;
const tooltipLeftMargin = 25;
const maxGrays = 10;
const grayscale = d3
    .scaleLinear()
    .domain([1, maxGrays])
    .range(['#7d7d7d', '#5a5a5a']);
const graticuleColor = '#373737';
const waterColor = 'rgba(20, 20, 20, 0.5)';
const projectionScale = d3
    .scaleLinear()
    // Width
    .domain([1, 562])

    // to projection scale
    .range([1, 280]);

export default class GlobeD3Canvas {
    constructor(
        container,
        worldData,
        {
            onLoad = () => {},
            onResize = () => {},
            onPointClick = () => {},
            onError = () => {},
            sidePadding = 0,
            topPadding = 0,
            classes = {
                cityText: 'globe-city',
                countryText: 'globe-country',
                point: 'globe-point',
                pointHover: 'globe-point--hover',
                pointInner: 'globe-point__inner',
                pointAnimate: 'globe-point__animate',
                pointInactive: 'globe-point--inactive',
                tooltip: 'globe-tooltip',
                tooltipActive: 'globe-tooltip--active'
            }
        } = {}
    ) {
        if (!container || !worldData) {
            onError(new Error('Container and world data required'));
            return;
        }

        this._container = d3.select(container);
        this._canvas = this._container.append('canvas');
        this._context = this._canvas.node().getContext('2d');
        this._classes = classes;
        this._projection = d3
            .geoOrthographic()
            .clipAngle(90)
            .precision(0.1)
            .rotate([0, 0, 0]);
        this._geoPath = d3
            .geoPath()
            .projection(this._projection)
            .context(this._context);
        // .pointRadius(1);
        this._pointPath = d3.geoPath().projection(this._projection);

        this._graticule = d3.geoGraticule();

        this._tooltip = this._container
            .append('div')
            .attr('class', this._classes.tooltip)
            .style('height', tooltipHeight);
        this._scaleLongitude = d3.scaleLinear().range([-180, 180]);
        this._zoom = 1;
        this._dragEnabled = false;
        this._disabledTooltips = true;
        this._scaleLatitude = d3.scaleLinear().range([90, -90]);
        this._cities = [];
        this._onLoad = onLoad;
        this._onResize = onResize;
        this._onPointClick = onPointClick;
        this._sidePadding = sidePadding;
        this._topPadding = topPadding;
        this._dpi = window.devicePixelRatio || 1;

        this._water = { type: 'Sphere' };

        this._tooltipContainer = this._container;
        this._land = feature(worldData, worldData.objects.countries);

        this.resize(() => {
            this.render();
            this.attachEvents();
            this.start();

            onLoad();
        });
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowResize = debounce(() => {
        this.resize(() => this.zoom(this._zoom, { animate: false }));
    }, 250);

    handleRafLoop = () => {
        this._loop = window.requestAnimationFrame(this.handleRafLoop);

        if (this._tooltipActive || this._isDragging) {
            return;
        }

        const rotation = this._projection.rotate();
        const increment = 1 / 10;

        this.rotate(rotation[0] - increment, rotation[1]);
        this.render();
    };

    handleContainerClick = (e) => {
        let button = e.target;
        let isButton = button.tagName.toLowerCase() === 'button';

        if (
            button.tagName.toLowerCase() === 'span' &&
            button.parentElement.tagName.toLowerCase() === 'button'
        ) {
            button = button.parentElement;
            isButton = true;
        }

        if (
            !isButton ||
            (isButton && !button.classList.contains(this._classes.point))
        ) {
            return;
        }

        const name = button.getAttribute('data-name');
        const id = parseInt(button.id, 10);

        this._onPointClick(name, id);
    };

    handleContainerMouseEnter = (e) => {
        const button = e.target;
        const isButton = button.tagName.toLowerCase() === 'button';

        if (
            !isButton ||
            (isButton && !button.classList.contains(this._classes.point))
        ) {
            return;
        }

        const rect = button.getBoundingClientRect();
        const left = rect.x - this._containerRect.x - tooltipLeftMargin;
        const top =
            rect.y - this._containerRect.y - tooltipTopMargin - tooltipHeight;
        const locationName = button.getAttribute('data-name');
        const [city, country] = locationName.split(',').map((s) => s.trim());
        let nameHtml = `<span class="${this._classes.cityText}">${city}${
            country ? ', ' : ''
        }</span>`;

        if (country) {
            nameHtml += `<span class="${
                this._classes.countryText
            }">${country}</span>`;
        }

        button.classList.add(this._classes.pointHover);

        clearTimeout(this._tooltipTimer);
        this._tooltipActive = true;
        this._tooltip
            .html(nameHtml)
            .style('top', `${top}px`)
            .style('left', `${left}px`)
            .attr(
                'class',
                `${this._classes.tooltip} ${this._classes.tooltipActive}`
            );
    };

    handleContainerMouseLeave = (e) => {
        const button = e.target;
        const isButton = button.tagName.toLowerCase() === 'button';

        if (
            !isButton ||
            (isButton && !button.classList.contains(this._classes.point))
        ) {
            return;
        }

        button.classList.remove(this._classes.pointHover);
        this._tooltip.attr('class', this._classes.tooltip);
        clearTimeout(this._tooltipTimer);

        this._tooltipTimer = setTimeout(() => {
            this._tooltipActive = false;
        }, 150);
    };

    /////////////////
    // Helper methods //
    ////////////////////

    getPointStyle = (d) => {
        if (!this._pointPath(d)) {
            return 'opacity: 0';
        }

        const [x, y] = this._projection([d.coordinates[0], d.coordinates[1]]);

        return `transform: translate(${round(x / this._dpi)}px, ${round(
            y / this._dpi
        )}px)`;
    };

    getPointTabIndex = (d) => {
        if (!this._pointPath(d)) {
            return '-1';
        }

        return null;
    };

    getPointClass = (d) => {
        return classnames(this._classes.point, {
            [this._classes.pointInactive]: !this._pointPath(d)
        });
    };

    attachEvents() {
        const passive = passiveOption();
        const container = this._container.node();

        window.addEventListener('resize', this.handleWindowResize, passive);

        container.addEventListener('click', this.handleContainerClick, true);
        container.addEventListener(
            'mouseenter',
            this.handleContainerMouseEnter,
            true
        );
        container.addEventListener(
            'mouseleave',
            this.handleContainerMouseLeave,
            true
        );
    }

    detachEvents() {
        const container = this._container.node();

        window.removeEventListener('resize', this.handleWindowResize);
        container.removeEventListener('click', this.handleContainerClick);
        container.removeEventListener(
            'mouseenter',
            this.handleContainerMouseEnter
        );
        container.removeEventListener(
            'mouseleave',
            this.handleContainerMouseLeave
        );
    }

    render() {
        this._context.clearRect(0, 0, this._width, this._height);

        this._context.save();

        this._context.beginPath();
        this._geoPath(this._water);
        this._context.fillStyle = waterColor;
        this._context.fill();

        this._land.features.forEach((d, i) => {
            const color = grayscale(i % maxGrays);

            // neighbors = topojson.neighbors(world.objects.countries.geometries)
            // this._context.fillStyle = color(d.color = d3.max(neighbors[i], function(n) { return countries[n].color; }) + 1 | 0);
            this._context.fillStyle = color;
            this._context.beginPath();
            this._geoPath(d);
            this._context.fill();
        });

        this._context.beginPath();
        this._geoPath(this._graticule());
        this._context.lineWidth = 0.5;
        this._context.strokeStyle = graticuleColor;
        this._context.stroke();

        this._context.restore();

        const cityData = this._disabledTooltips ? [] : this._cities;
        const tooltips = this._tooltipContainer
            .selectAll(`.${this._classes.point}`)
            .data(cityData);

        tooltips.exit().remove();

        const outerPoint = tooltips
            .enter()
            .append('button')
            .attr('class', this.getPointClass)
            .attr('style', this.getPointStyle)
            .attr('tabIndex', this.getPointTabIndex)
            .attr('id', (d) => d.id)
            .attr('data-name', (d) => d.name);

        outerPoint.append('span').attr('class', this._classes.pointInner);

        outerPoint.append('span').attr('class', this._classes.pointAnimate);

        this._container
            .selectAll(`.${this._classes.point}`)
            .attr('class', this.getPointClass)
            .attr('tabIndex', this.getPointTabIndex)
            .attr('style', this.getPointStyle);
    }

    resize(cb = () => {}) {
        this._canvas.attr('style', 'width: 0px; height: 0px');
        this._canvas.attr('width', 0).attr('height', 0);

        window.requestAnimationFrame(() => {
            const { clientWidth, clientHeight } = this._container.node();
            const size = Math.min(clientHeight, clientWidth);

            this._canvas.attr(
                'style',
                `width: ${clientWidth}px; height: ${clientHeight}px`
            );
            this._canvas
                .attr('width', clientWidth * this._dpi)
                .attr('height', clientHeight * this._dpi);

            this._containerRect = this._container
                .node()
                .getBoundingClientRect();
            this._size = size * this._dpi;
            this._width = clientWidth * this._dpi;
            this._height = clientHeight * this._dpi;
            this._scale(this._size);
            this._scaleLongitude.domain([0, this._size]);
            this._scaleLatitude.domain([0, this._size]);

            this._onResize(clientWidth, clientHeight);
            cb();
        });
    }

    destroy() {
        this.detachEvents();
        this.disableControls();
        window.cancelAnimationFrame(this._loop);
        this._lastCenteredPoint = null;
        this._container.innerHTML = '';
    }

    start() {
        this.stop();
        this._loop = window.requestAnimationFrame(this.handleRafLoop);
    }

    stop() {
        window.cancelAnimationFrame(this._loop);
        this._loop = null;
    }

    playing() {
        return !this.paused();
    }

    paused() {
        return this._loop === null;
    }

    rotate(x, y, z) {
        const currentValues = this._projection.rotate();

        this._projection.rotate([
            x || currentValues[0],
            y || currentValues[1],
            z || currentValues[2]
        ]);
    }

    enableControls() {
        // Drag and zoom
        // https://stackoverflow.com/questions/43772975/drag-rotate-projection-in-d3-v4
        this._dragEnabled = true;

        if (this._drag) {
            return;
        }

        let wasInAutoMode;

        this._drag = d3
            .drag()
            .subject(() => {
                const r = this._projection.rotate();

                return {
                    x: this._scaleLongitude.invert(r[0]),
                    y: this._scaleLatitude.invert(r[1])
                };
            })
            .on('start', () => {
                if (!this._dragEnabled) {
                    return;
                }

                wasInAutoMode = this._loop !== null;
                this.stop();
                this._isDragging = true;
                this._container.style('cursor', 'move');
            })
            .on('end', () => {
                if (!this._dragEnabled) {
                    return;
                }

                if (wasInAutoMode) {
                    this.start();
                }

                this._isDragging = false;
                this._container.style('cursor', null);
            })
            .on('drag', () => {
                if (!this._dragEnabled) {
                    return;
                }
                this.rotate(
                    this._scaleLongitude(d3.event.x),
                    this._scaleLatitude(d3.event.y)
                );
                this.render();
            });

        this._container.call(this._drag);
    }

    disableControls() {
        this._dragEnabled = false;
    }

    disableTooltips() {
        this._disabledTooltips = true;
        this.render();
    }

    enableTooltips() {
        this._disabledTooltips = false;
        this.render();
    }

    zoom(scale, options = {}) {
        this._zoom = scale;
        const { animate = true, rerender = true } = options;

        const increment = 110;
        const size = this._size;

        this._scale(size, {
            extra: Math.max(0, increment * (scale - 1)),
            animate,
            rerender
        });
    }

    _scale(size, options = {}) {
        const self = this;
        const { extra = 0, animate = false, rerender = true } = options;
        let sidePadding = this._sidePadding;
        let topPadding = this._topPadding; // eslint-disable-line

        if (typeof this._sidePadding === 'function') {
            sidePadding = this._sidePadding(size);
        }

        if (typeof this._topPadding === 'function') {
            topPadding = this._topPadding(size);
        }

        sidePadding = sidePadding * this._dpi;
        topPadding = topPadding * this._dpi;

        const width = size - sidePadding + extra * this._dpi;
        const newScale = projectionScale(width);
        const newTranslate = [this._width / 2, this._height / 2];

        if (animate) {
            d3.transition()
                .duration(300)
                .tween('scale', function() {
                    const s = d3.interpolate(
                        self._projection.scale(),
                        newScale
                    );
                    const t = d3.interpolate(
                        self._projection.translate(),
                        newTranslate
                    );

                    return function(i) {
                        self._projection.scale(s(i));
                        self._projection.translate(t(i));

                        if (rerender) {
                            self.render();
                        }
                    };
                });
            return;
        }

        this._projection.scale(newScale).translate(newTranslate);

        if (rerender) {
            self.render();
        }
    }

    setTooltipData(data) {
        this._cities = [];
        // Render empty because the solo render isnt working for some reason
        this.render();
        this._cities = data.map((city, i) => {
            return {
                type: 'Point',
                coordinates: [city.longitude, city.latitude],
                name: city.name,
                id: i
            };
        });

        if (this._disabledTooltips) {
            return;
        }

        this.render();
    }

    _dispatchEvent(node, eventName) {
        if (!node) {
            return;
        }

        const event = document.createEvent('MouseEvent');

        event.initEvent(eventName, true, true);
        node.dispatchEvent(event);
    }

    centerByCity(city, options = {}) {
        const self = this;
        const { zoomScale, rotate } = options;
        const pointNodes = this._tooltipContainer
            .selectAll(`.${this._classes.point}`)
            .nodes();
        const point = pointNodes.filter((p) => {
            return city && p.getAttribute('data-name') === city.name;
        })[0];

        d3.transition()
            .duration(1000)
            .on('start', function() {
                let currentName;
                let lastName;

                if (point) {
                    currentName = point.getAttribute('data-name');
                }

                if (self._lastCenteredPoint) {
                    lastName = self._lastCenteredPoint.getAttribute(
                        'data-name'
                    );
                }

                if (lastName !== currentName) {
                    self._dispatchEvent(self._lastCenteredPoint, 'mouseout');
                }

                self._lastCenteredPoint = point;
            })
            .tween('rotate', function() {
                let rotation;
                let zoom;

                if (zoomScale) {
                    zoom = d3.interpolate(self._zoom, zoomScale);
                }

                if (city) {
                    const [x, y] = d3.geoCentroid({
                        type: 'Point',
                        coordinates: [-city.longitude, -city.latitude]
                    });

                    rotation = d3.interpolate(self._projection.rotate(), [
                        x,
                        y
                    ]);
                }

                if (rotate) {
                    const currentRotation = self._projection.rotate();
                    const x =
                        typeof rotate[0] !== 'number'
                            ? currentRotation[0]
                            : rotate[0];
                    const y =
                        typeof rotate[1] !== 'number'
                            ? currentRotation[1]
                            : rotate[1];
                    const z =
                        typeof rotate[2] !== 'number'
                            ? currentRotation[2]
                            : rotate[2];

                    rotation = d3.interpolate(self._projection.rotate(), [
                        x,
                        y,
                        z
                    ]);
                }

                return function(t) {
                    if (rotation) {
                        self._projection.rotate(rotation(t));
                    }

                    if (zoom) {
                        self.zoom(zoom(t), {
                            animate: false,
                            rerender: false
                        });
                    }

                    self.render();
                };
            })
            .on('end', function() {
                self._dispatchEvent(point, 'mouseover');
            });
    }
}
