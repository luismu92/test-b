export function getArtistUrl(artist, { host = '' } = {}) {
    return `${host}/artist/${artist.url_slug}`;
}
