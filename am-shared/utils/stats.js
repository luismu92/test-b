import { countries } from 'constants/index';

import moment from 'moment';

export function queryToday(format = 'YYYY-MM-DD') {
    return moment().format(format);
}

export function queryWeek(format = 'YYYY-MM-DD') {
    return moment()
        .subtract(7, 'days')
        .format(format);
}

export function queryMonth(format = 'YYYY-MM-DD') {
    return moment()
        .subtract(1, 'month')
        .format(format);
}

export function queryThreeMonths(format = 'YYYY-MM-DD') {
    return moment()
        .subtract(3, 'months')
        .format(format);
}

export function querySixMonths(format = 'YYYY-MM-DD') {
    return moment()
        .subtract(6, 'months')
        .format(format);
}

export function queryYear(format = 'YYYY-MM-DD') {
    return moment()
        .subtract(12, 'months')
        .format(format);
}

export function getCountryName(isoCode) {
    return countries[isoCode] || isoCode;
}

export function queryForTimespan(timespan) {
    switch (timespan) {
        case 'month':
            return queryMonth();

        case 'three-months':
            return queryThreeMonths();

        case 'six-months':
            return querySixMonths();

        case 'year':
            return queryYear();

        case 'week':
        default:
            return queryWeek();
    }
}

// Map here:
// https://docs.google.com/spreadsheets/d/1A0kaG7uebZX5hPI0-d6z4lAzov1DfEJ5N0GbwK52k2o/edit#gid=0
//
// If we ever have multiple clients displaying this data, this logic
// should probably be moved into the API under another key like "aggregated: Feed"

// prettier-ignore
const aggregatedMap = {
    'Album Details': 'Album',

    'Artist': 'Your Profile',

    'Browse - Recently Added': 'Recently Added',
    'Browse - Top Albums': 'Top Albums',
    'Browse - Top Songs': 'Top Songs',
    'Browse - Trending': 'Trending',

    'Feed - Timeline': 'Feed',
    'Feed - Suggested Follows': 'Feed',
    'Feed - Following': 'Feed',

    'Highlighted Items': 'Highligted Music',

    'My Library - Favorites': 'Listener Library',
    'My Library - Offline': 'Listener Library',
    'My Library - Playlists': 'Listener Library',
    'My Library - Uploads': 'Listener Library',
    'My Library Search - Offline': 'Listener Library',
    'My Library Search - Playlists': 'Listener Library',
    'My Library Search - Uploads': 'Listener Library',
    'My Library Search - Favorites': 'Listener Library',

    'Playlist': 'Playlists',
    'New Playlists': 'Playlists',
    'Playlist Details': 'Playlists',
    'Playlists - #OnTheRadar': 'Playlists',
    'Playlists - Afrobeats': 'Playlists',
    'Playlists - Best Of: Artist': 'Playlists',
    'Playlists - Best Of: Month': 'Playlists',
    'Playlists - Curator Spotlight': 'Playlists',
    'Playlists - Electronic / EDM / House': 'Playlists',
    'Playlists - Electronic Era': 'Playlists',
    'Playlists - Guest Verses': 'Playlists',
    'Playlists - Hip-Hop / R&B': 'Playlists',
    'Playlusts - Mexican': 'Playlists',
    'Playlists - Moods': 'Playlists',
    'Playlists - Reggae / Dancehall / Soca': 'Playlists',
    'Playlists - Top 20': 'Playlists',
    'Playlists - Verified Series': 'Playlists',
    "Playlists - What's New": 'Playlists',

    'Profile - Favorites': 'User Profiles',
    'Profile - Playlists': 'User Profiles',
    'Profile - Uploads': 'User Profiles',

    'Search - Albums': 'Search',
    'Search - All Music': 'Search',
    'Search - Playlists': 'Search',
    'Search - Songs': 'Search',

    // Temporarily add these until we have more in-depth tracking from different sections
    'Home Page - Logged In': 'Home Page',
    'Home Page - Logged Out': 'Home Page',

    'About': 'Other',
    'Account Search': 'Other',
    'Amp Program': 'Other',
    'Artist Dashboard': 'Other',
    'Audiomack World': 'Other',
    'Contact Us': 'Other',
    'Edit Music': 'Other',
    'Edit Profile': 'Other',
    'Forgot Password': 'Other',
    'Legal': 'Other',
    'Login': 'Other',
    'Music Stats': 'Other',
    'Notifications': 'Other',
    'Partner Agreement': 'Other',
    'Play Album': 'Other',
    'Play Song': 'Other',
    'Popular Artists': 'Other',
    'Privacy Policy': 'Other',
    'Restore Downloads': 'Other',
    'Sign Up': 'Other',
    'Terms of Service': 'Other',
    'Upload': 'Other',
    'Wordpress': 'Other'
};

export function aggregatePlaySources(sources = [], stat = 'play_song') {
    const newMap = sources.reduce((acc, source) => {
        const aggregatedKey = aggregatedMap[source.section] || source.section;

        acc[aggregatedKey] = acc[aggregatedKey] || 0;
        acc[aggregatedKey] += source[stat];

        return acc;
    }, {});

    return Object.keys(newMap)
        .reduce((acc, key) => {
            acc.push({ section: key, [stat]: newMap[key] });
            return acc;
        }, [])
        .sort((x, y) => {
            return newMap[y] - newMap[x];
        })
        .filter((obj) => {
            return obj[stat] > 0;
        });
}
