import {
    waitForBranchMetadata,
    cleanParameters,
    isEmbedUrl
} from 'utils/index';

export default {
    release(releaseName, id) {
        if (!process.env.BROWSER) {
            return;
        }

        if (window.newrelic && releaseName && id) {
            window.newrelic.addRelease(releaseName, id);
        }
    },

    /**
     * Track certain events around the app and send it to wherever we
     * are tracking events
     *
     * @param  {String} event   Event category
     * @param  {Object} options data to send to the tracker functions
     * @param {Array} trackers Array of tracker keys that should be used when sending the data payload
     */

    track(event, options = {}, trackers = ['ga']) {
        if (!process.env.BROWSER) {
            return;
        }

        if (!event) {
            throw new Error('Must provide an event category');
        }

        let tracked = 0;

        if (typeof window.gtag === 'function' && trackers.includes('ga')) {
            tracked += 1;
            const { eventAction, eventLabel, eventValue, ...rest } = options;

            // Need to call firebase.analytics before gtag()
            // https://firebase.google.com/docs/analytics/get-started?platform=web#firebase-gtag
            if (typeof window.firebase !== 'undefined') {
                window.firebase.analytics();
            }

            window.gtag('event', options.event_action || eventAction, {
                ...rest,
                event_category: event,
                event_label: options.event_label || eventLabel,
                value: options.value || eventValue
            });
        }

        if (typeof window.fbq === 'function' && trackers.includes('fb')) {
            if (!options.eventAction) {
                throw new Error(
                    'Must provide an event action if using fb track'
                );
            }
            tracked += 1;

            window.fbq('track', options.eventAction);
        }

        if (typeof window.branch === 'object' && trackers.includes('branch')) {
            if (!options.eventAction) {
                throw new Error(
                    'Must provide an event action if using branch track'
                );
            }
            tracked += 1;

            const { eventAction, ...rest } = options;

            window.branch.logEvent(`${event} - ${eventAction}`, rest);
            window.branch.track(`${event} - ${eventAction}`, rest);
        }

        if (
            typeof window.mixpanel === 'object' &&
            trackers.includes('mixpanel')
        ) {
            tracked += 1;
            window.mixpanel.track(event, options);
        }

        if (tracked !== trackers.length) {
            console.error(
                `${trackers.length -
                    tracked} tracker(s) from ${trackers} were not successful. Check why.`
            );
        }
    },

    trackBranchData(data = {}, flags = {}) {
        if (!process.env.BROWSER || !window.branch) {
            return Promise.reject('No browser/branch found.').catch((err) => {
                console.error('You can ignore this on desktop');
                console.error(err);
            });
        }

        console.info('Tracking branch data', data);

        return new Promise((resolve, reject) => {
            window.branch.closeJourney(() => {
                const dynamicData = cleanParameters(data);

                window.branch.setBranchViewData({
                    data: dynamicData
                });

                window.branch.track(
                    'pageview',
                    dynamicData,
                    {
                        disable_entry_animation: true,
                        disable_exit_animation: true,
                        ...flags
                    },
                    (err) => {
                        if (err) {
                            console.log(err);
                            reject(err);
                            return;
                        }

                        resolve();
                    }
                );
            });
        }).catch((err) => {
            console.log('You can ignore this on desktop');
            console.log(err);
        });
    },

    identify(userId) {
        if (typeof window.mixpanel === 'object') {
            if (!userId) {
                window.mixpanel.reset();
            } else {
                window.mixpanel.identify(userId);
            }
        }
    },

    pageView(path) {
        if (!process.env.BROWSER) {
            return;
        }

        if (typeof window.gtag === 'function') {
            let ids = [
                process.env.GA_PROPERTY_ID,
                process.env.GA_MEASUREMENT_ID
            ];

            if (isEmbedUrl(window.location.pathname)) {
                ids = [process.env.GA_PROPERTY_ID_EMBED];
            }

            // Need to call firebase.analytics before gtag()
            // https://firebase.google.com/docs/analytics/get-started?platform=web#firebase-gtag
            if (typeof window.firebase !== 'undefined') {
                window.firebase.analytics();
            }

            ids.forEach((id) => {
                // Send to google analyics
                window.gtag('config', id, {
                    page_path: path
                });
            });
        }

        if (window.newrelic) {
            window.newrelic.setCurrentRouteName(path);
        }

        if (window.COMSCORE) {
            window.COMSCORE.beacon({
                c1: '2',
                c4: `${process.env.AM_URL}${path}`
            });
        }

        if (window.fbq) {
            window.fbq('track', 'PageView');
        }

        if (window.branch) {
            window.branch.closeJourney((err) => {
                if (err) {
                    console.log(err);
                }

                waitForBranchMetadata()
                    .then(() => {
                        window.branch.track('pageview');
                        return;
                    })
                    .catch((err2) => {
                        console.warn(err2);
                    });
            });
        }
    },

    error(err, fatal = false) {
        if (!process.env.BROWSER) {
            return;
        }

        if (typeof window.gtag === 'function') {
            // Need to call firebase.analytics before gtag()
            // https://firebase.google.com/docs/analytics/get-started?platform=web#firebase-gtag
            if (typeof window.firebase !== 'undefined') {
                window.firebase.analytics();
            }

            window.gtag('event', 'exception', {
                description: err.message,
                fatal
            });
        }

        if (window.newrelic) {
            window.newrelic.noticeError(err);
        }
    }
};

export const eventCategory = {
    ad: 'Advertisements',
    dmca: 'DMCA',
    auth: 'Authentication',
    app: 'App',
    player: 'Player',
    upload: 'Upload',
    ampInvite: 'AMP Invite',
    playlist: 'Playlist',
    user: 'User',
    socialShare: 'Social Share',
    comment: 'Comments',
    creatorAuth: 'Creator Authentication'
};

export const eventAction = {
    embedTakedown: 'Embed Takedown',
    tunecore: 'Tunecore',
    sonar: 'Sonarworks',
    bandzoogle: 'Bandzoogle',
    kali: 'Kali Audio',
    landr: 'Landr',
    hive: 'Hive',
    swisher: 'Swisher',
    bet: 'BET',
    sonarworks: 'Sonarworks',
    amworld: 'Audiomack World',
    audioblocks: 'Audioblocks',
    takedown: 'Takedown',
    artistPlay: 'Artist Play',
    adSkip: 'Skipped ad',
    videoAdPlay: 'Video ad should play',
    userRegistration: 'User registration',
    userForgotPassword: 'User forgot password',
    password: 'Password',
    facebook: 'Facebook',
    twitter: 'Twitter',
    google: 'Google',
    instagram: 'Instagram',
    albumUpload: 'Album Upload',
    songUpload: 'Song Upload',
    albumUploadSave: 'Album Save & Finish',
    songUploadSave: 'Song Save & Finish',
    createPlaylist: 'Create Playlist',
    appLinkClick: 'App Link Click',
    editProfile: 'Edit Profile',
    facebookSocial: 'Share on Facebook',
    twitterSocial: 'Share on Twitter',
    instagramSocial: 'Share on Instagram',
    userAddPinned: 'User Add Pin',
    userDeletePinned: 'User Delete Pin',
    userSavedPinned: 'User Saved Pin',
    commentPost: 'Comment Posted'
};

export const eventLabel = {
    homepageLoggedIn: 'Logged-in Homepage',
    mobile: 'mobile',
    desktop: 'desktop',
    songControl: 'Song Control',
    songUpload: 'Song Upload',
    songEdit: 'Song Edit',
    finishedSongUpload: 'Finished Song Upload',
    albumUpload: 'Album Upload',
    uploadStart: 'Upload Start',
    verifiedEmail: 'Verified Email',
    featuredAd: 'Featured 300x250',
    sidebarLink: 'Sidebar Link',
    dashboard: 'Dashboard',
    chartSponsorAd: 'Join Now Link',
    chartSponsorAdMobile: 'Join Now Link - Mobile',
    isUploader: 'uploader',
    isDesktopUploader: 'uploader-desktop',
    iosSidebar: 'iOS in Sidebar',
    androidSidebar: 'Android in Sidebar',
    playerAd: 'Player Uploader Ad',
    deleteAccount: 'Delete Account',
    changeEmail: 'Change Email',
    uploadFinishSocial: 'Upload Finish Social'
};

export const branchEvents = {
    /* player events */
    genre30: 'genre-30',
    genreStarted: 'genre-started',
    genreEnded: 'genre-ended',
    musicType30: 'musicType-30',
    musicTypeStarted: 'musicType-started',
    musicTypeEnded: 'musicType-ended',
    artist30: 'artist-30',
    artistStarted: 'artist-started',
    artistEnded: 'artist-ended',
    image30: 'image-30',
    imageStarted: 'image-started',
    imageEnded: 'image-ended',
    uploader30: 'uploader-30',
    uploaderStarted: 'uploader-started',
    uploaderEnded: 'uploader-ended',
    uploaderImage30: 'uploaderImage-30',
    uploaderImageStarted: 'uploaderImage-started',
    uploaderImageEnded: 'uploaderImage-ended',
    userGenre30: 'userGenre-30',
    userGenreStarted: 'userGenre-started',
    userGenreEnded: 'userGenre-ended',
    userUploads30: 'userUploads-30',
    userUploadsStarted: 'userUploads-started',
    userUploadsEnded: 'userUploads-ended',

    /* user actions */
    userRegistration: 'userRegistration'
};
