/* global describe, test, expect */

import * as utils from '../index';
import {
    playlist,
    song as exampleSong,
    album as exampleAlbum
} from '../../../test/helpers/musicItems';
import playerQueue from '../../../test/helpers/playerQueue';
import {
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    GENRE_TYPE_ALL,
    GENRE_TYPE_RAP,
    CHART_TYPE_DAILY,
    CHART_TYPE_WEEKLY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY
} from 'constants/index';

describe('#getMusicUrl', () => {
    test('should return the correct url for songs', () => {
        const hostUrl = 'https://www.audiomack.com';
        const musicItem = {
            type: 'song',
            uploader: {
                url_slug: 'artist-slug'
            },
            url_slug: 'song-slug'
        };
        const expectedUrl = `${hostUrl}/song/artist-slug/song-slug`;
        const expectedEmbedUrl =
            'https://www.audiomack.com/embed/song/artist-slug/song-slug';
        const expectedOembedUrl = `${hostUrl}/oembed?url=${encodeURIComponent(
            expectedUrl
        )}`;

        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl
            })
        ).toBe(expectedUrl);
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true
            })
        ).toBe(encodeURIComponent(expectedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true
            })
        ).toBe(encodeURIComponent(expectedEmbedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true,
                oembed: true
            })
        ).toBe(expectedOembedUrl);
    });

    test('should return the correct url for albums', () => {
        const hostUrl = 'https://www.audiomack.com';
        const musicItem = {
            type: 'album',
            uploader: {
                url_slug: 'artist-slug'
            },
            url_slug: 'album-slug'
        };
        const expectedUrl = `${hostUrl}/album/artist-slug/album-slug`;
        const expectedEmbedUrl =
            'https://www.audiomack.com/embed/album/artist-slug/album-slug';
        const expectedOembedUrl = `${hostUrl}/oembed?url=${encodeURIComponent(
            expectedUrl
        )}`;

        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl
            })
        ).toBe(expectedUrl);
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true
            })
        ).toBe(encodeURIComponent(expectedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true
            })
        ).toBe(encodeURIComponent(expectedEmbedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true,
                oembed: true
            })
        ).toBe(expectedOembedUrl);
    });

    test('should return the correct url for playlists', () => {
        const hostUrl = 'https://www.audiomack.com';
        const musicItem = {
            type: 'playlist',
            artist: {
                url_slug: 'artist-slug'
            },
            url_slug: 'playlist-slug'
        };
        const expectedUrl = `${hostUrl}/playlist/artist-slug/playlist-slug`;
        const expectedEmbedUrl =
            'https://www.audiomack.com/embed/playlist/artist-slug/playlist-slug';
        const expectedOembedUrl = `${hostUrl}/oembed?url=${encodeURIComponent(
            expectedUrl
        )}`;

        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl
            })
        ).toBe(expectedUrl);
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true
            })
        ).toBe(encodeURIComponent(expectedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true
            })
        ).toBe(encodeURIComponent(expectedEmbedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true,
                oembed: true
            })
        ).toBe(expectedOembedUrl);
    });

    test('should return the correct url for album tracks in a queue', () => {
        const hostUrl = 'https://www.audiomack.com';
        const musicItem = {
            type: 'song',
            parentDetails: {
                uploader: {
                    url_slug: 'artist-slug'
                }
            },
            url_slug: 'song-slug'
        };
        const expectedUrl = `${hostUrl}/song/artist-slug/song-slug`;
        const expectedEmbedUrl =
            'https://www.audiomack.com/embed/song/artist-slug/song-slug';
        const expectedOembedUrl = `${hostUrl}/oembed?url=${encodeURIComponent(
            expectedUrl
        )}`;

        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl
            })
        ).toBe(expectedUrl);
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true
            })
        ).toBe(encodeURIComponent(expectedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true
            })
        ).toBe(encodeURIComponent(expectedEmbedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true,
                oembed: true
            })
        ).toBe(expectedOembedUrl);
    });

    test('should return the correct url for playlist tracks in a queue', () => {
        const hostUrl = 'https://www.audiomack.com';
        const musicItem = {
            type: 'song',
            parentDetails: {
                artist: {
                    url_slug: 'artist-slug'
                }
            },
            url_slug: 'song-slug'
        };
        const expectedUrl = `${hostUrl}/song/artist-slug/song-slug`;
        const expectedEmbedUrl =
            'https://www.audiomack.com/embed/song/artist-slug/song-slug';
        const expectedOembedUrl = `${hostUrl}/oembed?url=${encodeURIComponent(
            expectedUrl
        )}`;

        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl
            })
        ).toBe(expectedUrl);
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true
            })
        ).toBe(encodeURIComponent(expectedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true
            })
        ).toBe(encodeURIComponent(expectedEmbedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                embed: true,
                oembed: true
            })
        ).toBe(expectedOembedUrl);
    });

    test('should add a query string for the query object', () => {
        const hostUrl = 'https://www.audiomack.com';
        const musicItem = {
            type: 'song',
            parentDetails: {
                artist: {
                    url_slug: 'artist-slug'
                }
            },
            url_slug: 'song-slug'
        };
        const query = {
            key: 'test'
        };
        const expectedUrl = `${hostUrl}/song/artist-slug/song-slug?key=test`;
        const expectedEmbedUrl =
            'https://www.audiomack.com/embed/song/artist-slug/song-slug?key=test';
        const expectedOembedUrl = `${hostUrl}/oembed?url=${encodeURIComponent(
            expectedUrl
        )}`;

        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                query
            })
        ).toBe(expectedUrl);
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                query,
                encoded: true
            })
        ).toBe(encodeURIComponent(expectedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                query,
                embed: true
            })
        ).toBe(encodeURIComponent(expectedEmbedUrl));
        expect(
            utils.getMusicUrl(musicItem, {
                host: hostUrl,
                encoded: true,
                query,
                embed: true,
                oembed: true
            })
        ).toBe(expectedOembedUrl);
    });

    test('should throw for oembed option when no host is provided', () => {
        const musicItem = {
            type: 'playlist',
            artist: {
                url_slug: 'artist-slug'
            },
            url_slug: 'playlist-slug'
        };

        const callFn = utils.getMusicUrl.bind(utils, musicItem, {
            oembed: true
        });

        expect(callFn).toThrow();
    });
});

describe('#yesBool', () => {
    test('should true for `true`', () => {
        expect(utils.yesBool(true)).toBe(true);
    });

    test('should true for `"true"`', () => {
        expect(utils.yesBool('true')).toBe(true);
    });

    test('should true for "yes"', () => {
        expect(utils.yesBool('yes')).toBe(true);
    });

    test('should true for "1"', () => {
        expect(utils.yesBool('1')).toBe(true);
    });

    test('should true for `1`', () => {
        expect(utils.yesBool(1)).toBe(true);
    });

    test('should false for any other number', () => {
        expect(utils.yesBool(0)).toBe(false);
    });

    test('should false for any other string', () => {
        expect(utils.yesBool('string')).toBe(false);
    });

    test('should false for undefined', () => {
        expect(utils.yesBool(undefined)).toBe(false);
    });
});

describe('#isCurrentMusicItem', () => {
    test('should return false for a falsy song or musicItem', () => {
        let currentSong = null;
        let musicItem = {};

        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(false);

        currentSong = {};
        musicItem = undefined;
        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(false);
    });

    test('should return true if the 2 items are the same reference', () => {
        const song = {};
        const currentSong = song;
        const musicItem = song;

        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(true);
    });

    test('should return false if the currentSong object is album and contains a song', () => {
        const song = {
            id: 271
        };

        const currentSong = {
            type: 'album',
            id: 102,
            tracks: [
                {
                    song_id: 271
                }
            ]
        };

        expect(utils.isCurrentMusicItem(currentSong, song)).toBe(false);
    });

    test('should return false if the currentSong object is playlist and contains a song', () => {
        const song = {
            id: 271
        };

        const currentSong = {
            type: 'playlist',
            id: 102,
            tracks: [song]
        };

        expect(utils.isCurrentMusicItem(currentSong, song)).toBe(false);
    });

    test('should return true if the currentSong object is album and musicItem is album with same id', () => {
        const currentSong = {
            type: 'album',
            id: 102,
            tracks: [
                {
                    song_id: 271
                }
            ]
        };

        const musicItem = {
            type: 'album',
            id: 102,
            tracks: [
                {
                    song_id: 271
                }
            ]
        };

        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(true);
    });

    test('should return true if the currentSong object is playlist and musicItem is album with same id', () => {
        const song = {
            id: 271
        };

        const currentSong = {
            type: 'playlist',
            id: 102,
            tracks: [song]
        };

        const musicItem = {
            type: 'playlist',
            id: 102,
            tracks: [song]
        };

        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(true);
    });

    test('should return true if the currentSong and musicItem has same id', () => {
        const currentSong = {
            id: 102,
            type: 'song'
        };

        const musicItem = {
            id: 102,
            type: 'song'
        };

        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(true);
    });

    test('should return false if the currentSong and musicItem has mismatch id', () => {
        const currentSong = {
            id: 102,
            type: 'song'
        };

        const musicItem = {
            id: 103,
            type: 'song'
        };

        expect(utils.isCurrentMusicItem(currentSong, musicItem)).toBe(false);
    });
});

describe('#round', () => {
    test('return non-numerical values', () => {
        expect(utils.round(null)).toBe(null);
        expect(utils.round(undefined)).toBe(undefined);
        expect(utils.round('sd3af')).toBe('sd3af');
    });

    test('accept string numerical values', () => {
        expect(utils.round('1')).toBe(1);
        expect(utils.round('1.2333')).toBe(1.23);
    });

    test('should truncate to 2 decimals by default', () => {
        expect(utils.round(1.2345323)).toBe(1.23);
    });

    test('respect decimals argument', () => {
        expect(utils.round(1.2345323, 3)).toBe(1.235);
    });
});

describe('#renderCappedDisplayCount', () => {
    test('should render with no options argument', () => {
        expect(utils.renderCappedDisplayCount(3)).toBe(3);
    });

    test('should be capped at 99 by default with a + suffix', () => {
        expect(utils.renderCappedDisplayCount(100)).toBe('99+');
    });

    test('should return null for anything less than or equal to 0', () => {
        expect(utils.renderCappedDisplayCount(0)).toBe(null);
    });

    test('should be ok with string numbers', () => {
        expect(utils.renderCappedDisplayCount('0')).toBe(null);
        expect(utils.renderCappedDisplayCount('4')).toBe(4);
        expect(utils.renderCappedDisplayCount('100')).toBe('99+');
    });

    test('should accept a different max argument', () => {
        expect(utils.renderCappedDisplayCount(100, { max: 100 })).toBe(100);
        expect(utils.renderCappedDisplayCount(101, { max: 100 })).toBe('100+');
    });

    test('should accept a different after argument', () => {
        expect(utils.renderCappedDisplayCount(100, { after: '' })).toBe('99');
        expect(utils.renderCappedDisplayCount(100, { after: '-' })).toBe('99-');
        expect(utils.renderCappedDisplayCount(100, { after: null })).toBe('99');
        expect(utils.renderCappedDisplayCount(100, { after: undefined })).toBe(
            '99+'
        );
        expect(utils.renderCappedDisplayCount(100, { after: 0 })).toBe('990');
    });
});

describe('#convertStringToUrl', () => {
    test('should convert a string appropriately', () => {
        const actual = 'kodak-black---kodak_black_-_roll_in_peace_feat_xxx3';
        const expected = 'kodak-black-kodak-black-roll-in-peace-feat-xxx3';

        expect(utils.convertStringToUrl(actual)).toBe(expected);
    });

    test('should remove the extension if removeExtension is true', () => {
        let actual = 'kodak-black---kodak_black_-_roll_in_peace_feat.mp3';
        const expected = 'kodak-black-kodak-black-roll-in-peace-feat';

        expect(
            utils.convertStringToUrl(actual, { removeExtension: true })
        ).toBe(expected);

        actual = 'kodak-black---kodak_black_-_roll_in_peace_feat.WAV';
        expect(
            utils.convertStringToUrl(actual, { removeExtension: true })
        ).toBe(expected);

        actual = 'kodak-black---kodak_black_-_roll_in_peace_feat.MP3';
        expect(
            utils.convertStringToUrl(actual, { removeExtension: true })
        ).toBe(expected);

        actual = 'kodak-black---kodak_black_-_roll_in_peace_feat.aiff';
        expect(
            utils.convertStringToUrl(actual, { removeExtension: true })
        ).toBe(expected);

        actual = 'kodak-black---kodak_black_-_roll_in_peace_feat.ogg';
        expect(
            utils.convertStringToUrl(actual, { removeExtension: true })
        ).toBe(expected);

        actual = 'kodak-black---kodak_black_-_roll_in_peace_feat.m4a';
        expect(
            utils.convertStringToUrl(actual, { removeExtension: true })
        ).toBe(expected);
    });
});

describe('#buildSearchUrl', () => {
    test('should not include falsy values in end result', () => {
        const options = {
            type: 'music',
            page: null,
            verfied: null
        };
        const query = ' trimmed  ';
        const expected = '/search?q=trimmed&show=music';

        expect(utils.buildSearchUrl(query, options)).toBe(expected);
    });
});

describe('#humanizeNumber', () => {
    test('should convert numbers correctly', () => {
        expect(utils.humanizeNumber()).toBe('0');
        expect(utils.humanizeNumber(999)).toBe('999');
        expect(utils.humanizeNumber(9996)).toBe('10K');
        expect(utils.humanizeNumber(99900)).toBe('99.9K');
        expect(utils.humanizeNumber(99990)).toBe('100K');
        expect(utils.humanizeNumber(999000)).toBe('999K');
        expect(utils.humanizeNumber(999900)).toBe('1M');
        expect(utils.humanizeNumber(9990000)).toBe('9.99M');
        expect(utils.humanizeNumber(9999000)).toBe('10M');
        expect(utils.humanizeNumber(99880000)).toBe('99.9M');
        expect(utils.humanizeNumber(99880000)).toBe('99.9M');
        expect(utils.humanizeNumber(1199880000)).toBe('1.20B');
    });
});

describe('#isDownloadableTrack', () => {
    test('should return false if theres no `download_url`', () => {
        const musicItem = {};

        expect(utils.isDownloadableTrack(musicItem)).toBe(false);
    });

    test('should return false if `stream_only` is "yes"', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'yes'
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(false);
    });

    test('should return false if `is_soundcloud` is `true`', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: true
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(false);
    });

    test('should return false if `follow_download` is "yes"', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: 'yes'
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(false);
    });

    test('should return true if `follow_download` is "0"', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: '0'
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(true);
    });

    test('should return true if `follow_download` is `0`', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: 0
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(true);
    });

    test('should return true if `follow_download` is `""`', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: ''
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(true);
    });

    test('should return true if `follow_download` is `"no"`', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: 'no'
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(true);
    });

    test('should return true if `follow_download` is `undefined`', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: undefined
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(true);
    });

    test('should return false if `album_track_only` is "1"', () => {
        const musicItem = {
            download_url: 'some.download.url',
            stream_only: 'no',
            is_soundcloud: false,
            follow_download: undefined,
            album_track_only: '1'
        };

        expect(utils.isDownloadableTrack(musicItem)).toBe(false);
    });
});

describe('#getChartUrl', () => {
    test('should return the appropriate link', () => {
        const hostUrl = 'https://www.audiomack.com';
        const page = 2;

        let expectedUrl = `${hostUrl}/trending-now`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_TRENDING,
                genre: GENRE_TYPE_ALL
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/trending-now/page/${page}`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_TRENDING,
                genre: GENRE_TYPE_ALL,
                page: page
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/songs`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_ALL
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/songs/day`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_ALL,
                timePeriod: CHART_TYPE_DAILY
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/songs/week`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_ALL,
                timePeriod: CHART_TYPE_WEEKLY
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/songs/month`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_ALL,
                timePeriod: CHART_TYPE_MONTHLY
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/songs/year`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_ALL,
                timePeriod: CHART_TYPE_YEARLY
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/${GENRE_TYPE_RAP}/songs/page/${page}`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_RAP,
                page: page
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/${GENRE_TYPE_RAP}/albums/page/${page}`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_ALBUM,
                genre: GENRE_TYPE_RAP,
                page: page
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/${GENRE_TYPE_RAP}/songs`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_SONG,
                genre: GENRE_TYPE_RAP
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/${COLLECTION_TYPE_RECENTLY_ADDED}`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_RECENTLY_ADDED,
                genre: GENRE_TYPE_RAP
            })
        ).toBe(expectedUrl);

        expectedUrl = `${hostUrl}/${COLLECTION_TYPE_RECENTLY_ADDED}/page/${page}`;

        expect(
            utils.getChartUrl({
                host: hostUrl,
                context: COLLECTION_TYPE_RECENTLY_ADDED,
                genre: GENRE_TYPE_RAP,
                page: page
            })
        ).toBe(expectedUrl);
    });
});

describe('#convertSecondsToTimecode', () => {
    test('should number correctly', () => {
        expect(utils.convertSecondsToTimecode()).toBe('0:00');
        expect(utils.convertSecondsToTimecode('1')).toBe('0:01');
        expect(utils.convertSecondsToTimecode('0')).toBe('0:00');
        expect(utils.convertSecondsToTimecode(0)).toBe('0:00');
        expect(utils.convertSecondsToTimecode('59')).toBe('0:59');
        expect(utils.convertSecondsToTimecode(89)).toBe('1:29');
        expect(utils.convertSecondsToTimecode(60 * 3)).toBe('3:00');
        expect(utils.convertSecondsToTimecode(60 * 59)).toBe('59:00');
        expect(utils.convertSecondsToTimecode(60 * 60)).toBe('1:00:00');
        expect(utils.convertSecondsToTimecode(3602)).toBe('1:00:02');
    });
});

describe('#convertTimeStringToSeconds', () => {
    test('should time string correctly', () => {
        expect(utils.convertTimeStringToSeconds()).toBe(0);
        expect(utils.convertTimeStringToSeconds('1')).toBe(1);
        expect(utils.convertTimeStringToSeconds('0')).toBe(0);
        expect(utils.convertTimeStringToSeconds('59')).toBe(59);
        expect(utils.convertTimeStringToSeconds('89')).toBe(89);
        expect(utils.convertTimeStringToSeconds('89m')).toBe(89 * 60);
        expect(utils.convertTimeStringToSeconds('1m2')).toBe(62);
        expect(utils.convertTimeStringToSeconds('1m2s')).toBe(62);
        expect(utils.convertTimeStringToSeconds('1h2')).toBe(3602);
        expect(utils.convertTimeStringToSeconds('1h2s')).toBe(3602);
        expect(utils.convertTimeStringToSeconds('1h1m2s')).toBe(3662);
        expect(utils.convertTimeStringToSeconds('1h1m2')).toBe(3662);
    });
});

describe('#convertSecondsToTimeString', () => {
    test('should seconds to time string correctly', () => {
        expect(utils.convertSecondsToTimeString('s')).toBe('');
        expect(utils.convertSecondsToTimeString()).toBe('');
        expect(utils.convertSecondsToTimeString(1)).toBe('1s');
        expect(utils.convertSecondsToTimeString(0)).toBe('');
        expect(utils.convertSecondsToTimeString(59)).toBe('59s');
        expect(utils.convertSecondsToTimeString(89)).toBe('1m29s');
        expect(utils.convertSecondsToTimeString(89 * 60)).toBe('1h29m');
        expect(utils.convertSecondsToTimeString(62)).toBe('1m2s');
        expect(utils.convertSecondsToTimeString(3602)).toBe('1h2s');
        expect(utils.convertSecondsToTimeString(3662)).toBe('1h1m2s');
    });
});

describe('#getFlattendedQueueFromApiItems', () => {
    test('should flatten API response objects properly', () => {
        expect(
            utils.getFlattendedQueueFromApiItems([playlist, exampleSong])
        ).toMatchSnapshot();
    });
});

describe('#getNormalizedQueueTrack', () => {
    test('should normalize API response object tracks properly', () => {
        expect(utils.getNormalizedQueueTrack(playlist, 1)).toMatchSnapshot();
    });
});

describe('#getQueueIndexForSong', () => {
    test('should return the index of the first track of a playlist', () => {
        expect(utils.getQueueIndexForSong(playlist, playerQueue)).toBe(1);
    });
    test('should return the index of the first track of a playlist if ignoreHistory is false and the current queue index has already passed the first track', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 7,
                ignoreHistory: false
            })
        ).toBe(1);
    });
    test('should return the correct index of the current queue index is 0', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 0
            })
        ).toBe(1);
    });
    test('should return -1 if the first track of the playlist is already the current queue index', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 1
            })
        ).toBe(-1);
    });
    test('should return the specific index of the specified track if the current queue index has not passed it', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 1,
                trackIndex: 1
            })
        ).toBe(2);
    });
    test('should return -1 for the specified track if the current queue index is the index of the track', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 2,
                trackIndex: 1
            })
        ).toBe(-1);
    });
    test('should return -1 for the specified track if the current queue index has passed it', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 3,
                trackIndex: 1
            })
        ).toBe(-1);
    });
    test('should return the correct index for the specified track if the current queue index has passed it and ignoreHistory is false', () => {
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 3,
                trackIndex: 1,
                ignoreHistory: false
            })
        ).toBe(2);
    });
    test('should return the latest track of the queue in history if it appears more than once', () => {
        const newQueue = Array.from(playerQueue);
        const trackIndex = 1;
        const track = {
            ...playlist.tracks[trackIndex]
        };
        const latestIndex = 2;

        newQueue.splice(0, 0, track);
        newQueue.splice(latestIndex, 0, track);
        expect(
            utils.getQueueIndexForSong(playlist, playerQueue, {
                currentQueueIndex: 5,
                trackIndex: trackIndex,
                ignoreHistory: false
            })
        ).toBe(latestIndex);
    });
});

describe('#queueHasMusicItem', () => {
    test('should return true if all tracks from a playlist are in a queue', () => {
        expect(utils.queueHasMusicItem(playerQueue, playlist)).toBe(true);
    });
    test('should return false if all tracks from a playlist are in a queue but the current queue index is passed the first track', () => {
        expect(utils.queueHasMusicItem(playerQueue, playlist, 4)).toBe(false);
    });
    test('should return false if all tracks from a playlist are in a queue but the current queue index is the first track', () => {
        expect(utils.queueHasMusicItem(playerQueue, playlist, 1)).toBe(true);
    });
});

function createWaitPromise(value, timeout = 10) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(value);
        }, timeout);
    });
}

describe('#batchPromises', () => {
    test('should return run sequentially by default', async () => {
        const expected = [100, 200, 300];

        const values = await utils.batchPromises(expected.length, (i) => {
            // Longer wait times for the subsequent promises
            // shouldnt matter because theyre sequential
            const timeout = (expected.length - i) * 50;

            return createWaitPromise(expected[i], timeout);
        });

        expect(values).toEqual(expected);
    });

    test('should return the expected values when doing parallel batching', async () => {
        const expected = [100, 200, 300];

        const values = await utils.batchPromises(
            expected.length,
            (i) => {
                const timeout = (3 - i) * 50;

                return createWaitPromise(expected[i], timeout);
            },
            { parallel: true, batchCount: 2 }
        );

        values.sort();

        expect(values).toEqual(expected);
    });

    test('should respect the `parallelAfterFirstCompletion` option by not running the rest of the promises until the first completes even when `parallel` is true', async () => {
        const expected = [100, 200, 300];

        const getTimeout = (i) => {
            // eslint-disable-line
            if (i === 0) {
                return 500;
            }

            return 10;
        };

        const values = await utils.batchPromises(
            expected.length,
            (i) => {
                const timeout = getTimeout(i);

                return createWaitPromise(expected[i], timeout);
            },
            { parallelAfterFirstCompletion: true, parallel: true }
        );

        expect(values[0]).toEqual(expected[0]);
    });
});

describe('#ownsMusic', () => {
    test('should return false is user is not logged in', () => {
        const currentUser = {};
        const musicItem = {};

        expect(utils.ownsMusic(currentUser, musicItem)).toEqual(false);
    });

    test('should return false is user is falsy', () => {
        const currentUser = null;
        const musicItem = {};

        expect(utils.ownsMusic(currentUser, musicItem)).toEqual(false);
    });

    test('should return false is musicItem is falsy', () => {
        const musicItem = null;
        const currentUser = {};

        expect(utils.ownsMusic(currentUser, musicItem)).toEqual(false);
    });

    test('should account for owned queue music items from an album', () => {
        const musicItem = {
            title: "Eight Nine (Won't You Be Mine?)",
            streaming_url:
                'https://songs.dev.audiomack.com/streaming/audiomack-test-march-25th/eight-nine-wont-you-be-mine.mp3?Expires=1560415130&Signature=AEx~DEwmiWUzCzGW057D93EOox~QyoGOjsFVFqULMysaml~eK7bgR5CmXU4koziJ4curPiIyYRTJYeRLpkok2IiNs8C35RvVGlrX8x~fdXq6TbvjdjPZDBtPyZPa9iY4MaV2FzAe-XOQMlivlmQAoKxLT0PxrbiZOOabiPc5Z4w_&Key-Pair-Id=APKAIKAIRXBA2H7FXITA',
            volume_data:
                '[0,14,14,14,9,8,17,15,17,6,12,18,16,14,6,14,15,14,13,5,9,16,11,12,5,12,19,15,11,9,16,14,16,10,10,18,15,17,8,21,33,30,35,35,26,34,35,32,34,27,36,35,36,35,29,38,36,29,0,0,0,0,0,0]',
            duration: '64',
            genre: 'rnb',
            artist: 'Ava Luna',
            producer: '',
            featuring: '',
            status: 'complete',
            video_ad: 'no',
            isrc: '',
            url_slug: 'eight-nine-wont-you-be-mine',
            accounting_code: '',
            album_track_only: false,
            errors: [],
            song_id: 17441,
            id: 17441,
            trackIndex: 0,
            image:
                'https://assets.dev.audiomack.com/--censored--/censored-260x260.png',
            image_base:
                'https://assets.dev.audiomack.com/--censored--/censored-260x260.png',
            released: 1553528836,
            type: 'song',
            parentDetails: {
                title: 'Fire Album',
                id: 17443,
                url_slug: 'fire-album',
                type: 'album',
                uploader: {
                    url_slug: 'audiomack-test-march-25th',
                    id: 2311,
                    name: 'audiomack test march 25th',
                    image:
                        'https://assets.dev.audiomack.com/audiomack-test-march-25th/69f1320bf2e9beb65fee90e5a258d6acb9e2ee46404b355602da58988f778194.jpeg',
                    image_base:
                        'https://assets.dev.audiomack.com/audiomack-test-march-25th/69f1320bf2e9beb65fee90e5a258d6acb9e2ee46404b355602da58988f778194.jpeg'
                }
            },
            startedPlaying: true
        };
        const currentUser = {
            isLoggedIn: true,
            profile: {
                id: 2311
            }
        };

        expect(utils.ownsMusic(currentUser, musicItem)).toEqual(true);
    });
    test('should account for non-owned queue music items from an album', () => {
        const musicItem = {
            title: "Eight Nine (Won't You Be Mine?)",
            streaming_url:
                'https://songs.dev.audiomack.com/streaming/audiomack-test-march-25th/eight-nine-wont-you-be-mine.mp3?Expires=1560415130&Signature=AEx~DEwmiWUzCzGW057D93EOox~QyoGOjsFVFqULMysaml~eK7bgR5CmXU4koziJ4curPiIyYRTJYeRLpkok2IiNs8C35RvVGlrX8x~fdXq6TbvjdjPZDBtPyZPa9iY4MaV2FzAe-XOQMlivlmQAoKxLT0PxrbiZOOabiPc5Z4w_&Key-Pair-Id=APKAIKAIRXBA2H7FXITA',
            volume_data:
                '[0,14,14,14,9,8,17,15,17,6,12,18,16,14,6,14,15,14,13,5,9,16,11,12,5,12,19,15,11,9,16,14,16,10,10,18,15,17,8,21,33,30,35,35,26,34,35,32,34,27,36,35,36,35,29,38,36,29,0,0,0,0,0,0]',
            duration: '64',
            genre: 'rnb',
            artist: 'Ava Luna',
            producer: '',
            featuring: '',
            status: 'complete',
            video_ad: 'no',
            isrc: '',
            url_slug: 'eight-nine-wont-you-be-mine',
            accounting_code: '',
            album_track_only: false,
            errors: [],
            song_id: 17441,
            id: 17441,
            trackIndex: 0,
            image:
                'https://assets.dev.audiomack.com/--censored--/censored-260x260.png',
            image_base:
                'https://assets.dev.audiomack.com/--censored--/censored-260x260.png',
            released: 1553528836,
            type: 'song',
            parentDetails: {
                title: 'Fire Album',
                id: 17443,
                url_slug: 'fire-album',
                type: 'album',
                uploader: {
                    url_slug: 'audiomack-test-march-25th',
                    id: 2311,
                    name: 'audiomack test march 25th',
                    image:
                        'https://assets.dev.audiomack.com/audiomack-test-march-25th/69f1320bf2e9beb65fee90e5a258d6acb9e2ee46404b355602da58988f778194.jpeg',
                    image_base:
                        'https://assets.dev.audiomack.com/audiomack-test-march-25th/69f1320bf2e9beb65fee90e5a258d6acb9e2ee46404b355602da58988f778194.jpeg'
                }
            },
            startedPlaying: true
        };
        const currentUser = {
            isLoggedIn: true,
            profile: {
                id: 2312
            }
        };

        expect(utils.ownsMusic(currentUser, musicItem)).toEqual(false);
    });
});

describe('#getFeaturing', () => {
    test('should correct featuring for song', () => {
        expect(utils.getFeaturing(exampleSong)).toMatchSnapshot();
    });

    test('should correct featuring for album', () => {
        expect(utils.getFeaturing(exampleAlbum)).toMatchSnapshot();
    });

    test('should de-dupe featuring for songs', () => {
        const song = {
            type: 'song',
            featuring: 'artist 1, artist2  , another artist, artist 1  , boom'
        };

        expect(utils.getFeaturing(song)).toMatchSnapshot();
    });

    test('should de-dupe featuring for albums', () => {
        const album = {
            type: 'album',
            tracks: [
                {
                    featuring:
                        'artist 1, artist 2  , another artist, artist 1  , boom'
                },
                {
                    featuring:
                        'artist 1, artist 3  , another artist, artist 1  , boom'
                },
                { featuring: 'artist 4' }
            ]
        };

        expect(utils.getFeaturing(album)).toMatchSnapshot();
    });
});

describe('#uploadSlugify', () => {
    test('should remove non word characters from the beginning and end', () => {
        const actual = "-+Someone's song (Intro)";
        const expected = 'someones-song-intro';
        expect(utils.uploadSlugify(actual)).toEqual(expected);
    });

    test('should be lowercased', () => {
        expect(utils.uploadSlugify('Some Slug')).toEqual('some-slug');
    });
});

describe('#formatCurrency', () => {
    test('should convert an even number to 2 decimals', () => {
        const actual = 3333;
        const expected = '$3,333.00';
        expect(utils.formatCurrency(actual)).toEqual(expected);
    });
    test('should convert tenth to 2 decimals', () => {
        const actual = 3333.1;
        const expected = '$3,333.10';
        expect(utils.formatCurrency(actual)).toEqual(expected);
    });
    test('should leave the hundreth place', () => {
        const actual = 333.15;
        const expected = '$333.15';
        expect(utils.formatCurrency(actual)).toEqual(expected);
    });
    test('should drop the thousands place to the hundreth place', () => {
        const actual = 3333.143;
        const expected = '$3,333.14';
        expect(utils.formatCurrency(actual)).toEqual(expected);
    });
});
