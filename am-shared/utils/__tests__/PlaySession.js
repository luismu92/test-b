/* global test, expect */

import PlaySession from '../PlaySession';

test('should be populated on play', function() {
    const playSession = new PlaySession();

    expect(playSession.calculateTime()).toEqual(0);

    const currentTime = 1;

    const session = playSession.play(currentTime);

    expect(session).toEqual([{ start: currentTime, end: currentTime }]);
});

test('should create a new block when seeking', function() {
    const playSession = new PlaySession();
    const currentTime = 0;
    const timeToGoTo = 10;

    playSession.play(currentTime);

    const session = playSession.seek(timeToGoTo);
    const expected = [
        { start: currentTime, end: currentTime },
        { start: timeToGoTo, end: timeToGoTo }
    ];

    expect(session).toEqual(expected);
});

test('should update the most recent block end time', function() {
    const playSession = new PlaySession();

    playSession.play();

    const seekTime = 10;
    const updateTime = 20;

    playSession.seek(seekTime);

    const session = playSession.timeupdate(updateTime);
    const expected = [
        { start: 0, end: 0 },
        { start: seekTime, end: updateTime }
    ];

    expect(session).toEqual(expected);
});

test('should be calculated for a total play time', function() {
    const playSession = new PlaySession();

    playSession.play();

    const seekTime = 10;
    const updateTime = 20;

    playSession.seek(seekTime);

    playSession.timeupdate(updateTime);

    expect(playSession.calculateTime()).toEqual(10);
});

test('should reset correctly', function() {
    const playSession = new PlaySession();

    playSession.play();

    const seekTime = 10;

    playSession.seek(seekTime);

    const session = playSession.reset();

    expect(session).toEqual([]);
});

test('should start/stop collecting correctly', function() {
    const playSession = new PlaySession();

    playSession.play();
    playSession.stopCollecting();

    const seekTime = 10;
    const updateTime = 20;

    playSession.seek(seekTime);

    playSession.timeupdate(updateTime);

    expect(playSession.calculateTime()).toEqual(0);

    playSession.startCollecting();
    playSession.timeupdate(updateTime);

    expect(playSession.calculateTime()).toEqual(20);
});

test('should call callbacks once', function() {
    const playSession = new PlaySession();

    let called = 0;

    playSession.once(10, () => {
        called += 1;
    });
    playSession.play();

    const updateTime = 20;

    playSession.timeupdate(updateTime);

    expect(called).toEqual(1);

    playSession.timeupdate(30);

    expect(called).toEqual(1);
});

test('should call callbacks once again after reset', function() {
    const playSession = new PlaySession();

    let called = 0;

    playSession.once(10, () => {
        called += 1;
    });
    playSession.play();

    const updateTime = 20;

    playSession.timeupdate(updateTime);

    expect(called).toEqual(1);

    playSession.reset();

    playSession.once(10, () => {
        called += 1;
    });

    playSession.play();

    playSession.timeupdate(updateTime);

    expect(called).toEqual(2);

    playSession.timeupdate(30);

    expect(called).toEqual(2);
});
