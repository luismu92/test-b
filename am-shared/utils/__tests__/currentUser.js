/* global describe, test, expect */

import * as utils from '../currentUser';

describe('#canUserJoinCreatorAuthentication', () => {
    test('should return false for logged out users', () => {
        const currentUser = {
            isLoggedIn: false
        };

        expect(utils.canUserJoinCreatorAuthentication(currentUser)).toBe(false);
    });

    test('should return false for user with no stats', () => {
        const currentUser = {
            isLoggedIn: true,
            profile: {}
        };

        expect(utils.canUserJoinCreatorAuthentication(currentUser)).toBe(false);
    });

    test('should return false for user with not enough clout', () => {
        const playsRequired = 100;
        const uploadsRequired = 100;
        const currentUser = {
            isLoggedIn: true,
            profile: {
                stats: {
                    'plays-raw': 99
                },
                upload_count_excluding_reups: 99
            }
        };

        expect(
            utils.canUserJoinCreatorAuthentication(currentUser, {
                playsRequired,
                uploadsRequired
            })
        ).toBe(false);
    });

    test('should return true for user with just enough clout', () => {
        const playsRequired = 100;
        const uploadsRequired = 100;
        const currentUser = {
            isLoggedIn: true,
            profile: {
                stats: {
                    'plays-raw': 100
                },
                upload_count_excluding_reups: 100
            }
        };

        expect(
            utils.canUserJoinCreatorAuthentication(currentUser, {
                playsRequired,
                uploadsRequired
            })
        ).toBe(true);
    });

    test('should return true for user with more than enough clout', () => {
        const playsRequired = 100;
        const uploadsRequired = 100;
        const currentUser = {
            isLoggedIn: true,
            profile: {
                stats: {
                    'plays-raw': 10000
                },
                upload_count_excluding_reups: 10000
            }
        };

        expect(
            utils.canUserJoinCreatorAuthentication(currentUser, {
                playsRequired,
                uploadsRequired
            })
        ).toBe(true);
    });
});
