/* global describe, test, expect */

import * as utils from '../blog';

describe('#getBlogUrl', () => {
    test('should return relative url if no host is provided', () => {
        const post = {
            slug: 'song',
            id: 'basdfajasdg'
        };
        const expectedUrl = `/world/post/${post.slug}`;

        expect(utils.getBlogPostUrl(post)).toBe(expectedUrl);
    });

    test('should return the correct url for a post', () => {
        const hostUrl = 'https://www.audiomack.com';
        const post = {
            slug: 'song',
            id: 'basdfajasdg'
        };
        const expectedUrl = `${hostUrl}/world/post/${post.slug}`;

        expect(
            utils.getBlogPostUrl(post, {
                host: hostUrl
            })
        ).toBe(expectedUrl);
    });
});
