/* global describe, expect, test */

import * as utils from '../tags';
import { tagSections } from 'constants/index';

describe('#getTagOptions', () => {
    test('should format API response to expected display format', () => {
        const input = {
            options: {
                geo: [],
                music: [
                    {
                        display: 'Chill Beats-4',
                        addedon: 1551885859,
                        tag: 'chillbeats4',
                        section: 'Sub-Genre',
                        normalizedKey: 'chillbeats4'
                    },
                    {
                        display: 'DMV',
                        addedon: 1576261230,
                        section: 'Admin Only',
                        tag: 'dmv',
                        normalizedKey: 'dmv'
                    },
                    {
                        display: 'High',
                        addedon: 1583947845,
                        section: 'Mood',
                        tag: 'high',
                        normalizedKey: 'high'
                    }
                ]
            },
            ui: {
                geo: ['United Kingdom', 'United States']
            }
        };
        const output = {
            locations: [],
            moods: [
                { text: 'Choose a mood', value: '' },
                { text: 'High', value: 'high' }
            ],
            subgenres: {
                [tagSections.defaultOption]: {
                    text: 'Choose up to (2) subgenres',
                    value: ''
                },
                [tagSections.subgenre]: [
                    { text: 'Chill Beats-4', value: 'chillbeats4' }
                ]
            }
        };

        expect(utils.getTagOptions(input)).toEqual(output);
    });

    test('should format empty state to expected display format', () => {
        const input = {
            options: {
                geo: [],
                music: []
            },
            ui: {
                geo: []
            }
        };
        const output = {
            locations: [],
            moods: [{ text: 'Choose a mood', value: '' }],
            subgenres: {
                [tagSections.defaultOption]: {
                    text: 'Choose up to (2) subgenres',
                    value: ''
                }
            }
        };

        expect(utils.getTagOptions(input)).toEqual(output);
    });
});

describe('#getUserTags', () => {
    test('should format API response to expected display format', () => {
        const input = [
            {
                display: 'Chill Beats-4',
                normalizedKey: 'chillbeats4',
                section: 'Sub-Genre'
            },
            {
                display: 'Raggae Mon',
                normalizedKey: 'raggaemon',
                section: 'Sub-Genre'
            },
            {
                display: 'High',
                normalizedKey: 'high',
                section: 'Mood'
            }
        ];
        const output = {
            subgenres: ['chillbeats4', 'raggaemon'],
            moods: ['high'],
            locations: []
        };

        expect(utils.getUserTags(input)).toEqual(output);
    });

    test('should format empty state to expected display format', () => {
        const input = [];
        const output = {
            subgenres: [],
            moods: [],
            locations: []
        };

        expect(utils.getUserTags(input)).toEqual(output);
    });
});

describe('#mapTagOptions', () => {
    test('should map tag option property value as key mapped to another property value from the same option', () => {
        const input = [
            'keyOne',
            'keyTwo',
            {
                options: {
                    geo: [],
                    music: [
                        {
                            keyOne: 'valueOne',
                            keyTwo: 'valueTwo'
                        }
                    ]
                },
                ui: {
                    geo: [
                        {
                            keyOne: 'valueThree',
                            keyTwo: 'valueFour'
                        }
                    ]
                }
            }
        ];
        const output = {
            valueOne: 'valueTwo',
            valueThree: 'valueFour'
        };

        expect(utils.mapTagOptions(...input)).toEqual(output);
    });
});
