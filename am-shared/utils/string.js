// Edited from:
// https://stackoverflow.com/a/41563966/1048847
export function csvToArray(text, mergeHeaders = true) {
    let p = '';
    let row = [''];
    const ret = [row];
    let i = 0;
    let r = 0;
    let s = !0;
    let l;
    for (l of text) {
        if (l === '"') {
            if (s && l === p) {
                row[i] += l;
            }
            s = !s;
        } else if (l === ',' && s) {
            l = row[++i] = '';
        } else if (l === '\n' && s) {
            if (p === '\r') {
                row[i] = row[i].slice(0, -1);
            }
            row = ret[++r] = [(l = '')];
            i = 0;
        } else {
            row[i] += l;
        }
        p = l;
    }

    if (mergeHeaders) {
        const headers = ret[0].map((str) => str.trim());

        return ret.slice(1).reduce((acc, line) => {
            const obj = headers.reduce((lineAcc, header, k) => {
                lineAcc[header] = line[k];
                return lineAcc;
            }, {});

            acc.push(obj);

            return acc;
        }, []);
    }

    return ret;
}

export function truncate(str = '', maxLength = 10) {
    if (str.length <= maxLength) {
        return str;
    }

    return `${str.substr(0, maxLength)}…`;
}

export function pluralize(count, string, stringOnly = false) {
    let plural = 's';

    if (count === 1 || string.endsWith('s')) {
        plural = '';
    }

    const newString = `${string}${plural}`;

    if (stringOnly) {
        return newString;
    }

    return `${count} ${newString}`;
}
