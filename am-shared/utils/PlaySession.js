/**
 * Calculates total time played based on a play session of
 * timeupdates & seeking
 *
 * Maybe we want to add an .on(time, callback)
 * method that can fire when a timeupdate is reached
 * in order to clean up the play tracking calls.
 */
export default class PlaySession {
    constructor() {
        // Array of start and end time objects.
        // i.e (in the case of skipping forward)
        // [{start: 0, end: 10}, {start: 100, end: 140}]
        this._session = [];
        this._callbacks = {};
        this._isPlaying = false;
        this._isCollecting = true;
    }

    play(time) {
        if (this._isPlaying) {
            return Array.from(this._session);
        }

        this._isPlaying = true;

        const t = isNaN(time) ? 0 : time;

        this._session.push({
            start: t,
            end: t
        });

        return Array.from(this._session);
    }

    once(time, callback) {
        if (typeof this._callbacks[time] !== 'undefined') {
            console.warn(
                'Callback already assigned for time',
                time,
                'Call reset to use this time slot again.'
            );
            return;
        }

        this._callbacks[time] = callback;
    }

    timeupdate(time) {
        if (!this._isPlaying) {
            console.warn('Must call play before calling timeupdate');
            return Array.from(this._session);
        }

        if (!this._isCollecting) {
            return Array.from(this._session);
        }

        const lastSessionObj = this._session[this._session.length - 1];

        if (lastSessionObj) {
            lastSessionObj.end = time;
        }

        Object.keys(this._callbacks).forEach((t) => {
            const parsedTime = parseInt(t, 10);

            if (
                typeof this._callbacks[t] === 'function' &&
                this.calculateTime() >= parsedTime
            ) {
                this._callbacks[t](parsedTime);
                this._callbacks[t] = null;
            }
        });

        return Array.from(this._session);
    }

    seek(time) {
        if (!this._isPlaying) {
            console.warn('Must call play before calling seek');
            return Array.from(this._session);
        }

        if (!this._isCollecting) {
            return Array.from(this._session);
        }

        this._session.push({
            start: time,
            end: time
        });

        return Array.from(this._session);
    }

    startCollecting() {
        const lastSessionObj = this._session[this._session.length - 1];

        if (lastSessionObj) {
            this._session.push({
                start: lastSessionObj.end,
                end: lastSessionObj.end
            });
        }

        this._isCollecting = true;
    }

    stopCollecting() {
        this._isCollecting = false;
    }

    reset() {
        this._session = [];
        this._callbacks = [];
        this._isPlaying = false;

        return [];
    }

    calculateTime() {
        return this._session.reduce((sum, obj) => {
            return sum + obj.end - obj.start;
        }, 0);
    }
}
