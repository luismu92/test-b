export function canUserJoinCreatorAuthentication(
    currentUser,
    {
        playsRequired = process.env.ARTIST_VALIDATION_REQUIRED_TOTAL_PLAYS,
        uploadsRequired = process.env.ARTIST_VALIDATION_REQUIRED_MUSIC_UPLOADS
    } = {}
) {
    if (!currentUser.isLoggedIn || !currentUser.profile.stats) {
        return false;
    }

    const plays = currentUser.profile.stats['plays-raw'];
    const uploads = currentUser.profile.upload_count_excluding_reups;
    const playsRequiredNumber = parseInt(playsRequired, 10);
    const uploadsRequiredNumber = parseInt(uploadsRequired, 10);

    return plays >= playsRequiredNumber && uploads >= uploadsRequiredNumber;
}
