import { debounce } from './index';

export default class ShootingStar {
    constructor(
        container,
        { interval = 2000, vx = 7, vy = 3, radius = 4, radiusDelta = 0.06 } = {}
    ) {
        this.container =
            typeof container === 'string'
                ? document.querySelector(container)
                : container;
        this.container.innerHTML = '';

        const star = document.createElement('span');

        star.style.pointerEvents = 'none';
        star.style.position = 'absolute';
        star.style.top = 0;
        star.style.left = 0;

        this.container.appendChild(star);

        this.vx = vx;
        this.vy = vy;
        this.radius = radius;
        this.radiusDelta = radiusDelta;

        this.interval = interval;
        this.lastShootingStar = Date.now();
        this.nextInterval = this.getValue('interval');
        this.currentStar = star;
        this.currentStarData = null;

        this.resize();

        window.addEventListener('resize', this.handleWindowResize);

        this.start();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowResize = debounce(() => {
        this.resize();
    }, 250);

    ////////////////////
    // Helper methods //
    ////////////////////

    getValue(property) {
        let value = this[property];

        if (typeof this[property] === 'function') {
            value = this[property]();
        }

        return value;
    }

    resize() {
        this.style = window.getComputedStyle(this.container);
        this.width = parseInt(this.style.width.replace('px', ''), 10);
        this.height = parseInt(this.style.height.replace('px', ''), 10);
    }

    star() {
        return {
            x: Math.round(Math.random() * this.width),
            y: Math.round(Math.random() * this.height),
            r: this.getValue('radius'),
            radiusDelta: this.getValue('radiusDelta'),
            vx: this.getValue('vx'),
            vy: this.getValue('vy'),
            scale: 1
        };
    }

    draw(star) {
        this.currentStar.style.transform = `translate3d(${star.x}px, ${
            star.y
        }px, 0) scale(${star.scale})`;
    }

    start = () => {
        if (this.currentStarData) {
            const star = this.currentStarData;

            this.draw(star);

            if (star.scale <= 0) {
                this.currentStarData = null;
                this.lastShootingStar = Date.now();
                this.nextInterval = this.getValue('interval');
            } else {
                star.x += star.vx;
                star.y += star.vy;
                star.scale -= star.radiusDelta;
            }
        } else if (Date.now() - this.lastShootingStar >= this.nextInterval) {
            this.currentStarData = this.star();

            const { r: radius } = this.currentStarData;
            const fill = 'rgba(255, 255, 255, 1)';
            const transparent = 'rgba(255, 255, 255, 0)';

            this.currentStar.style.height = `${radius * 2}px`;
            this.currentStar.style.width = `${radius * 2}px`;

            this.currentStar.style.borderRadius = `${radius}px`;
            this.currentStar.style.backgroundImage = `radial-gradient(${fill} 0%, ${transparent} 50%, ${transparent} 100%)`;

            if (this.currentStarData.radiusDelta < 0) {
                throw new Error('Value of `radiusDelta` must be positive');
            }
        }

        this.frameId = window.requestAnimationFrame(this.start);
    };

    stop() {
        window.cancelAnimationFrame(this.frameId);
    }

    destroy() {
        this.stop();
        window.removeEventListener('resize', this.handleWindowResize);
    }
}
