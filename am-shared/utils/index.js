import React from 'react';
import { Link } from 'react-router-dom';
import {
    timeMap,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED
} from 'constants/index';
import { MAX_PINS } from 'constants/user/pinned';
import { parse, stringify } from 'query-string';

require('isomorphic-fetch');

// Since fetch doesn't fail based on status codes >= 300
// we need to have this wrapper that will cause consuming code to
// act appropriately when there is a bad status code.
export function request(url, data = {}, getRawResponse = false) {
    if (!url) {
        return Promise.reject(`Invalid url: ${url}`);
    }

    const { body, headers = {}, ...rest } = data;
    const defaultContentTypeHeader =
        'application/x-www-form-urlencoded; charset=utf-8';

    const allHeaders = Object.keys(headers).map((h) => h.toLowerCase());

    if (allHeaders.indexOf('content-type') === -1) {
        headers['content-type'] = defaultContentTypeHeader;
    }

    const options = {
        method: 'GET',
        // Comment this out so we dont send an extra preflight request
        headers,
        // headers: {
        //     'Content-Type': 'application/json'
        // },
        credentials: 'include',
        ...rest
    };
    const lowercase = options.method.toLowerCase();

    if (body && Object.keys(body).length) {
        if (lowercase !== 'get' && lowercase !== 'head') {
            options.body = JSON.stringify(body);

            if (options.headers['content-type'] === defaultContentTypeHeader) {
                options.body = buildQueryString(body, false);
            }
        } else {
            console.info('Ignoring body', body, 'because of request method');
        }
    }

    // Allow for relative urls to be fetched on server
    if (typeof window === 'undefined' && url.match(/^\/\w+/)) {
        url = `${process.env.AM_URL}${url}`;
    }

    const promise = fetch(url, options);

    if (getRawResponse) {
        return promise;
    }

    return promise
        .then((response) => {
            return Promise.all([
                response.status,
                // Not using response.json() here because some requests
                // like OPTIONS requests return a non JSON response. Maybe
                // if we wanted to make things cleaner here, we could check
                // the request headers for application/json and return
                // response.json() specifically for that case, otherwise .text()
                response.text()
            ]);
        })
        .then(([status, text]) => {
            let ret = text;

            try {
                if (
                    process.env.NODE_ENV === 'development' ||
                    process.env.NODE_ENV === 'test'
                ) {
                    // Log API notices to console
                    if (text.includes('Notice:')) {
                        const parts = text.split('\n').filter(Boolean);
                        const json = parts.filter(
                            (str) => !str.includes('Notice:')
                        );
                        const notices = parts.filter((str) =>
                            str.includes('Notice:')
                        );

                        ret = JSON.parse(json.join(''));

                        console.warn(notices.join('\n'));
                    } else if (text.includes('Warning:')) {
                        const parts = text.split('\n').filter(Boolean);
                        const json = parts.filter(
                            (str) => !str.includes('Warning:')
                        );
                        const warnings = parts.filter((str) =>
                            str.includes('Warning:')
                        );

                        ret = JSON.parse(json.join(''));

                        console.warn(warnings.join('\n'));
                    } else {
                        ret = JSON.parse(text);
                    }
                } else {
                    ret = JSON.parse(text);
                }
            } catch (e) {} // eslint-disable-line no-empty

            if (status >= 200 && status < 300) {
                return ret;
            }

            throw ret;
        });
}

export function headRequest(url, onlyReturnStatus = true) {
    const options = {
        headers: {
            method: 'HEAD'
        }
    };
    const rawResponse = true;

    const promise = request(url, options, rawResponse);

    if (onlyReturnStatus) {
        return promise.then((response) => {
            return response.status;
        });
    }

    return promise;
}

export function formatCurrency(val, { prefix = '$', decimals = 2 } = {}) {
    const rounder = Math.pow(10, decimals);
    const defaultZeros = new Array(decimals + 1).join('0');
    const formatted =
        (Math.round(val * rounder) / rounder).toLocaleString('en') ||
        `0.${defaultZeros}`;
    const actualDecimals = formatted.split('.')[1] || '';

    let decimalPoint = '';
    if (!actualDecimals.length) {
        decimalPoint = '.';
    }
    const zerosToAdd = new Array(decimals - actualDecimals.length + 1).join(
        '0'
    );

    return `${prefix}${formatted}${decimalPoint}${zerosToAdd}`;
}

export function parseRevenueString(val) {
    let value = 0;

    try {
        value = parseFloat(
            val
                .replace(/,/g, '')
                .toString()
                .match(/\d+.\d+/)[0]
        );
    } catch (err) {} // eslint-disable-line

    return value;
}

export function replaceLinkWithAnchor(text) {
    return text.replace(
        /\[(https?:\/\/[^\]\s]+)(?: ([^\]]*))?\]/g,
        '<a href="$1">$2</a>'
    );
}

export function clamp(val, min, max) {
    return Math.min(Math.max(val, min), max);
}

export const Support = {
    touch:
        typeof window !== 'undefined' &&
        ('ontouchstart' in window ||
            (window.DocumentTouch && document instanceof DocumentTouch)), // eslint-disable-line
    pageIsVisible() {
        let hidden;

        // Don't refresh ads if page is not active/visible
        // reference https://developer.mozilla.org/en-US/docs/Web/API/Page_Visibility_API
        // @TODO perhaps replace this logic with https://www.npmjs.com/package/react-page-visibility
        if (typeof window.document.hidden !== 'undefined') {
            // Opera 12.10 and Firefox 18 and later support
            hidden = 'hidden';
        } else if (typeof window.document.msHidden !== 'undefined') {
            hidden = 'msHidden';
        } else if (typeof window.document.webkitHidden !== 'undefined') {
            hidden = 'webkitHidden';
        }

        return !window.document[hidden];
    },
    preloadSupported() {
        const link = document.createElement('link');
        const relList = link.relList;

        if (!relList || !relList.supports) {
            return false;
        }

        return relList.supports('preload');
    }
};

export function absoluteUrl(url = '', host = process.env.AM_URL) {
    if (url && url.startsWith('/')) {
        return `${host}${url}`;
    }
    return url;
}

export function loadScript(src, id, appendTo) {
    if (!src || !id) {
        throw new Error('Must provide a src to load and an id');
    }

    if (!appendTo) {
        appendTo = document.body;
    }

    return new Promise((resolve, reject) => {
        let script = document.getElementById(id);
        let counter = 40;

        // Set a timer because if we're here, that means another script has
        // already loaded this script src but it hasnt loaded yet. In that
        // case we need to wait for data-loaded to be set before calling
        // the scripLoaded callback
        const checkLoaded = () => {
            // eslint-disable-line
            counter -= 1;

            // Just reject if it's been about 40 seconds with no load
            if (counter === 0) {
                reject(`Could not load ${src} after 40 seconds`);
                return;
            }

            if (script.getAttribute('data-loaded')) {
                resolve({
                    firstLoad: false
                });
                return;
            }

            setTimeout(checkLoaded, 1000);
        };

        if (script) {
            checkLoaded();
            return;
        }

        script = document.createElement('script');

        script.id = id;
        script.onload = () => {
            script.setAttribute('data-loaded', true);
            resolve({
                firstLoad: true
            });
        };
        script.onerror = reject;
        script.src = src;

        appendTo.appendChild(script);
    });
}

export function loadFacebookSdk({ login = true, getFriends = false } = {}) {
    const src = '//connect.facebook.net/en_US/sdk.js';
    const id = 'facebook-jssdk';

    return loadScript(src, id).then(() => {
        window.FB.init({
            appId: process.env.FACEBOOK_APP_ID,
            cookie: true,
            xfbml: true,
            version: 'v3.1',
            status: true
        });

        const scope = ['email'];

        if (login || getFriends) {
            return new Promise((resolve, reject) => {
                if (getFriends) {
                    scope.push(
                        'public_profile',
                        'user_friends',
                        'user_likes',
                        'user_link'
                    );
                }

                window.FB.login(
                    (loginResponse) => {
                        if (!loginResponse || !loginResponse.authResponse) {
                            reject(
                                new Error(
                                    'User cancelled login or did not fully authorize.'
                                )
                            );
                            return;
                        }

                        if (getFriends) {
                            window.FB.api('/me/friends', (friendResponse) => {
                                if (!friendResponse || friendResponse.error) {
                                    reject(
                                        new Error(
                                            'Error retrieving user friends.'
                                        )
                                    );
                                    return;
                                }

                                resolve({
                                    login: loginResponse,
                                    friends: friendResponse
                                });
                            });
                            return;
                        }

                        resolve({ login: loginResponse });
                    },
                    {
                        scope: scope.join(',')
                    }
                );
            });
        }

        return undefined;
    });
}

export function loadSignInWithApple() {
    const src =
        'https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js';
    const id = 'apple-auth';

    return loadScript(src, id).then(() => {
        window.AppleID.auth.init({
            clientId: process.env.APPLE_CLIENT_ID,
            redirectURI: process.env.APPLE_REDIRECT_URI,
            scope: 'email',
            usePopup: true
        });

        return undefined;
    });
}

// Converts should-be-number strings
export function parseApiObject(apiObject) {
    const obj = {
        ...apiObject
    };
    const keysToUpdate = [
        'id',
        'user_id',
        'created',
        'uploader.id',
        'uploader.created',
        'uploader.updated',
        'released',
        'duration',
        'updated',

        // User object
        'favorite_music',
        'favorite_playlists',
        'followers_count',
        'following',
        'playlists',
        'reups',
        'new_feed_items',

        // Search results
        'song_id',
        'song_count',
        'album_count',
        'artist_count',
        'music_count',
        'playlist_count',

        'artist_id',
        'artist.id',
        'artist.created',
        'artist.updated',
        'uploaded',
        'accounting_code_id',

        'album_details.id'
    ];

    return keysToUpdate.reduce((acc, key) => {
        const parts = key.split('.');

        let schema = acc;
        let keyToUpdate = parts[0];

        for (let i = 0, len = parts.length; i < len - 1; i++) {
            if (typeof schema[keyToUpdate] !== 'undefined') {
                schema = schema[keyToUpdate];
                keyToUpdate = parts[i + 1];
            }
        }

        if (schema && typeof schema[keyToUpdate] !== 'undefined') {
            if (Array.isArray(schema[keyToUpdate])) {
                schema[keyToUpdate] = schema[keyToUpdate].map((item) => {
                    if (typeof item === 'string') {
                        return strToNumber(item || 0);
                    }
                    return item;
                });
            } else {
                schema[keyToUpdate] = strToNumber(schema[keyToUpdate] || 0);
            }
        }

        return acc;
    }, obj);
}

export function getUploader(musicItem) {
    if (musicItem.type === 'playlist') {
        return musicItem.artist;
    }

    return musicItem.uploader;
}

function getFeaturingArrayFromString(featureString) {
    const trimmed = featureString
        .split(',')
        .map((feature) => {
            return feature.trim();
        })
        .filter(Boolean);
    const uniqueFeatures = new Set(trimmed);
    const featuringArray = Array.from(uniqueFeatures);

    return featuringArray;
}

export function getFeaturing(musicItem, { asArray = false } = {}) {
    if (!musicItem) {
        if (asArray) {
            return [];
        }

        return '';
    }

    let featuring = musicItem.featuring || '';

    if (musicItem.type === 'album') {
        featuring = (musicItem.tracks || [])
            .map((track) => {
                return track.featuring;
            })
            .join(',');
    }

    const featuringArray = getFeaturingArrayFromString(featuring);

    if (asArray) {
        return featuringArray;
    }

    return featuringArray.join(', ');
}

export function getArtistName(musicItem) {
    if (musicItem.type === 'playlist') {
        return musicItem.artist.name;
    }

    return musicItem.artist;
}

export function ownsMusic(currentUser, musicItem) {
    if (!currentUser || !currentUser.isLoggedIn || !musicItem) {
        return false;
    }

    if (
        musicItem.parentDetails &&
        musicItem.parentDetails.uploader &&
        musicItem.parentDetails.type === 'album'
    ) {
        return musicItem.parentDetails.uploader.id === currentUser.profile.id;
    }

    if (
        (musicItem.type === 'song' || musicItem.type === 'album') &&
        musicItem.uploader.id === currentUser.profile.id
    ) {
        return true;
    }

    if (
        musicItem.type === 'playlist' &&
        currentUser.profile.id === musicItem.artist.id
    ) {
        return true;
    }

    if (
        currentUser.profile.label_owner &&
        currentUser.profile.label_id === musicItem.label_id
    ) {
        return true;
    }

    return false;
}

export function artistComment(comment, music) {
    const uploaderId =
        music.type === 'playlist' ? music.artist.id : music.uploader.id;

    if (comment.artist.artist_id === uploaderId) {
        return true;
    }

    return false;
}

export function hasMaxPins(currentUserPinned) {
    if (currentUserPinned.list.length < MAX_PINS) {
        return false;
    }

    return true;
}

export function formatBytes(bytes) {
    const units = ['B', 'KB', 'MB', 'GB', 'TB'];
    let i = 0;

    for (i; bytes >= 1024 && i < units.length - 1; i++) {
        bytes /= 1024;
    }

    return bytes.toFixed(2) + units[i];
}

export function renderCappedDisplayCount(
    number,
    { max = 99, after = '+' } = {}
) {
    const parsed = parseInt(number, 10);
    const parsedMax = parseInt(max, 10);
    const parsedAfter = after === null ? '' : after;

    if (isNaN(parsed) || parsed <= 0) {
        return null;
    }

    if (parsed > parsedMax) {
        return `${parsedMax}${parsedAfter}`;
    }

    return parsed;
}

/**
 * Normalizes a song object provided by the API. For example, soundcloud
 * streaming urls need to be requested and replaced so they can play in
 * an audio tag
 *
 * @param  {Object} musicItem Song/Album/Playlist object returned from AM API
 * @return {Promise} promise resolving with the normalized song object
 */
export function normalizeMusicItem(musicItem) {
    return new Promise((resolve, reject) => {
        const streamingUrl = musicItem.streaming_url || '';

        if (streamingUrl.match(/^https?:\/\/(www\.)?soundcloud\.com/)) {
            const consumerKey = 'e8d4a4460406f85186559062901c8a33';
            // const url = `https://api.soundcloud.com/resolve?url=${streamingUrl}&format=json&consumer_key=${consumerKey}`;
            const url = `/soundcloud?streamingUrl=${streamingUrl}&artistSlug=${
                getUploader(musicItem).url_slug
            }&songSlug=${musicItem.url_slug}`;

            request(url)
                .then((responseTrack) => {
                    let streamUrl = responseTrack.stream_url;

                    if (streamUrl.includes('?')) {
                        streamUrl = `${streamUrl}&`;
                    } else {
                        streamUrl = `${streamUrl}?`;
                    }

                    streamUrl = `${streamUrl}consumer_key=${consumerKey}`;

                    musicItem.streaming_url = streamUrl; // eslint-disable-line

                    return resolve(musicItem);
                })
                .catch(reject);
            return;
        }

        // https://github.com/audiomack/audiomack-js/issues/122
        // This is coming in as false or not at all instead of an empty string for some reason
        musicItem.streaming_url = musicItem.streaming_url || ''; // eslint-disable-line
        musicItem.normalized = true;

        resolve(musicItem);
    });
}

export function parametize(data) {
    return Object.keys(data)
        .map(function(keyName) {
            return `${encodeURIComponent(
                keyName
            )}=${encodeURIComponent(data[keyName])}`;
        })
        .join('&');
}

export function formData(data) {
    const form = new FormData();

    Object.keys(data).forEach((key) => {
        const value = data[key];

        form.append(key, value);
    });

    return form;
}

// Taken from https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
// Edited to not be mutating
export function shuffle(arr = []) {
    if (arr.length < 2) {
        return arr;
    }

    const a = Array.from(arr);

    for (let i = a.length; i; i--) {
        const j = Math.floor(Math.random() * i);

        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }

    return a;
}

export function convertTimeStringToSeconds(timestring = '') {
    const matches = timestring.toString().match(/((\d+)h)?((\d+)m)?((\d+)s?)?/);

    if (!matches) {
        return 0;
    }

    const [, , hourString, , minuteString, , secondString] = matches;
    const hours = parseInt(hourString, 10) || 0;
    const minutes = parseInt(minuteString, 10) || 0;
    const seconds = parseInt(secondString, 10) || 0;

    return hours * 3600 + minutes * 60 + seconds;
}

export function convertSecondsToTimeString(num = 0) {
    const parsed = parseInt(num, 10);

    if (isNaN(parsed)) {
        return '';
    }

    const hours = Math.floor(num / 3600);
    let minutes = Math.floor(num / 60);
    const seconds = Math.floor(num % 60);

    if (hours > 0) {
        minutes = Math.floor((num / 60) % 60);
    }

    let string = '';

    if (hours) {
        string += `${hours}h`;
    }

    if (minutes) {
        string += `${minutes}m`;
    }

    if (seconds) {
        string += `${seconds}s`;
    }

    return string;
}

export function convertSecondsToTimecode(num) {
    num = parseInt(num, 10);

    if (num === 0 || isNaN(num)) {
        return '0:00';
    }

    const format = 'hh:mm:ss';
    const formattedTime = [];
    const parts = format.split(':');
    let hours;
    let minutes;
    let seconds;

    parts.forEach((part) => {
        switch (part) {
            case 'hh':
                hours = Math.floor(num / 3600);

                if (hours) {
                    formattedTime.push(hours);
                }
                break;

            case 'mm':
                minutes = hours
                    ? Math.floor((num / 60) % 60)
                    : Math.floor(num / 60);
                if (minutes < 10 && hours) {
                    minutes = `0${minutes}`;
                }

                formattedTime.push(minutes);
                break;

            case 'ss':
                seconds = Math.floor(num % 60);
                if (seconds < 10) {
                    seconds = `0${seconds}`;
                }
                formattedTime.push(seconds);
                break;

            default:
                formattedTime.push('');
                break;
        }
    });

    return formattedTime.join(':');
}

export function suffix(action, suf, separator = '_') {
    return `${action}${separator}${suf}`;
}

export function requestSuffix(action) {
    return suffix(action, 'REQUEST');
}

export function failSuffix(action) {
    return suffix(action, 'FAILURE');
}

export function strToNumber(str) {
    return parseInt(str, 10);
}

export function renderFeaturingLinks(
    featuring,
    { featText = 'Feat. ', removeLinks = false, target = null } = {}
) {
    if (!featuring) {
        return null;
    }

    const list = getFeaturingArrayFromString(featuring).map(
        (item, i, items) => {
            const feature = item.trim();
            let comma = '';

            if (i !== items.length - 1) {
                comma = ', ';
            }

            if (items.length >= 2 && i === items.length - 2) {
                comma = ' & ';
            }

            if (removeLinks) {
                return (
                    <span key={i}>
                        {feature}
                        {comma}
                    </span>
                );
            }

            return (
                <span key={i}>
                    <Link
                        to={`/search?q=${encodeURIComponent(feature)}`}
                        target={target}
                    >
                        {feature}
                    </Link>
                    {comma}
                </span>
            );
        }
    );

    return [<span key="feat">{featText}</span>].concat(list);
}

// Might have to make this a little more robust in the future
export function getRouteByPathname(pathname = '', routes = []) {
    return Array.from(routes)
        .reverse()
        .find((route) => pathname.indexOf(route.path) === 0);
}

/**
 * Checks whether or not the currentSong is "active"
 *
 * @param  {Object}  currentSong The normalized/flattened song/album track/playlist track. @see getFlattendedQueueFromApiItems below
 * @param  {Object}  musicInfo   The api response object for the song/album/playlist or normalized/flattened song/album track/playlist track as mentioned above.
 * @param  {Boolean} allowTrack  Return true if comparing an album track/playlist track to an actual album/playlist api response object
 *
 * @return {Boolean}
 */
export function isCurrentMusicItem(currentSong, musicInfo, allowTrack = true) {
    if (!currentSong || !musicInfo) {
        return false;
    }

    if (currentSong === musicInfo) {
        return true;
    }

    if (currentSong.parentDetails && musicInfo.parentDetails) {
        return (
            currentSong.id === musicInfo.id &&
            currentSong.parentDetails.type === musicInfo.parentDetails.type &&
            currentSong.parentDetails.id === musicInfo.parentDetails.id
        );
    }

    if (
        currentSong.parentDetails &&
        currentSong.parentDetails.type === musicInfo.type &&
        currentSong.parentDetails.id === musicInfo.id &&
        allowTrack
    ) {
        return true;
    }

    return currentSong.id === musicInfo.id;
}

export function getNormalizedQueueTrack(item, trackIndex) {
    if (!item || !item.tracks || !item.tracks[trackIndex]) {
        return item;
    }

    const uploader = getUploader(item) || {};
    const track = item.tracks[trackIndex];
    // Gets a reference to the album/playlist id this came from.
    const normalizedTrack = {
        ...track,
        id: track.song_id || track.id,
        trackIndex: trackIndex,
        image: track.image || item.image,
        image_base: track.image_base || item.image_base,
        released: track.released || item.released,
        type: track.type || 'song'
    };

    if (item.type === 'album' || item.type === 'playlist') {
        const existingDetails = normalizedTrack.parentDetails || {};

        normalizedTrack.parentDetails = {
            // eslint-disable-line
            ...existingDetails,
            title: item.title,
            id: item.id,
            url_slug: item.url_slug,
            type: item.type
        };

        // Keep similar markup as API response objects
        if (item.type === 'album') {
            normalizedTrack.parentDetails.uploader = {
                url_slug: uploader.url_slug,
                id: uploader.id,
                name: uploader.name,
                image: uploader.image,
                image_base: uploader.image_base
            };
        }

        if (item.type === 'playlist') {
            normalizedTrack.parentDetails.artist = {
                url_slug: uploader.url_slug,
                id: uploader.id,
                name: uploader.name,
                image: uploader.image,
                image_base: uploader.image_base
            };
        }
    }

    return normalizedTrack;
}

export function getFlattendedQueueFromApiItems(musicItems = []) {
    return musicItems.reduce((acc, item) => {
        if (item.tracks && item.tracks.length) {
            const normalizedTracks = item.tracks.map((track, i) => {
                return getNormalizedQueueTrack(item, i);
            });

            acc = acc.concat(normalizedTracks);
        } else if (item) {
            acc.push(item);
        }

        return acc;
    }, []);
}

export function getQueueIndexForSong(
    musicItem,
    queue,
    { trackIndex = null, currentQueueIndex = -1, ignoreHistory = true } = {}
) {
    const itemToFind = musicItem.tracks
        ? { ...musicItem.tracks[trackIndex || 0] }
        : { ...musicItem };

    if (typeof itemToFind.song_id !== 'undefined') {
        itemToFind.id = itemToFind.song_id;
    }

    const midPoint = currentQueueIndex + 1;

    if (midPoint === 0) {
        return queue.findIndex((item) => item.id === itemToFind.id);
    }

    // Look forward first after currentTime
    const secondPart = queue.slice(midPoint);
    const secondSearch = secondPart.findIndex((item) => {
        return item.id === itemToFind.id;
    });

    if (secondSearch !== -1) {
        return secondSearch + midPoint;
    }

    if (ignoreHistory) {
        return -1;
    }

    // Look behind up to current item
    const firstPart = queue.slice(0, midPoint);
    const firstPartIds = firstPart.map((item) => {
        return item.id;
    });

    return firstPartIds.lastIndexOf(itemToFind.id);
}

export function queueHasMusicItem(queue, musicItem, currentQueueIndex = 0) {
    let trackIds = [musicItem.song_id || musicItem.id];

    if (musicItem.tracks) {
        trackIds = musicItem.tracks.map((track) => {
            return track.song_id || track.id;
        });
    }

    const searchQueue = queue.slice(currentQueueIndex);
    const queueIds = searchQueue.map((track) => {
        return track.id;
    });

    return trackIds.every((id) => {
        return queueIds.includes(id);
    });
}

export function isDownloadableTrack(track) {
    if (!track.download_url) {
        return false;
    }

    if (yesBool(track.stream_only)) {
        return false;
    }

    if (track.is_soundcloud) {
        return false;
    }

    if (yesBool(track.album_track_only)) {
        return false;
    }

    if (yesBool(track.follow_download)) {
        return false;
    }

    return true;
}

export function cleanParameters(obj, removeFalsyValues = false) {
    return Object.keys(obj).reduce((acc, key) => {
        if (typeof obj[key] !== 'undefined' && obj[key] !== null) {
            if (!removeFalsyValues || (removeFalsyValues && !!obj[key])) {
                acc[key] = obj[key];
            }
        }

        return acc;
    }, {});
}

export function round(num, decimals = 2) {
    const parsed = parseFloat(num);

    if (isNaN(parsed)) {
        return num;
    }

    const multiplier = Math.pow(10, decimals);

    return Math.round(num * multiplier) / multiplier;
}

export function tidyNumber(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function parseHumanNumber(num) {
    const denoms = ['K', 'M', 'B'];
    let intermediate = num;

    denoms.some((letter, i) => {
        const hasLetter = intermediate.indexOf(letter) !== -1;
        const isInvalid = intermediate.indexOf('1000') !== -1;
        const nextLetter = denoms[i + 1];

        if (hasLetter && nextLetter && isInvalid) {
            intermediate = `1${nextLetter}`;
            return true;
        }

        return false;
    });

    return intermediate.replace(/\.0{1,}(\w)$/, (match, denom) => {
        return denom;
    });
}

export function humanizeNumber(num) {
    if (!num || isNaN(num)) {
        return '0';
    }

    if (num < 1000) {
        return `${num}`;
    }

    if (num < 10000) {
        return parseHumanNumber(`${(num / 1000).toFixed(2)}K`);
    }

    if (num < 100000) {
        return parseHumanNumber(`${(num / 1000).toFixed(1)}K`);
    }

    if (num < 1000000) {
        return parseHumanNumber(`${(num / 1000).toFixed(0)}K`);
    }

    if (num < 10000000) {
        return parseHumanNumber(`${(num / 1000000).toFixed(2)}M`);
    }

    if (num < 100000000) {
        return parseHumanNumber(`${(num / 1000000).toFixed(1)}M`);
    }

    if (num < 1000000000) {
        return parseHumanNumber(`${(num / 1000000).toFixed(0)}M`);
    }

    if (num < 10000000000) {
        return parseHumanNumber(`${(num / 1000000000).toFixed(2)}B`);
    }

    return parseHumanNumber(`${(num / 1000000000).toFixed(0)}B`);
}

export function getEmbedUrl(musicItem, queryOptions = {}) {
    const uploaderItem =
        musicItem.parentDetails && musicItem.parentDetails.type === 'album'
            ? musicItem.parentDetails
            : musicItem;
    const artistSlug = getUploader(uploaderItem).url_slug;
    const musicSlug = musicItem.url_slug;
    let qs = buildQueryString(queryOptions);

    if (qs) {
        qs = `?${qs}`;
    }

    return `${process.env.AM_URL}/embed/${
        musicItem.type
    }/${artistSlug}/${musicSlug}${qs}`;
}

export function getIframe(
    type,
    artistSlug,
    musicSlug,
    width = '100%',
    height = 252
) {
    // eslint-disable-line max-params
    return `<iframe src="${
        process.env.AM_URL
    }/embed/${type}/${artistSlug}/${musicSlug}" scrolling="no" width="${width}" height="${height}" scrollbars="no" frameborder="0"></iframe>`;
}

export function getIframeString(
    musicItem,
    { height, width = '100%', queryOptions = {} } = {}
) {
    if (!height) {
        height = 252;

        if (musicItem.type === 'album' || musicItem.type === 'playlist') {
            height = 400;
        }
    }

    return `<iframe src="${getEmbedUrl(
        musicItem,
        queryOptions
    )}" scrolling="no" width="${width}" height="${height}" scrollbars="no" frameborder="0"></iframe>`;
}

export function getWordpressShortCode(musicItem, queryOptions = {}) {
    const options = {
        ...queryOptions,
        src: getEmbedUrl(musicItem)
    };
    const optionString = Object.keys(options).reduce((str, key) => {
        if (options[key]) {
            str += ` ${key}="${options[key]}"`;
        }

        return str;
    }, '');

    return `[audiomack${optionString}]`;
}

export function buildSearchUrl(
    query,
    { page, limit, type, context, genre, verified } = {}
) {
    const queryString = buildQueryString({
        q: (query || '').trim(),
        page: page,
        limit: limit,
        show: type,
        sort: context,
        genre: genre,
        verified: verified ? 'on' : null
    });

    return `/search?${queryString}`;
}

// generate RFC4122-compliant UUID
export function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        /* eslint no-bitwise: ["error", { "allow": ["|","&"] }] */
        const r = (Math.random() * 16) | 0;
        const v = c === 'x' ? r : (r & 0x3) | 0x8;

        return v.toString(16);
    });
}

export function debounce(fn, wait = 100) {
    let timer;

    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(fn, args);
        }, wait);

        return timer;
    };
}

// https://stackoverflow.com/a/27078401/1048847
export function throttle(func, wait, options) {
    let context;
    let args;
    let result;
    let timeout = null;
    let previous = 0;

    if (!options) {
        options = {};
    }

    const later = function() {
        // eslint-disable-line
        previous = options.leading === false ? 0 : Date.now();
        timeout = null;
        result = func.apply(context, args);

        if (!timeout) {
            context = null;
            args = null;
        }
    };

    return function() {
        const now = Date.now();

        if (!previous && options.leading === false) {
            previous = now;
        }
        const remaining = wait - (now - previous);

        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };
}

export function handlePlaywirePageChange(prevProps, currentProps) {
    if (
        typeof window.tyche === 'undefined' ||
        typeof window.tyche.changePath !== 'function'
    ) {
        return;
    }

    const { location, currentUser, mobile: isMobile } = currentProps;
    const currentLocation = location.pathname;
    const prevLocation = prevProps.location.pathname;
    const shouldNotShow = shouldNotShowAd(
        currentLocation,
        currentUser,
        isMobile
    );

    if (
        shouldNotShow !== shouldNotShowAd(prevLocation, currentUser, isMobile)
    ) {
        if (shouldNotShow) {
            window.tyche.changePath('no-ads');
            const loadedAds = document.querySelectorAll(
                "div[data-pw-status='loaded']"
            );
            Array.from(loadedAds).forEach((el) => {
                el.parentElement.removeChild(el);
            });
            window.console.warn('Hiding ads');
        } else {
            window.console.warn('Showing ads');
            window.tyche.changePath('ROS');
        }
    }
}

export function generateKeywords(musicItem) {
    const keywords = [
        musicItem.genre,
        getArtistName(musicItem),
        musicItem.title,
        musicItem.type
    ];

    if (musicItem.tracks && musicItem.tracks.length) {
        musicItem.tracks.forEach((track) => {
            keywords.push(track.title);
        });
    }

    return keywords
        .map((keyword) => {
            if (!keyword || !keyword.replace) {
                return null;
            }

            return keyword.replace(/,/g, ' ');
        })
        .filter(Boolean);
}

export function generateISODuration(duration) {
    const time = convertSecondsToTimecode(duration);
    const parts = time.split(':');

    return `PT${parts[parts.length - 2]}M${parts[parts.length - 1]}S`;
}

export function parseSongUrl(url) {
    const songsRegex = /^\/song\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
    const matches = url.match(songsRegex);

    if (!matches) {
        return null;
    }

    return {
        artistSlug: matches[1],
        musicSlug: matches[2]
    };
}

export function parseAlbumUrl(url) {
    const albumsRegex = /^\/album\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
    const matches = url.match(albumsRegex);

    if (!matches) {
        return null;
    }

    return {
        artistSlug: matches[1],
        musicSlug: matches[2]
    };
}

export function parseArtistUrl(url) {
    const artistRegex = /^\/artist\/([-_a-zA-Z0-9.]+)/i;
    const matches = url.match(artistRegex);

    if (!matches) {
        return null;
    }

    return {
        artistSlug: matches[1]
    };
}

export function pad(number, width, padWith = '0') {
    number = `${number}`;

    return number.length >= width
        ? number
        : new Array(width - number.length + 1).join(padWith) + number;
}

export function renderTrackTitle(trackItem, currentUser = {}) {
    if (trackItem.title) {
        return trackItem.title;
    }

    if (currentUser.profile && trackItem.streaming_url) {
        return trackItem.streaming_url
            .split(currentUser.profile.url_slug)[1]
            .split('?')[0]
            .replace(/\//g, '');
    }

    return 'Untitled track';
}

export function removeAudioExtension(str) {
    return str.replace(/\.(mp3|wav|aiff|ogg|m4a)$/i, '');
}

export function convertStringToUrl(str, { removeExtension = false } = {}) {
    if (removeExtension) {
        str = removeAudioExtension(str);
    }

    return str
        .replace(/\s+/g, '-')
        .match(/[a-zA-Z0-9-]*/g)
        .filter(Boolean)
        .join('-')
        .toLowerCase()
        .replace(/-+/g, '-');
}

export function getMusicUrl(
    musicItem,
    {
        host = '',
        encoded = false,
        embed = false,
        oembed = false,
        query = {}
    } = {}
) {
    let embedSuffix = '';
    let artistSlug;

    if (embed && !oembed) {
        embedSuffix = '/embed';
    }

    // Check if a queue item
    if (
        musicItem.parentDetails &&
        musicItem.parentDetails.type !== 'playlist'
    ) {
        if (musicItem.parentDetails.artist) {
            artistSlug = musicItem.parentDetails.artist.url_slug;
        } else {
            artistSlug = musicItem.parentDetails.uploader.url_slug;
        }
    } else {
        artistSlug =
            musicItem.type === 'playlist'
                ? musicItem.artist.url_slug
                : musicItem.uploader.url_slug;
    }

    if (oembed && !host) {
        throw new Error('Must provide a host when using oembed option');
    }

    let sourceUrl = `${host}${embedSuffix}/${musicItem.type}/${artistSlug}/${
        musicItem.url_slug
    }`;

    if (Object.keys(query).length) {
        sourceUrl += `?${buildQueryString(query)}`;
    }

    if (oembed) {
        return `${host}/oembed?url=${encodeURIComponent(sourceUrl)}`;
    }

    if (encoded) {
        return encodeURIComponent(sourceUrl);
    }

    return sourceUrl;
}

export function getPodcastUrl(artistSlug) {
    return `${process.env.AM_URL}/rss/${artistSlug}/podcast.rss`;
}

export function getChartUrl({
    host = '',
    genre = '',
    context = '',
    timePeriod = '',
    page = 1
} = {}) {
    let chartUrl;
    let genrePrefix = '';
    let periodText = '';

    if (genre) {
        genrePrefix = `${genre}/`;
    }

    if (timeMap[timePeriod]) {
        periodText = `/${timeMap[timePeriod]}`;
    }

    switch (context) {
        case COLLECTION_TYPE_SONG:
        case COLLECTION_TYPE_ALBUM: {
            chartUrl = `${host}/${genrePrefix}${context}${periodText}`;
            break;
        }

        case COLLECTION_TYPE_TRENDING: {
            chartUrl = `${host}/trending-now`;
            break;
        }

        case COLLECTION_TYPE_RECENTLY_ADDED: {
            chartUrl = `${host}/${context}`;
            break;
        }

        default: {
            console.log(context, 'No Context Found to match');
            return null;
        }
    }

    if (page > 1) {
        chartUrl = `${chartUrl}/page/${page}`;
    }

    return chartUrl;
}

export function getBranchUrl(host = '', query = {}) {
    const map = {
        deeplinkPath: '$deeplink_path',
        channel: '~channel',
        feature: '~feature',
        tags: '~tags',
        campaign: '~campaign',
        stage: '~stage',
        creationSource: '~creation_source',
        id: '~id'
    };
    const replacedQuery = Object.keys(query).reduce((obj, key) => {
        let actualKey = key;

        if (map[key]) {
            actualKey = map[key];
        }

        obj[actualKey] = query[key];

        return obj;
    }, {});
    const queryString = buildQueryString(replacedQuery);

    return `${host}?${queryString}`;
}

export function getArtistUrl(musicItem, { host = '' } = {}) {
    return `${host}/artist/${getUploader(musicItem).url_slug}`;
}

export function getArtistProfileImage(artist) {
    if (!artist) {
        return null;
    }

    return artist.image_base || artist.images.original.filenamae;
}

export function getTwitterShareLink(hostUrl, musicItem) {
    const encoded = true;
    const sourceUrl = getMusicUrl(musicItem, {
        host: hostUrl,
        encoded
    });
    const feat = musicItem.featuring ? ` feat. ${musicItem.featuring}` : '';
    const sourceTextTwitter = encodeURIComponent(
        `#nowplaying ${musicItem.title} by ${getArtistName(
            musicItem
        )}${feat} via @audiomack`
    );

    return `https://twitter.com/share?url=${sourceUrl}&text=${sourceTextTwitter}`;
}

export function getFacebookShareLink(hostUrl, musicItem, appId) {
    const encoded = true;
    const sourceUrl = getMusicUrl(musicItem, {
        host: hostUrl,
        encoded
    });
    const itemImage = encodeURIComponent(musicItem.image);
    const feat = musicItem.featuring ? ` feat. ${musicItem.featuring}` : '';
    const sourceTextFacebook = encodeURIComponent(
        `${musicItem.title} by ${getArtistName(
            musicItem
        )}${feat} via @audiomack`
    );

    return `https://www.facebook.com/dialog/feed?app_id=${appId}&link=${sourceUrl}&picture=${itemImage}&name=Now%20Playing&caption=%20&description=${sourceTextFacebook}`;
}

export function nativeShare(musicItem) {
    const feat = musicItem.featuring ? ` feat. ${musicItem.featuring}` : '';

    window.navigator
        .share({
            title: musicItem.title,
            text: `#nowplaying ${musicItem.title} by ${getArtistName(
                musicItem
            )}${feat} via @audiomack`,
            url: getMusicUrl(musicItem, {
                host: process.env.AM_URL
            })
        })
        .then(() => {
            return console.log('Successful share');
        })
        .catch((error) => {
            console.log('Error sharing', error);
        });
}

export function previewImage(
    img,
    { maxSize = 1600, returnContext = false, mimeType = 'image/jpeg' } = {}
) {
    return new Promise((resolve, reject) => {
        const image = new Image();
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        image.crossOrigin = 'Anonymous';

        image.onload = () => {
            let height = image.naturalHeight;
            let width = image.naturalWidth;

            if (height > maxSize) {
                width = (width / height) * maxSize;
                height = maxSize;
            }

            if (width > maxSize) {
                height = (height / width) * maxSize;
                width = maxSize;
            }

            canvas.height = height;
            canvas.width = width;

            ctx.drawImage(image, 0, 0, width, height);

            if (returnContext) {
                resolve(ctx);
                return;
            }

            const resizedImage = canvas.toDataURL(mimeType);

            resolve(resizedImage);
        };

        image.onerror = reject;

        if (img instanceof Image) {
            image.src = img.src;
            return;
        }

        if (typeof img === 'string') {
            image.src = img;
            return;
        }

        reject('First argument should be an image object or image src');
    });
}

export function previewFile(
    file,
    { maxSize = 1600, keepImageType = false } = {}
) {
    const reader = new FileReader();

    return new Promise((resolve, reject) => {
        reader.addEventListener(
            'load',
            () => {
                if (file.type && file.type.match(/^image\//) && maxSize) {
                    const extension = keepImageType ? file.type : 'image/jpeg';

                    previewImage(reader.result, {
                        maxSize,
                        mimeType: extension
                    })
                        .then(resolve)
                        .catch(reject);
                    return;
                }

                resolve(reader.result);
            },
            false
        );

        reader.readAsDataURL(file);
    });
}

export function isEmbedUrl(path) {
    return (
        path.indexOf('/embed/') === 0 ||
        path.indexOf('/embed4/') === 0 ||
        path.indexOf('/embed4-thin/') === 0 ||
        path.indexOf('/embed4-large/') === 0 ||
        path.indexOf('/embed4-album/') === 0 ||
        path.indexOf('/embed4-playlist/') === 0
    );
}

export function easeOutSine(pos) {
    return Math.sin(pos * (Math.PI / 2));
}

// https://stackoverflow.com/a/26808520/1048847
export function smoothScroll(
    scrollTargetY = 0,
    { speed = 3000, callback } = {}
) {
    // scrollTargetY: the target scrollY property of the window
    // speed: time in pixels per second
    // easing: easing equation to use

    const scrollY = window.scrollY || document.documentElement.scrollTop;
    let currentTime = 0;

    // min time .1, max time .8 seconds
    const time = Math.max(
        0.1,
        Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8)
    );

    // add animation loop
    function tick() {
        currentTime += 1 / 60;

        const p = currentTime / time;
        const t = easeOutSine(p);

        if (p < 1) {
            window.requestAnimationFrame(tick);
            window.scrollTo(0, scrollY + (scrollTargetY - scrollY) * t);
            return;
        }

        // Done
        window.scrollTo(0, scrollTargetY);

        if (typeof callback === 'function') {
            callback();
            return;
        }
    }

    // call it once to get started
    tick();
}

export function ucfirst(str) {
    return `${str.charAt(0).toUpperCase()}${str.substring(1)}`;
}

export function getUploadStatusData(status, enumObj) {
    let klass = 'u-text-orange';
    let text = 'Submitting...';

    switch (status) {
        case enumObj.CANCELED:
            text = 'Canceled';
            klass = 'u-text-red';
            break;

        case enumObj.PROCESSING:
            text = 'Processing...';
            break;

        case enumObj.UPLOADING:
            text = 'Uploading...';
            break;

        case enumObj.FAILED:
            text = 'Upload Failed';
            klass = 'u-text-red';
            break;

        case enumObj.COMPLETED:
            text = 'Completed';
            klass = 'u-text-green';
            break;

        case enumObj.EXISTING:
            text = 'Existing upload';
            klass = 'u-text-green';
            break;

        default:
            break;
    }

    return {
        text,
        klass
    };
}

export function yesBool(value) {
    return (
        value === 'yes' ||
        value === true ||
        value === '1' ||
        value === 1 ||
        value === 'true'
    );
}

export function provideHttp(url) {
    if (!url) {
        return '';
    }

    if (url.match(/(http:|https:)\/\//)) {
        return url;
    }

    return `http://${url}`;
}

export function buildQueryString(data = {}, removeFalsyValues = true) {
    return Object.keys(data)
        .map((key) => {
            if (removeFalsyValues && !data[key]) {
                return null;
            }

            return `${encodeURIComponent(key)}=${encodeURIComponent(
                data[key]
            )}`;
        })
        .filter(Boolean)
        .join('&');
}

// https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
export function copyToClipboard(text) {
    const textArea = document.createElement('textarea');

    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';

    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        const successful = document.execCommand('copy');

        return successful;
    } catch (err) {
        return false;
    }
}

export function nonce(length = 10) {
    const chars =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let text = '';

    for (let i = 0; i < length; i++) {
        text += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return text;
}

let passiveSupported = null;

export function passiveOption() {
    if (passiveSupported !== null) {
        return passiveSupported ? { passive: true } : false;
    }

    try {
        const options = Object.defineProperty({}, 'passive', {
            get() {
                passiveSupported = true;
            }
        });

        window.addEventListener('test', options, options);
        window.removeEventListener('test', options, options);
    } catch (err) {
        passiveSupported = false;
    }

    if (passiveSupported) {
        return { passive: true };
    }

    return false;
}

export function getYoutubeIdFromLink(link) {
    let youtubeId;
    let found;

    found = link.match(/^http(s)?:\/\/(www\.)?youtube\.com\/watch\?v=([^&]+)/);
    if (found) {
        youtubeId = found[3];
    } else {
        found = link.match(/^http(s)?:\/\/youtu\.be\/(.+)/);
        if (found) {
            youtubeId = found[2];
        } else {
            found = link.match(
                /<iframe.+src="\/\/www\.youtube\.com\/embed\/(.+)"/
            );
            if (found) {
                youtubeId = found[1];
            }
        }
    }

    if (!youtubeId) {
        return null;
    }

    return youtubeId;
}

export function canUserUpload(currentUser) {
    if (!currentUser.isLoggedIn) {
        return false;
    }

    if (!currentUser.profile.can_upload) {
        return false;
    }

    // Wait until
    // https://github.com/audiomack/audiomack/issues/339
    //
    // if (!currentUser.profile.verified_email) {
    //     return false;
    // }

    return true;
}

export function getVimeoIdFromLink(link) {
    let vimeoId;
    let found;

    found = link.match(/^http(s)?:\/\/vimeo\.com\/(.+)/i);
    if (found) {
        vimeoId = found[2];
    } else {
        found = link.match(
            /<iframe.+src="\/\/player\.vimeo\.com\/video\/([^"]+)"/
        );

        if (found) {
            vimeoId = found[1];
        }
    }

    if (!vimeoId) {
        return null;
    }

    return vimeoId;
}

// Naive color distance formula
function getRGBDistance(color1, color2) {
    const r = Math.pow(color2.r - color1.r, 2);
    const g = Math.pow(color2.g - color1.g, 2);
    const b = Math.pow(color2.b - color1.b, 2);

    return Math.sqrt(r + g + b);
}

/**
 * Get 2 most dominant colors of an image
 *
 * @param  {String|Image}  image                    Image object or src
 * @param  {Number}  options.clipWhites             Ignore equal rgb values from 255 - x
 * @param  {Number}  options.rgbDistance            Minimum required combined distance between rgb values
 * @param  {Float}  options.dominanceThreshold      Only consider values that are at least a % of the total amount of colors
 * @param  {Number}  options.finalColorMaxDistance  Minimum required combined distance between rgb values
 * @return {Array}                                  Dominant colors in rgb values
 */
export function getDominantColors(
    image,
    {
        clipWhites = 10,
        rgbDistance = 16,
        dominanceThreshold = 0,
        finalColorMaxDistance = 20
    } = {}
) {
    if (!image) {
        return Promise.resolve([]);
    }

    return previewImage(image, { returnContext: true, maxSize: 500 }).then(
        (ctx) => {
            return getColorDataWithContext(ctx, {
                clipWhites,
                rgbDistance,
                dominanceThreshold,
                finalColorMaxDistance
            });
        }
    );
}

export function getColorDataWithContext(
    ctx,
    {
        clipWhites = 10,
        rgbDistance = 16,
        dominanceThreshold = 0,
        finalColorMaxDistance = 20
    } = {}
) {
    const { data } = ctx.getImageData(
        0,
        0,
        ctx.canvas.width,
        ctx.canvas.height
    );
    const counts = {};
    let totalCount = 0;

    for (let i = 0, len = data.length; i < len; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];
        const colorValue = [r, g, b];

        if (clipWhites > 0 && r === g && r === b && r + clipWhites >= 255) {
            continue;
        }

        if (
            rgbDistance > 0 &&
            Math.abs(r - g) + Math.abs(g - b) < rgbDistance
        ) {
            continue;
        }

        const colorString = colorValue.join(',');

        counts[colorString] = counts[colorString] || 0;
        counts[colorString] += 1;
        totalCount += 1;
    }

    const sorted = Object.keys(counts)
        .sort((x, y) => {
            return counts[y] - counts[x];
        })
        .filter((color) => {
            return counts[color] / totalCount >= dominanceThreshold;
        })
        .map((color) => {
            return {
                color,
                count: counts[color]
            };
        });

    if (!sorted.length) {
        return [];
    }

    const color1String = sorted[0].color;
    const color1Parts = color1String.split(',');
    const color1 = {
        r: color1Parts[0],
        g: color1Parts[1],
        b: color1Parts[2]
    };
    const finalColors = [color1String, sorted[1].color || color1String];

    for (let k = 1, len = sorted.length; k < len; k++) {
        const colorString = sorted[k].color;
        const color2Parts = colorString.split(',');
        const color2 = {
            r: color2Parts[0],
            g: color2Parts[1],
            b: color2Parts[2]
        };

        finalColors[1] = colorString;

        if (getRGBDistance(color1, color2) > finalColorMaxDistance) {
            break;
        }
    }

    return finalColors;
}

// Edited from https://stackoverflow.com/questions/22837689/onload-issue-with-mobile-safari-chrome-and-windows-safari
export function loadCss(
    path,
    { callback = () => {}, existingLinkEl = null, appendLink = true } = {}
) {
    const link = existingLinkEl || document.createElement('link');
    const timeOut = 1000 * 30;
    const started = Date.now();
    const head = document.getElementsByTagName('head')[0];
    let loaded = false;

    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');
    link.setAttribute('href', path);

    if (link.readyState) {
        link.onreadystatechange = function() {
            if (
                link.readyState === 'loaded' ||
                link.readyState === 'complete'
            ) {
                link.onreadystatechange = null;
                loaded = true;
                callback(link);
                return;
            }
        };
    } else {
        link.onload = function() {
            loaded = true;
            callback(link);
            return;
        };
    }

    if (appendLink) {
        head.appendChild(link);
    }

    const timer = window.setInterval(function() {
        if (Date.now() - started > timeOut) {
            window.clearInterval(timer);
            console.warn('Link load timeout for path', path);
            return;
        }

        if (!loaded) {
            for (let i = 0; i < document.styleSheets.length; i++) {
                if (document.styleSheets[i].href === path) {
                    loaded = true;
                    window.clearInterval(timer);
                    callback(link);
                    return;
                }
            }

            // If we haven't loaded the stylesheet yet, reappend it to the DOM
            head.appendChild(link);
        }
    }, 100);
}

export function buildRedirectUrl(redirectTo, currentLocation) {
    const redirectParts = redirectTo.split('?');
    const query = parse(redirectParts[1]);

    query.redirectTo = encodeURIComponent(currentLocation.pathname);

    const stringified = stringify(query);
    const qs = stringified ? `?${stringified}` : '';

    return `${redirectParts[0]}${qs}`;
}

// https://github.com/audiomack/audiomack-lambda-functions/blob/master/packages/image/README.md
export function buildDynamicImage(imageSrc, options = {}) {
    if (!imageSrc) {
        return '';
    }

    // This isnt a base image and an old image url was sent here
    if (imageSrc.match(/275-275-\d+.jpg$/)) {
        return imageSrc;
    }

    if (imageSrc.match(/\.gravatar.com\//)) {
        const size = Math.max(
            options.width || 0,
            options.size || 0,
            options.height
        );
        const base = imageSrc.split('?')[0];
        const qs = buildQueryString({
            s: size,
            r: 'x',
            d: 'https://assets.audiomack.com/default-artist-image.png'
        });

        return `${base}?${qs}`;
    }

    // This is a default image
    if (
        imageSrc.match(/default-(artist|song|album|playlist)-image\.(jpg|png)$/)
    ) {
        return imageSrc;
    }

    // This is a base64 image
    if (imageSrc.match(/^data:image/)) {
        return imageSrc;
    }

    return `${imageSrc}?${buildQueryString(options)}`;
}

export function getDynamicImageProps(imageSrc, options = {}) {
    let widthSize = options.size;
    let heightSize = options.size;

    if (typeof options.height !== 'undefined') {
        heightSize = options.height;
    }

    if (typeof options.width !== 'undefined') {
        widthSize = options.width;
    }

    const artwork = buildDynamicImage(imageSrc, {
        ...options,
        width: widthSize,
        height: heightSize,
        max: true
    });

    const retinaArtwork = buildDynamicImage(imageSrc, {
        ...options,
        width: widthSize ? widthSize * 2 : null,
        height: heightSize ? heightSize * 2 : null,
        max: true
    });
    let srcSet;

    if (retinaArtwork) {
        srcSet = `${retinaArtwork} 2x`;
    }

    return [artwork, srcSet];
}

export function shouldShowDashboard(location, currentUser) {
    const onDashboardPage = [
        /^\/dashboard/,
        /^\/monetization/,
        /^\/edit\/profile/,
        /^\/stats\/music\/\d+/,
        /^\/notifications/
    ].some((pathRegex) => {
        return location.pathname.match(pathRegex);
    });

    return onDashboardPage && currentUser.isLoggedIn;
}

export function shouldShowAdmin(location, currentUser) {
    const onDashboardPage = [/^\/the-backwoods/].some((pathRegex) => {
        return location.pathname.match(pathRegex);
    });

    return onDashboardPage && currentUser.isLoggedIn && currentUser.isAdmin;
}

export function shouldNotShowAd(pathname, currentUser, isMobile = false) {
    if (currentUser.isLoggedIn && currentUser.profile.subscription !== null) {
        return true;
    }

    const routes = [
        /^\/amp$/,
        /^\/premium$/,
        /^\/about$/,
        /^\/login$/,
        /^\/join$/,
        /^\/premium-partner-agreement$/,
        /^\/forgot-password$/,
        /^\/dashboard(\/.*)?/,
        /^\/upload(\/(songs|albums)?)?/,
        /^\/edit\/profile/,
        /^\/edit\/pins/,
        /^\/edit\/(song|album)\/\d+/,
        /^\/edit\/playlist\/\d+$/,
        /^\/stats\/music\/\d+/,
        /^\/artist\/.*/,
        /^\/switch-account/,
        /^\/notifications/,
        /^\/oauth\/authenticate/,
        /^\/the-backwoods\/.*/,
        /^\/world\/globe/,
        /^\/monetization(\/.*)?/,
        /^\/playlist\/.*\/.*/,
        /^\/the-backwoods\/.*/,
        /^\/creators$/,
        /^\/validate\/.*/,
        /^\/amp-code$/,
        /^\/playground\/freestar-noads/
    ];

    if (!isMobile && !currentUser.isLoggedIn) {
        routes.push(/^\/$/);
    }

    return routes.some((pathRegex) => {
        return pathname.match(pathRegex);
    });
}

export function isSeoWorthyArtistProfile(uploadCount = 0) {
    return uploadCount > 2;
}

export function redirectNonEmbeddedEmbeds() {
    if (window === window.top) {
        window.location = window.location.pathname.replace('/embed', '');
    }
}

// https://stackoverflow.com/a/43846782/1048847
export function triggerResizeEvent() {
    if (typeof window.Event === 'function') {
        // modern browsers
        window.dispatchEvent(new Event('resize'));
        return;
    }

    // for IE and other old browsers
    // causes deprecation warning on modern browsers
    const evt = window.document.createEvent('UIEvents');

    evt.initUIEvent('resize', true, false, window, 0);
    window.dispatchEvent(evt);
}

export function waitUntil(fn, { pollInterval = 1000, timeout = 30000 } = {}) {
    const startTime = Date.now();

    return new Promise((resolve, reject) => {
        function runner() {
            if (Date.now() - startTime >= timeout) {
                reject(
                    `${fn.toString()} could not return true within timeout of ${timeout /
                        1000} seconds`
                );
                return;
            }

            if (fn()) {
                resolve();
                return;
            }

            setTimeout(runner, pollInterval);
        }

        runner();
    });
}

let branchIsAlreadyWaiting = false;

// We need to wait until the branch meta tags are in the DOM
// before calling pageview
function branchLinkWaitFunction() {
    return Array.from(document.head.querySelectorAll('meta')).some((tag) => {
        return tag.name.indexOf('branch:deeplink') !== -1;
    });
}

export function waitForBranchMetadata(options) {
    if (branchIsAlreadyWaiting) {
        return Promise.reject('Branch timer is already waiting');
    }

    branchIsAlreadyWaiting = true;

    return waitUntil(branchLinkWaitFunction, options)
        .then((data) => {
            branchIsAlreadyWaiting = false;
            return data;
        })
        .catch((err) => {
            branchIsAlreadyWaiting = false;
            throw err;
        });
}

export async function batchPromises(totalAmount, promiseFn, options = {}) {
    const {
        batchCount = 5,
        startIndex = 0,
        parallel = false,
        parallelAfterFirstCompletion = false
    } = options;
    const results = options.results || [];
    const amountToProcess = Math.min(totalAmount - startIndex, batchCount);

    if (amountToProcess <= 0) {
        return Promise.resolve(results);
    }

    let promise = Promise.resolve();
    const parallelPromises = [];

    for (let i = 0; i < amountToProcess; i++) {
        const index = i + startIndex;

        if (parallelAfterFirstCompletion) {
            if (index === 0) {
                promise = promise.then(() => {
                    return promiseFn(index).then((value) => {
                        // eslint-disable-line
                        results.push(value);
                        return;
                    });
                });
            } else {
                parallelPromises.push(promiseFn(index));
            }
        } else if (parallel) {
            parallelPromises.push(promiseFn(index));
        } else {
            promise = promise.then(() => {
                return promiseFn(index).then((value) => {
                    // eslint-disable-line
                    results.push(value);
                    return;
                });
            });
        }
    }

    if (parallelPromises.length) {
        promise = promise
            .then(() => {
                return Promise.all(parallelPromises);
            })
            .then((values) => {
                results.push(...values);
                return;
            });
    }

    await promise;

    return batchPromises(totalAmount, promiseFn, {
        ...options,
        startIndex: startIndex + batchCount,
        results
    });
}

export function openPopupWindow(
    url = '',
    { name = '', width = 600, height = 400 } = {}
) {
    const left = window.innerWidth / 2 - width / 2;
    const top = window.innerHeight / 2 - height / 2;

    return window.open(
        url,
        name,
        `toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=${width}, height=${height}, top=${top}, left=${left}`
    );
}

export function getLocationFromPopup(popup) {
    return new Promise((resolve, reject) => {
        let timer;

        function getLocation() {
            if (!popup || popup.closed || typeof popup.closed === 'undefined') {
                clearTimeout(timer);
                reject('Popup closed');
                return;
            }

            try {
                const hostname = popup.location.hostname || '';
                // Normally we dont want document/window API calls in the reducer
                // but this should really only be called from the browser so we
                // should be fine...
                const anchor = document.createElement('a');

                anchor.href = process.env.AM_URL;

                const hostnameSearch = anchor.hostname;

                if (hostname.includes(hostnameSearch)) {
                    const location = popup.location;

                    clearTimeout(timer);
                    popup.close();
                    resolve(location);
                    return;
                }
            } catch (error) {
                // Ignore DOMException: Blocked a frame with origin from accessing a cross-origin frame.
                // A hack to get around same-origin security policy errors in IE.
            }

            timer = setTimeout(() => {
                getLocation();
            }, 500);
        }

        getLocation();
    });
}

/**
 * Prevent resulting url slug from having underscores which
 * arent allowed by the API
 * @param  {String} str usually a title that needs to be slugified before uploading
 * @return {String}
 */
export function uploadSlugify(str = '') {
    return str
        .toLowerCase()
        .replace(/[^-_a-zA-Z0-9\s]/g, '')
        .replace(/[-_\s]+/g, '-')
        .replace(/-+$/, '')
        .replace(/^-+/, '');
}

export function convertSlugToTitle(str = '') {
    return str
        .split(/\-/g)
        .map((word) => {
            return ucfirst(word);
        })
        .join(' ');
}

// Inclusive
export function randomNumberBetween(min, max, decimals = 0) {
    const mutliplier = Math.pow(10, decimals);
    const newMax = max * mutliplier;
    const newMin = min * mutliplier;
    const random = Math.floor(Math.random() * (newMax - newMin + 1) + newMin);

    if (decimals > 0) {
        return random / mutliplier;
    }

    return random;
}

export function hasValidComments(comments) {
    const validComments = comments.filter((comment) => {
        if (comment.deleted === true) {
            return false;
        }

        if (
            typeof comment.artist !== 'undefined' &&
            comment.artist.comment_banned === true
        ) {
            return false;
        }

        return true;
    });

    return validComments.length > 0;
}

export function getPercentage(value, total) {
    if (!value || !total) {
        return null;
    }

    return Math.round((value / total) * 100);
}
