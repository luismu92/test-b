/**
 * Utils related to editing/uploading music
 */

import { allGenresMap } from 'constants/index';

export function handleMusicInputChange(input) {
    let inputName = input.name;
    let inputValue = input.value;
    let error = false;

    switch (input.type) {
        case 'checkbox':
            inputValue = input.checked;

            if (input.name === 'private') {
                inputValue = input.checked ? 'yes' : 'no';
            }

            if (input.name === 'stream_only') {
                inputValue = input.checked ? 'no' : 'yes';
            }
            break;

        case 'select-one': {
            const value = input.options[input.selectedIndex].value;

            inputValue = value;
            break;
        }

        default: {
            let value = input.value;

            if (input.name === 'url_slug') {
                value = value.toLowerCase();

                if (!input.value.match('^[a-zA-Z0-9-]*$')) {
                    error = 'Invalid url slug';
                }
            }

            inputValue = value;
            break;
        }
    }

    if (inputName === 'save_as_single') {
        inputValue = !inputValue;
        inputName = 'album_track_only';
    }

    const fieldError = getBasicInvalidFields(
        { [inputName]: inputValue },
        {
            includeUndefinedFields: false
        }
    );

    return {
        name: inputName,
        value: inputValue,
        error: fieldError ? fieldError[inputName] : error
    };
}

export function getBasicInvalidFields(
    musicInfo,
    {
        required = ['artist', 'title', 'genre', 'url_slug'],
        includeUndefinedFields = false
    } = {}
) {
    const invalidFields = required.filter((field) => {
        if (field === 'genre') {
            return (
                !musicInfo.genre ||
                !Object.keys(allGenresMap).includes(musicInfo.genre)
            );
        }

        if (includeUndefinedFields) {
            return !musicInfo[field];
        }

        return typeof musicInfo[field] !== 'undefined' && !musicInfo[field];
    });

    if (invalidFields.length) {
        const errors = invalidFields.reduce((acc, field) => {
            acc[field] = true;
            return acc;
        }, {});

        return errors;
    }

    return null;
}

export function getErroredKeysFromUpdateRequest(errors) {
    if (Array.isArray(errors) && errors.length) {
        return [];
    }

    const invalidFields = Object.keys(errors).filter((key) => {
        return errors[key].length;
    });

    return invalidFields;
}
