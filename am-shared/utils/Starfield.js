// Edited version of https://github.com/michealparks/canvas-starfield

/*
 * Starfield Constructor
 */

import { debounce } from './index';

export default class Starfield {
    constructor(
        canvas,
        {
            vx = 0.05,
            vy = 0.05,
            maxStarCount = 500,
            maxRadius = 4,
            dpi,
            background = 'black',
            shootingStarInterval = 2000,
            clearCanvasEveryFrame = true,
            shootingStarVx = 7,
            shootingStarVy = 3,
            shootingStarRadius = 3,
            shootingStarRadiusDelta = 0.06
        } = {}
    ) {
        this.canvas =
            typeof canvas === 'string'
                ? document.querySelector(canvas)
                : canvas;
        this.ctx = this.canvas.getContext('2d');

        this.vx = vx;
        this.vy = vy;

        this.shootingStarVx = shootingStarVx;
        this.shootingStarVy = shootingStarVy;
        this.shootingStarRadius = shootingStarRadius;
        this.shootingStarRadiusDelta = shootingStarRadiusDelta;

        this.maxStars = maxStarCount;
        this.maxRadius = maxRadius;

        this.background = background;
        this.clearCanvasEveryFrame = clearCanvasEveryFrame;

        this.shootingStarInterval = shootingStarInterval;
        this.lastShootingStar = Date.now();
        this.currentShootingStar = null;
        this.dpi = dpi || window.devicePixelRatio || 1;

        this.resize();

        window.addEventListener('resize', this.handleWindowResize);

        this._counter = 1;

        this.start();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowResize = debounce(() => {
        this.resize();
    }, 250);

    ////////////////////
    // Helper methods //
    ////////////////////

    getValue(property) {
        let value = this[property];

        if (typeof this[property] === 'function') {
            value = this[property]();
        }

        return value;
    }

    resize() {
        this.style = window.getComputedStyle(this.canvas);
        this.canvas.width =
            parseInt(this.style.width.replace('px', ''), 10) * this.dpi;
        this.canvas.height =
            parseInt(this.style.height.replace('px', ''), 10) * this.dpi;

        this.loadStars();
    }

    star() {
        return {
            x: Math.round(Math.random() * this.canvas.width),
            y: Math.round(Math.random() * this.canvas.height),
            vx: this.getValue('vx'),
            vy: this.getValue('vy'),
            r: 0.5 + Math.random() * this.getValue('maxRadius'),
            alpha: Math.random(),
            alphaBlinkThreshold: 0.1 * (Math.random() * 6 + 2),
            alphaDelta: Math.round(Math.random()) === 1 ? 0.01 : -0.01,
            blur: Math.random()
        };
    }

    loadStars() {
        const count = this.getValue('maxStars');

        this._maxStarCount = count;
        this.stars = new Array(count);

        for (let i = 0, len = count; i < len; i++) {
            this.stars[i] = this.star();
        }
    }

    draw(star) {
        const fill = `rgba(255, 255, 255, ${star.alpha})`;

        if (!star.blur) {
            this.ctx.beginPath();
            this.ctx.fillStyle = fill;
            this.ctx.arc(star.x, star.y, star.r, 0, 2 * Math.PI, false);
            this.ctx.fill();
        }

        const inverted = 1 - star.blur;
        const r0 = star.r * inverted;
        const radialGraident = this.ctx.createRadialGradient(
            star.x,
            star.y,
            r0,
            star.x,
            star.y,
            star.r
        );

        radialGraident.addColorStop(0, fill);
        radialGraident.addColorStop(1, 'rgba(255, 255, 255, 0)');

        // draw shape
        this.ctx.fillStyle = radialGraident;
        this.ctx.fillRect(
            star.x - star.r,
            star.y - star.r,
            star.r * 2,
            star.r * 2
        );
    }

    drawBackground() {
        if (this.background instanceof Image) {
            // let sWidth = this.background.naturalWidth;
            // let sHeight = this.background.naturalHeight;

            // if (this.background.naturalWidth > this.canvas.width / this.dpi) {
            //     sWidth = this.canvas.width / this.dpi;
            // }

            // if (this.background.naturalHeight > this.canvas.height / this.dpi) {
            //     sHeight = this.canvas.height / this.dpi;
            // }

            // this.ctx.drawImage(this.background, 0, 0, sWidth, sHeight, 0, 0, this.canvas.width, this.canvas.height);
            return;
        }

        this.ctx.fillStyle = this.background;
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    start = () => {
        // Skip every other frame
        this._counter = 1 - this._counter;

        if (this._counter % 2 === 0) {
            this.frameId = window.requestAnimationFrame(this.start);
            return;
        }

        if (this.clearCanvasEveryFrame) {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }

        this.drawBackground();

        for (let i = 0, len = this._maxStarCount; i < len; i++) {
            const star = this.stars[i];

            this.draw(star);

            star.y += star.vy;
            star.x += star.vx;
            star.alpha += star.alphaDelta;

            if (Math.abs(star.alpha - star.alphaBlinkThreshold) >= 0.25) {
                star.alphaDelta = -star.alphaDelta;
            }

            if (star.x > this.canvas.width) {
                star.x = star.vx > 0 ? -1 : this.canvas.width + 1;
            }

            if (star.y > this.canvas.height) {
                star.y = star.vy > 0 ? -1 : this.canvas.height + 1;
            }
        }

        if (this.currentShootingStar) {
            const star = this.currentShootingStar;

            this.draw(star);

            star.x += star.vx;
            star.y += star.vy;
            star.alpha += star.alphaDelta;
            star.r -= star.radiusDelta;

            if (star.r <= 0) {
                this.currentShootingStar = null;
            }
        } else if (this.shootingStarInterval) {
            const interval = this.getValue('shootingStarInterval');

            if (Date.now() - this.lastShootingStar >= interval) {
                this.currentShootingStar = this.star();
                this.currentShootingStar.r = this.getValue(
                    'shootingStarRadius'
                );
                this.currentShootingStar.vx = this.getValue('shootingStarVx');
                this.currentShootingStar.vy = this.getValue('shootingStarVy');
                this.currentShootingStar.radiusDelta = this.getValue(
                    'shootingStarRadiusDelta'
                );

                if (this.currentShootingStar.radiusDelta < 0) {
                    throw new Error(
                        'Value of `shootingStarRadiusDelta` must be positive'
                    );
                }
                this.lastShootingStar = Date.now();
            }
        }

        this.frameId = window.requestAnimationFrame(this.start);
    };

    stop() {
        window.cancelAnimationFrame(this.frameId);
    }

    destroy() {
        this.stop();
        this.background = null;
        window.removeEventListener('resize', this.handleWindowResize);
    }
}
