export function getTotalsFromPlaylists(playlists) {
    let totalPlaylists = 0;
    let totalSongs = 0;
    if (playlists && typeof playlists.length !== 'undefined') {
        for (const playlist of playlists) {
            totalPlaylists++;
            totalSongs += playlist.count;
        }
    }

    return {
        totalPlaylists,
        totalSongs
    };
}
