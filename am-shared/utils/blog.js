export const BLOG_TYPE_PHOTO = 'Photo';
export const BLOG_TYPE_PLAYLIST = 'Playlist';
export const BLOG_TYPE_VIDEO = 'Video';
export const BLOG_TYPE_MUSIC = 'Music';

export function getTypeForItem(item) {
    if (item.html && item.html.match(/<img/g).length >= 5) {
        return BLOG_TYPE_PHOTO;
    }

    if (item.tags && item.tags.some((tag) => tag.slug === 'playlist')) {
        return BLOG_TYPE_PLAYLIST;
    }

    if (item.tags && item.tags.some((tag) => tag.slug === 'videos')) {
        return BLOG_TYPE_VIDEO;
    }

    return BLOG_TYPE_MUSIC;
}

export function getBlogPageUrl(pageObj, { host = '' } = {}) {
    return `${host}/world/${pageObj.slug}`;
}

export function getBlogPostUrl(post, { host = '' } = {}) {
    return `${host}/world/post/${post.slug}`;
}

export function getBlogTagUrl(tag, { host = '' } = {}) {
    return `${host}/world/tag/${tag.slug}`;
}

export function getFeaturedImage(post) {
    if (!post) {
        return '';
    }

    if (post.feature_image) {
        return post.feature_image;
    }

    if (post.html) {
        const matches = post.html.match(/<img[^>]+src="?(([^"\s]+))"?/);

        if (matches) {
            return matches[1];
        }
    }

    return '';
}
