import { allGenresMap, tagSections } from 'constants/index';

const genres = Object.values(allGenresMap);

export function getTagOptions(tags) {
    const options = [...tags.options.music, ...tags.ui.geo];

    const locationMapper = (item) => ({
        text: item.display,
        value: item.normalizedKey,
        children: locationsMapper(item.children || []),
        hasChildren: item.hasChildren
    });

    const locationsMapper = (items) =>
        items.map((item) => locationMapper(item));

    const tagsObject = options.reduce(
        (acc, curr) => {
            const { display, normalizedKey, section } = curr;

            let item = {
                text: display,
                value: normalizedKey
            };

            if (section === tagSections.locations) {
                item = locationMapper(curr);
                acc.locations.push(item);
            }

            if (section === tagSections.mood) {
                acc.moods.push(item);
            }

            if (section === tagSections.subgenre || genres.includes(section)) {
                if (!acc.subgenres[section]) {
                    acc.subgenres[section] = [];
                }
                acc.subgenres[section].push(item);
            }

            return acc;
        },
        {
            locations: [],
            moods: [{ text: 'Choose a mood', value: '' }],
            subgenres: {
                [tagSections.defaultOption]: {
                    text: 'Choose up to (2) subgenres',
                    value: ''
                }
            }
        }
    );

    return tagsObject;
}

export function getUserTags(tags = []) {
    return tags.reduce(
        (acc, curr) => {
            if (!curr) {
                return acc;
            }

            const { display, normalizedKey, section } = curr;

            if (section === tagSections.locations) {
                acc.locations.push(display);
            }

            if (section === tagSections.mood) {
                acc.moods.push(normalizedKey);
            }

            if (section === tagSections.subgenre || genres.includes(section)) {
                acc.subgenres.push(normalizedKey);
            }

            return acc;
        },
        {
            subgenres: [],
            moods: [],
            locations: []
        }
    );
}

export function mapTagOptions(from, to, tags) {
    const options = [...tags.options.music, ...tags.ui.geo];

    return options.reduce((acc, curr) => {
        const tag = {
            [curr[from]]: curr[to]
        };

        return { ...acc, ...tag };
    }, {});
}
