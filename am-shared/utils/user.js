export function isAlreadyFavorited(currentUser, item) {
    if (!currentUser.profile) {
        return false;
    }
    const { profile } = currentUser;

    if (
        (item.type === 'playlist' &&
            profile.favorite_playlists &&
            profile.favorite_playlists.includes(item.id)) ||
        (item.type !== 'playlist' &&
            profile.favorite_music &&
            profile.favorite_music.includes(item.id))
    ) {
        return true;
    }

    return false;
}

export function isAlreadyReupped(currentUser, item) {
    if (!currentUser.profile) {
        return false;
    }
    const { profile } = currentUser;

    if (profile.reups && profile.reups.includes(item.id)) {
        return true;
    }

    return false;
}
