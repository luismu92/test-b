import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

export default class AuthPageMeta extends Component {
    static propTypes = {
        page: PropTypes.string.isRequired
    };

    getTitleFromPage(page) {
        switch (page) {
            case 'login':
                return 'Log in or Sign Up | Listen on Audiomack';

            case 'join':
                return 'Sign Up | Listen on Audiomack';

            case 'forgot':
                return 'Forgot your password? | Listen on Audiomack';

            default:
                return '';
        }
    }

    render() {
        const { page } = this.props;
        const title = this.getTitleFromPage(page);
        const description =
            'Join or sign in to Audiomack, a free music streaming and discovery platform that allows artists to share their music and fans to discover new artists, songs, albums, mixtapes, playlists and more.';

        return (
            <Helmet>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta name="twitter:title" content={title} />
                <meta name="twitter:description" content={description} />
                <meta name="robots" content="noindex" />
            </Helmet>
        );
    }
}
