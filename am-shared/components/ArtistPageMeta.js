import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { isSeoWorthyArtistProfile, getPodcastUrl } from 'utils/index';

export default class ArtistPageMeta extends Component {
    static propTypes = {
        artistProfile: PropTypes.object.isRequired,
        artistContext: PropTypes.string.isRequired,
        defaultContext: PropTypes.string.isRequired,
        page: PropTypes.number
    };

    getAppLinkPath(artistProfile, artistContext) {
        return `/artist_${artistContext}/${artistProfile.url_slug}`;
    }

    render() {
        const { artistProfile, artistContext, defaultContext } = this.props;
        const title = `${artistProfile.name} on Audiomack`;
        const description = `Stream new music from ${
            artistProfile.name
        } for free on Audiomack, including the latest songs, albums, mixtapes and playlists.`;
        const canonical = `${process.env.AM_URL}/artist/${
            artistProfile.url_slug
        }/${artistContext}`;
        const personSchema = {
            name: artistProfile.name,
            url: `${process.env.AM_URL}/artist/${artistProfile.url_slug}`,
            image: artistProfile.image,
            description: description,
            '@type': 'Person',
            '@context': 'https://schema.org'
        };
        const deepLinkPath = this.getAppLinkPath(artistProfile, artistContext);
        const appDeepLink = `audiomack:/${deepLinkPath}`;

        if (artistProfile.verified === 'yes') {
            personSchema.sameAs = [
                artistProfile.facebook,
                artistProfile.twitter
                    ? `https://twitter.com/${artistProfile.twitter}`
                    : '',
                artistProfile.instagram
                    ? `https://instagram.com/${artistProfile.instagram}`
                    : ''
            ].filter(Boolean);
        }

        let noIndex;
        let canonicalTag;
        let rssTag;

        if (artistContext === defaultContext) {
            canonicalTag = (
                <link
                    rel="canonical"
                    href={`${process.env.AM_URL}/artist/${
                        artistProfile.url_slug
                    }`}
                />
            );
        }

        if (
            artistContext !== defaultContext ||
            !isSeoWorthyArtistProfile(
                artistProfile.upload_count_excluding_reups
            )
        ) {
            noIndex = <meta name="robots" content="noindex, follow" />;
        }

        if (artistProfile.genre === 'podcast') {
            rssTag = (
                <link
                    rel="alternate"
                    type="application/rss+xml"
                    title={artistProfile.name}
                    href={getPodcastUrl(artistProfile.url_slug)}
                />
            );
        }

        return (
            <Helmet>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta property="og:image" content={artistProfile.image} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta property="og:url" content={canonical} />
                {canonicalTag}
                {noIndex}
                {rssTag}
                <link
                    href={`android-app://${
                        process.env.ANDROID_APP_PACKAGE
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
                <link
                    href={`ios-app://${
                        process.env.IOS_APP_ID
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />
                <meta name="twitter:image" content={artistProfile.image} />
                <meta name="twitter:title" content={title} />
                <meta name="twitter:description" content={description} />
                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />
                <script type="application/ld+json">
                    {JSON.stringify([personSchema])}
                </script>
            </Helmet>
        );
    }
}
