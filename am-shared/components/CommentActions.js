import React, { Component } from 'react';
import { createPortal } from 'react-dom';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Transition from 'react-transition-group/Transition';

import LinkIcon from 'icons/link';
import FlagIcon from 'icons/flag';
import TrashIcon from 'icons/trash';
import RemoveIcon from 'icons/remove';
import ThreeDots from 'icons/three-dots-vertical';

import BodyClickListener from './BodyClickListener';

import { getMusicUrl } from 'utils/index';

export default class CommentActions extends Component {
    static propTypes = {
        comment: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        onCommentActionClick: PropTypes.func,
        item: PropTypes.object
    };

    static defaultProps = {
        lightTheme: false,
        onCommentActionClick() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            tooltipActive: false,
            mounted: false
        };
    }

    componentDidMount() {
        // eslint-disable-next-line
        this.setState({
            mounted: true
        });

        this._tooltipContainer = document.getElementById(
            'commentTooltipContainer'
        );

        if (!this._tooltipContainer) {
            this._tooltipContainer = document.createElement('div');
            this._tooltipContainer.id = 'commentTooltipContainer';
            this._tooltipContainer.className = 'tooltip-container';
            document.body.appendChild(this._tooltipContainer);
        }
    }

    componentWillUnmount() {
        this._tooltipButton = null;
        this._tooltipContainer = null;
    }

    handleTooltipClick = (e) => {
        const button = e.currentTarget;
        const rect = button.getBoundingClientRect();
        const buttonWidth = button.clientWidth;

        button.blur();

        const newActiveState = !this.state.tooltipActive;
        const newState = {
            tooltipActive: newActiveState
        };

        if (newActiveState) {
            newState.tooltipRight =
                window.innerWidth - rect.left - buttonWidth / 2 - 45;
            newState.tooltipBottom = window.innerHeight - rect.top;
        }

        this.setState(newState);
    };

    render() {
        const { onCommentActionClick, comment, currentUser, item } = this.props;

        if (!currentUser.isLoggedIn) {
            return null;
        }

        const canDelete =
            currentUser.isAdmin ||
            currentUser.profile.id === comment.artist.artist_id;

        let removeButton;

        if (!comment.deleted && canDelete) {
            removeButton = (
                <li className="sub-menu__item">
                    <button
                        data-comment-uuid={comment.uuid}
                        data-comment-thread={comment.thread}
                        data-comment-kind={comment.kind}
                        data-comment-id={comment.id}
                        data-comment-action="remove"
                        onClick={onCommentActionClick}
                    >
                        <TrashIcon className="sub-menu__icon" />
                        Remove
                    </button>
                </li>
            );
        }

        let reportButton;

        if (currentUser.profile.id !== comment.artist.artist_id) {
            reportButton = (
                <li className="sub-menu__item">
                    <button
                        data-comment-uuid={comment.uuid}
                        data-comment-thread={comment.thread}
                        data-comment-kind={comment.kind}
                        data-comment-id={comment.id}
                        data-comment-action="report"
                        onClick={onCommentActionClick}
                    >
                        <FlagIcon className="sub-menu__icon" />
                        Report
                    </button>
                </li>
            );
        }

        let banButton;

        if (
            currentUser.isAdmin &&
            currentUser.profile.id !== comment.artist.artist_id
        ) {
            banButton = (
                <li className="sub-menu__item">
                    <button
                        data-user={comment.user_id}
                        data-comment-action="ban"
                        onClick={onCommentActionClick}
                    >
                        <RemoveIcon className="sub-menu__icon" />
                        Ban User
                    </button>
                </li>
            );
        }

        let flaggedButton;

        if (currentUser.isAdmin) {
            flaggedButton = (
                <li className="sub-menu__item">
                    <Link to="/dashboard/comment">Flagged Comments</Link>
                </li>
            );
        }

        let recentButton;

        if (currentUser.isAdmin) {
            recentButton = (
                <li className="sub-menu__item">
                    <Link to="/dashboard/comment-recent">Recent Comments</Link>
                </li>
            );
        }

        let userCommentButton;

        if (currentUser.isAdmin) {
            userCommentButton = (
                <li className="sub-menu__item">
                    <a
                        target="_blank"
                        href={`/dashboard/comment-user/user/${comment.user_id}`}
                    >
                        All From This User
                    </a>
                </li>
            );
        }

        let singleCommentButton;

        if (!comment.deleted) {
            let commentUrl = `${getMusicUrl(item)}?comment=${comment.uuid}`;
            if (
                typeof comment.thread !== 'undefined' &&
                comment.thread !== null
            ) {
                commentUrl += `&thread=${comment.thread}`;
            }
            singleCommentButton = (
                <li className="sub-menu__item">
                    <Link to={commentUrl}>
                        <LinkIcon className="sub-menu__icon" />
                        Link
                    </Link>
                </li>
            );
        }

        return (
            <ul className="comment-item__admin-actions">
                <BodyClickListener
                    shouldListen={this.state.tooltipActive}
                    onClick={this.handleTooltipClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <li className="u-pos-relative">
                    <button
                        className="comment__actions-button tracklist__track-interaction-icon tracklist__track-interaction-icon--dots u-d-block"
                        onClick={this.handleTooltipClick}
                        ref={(e) => {
                            this._tooltipButton = e;
                        }}
                    >
                        <ThreeDots />
                    </button>
                    {this.state.mounted &&
                        createPortal(
                            <Transition
                                in={this.state.tooltipActive}
                                timeout={120}
                                unmountOnExit
                            >
                                {(state) => {
                                    const tooltipClass = classnames(
                                        'tooltip tooltip--down-right-arrow tooltip--shadow sub-menu comment__sub-menu',
                                        {
                                            'tooltip--light': false,
                                            'tooltip--active':
                                                state === 'entered'
                                        }
                                    );

                                    return (
                                        <ul
                                            className={tooltipClass}
                                            style={{
                                                position: 'fixed',
                                                bottom: this.state
                                                    .tooltipBottom,
                                                right: this.state.tooltipRight,
                                                zIndex: 99,
                                                minWidth: 0,
                                                maxWidth: 300
                                            }}
                                        >
                                            {singleCommentButton}
                                            {removeButton}
                                            {banButton}
                                            {reportButton}
                                            {userCommentButton}
                                            {recentButton}
                                            {flaggedButton}
                                        </ul>
                                    );
                                }}
                            </Transition>,
                            this._tooltipContainer
                        )}
                </li>
            </ul>
        );
    }
}
