import React, { Component } from 'react';
import { setConfig } from 'react-hot-loader';
import { hot } from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

setConfig({ logLevel: process.env.HOT_LOADER_LOG_LEVEL || undefined });

export default class Root extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
        Router: PropTypes.oneOfType([
            PropTypes.object.isRequired,
            PropTypes.func.isRequired
        ]),
        routes: PropTypes.func.isRequired,
        routerProps: PropTypes.object,
        history: PropTypes.object
    };

    render() {
        const { store, Router, routes, routerProps } = this.props;

        return (
            <Provider store={store} key="provider">
                <Router {...routerProps}>{routes()}</Router>
            </Provider>
        );
    }
}

export const HotRoot = hot(Root);
