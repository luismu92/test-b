import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { isCurrentMusicItem } from 'utils/index';

import Waveform from './Waveform';
import { seek } from '../redux/modules/player';

class WaveformContainer extends Component {
    static propTypes = {
        musicItem: PropTypes.object,
        canvasProps: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        reflection: PropTypes.bool,
        color: PropTypes.string,
        fillColor: PropTypes.string
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentWillUnmount() {
        this.cleanUpSeekShit();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSeekMouseDown = (e) => {
        const {
            player: { duration }
        } = this.props;

        this._waveform = e.currentTarget;
        this._waveformRect = this._waveform.getBoundingClientRect();

        this.callSeek(e.pageX, this._waveformRect, duration);

        window.addEventListener('mousemove', this.handleSeekMouseMove, false);
        window.addEventListener('mouseup', this.handleSeekMouseUp, false);
    };

    handleSeekMouseMove = (e) => {
        const {
            player: { duration }
        } = this.props;

        this.callSeek(e.pageX, this._waveformRect, duration);
    };

    handleSeekMouseUp = () => {
        this.cleanUpSeekShit();
    };

    handleSeekTouchStart = (e) => {
        const {
            player: { duration }
        } = this.props;

        this._waveform = e.currentTarget;
        this._waveformRect = this._waveform.getBoundingClientRect();

        this.callSeek(e.touches[0].pageX, this._waveformRect, duration);

        window.addEventListener('touchmove', this.handleSeekTouchMove, false);
        window.addEventListener('touchend', this.handleSeekTouchEnd, false);
    };

    handleSeekTouchMove = (e) => {
        const {
            player: { duration }
        } = this.props;

        this.callSeek(e.touches[0].pageX, this._waveformRect, duration);
    };

    handleSeekTouchEnd = () => {
        this.cleanUpSeekShit();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    isCurrent(player, currentSong, musicItem) {
        return isCurrentMusicItem(currentSong, musicItem);
    }

    cleanUpSeekShit() {
        window.removeEventListener('mousemove', this.handleSeekMouseMove);
        window.removeEventListener('mouseup', this.handleSeekMouseUp);

        this._waveform = null;
        this._waveformRect = null;
    }

    callSeek(pageX, rect, duration) {
        const { dispatch } = this.props;

        if (!rect) {
            console.warn('No rect found for some reason. Maybe unmounted?');
            return;
        }

        const ratio = (pageX - rect.left) / rect.width;
        const toTime = ratio * duration;

        dispatch(seek(toTime), 150);
    }

    render() {
        const { musicItem, player } = this.props;
        const {
            currentSong,
            currentTime,
            duration,
            chromecastLoading
        } = player;
        const isCurrentSong = this.isCurrent(player, currentSong, musicItem);
        let progress = 0;

        if (isCurrentSong && duration && !chromecastLoading) {
            progress = currentTime / Math.max(duration, 1);
        }

        let dataAsString = '';
        let musicVolumeData = [];

        try {
            dataAsString = musicItem.volume_data;

            musicVolumeData = JSON.parse(dataAsString) || [];
        } catch (e) {} //eslint-disable-line

        return (
            <Waveform
                // Only allow seeking right now for currently playing songs
                onMouseDown={progress ? this.handleSeekMouseDown : null}
                onTouchStart={progress ? this.handleSeekTouchStart : null}
                reflection={this.props.reflection}
                color={this.props.color}
                fillColor={this.props.fillColor}
                fill={progress}
                dataAsString={dataAsString}
                data={musicVolumeData}
                canvasProps={this.props.canvasProps}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player
    };
}

export default connect(mapStateToProps)(WaveformContainer);
