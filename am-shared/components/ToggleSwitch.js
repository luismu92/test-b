import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import QuestionIcon from 'icons/question-mark';

import styles from './ToggleSwitch.module.scss';

export default function ToggleSwitch({
    label,
    showText,
    checked,
    disabled,
    id,
    name,
    className,
    onChange,
    tooltip,
    textExtraClassNames
}) {
    const klass = classnames(styles.toggle, {
        [className]: className
    });

    const textClass = classnames(styles.text, {
        [textExtraClassNames]: textExtraClassNames
    });

    let toggleTooltip;

    if (tooltip) {
        toggleTooltip = (
            <span className={styles.info} data-tooltip={tooltip}>
                <QuestionIcon />
            </span>
        );
    }

    return (
        <div className={klass}>
            {toggleTooltip}
            {showText && (
                <span className={textClass} role="presentation">
                    {label}
                </span>
            )}
            <input
                type="checkbox"
                className={styles.checkbox}
                id={id}
                name={name}
                checked={checked}
                disabled={disabled}
                onChange={onChange}
            />
            <label className={styles.label} htmlFor={id}>
                {label}
            </label>
        </div>
    );
}

ToggleSwitch.defaultProps = {
    id: 'switch'
};

ToggleSwitch.propTypes = {
    label: PropTypes.string.isRequired,
    showText: PropTypes.bool,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    id: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    onChange: PropTypes.func,
    tooltip: PropTypes.string,
    textExtraClassNames: PropTypes.string
};
