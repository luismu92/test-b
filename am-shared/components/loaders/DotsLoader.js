import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class DotsLoader extends Component {
    static propTypes = {
        size: PropTypes.number,
        containerStyles: PropTypes.object,
        className: PropTypes.string,
        color: PropTypes.string,
        active: PropTypes.bool
    };

    static defaultProps = {
        size: 6,
        containerStyles: {},
        color: 'currentColor',
        active: true
    };

    render() {
        const circleStyles = {
            width: `${this.props.size}px`,
            height: `${this.props.size}px`,
            background: this.props.color
        };

        if (!this.props.active) {
            return null;
        }

        const klass = classnames('loading-icon', {
            [this.props.className]: this.props.className
        });

        return (
            <span
                className={klass}
                style={this.props.containerStyles}
                title="Loading…"
            >
                <span
                    className="circle circle-1"
                    style={circleStyles}
                    role="presentation"
                />
                <span
                    className="circle circle-2"
                    style={circleStyles}
                    role="presentation"
                />
                <span
                    className="circle circle-3"
                    style={circleStyles}
                    role="presentation"
                />
            </span>
        );
    }
}
