import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import shave from 'shave';
import DOMPurify from 'dompurify';

import { throttle, uuid } from 'utils/index';

export default class Truncate extends Component {
    static propTypes = {
        label: PropTypes.string,
        lines: PropTypes.number,
        cutOffMoreCharacters: PropTypes.number,
        tagName: PropTypes.string,
        className: PropTypes.string,
        onTruncate: PropTypes.func,
        ellipsis: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.element
        ]),
        linkify: PropTypes.bool,
        linkedDomains: PropTypes.array,
        text: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.element,
            PropTypes.array
        ])
    };

    static defaultProps = {
        onTruncate() {},
        tagName: 'div',
        ellipsis: '…',
        linkedDomains: [process.env.AM_DOMAIN, 'audiomack.com']
    };

    constructor(props) {
        super(props);

        this.state = {
            collapsedText: props.text,
            uuid: uuid()
        };

        if (props.linkify) {
            this.state.collapsedText = Truncate.linkifyText(
                props.text,
                props.linkedDomains
            );
        }
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.createSizerElement();
        window.requestAnimationFrame(() => {
            this.shave();
        });

        window.addEventListener('resize', this.handleResize, false);
    }

    componentDidUpdate(prevProps) {
        if (
            this.getTextKey(this.props.text) !==
                this.getTextKey(prevProps.text) ||
            this.props.lines !== prevProps.lines ||
            this.props.ellipsis !== prevProps.ellipsis
        ) {
            this.shave();
        }
    }

    componentWillUnmount() {
        this._displayEllipsis = null;
        this._sizerElement = null;
        this._sanitizer = null;
        window.removeEventListener('resize', this.handleResize, false);

        const existing = document.getElementById(this.state.uuid);

        if (existing) {
            existing.parentElement.removeChild(existing);
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleResize = throttle(() => {
        this.resize();
    }, 100);

    ////////////////////
    // Helper methods //
    ////////////////////

    getTextKey(text) {
        if (Array.isArray(text)) {
            return text
                .map((part) => {
                    if (typeof part.key === 'string') {
                        return part.key;
                    }

                    return part.toString();
                })
                .join('');
        }

        return text;
    }

    /**
     * Find location in the text map where the plain truncated text
     * cuts off. This allows us to preserve any <Link> or <a> tags that
     * may be passed in the `text` prop. Assumes length of `text` and
     * `textMap` prop are equal
     *
     * @param  {Array} textMap       Array of strings
     * @param  {String} truncatedText truncated text after shave happens
     * @return {Number} maxNewLines amount of new lines allowed before exiting
     * @return {Number} Index to slice the original `text` prop to
     */
    getTruncatedTextMapIndex(textMap, truncatedText, maxNewLines) {
        let endIndex = textMap.length;
        let alteredTruncatedText = truncatedText;
        let newLineCounter = maxNewLines;

        textMap.some((part, i) => {
            if (alteredTruncatedText.indexOf(part) !== -1) {
                if (part.length) {
                    alteredTruncatedText = alteredTruncatedText.substr(
                        part.length
                    );
                    endIndex = i + 1;
                }

                if (part === '\n') {
                    newLineCounter -= 1;

                    if (newLineCounter === 0) {
                        return true;
                    }
                }

                return false;
            }

            return true;
        });

        return endIndex;
    }

    /**
     * Take a string of text and return an array of parts with
     * the allowedDomains as links
     * @param  {String} text           [description]
     * @param  {Array}  allowedDomains [description]
     * @return {Array}
     */
    static getLinkifiedTextParts(text = '', allowedDomains = []) {
        if (!allowedDomains.length) {
            return [{ text }];
        }

        const newlineRegex = '\n';
        const protoRegex = '(?:https?://)?';
        const pathRegex = '/[-a-zA-Z0-9@:%_+.~#?&//=]*';
        const spaceRegex = '\\s+';
        const urlRegex = allowedDomains
            .map((d) => `${protoRegex}${d}(?:${pathRegex})?`)
            .join('|');

        return text
            .split(
                new RegExp(`(${newlineRegex}|${urlRegex}|${spaceRegex})`, 'gi')
            )
            .filter(Boolean)
            .map((part) => {
                const isUrl = !!part.match(new RegExp(`(${urlRegex})`, 'gi'));
                const ret = {
                    text: part
                };

                if (isUrl) {
                    const pathMatch = part.match(
                        /(?:https?:\/\/)?(?:[^\/\s]+\/)(.*)/
                    );

                    ret.proto = part.match(/^https?:\/\//) || 'https://';
                    ret.domain = allowedDomains.find((d) =>
                        part.match(new RegExp(`^${protoRegex}${d}`))
                    );
                    ret.path = `/${pathMatch ? pathMatch[1] : ''}`;
                }

                return ret;
            });
    }

    /**
     * Get linkified text parts from a string of text
     * @param  {String} text
     * @param  {Array}  allowedDomains Allowed domains that should be linked. The first
     * domain in the array will be turned into a <Link> instead of <a>
     * @return {Array}
     */
    static linkifyText(text = '', allowedDomains = []) {
        return Truncate.getLinkifiedTextParts(text, allowedDomains).map(
            (obj, i) => {
                if (obj.path) {
                    // Use the first link as a react link instead of regular link
                    if (obj.domain === allowedDomains[0]) {
                        return (
                            <Link to={obj.path} key={i}>
                                {obj.text}
                            </Link>
                        );
                    }

                    return (
                        <a
                            href={`${obj.proto}${obj.domain}${obj.path}`}
                            key={i}
                            target="_blank"
                            rel="nofollow noopener"
                        >
                            {obj.text}
                        </a>
                    );
                }

                if (obj.text === '\n') {
                    return (
                        <span
                            key={`breaktag${i}`}
                            style={{ display: 'block', height: '.25em' }}
                        />
                    );
                }

                return obj.text;
            }
        );
    }

    /**
     * After getting the linkified text parts, it could be a potentially large
     * array of single words and react elements. This cleans it up so the strings
     * are bundled together and newlines are trimmed off the end of the array
     * @param  {Array}  parts returned items from [Truncate.linkifyText]
     * @return {Array}
     */
    static collapseLinkifiedText(parts = []) {
        // Duck type for a react component
        // Cut off trailing br tags that makes the truncated text look weird
        while (
            parts[parts.length - 1] &&
            parts[parts.length - 1].key &&
            parts[parts.length - 1].key.match(/^breaktag/)
        ) {
            parts.pop();
        }

        return parts.reduce((acc, obj, i) => {
            const lastItem = acc[acc.length - 1] || '';

            if (typeof obj === 'string') {
                if (!acc.length || typeof lastItem !== 'string') {
                    acc.push(obj);

                    return acc;
                }

                if (typeof lastItem === 'string') {
                    const concated = `${lastItem}${obj}`;

                    acc[acc.length - 1] = concated;

                    if (i === parts.length - 1) {
                        acc[acc.length - 1] = concated.trim();
                    }

                    return acc;
                }
            }

            // Duck type for react component
            if (typeof obj.key === 'string') {
                acc.push(obj);
            }

            return acc;
        }, []);
    }

    sanitizeHTML(str) {
        if (!this._sanitizer) {
            this._sanitizer = document.createElement('div');
        }

        this._sanitizer.textContent = str;

        return this._sanitizer.innerHTML;
    }

    resize() {
        const existingClone = document.getElementById(this.state.uuid);

        if (existingClone && this._container) {
            const rect = this._container.getBoundingClientRect();

            existingClone.style.width = `${rect.width}px`;
            existingClone.style.height = `${rect.height}px`;
        }

        this.shave();
    }

    createSizerElement() {
        const id = 'sizerElement';
        const existing = document.getElementById(id);

        if (existing) {
            this._sizerElement = existing;
            return;
        }

        this._sizerElement = document.createElement('div');
        this._sizerElement.id = id;
        this._sizerElement.style.width = '1em';
        this._sizerElement.style.position = 'absolute';
        this._sizerElement.style.left = '-99999px';
        this._sizerElement.setAttribute('aria-hidden', 'true');

        document.body.appendChild(this._sizerElement);
    }

    cloneTextArea(element) {
        const existing = document.getElementById(this.state.uuid);

        if (existing) {
            return existing;
        }

        const clone = element.cloneNode(true);
        const rect = element.getBoundingClientRect();

        clone.style.width = `${rect.width}px`;
        clone.style.height = `${rect.height}px`;
        clone.style.position = 'absolute';
        clone.style.left = '-99999px';
        clone.id = this.state.uuid;
        clone.setAttribute('aria-hidden', 'true');

        document.body.appendChild(clone);

        return clone;
    }

    shave() {
        window.requestAnimationFrame(() => {
            if (!this._text || !this._container) {
                return;
            }

            const {
                lines,
                ellipsis,
                cutOffMoreCharacters,
                linkify,
                linkedDomains
            } = this.props;
            let fullText = this.props.text;
            let textMap;
            let linkifiedText;

            if (linkify) {
                linkifiedText = Truncate.linkifyText(
                    this.props.text,
                    linkedDomains
                );
                textMap = Truncate.getLinkifiedTextParts(
                    this.props.text,
                    linkedDomains
                ).map((obj) => obj.text);
                fullText = textMap.join('');
            }
            const opts = {};

            let computedLines = lines;

            if (!computedLines) {
                computedLines = Infinity;
            }

            if (typeof ellipsis === 'string') {
                opts.character = ellipsis;
            }

            if (!isFinite(computedLines)) {
                // this._text.innerHTML = fullText;
                this.setState(
                    {
                        collapsedText: linkify ? linkifiedText : this.props.text
                    },
                    () => {
                        this.props.onTruncate(false);
                    }
                );
                return;
            }

            const style = window.getComputedStyle(this._text);

            this._sizerElement.style.fontSize = style['font-size'];
            this._sizerElement.style.lineHeight = style['line-height'];
            this._sizerElement.textContent = Array(computedLines)
                .fill('M')
                .join('\n');

            // Give extra space but not enough to be another line
            const height = Math.round(
                this._sizerElement.clientHeight +
                    (this._sizerElement.clientHeight / computedLines) * 0.5
            );

            const clone = this.cloneTextArea(this._container, style);

            clone.style.height = `${height}px`;
            clone.innerHTML = DOMPurify.sanitize(fullText);

            shave(clone, height, opts);

            const shaveEllipsis = clone.querySelector('.js-shave-char');

            this._displayEllipsis = false;

            if (shaveEllipsis) {
                shaveEllipsis.style.display = 'none';
                this._displayEllipsis = true;
            }

            let truncatedText = clone.textContent;

            if (shaveEllipsis) {
                const content = clone.childNodes[0].textContent || '';

                truncatedText = content;

                if (
                    cutOffMoreCharacters &&
                    content.length > cutOffMoreCharacters
                ) {
                    truncatedText = content.substr(
                        0,
                        content.length - cutOffMoreCharacters
                    );
                }
            }

            let collapsedText = truncatedText;

            if (linkify) {
                // Need to take the unserialized content so the map util function
                // works properly
                const unserializedText = fullText.substr(
                    0,
                    truncatedText.length
                );
                const endIndex = this.getTruncatedTextMapIndex(
                    textMap,
                    unserializedText,
                    computedLines
                );

                collapsedText = Truncate.collapseLinkifiedText(
                    linkifiedText.slice(0, endIndex)
                );
            }

            this.setState(
                {
                    collapsedText: linkify ? collapsedText : truncatedText
                },
                () => {
                    this.props.onTruncate(this._displayEllipsis);
                }
            );
        });
    }

    render() {
        let ellipsis = '';
        let label;

        if (this._displayEllipsis && this.props.ellipsis) {
            ellipsis = this.props.ellipsis;
        }

        if (this.props.label) {
            label = <span className="u-fw-700">{this.props.label}: </span>;
        }

        return React.createElement(
            this.props.tagName,
            {
                className: this.props.className,
                ref: (e) => {
                    this._container = e;
                }
            },
            <Fragment>
                {label}
                <span
                    ref={(e) => {
                        this._text = e;
                    }}
                >
                    {this.state.collapsedText}
                </span>
                {ellipsis}
            </Fragment>
        );
    }
}
