import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import tinycolor from 'tinycolor2';

import CheckIcon from '../icons/check-mark';
import TastemakerIcon from '../icons/tastemaker';

export default class Verified extends Component {
    static propTypes = {
        user: PropTypes.object,
        className: PropTypes.string,
        small: PropTypes.bool,
        noTooltip: PropTypes.bool,
        size: PropTypes.number,
        color: PropTypes.string,
        style: PropTypes.object
    };

    static defaultProps = {
        small: false
    };

    render() {
        const { user, className, size, noTooltip, color, style } = this.props;

        if (
            !user.verified ||
            user.verified === 'no' ||
            user.verified === 'authenticated-pending' ||
            user.verified === 'authenticated-declined' ||
            user.verified === 'banned'
        ) {
            return null;
        }

        let icon = <CheckIcon />;
        let titleText = 'Verified Artist';

        if (user.verified === 'tastemaker') {
            icon = <TastemakerIcon />;
            titleText = 'Verified Tastemaker';
        }

        if (
            user.verified === 'authenticated' ||
            user.verified === 'verified-pending' ||
            user.verified === 'verified-declined'
        ) {
            titleText = 'Authenticated Artist';
        }

        const styles = style || {};

        if (size) {
            styles.width = `${size}px`;
            styles.height = `${size}px`;
        }

        if (color) {
            styles.background = color;

            const ogColor = tinycolor(color);
            if (ogColor.isLight()) {
                styles.color = '#222';
            }
        }

        const klass = classnames('verified', {
            'verified--tastemaker': user.verified === 'tastemaker',
            'verified--authenticated':
                user.verified === 'authenticated' ||
                user.verified === 'verified-pending' ||
                user.verified === 'verified-declined',
            'verified--small': this.props.small,
            [className]: className
        });

        return (
            <span
                className={klass}
                style={styles}
                title={titleText}
                data-tooltip={noTooltip ? null : titleText}
            >
                {icon}
            </span>
        );
    }
}
