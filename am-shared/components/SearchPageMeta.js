import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { buildSearchUrl } from 'utils/index';

export default class SearchPageMeta extends Component {
    static propTypes = {
        context: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        query: PropTypes.string.isRequired,
        page: PropTypes.number.isRequired,
        genre: PropTypes.string,
        verified: PropTypes.bool
    };

    render() {
        const { query, type, genre, context, page, verified } = this.props;
        const url = buildSearchUrl(query, {
            type,
            genre,
            context,
            page,
            verified
        });
        let title = 'Search New Music on Audiomack';

        if (query) {
            title = `${query} | Audiomack`;
        }

        const canonical = `${process.env.AM_URL}${url}`;
        // No app links exist as of now
        // const deepLinkPath = '/search';
        // const appDeepLink = `audiomack:/${deepLinkPath}`;

        return (
            <Helmet>
                <title>{title}</title>
                <link rel="canonical" href={canonical} />
                <meta property="og:title" content={title} />
                <meta property="og:url" content={canonical} />
                {/* <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />

                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />
                <link href={`android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack${deepLinkPath}`} rel="alternate" />
                <link href={`ios-app://${process.env.IOS_APP_ID}/audiomack${deepLinkPath}`} rel="alternate" />
                */}
                <meta name="robots" content="noindex,follow" />
            </Helmet>
        );
    }
}
