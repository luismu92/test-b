import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import analytics, { eventCategory, eventAction } from 'utils/analytics';
import { adLoaded, deactivateAd, setError } from 'redux/modules/ad';

import VideoAd from './VideoAd';

const COUNTDOWN_TIME = 5;

class VideoAdContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object.isRequired,
        onAdStarted: PropTypes.func,
        isMobile: PropTypes.bool,
        isEmbed: PropTypes.bool,
        showSkipButton: PropTypes.bool,
        currentUser: PropTypes.object,
        ad: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            countdownTime: COUNTDOWN_TIME,
            countdownStarted: false,
            isMounted: false,
            shouldShow: false
        };
    }

    componentDidMount() {
        // Doint this because the react 16 requires the server markup
        // matches the client markup near perfectly.
        // https://github.com/facebook/react/issues/8017#issuecomment-256351955
        // eslint-disable-next-line
        this.setState({
            isMounted: true
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.ad.active && !this.props.ad.active) {
            // eslint-disable-next-line
            this.setState({
                countdownTime: COUNTDOWN_TIME,
                countdownStarted: false
            });
        }
    }

    componentWillUnmount() {
        clearTimeout(this._countdownTimer);
        this._countdownTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSkipButtonClick = () => {
        if (this.state.countdownTime !== 0) {
            return;
        }

        const { dispatch } = this.props;

        dispatch(deactivateAd());

        this.setState({
            shouldShow: false
        });
    };

    handleAdScriptLoaded = () => {
        const { dispatch } = this.props;

        dispatch(adLoaded());
    };

    handleAdStarted = () => {
        this.setState({
            shouldShow: true
        });
        this.startSkipCountDown();
        this.props.onAdStarted();
    };

    handleAdEnded = () => {
        const { dispatch } = this.props;

        dispatch(deactivateAd());

        this.setState({
            shouldShow: false
        });
    };

    handleAdSkipped = () => {
        const { dispatch } = this.props;

        dispatch(deactivateAd());

        analytics.track(eventCategory.ad, {
            eventAction: eventAction.adSkip
        });

        this.setState({
            shouldShow: false
        });
    };

    handleAdError = (error) => {
        const { dispatch } = this.props;

        dispatch(setError(error));
        dispatch(deactivateAd());

        this.setState({
            shouldShow: false
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    startSkipCountDown() {
        clearTimeout(this._countdownTimer);

        const refreshCountdown = (time) => {
            const newTime = time - 1;

            this.setState({
                countdownStarted: true,
                countdownTime: newTime
            });

            if (newTime === 0) {
                return;
            }

            this._countdownTimer = setTimeout(() => {
                refreshCountdown(newTime);
            }, 1000);
        };

        refreshCountdown(COUNTDOWN_TIME);
    }

    isIos() {
        if (typeof window === 'undefined') {
            return false;
        }

        const userAgent =
            window.navigator.userAgent ||
            window.navigator.vendor ||
            window.opera;

        return (
            userAgent.match(/iPad/i) ||
            userAgent.match(/iPhone/i) ||
            userAgent.match(/iPod/i)
        );
    }

    render() {
        if (!this.state.isMounted) {
            return null;
        }

        if (this.isIos()) {
            console.log('skipping ios...');
            return null;
        }

        return (
            <VideoAd
                isMobile={this.props.isMobile}
                isEmbed={this.props.isEmbed}
                shouldShow={this.state.shouldShow}
                showSkipButton={this.state.showSkipButton}
                active={this.props.ad.active}
                currentUser={this.props.currentUser}
                location={this.props.location}
                onScriptLoaded={this.handleAdScriptLoaded}
                onSkipButtonClick={this.handleSkipButtonClick}
                countdownTime={this.state.countdownTime}
                countdownStarted={this.state.countdownStarted}
                onAdStarted={this.handleAdStarted}
                onAdEnded={this.handleAdEnded}
                onAdSkipped={this.handleAdSkipped}
                onAdError={this.handleAdError}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(VideoAdContainer);
