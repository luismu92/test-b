import React, { Component, Fragment } from 'react';
import analytics, {
    eventAction,
    eventCategory,
    eventLabel
} from 'utils/analytics';
import PropTypes from 'prop-types';

export default class ChartSponsoredAd extends Component {
    static propTypes = {
        mobile: PropTypes.bool
    };

    static defaultProps = {
        mobile: false
    };

    handleAdLogging = () => {
        analytics.track(eventCategory.ad, {
            // Put the name of the currently running ad followed by - Click in the label
            eventAction: eventAction.audioblocks,
            eventLabel: this.props.mobile
                ? eventLabel.chartSponsorAdMobile
                : eventLabel.chartSponsorAd
        });
    };

    render() {
        const props = {
            style: {
                width: '100%',
                height: '100%'
            }
        };

        return (
            <Fragment>
                <div style={props.style} />
                <a
                    onClick={this.handleAdLogging}
                    href="https://www.audioblocks.com/join/new-brand-vidback-ft?utm_v=2&utm_source=Audiomack&utm_medium=publication&utm_campaign=AB-email---99&campaign_date=2017-10-03&utm_content=---ABNJNB"
                    target="_blank"
                    rel="nofollow noopener"
                >
                    <img
                        src="/static/images/desktop/ads/charts-ad.png"
                        alt="StoryBlocks: Instant Inspiration! Get 7 days of downloads on us! Choose from 110,000 stock music, loops, and sound effects. Start Downloading Now."
                        style={{ margin: '0 auto' }}
                    />
                </a>
            </Fragment>
        );
    }
}
