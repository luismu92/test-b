import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { loadScript, shouldNotShowAd } from 'utils/index';

// import Playwire from './Playwire';

class PlaywireContainer extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        // ie '300x250', '728x90', '320x50'
        adSize: PropTypes.oneOf(['300x250', '728x90', '320x50']),
        adSuffix: PropTypes.oneOf(['atf', 'btf']),
        adCount: PropTypes.number,
        mobile: PropTypes.bool,
        currentUser: PropTypes.object,
        player: PropTypes.object
    };

    static defaultProps = {
        mobile: false,
        adSize: '300x250',
        adSuffix: 'atf'
    };

    constructor(props) {
        super(props);

        this.state = {
            scriptLoaded: false
        };
    }

    componentDidMount() {
        this._mounted = true;

        const id = 'tycheConfig';
        const { location, currentUser, mobile } = this.props;

        if (shouldNotShowAd(location.pathname, currentUser, mobile)) {
            return;
        }

        if (document.head.querySelector(`#${id}`)) {
            return;
        }

        const configScript = document.createElement('script');
        configScript.id = 'tycheConfig';
        configScript.innerHTML = `window.tyche = {
    mode: 'tyche',
    config: '//config.playwire.com/1023425/v2/websites/71444/banner.json',
    observerMode: {
        enabled: true,
        selector: 'react-view'
    }
};`;
        configScript.type = 'text/javascript';
        document.head.appendChild(configScript);

        // Potential race condition if you're loading
        // this on a page that needs the ad and then
        // rely on AppContainer to hide it when going
        // to a non ad supported page
        loadScript(
            'https://cdn.intergi.com/hera/tyche.js',
            'tyche',
            document.head
        );
    }

    componentWillUnmount() {
        this._mounted = false;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleScriptLoaded = () => {
        this.setState({
            scriptLoaded: true
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderContainerDiv() {
        /**
    <!-- 728 x 90 ATF Desktop -->
    <div data-pw-desk="leaderboard_atf"></div>
    <!-- 320 x 50 ATF Mobile -->
    <div data-pw-mobi="leaderboard_atf"></div>

    <!-- 728 x 90 BTF Desktop -->
    <div data-pw-desk="leaderboard_btf"></div>

    <!-- 300 x 250 ATF Desktop -->
    <div data-pw-desk="med_rect_atf"></div>
    <!-- 300 x 250 ATF Mobile -->
    <div data-pw-mobi="med_rect_atf"></div>

    <!-- 300 x 250 BTF Desktop -->
    <div data-pw-desk="med_rect_btf"></div>
    <!-- 300 x 250 BTF Mobile -->
    <div data-pw-mobi="med_rect_btf"></div>
         */

        const { mobile, adSize, adSuffix } = this.props;

        let attr = 'data-pw-desk';

        if (mobile) {
            attr = 'data-pw-mobi';
        }

        let dataValue;

        switch (adSize) {
            case '728x90':
            case '320x50':
                dataValue = `leaderboard_${adSuffix}`;
                break;

            default:
                dataValue = `med_rect_${adSuffix}`;
                break;
        }

        const props = {
            [attr]: dataValue
        };

        if (adSize === '300x250') {
            props.style = {
                backgroundImage:
                    'url(/static/images/desktop/am-partner-placeholder-300x250.jpg)'
            };
        }

        return <div {...props} />;
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(PlaywireContainer));
