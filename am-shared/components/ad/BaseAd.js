import { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { loadScript, shouldNotShowAd } from 'utils/index';

class BaseAd extends Component {
    static propTypes = {
        onScriptLoaded: PropTypes.func,
        onScriptError: PropTypes.func,
        src: PropTypes.string.isRequired,
        location: PropTypes.object,
        currentUser: PropTypes.object,
        isMobile: PropTypes.bool,
        id: PropTypes.string.isRequired,
        containerId: PropTypes.string
    };

    static defaultProps = {
        isMobile: false,
        onScriptLoaded() {},
        onScriptError() {}
    };

    componentDidMount() {
        const { id, src, location, currentUser, isMobile } = this.props;

        if (shouldNotShowAd(location.pathname, currentUser, isMobile)) {
            return;
        }

        // Instead of putting the sdk in the html and have it slow down the
        // page on load, we will insert it only when we are using this component.
        // This makes things a bit more self contained. This doesn't affect
        // any of the react code because it's being appended to the body
        // outside of the root react container
        loadScript(src, id)
            .then(() => {
                return this.props.onScriptLoaded(this.props.containerId);
            })
            .catch((e) => {
                console.error(e);
                this.props.onScriptError(e);
            });
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(BaseAd));
