import React, { Component } from 'react';
import PropTypes from 'prop-types';

import BaseAd from './BaseAd';

/**
 * Make sure PlaywireConfig is rendered in the DOM first
 * before rendering this component
 */
export default class Playwire extends Component {
    static propTypes = {
        onScriptLoaded: PropTypes.func,
        onScriptError: PropTypes.func,
        containerId: PropTypes.string,
        isMobile: PropTypes.bool
    };

    render() {
        const src = '//cdn.intergi.com/hera/tyche.js';

        return (
            <BaseAd
                id="tyche"
                src={src}
                isMobile={this.props.isMobile}
                onScriptLoaded={this.props.onScriptLoaded}
                onScriptError={this.props.onScriptError}
                containerId={this.props.containerId}
            />
        );
    }
}
