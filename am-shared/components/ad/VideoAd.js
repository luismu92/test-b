import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { buildQueryString } from 'utils/index';

import ImaSDK from './ImaSDK';

export default class VideoAd extends Component {
    static propTypes = {
        location: PropTypes.object,
        currentUser: PropTypes.object,
        countdownTime: PropTypes.number,
        onSkipButtonClick: PropTypes.func,
        onScriptLoaded: PropTypes.func,
        onAdStarted: PropTypes.func,
        onAdEnded: PropTypes.func,
        // onAdSkipped: PropTypes.func,
        onAdError: PropTypes.func,
        isMobile: PropTypes.bool,
        isEmbed: PropTypes.bool,
        active: PropTypes.bool,
        shouldShow: PropTypes.bool,
        showSkipButton: PropTypes.bool,
        countdownStarted: PropTypes.bool
    };

    static defaultProps = {
        onScriptLoaded() {},
        showSkipButton: false
    };

    componentDidMount() {
        this._width = 600;
        this._height = 338;
        this._prerollPlays = 0;
    }

    componentDidUpdate(prevProps) {
        if (prevProps.active !== this.props.active && this.props.active) {
            window.console.log('componentDidUpdate renderAd');
            this.renderAd();
        }

        if (this._adsManager && prevProps.active && !this.props.active) {
            window.console.log('componentDidUpdate destroy');
            this._prerollPlays = 0;
            this._adsManager.destroy();
        }
    }

    componentWillUnmount() {
        this._video = null;
        this._width = null;
        this._height = null;
        this._adContainer = null;
        this._adDisplayContainer = null;
        this._adManager = null;
        this._adLoader = null;
        this._prerollPlays = null;
        this._c = console;
        this._l = 'log';
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleImaLoaded = () => {
        window.console.log('ima loaded');
        this.setUpAdUnit();
        this.props.onScriptLoaded();
    };

    handleAdsManagerLoaded = (adsManagerLoadedEvent) => {
        if (typeof window.google === 'undefined') {
            window.console.log('no google');
            console.warn('Ad blocker blocked us');
            return;
        }

        console.warn('adsmanagerloaded');
        try {
            // Get the ads manager.
            // See API reference for contentPlayback
            this._adsManager = adsManagerLoadedEvent.getAdsManager(this._video);

            // Add listeners to the required events.
            const error = window.google.ima.AdErrorEvent.Type.AD_ERROR;
            const started = window.google.ima.AdEvent.Type.STARTED;
            const pause =
                window.google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED;
            const resume =
                window.google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED;
            const complete = window.google.ima.AdEvent.Type.ALL_ADS_COMPLETED;
            const skipped = window.google.ima.AdEvent.Type.SKIPPED;

            this._adsManager.addEventListener(started, this.handleAdStarted);
            this._adsManager.addEventListener(error, this.handleAdError);
            this._adsManager.addEventListener(
                pause,
                this.handleContentPauseRequested
            );
            this._adsManager.addEventListener(
                resume,
                this.handleContentResumeRequested
            );
            this._adsManager.addEventListener(
                complete,
                this.handleContentComplete
            );
            this._adsManager.addEventListener(
                skipped,
                this.handleContentComplete
            );

            // Initialize the ads manager. Ad rules playlist will start at this time.
            this._adsManager.init(
                this._width,
                this._height,
                window.google.ima.ViewMode.NORMAL
            );
            // Call start to show ads. Single video and overlay ads will
            // start at this time; this call will be ignored for ad rules, as ad rules
            // ads start when the adsManager is initialized.
            this._adsManager.start();
            window.console.log('handleAdsManagerLoaded');
        } catch (adError) {
            window.console.log('handleAdsManagerLoaded error', adError);
            // An error may be thrown if there was a problem with the VAST response.
            this.props.onAdError(adError);
        }
    };

    handleAdStarted = () => {
        console.warn('content started');
        window.console.log('handleAdStarted');
        this._prerollPlays += 1;
        this.props.onAdStarted();
    };

    // Not used as the native skip is handled by the complete
    // event handler at the moment
    // handleContentSkipped = () => {
    //     console.warn('content ended');
    //     window.console.log('handleContentSkipped');

    //     this.props.onAdSkipped();
    // }

    handleContentComplete = () => {
        console.warn('content ended');
        window.console.log('handleContentComplete');

        if (this._prerollPlays === 0) {
            window.console.log('Ads didnt play');
            this.props.onAdError(new Error('Ads didnt play'));
            return;
        }

        this.props.onAdEnded();
    };

    handleContentResumeRequested = () => {
        window.console.log('handleContentResumeRequested');
        console.warn('resume required');
    };

    handleContentPauseRequested = () => {
        window.console.log('handleContentPauseRequested');
        console.warn('pause required');
    };

    handleAdError = (adErrorEvent) => {
        window.console.log('handleAdError', adErrorEvent);
        // Handle the error logging and destroy the AdsManager
        console.error(adErrorEvent.getError());
        if (this._adsManager) {
            this._adsManager.destroy();
        }
        this.props.onAdError(adErrorEvent);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setUpAdUnit() {
        try {
            const video = this._video;

            this._adDisplayContainer = new window.google.ima.AdDisplayContainer(
                this._adContainer,
                video
            );

            this._adsLoader = new window.google.ima.AdsLoader(
                this._adDisplayContainer
            );

            const loaded =
                window.google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED;
            const error = window.google.ima.AdErrorEvent.Type.AD_ERROR;

            this._adsLoader.addEventListener(
                loaded,
                this.handleAdsManagerLoaded,
                false
            );
            this._adsLoader.addEventListener(error, this.handleAdError, false);
            window.console.log('setUpAdUnit');
        } catch (e) {
            window.console.log('setUpAdUnit err', e);
            this.props.onAdError(e);
        }
    }

    // See params here: https://support.google.com/adsense/answer/3112148?hl=en
    googleAdsenseVastTag() {
        const query = buildQueryString({
            ad_type: 'video_text_image',
            client: 'ca-video-pub-3858157454086512',
            description_url: process.env.AM_URL,
            videoad_start_delay: 0,
            hl: 'en',
            channel: 'Music',
            max_ad_duration: 30000
        });

        return `https://googleads.g.doubleclick.net/pagead/ads?${query}`;
    }

    // See params here: https://support.google.com/admanager/answer/1068325?hl=en
    genericCampaignTag() {
        const { location, currentUser, isMobile, isEmbed } = this.props;
        const isUploader =
            currentUser.isLoggedIn &&
            currentUser.profile.upload_count_excluding_reups > 0;
        const query = buildQueryString({
            sz: '640x480',
            iu: '/72735579/Fans-Video',
            impl: 's',
            gdfp_req: 1,
            env: 'vp',
            output: 'vast',
            unviewed_position_start: 1,
            url: `${process.env.AM_URL}${location.pathname}`,
            correlator: Date.now()
        });

        const customParams = encodeURIComponent(
            buildQueryString({
                uploader: isUploader ? '1' : '0',
                mobile: isMobile ? '1' : '0',
                embed: isEmbed ? '1' : '0'
            })
        );

        return `https://pubads.g.doubleclick.net/gampad/ads?${query}&cust_params=${customParams}`;
    }

    renderAd() {
        try {
            // Must be done as the result of a user action on mobile
            this._adDisplayContainer.initialize();

            // Request video ads.
            const adsRequest = new window.google.ima.AdsRequest();
            const vastChainCurrent = 0;
            const vastChain = [
                this.genericCampaignTag(),
                this.googleAdsenseVastTag()
            ].filter(Boolean);

            adsRequest.adTagUrl = vastChain[vastChainCurrent];

            let width = this._width;
            let height = this._height;

            if (this._adContainer) {
                const style = window.getComputedStyle(this._adContainer);

                width = parseInt(style.width, 10) || this._width;
                height = parseInt(style.height, 10) || this._height;

                this._width = width;
                this._height = height;
            }
            // Specify the linear and nonlinear slot sizes. This helps the SDK to
            // select the correct creative if multiple are returned.
            adsRequest.linearAdSlotWidth = width;
            adsRequest.linearAdSlotHeight = height;

            this._adsLoader.requestAds(adsRequest);
            window.console.log('renderAd success');
        } catch (err) {
            console.error(err);
            window.console.log('renderAd e', err);
            this.props.onAdError(err);
        }
    }

    render() {
        const {
            shouldShow,
            countdownTime,
            countdownStarted,
            showSkipButton
        } = this.props;
        const klass = classnames('ad-container', {
            'ad-container--hidden': !shouldShow
        });
        let adIsNativelySkippable = false;
        let button;

        if (this._adsManager) {
            const ad = this._adsManager.getCurrentAd();

            if (ad) {
                adIsNativelySkippable = ad.isSkippable();
            }
        }

        if (showSkipButton && countdownStarted && !adIsNativelySkippable) {
            let buttonText = 'Skip Ad';

            if (countdownTime > 0) {
                buttonText = `Skip in ${countdownTime}`;
            }
            button = (
                <button
                    className="ad-container__skip-button"
                    onClick={this.props.onSkipButtonClick}
                    disabled={countdownTime !== 0}
                >
                    {buttonText}
                </button>
            );
        }

        return (
            <div className={klass}>
                <ImaSDK
                    isMobile={this.props.isMobile}
                    onScriptLoaded={this.handleImaLoaded}
                    onScriptError={this.props.onAdError}
                />
                {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
                <video ref={(v) => (this._video = v)} />
                <div className="ad-container__inner">
                    {button}
                    <div
                        className="ad-container__ad"
                        ref={(v) => (this._adContainer = v)}
                    />
                </div>
            </div>
        );
    }
}
