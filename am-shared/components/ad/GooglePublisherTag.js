import React, { Component } from 'react';
import PropTypes from 'prop-types';

import BaseAd from './BaseAd';

export default class GooglePublisherTag extends Component {
    static propTypes = {
        slot: PropTypes.object.isRequired,
        disabled: PropTypes.bool.isRequired,
        refreshInterval: PropTypes.number,
        isMobile: PropTypes.bool,
        placeholder: PropTypes.string,
        containerWidth: PropTypes.string,
        containerHeight: PropTypes.string,
        targetingKey: PropTypes.string,
        targetingValue: PropTypes.string
    };

    static defaultProps = {
        refreshInterval: 30000,
        disabled: false,
        containerWidth: '100%',
        containerHeight: '100%'
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentWillUnmount() {
        clearTimeout(this._googleRefreshTimer);
        this._googleRefreshTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleGPTLoaded = () => {
        const googletag = window.googletag || { cmd: [] };
        const {
            slot,
            targetingKey,
            targetingValue,
            refreshInterval
        } = this.props;

        googletag.cmd.push(() => {
            const { adUnitPath, sizes, divId } = slot;

            const pubService = googletag.pubads();
            let adSlot = googletag.defineSlot(adUnitPath, sizes, divId);

            // If you load this on a page like the song page, then you navigate
            // to another song page, adSlot will be null presumably because
            // we already defined the slot and it is saved in memory.
            if (adSlot) {
                adSlot.addService(pubService);
            } else {
                const existingAdSlot = pubService.getSlots().find((obj) => {
                    return obj.getAdUnitPath() === adUnitPath;
                });

                if (existingAdSlot) {
                    adSlot = existingAdSlot;
                    pubService.refresh(adSlot);
                }
            }

            pubService.enableSingleRequest();
            pubService.enableVideoAds();
            googletag.companionAds().setRefreshUnfilledSlots(true);

            if (targetingKey && targetingValue) {
                pubService.setTargeting(targetingKey, targetingValue);
            }

            pubService.collapseEmptyDivs();
            googletag.enableServices();

            if (refreshInterval) {
                const refreshGoogleTag = () => {
                    googletag.display(divId);
                    pubService.refresh([adSlot]);
                    this._googleRefreshTimer = setTimeout(
                        refreshGoogleTag,
                        refreshInterval
                    );
                };

                refreshGoogleTag();
                return;
            }

            googletag.display(divId);
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        // @todo Is there any clean up we need to do here?
        if (this.props.disabled) {
            return null;
        }

        const style = {
            width: this.props.containerWidth,
            height: this.props.containerHeight,
            backgroundSize: 'cover'
        };

        if (this.props.placeholder) {
            style.backgroundImage = `url(${this.props.placeholder})`;
            style.backgroundRepeat = 'no-repeat';
            style.backgroundPosition = 'center top';
        }

        return (
            <div className="gp-wrap" style={style}>
                <div id={this.props.slot.divId} style={style} />
                <BaseAd
                    isMobile={this.props.isMobile}
                    id="ad-googlepublishertag"
                    src="https://www.googletagservices.com/tag/js/gpt.js"
                    onScriptLoaded={this.handleGPTLoaded}
                />
            </div>
        );
    }
}
