import React, { Component } from 'react';
import PropTypes from 'prop-types';

import BaseAd from './BaseAd';

export default class ImaSDK extends Component {
    static propTypes = {
        onScriptLoaded: PropTypes.func,
        onScriptError: PropTypes.func,
        isMobile: PropTypes.bool
    };

    render() {
        return (
            <BaseAd
                id="ad-imasdk"
                src="https://imasdk.googleapis.com/js/sdkloader/ima3.js"
                isMobile={this.props.isMobile}
                onScriptLoaded={this.props.onScriptLoaded}
                onScriptError={this.props.onScriptError}
            />
        );
    }
}
