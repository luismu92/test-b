import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { loadScript, waitUntil } from 'utils/index';

/**
 * They have a prebuilt react component but it doesnt really follow best
 * practices and it also uses Q which is unnecssary. I've edited their
 * example component from here:
 *
 * https://github.com/freestarcapital/pubfig-adslot-react-component
 *
 */
/* eslint react/jsx-no-bind: 0 */

// export default class FreestarContainer extends Component {
//     componentDidMount() {
//         this.refreshAdSlot();
//     }

//     componentDidUpdate(prevProps) {
//         const adUnit = this.getAdUnit();
//         const { onAdRefreshHook } = this.props;
//         const shouldDisable =
//             this.props.disabled && prevProps.disabled !== this.props.disabled;
//         const shouldRefresh =
//             prevProps.adRefresh !== this.props.adRefresh ||
//             (this.props.disabled !== prevProps.disabled &&
//                 !this.props.disabled);
//         if (shouldRefresh) {
//             this.refreshAdSlot().then(() => {
//                 return onAdRefreshHook(adUnit.placementName);
//             }).catch((err) => {
//                 console.error(err);
//             });
//         }

//         if (shouldDisable) {
//             this.deleteAdSlot();
//         }
//     }

//     componentWillUnmount() {
//         this.deleteAdSlot();
//     }

//     getAdUnit() {
//         const placementName = this.getPlacementName();
//         const slotId = this.getContainerId();
//         const slot = {
//             placementName: placementName,
//             slotId: slotId
//         };

//         return slot;
//     }

//     getContainerId() {
//         return this.getPlacementName(true);
//     }

//     // eslint-disable-next-line no-unused-vars
//     getPlacementName(withSuffix = false) {
//         // eslint-disable-next-line no-unused-vars
//         const { slotSuffix, isMobile, type } = this.props;
//         // const suffix = withSuffix && slotSuffix ? `_${slotSuffix}` : '';
//         const suffix = '';

//         if (isMobile) {
//             if (type === 'leaderboard') {
//                 return `audiomack_300x250_320x50_300x50_incontent_dynamic${suffix}`;
//             }

//             if (type === 'incontent') {
//                 return `audiomack_300x250_320x50_300x50_incontent_dynamic${suffix}`;
//             }

//             return null;
//         }

//         if (type === 'leaderboard') {
//             return `audiomack_728x90_320x50_adhesion${suffix}`;
//         }

//         if (type === 'incontent') {
//             return `audiomack_300x250_728x90_970x250_incontent_dynamic${suffix}`;
//         }

//         return null;
//     }

//     getFreestar() {
//         return waitUntil(() => {
//             return (
//                 window.freestar && window.googletag && window.googletag.apiReady
//             );
//         }).then(() => {
//             return window.freestar;
//         });
//     }

//     async refreshAdSlot() {
//         const adUnit = this.getAdUnit();
//         const { onNewAdSlotsHook } = this.props;

//         try {
//             await this.maybeSetup();
//             const freestar = await this.getFreestar();
//             if (!this.props.disabled && this.adSlotIsReady(adUnit)) {
//                 window.console.warn('Enabling ad unit', adUnit);
//                 freestar.newAdSlots(adUnit);
//                 onNewAdSlotsHook(adUnit.placementName);
//             }
//         } catch (err) {
//             window.console.error(err);
//         }
//     }

//     async deleteAdSlot() {
//         const adUnit = this.getAdUnit();
//         const { onDeleteAdSlotsHook } = this.props;

//         try {
//             const freestar = await this.getFreestar();

//             if (this.adSlotIsReady(adUnit)) {
//                 window.console.warn('Deleting ad unit', adUnit);
//                 freestar.deleteAdSlots(adUnit);
//                 onDeleteAdSlotsHook(adUnit.placementName);
//             }
//         } catch (err) {
//             window.console.error(err);
//         }
//     }

//     async maybeSetup() {
//         const { location } = this.props;
//         const id = 'freestar';

//         if (!document.head.querySelector(`#${id}`)) {
//             const debug = location.search.indexOf('fsdebug') !== -1;
//             const script = `https://a.pub.network/audiomack-com${
//                 debug ? '/qa/pubfig.min.js' : '/pubfig.min.js'
//             }`;

//             window.freestar = window.freestar || {};
//             window.freestar.hitTime = Date.now();
//             window.freestar.queue = window.freestar.queue || [];
//             window.freestar.config = window.freestar.config || {};
//             window.freestar.debug = debug;
//             window.freestar.config.enabled_slots = [];
//             await loadScript(script, id, document.head);
//         }
//     }

//     adSlotIsReady({ placementName, slotId }) {
//         return (
//             placementName && slotId && document.getElementById(placementName)
//         );
//     }

//     render() {
//         const adUnit = this.getAdUnit();

//         if (this.props.disabled) {
//             window.console.log('Removing from DOM', adUnit);
//             return null;
//         }

//         return (
//             <div style={{ textAlign: 'center' }} id={adUnit.slotId} />
//         );
//     }
// }

// FreestarContainer.propTypes = {
//     classList: PropTypes.array,
//     adRefresh: PropTypes.number,
//     onNewAdSlotsHook: PropTypes.func,
//     onDeleteAdSlotsHook: PropTypes.func,
//     onAdRefreshHook: PropTypes.func,

//     location: PropTypes.object.isRequired,
//     slotSuffix: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
//     type: PropTypes.oneOf(['incontent', 'leaderboard']).isRequired,
//     isMobile: PropTypes.bool.isRequired,
//     disabled: PropTypes.bool.isRequired
// };

// FreestarContainer.defaultProps = {
//     adRefresh: 0,
//     onNewAdSlotsHook: () => {},
//     onDeleteAdSlotsHook: () => {},
//     onAdRefreshHook: () => {},

//     isMobile: false,
//     disabled: false
// };

const getFreestar = () => {
    return waitUntil(() => {
        return window.freestar && window.googletag && window.googletag.apiReady;
    }).then(() => {
        return window.freestar;
    });
};

class FreestarAdSlot extends Component {
    componentDidMount() {
        this.maybeSetup();
        this.newAdSlots();
    }

    componentDidUpdate(prevProps) {
        const { adUnit, onAdRefreshHook } = this.props;
        const changedRefresh = prevProps.adRefresh !== this.props.adRefresh;
        const changedPage =
            prevProps.location.pathname !== this.props.location.pathname;

        if (changedRefresh || changedPage) {
            this.newAdSlots();
            onAdRefreshHook(adUnit.placementName);
        }
    }

    componentWillUnmount() {
        const { adUnit, onDeleteAdSlotsHook } = this.props;
        getFreestar()
            .then((freestar) => {
                if (this.adSlotIsReady(adUnit)) {
                    freestar.deleteAdSlots(adUnit);
                    onDeleteAdSlotsHook(adUnit.placementName);
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    maybeSetup() {
        const { location, disabled } = this.props;
        const id = 'freestar';

        if (!document.head.querySelector(`#${id}`) && !disabled) {
            const debug = location.search.indexOf('fsdebug') !== -1;
            const script = `https://a.pub.network/audiomack-com${
                debug ? '/qa/pubfig.min.js' : '/pubfig.min.js'
            }`;

            window.freestar = window.freestar || {};
            window.freestar.hitTime = Date.now();
            window.freestar.queue = window.freestar.queue || [];
            window.freestar.config = window.freestar.config || {};
            window.freestar.debug = debug;
            window.freestar.config.enabled_slots = [];
            loadScript(script, id, document.head);
        }
    }

    newAdSlots() {
        const { adUnit, onNewAdSlotsHook, channel } = this.props;
        getFreestar()
            .then((freestar) => {
                if (this.adSlotIsReady(adUnit)) {
                    freestar.newAdSlots(adUnit, channel);
                    onNewAdSlotsHook(adUnit.placementName);
                }
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    }

    adSlotIsReady({ placementName, slotId }) {
        return (
            placementName && slotId && document.getElementById(placementName)
        );
    }

    classes() {
        const { classList } = this.props;
        return classList ? classList.join(' ') : '';
    }

    render() {
        const { adUnit } = this.props;
        return <div className={this.classes()} id={adUnit.slotId} />;
    }
}

FreestarAdSlot.propTypes = {
    adUnit: PropTypes.shape({
        placementName: PropTypes.string.isRequired,
        slotId: PropTypes.string.isRequired,
        targeting: PropTypes.object
    }).isRequired,
    channel: PropTypes.string,
    classList: PropTypes.array,
    adRefresh: PropTypes.number,
    location: PropTypes.object,
    disabled: PropTypes.bool,
    onNewAdSlotsHook: PropTypes.func,
    onDeleteAdSlotsHook: PropTypes.func,
    onAdRefreshHook: PropTypes.func
};

FreestarAdSlot.defaultProps = {
    adUnit: {},
    channel: null,
    classList: [],
    adRefresh: 0,
    disabled: false,
    onNewAdSlotsHook: () => {},
    onDeleteAdSlotsHook: () => {},
    onAdRefreshHook: () => {}
};

// eslint-disable-next-line react/no-multi-comp
export default function Comp(props) {
    function getAdUnit() {
        const containerId = getPlacementName();
        const slotId = getContainerId();
        const slot = {
            placementName: containerId,
            slotId: slotId,
            targeting: props.extraData
        };

        return slot;
    }

    function getContainerId() {
        return getPlacementName(true);
    }

    // eslint-disable-next-line no-unused-vars
    function getPlacementName(withSuffix = false) {
        // eslint-disable-next-line no-unused-vars
        const { slotSuffix, isMobile, type } = props;
        const suffix = '';
        // const suffix = withSuffix && slotSuffix ? `_${slotSuffix}` : '';

        if (isMobile) {
            if (type === 'leaderboard') {
                return `audiomack_728x90_320x50_adhesion${suffix}`;
            }

            if (type === 'incontent') {
                return `audiomack_300x250_320x50_300x50_incontent_dynamic${suffix}`;
            }

            return null;
        }

        if (type === 'leaderboard') {
            return `audiomack_728x90_320x50_adhesion${suffix}`;
        }

        if (type === 'incontent') {
            return `audiomack_300x250_728x90_970x250_incontent_dynamic${suffix}`;
        }

        return null;
    }

    if (props.disabled && typeof window !== 'undefined') {
        console.log('Removing from DOM', getAdUnit());
        return null;
    }

    return (
        <FreestarAdSlot
            key="freestar"
            location={props.location}
            disabled={props.disabled}
            adUnit={getAdUnit()}
            // channel="custom_channel"
            // classList={['m-30', 'p-15', 'b-thin-red']}
            adRefresh={0}
            onNewAdSlotsHook={(placementName) =>
                console.log('freestar.newAdSlots() was called', {
                    placementName
                })
            }
            onDeleteAdSlotsHook={(placementName) =>
                console.log('freestar.deleteAdSlots() was called', {
                    placementName
                })
            }
            onAdRefreshHook={(placementName) =>
                console.log('adRefresh was called', { placementName })
            }
        />
    );
}

Comp.propTypes = {
    disabled: PropTypes.bool,
    location: PropTypes.object,
    extraData: PropTypes.object
};

Comp.defaultProps = {
    extraData: {}
};
