import React, { Component } from 'react';
import PropTypes from 'prop-types';

import BaseAd from './BaseAd';

export default class PageAd extends Component {
    static propTypes = {
        onScriptLoaded: PropTypes.func,
        onScriptError: PropTypes.func,
        isMobile: PropTypes.bool
    };

    render() {
        return (
            <BaseAd
                id="ad-pagead"
                src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
                isMobile={this.props.isMobile}
                onScriptLoaded={this.props.onScriptLoaded}
                onScriptError={this.props.onScriptError}
            />
        );
    }
}
