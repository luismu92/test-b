import React, { Fragment } from 'react';

export default function PrivacyContent() {
    return (
        <Fragment>
            <p>Effective as of February 10, 2016</p>

            <div
                className="anchor-links page-section"
                style={{ margin: '20px 0' }}
            >
                <p>
                    <strong>
                        <a href="#overview">1 - Introduction/Overview</a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#information">
                            2 - Information we collect about you
                        </a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#how-we-use">
                            3 - How we use your information
                        </a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#how-we-share">
                            4 - How we share your information
                        </a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#cookies">
                            5 - How we use cookies and technology
                        </a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#accessing-updating">
                            6 - Accessing and updating your information
                        </a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#transfer">
                            7 - Transfer to other countries/territories
                        </a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#links">8 - Links</a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#children">9 - Children</a>
                    </strong>
                </p>

                <p>
                    <strong>
                        <a href="#changes">
                            10 - Changes to the Privacy Policy
                        </a>
                    </strong>
                </p>
            </div>

            <p>
                This Privacy Policy explains how Audiomack, a music service and
                mobile app owned and operated by Audiomack Inc., collects, uses,
                and manages your personal data. By using the Platform (defined
                below) and/or by registering for a user account, you consent to
                our Privacy Policy, so it’s important you read it carefully. If
                you do not agree to any term, provision, or concept in this
                Privacy Policy, you should not use the Audiomack website, mobile
                website, mobile apps services or platform. Please contact{' '}
                <a href="mailto:support@audiomack.com">support@audiomack.com</a>{' '}
                with any privacy-related questions.
            </p>

            <p>
                Note: this Privacy Policy only applies to the Audiomack website,
                app, and any additional music delivery mechanism or platform
                hereafter developed and/or utilized in any way, shape or form by
                Audiomack Inc. (collectively hereby referred to as the
                “Platform”), When using the Platform you may encounter links or
                ads for other services, platforms, apps, and/or websites that
                are not a part of the Services. Audiomack assumes no
                responsibility for the privacy policies or activities of such
                third parties, and users should be sure to check the privacy
                policy of any third party site or platform they use.
            </p>

            <div
                id="overview"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>1 - Introduction/Overview</h2>

                <p>
                    Our Privacy Policy will provide you with a detailed
                    explanation of how we collect and use your data on the
                    Platform. Before we delve into the details, however, we’d
                    like to provide you with a brief overview of our philosophy
                    as it pertains to user privacy and to highlight some key
                    basic ideas.
                </p>

                <p>
                    By using the Platform, you consent to the collection of
                    certain types of data (outlined below). You should fully
                    understand what we do (and don’t do) with your personal
                    information and data.
                </p>

                <p>
                    We’d also like to offer a brief overview of what you’re
                    consenting to by using the Platform. All of these points
                    will be explained in more detail below.
                </p>

                <p>By using the Platform, you consent to;</p>

                <p>Our use of cookies and other related technologies;</p>

                <p>
                    The transfer of your information to a 3rd party outside of
                    the country you reside in;
                </p>

                <p>
                    The collection, processing, use, and sharing of your
                    information;
                </p>

                <p>
                    The public availability of certain kinds of personal
                    information you elect to share by registering for an
                    account, and your controls for sharing such information.
                </p>
            </div>

            <div
                id="information"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>2 - Information we collect about you</h2>

                <p>
                    Visiting and using the Platform does not require you to
                    provide any personal information, however, by electing to
                    register for a user account you will provide us with certain
                    information as you complete the registration process:
                </p>

                <p>
                    <strong>Opt-In/Registration Data:</strong>
                </p>

                <p>
                    To register for an Audiomack account, you must provide us
                    with a valid email address as well as your age and gender.
                    If you use Social Login (e.g. Facebook) to sign up, we may
                    receive some of this information from the social network.
                    You may <i>elect</i> to provide us with certain other
                    personal information - personal/artist biography, hometown,
                    and links to your profiles on other social media platforms
                    (Facebook, Twitter, etc).
                </p>

                <p>
                    We also collect and display the date of your initial user
                    registration on your user profile as a month and year - e.g.
                    August ‘14.
                </p>

                <p>
                    You will also provide certain personal data if you use our
                    Contact Us tools, on-site forms, or send us an email
                    regarding support, your account, or other topics.
                </p>

                <p>
                    You <i>may</i> also provide other personal information
                    through your use of the site and posting of audio material,
                    in the track description, comments, or title fields.
                </p>

                <p>
                    <strong>
                        Data we collect on usage, analytics, and cookies:
                    </strong>
                </p>

                <p>
                    By using the Platform, you consent to our use of different
                    technologies and methods that collect information about how
                    you use, access, and interact with the Platform, which may
                    include any of the following;
                </p>

                <p>
                    Technical data such as your IP Address, geolocation, cookie
                    data, URLs visited, mobile device ID and information, device
                    attributes, operating system, network connection, browser
                    type, language, and mobile app version.
                </p>

                <p>
                    Details of any searches you make using the Audiomack search
                    tools, including the date and time searches were made.
                </p>

                <p>
                    Metrics concerning the duration, time, and frequency of your
                    visits to and interactions with the Platform. Details of
                    your subscription (if applicable), as well as information
                    about your interactions with the Platform, specifically
                    including but not limited to: plays, favorites, re-ups,
                    playlist use, and interaction with other Audiomack members
                    and/or artists.
                </p>

                <p>
                    Device sensor data including lat/long, geolocation,
                    altitude, and measurements from your mobile device's
                    gyroscope, GPS receivers, accelerometers, and other sensors.
                </p>

                <p>
                    Details of your interaction with any third-party apps,
                    widgets, modals, and/or advertisements which are linked to
                    or made available on the Platform.
                </p>

                <p>
                    Information about the website you visited immediately prior
                    to your interaction with the Platform. Details of any
                    content (music, song information, descriptions) you upload
                    to the Platform.
                </p>

                <p>
                    Details concerning your interaction with any Platform
                    methods of communication specifically including but not
                    limited to emails and push/in-app notifications, such as
                    whether you opened and/or clicked on such messages.
                </p>

                <p>
                    We may also obtain information about you from any 3rd party
                    providers/partners and/or advertisers on the Platform which
                    measure ad interaction.
                </p>
            </div>

            <div
                id="how-we-use"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>3 - How we use your information</h2>

                <p>
                    We may use your information for any of the following
                    purposes;
                </p>

                <p>
                    To operate your Audiomack account, and to identify you when
                    you use the Platform. Your email address and password are
                    used for this identification;
                </p>

                <p>
                    To identify you and your actions (uploads, favorites,
                    re-up’s, comments, etc) on the Platform; To provide
                    technical functionality to the Platform; for research and
                    development of new products, services, or functionality; and
                    analyze your use of the Platform;
                </p>

                <p>To provide you with technical support or assistance;</p>

                <p>
                    To communicate with you for marketing, support, and/or
                    promotional purposes through email, text message, push, or
                    in-app notifications;
                </p>

                <p>
                    To detect fraud or attempted manipulation of our charts,
                    trending sections, or proprietary algorithms. Audiomack
                    expressly reserves the right to remove any and all content
                    from the Platform that Audiomack in its sole discretion
                    reasonably believes is violating any copyright laws and/or
                    is being used in any way, shape or form to manipulate any
                    measurement system on the Platform (specifically including
                    but not limited to any charts, trending sections,
                    proprietary algorithms, play counts, favorites, re-up’s,
                    download counts;
                </p>

                <p>
                    To provide you with advertising, features, information,
                    links, messaging, or other content based on your geolocation
                    and time of use;
                </p>

                <p>
                    To determine the location, orientation, and acceleration of
                    a mobile device;
                </p>

                <p>
                    To ensure compliance with our Terms of Service (TOS), this
                    Privacy Policy, or with any applicable state, local,
                    federal, or international Copyright Law;
                </p>

                <p>
                    Information about your subscription tier (Gold, Platinum) if
                    applicable;
                </p>

                <p>
                    To show you relevant ads on behalf of advertisers and/or
                    sponsors, which may include location and sensor data, along
                    with demographic data you make available to us or through
                    connected third party services, recent search history, and
                    behavior with content, ads, or other users on the Platform;
                </p>

                <p>
                    To provide you with a customized experience on the Platform.
                </p>

                <p>
                    To provide the content creators and rightsholders who
                    provide audio content to the audiomack platform with
                    statistics on who is listening to their content.
                </p>
            </div>

            <div
                id="how-we-share"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>4 - How we share your information</h2>

                <p>
                    Information about you or your activity on the Platform may
                    be shared in two ways: the first is information you share
                    via your public profile on the Platform is publicly visible
                    to other users of the Platform and thus is shared and the
                    second is data and information Audiomack collects on you (as
                    described in section two above) that Audiomack may in their
                    sole discretion share with reputable third parties in hopes
                    of enhancing the Platform, its analytics and services.
                </p>

                <p>Information you share via your use of the Platform:</p>

                <p>
                    Any information you provide in your public profile
                    (username, name, location, social media feeds) and/or use of
                    the Platform (plays, favorites, following, re-ups,
                    playlists) is public and may be seen by other users on the
                    Platform or by Audiomack administrators.
                </p>

                <p>Information we share:</p>

                <p>
                    Audiomack uses several reputable third party partners and
                    services, based in the U.S., to enhance the functionality,
                    reliability, utility, and profitability of the Platform and
                    to provide certain specialized services for our
                    administrators and users. We may also store your email
                    address, device id, and other tokens in services that enable
                    us to send targeted messages to you via email, push
                    notification, and on-platform messaging. Although Audiomack
                    is not responsible for the Privacy Policy or Data Security
                    of said third party services, we emphasize data security and
                    integrity as a top priority when selecting services to work
                    with.
                </p>

                <p>
                    Audiomack will disclose your personal information in any
                    case where it is necessary to do so to comply with a
                    criminal complaint, subpoena, court order, or legitimate law
                    enforcement inquiry.
                </p>

                <p>
                    Audiomack may disclose your personal information if we feel
                    it is necessary to do so to protect our intellectual
                    property, rights, business interests, or the safety or
                    reliability of our platform, users, employees, or
                    stakeholders.
                </p>

                <p>
                    Audiomack may transfer your information to any person or
                    party that acquires all or a substantial portion of our
                    company or assets, or in the case of a merger, acquisition,
                    or if the company becomes insolvent.
                </p>

                <p>
                    Notwithstanding any other provision, we partner with third
                    parties that collect information across various channels,
                    including offline and online, for purposes of delivering
                    more relevant advertising to you or your business. Our
                    partners may place or recognize a cookie on your computer,
                    device, or directly in our emails/communications, and we may
                    share personal information with them if you have submitted
                    such information to us, such as hashed email address, or
                    device ID. Our partners may link the hashed or obfuscated
                    personal information we share with them to the cookie stored
                    on your browser or device, and they may collect information
                    such as your IP address, browser or operating system type
                    and version, and demographic or inferred-interest
                    information. Our partners use this information to recognize
                    you across different channels and platforms, including but
                    not limited to, computers, mobile devices, and Smart TVs,
                    over time for advertising, analytics, attribution, and
                    reporting purposes. For example, our partners may deliver an
                    ad to you in your web browser based on a purchase you made
                    in a physical retail store, or they may send a personalized
                    marketing email to you based on the fact that you visited a
                    particular website. Your data may be transferred outside of
                    the country from which it was originally collected. To learn
                    more about interest-based advertising in general and to opt
                    out, please visit{' '}
                    <a
                        href="http://www.aboutads.info/choices"
                        rel="nofollow noopener"
                        target="_blank"
                    >
                        http://www.aboutads.info/choices
                    </a>
                    . To opt-out of the use of your mobile device ID for
                    targeted advertising, please see{' '}
                    <a
                        href="http://www.aboutads.info/appchoices"
                        rel="nofollow noopener"
                        target="_blank"
                    >
                        http://www.aboutads.info/appchoices
                    </a>
                    .
                </p>
            </div>

            <div
                id="cookies"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>5 - How we use cookies and technology</h2>

                <p>
                    Audiomack uses cookies and related technology to enhance the
                    Platform, provide you with a consistent experience, record
                    login and registration information, and to serve you more
                    relevant advertisements. Some ads served by third parties
                    may use limited information about you and your interaction
                    with the Platform to enhance the targeting or retargeting of
                    advertisements and marketing campaigns. You may refuse to
                    accept cookies from the Audiomack and/or the Platform by
                    declining the Desktop Notification policy that appeared to
                    you the first time you viewed Audiomack’s Desktop
                    Platform(s).
                </p>

                <p>
                    A cookie is a small file placed on your computer or mobile
                    device when you visit a website which helps our site and our
                    3rd party advertising and data partners recognize your
                    device the next time you visit the site. Cookies are
                    essential for many simple functions such as recognizing a
                    repeat visitor as logged in, and providing access to your
                    account, favorites, playlists, and profile. Audiomack
                    utilizes two common types of cookies - session based cookies
                    (which expire once you close your web browser) and
                    persistent cookies (which stay active until deleted or
                    deactivate after a set period of time). Other technologies
                    include pixel tags (transparent images which help us
                    determine if a page or email has been viewed by a user), web
                    beacons (also known as web bugs, which serve a similar
                    function to pixel tags), and web storage. Additionally,
                    mobile users may be tracked using Device ID and/or hashed
                    data to identify repeat mobile usage. These combined
                    technologies are used both by Audiomack and our 3rd party
                    partners to ensure essential Platform functionality; target,
                    serve, measure, and track advertising delivery, targeting,
                    optimization, and performance; measure Platform analytics
                    and usage; and to measure user and audience behavior,
                    trends, demographics, and location.
                </p>

                <p>
                    Audiomack does not currently respond to or acknowledge Do
                    Not Track (DNT) requests.
                </p>
            </div>

            <div
                id="accessing-updating"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>6 - Accessing and updating your information</h2>

                <p>
                    You may update your personal information at any time by
                    visiting your Dashboard page in your user profile. Please
                    bear in mind that profile data that is updated may take a
                    few hours or another reasonable period of time to update
                    internally.
                </p>

                <p>
                    Your data will be deleted if you delete your account which
                    can be done by accessing your account on our desktop site
                    and clicking the “Delete account” button. However, please
                    bear in mind that it may still show up on internet search
                    engines for some time after deletion due to caching.
                </p>

                <p>
                    Please note, logging out of or deleting the mobile app from
                    your device does not delete your user account.
                </p>

                <p>
                    Users accessing audiomack from an android or iOS mobile
                    device may opt out of any sales of their personal
                    information by using their mobile platform settings (on iOS,
                    by enabling the “Limit Ad Tracking” setting, and on Android
                    devices, by enabling the “Opt out of Ads Personalization”
                    setting).
                </p>

                <p>
                    <strong>FOR European and California Users:</strong> In the
                    event you withdraw your consent for Audiomack to collect
                    data from you, you shall have the right to obtain from
                    Audiomack the erasure of personal data concerning yourself
                    without undue delay and Audiomack shall have the obligation
                    to erase your personal data without undue delay.
                </p>

                <p>
                    In the event you elect to withdraw your consent for
                    Audiomack to collect data from you, you can contact us by
                    sending an email{' '}
                    <a href="mailto:privacy@audiomack.com">
                        privacy@audiomack.com
                    </a>{' '}
                    from the email address associated with your account OR by
                    logging into your account and submitting a{' '}
                    <a
                        href="https://audiomack.zendesk.com/"
                        target="_blank"
                        rel="noopener nofollow"
                    >
                        support ticket
                    </a>{' '}
                    requesting that Audiomack erase any and all personal data
                    concerning yourself. Once received, Audiomack shall process
                    your request in the order it was received and shall erase
                    your data without undue delay. You may use these same
                    methods to request that Audiomack send to you all the data
                    we have stored that is related to your account.
                </p>
            </div>

            <div
                id="transfer"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>7 - Transfer to other countries/territories</h2>

                <p>
                    Audiomack Inc. is based in the United States and follows all
                    applicable U.S. laws concerning data storage and privacy.
                    Although our third party providers and services are based in
                    the United States, there may in some circumstances be cases
                    where data is transmitted to servers outside of the U.S. as
                    part of the normal operation of the Platform. Please be
                    aware that not all countries have the same data storage and
                    privacy laws as the United States, and by using the
                    Audiomack Platform you consent to the storage, transfer,
                    collection, and transmission of your data to other
                    territories as necessary.
                </p>
            </div>

            <div id="links" className="page-section" style={{ marginTop: 20 }}>
                <h2>8 - Links</h2>

                <p>
                    The Platform contains links to third-party content,
                    advertisements, and sites that are not owned by nor
                    affiliated with Audiomack Inc.. We cannot be held
                    accountable for the content or privacy policy of third-
                    party sites. Be sure to read their privacy policies to
                    determine if you are comfortable with how they handle your
                    information.
                </p>
            </div>

            <div
                id="children"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>9 - Children</h2>

                <p>
                    The Platform is not intended for use by those under the age
                    of 13. We never knowingly or purposefully collect
                    information from anyone under the age of 13, or under the
                    legal age limit in their respective territory (“Age Limit”),
                    under any circumstances. If you are under the Age Limit, you
                    should not use the Platform and are expressly prohibited
                    from registering for a user account, or transmitting any
                    data or personal information on the Platform. If you are the
                    parent/guardian of a child under the Age Limit and believe
                    we have collected such information, please contact us
                    immediately at{' '}
                    <a href="mailto:support@audiomack.com">
                        support@audiomack.com
                    </a>{' '}
                    and it will be promptly removed.
                </p>
            </div>

            <div
                id="changes"
                className="page-section"
                style={{ marginTop: 20 }}
            >
                <h2>10 - Changes to our Privacy Policy</h2>

                <p>
                    We may, at our sole discretion, change the terms of this
                    Privacy Policy at any time and for any reason. In the event
                    that we do change this Privacy Policy, we will post
                    notification of updates both within the Policy itself and
                    via notifications throughout the Platform and specifically
                    on the Audiomack website and mobile app. Please be sure to
                    read all updates to the Privacy Policy thoroughly before
                    continuing to use the Audiomack platform. Your continued use
                    of the Platform constitutes acceptance of any new or changed
                    terms in this Privacy Policy.
                </p>
            </div>

            <p>
                Please contact{' '}
                <a href="mailto:support@audiomack.com">support@audiomack.com</a>{' '}
                with any questions or concerns about the Privacy Policy.
            </p>
        </Fragment>
    );
}
