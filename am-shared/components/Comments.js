import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import {
    ORDER_BY_VOTE,
    ORDER_BY_DATE,
    ORDER_BY_OLD,
    LIMIT_FEATHER,
    LIMIT_DISPLAY_CHILDREN
} from 'constants/comment';
import { buildDynamicImage, artistComment, getMusicUrl } from 'utils/index';
import Avatar from 'components/Avatar';
import VoteUpIcon from 'icons/thumbs-up-solid';
import VoteDownIcon from 'icons/thumbs-down-solid';
import SortIcon from 'icons/sort';
import CommentIcon from 'icons/comment';
import CloseIcon from 'icons/close-thin';
import ThreeDots from 'icons/three-dots-vertical';
import Verified from 'components/Verified';

import BodyClickListener from './BodyClickListener';
import AndroidLoader from './loaders/AndroidLoader';

import CommentActions from './CommentActions';

export default class Comments extends Component {
    static propTypes = {
        isMobile: PropTypes.bool,
        item: PropTypes.object,
        currentUser: PropTypes.object,
        comment: PropTypes.object,
        total: PropTypes.number,
        className: PropTypes.string,
        emptyInput: PropTypes.bool,
        expanded: PropTypes.bool,
        onCommentPost: PropTypes.func,
        onCommentLoadMore: PropTypes.func,
        onCommentVote: PropTypes.func,
        onCommentAction: PropTypes.func,
        onVoteForGuestUser: PropTypes.func,
        onToggleReplyForm: PropTypes.func,
        onToggleChildren: PropTypes.func,
        childExpanded: PropTypes.array,
        currentReplyTo: PropTypes.string,
        onCommentInputFocus: PropTypes.func,
        onCommentInputChange: PropTypes.func,
        onCommentSortClick: PropTypes.func,
        onCommentActionClick: PropTypes.func,
        onCommentSort: PropTypes.func,
        sortTooltipActive: PropTypes.bool,
        activeCommentContext: PropTypes.string,
        singleCommentUuid: PropTypes.string
    };

    componentWillUnmount() {
        this._tooltipButton = null;
    }

    renderLoader(loading) {
        if (!loading) {
            return null;
        }

        return (
            <div className="dashboard-panel__loader">
                <AndroidLoader />
            </div>
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p>{String(error)}</p>;
    }

    renderLoadMore(nextPage) {
        const {
            item,
            onCommentLoadMore,
            total,
            expanded,
            isMobile,
            singleCommentUuid
        } = this.props;

        if (typeof singleCommentUuid !== 'undefined') {
            return (
                <div className="u-text-center">
                    <Link to={getMusicUrl(item)}>
                        <button className="button button--med u-fs-14 u-ls-n-05 u-br-25">
                            <CommentIcon className="button__icon" />
                            View all comments
                        </button>
                    </Link>
                </div>
            );
        }

        if (
            total < LIMIT_FEATHER ||
            (total >= LIMIT_FEATHER && expanded && nextPage === null) ||
            (isMobile && nextPage === null)
        ) {
            return null;
        }

        let buttonText = 'Load more comments';

        if (!nextPage && !isMobile) {
            buttonText = 'Expand comments';
        }

        return (
            <div className="u-text-center">
                <button
                    className="button button--med u-fs-14 u-ls-n-05 u-br-25"
                    onClick={onCommentLoadMore}
                >
                    <CommentIcon className="button__icon" />
                    {buttonText}
                </button>
            </div>
        );
    }

    renderCommentItem(
        commentItem,
        index,
        allowPostReply = true,
        thread = null
    ) {
        const { uuid, content, deleted, artist } = commentItem;

        if (!content || !artist) {
            return null;
        }

        const { item, comment, currentUser, isMobile } = this.props;
        const threadId = thread !== null ? thread.uuid : null;
        const artistUrl =
            item.type === 'playlist'
                ? item.artist.url_slug
                : item.uploader.url_slug;

        const postDatetime = (
            <Link
                to={`/${item.type}/${artistUrl}/${item.url_slug}#comment-${
                    commentItem.id
                }`}
                title={moment
                    .unix(commentItem.created_at)
                    .format('MMMM Do YYYY, h:mm A')}
                className="u-text-gray7"
            >
                {moment.unix(commentItem.created_at).fromNow()}
            </Link>
        );

        // Artist name with possible link
        let artistLink = 'deleted';
        let verified;

        if (artistComment(commentItem, item)) {
            verified = (
                <Verified
                    user={
                        item.type === 'playlist' ? item.artist : item.uploader
                    }
                    className="comment-author__check"
                    size={9}
                />
            );
        }

        const authorClass = classnames('comment-author', {
            'comment-author__og': artistComment(commentItem, item)
        });

        if (artist.name) {
            artistLink = (
                <Link
                    className={authorClass}
                    to={`/artist/${artist.url_slug}`}
                    data-popover
                >
                    {artist.name}
                    {verified}
                </Link>
            );
        }

        let displayContent = content;
        let artistImage = artist.image_base || artist.image;
        let showContent = true;
        let children =
            Array.isArray(commentItem.children) &&
            commentItem.children.length > 0
                ? commentItem.children
                : [];
        let userAndTimeStamp = (
            <span>
                {artistLink} {postDatetime}
            </span>
        );

        children = children.filter(
            (child) =>
                !child.deleted &&
                (!child.artist.comment_banned || child.artist.admin)
        );
        commentItem.children = children;

        const maskComment = (artist.comment_banned && !artist.admin) || deleted;

        if (maskComment) {
            if (children.length > 0) {
                displayContent = '<em>deleted</em>';
                showContent = false;
                userAndTimeStamp = 'deleted';
                artistImage = null;
            } else {
                return null;
            }
        }

        let replyForm = null;
        let replyButton = null;
        let childComments = null;
        let commentActionContent = (
            <div className="comment-item__actions">&nbsp;</div>
        );

        if (allowPostReply) {
            let replyCount;

            if (children.length > 0) {
                childComments = (
                    <div className="comment-item__children u-spacing-top-25">
                        {this.renderChildComments(
                            commentItem.children,
                            commentItem
                        )}
                        {this.renderChildCommentMoreButton(commentItem)}
                    </div>
                );

                replyCount = (
                    <span className="comment-item__reply-count u-fw-600">
                        {commentItem.children.length}
                    </span>
                );
            }

            replyButton = (
                <button
                    className="u-spacing-left-em button--link comment-item__reply u-fs-14 u-ls-n-05 u-lh-14 u-fw-600 u-tt-uppercase"
                    rel={commentItem.uuid}
                    onClick={this.props.onToggleReplyForm}
                >
                    <CommentIcon className="comment-item__reply-icon" />{' '}
                    {replyCount}
                    <span className="u-spacing-left-20 u-d-inline-block u-fw-600">
                        Reply
                    </span>
                </button>
            );

            if (commentItem.uuid === this.props.currentReplyTo && showContent) {
                replyForm = this.renderPostCommentForm(commentItem.uuid);
            }
        }

        let voteStatus = null;
        const userVoteStatus =
            comment.userVotes[`${comment.kind}-${comment.id}`];

        if (typeof userVoteStatus !== 'undefined') {
            for (const vote of userVoteStatus) {
                if (vote.uuid === commentItem.uuid) {
                    voteStatus = vote.vote_up;
                }
            }
        }

        const contentBlock = (
            <div
                className="comment__content u-fw-600 u-ls-n-06"
                dangerouslySetInnerHTML={{ __html: displayContent }}
            />
        ); //eslint-disable-line

        if (showContent) {
            commentActionContent = (
                <div className="comment-item__actions u-spacing-top-10 u-pos-relative">
                    <div>
                        <span className="u-fw-600 u-fs-14">
                            {commentItem.vote_total}
                        </span>
                        {this.renderVoteButton(
                            voteStatus,
                            true,
                            commentItem.uuid,
                            threadId
                        )}
                        {this.renderVoteButton(
                            voteStatus,
                            false,
                            commentItem.uuid,
                            threadId
                        )}
                        {replyButton}
                    </div>
                </div>
            );
        }

        const commentClass = classnames('comment-item', {
            'comment-item--has-children':
                commentItem.children && commentItem.children.length > 0,
            'comment-item--top-level': allowPostReply,
            'comment-item--deleted': deleted
        });

        let adminActions;

        if (!isMobile && !deleted) {
            adminActions = (
                <CommentActions
                    comment={commentItem}
                    currentUser={currentUser}
                    onCommentActionClick={this.props.onCommentAction}
                    item={item}
                />
            );
        }

        if (isMobile && !maskComment) {
            adminActions = (
                <button
                    className="comment-item__admin-actions"
                    onClick={this.props.onCommentActionClick}
                    data-uuid={commentItem.uuid}
                    data-thread={commentItem.thread}
                    data-comment-id={commentItem.id}
                    data-user={commentItem.user_id}
                    data-artist={commentItem.artist.artist_id}
                >
                    <ThreeDots />
                </button>
            );
        }

        let commentUserImage = this.renderAvatar(artistImage);

        if (!maskComment && typeof artist.url_slug !== 'undefined') {
            commentUserImage = (
                <Link to={`/artist/${artist.url_slug}`}>
                    {this.renderAvatar(artistImage)}
                </Link>
            );
        }

        const commentUser = (
            <div className="comment-item__user">
                <div className="account-summary__info">{commentUserImage}</div>
            </div>
        );

        return (
            <div className={commentClass} key={index} rel={uuid}>
                {commentUser}
                <div className="comment-item__content">
                    <div className="u-padding-right-50">
                        {contentBlock}
                        <div className="comment-item__meta u-fs-12 u-fw-600 u-ls-n-05 u-text-gray7">
                            {userAndTimeStamp}
                        </div>
                        {commentActionContent}
                        {replyForm}
                    </div>
                    {childComments}
                </div>
                {adminActions}
            </div>
        );
    }

    renderAvatar(image) {
        const size = 50;
        const artwork = buildDynamicImage(image, {
            width: size,
            height: size,
            max: true
        });

        const retinaArtwork = buildDynamicImage(image, {
            width: size * 2,
            height: size * 2,
            max: true
        });
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        return (
            <Avatar
                className="artist-page__avatar media-item__figure comments__avatar"
                type="artist"
                image={artwork}
                srcSet={srcSet}
                size={50}
                rounded
            />
        );
    }

    renderVoteButton(voteStatus = null, voteUpButton = true, uuid, threadId) {
        const { onCommentVote } = this.props;
        const buttonText = voteUpButton ? (
            <span className="comment-item__vote-icon comment-item__vote-icon--up u-d-inline-block">
                <VoteUpIcon className="" />
            </span>
        ) : (
            <span className="comment-item__vote-icon comment-item__vote-icon--down u-d-inline-block">
                <VoteDownIcon className="" />
            </span>
        );
        let clickHandle = onCommentVote;
        const tooltip = voteUpButton ? 'Like' : 'Dislike';

        if (!this.props.currentUser.isLoggedIn) {
            clickHandle = this.props.onVoteForGuestUser;
        }

        let button = (
            <button
                className="u-spacing-left-em"
                onClick={clickHandle}
                data-model-uuid={uuid}
                data-model-thread={threadId}
                data-model-vote={voteUpButton}
                aria-label={tooltip}
                data-tooltip={tooltip}
            >
                {buttonText}
            </button>
        );

        if (voteUpButton) {
            if (voteStatus === true) {
                button = (
                    <span
                        className="comment-item__vote-icon comment-item__vote-icon--up u-text-orange u-spacing-left-em u-d-inline-block"
                        title={'Liked'}
                        data-tooltip={'Liked'}
                    >
                        <VoteUpIcon />
                    </span>
                );
            }
        }

        if (!voteUpButton && voteStatus === false) {
            button = (
                <span
                    className="comment-item__vote-icon comment-item__vote-icon--down u-text-orange u-spacing-left-em u-d-inline-block"
                    title={'Disliked'}
                    data-tooltip={'Disliked'}
                >
                    <VoteDownIcon />
                </span>
            );
        }

        return button;
    }

    renderChildCommentMoreButton(thread) {
        if (
            thread.children.length <= LIMIT_DISPLAY_CHILDREN ||
            this.props.childExpanded.indexOf(thread.uuid) !== -1
        ) {
            return null;
        }

        const moreCount = thread.children.length - LIMIT_DISPLAY_CHILDREN;
        const moreLabel = moreCount === 1 ? 'Reply' : 'Replies';

        return (
            <div className="u-spacing-top-15">
                <button
                    className="u-text-orange u-fs-15 u-ls-n-06 u-fw-700 u-lh-13 u-tt-uppercase"
                    onClick={this.props.onToggleChildren}
                    rel={thread.uuid}
                >
                    View {moreCount} More {moreLabel}
                </button>
            </div>
        );
    }

    renderChildComments(comments = [], thread) {
        if (comments === null || comments.length === 0) {
            return null;
        }

        let commentsToShow = comments;

        if (this.props.childExpanded.indexOf(thread.uuid) === -1) {
            commentsToShow = comments.slice(0, LIMIT_DISPLAY_CHILDREN);
        }

        return commentsToShow.map((item, i) => {
            return this.renderCommentItem(item, i, false, thread);
        });
    }

    renderComments(comments = []) {
        const { expanded, total } = this.props;

        if (comments === null || comments.length === 0) {
            return null;
        }

        const commentList = comments.map((item, i) => {
            return this.renderCommentItem(item, i, true);
        });

        const wrapClass = classnames(
            'comments-wrap u-padding-top-25 u-padding-bottom-40',
            {
                'comments-wrap--expanded': expanded,
                'comments-wrap--feathered': total >= LIMIT_FEATHER
            }
        );

        return <div className={wrapClass}>{commentList}</div>;
    }

    renderPostCommentForm(uuid = null) {
        const { currentUser, emptyInput } = this.props;

        const userCanComment =
            currentUser.isLoggedIn && currentUser.profile.can_comment;
        let threadHidden = null;
        let placeholder = 'Write a public comment...';

        if (uuid !== null) {
            threadHidden = (
                <input type="hidden" value={uuid} name="comment_thread" />
            );
            placeholder = 'Add your reply...';
        }

        if (!currentUser.isLoggedIn) {
            placeholder = 'Please login to write a comment...';
        } else if (!currentUser.profile.can_comment) {
            placeholder = 'Sorry, but you have been banned from commenting...';
        }

        let avatarImage;

        if (currentUser.isLoggedIn) {
            avatarImage =
                currentUser.profile.image_base || currentUser.profile.image;
        }

        let resetButton;

        if (!emptyInput) {
            resetButton = (
                <button className="comment__form-input-clear" type="reset">
                    <CloseIcon className="u-d-block" />
                </button>
            );
        }

        return (
            <form
                className="form u-clearfix comment__form u-padding-bottom-20"
                onSubmit={this.props.onCommentPost}
                onReset={this.props.onCommentInputChange}
            >
                <div className="u-d-flex u-d-flex--align-center u-d-flex--wrap">
                    <div>{this.renderAvatar(avatarImage)}</div>
                    <div className="comment__form-input u-pos-relative">
                        <label
                            htmlFor="ctext"
                            style={{ position: 'absolute', top: -9999 }}
                        >
                            {placeholder}
                        </label>
                        {resetButton}
                        <textarea
                            name="comment_content"
                            placeholder={placeholder}
                            required
                            id="ctext"
                            onFocus={this.props.onCommentInputFocus}
                            onChange={this.props.onCommentInputChange}
                            className="comment__form-textarea"
                        />
                    </div>
                    <input
                        className="button button--pill u-right comment__form-button u-br-25 u-fs-14 u-fw-700 u-ls-n-05"
                        type="submit"
                        value="Comment"
                        disabled={!userCanComment}
                    />
                    {threadHidden}
                </div>
            </form>
        );
    }

    renderTitleBar() {
        const {
            total,
            isMobile,
            onCommentSort,
            sortTooltipActive,
            activeCommentContext
        } = this.props;

        const totalCount =
            total === 1 ? `${total} Comment` : `${total} Comments`;

        let orderBy = 'Top Comments';

        if (activeCommentContext === ORDER_BY_DATE) {
            orderBy = 'Newest First';
        } else if (activeCommentContext === ORDER_BY_OLD) {
            orderBy = 'Oldest First';
        }

        let sortSubmenu;

        if (!isMobile) {
            const submenuClass = classnames(
                'tooltip sub-menu sub-menu--condensed tooltip--radius tooltip--right-arrow feed-bar__tooltip comment-sort__tooltip',
                {
                    'tooltip--active': sortTooltipActive
                }
            );

            const submenuItems = [
                {
                    text: 'Top Comments',
                    action: ORDER_BY_VOTE,
                    className:
                        activeCommentContext === ORDER_BY_VOTE
                            ? 'menu-button__list-item--active'
                            : ''
                },
                {
                    text: 'Newest First',
                    action: ORDER_BY_DATE,
                    className:
                        activeCommentContext === ORDER_BY_DATE
                            ? 'menu-button__list-item--active'
                            : ''
                },
                {
                    text: 'Oldest First',
                    action: ORDER_BY_OLD,
                    className:
                        activeCommentContext === ORDER_BY_OLD
                            ? 'menu-button__list-item--active'
                            : ''
                }
            ].map((item, i) => {
                return (
                    <li
                        key={i}
                        className={`menu-button__list-item ${item.className}`}
                    >
                        <button
                            onClick={onCommentSort}
                            data-action={item.action}
                        >
                            {item.text}
                        </button>
                    </li>
                );
            });

            sortSubmenu = (
                <div>
                    <ul className={submenuClass}>{submenuItems}</ul>
                    <BodyClickListener
                        shouldListen={sortTooltipActive}
                        onClick={this.props.onCommentSortClick}
                        ignoreDomElement={this._tooltipButton}
                    />
                </div>
            );
        }

        let sortButton;

        if (total >= 2) {
            sortButton = (
                <div>
                    <button
                        className="comment-sort"
                        onClick={this.props.onCommentSortClick}
                        ref={(e) => {
                            this._tooltipButton = e;
                        }}
                    >
                        <SortIcon className="comment-sort__icon" />
                        <span className="comment-sort__label u-spacing-left-5 u-fs-13 u-fw-700 u-ls-n-07">
                            {orderBy}
                        </span>
                    </button>
                    {sortSubmenu}
                </div>
            );
        }

        let labelStyle = {
            fontSize: 18
        };

        if (!isMobile) {
            labelStyle = {
                fontSize: 14
            };
        }

        return (
            <div className="comment-sort-wrap u-padding-y-10 u-d-flex u-d-flex--justify-between">
                <h2 className="u-ls-n-06 u-lh-11" style={labelStyle}>
                    <strong>{totalCount}</strong>
                </h2>
                {sortButton}
            </div>
        );
    }

    render() {
        const { comments, nextPage, loading, error } = this.props.comment;
        const { total, item, isMobile, className } = this.props;

        const innerProps = [];

        if (
            typeof item !== 'undefined' &&
            item.type !== 'playlist' &&
            item.description
        ) {
            innerProps.className = 'u-padding-top-10';
        }

        const klass = classnames('row expanded column small-24 comments', {
            'comments--empty u-padding-top-5': total < 1,
            'comments--has-comments': total >= 1,
            'u-padding-top-10':
                total >= 1 && typeof item !== 'undefined' && item.description,
            'u-bg-white': !isMobile
        });

        const containerClass = classnames('', {
            [className]: className,
            row: !isMobile,
            'row mobile': isMobile
        });

        return (
            <div id="comments" className={containerClass}>
                <div className={klass}>
                    <div className="column small-24 u-padding-bottom-20 u-pos-relative">
                        <div {...innerProps}>
                            {this.renderTitleBar()}
                            {this.renderLoader(loading && !error)}
                            {this.renderError(error)}
                            {this.renderPostCommentForm()}
                            {this.renderComments(comments)}
                            {this.renderLoadMore(nextPage)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
