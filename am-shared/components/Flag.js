import React from 'react';
import PropTypes from 'prop-types';

import { getCountryName } from 'utils/stats';

import styles from './Flag.module.scss';

function Flag({ country, width = 30, height = 16 }) {
    const flagPath = `/static/images/shared/flags/${country.toLowerCase()}.svg`;

    const style = {
        width: `${width}px`,
        height: `${height}px`
    };

    return (
        <span className={styles.flag} style={style}>
            <img src={flagPath} alt={`Flag of ${getCountryName(country)}`} />
        </span>
    );
}

Flag.propTypes = {
    country: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number
};

export default Flag;
