import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CameraIcon from 'icons/camera';

export default class Avatar extends Component {
    static propTypes = {
        children: PropTypes.object,
        style: PropTypes.object,
        image: PropTypes.string,
        srcSet: PropTypes.string,
        className: PropTypes.string,
        zoomImage: PropTypes.string,
        size: PropTypes.number,
        transitionTime: PropTypes.number,
        center: PropTypes.bool,
        lazyLoad: PropTypes.bool,
        dirty: PropTypes.bool,
        radius: PropTypes.bool,
        square: PropTypes.bool,
        rounded: PropTypes.bool,
        editable: PropTypes.bool,
        stack: PropTypes.bool,
        onInputChange: PropTypes.func,
        onClick: PropTypes.func
    };

    static defaultProps = {
        onInputChange() {},
        style: {},
        dirty: false,
        editable: false,
        center: false,
        rounded: true,
        transitionTime: 120
    };

    constructor(props) {
        super(props);

        this.state = {
            zoomed: false
        };
    }

    componentWillUnmount() {
        const unmounting = true;

        this.resetState(unmounting);
        this._originalWidth = null;
        this._originalHeight = null;
        this._originalScale = null;
        this._button = null;
        this._originalButtonStyle = null;
        this._image = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleZoomClick = (e) => {
        const button = e.currentTarget;
        const image = button.querySelector('img');
        const { zoomImage, transitionTime } = this.props;
        const maxPercent = 0.8;

        if (this.state.zoomed || !image) {
            return;
        }

        this._button = button;
        this._image = image;

        this.loadImage(zoomImage)
            .then((evt) => {
                const { naturalWidth, naturalHeight } = evt.target;
                const width = Math.min(
                    naturalWidth,
                    window.innerWidth * maxPercent
                );
                const height = Math.min(
                    naturalHeight,
                    window.innerHeight * maxPercent
                );
                const {
                    top: originalTop,
                    left: originalLeft,
                    width: originalWidth,
                    height: originalHeight
                } = image.getBoundingClientRect();

                let ratio;

                if (width > height) {
                    ratio = height / naturalHeight;
                } else {
                    ratio = width / naturalWidth;
                }

                const finalWidth = naturalWidth * ratio;
                const finalHeight = naturalHeight * ratio;

                this._originalWidth = originalWidth;
                this._originalHeight = originalHeight;
                this._originalScale = `scale(${originalWidth /
                    finalWidth}, ${originalHeight / finalHeight})`;
                this._originalButtonStyle = button.getAttribute('style');

                this.setState(
                    {
                        zoomed: true
                    },
                    () => {
                        this.addBodyClick();
                        this.addImageClick();
                        document.body.appendChild(image);

                        window.requestAnimationFrame(() => {
                            image.src = zoomImage;
                            image.style.zIndex = 99;
                            image.style.cursor = 'zoom-out';
                            image.style.position = 'fixed';
                            image.style.left = `${originalLeft}px`;
                            image.style.top = `${originalTop}px`;
                            image.style.transformOrigin = 'top left';
                            image.style.height = `${finalHeight}px`;
                            image.style.width = `${finalWidth}px`;
                            image.style.transition = 'none';
                            image.style.transform = this._originalScale;

                            button.style.width = `${originalWidth}px`;
                            button.style.height = `${originalHeight}px`;

                            window.requestAnimationFrame(() => {
                                const transformX =
                                    (window.innerWidth - finalWidth) / 2 -
                                    originalLeft;
                                const transformY =
                                    (window.innerHeight - finalHeight) / 2 -
                                    originalTop;

                                image.style.transition = `transform ${transitionTime}ms ease-in`;
                                image.style.transform = `translateX(${transformX}px) translateY(${transformY}px) translateZ(0)`;
                            });
                        });
                    }
                );

                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleImageClick = () => {
        this.resetState();
    };

    handleBodyClick = () => {
        this.resetState();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    addBodyClick() {
        window.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyClick() {
        window.removeEventListener('click', this.handleBodyClick);
    }

    addImageClick() {
        if (!this._image) {
            return;
        }

        this._image.addEventListener('click', this.handleImageClick, false);
    }

    removeImageClick() {
        if (!this._image) {
            return;
        }

        this._image.removeEventListener('click', this.handleImageClick);
    }

    resetState(unmounting = false) {
        const { image: originalImage } = this.props;

        if (unmounting && this._image) {
            this._image.parentElement.removeChild(this._image);
            this._image = null;
        }

        this.resetImage(originalImage);
        this.removeBodyClick();
        this.removeImageClick();
    }

    resetImage(originalImage) {
        const image = this._image;

        if (!image) {
            return;
        }

        const { transitionTime } = this.props;

        image.style.transform = this._originalScale;

        setTimeout(() => {
            window.requestAnimationFrame(() => {
                if (this._button) {
                    this._button.style = this._originalButtonStyle;
                    this._button.appendChild(image);
                }

                image.style.transition = 'none';
                image.style.height = null;
                image.style.width = null;
                image.style.top = null;
                image.style.left = null;
                image.style.transform = null;
                image.src = originalImage;
                image.style.position = null;
                image.style.transformOrigin = null;
                image.style.zIndex = null;
                image.style.cursor = 'zoom-in';

                window.requestAnimationFrame(() => {
                    image.style.transition = null;
                });
            });
        }, transitionTime);

        this.setState({
            zoomed: false
        });
    }

    loadImage(src) {
        return new Promise((resolve, reject) => {
            const img = new Image();

            img.onload = resolve;
            img.onerror = reject;

            img.src = src;
        });
    }

    render() {
        const {
            zoomImage,
            size,
            style,
            center,
            rounded,
            square,
            radius,
            className,
            editable,
            dirty,
            onInputChange,
            onClick,
            srcSet,
            stack,
            lazyLoad
        } = this.props;
        const image =
            this.props.image ||
            'https://d3m79pznqer0b2.cloudfront.net/default-artist-image.jpg';
        const { zoomed } = this.state;
        const styles = {
            ...style
        };

        const imageStyle = {};

        if (size) {
            styles.width = `${size}px`;
            styles.height = `${size}px`;
        }

        if (center) {
            styles.margin = '0 auto';
        }

        if (rounded) {
            styles.borderRadius = '50%';
            imageStyle.borderRadius = '50%';
        }

        if (square) {
            styles.borderRadius = '0';
            imageStyle.borderRadius = '0';
        }

        if (radius) {
            styles.borderRadius = '10px';
        }

        if (zoomImage) {
            imageStyle.cursor = 'zoom-in';
        }

        const klass = classnames('avatar-container', {
            [className]: className,
            'avatar-container--dirty': dirty,
            'avatar-container--zoomable': !!zoomImage,
            'avatar-container--active': zoomed,
            'avatar-container--square': square,
            'avatar-container--stack': stack
        });

        let editableOverlay;

        if (editable) {
            let camera;
            let requirements;

            if (!dirty) {
                camera = <CameraIcon className="avatar-container__icon" />;
                requirements = (
                    <p className="user-profile__avatar-req u-text-center">
                        Recommended size 800x800, JPG or PNG
                    </p>
                );
            }

            editableOverlay = (
                <span className="avatar-container__input-wrap">
                    <input
                        type="file"
                        name="image"
                        accept="image/*"
                        onChange={onInputChange}
                    />
                    {camera}
                    {requirements}
                </span>
            );
        }

        return (
            /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
            <div
                className={klass}
                style={styles}
                onClick={zoomImage ? this.handleZoomClick : onClick}
            >
                {editableOverlay}
                <img
                    src={image}
                    style={imageStyle}
                    alt=""
                    srcSet={srcSet}
                    loading={lazyLoad ? 'lazy' : 'eager'}
                />
                {this.props.children}
            </div>
        );
    }
}
