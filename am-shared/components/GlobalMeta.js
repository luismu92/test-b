import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

export default class GlobalMeta extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        embed: PropTypes.bool
    };

    static defaultProps = {
        embed: false
    };

    render() {
        const { location, embed } = this.props;
        const description =
            'Audiomack is a free music streaming and discovery platform that allows artists to share their music and fans to discover new artists, songs, albums, mixtapes, playlists and more.';
        const canonical = `${process.env.AM_URL}${location.pathname}`;
        const title = 'Free Music Streaming & Sharing | Audiomack';
        const appDeepLink = 'audiomack://';

        let websiteAction;
        let canonicalTag;

        if (location.pathname === '/') {
            canonicalTag = <link rel="canonical" href={canonical} />;
            websiteAction = (
                <script type="application/ld+json">
                    {JSON.stringify([
                        {
                            name: process.env.AM_NAME,
                            url: process.env.AM_URL,
                            sameAs: [
                                'https://twitter.com/audiomack',
                                'https://www.facebook.com/audiomack',
                                'https://plus.google.com/+audiomack',
                                'https://www.instagram.com/audiomack',
                                'https://www.linkedin.com/company/audiomack'
                            ],
                            potentialAction: [
                                {
                                    '@type': 'SearchAction',
                                    target: `${
                                        process.env.AM_URL
                                    }/search?q={search_term_string}`,
                                    'query-input':
                                        'required name=search_term_string'
                                },
                                {
                                    '@type': 'SearchAction',
                                    target: `android-app://${
                                        process.env.ANDROID_APP_PACKAGE
                                    }/audiomack/search/?q={search_term_string}`,
                                    'query-input':
                                        'required name=search_term_string'
                                }
                            ],
                            '@type': 'WebSite',
                            publisher: {
                                '@type': 'Organization',
                                logo: {
                                    '@type': 'ImageObject',
                                    url: `${
                                        process.env.AM_URL
                                    }/static/images/fb-graphic_275x275.jpg`
                                }
                            },
                            '@context': 'https://schema.org'
                        }
                    ])}
                </script>
            );
        }

        let noindex;

        if (embed) {
            noindex = <meta name="robots" content="noindex,follow" />;
        }

        return (
            <Helmet>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta property="og:url" content={canonical} />
                <meta
                    property="og:image"
                    content="/static/images/fb-graphic_275x275.jpg"
                />
                <meta property="og:locale" content="en_US" />
                {/* <link rel="alternate" type="application/rss+xml" title="Trending Music Feed"
                    href="/rss/trending"
                />*/}
                {noindex}
                {canonicalTag}
                <meta name="twitter:domain" content={process.env.AM_URL} />
                <meta name="twitter:site" content="@audiomack" />
                <meta name="twitter:description" content={description} />
                <meta
                    name="twitter:app:name:iphone"
                    content={process.env.IOS_APP_NAME}
                />
                <meta
                    name="twitter:app:id:iphone"
                    content={process.env.IOS_APP_ID}
                />
                <meta
                    name="twitter:app:name:googleplay"
                    content={process.env.ANDROID_APP_NAME}
                />
                <meta
                    name="twitter:app:id:googleplay"
                    content={process.env.ANDROID_APP_PACKAGE}
                />
                <meta
                    property="al:ios:app_name"
                    content={process.env.IOS_APP_NAME}
                />
                <meta
                    property="al:ios:app_store_id"
                    content={process.env.IOS_APP_ID}
                />
                <meta
                    property="al:android:app_name"
                    content={process.env.ANDROID_APP_NAME}
                />
                <meta
                    property="al:android:package"
                    content={process.env.ANDROID_APP_PACKAGE}
                />
                <meta property="al:web:should_fallback" content="true" />
                <meta
                    property="fb:app_id"
                    content={process.env.FACEBOOK_APP_ID}
                />

                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />

                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />
                <link
                    href={`android-app://${
                        process.env.ANDROID_APP_PACKAGE
                    }/audiomack/`}
                    rel="alternate"
                />
                <link
                    href={`ios-app://${process.env.IOS_APP_ID}/audiomack/`}
                    rel="alternate"
                />
                <meta name="referrer" content="origin" />
                {/* <link rel="alternate" hrefLang="x-default" href={process.env.AM_URL} />
                <link rel="alternate" hrefLang="en" href={process.env.AM_URL} /> */}
                <link
                    href="/opensearch.xml"
                    rel="search"
                    title={`Search on ${process.env.AM_NAME}`}
                    type="application/opensearchdescription+xml"
                />
                {websiteAction}
            </Helmet>
        );
    }
}
