import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import {
    allGenresMap,
    timeMap,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    CHART_TYPE_DAILY,
    CHART_TYPE_WEEKLY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY
} from 'constants/index';
import { getChartUrl } from 'utils/index';

export default class BrowsePageMeta extends Component {
    static propTypes = {
        page: PropTypes.number.isRequired,
        onLastPage: PropTypes.bool.isRequired,
        activeGenre: PropTypes.string.isRequired,
        activeContext: PropTypes.string.isRequired,
        activeTimePeriod: PropTypes.string.isRequired
    };

    getChartsTimePeriodText(activeTimePeriod) {
        let periodText = '';

        switch (activeTimePeriod) {
            case CHART_TYPE_DAILY:
                periodText = 'Today';
                break;

            case CHART_TYPE_WEEKLY:
                periodText = 'This Week';
                break;

            case CHART_TYPE_MONTHLY:
                periodText = 'This Month';
                break;

            case CHART_TYPE_YEARLY:
                periodText = 'This Year';
                break;

            default:
                periodText = 'All-Time';
        }

        return periodText;
    }

    getDescription(activeGenre, activeContext, activeTimePeriod) {
        const periodText = this.getChartsTimePeriodText(
            activeTimePeriod
        ).toLowerCase();
        const genre = this.getGenreText(activeGenre).toLowerCase();
        const genreText = genre ? ` ${genre}` : '';

        switch (activeContext) {
            case COLLECTION_TYPE_SONG:
            case COLLECTION_TYPE_ALBUM:
                return `Stream and download the most popular${genreText} ${activeContext} of ${periodText} on Audiomack with our top music charts.`;

            case COLLECTION_TYPE_TRENDING:
                return 'Stream, discover and download the most popular new music on Audiomack.';

            case COLLECTION_TYPE_RECENTLY_ADDED:
                return 'Listen to the newest music added to Audiomack';

            default: {
                console.log(activeContext, 'No Context Found to match');
                return '';
            }
        }
    }

    getGenreText(activeGenre) {
        if (!activeGenre) {
            return '';
        }

        return allGenresMap[activeGenre] || '';
    }

    getCanonical(activeGenre, activeContext, activeTimePeriod) {
        let genrePrefix = '';
        let periodText = '';

        if (activeGenre) {
            genrePrefix = `${activeGenre}/`;
        }

        if (timeMap[activeTimePeriod]) {
            periodText = `/${timeMap[activeTimePeriod]}`;
        }

        switch (activeContext) {
            case COLLECTION_TYPE_SONG:
            case COLLECTION_TYPE_ALBUM:
                return `${
                    process.env.AM_URL
                }/${genrePrefix}${activeContext}${periodText}`;

            case COLLECTION_TYPE_TRENDING:
                return `${process.env.AM_URL}/trending-now`;

            case COLLECTION_TYPE_RECENTLY_ADDED:
                return `${process.env.AM_URL}/${activeContext}`;

            default: {
                console.log(activeContext, 'No Context Found to match');
                return null;
            }
        }
    }

    getTitle(activeGenre, activeContext, activeTimePeriod) {
        const genre = this.getGenreText(activeGenre);
        const periodText = this.getChartsTimePeriodText(
            activeTimePeriod
        ).toLowerCase();
        const genreText = genre ? ` ${activeGenre}` : '';
        let prefix;

        switch (activeContext) {
            case COLLECTION_TYPE_TRENDING: {
                prefix = `Trending${genreText} Music`;
                break;
            }

            case COLLECTION_TYPE_SONG:
            case COLLECTION_TYPE_ALBUM: {
                prefix = `Top${genreText} ${activeContext} of ${periodText}`;
                break;
            }

            case COLLECTION_TYPE_RECENTLY_ADDED: {
                prefix = `Recent${genreText} releases`;
                break;
            }

            default:
                console.log(activeContext, 'No Context Found to match');
                return 'Audiomack';
        }

        return `${prefix} | Listen on Audiomack`;
    }

    getAppLinkPath({ context = '', genre = '', period = '' } = {}) {
        if (!context && !genre && !period) {
            return '/';
        }

        let contextText = '';
        let genreText = '';
        let periodText = '';
        let hrefString = '';

        if (context !== '') {
            contextText = `/${context}`;
        }

        if (genre !== '' && typeof genre !== 'undefined') {
            genreText = `/${genre}`;
        }

        if (period !== '') {
            periodText = `/${period}`;
        }

        switch (context) {
            case COLLECTION_TYPE_SONG:
            case COLLECTION_TYPE_ALBUM:
                hrefString = `${genreText}${contextText}${periodText}`;
                break;

            case COLLECTION_TYPE_TRENDING:
                hrefString = `${genreText}/trending`;
                break;

            default:
                return '/';
        }

        return hrefString;
    }

    renderPrev(canonical, page) {
        if (page === 1) {
            return null;
        }

        const hrefString = `${canonical}/page/${page - 1}`;

        return <link href={hrefString} rel="prev" />;
    }

    renderNext(canonical = '', page, onLastPage) {
        if (onLastPage) {
            return null;
        }

        const hrefString = `${canonical}/page/${page + 1}`;

        return <link href={hrefString} rel="next" />;
    }

    render() {
        const {
            activeTimePeriod,
            activeGenre,
            activeContext,
            page,
            onLastPage
        } = this.props;
        const title = this.getTitle(
            activeGenre,
            activeContext,
            activeTimePeriod
        );
        const canonical = getChartUrl({
            host: process.env.AM_URL,
            genre: activeGenre,
            context: activeContext,
            timePeriod: activeTimePeriod
        });
        const deepLinkPath = this.getAppLinkPath({
            context: activeContext,
            genre: activeGenre,
            period: activeTimePeriod,
            page
        });
        const appDeepLink = `audiomack:/${deepLinkPath}`;
        const description = this.getDescription(
            activeGenre,
            activeContext,
            activeTimePeriod
        );

        return (
            <Helmet>
                <title>{title}</title>,
                <link rel="og:title" href={title} />
                <link rel="canonical" href={canonical} />
                <link rel="og:url" href={canonical} />
                <meta name="description" content={description} />
                <meta name="og:description" content={description} />
                <link
                    href={`android-app://${
                        process.env.ANDROID_APP_PACKAGE
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
                <link
                    href={`ios-app://${
                        process.env.IOS_APP_ID
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
                {this.renderPrev(canonical, page)}
                {this.renderNext(canonical, page, onLastPage)}
                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />
                <meta name="twitter:description" content={description} />
                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />
            </Helmet>
        );
    }
}
