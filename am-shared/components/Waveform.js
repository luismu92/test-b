import React, { Component } from 'react';
import PropTypes from 'prop-types';

import defaultData from 'utils/defaultWaveformData';

export default class Waveform extends Component {
    static propTypes = {
        onMouseDown: PropTypes.func,
        onTouchStart: PropTypes.func,
        data: PropTypes.array,
        dataAsString: PropTypes.string,
        canvasProps: PropTypes.object,
        reflection: PropTypes.bool,
        fill: PropTypes.number,
        spaceBetween: PropTypes.number,
        markWidth: PropTypes.number,
        color: PropTypes.string,
        fillColor: PropTypes.string
    };

    static defaultProps = {
        onMouseDown() {},
        onTouchStart() {},
        canvasProps: {},
        dataAsString: '',
        data: [],
        reflection: false,
        spaceBetween: 1,
        markWidth: 2,
        color: 'rgba(199, 199, 199, 1)',
        fillColor: '#f79b00'
    };

    constructor(props) {
        super(props);

        // This component has internal state because the canvas tag needs
        // to have explicit height and width to draw correctly. We give the
        // developer control by just measuring ourselves and setting it directly
        // and allowing the developer to size the container around the canvas
        // however they want.
        this.state = {
            dpi: 1
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this._ctx = this._canvas.getContext('2d');
        this.setInternalValues(this._canvas);

        // Sometimes on render the container is not properly sized causing
        // a blurry waveform.
        setTimeout(() => {
            this.setInternalValues(this._canvas);
        }, 300);

        window.addEventListener('resize', this.handleWindowResize, false);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.fill !== this.props.fill) {
            return true;
        }

        if (nextProps.dataAsString !== this.props.dataAsString) {
            return true;
        }

        if (
            nextState.height !== this.state.height ||
            nextState.width !== this.state.width ||
            nextState.dpi !== this.state.dpi
        ) {
            return true;
        }

        return false;
    }

    componentDidUpdate(prevProps) {
        if (
            this.props.dataAsString !== prevProps.dataAsString ||
            !this.state.width ||
            !this.state.height
        ) {
            this.setInternalValues(this._canvas);
        }

        if (prevProps.fill !== this.props.fill) {
            this.renderWaveform();
        }
    }

    componentWillUnmount() {
        this._canvas = null;
        this._ctx = null;
        this._width = null;
        this._height = null;
        this._maxHeightValue = null;
        this._resizeTimer = null;
        window.removeEventListener('resize', this.handleWindowResize);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowResize = () => {
        clearTimeout(this._resizeTimer);
        this._resizeTimer = setTimeout(() => {
            this.setInternalValues(this._canvas);
        }, 200);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    // http://stackoverflow.com/questions/26941168/javascript-interpolate-an-array-of-numbers
    getInterpolatedData(data = [], maxPoints = 10) {
        const newData = [];
        const springFactor = (data.length - 1) / (maxPoints - 1);

        newData[0] = data[0];

        for (let i = 1; i < maxPoints - 1; i++) {
            const tmp = i * springFactor;
            const before = Math.floor(tmp);
            const after = Math.ceil(tmp);
            const atPoint = tmp - before;

            newData[i] = this.getLinearInterpolatedPoint(
                data[before],
                data[after],
                atPoint
            );
        }
        newData[maxPoints - 1] = data[data.length - 1];
        return newData;
    }

    getLinearInterpolatedPoint(before, after, atPoint) {
        return before + (after - before) * atPoint;
    }

    setInternalValues(canvas, defaultWaveData = defaultData) {
        if (!canvas) {
            return;
        }

        const { spaceBetween, markWidth, data: dataProp } = this.props;
        const { height, width } = window.getComputedStyle(canvas);
        const propData = dataProp || [];
        const data = propData.length ? propData : defaultWaveData;

        this._height = parseInt(height, 10) || 0;
        this._width = parseInt(width, 10) || 0;
        this._maxHeightValue = Math.max(...data);

        const maxTicks = Math.round(this._width / (spaceBetween + markWidth));

        this._data = this.getInterpolatedData(data, maxTicks);

        this.setState(
            {
                height: this._height,
                width: this._width,
                dpi: window.devicePixelRatio || 1
            },
            () => {
                this.renderWaveform();
            }
        );
    }

    renderWaveform() {
        const {
            spaceBetween,
            markWidth,
            color,
            fillColor,
            reflection,
            fill
        } = this.props;
        const { dpi } = this.state;
        const calculatedSpaceBetween = spaceBetween * dpi;
        const calculatedMarkWidth = markWidth * dpi;
        const height = this._height * dpi;
        const width = this._width * dpi;

        this._ctx.clearRect(0, 0, width, height);
        this._ctx.globalCompositeOperation = 'source-over';

        this._data.forEach((value, i) => {
            const ratio = value / this._maxHeightValue;
            const barHeight = Math.round(ratio * height);
            const x = i * (calculatedMarkWidth + calculatedSpaceBetween);
            let y = height - barHeight;

            if (reflection) {
                y = (height - barHeight) / 2;
            }

            this._ctx.fillStyle = color;

            if ((i + 1) / this._data.length < fill) {
                this._ctx.fillStyle = fillColor;
            }

            this._ctx.fillRect(x, y, calculatedMarkWidth, barHeight);
        });

        if (reflection) {
            // Make lower half of waveform dark
            this._ctx.globalCompositeOperation = 'destination-out';
            this._ctx.fillStyle = 'rgba(0, 0, 0, 0.9)';
            this._ctx.fillRect(0, height / 2, width, height / 2);
        }
    }

    render() {
        return (
            <div className="waveform-container">
                <canvas
                    className="waveform"
                    {...this.props.canvasProps}
                    onMouseDown={this.props.onMouseDown}
                    onTouchStart={this.props.onTouchStart}
                    height={this.state.height * this.state.dpi || 0}
                    width={this.state.width * this.state.dpi || 0}
                    ref={(c) => {
                        this._canvas = c;
                    }}
                />
            </div>
        );
    }
}
