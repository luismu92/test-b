import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

export default class BrowsePlaylistMeta extends Component {
    render() {
        const { urlSlug, category } = this.props;

        let title = 'Stream Playlists | Audiomack Music';

        if (category) {
            title = `Stream ${category} Playlists | Audiomack Music`;
        }

        const description =
            'Stream and download top playlists of new music on Audiomack. Hip-hop, rap, R&B, Latin, Reggae, Afrobeats, Electronic and much more.';
        const canonical = `${process.env.AM_URL}/playlists/browse/${urlSlug}`;
        const deepLinkPath = `/playlists/${urlSlug ? urlSlug : ''}`;
        const appDeepLink = `audiomack:/${deepLinkPath}`;

        return (
            <Helmet>
                <title>{title}</title>
                <link rel="canonical" href={canonical} />
                <meta name="description" content={description} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta property="og:url" content={canonical} />
                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />
                <meta name="twitter:description" content={description} />

                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />
                <link
                    href={`android-app://${
                        process.env.ANDROID_APP_PACKAGE
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
                <link
                    href={`ios-app://${
                        process.env.IOS_APP_ID
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
            </Helmet>
        );
    }
}

BrowsePlaylistMeta.propTypes = {
    urlSlug: PropTypes.string.isRequired,
    category: PropTypes.string
};
