import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { getBlogPostUrl } from 'utils/blog';

export default class BlogPostMeta extends Component {
    static propTypes = {
        post: PropTypes.object.isRequired
    };

    render() {
        const { post } = this.props;
        const title = post.meta_title || post.title || 'Audiomack World';
        const description =
            post.excerpt ||
            'An interactive experience that showcases original content from around the globe.';
        const postUrl = getBlogPostUrl(post, {
            host: process.env.AM_URL
        });
        const newsArticle = {
            name: title,
            url: postUrl,
            image: post.feature_image,
            thumbnailUrl: post.feature_image,
            '@type': 'NewsArticle',
            '@context': 'https://schema.org',
            dateCreated: post.created_at,
            datePublished: post.published_at,
            dateModified: post.updated_at,
            headline: title,
            publisher: {
                '@type': 'Organization',
                logo: {
                    '@type': 'ImageObject',
                    url: `${
                        process.env.AM_URL
                    }/static/images/fb-graphic_275x275.jpg`
                }
            },
            mainEntityOfPage: postUrl
        };

        if (post.primary_author) {
            newsArticle.author = {
                '@type': 'Person',
                name: post.primary_author.name
            };

            newsArticle.creator = [post.primary_author.name];

            if (post.primary_author.audiomack) {
                newsArticle.author.url = `${process.env.AM_URL}/artist/${
                    post.primary_author.audiomack
                }`;
            }
        }

        // const deepLinkPath = `/world/${postSlug}`;
        // const appDeepLink = `audiomack:/${deepLinkPath}`;

        return (
            <Helmet>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta
                    property="og:image"
                    content={post.og_image || post.feature_image}
                />
                <meta property="og:title" content={post.og_title || title} />
                <meta
                    property="og:description"
                    content={post.og_description || description}
                />
                <meta property="og:url" content={postUrl} />
                <link rel="canonical" href={postUrl} />
                {/* These arent in the app yet
                <link href={`android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack${deepLinkPath}`} rel="alternate" />
                <link href={`ios-app://${process.env.IOS_APP_ID}/audiomack${deepLinkPath}`} rel="alternate" />
                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />
                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />*/}
                <meta
                    name="twitter:image"
                    content={post.twitter_image || post.feature_image}
                />
                <meta
                    name="twitter:title"
                    content={post.twitter_title || title}
                />
                <meta
                    name="twitter:description"
                    content={post.twitter_description || description}
                />

                <script type="application/ld+json">
                    {JSON.stringify([newsArticle])}
                </script>
            </Helmet>
        );
    }
}
