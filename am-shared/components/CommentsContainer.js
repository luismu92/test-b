import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    setCommentContext,
    getComments,
    postComment,
    addUserVote,
    postUserVotes,
    getUserVoteStatus,
    deleteComment,
    reportComment,
    banComment
} from '../redux/modules/comment';

import { ucfirst, hasValidComments } from 'utils/index';
import {
    showModal,
    hideModal,
    MODAL_TYPE_AUTH,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import analyics, { eventCategory, eventAction } from 'utils/analytics';

import Comments from './Comments';

export default class CommentsContainer extends Component {
    static propTypes = {
        item: PropTypes.object,
        kind: PropTypes.string,
        id: PropTypes.number,
        total: PropTypes.number,
        className: PropTypes.string,
        dispatch: PropTypes.func,
        comment: PropTypes.object,
        currentUser: PropTypes.object,
        isMobile: PropTypes.bool,
        onCommentAdminAction: PropTypes.func,
        onCommentSortClick: PropTypes.func,
        onCommentActionClick: PropTypes.func,
        onCommentSort: PropTypes.func,
        sortTooltipActive: PropTypes.bool,
        activeCommentContext: PropTypes.string,
        commentsExpanded: PropTypes.bool,
        history: PropTypes.object,
        singleCommentUuid: PropTypes.string,
        singleCommentThread: PropTypes.string
    };

    static defaultProps = {
        isMobile: false
    };

    constructor(props) {
        super(props);

        this.state = {
            expanded: props.commentsExpanded,
            currentReplyTo: null,
            childExpanded: [],
            total: props.total,
            emptyInput: true
        };
    }

    componentDidMount() {
        this.setCommentContextForPage();
    }

    componentDidUpdate(prevProps) {
        const { kind, id, singleCommentUuid } = prevProps;
        const currentKind = this.props.kind;
        const currentId = this.props.id;

        if (
            kind !== currentKind ||
            id !== currentId ||
            singleCommentUuid !== this.props.singleCommentUuid
        ) {
            this.setCommentContextForPage();
        }

        if (prevProps.commentsExpanded && this.state.expanded === null) {
            // eslint-disable-next-line
            this.setState({
                expanded: true
            });
        }
    }

    componentWillUnmount() {
        clearTimeout(this._syncVoteTimer);
        this._syncVoteTimer = null;

        this.syncVote(); // Last chance to sync the votes
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCommentLoadMore = () => {
        const { isMobile } = this.props;
        const { expanded } = this.state;

        if (!isMobile && !expanded) {
            this.setState({
                expanded: !expanded
            });
        } else {
            this.getMoreComments();
        }
    };

    handleCommentPost = (e) => {
        e.preventDefault();
        const form = e.currentTarget;
        const { dispatch } = this.props;
        const { total } = this.state;
        let thread = null;

        if (typeof form.comment_thread !== 'undefined') {
            thread = form.comment_thread.value;
        }

        dispatch(postComment(form.comment_content.value, thread))
            .then(() => {
                analyics.track(eventCategory.comment, {
                    eventAction: eventAction.commentPost
                });
                form.comment_content.value = null;
                this.setState({
                    total: total + 1,
                    expanded: true
                });
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleCommentVote = (e) => {
        e.preventDefault();
        const voteLink = e.currentTarget;
        const voteUp = voteLink.getAttribute('data-model-vote') === 'true';
        const { dispatch } = this.props;

        dispatch(
            addUserVote(
                voteLink.getAttribute('data-model-uuid'),
                voteLink.getAttribute('data-model-thread'),
                voteUp
            )
        );

        this.setState({
            expanded: true
        });

        this.syncVotesByTimer();
    };

    handleCommentAction = (e) => {
        e.preventDefault();
        const action = e.currentTarget.getAttribute('data-comment-action');
        const uuid = e.currentTarget.getAttribute('data-comment-uuid');
        const thread = e.currentTarget.getAttribute('data-comment-thread');
        const userId = e.currentTarget.getAttribute('data-user');

        this.props.dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `${ucfirst(action)} ${
                    action === 'ban' ? 'user' : 'comment'
                }`,
                message: `Are you sure you want to ${action} ${
                    action === 'ban' ? 'this user?' : 'this comment?'
                }`,
                handleConfirm: () => {
                    switch (action) {
                        case 'report':
                            this.doCommentReport(uuid, thread);
                            break;
                        case 'ban':
                            this.doBanUser(userId);
                            break;
                        case 'remove':
                            this.doCommentRemove(uuid, thread);
                            break;
                        default:
                            this.props.dispatch(hideModal());
                            break;
                    }
                }
            })
        );

        this.setState({
            expanded: true
        });
    };

    handleVoteForGuestUser = (e) => {
        e.preventDefault();
        this.showLogin();
    };

    handleToggleReplyForm = (e) => {
        const button = e.currentTarget;
        const uuid = button.getAttribute('rel');

        if (!this.props.currentUser.isLoggedIn) {
            this.showLogin();
        }

        this.setState({
            currentReplyTo: uuid,
            expanded: true
        });
    };

    handleToggleChildren = (e) => {
        const button = e.currentTarget;
        const uuid = button.getAttribute('rel');

        this.setState((state) => {
            return {
                childExpanded: state.childExpanded.concat(uuid),
                expanded: true
            };
        });
    };

    handleCommentInputFocus = () => {
        const { currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            this.showLogin();
        }
    };

    handleCommentInputChange = (e) => {
        const input = e.currentTarget;
        const value = input.value;

        this.setState({
            emptyInput: value ? false : true // eslint-disable-line
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setCommentContextForPage() {
        const {
            dispatch,
            kind,
            id,
            comment,
            currentUser,
            singleCommentUuid,
            singleCommentThread
        } = this.props;

        if (
            kind !== null &&
            typeof kind !== 'undefined' &&
            id !== null &&
            typeof id !== 'undefined'
        ) {
            dispatch(
                setCommentContext(kind, id, comment.orderBy, singleCommentUuid)
            );
            dispatch(getComments(singleCommentUuid, singleCommentThread));

            // Check whether we need to sync vote status with server
            const userVoteStatus = comment.userVotes[`${kind}-${id}`];

            if (
                currentUser.isLoggedIn &&
                typeof userVoteStatus === 'undefined'
            ) {
                dispatch(getUserVoteStatus());
            }
        }
    }

    getMoreComments() {
        const { comment, dispatch } = this.props;

        if (comment.nextPage === null) {
            return;
        }

        dispatch(getComments())
            .then((json) => {
                if (!hasValidComments(json.resolved)) {
                    this.getMoreComments();
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    doCommentReport = (uuid, thread = null) => {
        const { dispatch } = this.props;

        dispatch(reportComment(uuid, thread === '' ? null : thread))
            .then(() => {
                this.props.onCommentAdminAction('report');
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doCommentRemove = (uuid, thread = null) => {
        const { dispatch } = this.props;

        dispatch(deleteComment(uuid, thread === '' ? null : thread))
            .then(() => {
                this.props.onCommentAdminAction('delete');
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doBanUser = (userId) => {
        const { dispatch } = this.props;

        dispatch(banComment(userId))
            .then(() => {
                this.props.onCommentAdminAction('ban');
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    // Sync local votes with server debounced by 3 sec
    syncVotesByTimer() {
        clearTimeout(this._syncVoteTimer);
        this._syncVoteTimer = setTimeout(() => {
            this.syncVote();
        }, 3000);
    }

    syncVote() {
        const { comment, dispatch } = this.props;
        const { userVotesToSync } = comment;

        if (userVotesToSync !== null && userVotesToSync.length > 0) {
            dispatch(postUserVotes());
        }
    }

    showLogin = () => {
        const { dispatch, currentUser, isMobile, history } = this.props;
        const value = 'login';

        if (currentUser.isLoggedIn) {
            return;
        }

        if (isMobile) {
            history.push('/login');
        } else {
            dispatch(showModal(MODAL_TYPE_AUTH, { type: value }));
        }
    };

    render() {
        const { currentUser, comment } = this.props;

        return (
            <Comments
                isMobile={this.props.isMobile}
                item={this.props.item}
                currentUser={currentUser}
                total={this.state.total}
                className={this.props.className}
                emptyInput={this.state.emptyInput}
                comment={comment}
                expanded={this.state.expanded}
                onCommentPost={this.handleCommentPost}
                onCommentLoadMore={this.handleCommentLoadMore}
                onCommentVote={this.handleCommentVote}
                onCommentAction={this.handleCommentAction}
                onVoteForGuestUser={this.handleVoteForGuestUser}
                onToggleReplyForm={this.handleToggleReplyForm}
                onToggleChildren={this.handleToggleChildren}
                childExpanded={this.state.childExpanded}
                currentReplyTo={this.state.currentReplyTo}
                onCommentInputFocus={this.handleCommentInputFocus}
                onCommentInputChange={this.handleCommentInputChange}
                onCommentSortClick={this.props.onCommentSortClick}
                onCommentActionClick={this.props.onCommentActionClick}
                onCommentSort={this.props.onCommentSort}
                sortTooltipActive={this.props.sortTooltipActive}
                activeCommentContext={this.props.activeCommentContext}
                singleCommentUuid={this.props.singleCommentUuid}
            />
        );
    }
}
