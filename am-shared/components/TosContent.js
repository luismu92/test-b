import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

export default function ToSContent() {
    return (
        <Fragment>
            <h2>User Acceptance</h2>

            <p>
                This Audiomack Inc. (“Audiomack”) Terms of Service, in
                conjunction with the{' '}
                <Link to="/about/privacy-policy">Privacy Policy</Link> and other
                terms and conditions of use which are incorporated herein by
                reference and may be posted and applicable to specific services,
                governs your use of the Audiomack Inc. website (“Audiomack.com”)
                and any Audiomack platform (“Platform”) specifically including
                but not limited to the Audiomack mobile application, is a
                LEGALLY BINDING CONTRACT, and are referred to throughout as the
                “Agreement’.
            </p>

            <p>
                By visiting the Audiomack.com, using any content, products or
                services provided to you on, from, or through the Audiomack Inc.
                website or Platform (collectively the “Services”) you are
                signifying that you have read the Agreement, that you understand
                the Agreement, and you agree to be bound by all of the terms of
                the Agreement.
            </p>

            <p>
                If you don’t completely agree with the terms of the agreement,
                but still use any Audiomack Service or Platform, you will still
                be bound to the terms of the agreement.
            </p>

            <p>
                Audiomack may modify this Agreement at any time without any
                prior notice to you and said modifications may become effective
                immediately upon their posting to the website. Your continued
                use of any Audiomack Service or Platform following a
                modification to this Agreement indicates your acceptance of the
                Agreement and you agree to be bound by any such modification. If
                you are dissatisfied with anything related to any Audiomack.com
                Service or Platform your sole remedy is to discontinue use of
                said Service or Platform. You use any Audiomack Service or
                Platform at your own risk.
            </p>

            <h2>Services - Terms of Use</h2>

            <p>
                The Agreement applies to all users of any Audiomack Service or
                Platform, including users who contribute content. "content"
                includes text, software, scripts, graphics, photos, sounds,
                music, videos, audiovisual combinations, interactive features
                and other materials you may contribute to any Audiomack Service
                or Platform. This includes all aspects of Audiomack including
                but not limited to, all products, software and services offered
                via the Audiomack website and mobile app.
            </p>

            <p>
                Audiomack may change, suspend or discontinue all or any aspect
                of any Service or Platform at any time, including the
                availability of any feature, database, or content, without prior
                notice or liability.
            </p>

            <p>
                Audiomack may contain links to third party websites. By use of
                any Audiomack Service or Platform, you expressly relieve
                Audiomack Inc. from any and all liability arising from your use
                of any third-party website.
            </p>

            <p>
                By signing up for an Audiomack account you opt-in to receiving
                emails from Audiomack. At times Audiomack will send account
                holders emails notifying them of system upgrades, issues and
                other messages. You will have the option to opt-out of these
                emails by clicking the unsubscribe link included in any of our
                email messages.
            </p>

            <h2>Acceptable Use of the Service – User Responsibility</h2>

            <p>
                You agree while using any Audiomack Service or Platform that you
                MAY NOT:
            </p>

            <ul>
                <li>
                    Distribute an illegal or unauthorized copy of another
                    person's trademarked or copyrighted work;
                </li>

                <li>Alter or modify any part of the Services;</li>

                <li>Alter or modify any part of the Platform;</li>

                <li>Use the Services for any illegal purpose;</li>

                <li>Use the Platform for any illegal purpose;</li>

                <li>
                    Use any spider, readers, site search application, or other
                    device to retrieve or index any portion of the Service or
                    Platform, with the exception of major public search engines;
                </li>

                <li>
                    Transmit any viruses, worms, spam or malware or other items
                    of a destructive nature;
                </li>

                <li>
                    Upload any content that includes code hidden or otherwise
                    contained within the content which is unrelated to the
                    content;
                </li>

                <li>
                    Reformat or frame any portion of any web page that is part
                    of the Service or Platform;
                </li>

                <li>
                    Collect or harvest any personal identifiable information or
                    account names or solicit users;
                </li>

                <li>Impersonate another person or artist;</li>

                <li>
                    Violate or attempt to violate Audiomack systems or interfere
                    with the normal use of the Service or Platform by users;
                </li>

                <li>Post advertisements, promotions or solicitations;</li>

                <li>
                    Submit any content that falsely implies sponsorship of the
                    content by the Service or Platform, falsify or delete any
                    author attribution in any content, or promote any
                    information that you know is false or misleading;
                </li>

                <li>
                    Post any links to any external Internet sites that are
                    obscene or sexually explicit material of any kind;
                </li>

                <li>
                    Sell or transfer or offer to sell or transfer an Audiomack
                    account to a third party without prior written approval from
                    Audiomack;
                </li>

                <li>
                    Artificially increase play counts or other statistics
                    through the use of bots, pay-for-play services or other
                    means
                </li>
            </ul>

            <h2>Audiomack Accounts</h2>

            <p>
                To access or benefit from some of the Audiomack Service or
                Platform you will have to create an Audiomack account. You are
                responsible for maintaining the confidentiality of your access
                information and are responsible for all activities that occur
                utilizing your information. Although Audiomack will not be
                liable for any losses you might suffer, you may be liable for
                the losses of Audiomack or others.
            </p>

            <h2>User Content and Conduct</h2>

            <p>
                In addition to the general restrictions above, the following
                restrictions and conditions apply specifically to your use of
                the content. You will comply with laws regarding transmission of
                data.
            </p>

            <p>
                Content is provided to you as is. You understand that when using
                any Audiomack Service or Platform, you will be exposed to
                content from a many different sources, and that Audiomack is not
                responsible for the accuracy, usefulness, safety, or
                intellectual property rights of such content. You further
                understand and acknowledge that you may be exposed to content
                that is inaccurate, offensive, indecent, or objectionable, and
                you agree to waive, and hereby do waive, any legal or equitable
                rights or remedies you have or may have against Audiomack with
                respect to the extent permitted by USA law, agree to indemnify
                and hold harmless Audiomack its owners, operators, affiliates,
                licensors, and licensees to the fullest extent allowed by law
                regarding all matters related to your use of the Service or
                Platform. You acknowledge your use of content is at your sole
                risk.
            </p>

            <p>
                You shall be solely responsible for your own content and the
                consequences of storing and distributing your content on the
                Audiomack Service or on any Audiomack Platform. You affirm,
                represent, and warrant that you own or have the necessary
                licenses, rights, consents, and permissions to distribute
                content through the Audiomack Service or on any Audiomack
                Platform; and you license to Audiomack all patent, trademark,
                copyright or other proprietary rights for distribution on any
                Audiomack Service or Platform pursuant to the Agreement.
            </p>

            <p>
                For clarity, you retain all of your ownership rights in your
                content. However, by distributing content through Audiomack.com,
                you grant Audiomack a non-exclusive, royalty-free, and
                licensable to use, reproduce, distribute, prepare derivative
                works of, display, perform the content in connection with any
                Audiomack Service, Platform and Audiomack’s (including but not
                limited to its successors and affiliates) business. By accepting
                these terms and uploading your content, you agree and understand
                that these materials will be made available to iOS and Android
                app users who select to store your material on their device for
                the purpose of offline play. As a reminder - you cannot upload
                any third party content which infringes on third party
                intellectual property or copyrights, and you cannot make any
                material available that you are not the authorized copyright
                holder for. You will not use Audiomack to upload or distribute
                any content that is abusive, libelous, defamatory, pornographic
                or obscene, that promotes or incites violence, terrorism,
                illegal acts, or hatred, or is otherwise objectionable in
                Audiomack’s reasonable discretion.
            </p>

            <p>
                Audiomack will respond to any and all takedown requests that
                comply with the requirements of the Digital Millennium Copyright
                Act (“DMCA”), and other applicable intellectual property laws.
                To file a copyright infringement notification with us you will
                need to send a written or electronic communication to Audiomack
                at the email address{' '}
                <a href="mailto:dmca@audiomack.com">dmca@audiomack.com</a> or
                via our{' '}
                <a
                    href="https://audiomack.com/about/legal"
                    target="_blank"
                    rel="nofollow noopener"
                >
                    webform
                </a>{' '}
                which includes the following information:
            </p>

            <p>
                <strong>(1)</strong> A physical or electronic signature of a
                person authorized to act on behalf of the owner of an exclusive
                right that is allegedly infringed.
            </p>

            <p>
                <strong>(2)</strong> Identification of the copyrighted work
                claimed to have been infringed. Please include a screenshot of
                the alleged copyright infringement on the Audiomack platform.
            </p>

            <p>
                <strong>(3)</strong> Information reasonably sufficient to permit
                Audiomack to contact the complaining party, such as an address,
                telephone number and, if available, an electronic mail address
                at which the complaining party may be contacted.
            </p>

            <p>
                You understand and agree that Audiomack reserves the right to
                delete, move, or edit any content that it may determine violates
                the Agreement and/or Privacy Policy or is otherwise unacceptable
                and may terminate your access to the Services, without prior
                notice and at its sole discretion.
            </p>

            <p>
                You control your data through your account and/or cookies which
                may be placed on your computer. Audiomack maintains multiple
                copies of active files. Audiomack bears no responsibility for
                maintaining your data.
            </p>

            <p>
                Audiomack.com is not a data warehousing website. The intended
                use is for active file sharing which may inadvertently result in
                backups of files being shared.
            </p>

            <p>
                Any views expressed on the website do not reflect the views of
                Audiomack.
            </p>

            <h2>Warranty Disclaimer and Limitation of Liability</h2>

            <p>
                YOU AGREE THAT YOUR USE OF THE SERVICES SHALL BE AT YOUR SOLE
                RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, AUDIOMACK, ITS
                OFFICERS, EMPLOYEES, AND AGENTS DISCLAIM ALL WARRANTIES, EXPRESS
                OR IMPLIED, IN CONNECTION WITH THE SERVICES AND YOUR USE
                THEREOF.
            </p>

            <p>
                NEITHER AUDIOMACK NOR ANY PROVIDER OF ANY THIRD PARTY CONTENT
                WARRANTS THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR FREE
                OR MAKES ANY WARRANTY OF THE RESULTS TO BE OBTAINED FROM USE OF
                THE SERVICES OR CONTENT.
            </p>

            <p>
                NEITHER AUDIOMACK NOR ANY THIRD PARTY WARRANT OR GUARANTEE THAT
                ANY FILES AVAILABLE FOR DOWNLOADING THROUGH ANY AUDIOMACK
                SERVICE OR PLATFORM WILL BE FREE OF VIRUSES OR SIMILAR
                DESTRUCTIVE FEATURES. YOU AGREE THAT YOU ASSUME ALL RISK AS TO
                THE QUALITY AND PERFORMANCE OF THE SERVICES AND THE ACCURACY OF
                THE CONTENT. AUDIOMACK IS NOT RESPONSIBLE UNAUTHORIZED OR
                ALTERATION OF YOUR CONTENT OR FOR ANY VIOLATION OF ITS
                AGREEMENT.
            </p>

            <p>
                AUDIOMACK DOES NOT WARRANT, GUARANTEE, OR ASSUME RESPONSIBILITY
                FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD
                PARTY THROUGH THE SERVICES OR ANY HYPERLINKED SERVICES OR
                ADVERTISING, AND AUDIOMACK WILL NOT BE A PARTY TO OR IN ANY WAY
                BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND
                THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES.
            </p>

            <p>
                NEITHER AUDIOMACK, NOR ANY THIRD PARTY PROVIDER SHALL BE LIABLE
                TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT, INCIDENTAL,
                SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES ARISING OUT OF THE
                USE OF OR INABILITY TO USE THE SERVICES, EVEN IF SUCH PARTY HAS
                BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN NO EVENT
                WILL AUDIOMACK's AGGREGATE LIABILITY TO YOUR OR ANY THIRD PARTY
                FOR ALL CLAIMS ARISING OUT OF THE USE OF THE SERVICES. THE
                LIMITATIONS OF DAMAGES SET FORTH ABOVE ARE FUNDAMENTAL ELEMENTS
                OF THE BASIS OF THE DEAL BETWEEN AUDIOMACK AND YOU. IN STATES OR
                COUNTRIES NOT ALLOWING EXCLUSION OF WARRANTIES OR LIMITATION OF
                LIABILITY FOR INCIDENTAL DAMAGES, AUDIOMACK AND ANY THIRD PARTY
                PROVIDER SHALL BE LIMITED TO THE GREATEST EXTENT OF THE LAW.
            </p>

            <h2>Indemnification</h2>

            <p>
                You agree to indemnify, defend and hold Audiomack and its
                affiliates and their respective officers, owners, employees,
                agents, information providers and licensors harmless from and
                against any and all claims, liability, losses, damages,
                obligations, costs and expenses, including attorney’s fees,
                incurred by any Audiomack Party in connection with any content
                or use of any Audiomack Service or Platform, whether via your
                password and by any other person, whether or not authorized by
                you. This defense and indemnification obligation will survive
                these Terms of Service and your use of the Services.
            </p>

            <h2>General Terms and Conditions</h2>

            <p>
                The Agreement shall be construed in accordance with the laws of
                the State of New York, without reference to principles of choice
                of law. You and Audiomack each irrevocably consent to the
                personal jurisdiction of the federal or state courts located in
                New York County, New York ("Courts") with respect to any action,
                suit or proceeding arising out of or related to the Agreement or
                to the Services and /or content and waive any objection to venue
                in any of the Courts for such as action, suit or proceeding;
                additionally, you agree that you will not bring any such action,
                suit or proceeding in any court other than the Courts.
            </p>

            <p>
                These Terms of Service, together with the Privacy Policy and any
                other legal notices published by Audiomack on any Audiomack
                Service, Platform or within the Agreement, constitutes the
                entire agreement between the parties with respect to the subject
                matter hereof, and supersedes all previous written or oral
                agreements between the parties with respect to such subject
                matter. If any inconsistency exists between the terms of the
                Agreement and any additional terms and conditions posted on any
                Audiomack Service or Platform, such terms shall be interpreted
                as to eliminate any inconsistency, if possible, and otherwise,
                the additional terms and conditions shall control.
            </p>

            <p>
                Please e-mail reports of any violations to{' '}
                <a href="mailto:support@audiomack.com">support@audiomack.com</a>
            </p>
        </Fragment>
    );
}
