import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import MediaSession from './MediaSession';

import {
    play,
    pause,
    stop,
    next as playerNext,
    timeUpdate,
    ended,
    prev,
    seek,
    songLoaded,
    refreshCurrentPlaylistItem
} from '../../redux/modules/player';

import PlayerAudio from './PlayerAudio';

class PlayerAudioContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onStop: PropTypes.func,
        player: PropTypes.object,
        id: PropTypes.string,
        onAirplayStateChange: PropTypes.func,
        onAirplayAvailabilityChange: PropTypes.func
    };

    static defaultProps = { onStop() {} };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        // Prevent uglify from removing console.warn calls
        this._c = console;
        this._l = 'warn';
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTimeUpdate = (e) => {
        const { dispatch, player } = this.props;

        clearTimeout(this._playTimer);
        this._playTimer = null;

        const time = e.target.currentTime;

        // This happens when we disconnect from the
        // chromecast and the player becomes paused.
        // After pressing play the, player starts over
        // because of a 0 timeupdate event fired from
        // the audio tag switching srcs. Not sure if
        // this is a good way to handle yet.
        if (time === 0 && player.currentTime > 0) {
            e.target.currentTime = player.currentTime;
        }

        if (player.currentSong) {
            dispatch(timeUpdate(e.target.currentTime));
        }
    };

    handleLoadedMetadata = (e) => {
        const { dispatch } = this.props;

        dispatch(songLoaded(e.target));
    };

    handleEnded = (e) => {
        const { dispatch, player } = this.props;
        const isOneSecondHack = e.target.src.indexOf('data:audio') === 0;

        // Ignore next action for the one second song that gets played before
        // every song.
        if (isOneSecondHack) {
            return;
        }

        dispatch(ended());

        if (player.queueIndex === player.queue.length - 1 && !player.repeat) {
            dispatch(stop());
            this.props.onStop();
            return;
        }

        dispatch(playerNext());
    };

    handlePlayClick = () => {
        const { dispatch, player } = this.props;

        if (!player.paused || player.chromecastActive) {
            return;
        }

        dispatch(play());
        // If we press play and there's no subsequent timeupdate
        // event, then we've probably left a song in the player
        // for too long and it expired.
        this._playTimer = setTimeout(() => {
            dispatch(refreshCurrentPlaylistItem());
        }, 4000);
    };

    handlePauseClick = () => {
        this.props.dispatch(pause());
    };

    handleNextClick = () => {
        this.props.dispatch(playerNext());
    };

    handlePrevClick = () => {
        this.props.dispatch(prev());
    };

    handleSeekBackward = () => {
        const adjustTime = -10;
        const { currentTime } = this.props.player;

        this.props.dispatch(seek(currentTime + adjustTime));
    };

    handleSeekForward = () => {
        const adjustTime = 30;
        const { currentTime } = this.props.player;

        this.props.dispatch(seek(currentTime + adjustTime));
    };

    handleError = (e) => {
        const target = e.currentTarget || {};
        const error = target.error || {};
        const {
            code,
            MEDIA_ERR_ABORTED,
            MEDIA_ERR_NETWORK,
            MEDIA_ERR_DECODE,
            MEDIA_ERR_SRC_NOT_SUPPORTED
        } = error;
        const { dispatch, player } = this.props;

        switch (code) {
            case MEDIA_ERR_ABORTED:
                this._c[this._l](code, 'Aborted playback.');
                break;

            case MEDIA_ERR_NETWORK:
                this._c[this._l](
                    code,
                    'A network error caused the audio download to fail.'
                );
                break;

            case MEDIA_ERR_DECODE:
                this._c[this._l](
                    code,
                    'The audio playback was aborted due to a corruption problem or because the video used features your browser did not support.'
                );
                break;

            case MEDIA_ERR_SRC_NOT_SUPPORTED: {
                this._c[this._l](
                    code,
                    'The audio could not be loaded, either because the server or network failed or because the format is not supported.'
                );

                if (
                    player.currentSong &&
                    !player.currentSong.refreshAttempted
                ) {
                    dispatch(refreshCurrentPlaylistItem());
                }
                break;
            }

            default:
                this._c[this._l](code, 'An unknown error occurred.');
                break;
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { player } = this.props;
        const song = player.queue[player.queueIndex] || {};
        const src = player.placeholderSong || song.streaming_url;

        return (
            <Fragment>
                <MediaSession
                    currentSong={player.currentSong}
                    paused={player.paused}
                    onPlay={this.handlePlayClick}
                    onPause={this.handlePauseClick}
                    onPreviousTrack={this.handlePrevClick}
                    onNextTrack={this.handleNextClick}
                    onSeekBackward={this.handleSeekBackward}
                    onSeekForward={this.handleSeekForward}
                />
                <PlayerAudio
                    ref={(c) => (this._playerAudio = c)}
                    id={this.props.id}
                    src={src}
                    songUuid={`${song.type}:${song.id}:${player.queueIndex}`}
                    paused={player.paused}
                    volume={player.volume}
                    seeking={player.seeking}
                    muted={player.muted}
                    currentTime={player.currentTime}
                    isChromecastActive={player.chromecastActive}
                    onTimeUpdate={this.handleTimeUpdate}
                    onLoadedMetadata={this.handleLoadedMetadata}
                    onEnded={this.handleEnded}
                    onPlay={this.handlePlayClick}
                    onPause={this.handlePauseClick}
                    onError={this.handleError}
                    onAirplayAvailabilityChange={
                        this.props.onAirplayAvailabilityChange
                    }
                    onAirplayStateChange={this.props.onAirplayStateChange}
                />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player
    };
}

export default connect(mapStateToProps)(PlayerAudioContainer);
