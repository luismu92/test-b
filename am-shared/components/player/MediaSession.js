import { Component } from 'react';
import PropTypes from 'prop-types';

import { getArtistName } from 'utils/index';

export default class MediaSession extends Component {
    static propTypes = {
        currentSong: PropTypes.object,
        paused: PropTypes.bool.isRequired,
        onPlay: PropTypes.func.isRequired,
        onPause: PropTypes.func.isRequired,
        onSeekBackward: PropTypes.func,
        onSeekForward: PropTypes.func,
        onPreviousTrack: PropTypes.func.isRequired,
        onNextTrack: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.setSession();
    }

    shouldComponentUpdate(nextProps) {
        const nextSong = nextProps.currentSong || {};
        const currentSong = this.props.currentSong || {};

        return nextSong.id !== currentSong.id;
    }

    componentDidUpdate() {
        this.setSession();
    }

    setSession() {
        if (!('mediaSession' in window.navigator)) {
            return;
        }

        const { currentSong, paused } = this.props;
        const session = window.navigator.mediaSession;

        if (!currentSong) {
            session.metadata = null;
            return;
        }

        const metadata = {
            title: currentSong.title,
            artist: getArtistName(currentSong),
            artwork: [
                // { src: 'https://dummyimage.com/96x96',   sizes: '96x96',   type: 'image/png' },
                // { src: 'https://dummyimage.com/128x128', sizes: '128x128', type: 'image/png' },
                // { src: 'https://dummyimage.com/192x192', sizes: '192x192', type: 'image/png' },
                { src: currentSong.image, sizes: '256x256', type: 'image/jpeg' }
                // { src: 'https://dummyimage.com/384x384', sizes: '384x384', type: 'image/png' },
                // { src: 'https://dummyimage.com/512x512', sizes: '512x512', type: 'image/png' }
            ]
        };
        const seekForward =
            currentSong.genre === 'podcast' ? this.props.onSeekForward : null;
        const seekBackward =
            currentSong.genre === 'podcast' ? this.props.onSeekBackward : null;

        if (currentSong.album) {
            metadata.album = currentSong.album;
        }

        if (
            currentSong.parentDetails &&
            currentSong.parentDetails.type === 'album'
        ) {
            metadata.album = currentSong.parentDetails.title;
        }

        session.metadata = new window.MediaMetadata(metadata);

        session.setActionHandler('play', this.props.onPlay);
        session.setActionHandler('pause', this.props.onPause);
        session.setActionHandler('seekbackward', seekBackward);
        session.setActionHandler('seekforward', seekForward);
        session.setActionHandler('previoustrack', this.props.onPreviousTrack);
        session.setActionHandler('nexttrack', this.props.onNextTrack);

        session.playbackState = paused ? 'paused' : 'playing';
    }

    render() {
        return null;
    }
}
