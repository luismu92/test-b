import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class PlayerAudio extends Component {
    static propTypes = {
        id: PropTypes.string,
        src: PropTypes.string,
        songUuid: PropTypes.string,
        paused: PropTypes.bool,
        volume: PropTypes.number,
        currentTime: PropTypes.number,
        seeking: PropTypes.bool,
        muted: PropTypes.bool,
        isChromecastActive: PropTypes.bool,
        onLoadedMetadata: PropTypes.func,
        onAirplayStateChange: PropTypes.func,
        onAirplayAvailabilityChange: PropTypes.func,
        onError: PropTypes.func,
        onTimeUpdate: PropTypes.func,
        onPlay: PropTypes.func,
        onPause: PropTypes.func,
        onEnded: PropTypes.func
    };

    static defaultProps = {
        isChromecastActive: false,
        onAirplayStateChange() {},
        onAirplayAvailabilityChange() {},
        onLoadedMetadata() {},
        onError() {},
        onTimeUpdate() {},
        onPlay() {},
        onPause() {},
        onEnded() {}
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        if (this.props.volume && this._audio) {
            this._audio.volume = this.props.volume;
        }

        if (window.WebKitPlaybackTargetAvailabilityEvent) {
            this._audio.addEventListener(
                'webkitplaybacktargetavailabilitychanged',
                this.handleAirplayAvailabilityChange
            );
        }

        this._airplayActive = null;
    }

    shouldComponentUpdate(nextProps) {
        return (
            nextProps.src !== this.props.src ||
            nextProps.songUuid !== this.props.songUuid ||
            nextProps.isChromecastActive !== this.props.isChromecastActive ||
            nextProps.paused !== this.props.paused ||
            nextProps.seeking !== this.props.seeking ||
            nextProps.volume !== this.props.volume ||
            nextProps.muted !== this.props.muted
        );
    }

    componentDidUpdate(prevProps) {
        if (this.props.isChromecastActive) {
            if (this._audio) {
                this._audio.pause();
            }
            return;
        }

        if (this.props.volume !== prevProps.volume && this._audio) {
            this._audio.volume = this.props.volume;
        }

        if (
            (this._audio && this.props.seeking) ||
            (!this.props.isChromecastActive && prevProps.isChromecastActive)
        ) {
            this._audio.currentTime = this.props.currentTime;
        }

        if (prevProps.songUuid !== this.props.songUuid && this._audio) {
            this._audio.currentTime = 0;
        }

        if (this._audio && this.props.muted !== prevProps.muted) {
            this._audio.muted = this.props.muted;
        }

        if (this.props.paused !== prevProps.paused) {
            this.setPlayerState(this.props.paused);
        }

        if (this._audio) {
            const shouldBeActive = this._audio
                .webkitCurrentPlaybackTargetIsWireless;

            if (
                typeof shouldBeActive !== 'undefined' &&
                (this._airplayActive === null ||
                    (shouldBeActive && !this._airplayActive) ||
                    (!shouldBeActive && this._airplayActive))
            ) {
                this._airplayActive = shouldBeActive;
                this.props.onAirplayStateChange(shouldBeActive);
            }
        }
    }

    componentWillUnmount() {
        this._audio.removeEventListener(
            'webkitplaybacktargetavailabilitychanged',
            this.handleAirplayAvailabilityChange
        );
        this._audio = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoadedMetadata = (e) => {
        this.props.onLoadedMetadata(e);
        this.setPlayerState(this.props.paused);
    };

    handleAirplayAvailabilityChange = (e) => {
        this.props.onAirplayAvailabilityChange(e);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setPlayerState(paused) {
        if (this.props.isChromecastActive) {
            return;
        }

        const fn = paused ? 'pause' : 'play';

        const promise = this._audio[fn]();

        if (promise && promise.catch) {
            promise.catch((err) => {
                console.error(err);
                if (this.props.paused || err.name === 'NotAllowedError') {
                    this._audio.pause();
                    this.props.onPause();
                }
            });
        }
    }

    render() {
        const { isChromecastActive, src } = this.props;
        const hasOneSecond = src && src.indexOf('data:audio') === 0;

        // This fixes the issue where a mobile play has to happen right
        // after user interaction. We take the one second song and play/pause it
        // with the real song coming shortly thereafter
        if (this._audio && hasOneSecond && !isChromecastActive) {
            const shouldPause = !this._audio.paused;
            const promise = this._audio.play();

            if (promise && promise.catch) {
                promise.catch(() => {
                    // We can ignore this error since it only happens when
                    // trying to play the 1 sec audio hack
                    if (this.props.paused) {
                        this._audio.pause();
                        this.props.onPause();
                    }
                });
            }

            if (shouldPause) {
                this._audio.pause();
            }
        }

        // @todo turn this to new Audio() instead of having
        // element in the DOM for people to easily RIP the src
        //
        // Attributes to set:
        // set this before setting src
        // msaudiocategory="BackgroundCapableMedia"
        // preload="metadata"
        // crossorigin="anonymous"
        return (
            // eslint-disable-next-line jsx-a11y/media-has-caption
            <audio
                id={this.props.id}
                src={this.props.src}
                ref={(c) => (this._audio = c)}
                onLoadedMetadata={this.handleLoadedMetadata}
                onTimeUpdate={this.props.onTimeUpdate}
                onEnded={this.props.onEnded}
                onPlay={this.props.onPlay}
                onError={this.props.onError}
            />
        );
    }
}
