import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './MusicRankings.module.scss';

export default function MusicRankings({ type, rankings, style }) {
    if (!rankings) {
        return null;
    }

    // prettier-ignore
    const labelToKeyMap = {
        'Today': 'daily',
        'This week': 'weekly',
        'This month': 'monthly',
        'All time': 'total'
    };

    const keyToUrlMap = {
        daily: `/${type}s/day`,
        weekly: `/${type}s/week`,
        monthly: `/${type}s/month`,
        total: `/${type}s`
    };

    const listItems = Object.keys(labelToKeyMap).map((label, i) => {
        const key = labelToKeyMap[label];

        if (!rankings[key]) {
            return null;
        }

        // Strip hashtah out of API response and re-add it
        // so we can style it differently
        const rawRank = rankings[key].replace('#', '');

        let number = rawRank;
        if (!isNaN(rawRank)) {
            number = (
                <Fragment>
                    <span className="u-text-orange">#</span>
                    {rawRank}
                </Fragment>
            );
        }

        return (
            <li key={i} className={styles.stat}>
                <Link to={keyToUrlMap[key]}>
                    <p className={styles.number}>{number}</p>
                    <strong className={styles.label}>{label}</strong>
                </Link>
            </li>
        );
    });

    return (
        <ul className={styles.container} style={style || {}}>
            {listItems}
        </ul>
    );
}

MusicRankings.propTypes = {
    type: PropTypes.string,
    rankings: PropTypes.object,
    style: PropTypes.object
};
