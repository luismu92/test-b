import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

export default class BrowseArtistMeta extends Component {
    static propTypes = {
        mobile: PropTypes.bool
    };

    getCanonical(isMobile) {
        if (isMobile) {
            return `${process.env.AM_URL}/feed/suggested-follows`;
        }

        return `${process.env.AM_URL}/artists/popular`;
    }

    render() {
        const title = 'Suggested Audiomack Accounts to Follow';
        const description = 'Check out these accounts on Audiomack';
        const canonical = this.getCanonical(this.props.mobile);
        const deepLinkPath = '/suggested_follows';
        const appDeepLink = `audiomack:/${deepLinkPath}`;

        return (
            <Helmet>
                <title>{title}</title>
                <link rel="canonical" href={canonical} />
                <meta name="description" content={description} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta property="og:url" content={canonical} />
                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />
                <meta name="twitter:description" content={description} />

                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />
                <link
                    href={`android-app://${
                        process.env.ANDROID_APP_PACKAGE
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
                <link
                    href={`ios-app://${
                        process.env.IOS_APP_ID
                    }/audiomack${deepLinkPath}`}
                    rel="alternate"
                />
            </Helmet>
        );
    }
}
