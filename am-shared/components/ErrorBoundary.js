import { Component } from 'react';
import PropTypes from 'prop-types';

import analytics from 'utils/analytics';

export default class ErrorBoundary extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
        boundaryName: PropTypes.string.isRequired,
        displayError: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    };

    static defaultProps = {
        displayError: null
    };

    constructor(props) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error, info) {
        console.error(`Error in boundary: ${this.props.boundaryName}`);
        console.error(error);
        console.error(info);
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        analytics.error(error);
    }

    static getDerivedStateFromError(error) {
        console.error(error);
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    render() {
        if (this.state.hasError) {
            return this.props.displayError;
        }

        return this.props.children;
    }
}
