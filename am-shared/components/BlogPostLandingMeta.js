import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { ucfirst } from 'utils/index';

export default class BlogPostLandingMeta extends Component {
    static propTypes = {
        settings: PropTypes.object,
        match: PropTypes.object
    };

    render() {
        const settings = this.props.settings || {};
        let title = settings.title || 'Audiomack World';

        if (
            this.props.match.path.match(/^\/world\/tag/) &&
            this.props.match.params.slug
        ) {
            title = `${ucfirst(this.props.match.params.slug)} | ${title}`;
        }

        const description =
            settings.description ||
            'Updates, blog posts, and streaming music analysis from the Audiomack Team';
        const canonical = '/world';

        // const deepLinkPath = `/world`;
        // const appDeepLink = `audiomack:/${deepLinkPath}`;

        return (
            <Helmet>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta property="og:image" content={settings.icon} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta property="og:url" content={canonical} />
                <link rel="canonical" href={canonical} />
                {/* These arent in the app yet
                <link href={`android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack${deepLinkPath}`} rel="alternate" />
                <link href={`ios-app://${process.env.IOS_APP_ID}/audiomack${deepLinkPath}`} rel="alternate" />
                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />
                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />*/}
                <meta name="twitter:image" content={settings.icon} />
                <meta name="twitter:title" content={title} />
                <meta name="twitter:description" content={description} />
            </Helmet>
        );
    }
}
