import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import moment from 'moment';
import { parse } from 'query-string';

import { DEFAULT_DATE_FORMAT } from 'constants/index';
import {
    generateISODuration,
    generateKeywords,
    getMusicUrl,
    isDownloadableTrack,
    getUploader,
    getArtistUrl,
    getArtistName,
    buildDynamicImage
} from 'utils/index';
import { getFeaturing } from '../utils';

export default class MusicPageMeta extends Component {
    static propTypes = {
        musicItem: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired
    };

    getDownloadText(musicItem) {
        return isDownloadableTrack(musicItem)
            ? 'Download for free'
            : 'Listen for free';
    }

    getDescription(musicItem, downloadText) {
        const suffix = downloadText === 'Download' ? 'and Download ' : '';
        const artist =
            musicItem.type === 'playlist'
                ? musicItem.artist.name
                : musicItem.artist;

        let feat;
        let producer;
        let fromAlbum;
        let releaseDate;
        const featArtists = getFeaturing(musicItem);

        if (musicItem.type === 'song') {
            feat = featArtists.length ? ` Featuring: ${featArtists}` : '';
            fromAlbum = musicItem.album ? ` Album: ${musicItem.album}.` : '';
        }

        if (musicItem.type === 'album') {
            feat = featArtists.length ? ` Featuring: ${featArtists}` : '';
        }

        if (musicItem.type !== 'playlist') {
            const date = moment(musicItem.released * 1000).format(
                DEFAULT_DATE_FORMAT
            );
            releaseDate = ` Release Date: ${date}.`;
            producer = musicItem.producer
                ? ` Producer: ${musicItem.producer}.`
                : '';
        }

        switch (musicItem.type) {
            case 'album':
                return `Stream ${
                    musicItem.title
                }, an album by ${artist}.${feat}${producer}${releaseDate}`;
            case 'playlist':
                return `Stream ${musicItem.title}, a ${
                    musicItem.genre
                } playlist by ${artist}, to discover new music on Audiomack.`;
            default:
                return `Stream ${suffix}${musicItem.title} the new ${
                    musicItem.genre === 'podcast' ? 'podcast' : musicItem.type
                } from ${artist}.${feat}${producer}${fromAlbum}${releaseDate}`;
        }
    }

    getTitle(musicItem, downloadText) {
        switch (musicItem.type) {
            case 'song':
                return `${musicItem.title} by ${
                    musicItem.artist
                }: ${downloadText}`;

            case 'album':
                return `${musicItem.title} by ${musicItem.artist} on Audiomack`;

            case 'playlist':
                return `${musicItem.title} music playlist by ${
                    musicItem.artist.name
                } | Listen on Audiomack`;

            default:
                return '';
        }
    }

    getTwitterTitle(musicItem) {
        let feat = '';

        if (musicItem.type === 'song' && musicItem.featuring) {
            feat = ` featuring ${musicItem.featuring}`;
        }

        switch (musicItem.type) {
            case 'song':
            case 'album':
                return `${musicItem.artist} - ${musicItem.title}${feat}`;

            case 'playlist':
                return `${musicItem.artist.name} - ${musicItem.title}`;

            default:
                return '';
        }
    }

    getTwitterPlayerHeight(musicItem) {
        switch (musicItem.type) {
            case 'playlist':
                return 250;

            case 'album':
                return 300;

            default:
                return 200;
        }
    }

    getMusicArtwork(musicItem) {
        return buildDynamicImage(musicItem.image_base || musicItem.image, {
            width: 1000,
            height: 1000,
            max: true
        });
    }

    renderTwitterCreator(musicItem) {
        const twitter = getUploader(musicItem).twitter;

        if (!twitter) {
            return null;
        }

        return <meta name="twitter:creator" content={`@${twitter}`} />;
    }

    renderBranchTags(musicItem = {}, currentUser) {
        const uploader = getUploader(musicItem);
        let genre;

        if (musicItem.genre) {
            genre = (
                <meta
                    key="genre"
                    name="branch:deeplink:genre"
                    content={musicItem.genre}
                />
            );
        }

        let userGenre;
        let userUploads;

        if (currentUser.isLoggedIn) {
            if (currentUser.profile.genre) {
                userGenre = (
                    <meta
                        key="userGenre"
                        name="branch:deeplink:userGenre"
                        content={currentUser.profile.genre}
                    />
                );
            }

            if (currentUser.profile.upload_count) {
                userUploads = (
                    <meta
                        key="userUploads"
                        name="branch:deeplink:userUploads"
                        content={currentUser.profile.upload_count}
                    />
                );
            }
        }

        return [
            genre,
            <meta
                key="musicType"
                name="branch:deeplink:musicType"
                content={musicItem.type}
            />,
            <meta
                key="artist"
                name="branch:deeplink:artist"
                content={getArtistName(musicItem)}
            />,
            <meta
                key="image"
                name="branch:deeplink:image"
                content={this.getMusicArtwork(musicItem)}
            />,
            <meta
                key="uploader"
                name="branch:deeplink:uploader"
                content={uploader.name}
            />,
            <meta
                key="uploaderImage"
                name="branch:deeplink:uploaderImage"
                content={uploader.image}
            />,
            userGenre,
            userUploads
        ];
    }

    render() {
        const { musicItem, currentUser, location } = this.props;
        const downloadText = this.getDownloadText(musicItem);
        const title = this.getTitle(musicItem, downloadText);
        const description = this.getDescription(musicItem, downloadText);
        const canonical = getMusicUrl(musicItem, {
            host: process.env.AM_URL
        });
        const embedUrl = getMusicUrl(musicItem, {
            host: process.env.AM_URL,
            embed: true
        });
        const twitterEmbedUrl = getMusicUrl(musicItem, {
            host: process.env.AM_URL,
            embed: true,
            query: {
                twitterEmbed: true
            }
        });
        const oembedUrl = getMusicUrl(musicItem, {
            host: process.env.AM_URL,
            oembed: true
        });
        const artistUrl = getArtistUrl(musicItem, {
            host: process.env.AM_URL
        });
        const appDeepLink = getMusicUrl(musicItem, {
            query: parse(location.search),
            host: 'audiomack:/' // 1 slash since the url will start with a slash
        });
        // https://blog.branch.io/creating-an-app-content-sitemap-for-google-app-indexing/
        const googleAndroidIndex = getMusicUrl(musicItem, {
            query: parse(location.search),
            host: `android-app://${process.env.ANDROID_APP_PACKAGE}/audiomack`
        });
        const googleIosIndex = getMusicUrl(musicItem, {
            query: parse(location.search),
            host: `ios-app://${process.env.IOS_APP_ID}/audiomack`
        });

        let nofollow;

        if (
            musicItem.status === 'suspended' ||
            musicItem.status === 'takedown'
        ) {
            nofollow = <meta name="robots" content="noindex, follow" />;
        }

        const artistName = getArtistName(musicItem) || 'Unnamed Artist';

        const json = {
            '@type': 'MusicRecording',
            '@context': 'https://schema.org',
            url: canonical,
            thumbnailUrl: this.getMusicArtwork(musicItem),
            description: description,
            name: title,
            dateModified: new Date(musicItem.updated * 1000).toISOString(),
            author: {
                '@type': 'Person',
                name: artistName,
                url: artistUrl
            },
            interactionStatistic: {
                '@type': 'InteractionCounter',
                interactionType: 'https://schema.org/ListenAction',
                userInteractionCount: musicItem.stats['plays-raw']
            },
            keywords: generateKeywords(musicItem),
            audio: {
                '@type': 'AudioObject',
                embedUrl: embedUrl,
                uploadDate: new Date(
                    (musicItem.created || musicItem.uploaded) * 1000
                ).toISOString(),
                playerType: 'HTML5'
            },
            provider: {
                '@type': 'Organization',
                name: process.env.AM_NAME,
                image: `${
                    process.env.AM_URL
                }/static/images/fb-graphic_275x275.jpg`
            }
        };

        if (musicItem.isrc) {
            json.isrcCode = musicItem.isrc;
        }

        if (musicItem.type === 'song') {
            const durationString = generateISODuration(musicItem.duration);

            json.duration = durationString;
            json.audio.duration = durationString;

            if (musicItem.album) {
                json.inAlbum = {
                    '@type': 'MusicAlbum',
                    name: musicItem.album
                };
            }
        } else if (
            musicItem.type === 'album' ||
            musicItem.type === 'playlist'
        ) {
            const isAlbum = musicItem.type === 'album';

            json['@type'] = isAlbum ? 'MusicAlbum' : 'MusicPlaylist';

            if (isAlbum) {
                json.byArtist = {
                    '@type': 'MusicGroup',
                    name: artistName,
                    url: artistUrl
                };
            }

            json.track = {
                '@type': 'ItemList',
                numberOfItems: musicItem.tracks.length,
                itemListElement: musicItem.tracks.map((track, i) => {
                    const item = {
                        '@type': 'MusicRecording',
                        name: track.title
                    };

                    if (track.duration) {
                        item.duration = generateISODuration(track.duration);
                    }

                    if (track.isrc) {
                        item.isrcCode = track.isrc;
                    }

                    if (track.album) {
                        item.inAlbum = {
                            '@type': 'MusicAlbum',
                            name: track.album
                        };
                    }

                    if (!isAlbum) {
                        item.byArtist = {
                            '@type': 'MusicGroup',
                            name: getArtistName(track),
                            url: getArtistUrl(track, {
                                host: process.env.AM_URL
                            })
                        };
                    }

                    return {
                        '@type': 'ListItem',
                        position: i + 1,
                        item: item
                    };
                })
            };
        }

        return (
            <Helmet>
                <title>{title}</title>
                <link rel="canonical" href={canonical} />
                {nofollow}
                <meta name="description" content={description} />
                <meta name="twitter:card" content="player" />
                <meta
                    name="twitter:title"
                    content={this.getTwitterTitle(musicItem)}
                />
                <meta name="twitter:description" content={description} />
                <meta
                    name="twitter:image"
                    content={this.getMusicArtwork(musicItem)}
                />
                <meta name="twitter:player" content={twitterEmbedUrl} />
                <meta name="twitter:player:width" content="400" />
                <meta
                    name="twitter:player:height"
                    content={this.getTwitterPlayerHeight(musicItem)}
                />

                <meta name="twitter:app:url:iphone" content={appDeepLink} />
                <meta name="twitter:app:url:googleplay" content={appDeepLink} />

                <meta property="al:ios:url" content={appDeepLink} />
                <meta property="al:android:url" content={appDeepLink} />

                {this.renderTwitterCreator(musicItem)}
                <meta
                    property="og:title"
                    content={this.getTwitterTitle(musicItem)}
                />
                <meta
                    property="og:image"
                    content={this.getMusicArtwork(musicItem)}
                />
                <meta property="og:description" content={description} />
                <meta property="og:url" content={canonical} />
                <meta property="og:type" content={`music.${musicItem.type}`} />
                <link href={googleIosIndex} rel="alternate" />
                <link href={googleAndroidIndex} rel="alternate" />
                <link
                    href={canonical}
                    media="only screen and (max-width: 640px)"
                    rel="alternate"
                />
                <link
                    href={oembedUrl}
                    rel="alternate"
                    type="application/json+oembed"
                    title={title}
                />
                {this.renderBranchTags(musicItem, currentUser)}
                <script type="application/ld+json">
                    {JSON.stringify([
                        json,
                        {
                            itemListElement: [
                                {
                                    '@type': 'ListItem',
                                    position: 1,
                                    item: {
                                        '@id': artistUrl,
                                        name: artistName
                                    }
                                },
                                {
                                    '@type': 'ListItem',
                                    position: 2,
                                    item: {
                                        '@id': `${artistUrl}/uploads`,
                                        name: `Recent tracks and albums from ${artistName}`
                                    }
                                }
                            ],
                            '@type': 'BreadcrumbList',
                            '@context': 'https://schema.org'
                        }
                    ])}
                </script>
            </Helmet>
        );
    }
}
