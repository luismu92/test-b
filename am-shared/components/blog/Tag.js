import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { getBlogTagUrl } from 'utils/blog';

import styles from './Tag.module.scss';

export default class Tag extends Component {
    static propTypes = {
        tag: PropTypes.object,
        size: PropTypes.string,
        style: PropTypes.object
    };

    static defaultProps = {
        size: ''
    };

    render() {
        const { tag, size } = this.props;

        if (!tag) {
            return null;
        }

        const klass = classnames(styles.tag, {
            [styles.small]: size === 'small',
            [styles.large]: size === 'large'
        });

        const tagStyle = this.props.style || {};

        return (
            <Link to={getBlogTagUrl(tag)} className={klass} style={tagStyle}>
                {tag.name}
            </Link>
        );
    }
}
