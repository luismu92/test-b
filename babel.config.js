const pkg = require('./package.json');

function getBabelConfig({ isProd, browserList, modern, isServer }) {
    const targets = isServer
        ? {
              node: 'current'
          }
        : {
              browsers: browserList,
              esmodules: modern
          };
    const plugins = [
        'react-hot-loader/babel',
        [
            '@babel/plugin-transform-runtime',
            {
                corejs: 2
            }
        ],
        [
            'universal-import',
            {
                disableWarnings: true
            }
        ],
        [
            'babel-plugin-transform-builtin-extend',
            {
                globals: ['Error'],
                approximate: true
            }
        ],
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-syntax-import-meta',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-json-strings',
        [
            '@babel/plugin-proposal-decorators',
            {
                legacy: true
            }
        ],
        '@babel/plugin-proposal-function-sent',
        '@babel/plugin-proposal-export-namespace-from',
        '@babel/plugin-proposal-numeric-separator',
        '@babel/plugin-proposal-throw-expressions'
    ];

    // We dont want the warning:
    //
    // @babel/preset-env: esmodules and browsers targets have been specified together.
    // `browsers` target, `Safari >= 10.1,Chrome >= 61,Firefox >= 60,Edge >= 16` will be ignored.
    //
    // To appear in the final stats.json

    if (modern) {
        targets.browsers = undefined;
    } else {
        plugins.push('@babel/plugin-transform-arrow-functions');
    }

    return {
        presets: [
            [
                '@babel/preset-env',
                {
                    forceAllTransforms: isProd,
                    modules: !isServer && modern ? false : 'commonjs',
                    targets: targets,
                    // debug: true,
                    corejs: 2,
                    useBuiltIns: modern ? 'usage' : 'entry'
                }
            ],
            '@babel/preset-react'
        ],
        plugins,
        env: {
            test: {
                plugins: [
                    [
                        'universal-import',
                        {
                            babelServer: true
                        }
                    ]
                ]
            }
        }
    };
}

module.exports = function(api) {
    return getBabelConfig({
        isProd: api.env('production'),
        browserList: pkg.browserslist.legacyBrowsers,
        modern: false,
        isServer: false
    });
};

module.exports.getBabelConfig = getBabelConfig;
