import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PlayButton from '../components/PlayButton';

import FastBackwardIcon from '../icons/fast-backward';
import FastForwardIcon from '../icons/fast-forward';
import SeekBackwardsIcon from 'icons/seek-backwards';
import SeekForwardsIcon from 'icons/seek-forwards';

export default class PlayerControls extends Component {
    static propTypes = {
        item: PropTypes.object,
        paused: PropTypes.bool,
        loading: PropTypes.bool,
        onControlClick: PropTypes.func,
        queueLength: PropTypes.number
    };

    renderPrevButton(item) {
        if (!item) {
            return null;
        }

        const { onControlClick, queueLength } = this.props;

        let action = 'back';
        let label = 'Previous Song';
        let icon = <FastBackwardIcon />;

        if (item.genre === 'podcast') {
            action = 'seek-back';
            label = 'Skip backwards';
            icon = <SeekBackwardsIcon />;
        }

        return (
            <button
                className="player__control player__control--back"
                onClick={onControlClick}
                data-action={action}
                aria-label={label}
                disabled={queueLength === 1}
            >
                {icon}
            </button>
        );
    }

    renderNextButton(item) {
        if (!item) {
            return null;
        }

        const { onControlClick, queueLength } = this.props;

        let action = 'forward';
        let label = 'Next Song';
        let icon = <FastForwardIcon />;

        if (item.genre === 'podcast') {
            action = 'seek-forward';
            label = 'Skip forwards';
            icon = <SeekForwardsIcon />;
        }

        return (
            <button
                className="player__control player__control--forwards"
                onClick={onControlClick}
                data-action={action}
                aria-label={label}
                disabled={queueLength === 1}
            >
                {icon}
            </button>
        );
    }

    render() {
        const { item, paused, onControlClick, loading } = this.props;

        return (
            <div className="player__controls u-text-center">
                {this.renderPrevButton(item)}
                <PlayButton
                    onlySizeLoader
                    className="player__control player__control--main"
                    loading={loading}
                    disabled={loading}
                    paused={paused}
                    onButtonClick={onControlClick}
                    size={32}
                />
                {this.renderNextButton(item)}
            </div>
        );
    }
}
