import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
    renderFeaturingLinks,
    getArtistName,
    getMusicUrl,
    buildDynamicImage
} from 'utils/index';

import Avatar from 'components/Avatar';

export default class PlayerArtistInfo extends Component {
    static propTypes = {
        player: PropTypes.object
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    renderFeaturing(featuring) {
        if (!featuring) {
            return null;
        }

        return (
            <span className="player__featuring u-d-block u-trunc u-fw-700 u-fs-11">
                {renderFeaturingLinks(featuring)}
            </span>
        );
    }

    render() {
        const { player } = this.props;
        const { currentSong: currentMusicItem } = player;

        if (!currentMusicItem) {
            return null;
        }

        const artist = getArtistName(currentMusicItem);
        const title = currentMusicItem.title;
        const uploader = currentMusicItem.parentDetails
            ? currentMusicItem.parentDetails.uploader
            : null;
        const url = getMusicUrl({
            ...currentMusicItem,
            uploader: uploader || currentMusicItem.uploader
        });
        const imageSize = 48;

        const artwork = buildDynamicImage(
            currentMusicItem.image_base || currentMusicItem.image,
            {
                width: imageSize,
                height: imageSize,
                max: true
            }
        );

        const retinaArtwork = buildDynamicImage(
            currentMusicItem.image_base || currentMusicItem.image,
            {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            }
        );
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        return (
            <div className="player__details-wrap u-clearfix">
                <div className="player__artwork">
                    <Link to={url}>
                        <Avatar
                            image={artwork}
                            srcSet={srcSet}
                            size={48}
                            square
                        />
                    </Link>
                </div>
                <p className="player__details">
                    <Link to={url}>
                        <span className="player__heading player__artist u-d-block u-trunc">
                            {artist}
                        </span>{' '}
                        <span className="player__heading player__title u-d-block u-trunc">
                            <strong>{title}</strong>
                        </span>
                    </Link>
                    {this.renderFeaturing(currentMusicItem.featuring || null)}
                </p>
            </div>
        );
    }
}
