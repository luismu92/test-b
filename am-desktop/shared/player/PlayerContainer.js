import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import classnames from 'classnames';
import { parse } from 'query-string';

import {
    convertSecondsToTimecode,
    clamp,
    getArtistName,
    getMusicUrl,
    debounce,
    triggerResizeEvent,
    ucfirst,
    getUploader,
    getTwitterShareLink,
    getFacebookShareLink
} from 'utils/index';
import storage, {
    STORAGE_KEY_VOLUME,
    STORAGE_MUSIC_HISTORY,
    STORAGE_ACTIVE_PLAYER_ID
} from 'utils/storage';
import PlayerAudioContainer from 'components/player/PlayerAudioContainer';
import WaveformContainer from 'components/WaveformContainer';
import BodyClickListener from 'components/BodyClickListener';

import SoundcloudIcon from '../icons/soundcloud-icon';
import EmbedIcon from '../icons/embed-close';
import PlusIcon from '../icons/plus';
import RetweetIcon from '../icons/retweet';
import HeartIcon from 'icons/heart';
import VolumeMuteIcon from '../icons/volume-mute';
import VolumeLowIcon from '../icons/volume-low';
import VolumeHighIcon from '../icons/volume-high';
import ThreeDotsIcon from '../icons/three-dots';
import AirplayIcon from '../icons/airplay';
import QueueIcon from '../icons/queue';
import ChevronDownIcon from '../icons/chevron-down-rounded';
import TwitterIcon from 'icons/twitter-logo-new';
import FacebookIcon from 'icons/facebook-letter-logo';

import {
    pause,
    play,
    prev,
    next as playerNext,
    setVolume,
    loadMusicFromStorage,
    unmute,
    mute,
    seek,
    swapQueueItems,
    clearQueue,
    shuffleTracks,
    removeQueueItem
} from '../redux/modules/player';
import {
    showModal,
    hideModal,
    MODAL_TYPE_ADD_TO_PLAYLIST,
    MODAL_TYPE_AUTH,
    MODAL_TYPE_EMBED,
    MODAL_TYPE_SAVE_TO_PLAYLIST,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import { addSong } from '../redux/modules/playlist';
import {
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction
} from '../redux/modules/user';
import { getPlaylistsWithSongId } from '../redux/modules/user/playlists';
import {
    favoritePlaylist,
    unfavoritePlaylist
} from '../redux/modules/playlist';
import { addToast } from '../redux/modules/toastNotification';

import NewPlaylistModal from '../modal/NewPlaylistModal';
import QueueAlertModal from '../modal/QueueAlertModal';
import QueueDuplicateModal from '../modal/QueueDuplicateModal';
import EmbedModal from '../modal/EmbedModal';
import VideoModal from '../modal/VideoModal';

import PlayerArtistInfo from './PlayerArtistInfo';
import PlayerControls from './PlayerControls';
import PlayerQueueMenu from './PlayerQueueMenu';

class PlayerContainer extends Component {
    static propTypes = {
        player: PropTypes.object,
        modal: PropTypes.object,
        location: PropTypes.object,
        stats: PropTypes.object,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            collapsed: false,
            queueMenuActive: false,
            volume: null,
            PlayerChromecastContainer: null,
            chromecastName: null,
            chromecastAvailable: false,
            chromecastError: false,
            airplayAvailable: false,
            airplayActive: false,
            playerTooltipActive: false
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        const volume = storage.get(STORAGE_KEY_VOLUME);

        // Doing this because the chromecast button puts the contents of the onClick event handler in the HTML
        this._castButton = document.getElementById('google-cast-button');

        if (volume && !isNaN(volume)) {
            dispatch(setVolume(volume));
        }

        window.addEventListener('keyup', this.handlePlayerKeyUp, false);
        window.addEventListener('storage', this.handleStorageChange, false);

        this._castButton.addEventListener(
            'click',
            this.handleControlClick,
            false
        );

        this.loadQueueHistory();
        this.maybeLoadChomecast();
        this.exposePlayer();
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.player.queue.length &&
            !this.props.player.queue.length &&
            this.state.queueMenuActive
        ) {
            // eslint-disable-next-line
            this.setState({
                queueMenuActive: false
            });
        }
    }

    componentWillUnmount() {
        this.cleanUpVolumeEvents();

        window.removeEventListener('keyup', this.handlePlayerKeyUp);
        window.removeEventListener('storage', this.handleStorageChange);

        this._castButton.removeEventListener('click', this.handleControlClick);
        this._castButton = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleControlClick = (e) => {
        const { dispatch, player } = this.props;
        const button = e.currentTarget;
        const action = button.getAttribute('data-action');
        const musicItem = this.getCurrentSong(player);

        this.setState({
            playerTooltipActive: false
        });

        switch (action) {
            case 'airplay': {
                const playerEl = document.getElementById('player-audio');

                if (playerEl) {
                    playerEl.webkitShowPlaybackTargetPicker();
                }
                break;
            }

            case 'chromecast': {
                button.blur();
                break;
            }

            case 'queue':
                this.handleQueueMenuToggle();
                break;

            case 'back':
                dispatch(prev());
                break;

            case 'forward':
                dispatch(playerNext());
                break;

            case 'seek-back': {
                const { currentTime } = player;
                const adjustTime = -15;

                let newTime = currentTime + adjustTime;
                if (newTime < 0) {
                    newTime = 0;
                }

                dispatch(seek(newTime));
                break;
            }

            case 'seek-forward': {
                const { duration, currentTime } = player;
                const adjustTime = 30;

                let newTime = currentTime + adjustTime;
                if (newTime > duration) {
                    newTime = duration - 1;
                }

                dispatch(seek(newTime));
                break;
            }

            case 'play':
                dispatch(play());
                break;

            case 'pause':
                dispatch(pause());
                break;

            case 'embed':
                dispatch(
                    showModal(MODAL_TYPE_EMBED, {
                        item: player.currentSong
                    })
                );
                break;

            case 'playlist':
            case 'reup':
            case 'favorite':
                this.doAction(action, musicItem);
                break;

            default:
                console.warn('`data-action` not found on', e.currentTarget);
                break;
        }

        button.blur();
    };

    handleVolumeMouseUp = () => {
        this.cleanUpVolumeEvents();
    };

    handleVolumeMouseDown = (e) => {
        this._volumeFill = e.currentTarget.querySelector(
            '.volume-slider__backfill'
        );
        this._volumeFillRect = this._volumeFill.getBoundingClientRect();

        this.volume(e.clientY, this._volumeFillRect);

        document.body.classList.add('no-select');

        window.addEventListener('mousemove', this.handleVolumeMouseMove, false);
        window.addEventListener('mouseup', this.handleVolumeMouseUp, false);
    };

    handleVolumeMouseMove = (e) => {
        this.volume(e.clientY, this._volumeFillRect);
    };

    handleVolumeMuteClick = () => {
        const { dispatch, player } = this.props;

        if (player.muted) {
            dispatch(unmute());
        } else {
            dispatch(mute());
        }
    };

    // @todo contain basic modal closing logic in modal component
    // and call props.onClose if provided as a prop
    handleModalClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handlePlayerKeyUp = (e) => {
        const { dispatch, player } = this.props;
        const pressed = e.keyCode;
        const space = 32;
        const right = 39;
        const left = 37;
        const allowed = [space, right, left];
        const isValidKey = allowed.indexOf(pressed) !== -1;
        const playerHasActiveSong = !!player.currentSong;
        const playPause = player.paused ? play : pause;
        const lowercased = e.target.tagName.toLowerCase();
        const inputIsFocused =
            lowercased === 'input' || lowercased === 'textarea';

        if (playerHasActiveSong && isValidKey && !inputIsFocused) {
            e.preventDefault();

            switch (pressed) {
                case space:
                    dispatch(playPause());
                    break;

                case right:
                    dispatch(playerNext());
                    break;

                case left:
                    dispatch(prev());
                    break;

                default:
                    break;
            }
        }
    };

    handleStorageChange = (e) => {
        if (e.key === STORAGE_ACTIVE_PLAYER_ID) {
            const isSamePlayer = e.newValue === this.props.stats.sessionId;

            if (!isSamePlayer) {
                this.props.dispatch(pause());
            }
        }
    };

    handleChromecastAvailability = (state) => {
        this.setState({
            chromecastAvailable: state
        });
    };

    handleChromecastSessionStart = (name) => {
        this.setState({
            chromecastName: name,
            chromecastError: false
        });
    };

    handleChromecastSessionStartFailed = () => {
        this.setState({
            chromecastName: null,
            chromecastError: true
        });
    };

    handleChromecastSessionResumed = (name) => {
        this.setState({
            chromecastName: name,
            chromecastError: false
        });
    };

    handleChromecastSessionEnded = () => {
        this.setState({
            chromecastName: null,
            chromecastError: false
        });
    };

    handleAirplayAvailabilityChange = (e) => {
        switch (e.availability) {
            case 'available':
                this.setState({
                    airplayAvailable: true
                });
                break;

            default:
                this.setState({
                    airplayAvailable: false
                });
                break;
        }
    };

    handleAirplayStateChange = (active) => {
        this.setState({
            airplayActive: active
        });
    };

    handlePlayerMenuClick = () => {
        this.setState({
            playerTooltipActive: !this.state.playerTooltipActive
        });
    };

    handleCollapseClick = () => {
        const isCollapsed = !this.state.collapsed;
        const fn = isCollapsed ? 'add' : 'remove';

        document.body.classList[fn]('has-collapsed-player');

        if (!isCollapsed) {
            this.forceResize();
        }

        this.setState({
            collapsed: isCollapsed,
            queueMenuActive: isCollapsed ? false : this.state.queueMenuActive
        });
    };

    handleQueueReorder = (fromIndex, toIndex) => {
        this.props.dispatch(swapQueueItems(fromIndex, toIndex));
    };

    handleQueueTrackClick = (e) => {
        const button = e.currentTarget;
        const queueIndex = parseInt(
            button.getAttribute('data-track-index'),
            10
        );

        if (isNaN(queueIndex)) {
            console.warn('Couldnt get queueIndex. Not playing.');
            return;
        }

        this.props.dispatch(play(queueIndex));
    };

    handleQueueTrackActionClick = (e) => {
        const button = e.currentTarget;
        const action = button.getAttribute('data-track-action');
        const queueIndex = parseInt(
            button.getAttribute('data-track-index'),
            10
        );
        const { dispatch, player } = this.props;
        const musicItem = player.queue[queueIndex];

        switch (action) {
            case 'playlist':
            case 'reup':
            case 'favorite':
                this.doAction(action, musicItem);
                break;

            case 'remove-track':
                dispatch(removeQueueItem(queueIndex));
                break;

            default:
                break;
        }
    };

    handleQueueClearClick = () => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Clear Queue',
                message:
                    'This will clear all history and future music within the queue.',
                handleConfirm: () => {
                    dispatch(clearQueue());
                    dispatch(hideModal());
                },
                confirmButtonText: 'Clear'
            })
        );
    };

    handleQueueSaveClick = () => {
        const { dispatch, player, currentUser } = this.props;
        const queuedAction = () => {
            // eslint-disable-line
            dispatch(
                showModal(MODAL_TYPE_SAVE_TO_PLAYLIST, {
                    songList: player.queue
                })
            );
        };

        if (!currentUser.profile) {
            dispatch(queueAction(queuedAction));
            this.showLoginModal();
            return;
        }

        queuedAction();
    };

    handleQueueShuffle = () => {
        this.props.dispatch(shuffleTracks());
    };

    handleQueueMenuToggle = () => {
        this.setState({
            queueMenuActive: !this.state.queueMenuActive
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getCurrentSong(player) {
        return player.queue[player.queueIndex] || {};
    }

    getAlbumPlaylistText(musicItem) {
        if (!musicItem) {
            return null;
        }

        let text;
        let details;

        if (musicItem.parentDetails) {
            text = ucfirst(musicItem.parentDetails.type);
            details = musicItem.parentDetails;
        }

        if (!text) {
            return null;
        }

        return (
            <span className="player__album-text u-trunc">
                From {text}:{' '}
                <Link to={getMusicUrl(details)}>{details.title}</Link>
            </span>
        );
    }

    forceResize = debounce(() => {
        triggerResizeEvent();
    }, 150);

    isFavorited(currentUser, musicItem) {
        if (!currentUser.isLoggedIn || !musicItem) {
            return false;
        }

        return (
            currentUser.profile.favorite_music &&
            currentUser.profile.favorite_music.indexOf(musicItem.id) !== -1
        );
    }

    doAction(action, musicItem) {
        const { currentUser, dispatch } = this.props;

        switch (action) {
            case 'favorite': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.favoriteItem(musicItem, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'reup': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.reupItem(musicItem, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'playlist': {
                const queuedAction = () => this.playlistItem(musicItem);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            default:
                break;
        }
    }

    loadQueueHistory() {
        const { dispatch } = this.props;
        const json = true;
        const history = storage.get(STORAGE_MUSIC_HISTORY, json);

        if (history) {
            dispatch(loadMusicFromStorage(history));
        }
    }

    maybeLoadChomecast() {
        if (window.__UA__.browser !== 'Chrome') {
            return;
        }

        require.ensure([], (require) => {
            this.setState({
                PlayerChromecastContainer: require('./PlayerChromecastContainer')
                    .default
            });
        });
    }

    exposePlayer() {
        const { dispatch } = this.props;

        window.amPlayer = {
            play: () => {
                if (!this.props.player.currentSong) {
                    const albumPlaylistPlayButton = document.querySelector(
                        '[data-play-start]'
                    );

                    if (albumPlaylistPlayButton) {
                        albumPlaylistPlayButton.click();
                        return;
                    }

                    const musicDetailPlayButton = document.querySelector(
                        '[data-action="play"]'
                    );

                    if (musicDetailPlayButton) {
                        musicDetailPlayButton.click();
                        return;
                    }
                }
                dispatch(play());
            },
            next: () => dispatch(playerNext()),
            prev: () => dispatch(prev()),
            pause: () => dispatch(pause()),
            paused: () => this.props.player.paused,
            info: () => {
                const currentUser = this.props.currentUser;
                const {
                    currentSong,
                    currentTime,
                    duration
                } = this.props.player;
                const info = {
                    title: null,
                    artist: null,
                    album: null,
                    image: null,
                    favorited: false,
                    progress: '0:00 of 0:00'
                };

                if (!currentSong) {
                    return info;
                }

                info.title = currentSong.title;
                info.artist = getArtistName(currentSong);
                info.image = currentSong.image;

                if (currentUser.isLoggedIn) {
                    info.favorited = currentUser.profile.favorite_music.includes(
                        currentSong.id
                    );
                }

                if (currentSong.album) {
                    info.album = currentSong.album;
                }

                if (
                    currentSong.parentDetails &&
                    currentSong.parentDetails.type === 'album'
                ) {
                    info.album = currentSong.parentDetails.title;
                }

                info.progress = `${convertSecondsToTimecode(
                    currentTime
                )} of ${convertSecondsToTimecode(duration)}`;

                return info;
            },
            favorite: () => {
                const currentSong = this.getCurrentSong(this.props.player);

                if (!this.props.currentUser.isLoggedIn) {
                    return;
                }

                this.doAction('favorite', currentSong);
            }
        };
    }

    showLoginModal() {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_AUTH, { type: 'login' }));
    }

    favoriteItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        let action;

        if (item.type === 'playlist') {
            // Dont allow a newly logged in user to undo on an item
            if (
                newlyLoggedInUser ||
                user.favorite_playlists.indexOf(item.id) === -1
            ) {
                action = 'favorite';
                dispatch(favoritePlaylist(item.id));
            } else {
                action = 'unfavorite';
                dispatch(unfavoritePlaylist(item.id));
            }
        }
        // Dont allow a newly logged in user to undo on an items
        else if (
            newlyLoggedInUser ||
            user.favorite_music.indexOf(item.id) === -1
        ) {
            action = 'favorite';
            dispatch(favorite(item.id));
        } else {
            action = 'unfavorite';
            dispatch(unfavorite(item.id));
        }

        dispatch(
            addToast({
                type: item.type,
                action: action,
                item: `${getArtistName(item)} - ${item.title}`
            })
        );
    }

    reupItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        let action;

        // Make sure we're comparing the same types
        const itemId = item.id;

        // Dont allow a newly logged in user to undo on an items
        if (newlyLoggedInUser || user.reups.indexOf(itemId) === -1) {
            action = 'reup';
            dispatch(repost(itemId));
        } else {
            action = 'unreup';
            dispatch(unrepost(itemId));
        }

        dispatch(
            addToast({
                type: item.type,
                action: action,
                item: `${getArtistName(item)} - ${item.title}`
            })
        );
    }

    playlistItem(item) {
        const { dispatch, player } = this.props;
        const song = this.getCurrentSong(player);

        if (item.type === 'album' || item.type === 'playlist') {
            song.artist = item.artist;
        }

        // Account for album tracks which may not have id
        const id = song.id || song.song_id;

        dispatch(addSong(song));
        dispatch(getPlaylistsWithSongId(id));
        dispatch(showModal(MODAL_TYPE_ADD_TO_PLAYLIST));
    }

    volume(clientY, rect) {
        const { dispatch } = this.props;
        const numerator = rect.height - (clientY - rect.top);
        const volume = clamp(numerator, 0, rect.height) / rect.height;

        clearTimeout(this._volumeTimer);
        this._volumeTimer = setTimeout(() => {
            storage.set(STORAGE_KEY_VOLUME, volume);
            dispatch(setVolume(volume), 150);
            this.setState({
                volume: null
            });
        }, 200);

        // Use local state as an intermediate step so
        // we're not updating the whole state tree
        // on mousemove which fires a bajillion times
        // a second. Above we will reset the local state
        // to null after state tree gets updated.
        this.setState({
            volume
        });
    }

    cleanUpVolumeEvents() {
        window.removeEventListener('mousemove', this.handleVolumeMouseMove);
        window.removeEventListener('mouseup', this.handleVolumeMouseUp);
        document.body.classList.remove('no-select');

        this._volumeTimer = null;
        this._volumeFill = null;
        this._volumeFill = null;
        this._seekTime = null;
    }

    renderVolumeSlider(volume, muted) {
        const style = {
            bottom: `${volume * 100}%`
        };

        const fillStyle = {
            height: `${volume * 100}%`
        };

        if (muted) {
            style.bottom = '0%';
            fillStyle.height = '0%';
        }

        const klass = classnames('volume-slider');

        return (
            <span
                className={klass}
                onMouseDown={this.handleVolumeMouseDown}
                aria-valuemin="0"
                aria-valuemax="100"
                aria-valuenow={Math.round(100 * volume)}
                role="slider"
                tabIndex="0"
            >
                <span className="volume-slider__inner">
                    <span className="volume-slider__scrubber" style={style} />
                    <span className="volume-slider__backfill" />
                    <span className="volume-slider__fill" style={fillStyle} />
                </span>
            </span>
        );
    }

    // Kinda shitty the way i'm doing this. rendering
    // all the icons in the DOM so we dont get flashing
    // between icon changes. Same as the play/pause/load
    // component. A better way to do this might be to
    // have the loud icon inline'd and we hide each wave
    // coming out of the speaker individually. That
    // would require using the actual inline svg
    // instead of the sprite
    renderVolumeIcon(volume, muted) {
        const hide = {
            display: 'none'
        };

        if (volume > 0.1 && volume < 0.5 && !muted) {
            return {
                icon: [
                    <VolumeMuteIcon key="none" style={hide} />,
                    <VolumeLowIcon key="low" />,
                    <VolumeHighIcon key="high" style={hide} />
                ],
                klass: 'player__icon--volume player__icon--volume-low'
            };
        }

        if (volume >= 0.5 && !muted) {
            return {
                icon: [
                    <VolumeMuteIcon key="none" style={hide} />,
                    <VolumeLowIcon key="low" style={hide} />,
                    <VolumeHighIcon key="high" />
                ],
                klass: 'player__icon--volume player__icon--volume-high'
            };
        }

        return {
            icon: [
                <VolumeMuteIcon key="none" />,
                <VolumeLowIcon key="low" style={hide} />,
                <VolumeHighIcon key="high" style={hide} />
            ],
            klass: 'player__icon--volume'
        };
    }

    renderPlayerMenu() {
        const { player } = this.props;
        const { playerTooltipActive } = this.state;

        const menuClass = classnames(
            'tooltip tooltip--player tooltip--down-right-arrow sub-menu u-text-left',
            {
                'tooltip--active': playerTooltipActive
            }
        );

        let airplayIcon;

        if (this.state.airplayAvailable) {
            airplayIcon = (
                <li className="sub-menu__item">
                    <button
                        data-action="airplay"
                        onClick={this.handleControlClick}
                    >
                        <AirplayIcon className="sub-menu__icon sub-menu__icon--airplay" />
                        Airplay
                    </button>
                </li>
            );
        }

        let shareButtons;

        if (player.currentSong) {
            shareButtons = (
                <span>
                    <li className="sub-menu__item">
                        <a
                            href={getTwitterShareLink(
                                process.env.AM_URL,
                                player.currentSong
                            )}
                            target="_blank"
                            rel="nofollow noopener"
                        >
                            <TwitterIcon className="sub-menu__icon sub-menu__icon--twitter" />
                            Share on Twitter
                        </a>
                    </li>
                    <li className="sub-menu__item">
                        <a
                            href={getFacebookShareLink(
                                process.env.AM_URL,
                                player.currentSong,
                                process.env.FACEBOOK_APP_ID
                            )}
                            target="_blank"
                            rel="nofollow noopener"
                        >
                            <FacebookIcon className="sub-menu__icon sub-menu__icon--facebook" />
                            Share on Facebook
                        </a>
                    </li>
                </span>
            );
        }

        return (
            <ul className={menuClass}>
                <li className="sub-menu__item">
                    <button
                        data-action="embed"
                        onClick={this.handleControlClick}
                    >
                        <EmbedIcon className="sub-menu__icon sub-menu__icon--embed" />
                        Embed
                    </button>
                </li>
                {airplayIcon}
                {shareButtons}
            </ul>
        );
    }

    render() {
        const { player, currentUser, modal } = this.props;
        const {
            paused,
            duration,
            currentTime,
            currentSong: currentMusicItem,
            loading,
            volume,
            muted
        } = player;
        const elapsedDisplay = convertSecondsToTimecode(currentTime || 0);
        const durationDisplay = convertSecondsToTimecode(duration || 0);
        const isActive = !!currentMusicItem;
        const klass = classnames('player player--wide', {
            'player--active': isActive,
            'player--icon-visible': this.state.playerTooltipActive,
            'player--collapsed': this.state.collapsed
        });

        const song = this.getCurrentSong(player);
        const isPlaylist = (currentMusicItem || {}).type === 'playlist';

        const style = {};
        const volumeValue =
            typeof this.state.volume !== 'number' ? volume : this.state.volume;
        const { klass: volumeClass, icon: volumeIcon } = this.renderVolumeIcon(
            volumeValue,
            muted
        );

        let soundcloudButton;

        if (song && song.is_soundcloud) {
            soundcloudButton = (
                <a
                    href={song.sourceUrl}
                    target="_blank"
                    rel="nofollow noopener"
                    className="player__icon player__icon--soundcloud"
                    data-tooltip="Listen on Soundcloud"
                >
                    <SoundcloudIcon />
                </a>
            );
        }

        const chromecastClass = classnames(
            'player__icon player__icon--chromecast',
            {
                'player__icon--active': this.state.chromecastAvailable,
                'player__icon--error': this.state.chromecastError
            }
        );

        let chromecastTooltip = this.state.chromecastName
            ? `Playing on ${this.state.chromecastName}`
            : 'Listen on Chromecast';

        if (this.state.chromecastError) {
            chromecastTooltip =
                'There was an error starting your cast session. Please try again.';
        }

        const chromecastButton = (
            <button
                id="google-cast-button"
                is="google-cast-button"
                class={chromecastClass}
                data-tooltip={chromecastTooltip}
                data-tooltip-danger={this.state.chromecastError ? true : null}
                data-action="chromecast"
                aria-label="Listen on Chromecast"
            />
        );

        let playerChromecast;

        if (this.state.PlayerChromecastContainer) {
            const PlayerChromecastContainer = this.state
                .PlayerChromecastContainer;

            playerChromecast = (
                <PlayerChromecastContainer
                    promoKey={parse(this.props.location.search).key}
                    onAvailablilityChange={this.handleChromecastAvailability}
                    onSessionStart={this.handleChromecastSessionStart}
                    onSessionStartFailed={
                        this.handleChromecastSessionStartFailed
                    }
                    onSessionResumed={this.handleChromecastSessionResumed}
                    onSessionEnded={this.handleChromecastSessionEnded}
                />
            );
        }

        const buttonClass = 'player__icon';
        const hasReup =
            currentUser.profile &&
            currentUser.profile.reups &&
            currentUser.profile.reups.indexOf(song.id) !== -1;
        const reupClass = classnames(`${buttonClass} player__icon--reup`, {
            'player__icon--active': hasReup
        });
        let reupButton;

        const uploader = isPlaylist
            ? getUploader(song)
            : getUploader(currentMusicItem || {});

        if (
            !isPlaylist &&
            (!currentUser.isLoggedIn ||
                (currentUser.isLoggedIn &&
                    currentUser.profile.id !== (uploader || {}).id))
        ) {
            reupButton = (
                <button
                    className={reupClass}
                    aria-label={hasReup ? 'Remove Re-Up' : 'Re-Up'}
                    data-tooltip={hasReup ? 'Remove Re-Up' : 'Re-Up'}
                    data-action="reup"
                    onClick={this.handleControlClick}
                >
                    <RetweetIcon />
                </button>
            );
        }

        const hasFavorite = this.isFavorited(currentUser, song);
        const favoriteClass = classnames(`${buttonClass} player__icon--fav`, {
            'player__icon--active': hasFavorite
        });

        const queueClass = classnames('player__icon player__icon--queue', {
            'player__icon--active': this.state.queueMenuActive
        });

        return (
            <div
                className={klass}
                style={style}
                aria-hidden={!isActive}
                key="player"
            >
                <div className="player__inner">
                    <PlayerAudioContainer
                        id="player-audio"
                        onAirplayAvailabilityChange={
                            this.handleAirplayAvailabilityChange
                        }
                        onAirplayStateChange={this.handleAirplayStateChange}
                    />
                    {playerChromecast}
                    <EmbedModal
                        active={modal.type === MODAL_TYPE_EMBED}
                        dispatch={this.props.dispatch}
                        item={modal.extraData.item}
                        onClose={this.handleModalClose}
                        currentUser={this.props.currentUser}
                    />

                    <VideoModal item={modal.extraData.item} />
                    <NewPlaylistModal />
                    <QueueAlertModal />
                    <QueueDuplicateModal />

                    <PlayerControls
                        item={currentMusicItem}
                        paused={paused}
                        loading={loading || player.chromecastLoading}
                        onControlClick={this.handleControlClick}
                        queueLength={player.queue.length}
                    />
                    <PlayerArtistInfo player={player} />
                    <div className="player__waveform">
                        {this.getAlbumPlaylistText(currentMusicItem)}
                        <div className="waveform-wrap">
                            <WaveformContainer
                                musicItem={currentMusicItem}
                                color="#606060"
                            />
                            <span className="waveform__elapsed waveform__time waveform__time--dark">
                                {elapsedDisplay}
                            </span>
                            <span className="waveform__duration waveform__time waveform__time--dark">
                                {durationDisplay}
                            </span>
                        </div>
                    </div>
                    <div className="player__icons-wrap">
                        <div className="player__icons">
                            {soundcloudButton}
                            {chromecastButton}
                            <span className="u-pos-relative">
                                <PlayerQueueMenu
                                    queue={player.queue}
                                    activeIndex={player.queueIndex}
                                    active={this.state.queueMenuActive}
                                    currentUser={currentUser}
                                    isPlaying={!player.paused}
                                    onQueueReorder={this.handleQueueReorder}
                                    onQueueTrackClick={
                                        this.handleQueueTrackClick
                                    }
                                    onQueueTrackActionClick={
                                        this.handleQueueTrackActionClick
                                    }
                                    onQueueSaveClick={this.handleQueueSaveClick}
                                    onQueueClearClick={
                                        this.handleQueueClearClick
                                    }
                                    onQueueCloseClick={
                                        this.handleQueueMenuToggle
                                    }
                                    onQueueShuffle={this.handleQueueShuffle}
                                />
                                <button
                                    className={queueClass}
                                    data-tooltip="Queue"
                                    data-action="queue"
                                    aria-label={
                                        this.state.queueMenuActive
                                            ? 'Close queue menu'
                                            : 'Hide queue menu'
                                    }
                                    onClick={this.handleControlClick}
                                >
                                    <QueueIcon />
                                </button>
                            </span>
                            <button
                                className={favoriteClass}
                                aria-label={
                                    hasFavorite
                                        ? 'Remove to favorites'
                                        : 'Add to favorites'
                                }
                                data-tooltip={
                                    hasFavorite
                                        ? 'Remove to favorites'
                                        : 'Add to favorites'
                                }
                                data-action="favorite"
                                onClick={this.handleControlClick}
                            >
                                <HeartIcon />
                            </button>
                            <button
                                className="player__icon player__icon--playlist"
                                data-tooltip="Add to Playlist"
                                data-action="playlist"
                                aria-label="Add song to a playlist"
                                onClick={this.handleControlClick}
                            >
                                <PlusIcon />
                            </button>
                            {reupButton}
                            <span className="volume-container u-d-inline-block">
                                {this.renderVolumeSlider(volumeValue, muted)}
                                <button
                                    className={`player__icon ${volumeClass}`}
                                    onClick={this.handleVolumeMuteClick}
                                    aria-label="Volume control. Click to toggle mute"
                                >
                                    {volumeIcon}
                                </button>
                            </span>
                            <span className="u-d-inline-block u-pos-relative">
                                <BodyClickListener
                                    shouldListen={
                                        this.state.playerTooltipActive
                                    }
                                    onClick={this.handlePlayerMenuClick}
                                    ignoreDomElement={this._tooltipButton}
                                />
                                {this.renderPlayerMenu()}
                                <button
                                    className="player__icon player__icon--dots"
                                    onClick={this.handlePlayerMenuClick}
                                    aria-label="More options"
                                    ref={(e) => {
                                        this._tooltipButton = e;
                                    }}
                                >
                                    <ThreeDotsIcon />
                                </button>
                            </span>
                        </div>
                        <button
                            className="player__icon-collapse"
                            onClick={this.handleCollapseClick}
                            aria-label="Minimize player"
                        >
                            <ChevronDownIcon />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        currentUser: state.currentUser,
        stats: state.stats,
        modal: state.modal
    };
}

export default withRouter(connect(mapStateToProps)(PlayerContainer));
