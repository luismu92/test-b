import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Transition from 'react-transition-group/Transition';

import DraggableList from '../list/DraggableList';
import MusicDetailTrack from '../browse/MusicDetailTrack';

import QueueIcon from '../icons/queue';
import ShuffleIcon from '../icons/shuffle';
import CloseIcon from '../icons/close-thin';
import PlaylistAddIcon from '../icons/playlist-add';
import DragHandle from '../../../am-shared/icons/drag-handle';

export default class PlayerQueueMenu extends Component {
    static propTypes = {
        queue: PropTypes.array.isRequired,
        currentUser: PropTypes.object.isRequired,
        activeIndex: PropTypes.number,
        active: PropTypes.bool,
        onQueueSaveClick: PropTypes.func,
        onQueueClearClick: PropTypes.func,
        onQueueCloseClick: PropTypes.func,
        onQueueShuffle: PropTypes.func,
        onQueueTrackClick: PropTypes.func,
        onQueueTrackActionClick: PropTypes.func,
        onQueueReorder: PropTypes.func,
        className: PropTypes.string
    };

    static defaultProps = {
        active: false
    };

    shouldComponentUpdate(nextProps) {
        return (
            this.props.active !== nextProps.active ||
            this.props.queue !== nextProps.queue ||
            this.props.activeIndex !== nextProps.activeIndex
        );
    }

    componentDidUpdate(prevProps) {
        if (prevProps.activeIndex !== this.props.activeIndex) {
            if (!this._isMousingOver) {
                this.scrollToActiveItem();
            }

            if (this.props.active) {
                this.DraggableList.forceUpdateGrid();
            }
        }

        if (this.props.active && !prevProps.active && this.DraggableList) {
            // Need to wait til the queue menu renders
            setTimeout(() => {
                this.scrollToActiveItem();
            }, 200);
        }
    }

    componentWillUnmount() {
        this._isMousingOver = null;
        this.DraggableList = null;
    }

    handleMouseEnter = () => {
        this._isMousingOver = true;
    };

    handleMouseLeave = () => {
        this._isMousingOver = false;
    };

    handleChange = (data) => {
        console.log(data);
    };

    handleRowHeight = () => {
        return 45;
    };

    handleRenderDragHandle = (handle, index) => {
        if (this.props.activeIndex === index) {
            return null;
        }

        return handle;
    };

    handleQueueShuffleClick = (e) => {
        this.DraggableList.forceUpdateGrid();

        this.props.onQueueShuffle(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    scrollToActiveItem() {
        // Show next couple tracks
        const additionalTracks = 2;

        if (this.DraggableList) {
            this.DraggableList.scrollToRow(
                this.props.activeIndex + additionalTracks
            );
        }
    }

    renderMenu = (state) => {
        const { className, queue, currentUser, activeIndex } = this.props;
        const klass = classnames(
            'player-queue-menu tooltip tooltip--down-right-arrow',
            {
                'tooltip--active': state === 'entered',
                [className]: className
            }
        );

        const filteredQueue = queue.filter((track) => !track.geo_restricted);

        const items = filteredQueue.map((track, i) => {
            return (
                <MusicDetailTrack
                    className="tracklist__track--queue"
                    key={`${track.id}:${i}`}
                    track={track}
                    trackIndex={i}
                    musicItem={track}
                    currentUser={currentUser}
                    isActive={i === activeIndex}
                    isInactive={i < activeIndex}
                    onTrackClick={this.props.onQueueTrackClick}
                    onTrackActionClick={this.props.onQueueTrackActionClick}
                    showRemoveFromQueue={filteredQueue.length > 1}
                    hideQueueAddActions
                    showArtwork
                    showArtistName
                    darkTheme
                    draggable
                    hideWaveform
                    location="player"
                />
            );
        });

        return (
            <div id="player-queue-menu" className={klass}>
                <header className="player-queue-menu__header u-left-right u-d-flex--align-center">
                    <h3 className="u-fs-18 u-ls-n-06">
                        <QueueIcon className="u-text-icon u-text-orange" /> Your
                        Queue
                    </h3>
                    <div className="player-queue-menu__actions u-d-flex u-d-flex--align-center">
                        <button
                            data-tooltip="Shuffle"
                            className="u-spacing-right-em player-queue-menu__shuffle"
                            disabled={
                                queue.length <= 1 ||
                                activeIndex >= queue.length - 2
                            }
                            onClick={this.handleQueueShuffleClick}
                            aria-label="Shuffle queue"
                        >
                            <ShuffleIcon />
                        </button>
                        <button
                            className="button button--pill"
                            disabled={!queue.length}
                            onClick={this.props.onQueueSaveClick}
                            aria-label="Save current queue as a playlist"
                        >
                            <PlaylistAddIcon className="u-text-icon u-text-icon--wide" />{' '}
                            Save as playlist
                        </button>
                        <button
                            className="button button--pill"
                            onClick={this.props.onQueueClearClick}
                            disabled={queue.length <= 1}
                            aria-label="Clear the current queue"
                        >
                            Clear
                        </button>
                        <button
                            className="player-queue-menu__close"
                            onClick={this.props.onQueueCloseClick}
                            title="Close"
                            aria-label="Close queue menu"
                        >
                            <CloseIcon />
                        </button>
                    </div>
                </header>
                <DraggableList
                    onDragFinish={this.props.onQueueReorder}
                    draggingClass="tracklist__track--dragging"
                    dragHandle={
                        <span className="u-drag-handle u-drag-handle--queue u-text-gray9">
                            <DragHandle />
                        </span>
                    }
                    containerProps={{
                        className: 'player-queue-menu__list',
                        onMouseEnter: this.handleMouseEnter,
                        onMouseLeave: this.handleMouseLeave
                    }}
                    virtualListProps={{
                        className: 'u-scrollable u-scrollable--light'
                    }}
                    ref={(instance) => {
                        this.DraggableList = instance;
                    }}
                    onRenderDragHandle={this.handleRenderDragHandle}
                    items={items}
                    onRowHeight={this.handleRowHeight}
                />
            </div>
        );
    };

    render() {
        const { active } = this.props;

        return (
            <Transition in={active} timeout={120} unmountOnExit>
                {this.renderMenu}
            </Transition>
        );
    }
}
