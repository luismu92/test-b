import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { loadScript } from 'utils/index';
import { chromecastMessageTypes } from 'constants/index';
import Chromecast from 'utils/Chromecast';

import {
    pause,
    play,
    setVolume,
    timeUpdate,
    next as playerNext,
    ended,
    stop,
    setChromecastState,
    setChromecastLoading
} from '../redux/modules/player';

class PlayerChromecastContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        player: PropTypes.object,
        currentUser: PropTypes.object,
        promoKey: PropTypes.string,
        onAvailablilityChange: PropTypes.func,
        onSessionStart: PropTypes.func,
        onSessionStartFailed: PropTypes.func,
        onSessionEnded: PropTypes.func,
        onSessionResumed: PropTypes.func
    };

    static defaultProps = {
        onAvailablilityChange() {},
        onSessionResumed() {},
        onSessionStart() {},
        onSessionStartFailed() {},
        onSessionEnded() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            isCurrentWithinChromecast: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        loadScript(
            'https://www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1',
            'chromecast-api'
        )
            .then(() => {
                this._chromecast = new Chromecast({
                    onAvailablilityChange: this.handleAvailablilityChange,
                    onSessionStart: this.handleSessionStart,
                    onSessionResumed: this.handleSessionResumed,
                    onSessionStartFailed: this.handleSessionStartFailed,
                    onEnded: this.handleEnded,
                    onTimeUpdate: this.handleTimeUpdate,
                    onSessionEnded: this.handleSessionEnded,
                    onVolumeChange: this.handleVolumeChange,
                    onPlaybackStateChange: this.handlePlaybackStateChange,
                    onMediaInfoChange: this.handleMediaInfoChange
                });
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    }

    componentDidUpdate(prevProps) {
        if (!this._chromecast) {
            return;
        }

        const { player } = this.props;

        if (prevProps.player.paused !== player.paused) {
            this.setPlayerState(player.paused);
        }

        if (prevProps.player.muted !== player.muted) {
            const fn = player.muted ? 'mute' : 'unmute';

            this._chromecast[fn]();
        }

        if (prevProps.player.volume !== player.volume) {
            this._chromecast.volume(player.volume);
        }

        if (player.seeking) {
            this._chromecast.seek(player.currentTime);
        }

        const song = player.currentSong || {};
        const prevSong = prevProps.player.currentSong || {};
        const currentSongUuid = `${song.type}:${song.id}`;
        const prevSongUuid = `${prevSong.type}:${prevSong.id}`;

        if (player.currentSong && currentSongUuid !== prevSongUuid) {
            // console.trace(prevSong.title, prevSongUuid, '->', song.title, currentSongUuid);
            this.setChromecastMedia();
        }
    }

    componentWillUnmount() {
        this._setMediaPromise = null;
        this._chromecast = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAvailablilityChange = (state) => {
        this.props.onAvailablilityChange(state);
    };

    handleSessionStart = () => {
        const { dispatch, player } = this.props;

        dispatch(setChromecastState(true));

        this._chromecast.volume(player.volume);
        this.setChromecastMedia();
        this.props.onSessionStart(this._chromecast.deviceName);
    };

    handleSessionStartFailed = () => {
        this.props.onSessionStartFailed();
    };

    handleSessionResumed = () => {
        const { dispatch, player } = this.props;

        dispatch(setChromecastState(true));

        this._chromecast.volume(player.volume);
        this.props.onSessionResumed(this._chromecast.deviceName);
    };

    handleSessionEnded = () => {
        const { dispatch } = this.props;

        dispatch(setChromecastState(false));

        this.props.onSessionEnded();
    };

    handleEnded = () => {
        const { dispatch, player } = this.props;

        dispatch(ended());

        if (player.queueIndex === player.queue.length - 1 && !player.repeat) {
            dispatch(stop());
            return;
        }

        dispatch(playerNext());
    };

    handleTimeUpdate = () => {
        const { dispatch } = this.props;

        if (!this.state.isCurrentWithinChromecast) {
            return;
        }

        dispatch(timeUpdate(this._chromecast.currentTime));
    };

    handlePlaybackStateChange = (isPaused) => {
        const { dispatch } = this.props;

        if (!this.state.isCurrentWithinChromecast) {
            return;
        }

        if (isPaused) {
            dispatch(pause());
            return;
        }

        dispatch(play());
    };

    handleVolumeChange = (volume) => {
        const { dispatch } = this.props;

        dispatch(setVolume(volume));
    };

    handleMediaInfoChange = (newInfo, oldInfo) => {
        const oldData = (oldInfo || {}).customData || {};
        const oldMetadata = (oldInfo || {}).metadata || {};
        const oldKey = `${oldData.type}:${oldData.id}:${
            oldMetadata.trackNumber
        }`;
        const newData = (newInfo || {}).customData || {};
        const newMetadata = (newInfo || {}).metadata || {};
        const newKey = `${newData.type}:${newData.id}:${
            newMetadata.trackNumber
        }`;

        if (newKey === oldKey) {
            return;
        }

        console.warn('Chromecast info change', oldInfo, '->', newInfo);

        const { dispatch } = this.props;
        const isChromecastSongInPlayer = this.isCurrentSong(newInfo);

        this.setState(
            {
                isCurrentWithinChromecast: isChromecastSongInPlayer
            },
            () => {
                if (!isChromecastSongInPlayer) {
                    // The Cast SDK for handling multiple tabs with
                    // with restoring sessions isnt public so the
                    // best we can do here is pause and not show
                    // time updates when opening a new tab.
                    //
                    // https://stackoverflow.com/questions/23005061/disconnect-chrome-sender-from-the-receiver
                    // https://stackoverflow.com/questions/21147421/how-does-a-sender-app-resume-the-session-after-being-killed
                    // dispatch(setChromecastState(false));
                    dispatch(timeUpdate(0));
                    dispatch(pause());
                }
            }
        );
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getCurrentSong(player) {
        return player.currentSong || {};
    }

    setChromecastMedia() {
        if (!this._chromecast.isActive()) {
            return;
        }

        const { currentUser, player, promoKey: key, dispatch } = this.props;
        const { token, secret } = currentUser;

        if (!player.currentSong) {
            this._chromecast.sendMessage(chromecastMessageTypes.idle);
            return;
        }

        // Make sure we're not calling setMedia more than once
        // without the previous setMedia promise resolving
        let promiseChain = this._setMediaPromise || Promise.resolve();

        dispatch(setChromecastLoading(true));

        promiseChain = promiseChain.then(() => {
            return this._chromecast
                .setMedia(player, { token, secret, key })
                .then(() => {
                    this.setState({
                        isCurrentWithinChromecast: true
                    });
                    dispatch(setChromecastLoading(false));
                    return;
                })
                .catch((err) => {
                    console.error('chromecast setMedia error:', err);
                });
        });

        this._setMediaPromise = promiseChain;
    }

    setPlayerState(paused) {
        if (!this.state.isCurrentWithinChromecast) {
            return;
        }

        const fn = paused ? 'pause' : 'play';

        if (this._chromecast) {
            this._chromecast[fn]();
        }
    }

    isCurrentSong(mediaInfo) {
        if (!mediaInfo) {
            return false;
        }

        const { player } = this.props;
        const currentSong = this.getCurrentSong(player);

        // If there's no customData key, this is most likely
        // a case of SESSION_RESUMED. This also happens
        // when chromecasting a song and then opening another
        // tab, and/or then clicking the chromecast button.
        if (!mediaInfo.customData) {
            // Benefit of the doubt here since we dont have access to the id
            if (mediaInfo.metadata.title === currentSong.title) {
                return true;
            }
            return false;
        }
        const currentTrackIndex = currentSong.trackIndex || 0;
        let currentMusicId = currentSong.id;
        let currentMusicType = currentSong.type;

        if (currentSong.parentDetails) {
            currentMusicId = currentSong.parentDetails.id;
            currentMusicType = currentSong.parentDetails.type;
        }

        const trackIndex = mediaInfo.metadata.trackNumber - 1 || 0;
        const { type, id } = mediaInfo.customData;

        return (
            currentMusicId === id &&
            currentMusicType === type &&
            currentTrackIndex === trackIndex
        );
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(PlayerChromecastContainer);
