import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import LoginFormContainer from './LoginFormContainer';
import AndroidLoader from '../loaders/AndroidLoader';

import AuthButtons from './AuthButtons';
import ArrowIcon from 'icons/arrow-next';
export default class Login extends Component {
    static propTypes = {
        onAuthPageClick: PropTypes.func,
        onEmailSubmit: PropTypes.func,
        onLoginSuccess: PropTypes.func,
        onSocialAuth: PropTypes.func,
        onSocialAuthWithEmail: PropTypes.func,
        onHiddenPasswordInput: PropTypes.func,
        onEmailInput: PropTypes.func,
        currentUser: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        emailStatus: PropTypes.string,
        userEmail: PropTypes.string,
        userPassword: PropTypes.string,
        socialEmailError: PropTypes.bool
    };

    constructor(props) {
        super(props);

        this.state = {
            showPassword: false
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleEyeClick = () => {
        if (this.state.showPassword) {
            this.setState({
                showPassword: false
            });
        } else {
            this.setState({
                showPassword: true
            });
        }
    };

    getAuthErrors(errors) {
        return errors.reduce(
            (acc, error) => {
                const description = error.description || '';

                // If error is about the email not existing, ignore it
                // as we have a specific check for this within the first
                // screen of the login form. This is here for the specific
                // case of using a password manager or browser saved password
                // but the credentials are incorrect.
                if (error.errorcode === 1008) {
                    return acc;
                }

                // Seems like this error code doesnt change when getting different
                // errors
                if (error.errorcode === 1037) {
                    try {
                        // Twitter error response
                        acc.social = JSON.parse(
                            error.message
                        ).errors[0].message;
                    } catch (err) {
                        // Default error
                        acc.social = error.message || description;
                    }
                } else if (description.toLowerCase().indexOf('email') !== -1) {
                    acc.email = description;
                } else if (
                    description.toLowerCase().indexOf('attempts') !== -1
                ) {
                    acc.blocked =
                        'Too many unsuccessful login attempts. Please contact Audiomack support.';
                } else if (
                    description.toLowerCase().indexOf('password') !== -1
                ) {
                    acc.password = description;
                }
                // }

                if (error.errorcode === 1056) {
                    acc.email =
                        'There is already a registered user with this social network.';
                }

                if (error.errorcode === 1057) {
                    acc.email =
                        'There is already a registered user with this social network email.';
                }

                return acc;
            },
            {
                email: '',
                password: '',
                social: ''
            }
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="u-text-red">{error.description}</p>;
    }

    renderSocialAuth(loading) {
        const { onSocialAuth, currentUser, location } = this.props;
        const errors = this.getAuthErrors(currentUser.errors);
        let loader;

        if (loading) {
            loader = (
                <div
                    className="u-overlay-loader"
                    style={{ background: 'none' }}
                >
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <div className="column small-24 u-pos-relative u-border-bottom u-spacing-bottom-25 u-padding-0">
                {loader}
                <AuthButtons
                    onSocialAuth={onSocialAuth}
                    location={location}
                    loading={loading}
                />
                {this.renderError({ description: errors.social })}
            </div>
        );
    }

    renderSocialsAndEmailInput() {
        const {
            onEmailSubmit,
            loading,
            onAuthPageClick,
            emailStatus,
            currentUser,
            onHiddenPasswordInput,
            onEmailInput,
            userEmail,
            socialEmailError
        } = this.props;
        const errors = this.getAuthErrors(currentUser.errors);

        if (emailStatus === 'notTaken' || socialEmailError) {
            return null;
        }

        let loginValidationError;

        if (emailStatus === 'taken') {
            loginValidationError = (
                <div className="u-padding-x-20 u-padding-y-10 u-ls-n-06 u-lh-14 u-text-center u-spacing-bottom-20 u-bg-gray11">
                    <p className="u-fw-700">
                        We don't have an account that matches that email.{' '}
                        <a
                            href="/join"
                            onClick={onAuthPageClick}
                            data-page="join"
                        >
                            Make a new account for free.
                        </a>
                    </p>
                </div>
            );
        }

        const hasPassword = !!this.props.userPassword;
        const passwordStyle = {};
        let submittingText = 'Logging in…';

        passwordStyle.marginBottom = 10;

        if (!hasPassword || currentUser.checkEmail.loading) {
            passwordStyle.height = 0;
            passwordStyle.overflow = 'hidden';
            submittingText = 'Verifying email…';
        }

        let blockedError;

        if (errors.blocked || errors.email) {
            blockedError = (
                <div className="u-spacing-top-10 u-padding-x-25 u-lh-15 u-text-center">
                    {this.renderError({
                        description: errors.blocked || errors.email
                    })}
                </div>
            );
        }

        const formButtonLoading =
            currentUser.loading || currentUser.checkEmail.loading;

        return (
            <Fragment>
                {this.renderSocialAuth(loading)}
                <form
                    className="column small-24 u-padding-0"
                    onSubmit={onEmailSubmit}
                >
                    {loginValidationError}
                    <input
                        key="email1"
                        type="email"
                        name="email"
                        data-testid="email"
                        onChange={onEmailInput}
                        placeholder="Enter your email"
                        defaultValue={userEmail ? userEmail : ''}
                        autoComplete="username"
                        autoCapitalize="none"
                    />
                    <div style={passwordStyle}>
                        <input
                            key="password1"
                            type="password"
                            autoComplete="current-password"
                            autoCapitalize="none"
                            name="password"
                            data-testid="hidden-password"
                            data-type="password-manager"
                            aria-hidden="true"
                            placeholder="Password"
                            className="u-margin-0"
                            onChange={onHiddenPasswordInput}
                        />
                        {this.renderError({ description: errors.password })}
                    </div>
                    <button
                        type="submit"
                        disabled={formButtonLoading}
                        className="button auth__button auth__button--submit u-text-center u-fs-14 u-fw-700"
                        data-testid="continueToLogin"
                    >
                        <span className="auth__button-text">
                            {formButtonLoading ? submittingText : 'Continue'}
                        </span>
                        <span className="auth__button-icon">
                            <ArrowIcon />
                        </span>
                    </button>
                    <p
                        style={{
                            textAlign: 'center',
                            marginTop: 10,
                            fontWeight: 600
                        }}
                    >
                        By signing up you agree to our
                        <br />
                        <a href="/about/terms-of-service" target="_blank">
                            Terms&nbsp;of&nbsp;Service
                        </a>{' '}
                        and{' '}
                        <a href="/about/privacy-policy" target="_blank">
                            Privacy&nbsp;Policy
                        </a>
                        .
                    </p>
                    {blockedError}
                </form>
            </Fragment>
        );
    }

    renderFullLoginForm() {
        const {
            onAuthPageClick,
            onLoginSuccess,
            emailStatus,
            userEmail,
            socialEmailError
        } = this.props;

        if (emailStatus !== 'notTaken' || socialEmailError) {
            return null;
        }

        return (
            <div className="column small-24 u-padding-0">
                <LoginFormContainer
                    submitButtonClass="auth__submit u-spacing-top-20"
                    onLoginSuccess={onLoginSuccess}
                    onForgotLinkClick={onAuthPageClick}
                    formClassName="collapse"
                    defaultEmail={userEmail}
                    defaultPassword={this.props.userPassword}
                />
            </div>
        );
    }

    renderEmailForSocialPanel() {
        const {
            onSocialAuthWithEmail,
            emailStatus,
            currentUser,
            userEmail,
            socialEmailError
        } = this.props;
        const errors = this.getAuthErrors(currentUser.errors);

        if (!socialEmailError) {
            return null;
        }

        let loginValidationError;

        if (emailStatus === 'notTaken') {
            loginValidationError = (
                <div className="u-padding-x-10 u-padding-y-10 u-ls-n-06 u-lh-14 u-text-center u-spacing-bottom-20 u-bg-gray11">
                    <p className="u-fw-700">
                        An account has already been created with this email
                        address.
                    </p>
                </div>
            );
        }

        const hasPassword = !!this.props.userPassword;
        const passwordStyle = {};
        let submittingText = 'Logging in…';

        passwordStyle.marginBottom = 10;

        if (!hasPassword || currentUser.checkEmail.loading) {
            submittingText = 'Verifying email…';
        }

        let blockedError;

        if (errors.blocked || errors.email) {
            blockedError = (
                <div className="u-spacing-top-10 u-padding-x-25 u-lh-15 u-text-center">
                    {this.renderError({
                        description: errors.blocked || errors.email
                    })}
                </div>
            );
        }

        const formButtonLoading =
            currentUser.loading || currentUser.checkEmail.loading;

        return (
            <Fragment>
                <form
                    className="column small-24 u-padding-0"
                    onSubmit={onSocialAuthWithEmail}
                >
                    <div className="u-padding-x-10 u-padding-y-10 u-ls-n-06 u-lh-14 u-text-center u-spacing-bottom-5">
                        <p className="u-fw-700">
                            Your social account does not have an associated
                            email, please provide one to complete account
                            creation.
                        </p>
                    </div>
                    {loginValidationError}
                    <input
                        key="email1"
                        type="email"
                        name="email"
                        data-testid="email"
                        placeholder="Enter your email"
                        defaultValue={userEmail ? userEmail : ''}
                        autoComplete="username"
                        autoCapitalize="none"
                    />
                    <button
                        type="submit"
                        disabled={formButtonLoading}
                        className="button auth__button auth__button--submit u-text-center u-fs-14 u-fw-700"
                        data-testid="socialEmailRegistration"
                    >
                        <span className="auth__button-text">
                            {formButtonLoading ? submittingText : 'Continue'}
                        </span>
                        <span className="auth__button-icon">
                            <ArrowIcon />
                        </span>
                    </button>
                    {blockedError}
                </form>
            </Fragment>
        );
    }

    render() {
        return (
            <div className="row u-margin-0">
                {this.renderSocialsAndEmailInput()}
                {this.renderFullLoginForm()}
                {this.renderEmailForSocialPanel()}
            </div>
        );
    }
}
