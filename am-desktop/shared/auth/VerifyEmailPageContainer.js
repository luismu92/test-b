import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { addToast } from '../redux/modules/toastNotification';
import { verifyHash, resendEmail } from '../redux/modules/user/index';
import { hideModal } from '../redux/modules/modal';
import VerifyEmailPage from './VerifyEmailPage';

class VerifyEmailPageContainer extends Component {
    static propTypes = {
        hash: PropTypes.string.isRequired,
        dispatch: PropTypes.func,
        location: PropTypes.object,
        history: PropTypes.object,
        currentUser: PropTypes.object
    };

    componentDidMount() {
        const { history, location } = this.props;
        const { hash, dispatch } = this.props;

        dispatch(verifyHash(hash))
            .then(() => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'Thanks for verifying your email!'
                    })
                );
                return;
            })
            .catch((err) => {
                console.log(err);
            });

        history.replace(location.pathname);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleResendEmailClick = () => {
        const { dispatch } = this.props;

        dispatch(resendEmail());
        dispatch(hideModal());
        dispatch(
            addToast({
                action: 'message',
                message: 'An email confirmation has been sent.'
            })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <VerifyEmailPage
                hash={this.props.hash}
                isVerifying={this.props.currentUser.verifyHash.loading}
                onResendEmailClick={this.handleResendEmailClick}
                error={this.props.currentUser.verifyHash.error}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(VerifyEmailPageContainer);
