import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PasswordInput from '../components/PasswordInput';

export default class LoginForm extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        onFormSubmit: PropTypes.func,
        onForgotLinkClick: PropTypes.func,
        showLabels: PropTypes.bool,
        formClassName: PropTypes.string,
        submitButtonText: PropTypes.string,
        submitButtonIcon: PropTypes.object,
        submitButtonClass: PropTypes.string,
        defaultEmail: PropTypes.string,
        defaultPassword: PropTypes.string
    };

    static defaultProps = {
        showLabels: false,
        submitButtonText: 'Sign in'
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getAuthErrors(errors) {
        return errors.reduce(
            (acc, error) => {
                const description = error.description || '';

                // Seems like this error code doesn't change when getting different errors
                if (error.errorcode === 1037) {
                    acc.facebook =
                        'Cannot retrieve necessary details from Facebook. Please make sure you have an email address set in your Facebook account and try again.';
                } else if (description.toLowerCase().indexOf('email') !== -1) {
                    acc.email = description;
                } else if (
                    description.toLowerCase().indexOf('password') !== -1
                ) {
                    acc.password = description;
                } else if (
                    description.toLowerCase().indexOf('attempts') !== -1
                ) {
                    acc.blocked =
                        'Too many unsuccessful login attempts. Please contact Audiomack support.';
                } else if (
                    description.toLowerCase().indexOf('connected') !== -1
                ) {
                    acc.password = description;
                }

                return acc;
            },
            {
                email: '',
                password: '',
                facebook: ''
            }
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="u-text-red">{error}</p>;
    }

    render() {
        const {
            onFormSubmit,
            currentUser,
            showLabels,
            submitButtonText,
            submitButtonClass,
            submitButtonIcon,
            onForgotLinkClick,
            formClassName,
            defaultEmail
        } = this.props;
        const errors = this.getAuthErrors(currentUser.errors);

        const formClass = classnames('form row auth-form', {
            'form--show-labels': showLabels,
            [formClassName]: formClassName
        });

        const emailClass = classnames({
            'form__input--has-error': !!errors.email
        });

        const passwordClass = classnames('margin-bottom-10', {
            'form__input--has-error': !!errors.password
        });

        const submitClass = classnames('button button--pill button--padded', {
            [submitButtonClass]: submitButtonClass
        });

        let blockedError;

        if (errors.blocked) {
            blockedError = (
                <div className="u-spacing-top-10 u-padding-x-25 u-lh-15">
                    {this.renderError(errors.blocked)}
                </div>
            );
        }

        return (
            <form
                className={formClass}
                action="/auth/login"
                onSubmit={onFormSubmit}
                method="post"
            >
                <div className="column small-24">
                    {showLabels && <label htmlFor="email">Email</label>}
                    <input
                        className={emailClass}
                        type="email"
                        name="email"
                        data-testid="email"
                        placeholder={!showLabels ? 'Email' : null}
                        defaultValue={defaultEmail ? defaultEmail : ''}
                        autoComplete="username"
                        autoCapitalize="none"
                        required
                    />
                    {this.renderError(errors.email)}
                </div>
                <div className="column small-24">
                    <PasswordInput
                        id="password"
                        showLabels={showLabels}
                        inputProps={{
                            required: true,
                            className: passwordClass,
                            defaultValue: this.props.defaultPassword
                        }}
                    />
                    {this.renderError(errors.password)}
                </div>
                <div className="column small-24 u-text-right">
                    <a
                        href="/forgot-password"
                        className="u-fs-12 u-text-orange"
                        onClick={onForgotLinkClick}
                        data-page="forgot"
                        data-testid="forgot-password"
                    >
                        Forgot your password?
                    </a>
                </div>
                <div className="column small-24 u-text-center">
                    <button
                        className={submitClass}
                        type="submit"
                        disabled={currentUser.loading}
                        data-testid="loginSubmit"
                    >
                        {currentUser.loading
                            ? 'Logging in...'
                            : submitButtonText}{' '}
                        {submitButtonIcon}
                    </button>
                    {blockedError}
                </div>
            </form>
        );
    }
}
