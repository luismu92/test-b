import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ForgotPassword extends Component {
    static propTypes = {
        onPasswordResetSubmit: PropTypes.func,
        onAuthPageClick: PropTypes.func,
        currentUser: PropTypes.object,
        defaultValue: PropTypes.string
    };

    renderPasswordError(errors) {
        if (errors.length === 0) {
            return false;
        }

        let pwError = '';

        if (errors[0].keyNotFound) {
            pwError =
                'User could not be found in our system, please check for typos or extra spaces';
        } else if (errors[0]) {
            pwError =
                'There was an issue sending the password email, please contact support@audiomack.com';
        } else {
            return false;
        }

        return <p className="auth__error">{pwError}</p>;
    }

    render() {
        const {
            currentUser,
            onPasswordResetSubmit,
            onAuthPageClick,
            defaultValue
        } = this.props;

        return (
            <div className="auth single-page u-padding-0">
                <div className="auth__top">
                    <h2 className="auth__title u-fs-18 u-spacing-bottom-10">
                        Forgot your password?
                    </h2>
                    <p className="u-spacing-bottom-20">
                        Enter your email below to receive instructions on
                        changing your password or{' '}
                        <a
                            href="/login"
                            onClick={onAuthPageClick}
                            data-page="login"
                        >
                            sign in
                        </a>
                        .
                    </p>
                    <form
                        action="/auth/forgot-password"
                        onSubmit={onPasswordResetSubmit}
                    >
                        <input
                            type="email"
                            name="email"
                            placeholder="Email"
                            autoCapitalize="none"
                            required
                            defaultValue={defaultValue ? defaultValue : ''}
                        />
                        {this.renderPasswordError(currentUser.errors)}
                        <input
                            className="button button--pill button--padded auth__button"
                            type="submit"
                            value={
                                currentUser.loading
                                    ? 'Submitting...'
                                    : 'Send reset instructions'
                            }
                            disabled={currentUser.loading}
                        />
                    </form>
                </div>
            </div>
        );
    }
}
