import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { logIn, dequeueAction } from '../redux/modules/user';

import LoginForm from './LoginForm';

class LoginFormContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onForgotLinkClick: PropTypes.func,
        currentUser: PropTypes.object,
        submitButtonIcon: PropTypes.object,
        submitButtonText: PropTypes.string,
        formClassName: PropTypes.string,
        submitButtonClass: PropTypes.string,
        showLabels: PropTypes.bool,
        onLoginSuccess: PropTypes.func,
        shouldCookieUser: PropTypes.bool,
        defaultEmail: PropTypes.string,
        defaultPassword: PropTypes.string
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, shouldCookieUser } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;
        const password = form.password.value;

        dispatch(logIn(email, password, shouldCookieUser))
            .then((action) => {
                this.loadSuccessfulLogin(e, action, email);
                return;
            })
            .catch((err) => console.log(err));
    };

    /////////////////////
    // Helpers methods //
    /////////////////////

    loadSuccessfulLogin(e, action, email) {
        const { currentUser, dispatch, onLoginSuccess } = this.props;

        if (action && typeof currentUser.queuedLoginAction === 'function') {
            currentUser.queuedLoginAction(action.resolved);
            dispatch(dequeueAction());
        }

        if (typeof onLoginSuccess === 'function') {
            onLoginSuccess(e, action, email);
        }
    }

    render() {
        return (
            <LoginForm
                onFormSubmit={this.handleFormSubmit}
                currentUser={this.props.currentUser}
                formClassName={this.props.formClassName}
                submitButtonText={this.props.submitButtonText}
                submitButtonIcon={this.props.submitButtonIcon}
                submitButtonClass={this.props.submitButtonClass}
                showLabels={this.props.showLabels}
                onForgotLinkClick={this.props.onForgotLinkClick}
                defaultEmail={this.props.defaultEmail}
                defaultPassword={this.props.defaultPassword}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(LoginFormContainer));
