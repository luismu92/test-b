import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { emailUnsubscribe } from '../redux/modules/user/index';
import EmailUnsubscribePage from './EmailUnsubscribePage';

class EmailUnsubscribePageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        match: PropTypes.object
    };

    componentDidMount() {
        console.log(this.props.currentUser);
        const { dispatch } = this.props;

        dispatch(emailUnsubscribe(this.props.match.params.hash));
    }

    render() {
        return (
            <EmailUnsubscribePage
                isUnsubsribing={this.props.currentUser.emailUnsubscribe.loading}
                error={this.props.currentUser.emailUnsubscribe.error}
                type={this.props.currentUser.emailUnsubscribe.type}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(EmailUnsubscribePageContainer);
