import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PasswordInput from '../components/PasswordInput';

export default class GhostClaimPage extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        email: PropTypes.string,
        artist: PropTypes.object,
        onClaimSubmit: PropTypes.func,
        claimError: PropTypes.object
    };

    renderError(error) {
        if (!error) {
            return null;
        }

        let text = error.message;
        const errorKeys = Object.keys(error.errors);

        if (errorKeys.length) {
            const firstError = error.errors[errorKeys[0]];

            if (firstError) {
                text = firstError[Object.keys(firstError)[0]];
            }
        }

        return <p className="u-text-red u-spacing-bottom-em">{text}</p>;
    }

    renderSigninForm() {
        const { onClaimSubmit, artist, id, email, claimError } = this.props;
        const passwordProps = {
            name: 'password',
            'data-testid': 'password',
            placeholder: 'Password',
            autoComplete: 'current-password',
            required: true
        };

        return (
            <form
                action={`${process.env.API_URL}/artist/${id}/claim`}
                method="PUT"
                onSubmit={onClaimSubmit}
                className="u-spacing-top-em"
            >
                <div hidden style={{ display: 'none' }}>
                    <input
                        type="email"
                        name="email"
                        tabIndex="-1"
                        value={email}
                        autoComplete="username"
                        autoCapitalize="none"
                        readOnly
                    />
                </div>
                <PasswordInput id="password" inputProps={passwordProps} />

                {this.renderError(claimError)}

                <div className="u-text-center">
                    <input
                        className="button button--pill"
                        type="submit"
                        data-testid="loginSubmit"
                        value={
                            artist.isClaimingArtist
                                ? 'Finalizing...'
                                : 'Claim account'
                        }
                        disabled={artist.isClaimingArtist}
                    />
                </div>
            </form>
        );
    }

    render() {
        return <div>{this.renderSigninForm()}</div>;
    }
}
