import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { createNewArtist } from '../redux/modules/monetization/associatedArtists';

import CreateNewArtistPage from './CreateNewArtistPage';

class CreateNewArtistPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onFormSubmit: PropTypes.func,
        associatedArtists: PropTypes.object
    };

    static defaultProps = {
        onFormSubmit() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            bio: '',
            twitterChecked: false,
            twitterHandle: ''
        };
    }

    handleInputChange = (e) => {
        const input = e.currentTarget;
        const inputName = input.name;
        let inputValue = input.value;

        switch (input.type) {
            case 'checkbox': {
                inputValue = input.checked;
                break;
            }

            default:
                break;
        }

        this.setState({
            [inputName]: inputValue
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const { name, ...options } = this.state;

        dispatch(createNewArtist(name, options))
            .then(() => {
                this.props.onFormSubmit(e);
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    render() {
        return (
            <CreateNewArtistPage
                associatedArtists={this.props.associatedArtists}
                onFormSubmit={this.handleFormSubmit}
                onInputChange={this.handleInputChange}
                name={this.state.name}
                bio={this.state.bio}
                twitterChecked={this.state.twitterChecked}
                twitterHandle={this.state.twitterHandle}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        associatedArtists: state.monetizationAssociatedArtists
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(CreateNewArtistPageContainer);
