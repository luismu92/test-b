import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
    register,
    registerWithCaptcha,
    dequeueAction
} from '../redux/modules/user';

import JoinForm from './JoinForm';

class JoinFormContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        submitButtonIcon: PropTypes.object,
        submitButtonText: PropTypes.string,
        submitButtonClass: PropTypes.string,
        showLabels: PropTypes.bool,
        onJoinSuccess: PropTypes.func,
        shouldCookieUser: PropTypes.bool,
        defaultEmail: PropTypes.string,
        defaultPassword: PropTypes.string
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, shouldCookieUser } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;
        const username = form.username.value;
        const password = form.password.value;
        const captcha = window.__captchaResponse;

        let actionObj;

        if (window.__UA__.isHeadless) {
            actionObj = register(
                email,
                username,
                password,
                password,
                shouldCookieUser
            );
        } else {
            actionObj = registerWithCaptcha(
                email,
                username,
                [password, password],
                captcha,
                shouldCookieUser
            );
        }

        dispatch(actionObj)
            .then((action) => {
                this.loadSuccessfulJoin(e, action, email);
                return;
            })
            .catch((err) => {
                if (window.grecaptcha) {
                    window.grecaptcha.reset();
                }
                console.log(err);
            });
        return;
    };

    /////////////////////
    // Helpers methods //
    /////////////////////

    loadSuccessfulJoin(e, action, email) {
        const { currentUser, dispatch, onJoinSuccess } = this.props;

        if (action && typeof currentUser.queuedLoginAction === 'function') {
            currentUser.queuedLoginAction(action.resolved);
            dispatch(dequeueAction());
        }

        if (typeof onJoinSuccess === 'function') {
            onJoinSuccess(e, action, email);
        }
    }

    render() {
        return (
            <JoinForm
                onFormSubmit={this.handleFormSubmit}
                currentUser={this.props.currentUser}
                submitButtonText={this.props.submitButtonText}
                submitButtonIcon={this.props.submitButtonIcon}
                submitButtonClass={this.props.submitButtonClass}
                showLabels={this.props.showLabels}
                defaultEmail={this.props.defaultEmail}
                defaultPassword={this.props.defaultPassword}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(JoinFormContainer));
