import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { parse } from 'query-string';
import classnames from 'classnames';

import AuthPageMeta from 'components/AuthPageMeta';
import analytics from 'utils/analytics';
import { loadScript, loadFacebookSdk, loadSignInWithApple } from 'utils/index';
import { AUTH_PROVIDER } from 'constants/index';

import {
    showModal,
    hideModal,
    MODAL_TYPE_CONTACT,
    MODAL_TYPE_AUTH
} from '../redux/modules/modal';
import {
    logIn,
    identityCheck,
    logInFacebook,
    logInGoogle,
    logInTwitter,
    logInInstagram,
    logInApple,
    getTwitterRequestToken,
    dequeueAction,
    forgotpw,
    clearErrors
} from '../redux/modules/user';

import { addToast } from '../redux/modules/toastNotification';
import ForgotPassword from './ForgotPassword';
import Login from './Login';
import Join from './Join';
import Demographics from './Demographics';

import AmMark from '../icons/am-mark';
import ArrowIcon from 'icons/arrow-next';

class AuthPageContainer extends Component {
    static propTypes = {
        activePage: PropTypes.string,
        history: PropTypes.object,
        location: PropTypes.object,
        currentUser: PropTypes.object,
        route: PropTypes.object,
        dispatch: PropTypes.func,
        inModal: PropTypes.bool,
        onPasswordResetSubmit: PropTypes.func,
        onSocialAuth: PropTypes.func,
        onFormSubmit: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            socialLoginError: false,
            loginError: false,
            lastSocialLogin: null,
            twitterToken: null,
            twitterSecret: null,
            emailStatus: null,
            userEmail: null,
            userPassword: null,
            collectDemographicInfo: false,
            queuedRedirect: null,
            queuedToast: null
        };

        const shoudldRedirect = props.currentUser.isLoggedIn;

        if (shoudldRedirect) {
            props.history.replace({
                pathname: '/'
            });
        }
    }

    //////////////////////
    // Lifecycle methods //
    //////////////////////

    componentDidMount() {
        this._isMounted = true;

        Promise.all([
            // These 2 things need to be loaded before we allow a user to click
            // log in with google/twitter
            loadScript(
                'https://apis.google.com/js/platform.js',
                'google-auth'
            ).then(() => {
                window.gapi.load('auth2', () => {
                    window.gapi.auth2.init({
                        client_id: process.env.GOOGLE_AUTH_ID
                    });
                });
                return;
            }),

            this.setTwitterToken()
        ])
            .catch((err) => {
                console.error(err);
            })
            .finally(() => {
                this.hideLoader();
            });
    }

    componentWillUnmount() {
        this._isMounted = false;

        this.setState({
            userPassword: null
        });
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoginSuccess = () => {
        const { history } = this.props;
        const redirectTo = parse(window.location.search).redirectTo || '';

        let goTo = decodeURIComponent(redirectTo) || '/';

        if (goTo.match(/^\/login|\/join/)) {
            goTo = '/';
        }

        history.replace(goTo);

        if (typeof this.props.onFormSubmit === 'function') {
            this.props.onFormSubmit();
        }
    };

    handleJoinSuccess = (e, action, email) => {
        const redirectTo = parse(window.location.search).redirectTo || '';
        let goTo = decodeURIComponent(redirectTo) || '/';

        if (goTo.match(/^\/login|\/join/)) {
            goTo = `/artist/${action.resolved.user.url_slug}`;
        }

        this.setState({
            collectDemographicInfo: true,
            queuedRedirect: goTo,
            queuedToast: {
                text: `An email verification link has been sent to ${email}`,
                type: 'info'
            }
        });
    };

    handleDemographicsSuccess = () => {
        const { history } = this.props;
        const { queuedRedirect, queuedToast } = this.state;

        history.replace(queuedRedirect);

        if (typeof this.props.onFormSubmit === 'function') {
            this.props.onFormSubmit();
        }

        if (queuedToast) {
            this.showToast(queuedToast.text, queuedToast.type);
        }
    };

    handleSocialAuth = (e) => {
        const provider = e.currentTarget.getAttribute('data-provider');

        let promise;

        switch (provider) {
            case AUTH_PROVIDER.FACEBOOK: {
                promise = this.doFacebookLogin();
                break;
            }

            case AUTH_PROVIDER.TWITTER: {
                promise = this.doTwitterLogin();
                break;
            }

            case AUTH_PROVIDER.GOOGLE: {
                promise = this.doGoogleLogin();
                break;
            }

            case AUTH_PROVIDER.INSTAGRAM: {
                promise = this.doInstagramLogin();
                break;
            }

            case AUTH_PROVIDER.APPLE: {
                promise = this.doAppleLogin();
                break;
            }

            default:
                return;
        }

        promise
            .then(() => {
                if (
                    typeof this.props.onSocialAuth === 'function' &&
                    !this.state.socialLoginError &&
                    !this.state.loginError
                ) {
                    this.props.onSocialAuth(provider);
                }
                return;
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            })
            .finally(() => {
                this.hideLoader();
            });
    };

    // Check if provided email do no exist in DB
    handleSocialAuthEmailValidation = (e) => {
        e.preventDefault();
        const { dispatch } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;

        dispatch(identityCheck(email, 'email'))
            .then((data) => {
                const isTaken = data.resolved.data.email.taken;

                if (email.length && isTaken) {
                    this.setState({
                        emailStatus: 'notTaken',
                        userEmail: email
                    });
                } else {
                    // If the email is valid, do the social login again with the last token
                    this.handleSocialAuthWithEmail(email);
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleSocialAuthWithEmail = (email) => {
        const { lastSocialLogin } = this.state;
        let promise;

        switch (lastSocialLogin) {
            case 'facebook': {
                promise = this.doFacebookLogin(email);
                break;
            }

            case 'twitter': {
                promise = this.doTwitterLogin(email);
                break;
            }

            default:
                return;
        }

        promise
            .then(() => {
                if (
                    typeof this.props.onSocialAuth === 'function' &&
                    !this.state.loginError
                ) {
                    this.props.onSocialAuth();
                }
                return;
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            })
            .finally(() => {
                this.hideLoader();
            });
    };

    handleAuthPageClick = (e) => {
        e.preventDefault();

        const link = e.currentTarget;
        const { dispatch, inModal, history } = this.props;
        const page = link.getAttribute('data-page');
        const goToUrl = `${link.pathname}${window.location.search}`;

        // If going back to login page, erase whatever password we had
        // fill/pre-filled in
        if (page === 'login') {
            this.setState({
                userPassword: ''
            });
        }

        dispatch(clearErrors());

        if (!inModal) {
            history.replace(goToUrl, {
                userEmail: this.state.userEmail
            });
            return;
        }

        this.setState({
            emailStatus: null
        });

        if (page === 'contact') {
            dispatch(
                showModal(MODAL_TYPE_CONTACT, {
                    page: page
                })
            );
            return;
        }

        if (page) {
            dispatch(showModal(MODAL_TYPE_AUTH, { type: page }));
        }

        // Using window.history instead of props.history because we're
        // manipulating the url outside of react-router so the underlying
        // content does not change
        window.history.replaceState(null, null, goToUrl);
    };

    handleBackToLogin = () => {
        this.setState({
            emailStatus: null,
            userEmail: null
        });
    };

    handlePasswordResetSubmit = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;

        dispatch(forgotpw(email))
            .then(() => {
                this.showToast(
                    `Password reset instructions have been sent to ${email}`,
                    'info'
                );

                if (typeof this.props.onPasswordResetSubmit === 'function') {
                    this.props.onPasswordResetSubmit(e);
                }
                return this.props.history.push('/');
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleEmailSubmit = (e) => {
        e.preventDefault();

        const { dispatch, currentUser } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;
        const password = form.password.value;
        const isHiddenPassword =
            form.password.getAttribute('data-type') === 'password-manager';

        this.setState({
            emailStatus: null
        });

        // If we have both values, we're probably using a password
        // manager in which case we can just submit everything.
        if (
            !!email &&
            !!password &&
            !isHiddenPassword &&
            !currentUser.loading
        ) {
            dispatch(logIn(email, password))
                .then(() => {
                    this.handleLoginSuccess();
                    dispatch(hideModal());
                    return;
                })
                .catch((err) => {
                    console.error(err);
                });
            return;
        }

        this.checkIfEmailExists(email);
    };

    // If a user for example, wants to cancel the auto sign-in flow,
    // the browser may have already filled in the email/password input
    // which includes the hidden password input that exists for this
    // exact use case (password managers). In this case, if they change
    // the email input, we can assume the password will be entered later
    // as well.
    handleEmailInput = () => {
        if (this.state.userPassword) {
            this.setState({
                userPassword: ''
            });
        }
    };

    handleHiddenPasswordInput = (e) => {
        const { dispatch, currentUser } = this.props;
        const form = e.currentTarget.form;
        const email = form.email.value;
        const password = form.password.value;

        if (!email || !password || currentUser.loading) {
            return;
        }

        this.setState({
            userPassword: password
        });

        // If we have both values, we're probably using a password
        // manager in which case we can just submit everything.
        dispatch(logIn(email, password))
            .then(() => {
                this.handleLoginSuccess();
                dispatch(hideModal());
                return;
            })
            .catch((err) => {
                this.setState({
                    userPassword: ''
                });

                console.error(err);

                if (
                    window.navigator.credentials &&
                    window.navigator.credentials.preventSilentAccess
                ) {
                    // Turn on the mediation mode so auto sign-in won't happen
                    // until next time user intended to do so.
                    window.navigator.credentials.preventSilentAccess();
                }
                if (
                    window.navigator.credentials &&
                    window.navigator.credentials.requireUserMediation
                ) {
                    // Turn on the mediation mode so auto sign-in won't happen
                    // until next time user intended to do so.
                    window.navigator.credentials.requireUserMediation();
                }
                this.checkIfEmailExists(email);
            });
    };

    /////////////////////
    // Helpers methods //
    /////////////////////

    setTwitterToken() {
        return this.props.dispatch(getTwitterRequestToken()).then((result) => {
            this.setState({
                twitterToken: result.resolved.token,
                twitterSecret: result.resolved.secret
            });
            return;
        });
    }

    showLoader() {
        if (!this._isMounted) {
            return;
        }

        this.setState({ loading: true });
    }

    hideLoader() {
        if (!this._isMounted) {
            return;
        }

        this.setState({ loading: false });
    }

    doFacebookLogin = (email) => {
        const { dispatch } = this.props;
        const userEmail = email ? email : null;

        this.showLoader();

        return loadFacebookSdk().then(({ login }) => {
            const fbToken = login.authResponse.accessToken;
            const fbUserId = login.authResponse.userID;
            // Idk what this is but it doesnt seem to be used in the API
            const fbUsername = 'username';

            return dispatch(
                logInFacebook(fbToken, fbUserId, fbUsername, userEmail)
            )
                .then((action) => {
                    return this.finishLogin(action);
                })
                .catch(() => {
                    const {
                        currentUser: { errors }
                    } = this.props;

                    // Error code for empty email in social token
                    if (errors.length > 0 && errors[0].errorcode === 1052) {
                        this.setState({
                            lastSocialLogin: 'facebook',
                            socialLoginError: true
                        });
                    }

                    // To not close the modal when any of these errors occur
                    if (
                        errors.length > 0 &&
                        (errors[0].errorcode === 1056 ||
                            errors[0].errorcode === 1057)
                    ) {
                        this.setState({
                            loginError: true
                        });
                    }
                });
        });
    };

    doInstagramLogin = () => {
        const { dispatch } = this.props;

        return dispatch(logInInstagram());
    };

    // https://developer.twitter.com/en/docs/twitter-for-websites/log-in-with-twitter/guides/implementing-sign-in-with-twitter
    doTwitterLogin = (email) => {
        const { dispatch } = this.props;
        const { twitterToken, twitterSecret } = this.state;
        const userEmail = email ? email : null;

        this.showLoader();

        return dispatch(logInTwitter(twitterToken, twitterSecret, userEmail))
            .then((action) => {
                this.finishLogin(action);
                return;
            })
            .catch((error) => {
                const {
                    currentUser: { errors }
                } = this.props;

                if (errors.length > 0) {
                    if (errors[0] === 'Popup closed') {
                        throw error;
                    }

                    // Error code for empty email in social token
                    if (errors[0].errorcode === 1052) {
                        this.setState({
                            lastSocialLogin: 'twitter',
                            socialLoginError: true
                        });
                        // Get new twitter tokens
                        this.setTwitterToken();
                    }

                    // To not close the modal when any of these errors occur
                    if (
                        errors[0].errorcode === 1056 ||
                        errors[0].errorcode === 1057
                    ) {
                        this.setState({
                            loginError: true
                        });
                    }
                }
            });
    };

    initGoogleShit(email) {
        // Using our own promise here for .finally() support
        return new Promise((resolve, reject) => {
            const auth2 = window.gapi.auth2.getAuthInstance();

            window.gapi.load('client:auth2', auth2);

            if (auth2.isSignedIn.get()) {
                // Check if currently signed in user is the same as intended.
                const googleUser = auth2.currentUser.get();

                if (googleUser.getBasicProfile().getEmail() === email) {
                    resolve(googleUser);
                    return;
                }
            }

            // If the user is not signed in with expected account, let sign in.
            auth2
                .signIn({
                    // Set `login_hint` to specify an intended user account,
                    // otherwise user selection dialog will popup.
                    login_hint: email || ''
                })
                .then((googleUser) => {
                    // Now user is successfully authenticated with Google.
                    // Send ID Token to the server to authenticate with our server.
                    // const form = new FormData();
                    const token = googleUser.getAuthResponse().id_token;
                    const accessToken = googleUser.getAuthResponse(true)
                        .access_token;

                    return this.props
                        .dispatch(logInGoogle(token, accessToken))
                        .then((action) => {
                            this.finishLogin(action);
                            resolve();
                            return;
                        });
                })
                .catch(reject);
        });
    }

    // https://github.com/GoogleChromeLabs/credential-management-sample/blob/master/static/scripts/app.js
    doGoogleLogin(preferredEmail) {
        this.showLoader();

        return this.initGoogleShit(preferredEmail);
    }

    doAppleLogin() {
        this.showLoader();

        return loadSignInWithApple()
            .then(() => window.AppleID.auth.signIn())
            .then((data) => {
                return this.props
                    .dispatch(logInApple(data.authorization.id_token))
                    .then((action) => {
                        this.finishLogin(action);
                        return;
                    });
            });
    }

    showToast(message, messageType) {
        const { dispatch } = this.props;

        dispatch(
            addToast({
                type: messageType,
                message: message
            })
        );
    }

    finishLogin(action) {
        const { dispatch, history, currentUser } = this.props;

        dispatch(hideModal());

        history.replace(`/artist/${action.resolved.user.url_slug}`);
        if (currentUser.queuedLoginAction) {
            currentUser.queuedLoginAction(action.resolved);
            dispatch(dequeueAction());
        }
    }

    checkIfEmailExists(email) {
        const { dispatch } = this.props;
        dispatch(identityCheck(email, 'email'))
            .then((data) => {
                const isTaken = data.resolved.data.email.taken;

                if (email.length && isTaken) {
                    this.setState({
                        emailStatus: 'notTaken',
                        userEmail: email
                    });
                } else {
                    this.setState({
                        emailStatus: 'taken',
                        userEmail: email
                    });
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        const {
            activePage,
            route,
            currentUser,
            location,
            inModal
        } = this.props;

        const {
            loading,
            emailStatus,
            socialLoginError,
            collectDemographicInfo
        } = this.state;

        const page = activePage || route.activePage;

        let headMessage = 'Sign in to Audiomack';
        let content;

        switch (page) {
            case 'login':
                content = (
                    <Login
                        location={location}
                        socialEmailError={socialLoginError}
                        onSocialAuthWithEmail={
                            this.handleSocialAuthEmailValidation
                        }
                        onEmailSubmit={this.handleEmailSubmit}
                        onHiddenPasswordInput={this.handleHiddenPasswordInput}
                        onLoginSuccess={this.handleLoginSuccess}
                        onSocialAuth={this.handleSocialAuth}
                        onAuthPageClick={this.handleAuthPageClick}
                        onEmailInput={this.handleEmailInput}
                        currentUser={currentUser}
                        loading={loading}
                        inModal={inModal}
                        emailStatus={this.state.emailStatus}
                        userEmail={this.state.userEmail}
                        userPassword={this.state.userPassword}
                    />
                );
                break;

            case 'join':
                if (collectDemographicInfo) {
                    headMessage = 'Tell us about yourself.';
                    content = (
                        <Demographics
                            onSubmitSuccess={this.handleDemographicsSuccess}
                            currentUser={currentUser}
                            loading={loading}
                        />
                    );
                    break;
                }

                headMessage =
                    'Looks like you haven’t signed up yet. Register Today!';
                content = (
                    <Join
                        location={location}
                        onAuthPageClick={this.handleAuthPageClick}
                        onJoinSuccess={this.handleJoinSuccess}
                        onSocialAuth={this.handleSocialAuth}
                        onLoginLinkClick={this.handleAuthPageClick}
                        loading={loading}
                        inModal={inModal}
                        defaultEmail={
                            (location.state || {}).userEmail ||
                            this.state.userEmail
                        }
                        defaultPassword={this.state.userPassword}
                    />
                );
                break;

            case 'forgot':
                content = (
                    <ForgotPassword
                        onAuthPageClick={this.handleAuthPageClick}
                        onPasswordResetSubmit={this.handlePasswordResetSubmit}
                        currentUser={currentUser}
                        defaultValue={this.state.userEmail}
                    />
                );
                break;

            default: {
                const error = new Error(`No auth page found for ${page}`);

                analytics.error(error);
                console.log(error);
                return <div />;
            }
        }

        const containerClass = classnames(
            'auth u-margin-0 u-padding-0 column small-24',
            {
                'auth--page': !inModal
            }
        );

        let backArrow;

        if (inModal && page !== 'login') {
            backArrow = (
                <Link
                    to="/login"
                    className="auth-back"
                    onClick={this.handleAuthPageClick}
                    data-page="login"
                >
                    <ArrowIcon />
                </Link>
            );
        }

        if (
            inModal &&
            page === 'login' &&
            emailStatus === 'notTaken' &&
            !socialLoginError
        ) {
            backArrow = (
                <button className="auth-back" onClick={this.handleBackToLogin}>
                    <ArrowIcon />
                </button>
            );
        }

        let footerLink = (
            <p className="u-fw-600 u-ls-n-05">
                <Link
                    to="/forgot-password"
                    onClick={this.handleAuthPageClick}
                    data-page="forgot"
                >
                    Having trouble signing in?
                </Link>
            </p>
        );

        if (page === 'forgot') {
            footerLink = (
                <p className="u-fw-600 u-ls-n-05">
                    <a
                        href="https://audiomack.zendesk.com/hc/en-us/articles/360039143492"
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        Having trouble logging in?
                    </a>
                </p>
            );
        }

        if (page === 'join') {
            footerLink = (
                <p className="u-fw-600 u-ls-n-05">
                    Having trouble signing up?{' '}
                    <Link
                        to="/contact-us"
                        onClick={this.handleAuthPageClick}
                        data-page="contact"
                    >
                        Contact us
                    </Link>
                </p>
            );
        }

        return (
            <Fragment>
                <AuthPageMeta page={page} />
                {backArrow}
                <div className="auth row">
                    <div className="auth-head column small-24 u-text-center u-padding-top-25 u-padding-bottom-30">
                        <div className="auth-head__logo u-spacing-bottom-10">
                            <AmMark />
                        </div>
                        <h2 className="auth-head__title u-fs-13 u-fw-700 u-lh-15 u-ls-n-07">
                            {headMessage}
                        </h2>
                    </div>
                    <div className={containerClass}>{content}</div>
                    <div className="auth-footer u-text-center u-padding-y-20 column small-24">
                        {!socialLoginError && footerLink}
                    </div>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(AuthPageContainer));
