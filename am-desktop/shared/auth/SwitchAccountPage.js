import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import AmLogo from '../icons/am-mark';
import PlusIcon from '../icons/plus';
import TrashIcon from '../icons/trash';
import CheckIcon from '../icons/check-mark';
import AndroidLoader from '../loaders/AndroidLoader';

export default class SwitchAccountPage extends Component {
    static propTypes = {
        accounts: PropTypes.array,
        loading: PropTypes.bool,
        hasSuccessfulSignin: PropTypes.bool,
        currentUser: PropTypes.object,
        signinFormAccount: PropTypes.object,
        onAccountAddClick: PropTypes.func,
        onAccountClick: PropTypes.func,
        onAccountRemove: PropTypes.func,
        onSigninCancel: PropTypes.func,
        onSigninSubmit: PropTypes.func,
        signinFormActive: PropTypes.bool
    };

    getAuthErrors(errors) {
        return errors.reduce(
            (acc, error) => {
                // Seems like this error code doesnt change when getting different
                // errors
                if (error.errorcode === 1037) {
                    acc.facebook =
                        'Cannot retrieve necessary details from Facebook. Please make sure you have an email address set in your Facebook account and try again.';
                } else if (
                    error.description.toLowerCase().indexOf('email') !== -1
                ) {
                    acc.email = error.description;
                } else if (
                    error.description.toLowerCase().indexOf('password') !== -1
                ) {
                    acc.password = error.description;
                }
                // }

                return acc;
            },
            {
                email: '',
                password: '',
                facebook: ''
            }
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="auth__error">{error}</p>;
    }

    renderList(accounts = []) {
        const items = accounts.map((account, i) => {
            const isCurrentUser =
                this.props.currentUser.profile.id === account.id;
            let status;

            if (!account.active) {
                status = <p className="switch-account__status">Logged out</p>;
            }

            let icon;

            if (isCurrentUser) {
                icon = (
                    <span className="switch-account__icon switch-account__icon--check u-orange-check">
                        <CheckIcon title="Verified artist" />
                    </span>
                );
            } else {
                icon = (
                    <button
                        data-tooltip="Remove account"
                        className="switch-account__icon switch-account__icon--remove"
                        data-id={account.id}
                        onClick={this.props.onAccountRemove}
                    >
                        <TrashIcon />
                    </button>
                );
            }

            return (
                <li key={i}>
                    <button
                        className="switch-account__account"
                        data-id={account.id}
                        onClick={this.props.onAccountClick}
                        disabled={isCurrentUser}
                    >
                        <div className="u-pos-relative">
                            <img src={account.image} alt="" />
                        </div>

                        <div className="switch-account__account-name u-text-left">
                            <p>{account.name}</p>
                            {status}
                        </div>
                    </button>
                    {icon}
                </li>
            );
        });

        return <ul className="switch-account__list">{items}</ul>;
    }

    renderSigninForm() {
        const {
            currentUser,
            onSigninSubmit,
            signinFormAccount,
            onSigninCancel,
            hasSuccessfulSignin
        } = this.props;

        if (hasSuccessfulSignin) {
            return null;
        }

        const errors = this.getAuthErrors(currentUser.errors);
        const emailClass = classnames({
            'form__input--has-error': !!errors.email
        });
        const passwordClass = classnames({
            'form__input--has-error': !!errors.password
        });

        let user;

        if (signinFormAccount) {
            user = (
                <ul className="switch-account__list">
                    <li className="switch-account__account switch-account__account--center">
                        <img src={signinFormAccount.image} alt="" />{' '}
                        {signinFormAccount.name}
                    </li>
                </ul>
            );
        }

        return (
            <form
                action="/auth/login"
                onSubmit={onSigninSubmit}
                method="post"
                className="u-spacing-top-em"
            >
                {user}
                <input
                    className={emailClass}
                    type="email"
                    name="email"
                    data-testid="email"
                    placeholder="Email"
                    autoCapitalize="none"
                    autoComplete="username"
                    required
                />
                {errors.email && this.renderError(errors.email)}

                <input
                    className={passwordClass}
                    type="password"
                    name="password"
                    data-testid="password"
                    placeholder="Password"
                    autoComplete="current-password"
                    autoCapitalize="none"
                    required
                />
                {errors.password && this.renderError(errors.password)}

                <div className="u-text-center">
                    <input
                        className="button button--pill"
                        type="submit"
                        data-testid="loginSubmit"
                        value={
                            currentUser.loading ? 'Logging in...' : 'Sign in'
                        }
                        disabled={currentUser.loading}
                    />
                    <button
                        className="button button--pill button--outline"
                        onClick={onSigninCancel}
                        type="button"
                    >
                        Cancel
                    </button>
                </div>
            </form>
        );
    }

    render() {
        const {
            loading,
            accounts,
            onAccountAddClick,
            signinFormActive,
            hasSuccessfulSignin
        } = this.props;
        let loader;

        if (loading || hasSuccessfulSignin) {
            loader = (
                <div className="u-text-center u-spacing-top-em">
                    <AndroidLoader />
                </div>
            );
        }

        let content;

        if (signinFormActive) {
            content = this.renderSigninForm();
        } else {
            content = (
                <Fragment>
                    {this.renderList(accounts)}
                    <div className="u-text-center u-spacing-top">
                        <button
                            className="button button--pill button--add"
                            onClick={onAccountAddClick}
                        >
                            <span className="button__icon button__icon--plus">
                                <PlusIcon className="u-d-block" />
                            </span>
                            Add Account
                        </button>
                    </div>
                </Fragment>
            );
        }

        return (
            <div className="switch-account u-box-shadow u-padded">
                <AmLogo className="switch-account__logo" />
                <p className="switch-account__heading u-text-center">
                    Switch to another account
                </p>
                {loader}
                {content}
            </div>
        );
    }
}
