import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import JoinFormContainer from './JoinFormContainer';
import AndroidLoader from '../loaders/AndroidLoader';

export default class Join extends Component {
    static propTypes = {
        onJoinSuccess: PropTypes.func,
        loading: PropTypes.bool,
        defaultEmail: PropTypes.string,
        defaultPassword: PropTypes.string
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const { onJoinSuccess, loading } = this.props;

        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <Fragment>
                {loader}
                <JoinFormContainer
                    submitButtonClass="auth__submit auth-submit-button u-spacing-top-20"
                    onJoinSuccess={onJoinSuccess}
                    showLabels
                    defaultEmail={this.props.defaultEmail}
                    defaultPassword={this.props.defaultPassword}
                />
            </Fragment>
        );
    }
}
