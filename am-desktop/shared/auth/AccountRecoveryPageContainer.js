import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import {
    verifyForgotPasswordToken,
    recoverAccount
} from '../redux/modules/user/index';
import { showModal, MODAL_TYPE_AUTH } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import { logOut } from '../redux/modules/user/index';
import AccountRecoveryPage from './AccountRecoveryPage';

class AccountRecoveryPageContainer extends Component {
    static propTypes = {
        token: PropTypes.string.isRequired,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            isVerifyingToken: false,
            isValidToken: false,
            isRecoveringAccount: false,
            recoveryError: null
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const { history } = this.props;

        history.replace(window.location.pathname);

        this.verifyToken();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleForgotPasswordClick = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_AUTH, { type: 'forgot' }));
    };

    handleFormSubmit = (e) => {
        const { dispatch, token } = this.props;
        const password = e.currentTarget.password.value;

        e.preventDefault();

        this.setState({
            isRecoveringAccount: true
        });

        dispatch(recoverAccount(token, password))
            .then(() => {
                this.setState({
                    isRecoveringAccount: false
                });

                dispatch(showModal(MODAL_TYPE_AUTH, { type: 'login' }));

                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            'Password successfully updated. Please login with your new password.'
                    })
                );
                return;
            })
            .catch((err) => {
                this.setState({
                    isRecoveringAccount: false,
                    recoveryError: err.message
                });
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    verifyToken() {
        const { currentUser, dispatch, token } = this.props;

        if (!token) {
            return;
        }

        this.setState({
            isVerifyingToken: true
        });

        dispatch(verifyForgotPasswordToken(token))
            .then(() => {
                this.setState({
                    isVerifyingToken: false,
                    isValidToken: true
                });

                if (currentUser.isLoggedIn) {
                    dispatch(logOut());
                }
                return;
            })
            .catch(() => {
                this.setState({
                    isVerifyingToken: false
                });
            });
    }

    render() {
        return (
            <AccountRecoveryPage
                token={this.props.token}
                isVerifyingToken={this.state.isVerifyingToken}
                isValidToken={this.state.isValidToken}
                isRecoveringAccount={this.state.isRecoveringAccount}
                recoveryError={this.state.recoveryError}
                onForgotPasswordClick={this.handleForgotPasswordClick}
                onFormSubmit={this.handleFormSubmit}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(AccountRecoveryPageContainer);
