import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import DemographicsForm from './DemographicsForm';

import { saveUserDetails } from '../redux/modules/user';

class DemographicsFormContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        submitButtonText: PropTypes.string,
        submitButtonClass: PropTypes.string,
        showLabels: PropTypes.bool,
        onSubmitSuccess: PropTypes.func,
        shouldCookieUser: PropTypes.bool
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, onSubmitSuccess } = this.props;
        const form = e.currentTarget;
        const birthday = form.birthdayFormatted.value;
        const gender = form.gender.value;

        const actionObj = saveUserDetails({
            gender,
            birthday
        });

        dispatch(actionObj)
            .then((action) => {
                if (typeof onSubmitSuccess === 'function') {
                    onSubmitSuccess(e, action);
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });

        return;
    };

    render() {
        return (
            <DemographicsForm
                onFormSubmit={this.handleFormSubmit}
                submitButtonText={this.props.submitButtonText}
                submitButtonClass={this.props.submitButtonClass}
                showLabels={this.props.showLabels}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(DemographicsFormContainer));
