import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class CreateNewArtistPage extends Component {
    static propTypes = {
        associatedArtists: PropTypes.object,
        onFormSubmit: PropTypes.func,
        onInputChange: PropTypes.func,
        name: PropTypes.string,
        bio: PropTypes.string,
        twitterChecked: PropTypes.bool,
        twitterHandle: PropTypes.string
    };

    renderError(error) {
        if (!error) {
            return null;
        }

        let text = error.message;
        const errorKeys = Object.keys(error.errors);

        if (errorKeys.length) {
            const firstError = error.errors[errorKeys[0]];

            if (firstError) {
                text = firstError[Object.keys(firstError)[0]];
            }
        }

        return <p className="u-text-red u-spacing-bottom-em">{text}</p>;
    }

    renderSigninForm() {
        const {
            onFormSubmit,
            onInputChange,
            associatedArtists,
            name,
            bio,
            twitterChecked,
            twitterHandle
        } = this.props;
        const placeholderName = name.trim() || 'Big John';

        let twitterImport;

        if (twitterChecked) {
            twitterImport = (
                <label>
                    Twitter Handle:
                    <input
                        type="text"
                        name="twitterHandle"
                        value={twitterHandle}
                        onChange={onInputChange}
                        placeholder={`@${placeholderName
                            .toLowerCase()
                            .replace(/\s+/g, '')}`}
                        required
                    />
                </label>
            );
        }

        return (
            <form
                action={`${process.env.API_URL}/artist`}
                method="POST"
                onSubmit={onFormSubmit}
                className="u-spacing-top-em"
            >
                <label>
                    Artist Name:
                    <input
                        type="text"
                        name="name"
                        value={name}
                        onChange={onInputChange}
                        placeholder={placeholderName}
                        required
                    />
                </label>

                <label>
                    Bio:
                    <textarea
                        name="bio"
                        value={bio}
                        onChange={onInputChange}
                        disabled={twitterChecked}
                        placeholder={`${placeholderName} is originally from Georgia. His original name was...`}
                    />
                </label>

                <label>
                    <input
                        type="checkbox"
                        name="twitterChecked"
                        checked={twitterChecked}
                        onChange={onInputChange}
                    />
                    Import from Twitter
                </label>
                {twitterImport}

                {this.renderError(associatedArtists.createAccountError)}

                <div className="u-text-center">
                    <input
                        className="button button--pill"
                        type="submit"
                        data-testid="loginSubmit"
                        value={
                            associatedArtists.isCreatingAccount
                                ? 'Creating...'
                                : 'Create Account'
                        }
                        disabled={associatedArtists.isCreatingAccount}
                    />
                </div>
            </form>
        );
    }

    render() {
        return <div>{this.renderSigninForm()}</div>;
    }
}
