import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { AUTH_PROVIDER } from 'constants/index';

import GoogleLogo from 'icons/google-logo-color';
import TwitterLogo from 'icons/twitter-logo-new';
import FacebookLogo from 'icons/facebook-letter-logo';
import AppleLogo from 'icons/apple-logo';

export default class AuthButtons extends Component {
    static propTypes = {
        join: PropTypes.bool,
        containerClass: PropTypes.string,
        buttonClass: PropTypes.string,
        onSocialAuth: PropTypes.func,
        loading: PropTypes.bool
    };

    render() {
        const { buttonClass, containerClass, loading } = this.props;

        let label = 'Continue with ';

        if (this.props.join) {
            label = 'Sign up with ';
        }

        const providers = [
            {
                network: 'google',
                provider: AUTH_PROVIDER.GOOGLE,
                icon: <GoogleLogo className="u-no-fill" />
            },
            {
                network: 'twitter',
                provider: AUTH_PROVIDER.TWITTER,
                icon: <TwitterLogo />
            },
            {
                network: 'facebook',
                provider: AUTH_PROVIDER.FACEBOOK,
                icon: <FacebookLogo />
            },
            {
                network: 'apple',
                provider: AUTH_PROVIDER.APPLE,
                icon: <AppleLogo />
            }
        ];

        const buttons = providers.map((button, i) => {
            const buttonKlass = classnames('u-spacing-bottom-15', {
                [buttonClass]: buttonClass
            });

            return (
                <div
                    className={buttonKlass}
                    key={i}
                    style={{ pointerEvents: loading ? 'none' : 'auto' }}
                >
                    <button
                        className={`auth__button auth__button--${
                            button.network
                        }`}
                        data-provider={button.provider}
                        onClick={this.props.onSocialAuth}
                        disabled={loading}
                    >
                        <span className="auth__button-icon">{button.icon}</span>
                        <span className="auth__button-text u-fs-13 u-tt-uppercase u-fw-700">
                            {label} {button.network}
                        </span>
                    </button>
                </div>
            );
        });

        const containerKlass = classnames('auth__buttons u-spacing-bottom-25', {
            'auth__buttons--join': this.props.join,
            [containerClass]: this.props.containerClass
        });

        return <div className={containerKlass}>{buttons}</div>;
    }
}
