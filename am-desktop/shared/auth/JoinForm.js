import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Recaptcha from 'react-google-recaptcha/lib/recaptcha';

import { loadScript } from 'utils/index';
import analytics from 'utils/analytics';

import PasswordInput from '../components/PasswordInput';

export default class JoinForm extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        onFormSubmit: PropTypes.func,
        showLabels: PropTypes.bool,
        submitButtonText: PropTypes.string,
        submitButtonIcon: PropTypes.object,
        submitButtonClass: PropTypes.string,
        defaultEmail: PropTypes.string,
        defaultPassword: PropTypes.string
    };

    static defaultProps = {
        showLabels: false,
        submitButtonText: 'Sign Up'
    };

    constructor(props) {
        super(props);

        this.state = {
            captchaLoaded: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        if (!window.__UA__.isHeadless) {
            loadScript(
                'https://www.google.com/recaptcha/api.js',
                'recaptcha-api'
            )
                .then(() => {
                    this.getCaptchaReference();
                    return;
                })
                .catch((err) => {
                    console.log(err);
                    analytics.error(err);
                });
        }
    }

    componentWillUnmount() {
        clearTimeout(this._timer);
        this._timer = null;
        this._hackedAroundChromeAutofill = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCaptchaChange = (response) => {
        window.__captchaResponse = response;
    };

    // Hack around browser autofill where it erroneously fills
    // this field out as the email
    handleUsernameChange = (e) => {
        if (this._hackedAroundChromeAutofill) {
            return;
        }

        const input = e.currentTarget;

        if (input.value.indexOf('@') !== -1) {
            input.value = '';
        }

        // Stupid hack here but right now but if a user starts typing
        // let's cancel out any attempt to erase the input with the logic
        // above
        if (input.value.length === 1) {
            this._hackedAroundChromeAutofill = true;
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getCaptchaReference() {
        this._timer = setTimeout(() => {
            if (!window.grecaptcha) {
                this.getCaptchaReference();
                return;
            }

            this._grecaptcha = window.grecaptcha;
            this.setState({
                captchaLoaded: true
            });
        }, 1000);
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="auth__error">{error.description}</p>;
    }

    render() {
        const {
            onFormSubmit,
            currentUser,
            showLabels,
            submitButtonText,
            submitButtonClass,
            submitButtonIcon,
            defaultEmail,
            defaultPassword
        } = this.props;
        const errors = currentUser.errors;

        const errorObj =
            errors && errors[0] && errors[0].errors ? errors[0].errors : {};
        const messages = Object.keys(errorObj).reduce((obj, key) => {
            const keyObj = errors[0].errors[key];
            const keyObjKey = Object.keys(keyObj)[0];
            const newObj = obj;

            newObj[key] = {
                description: keyObj[keyObjKey]
            };

            return newObj;
        }, {});

        let captcha;

        if (this.state.captchaLoaded) {
            captcha = (
                <Recaptcha
                    className="g-recaptcha"
                    sitekey={process.env.CAPTCHA_SITEKEY}
                    onChange={this.handleCaptchaChange}
                    grecaptcha={this._grecaptcha}
                />
            );
        }

        const formClass = classnames('form row auth-form', {
            'form--show-labels': showLabels
        });

        const submitClass = classnames('button button--pill button--padded ', {
            [submitButtonClass]: submitButtonClass
        });

        return (
            <form
                action="/auth/join"
                className={formClass}
                onSubmit={onFormSubmit}
            >
                <div className="column small-24">
                    {showLabels && <label htmlFor="email">Email</label>}
                    <input
                        required
                        type="email"
                        autoComplete="email"
                        name="email"
                        id="email"
                        data-testid="email"
                        autoCapitalize="none"
                        placeholder={!showLabels ? 'Email' : null}
                        defaultValue={defaultEmail ? defaultEmail : ''}
                    />
                    {this.renderError(messages.email)}
                </div>
                <div className="column small-24">
                    {showLabels && (
                        <label htmlFor="username">Choose a Username</label>
                    )}
                    <input
                        required
                        type="text"
                        name="username"
                        id="username"
                        data-testid="username"
                        placeholder={!showLabels ? 'Username' : null}
                        onChange={this.handleUsernameChange}
                    />
                    {this.renderError(messages.artist_name)}
                </div>
                <div className="column small-24">
                    <PasswordInput
                        id="password"
                        showLabels={showLabels}
                        label="Create a Password"
                        inputProps={{
                            tabIndex: '3',
                            required: true,
                            defaultValue: defaultPassword
                        }}
                    />
                    {this.renderError(messages.password)}
                </div>
                <div className="small-24 align-middle align-center flex-dir-column join-submit-container u-text-center">
                    {captcha}
                    <p
                        style={{
                            textAlign: 'center',
                            marginTop: 10,
                            fontWeight: 600
                        }}
                    >
                        By signing up you agree to our{' '}
                        <a href="/about/terms-of-service" target="_blank">
                            Terms of Service
                        </a>{' '}
                        and{' '}
                        <a href="/about/privacy-policy" target="_blank">
                            Privacy Policy
                        </a>
                        .
                    </p>
                    <button
                        className={submitClass}
                        type="submit"
                        disabled={currentUser.loading}
                        data-testid="registerSubmit"
                    >
                        {currentUser.loading
                            ? 'Submitting...'
                            : submitButtonText}{' '}
                        {submitButtonIcon}
                    </button>
                </div>
            </form>
        );
    }
}
