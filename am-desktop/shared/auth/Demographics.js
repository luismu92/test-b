import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import DemographicsFormContainer from './DemographicsFormContainer';
import AndroidLoader from '../loaders/AndroidLoader';

export default class Demographics extends Component {
    static propTypes = {
        onSubmitSuccess: PropTypes.func,
        loading: PropTypes.bool,
        defaultEmail: PropTypes.string,
        defaultPassword: PropTypes.string
    };

    render() {
        const { onSubmitSuccess, loading } = this.props;

        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <Fragment>
                {loader}
                <DemographicsFormContainer
                    submitButtonClass="auth__submit auth-submit-button u-spacing-top-20"
                    onSubmitSuccess={onSubmitSuccess}
                    showLabels
                />
            </Fragment>
        );
    }
}
