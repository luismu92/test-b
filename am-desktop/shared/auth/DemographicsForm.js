import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';

import CustomDatePicker from '../components/CustomDatePicker';
import CalendarIcon from '../icons/calendar';
import Select from '../components/Select';

import styles from './DemographicsForm.module.scss';
import { genders } from 'constants/index';

export default class DemographicsForm extends Component {
    static propTypes = {
        onFormSubmit: PropTypes.func,
        showLabels: PropTypes.bool,
        submitButtonText: PropTypes.string,
        submitButtonClass: PropTypes.string
    };

    static defaultProps = {
        showLabels: false,
        submitButtonText: 'Finish'
    };

    constructor(props) {
        super(props);
        this.state = {
            date: moment().subtract(22, 'years'),
            gender: ''
        };
    }

    handleDateChange = (date) => {
        this.setState({ date });
    };

    handleGenderChange = (event) => {
        this.setState({
            gender: event.target.value
        });
    };

    render() {
        const {
            onFormSubmit,
            showLabels,
            submitButtonClass,
            submitButtonText
        } = this.props;
        const { date, gender } = this.state;

        const submitClass = classnames('button button--pill button--padded ', {
            [submitButtonClass]: submitButtonClass
        });

        return (
            <form
                onSubmit={onFormSubmit}
                className="form row auth-form form--show-labels"
            >
                <div className="column small-24">
                    {showLabels && (
                        <label htmlFor="birthday">When is your birthday?</label>
                    )}
                    <div className={`date-picker ${styles.datePicker}`}>
                        <CustomDatePicker
                            name="birthday"
                            id="birthday"
                            className={styles.birthday}
                            autoComplete="bday"
                            selected={date}
                            minDate={moment().subtract(100, 'years')}
                            maxDate={moment().subtract(13, 'years')}
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            popperModifiers={{
                                offset: {
                                    enabled: true,
                                    offset: '0, 0'
                                },
                                preventOverflow: {
                                    enabled: true,
                                    boundariesElement: 'viewport'
                                }
                            }}
                            onChange={this.handleDateChange}
                        />
                        <CalendarIcon className="date-picker__icon" />
                        <input
                            type="hidden"
                            name="birthdayFormatted"
                            value={date.format('YYYY-MM-DD')}
                        />
                    </div>
                </div>
                <div className={`column small-24 ${styles.gender}`}>
                    {showLabels && (
                        <label htmlFor="gender">What is your gender?</label>
                    )}
                    <Select
                        id="gender"
                        name="gender"
                        selectProps={{
                            'data-testid': 'gender'
                        }}
                        value={gender}
                        options={[
                            {
                                value: '',
                                text: 'Choose a gender',
                                disabled: true
                            },
                            ...Object.keys(genders).map((g) => {
                                return {
                                    value: g,
                                    text: genders[g]
                                };
                            })
                        ]}
                        onChange={this.handleGenderChange}
                    />
                </div>
                <div className="small-24 align-middle align-center flex-dir-column u-text-center">
                    <p className={styles.notice}>
                        Audiomack uses this data to personalize your experience,
                        help artists understand their listeners, and other uses.
                        We will always keep your personal data anonymous.
                    </p>
                </div>
                <div className="column small-24 u-text-center join-submit-container">
                    <p className={styles.terms}>
                        By signing up you agree to our{' '}
                        <a href="/about/terms-of-service" target="_blank">
                            Terms of Service
                        </a>{' '}
                        and{' '}
                        <a href="/about/privacy-policy" target="_blank">
                            Privacy Policy
                        </a>
                        .
                    </p>
                    <button
                        className={submitClass}
                        type="submit"
                        data-testid="demographicsSubmit"
                    >
                        {submitButtonText}
                    </button>
                </div>
            </form>
        );
    }
}
