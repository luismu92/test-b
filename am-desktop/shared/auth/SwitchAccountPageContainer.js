import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import Cookie from 'js-cookie';

import api from 'api/index';
import { cookies } from 'constants/index';

import { logIn } from '../redux/modules/user';
import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import requireAuth from '../hoc/requireAuth';
import SwitchAccountPage from './SwitchAccountPage';

class SwitchAccountPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            accounts: [],
            loading: true,
            signinFormActive: false,
            hasSuccessfulSignin: false,
            signinFormAccount: null
        };
    }

    componentDidMount() {
        this.loadAccounts();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAccountRemove = (e) => {
        const { dispatch } = this.props;
        const id = parseInt(e.currentTarget.getAttribute('data-id'), 10);
        const account = this.state.accounts.find((acc) => acc.id === id);

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Remove account',
                message: `Are you sure you want to remove <strong>${
                    account.name
                }</strong> from your account list? You can always add this account back later.`,
                confirmButtonText: 'Remove Account',
                handleConfirm: () => this.removeAccount(id)
            })
        );
    };

    handleAccountAddClick = () => {
        this.setState({
            signinFormActive: true,
            signinFormAccount: null
        });
    };

    handleSigninSubmit = (e) => {
        e.preventDefault();

        const { dispatch, currentUser } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;
        const password = form.password.value;
        const shouldCookieUser = true;

        dispatch(logIn(email, password, shouldCookieUser))
            .then(() => {
                this.setState({
                    hasSuccessfulSignin: true
                });
                this.saveUser(currentUser);

                window.location.href = '/';
                return;
            })
            .catch((err) => console.log(err));
    };

    handleAccountClick = (e) => {
        const id = parseInt(e.currentTarget.getAttribute('data-id'), 10);
        const account = this.state.accounts.find((acc) => acc.id === id);

        if (!account.active) {
            this.setState({
                signinFormActive: true,
                signinFormAccount: account
            });
            return;
        }

        const setState = false;

        this.saveUser(this.props.currentUser, setState, account);
        this.cookieUserAction(account)
            .then(() => {
                return (window.location.href = '/');
            })
            .catch((err) => {
                console.error(err);
            });
    };

    handleSigninCancel = () => {
        this.setState({
            signinFormActive: false,
            signinFormAccount: null
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getDataFromUser(user) {
        return {
            id: user.profile.id,
            token: user.token,
            secret: user.secret,
            image: user.profile.image,
            name: user.profile.name || user.profile.url_slug
        };
    }

    removeAccount(id) {
        const accounts = this.state.accounts.filter((account) => {
            return account.id !== id;
        });

        this.saveAccounts(accounts);

        this.setState({
            accounts: accounts
        });

        this.props.dispatch(hideModal());
    }

    loadAccounts() {
        const { currentUser } = this.props;

        let accounts = [];

        try {
            accounts =
                JSON.parse(
                    window.localStorage.getItem(cookies.otherAccounts)
                ) || [];
        } catch (err) {} // eslint-disable-line

        const me = this.getDataFromUser(currentUser);
        const filteredAccounts = accounts.filter((account) => {
            return account.id !== currentUser.profile.id;
        });

        const promises = [me].concat(filteredAccounts).map((account) => {
            const { token, secret } = account;

            if (account.id === currentUser.profile.id) {
                return Promise.resolve({
                    ...account,
                    active: true
                });
            }

            return api.user
                .get(token, secret)
                .then(() => {
                    return {
                        ...account,
                        active: true
                    };
                })
                .catch(() => {
                    return {
                        ...account,
                        active: false
                    };
                });
        });

        Promise.all(promises)
            .then((values) => {
                this.setState({
                    loading: false,
                    accounts: values
                });
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    }

    saveAccounts(accounts = []) {
        try {
            window.localStorage.setItem(
                cookies.otherAccounts,
                JSON.stringify(accounts.slice(0, 10))
            );
        } catch (err) {} // eslint-disable-line
    }

    saveUser(user, setState = true, firstAccount = null) {
        const currentAccounts = this.state.accounts.filter((account) => {
            return account.id !== user.profile.id;
        });

        let newAccounts = [this.getDataFromUser(user)].concat(currentAccounts);

        if (firstAccount) {
            newAccounts = [firstAccount].concat(
                newAccounts.filter((acc) => {
                    return acc.id !== firstAccount.id;
                })
            );
        }

        this.saveAccounts(newAccounts);

        if (setState) {
            this.setState({
                accounts: newAccounts
            });
        }

        return newAccounts;
    }

    cookieUserAction({ token, secret, expires = 0 } = {}) {
        const nowSeconds = Date.now() / 1000;
        const nowExpireSeconds = expires;
        const hours = (nowExpireSeconds - nowSeconds) / 3600;

        // Convert expires to days
        const expiration = hours / 24;

        Cookie.set('otoken', token, {
            path: '/',
            expires: expires ? expiration : undefined,
            secure:
                process.env.NODE_ENV !== 'development' &&
                process.env.AUDIOMACK_DEV !== 'true',
            sameSite: 'lax'
        });
        Cookie.set('osecret', secret, {
            path: '/',
            expires: expires ? expiration : undefined,
            secure:
                process.env.NODE_ENV !== 'development' &&
                process.env.AUDIOMACK_DEV !== 'true',
            sameSite: 'lax'
        });

        return api.user.get(token, secret);
    }

    render() {
        const { currentUser } = this.props;
        const {
            accounts,
            loading,
            signinFormActive,
            signinFormAccount
        } = this.state;

        return (
            <SwitchAccountPage
                currentUser={currentUser}
                loading={loading}
                accounts={accounts}
                signinFormActive={signinFormActive}
                signinFormAccount={signinFormAccount}
                hasSuccessfulSignin={this.state.hasSuccessfulSignin}
                onAccountRemove={this.handleAccountRemove}
                onAccountAddClick={this.handleAccountAddClick}
                onSigninSubmit={this.handleSigninSubmit}
                onSigninCancel={this.handleSigninCancel}
                onAccountClick={this.handleAccountClick}
            />
        );
    }
}

export default compose(requireAuth)(SwitchAccountPageContainer);
