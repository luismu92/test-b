import React from 'react';

export default function UploadMaintenance() {
    return (
        <div className="upload-page upload-start u-text-center">
            <h1 className="upload__main-title upload-start__main-title">
                Pardon the interruption
            </h1>
            <p className="upload-start__subtitle">
                The site is currently undergoing maintenance. Please try
                uploading again later. We’re sorry for any inconvenience.
            </p>
        </div>
    );
}
