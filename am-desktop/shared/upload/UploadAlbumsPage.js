import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {
    allGenresMap,
    GENRE_TYPE_OTHER,
    tagSections,
    UPLOAD_STATUS
} from 'constants/index';
import { yesBool } from 'utils/index';
import { eventLabel } from 'utils/analytics';
import { getTagOptions, getUserTags } from 'utils/tags';

import AddSingleScreen from './AddSingleScreen';
import AlbumEditPageContainer from '../edit/AlbumEditPageContainer';
import UploadForm from './UploadForm';
import UploadItem from './UploadItem';
import UploadMaintenance from './UploadMaintenance';

import ArrowIcon from 'icons/arrow-next';
import CloseThinIcon from '../icons/close-thin';

import AddArtwork from '../components/AddArtwork';
import Dropdown from '../components/Dropdown';
import FixedActionBarContainer from '../components/FixedActionBarContainer';
import LabeledInput, {
    inputTypes as labeledInputTypes
} from '../components/LabeledInput';
import StepProgressBar from './StepProgressBar';
import TunecoreAd from '../components/Tunecore';

import styles from './UploadAlbumsPage.module.scss';

export default class UploadAlbumsPage extends Component {
    static propTypes = {
        upload: PropTypes.object.isRequired,
        musicEdit: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        currentUploadStep: PropTypes.object.isRequired,
        previousUploadStep: PropTypes.object.isRequired,
        maxVisitedUploadStep: PropTypes.number.isRequired,
        uploadSteps: PropTypes.array.isRequired,
        errors: PropTypes.object.isRequired,
        // player: PropTypes.object,
        // currentUserUploads: PropTypes.object,
        fromMobile: PropTypes.bool,
        isEditPage: PropTypes.bool,
        noLeaveModal: PropTypes.bool,
        tags: PropTypes.object,
        temporaryAlbumTags: PropTypes.array,
        uploadAlbum: PropTypes.object,
        onAlbumDelete: PropTypes.func,
        onAlbumInfoInput: PropTypes.func,
        onAlbumEditSave: PropTypes.func,
        onAlbumSave: PropTypes.func,
        onCancelUploadStep: PropTypes.func,
        onDropdownChange: PropTypes.func,
        onNextUploadStep: PropTypes.func,
        onPreExistingCancelClick: PropTypes.func,
        onSuggestTagClick: PropTypes.func,
        onUploadCancelClick: PropTypes.func,
        onUploadStepClick: PropTypes.func
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getUploadList() {
        const { upload } = this.props;

        return upload.list.reduce(
            (acc, item) => {
                if (typeof item.replaceId !== 'undefined') {
                    return acc;
                }

                if (item.status === UPLOAD_STATUS.FAILED) {
                    acc.failed.push(item);
                } else if (item.status === UPLOAD_STATUS.CANCELED) {
                    acc.canceled.push(item);
                } else if (item.status !== UPLOAD_STATUS.COMPLETED) {
                    acc.nonFinished.push(item);
                } else if (item.status === UPLOAD_STATUS.COMPLETED) {
                    acc.completed.push(item);
                }

                return acc;
            },
            { canceled: [], completed: [], failed: [], nonFinished: [] }
        );
    }

    renderAlbumDetails() {
        const {
            currentUser,
            musicEdit,
            onAlbumInfoInput,
            onDropdownChange,
            onSuggestTagClick,
            tags,
            temporaryAlbumTags
        } = this.props;
        let { errors } = this.props;

        const disabledAmp = ['yes-disabled', 'no-disabled'].includes(
            currentUser.profile.video_ads
        );

        const genres = Object.entries(allGenresMap).map(([key, value]) => ({
            value: key,
            text: value
        }));

        const tagOptions = getTagOptions(tags);
        const userTags = getUserTags(
            tags.id[musicEdit.info.id] || temporaryAlbumTags
        );

        errors = errors[musicEdit.info.id] || {};

        let monetization = (
            <LabeledInput
                id="album-upc"
                label="UPC"
                name="upc"
                onChange={onAlbumInfoInput}
                tooltip="Enter UPC code if this item has one"
                type={labeledInputTypes.text}
                value={musicEdit.info.upc}
            />
        );

        if (musicEdit.info.amp) {
            monetization = (
                <Fragment>
                    <LabeledInput
                        checked={yesBool(musicEdit.info.video_ad)}
                        disabled={disabledAmp}
                        id="album-monetize"
                        label="Monetize"
                        name="video_ad"
                        onChange={onAlbumInfoInput}
                        type={labeledInputTypes.toggle}
                        value={currentUser.profile.label_name}
                    />

                    <LabeledInput
                        id="album-upc"
                        label="UPC"
                        name="upc"
                        onChange={onAlbumInfoInput}
                        tooltip="Enter UPC code if this item has one"
                        type={labeledInputTypes.text}
                        value={musicEdit.info.upc}
                    />
                </Fragment>
            );
        }

        return (
            <main className={styles.content}>
                <section className={styles.requiredMetadata}>
                    <AddArtwork
                        image={musicEdit.info.image}
                        onChange={onAlbumInfoInput}
                        type="album"
                    />

                    <div className={styles.requiredInputs}>
                        <LabeledInput
                            data-testid="albumArtist"
                            error={errors.artist}
                            id="album-artist"
                            label="Artist"
                            maxLength={75}
                            name="artist"
                            onChange={onAlbumInfoInput}
                            type={labeledInputTypes.text}
                            value={musicEdit.info.artist}
                            required
                        />

                        <LabeledInput
                            data-testid="albumTitle"
                            error={errors.title}
                            id="album-title"
                            label="Album Name"
                            maxLength={75}
                            name="title"
                            onChange={onAlbumInfoInput}
                            type={labeledInputTypes.text}
                            value={musicEdit.info.title}
                            required
                        />

                        <div className={styles.dropdown}>
                            <LabeledInput
                                data-testid="albumGenre"
                                error={errors.genre}
                                id="album-genre"
                                label="Genre"
                                name="genre"
                                onChange={onDropdownChange}
                                options={genres}
                                type={labeledInputTypes.dropdown}
                                value={
                                    musicEdit.info.genre !== GENRE_TYPE_OTHER
                                        ? musicEdit.info.genre
                                        : ''
                                }
                                required
                            />
                        </div>
                    </div>
                </section>

                <div className={styles.tagsLabelRow}>
                    <span className={styles.tagsLabel}>Tags</span>

                    <span className={styles.suggestTag}>
                        Want to{' '}
                        <button
                            className="button-link"
                            onClick={onSuggestTagClick}
                        >
                            suggest a new tag?
                        </button>
                    </span>
                </div>

                <section className={styles.inputGroup}>
                    <div className={styles.dropdown}>
                        <Dropdown
                            id={`subgenres-${musicEdit.info.id}`}
                            multiple
                            name={tagSections.subgenre}
                            onChange={onDropdownChange}
                            options={tagOptions.subgenres}
                            sections
                            showDeselectButton
                            type="tag"
                            value={userTags.subgenres}
                        />
                    </div>

                    <div className={styles.dropdown}>
                        <Dropdown
                            id={`mood-${musicEdit.info.id}`}
                            multiple
                            name={tagSections.mood}
                            onChange={onDropdownChange}
                            options={tagOptions.moods}
                            showDeselectButton
                            type="tag"
                            value={userTags.moods}
                        />
                    </div>
                </section>

                <LabeledInput
                    id="album-description"
                    label="Album Description"
                    maxLength={800}
                    name="description"
                    onChange={onAlbumInfoInput}
                    type={labeledInputTypes.textarea}
                    value={musicEdit.info.description}
                />

                <LabeledInput
                    id="album-video"
                    label="YouTube / Vimeo URL"
                    subtitle="(Note: Video URLs only. This entry will link to a music video or album promo video of your choice.)"
                    name="video"
                    onChange={onAlbumInfoInput}
                    type={labeledInputTypes.url}
                    value={musicEdit.info.video}
                />
                {monetization}
            </main>
        );
    }

    renderSongUploads() {
        const {
            currentUser,
            onPreExistingCancelClick,
            onUploadCancelClick,
            upload,
            uploadAlbum
        } = this.props;
        const { preExistingTrackQueue } = uploadAlbum;
        const { completed, failed, nonFinished } = this.getUploadList();
        const uploadList = [...completed, ...failed, ...nonFinished];

        const uploadItems = uploadList.map((uploadItem) => {
            return (
                <UploadItem
                    key={uploadItem.id}
                    currentUser={currentUser}
                    uploadItem={uploadItem}
                    onCancelClick={onUploadCancelClick}
                    uploadPage={true}
                />
            );
        });

        let preExistingTracks;

        if (preExistingTrackQueue.length) {
            preExistingTracks = preExistingTrackQueue.map((track) => (
                <UploadItem
                    key={track.id}
                    currentUser={currentUser}
                    uploadItem={track}
                    preExistingTrack={true}
                    onCancelClick={onPreExistingCancelClick}
                    uploadPage={true}
                />
            ));
        } else if (uploadAlbum.info.tracks && uploadAlbum.info.tracks.length) {
            preExistingTracks = uploadAlbum.info.tracks.reduce(
                (tracks, track) => {
                    if (track.preExisting) {
                        const uploadItem = (
                            <UploadItem
                                key={track.id}
                                currentUser={currentUser}
                                uploadItem={track}
                                preExistingTrack={true}
                                onCancelClick={onPreExistingCancelClick}
                                uploadPage={true}
                            />
                        );

                        tracks.push(uploadItem);
                    }

                    return tracks;
                },
                []
            );
        }

        return (
            <main>
                <UploadForm
                    error={upload.policyError}
                    headerText="Browse for files or drag and drop them here"
                    inputId="file-input"
                    inputType="audio/*,application/zip"
                    multiple
                    uploadType="albumUpload"
                    withForm={false}
                />
                {uploadItems}
                {preExistingTracks}
            </main>
        );
    }

    renderAlbumEdit() {
        const {
            currentUploadStep,
            previousUploadStep,
            fromMobile,
            isEditPage,
            noLeaveModal,
            onAlbumDelete,
            onAlbumSave,
            onSuggestTagClick,
            uploadAlbum
        } = this.props;

        return (
            <AlbumEditPageContainer
                currentUploadStep={currentUploadStep}
                previousUploadStep={previousUploadStep}
                isEditPage={isEditPage}
                noLeaveModal={noLeaveModal}
                onAlbumDelete={onAlbumDelete}
                onAlbumSave={onAlbumSave}
                onSuggestTagClick={onSuggestTagClick}
                preExistingTrackQueue={uploadAlbum.preExistingTrackQueue}
                fromMobile={fromMobile}
                AlbumDetails={this.renderAlbumDetails()}
                SongUploads={this.renderSongUploads()}
            />
        );
    }

    renderHeader() {
        const {
            currentUploadStep,
            previousUploadStep,
            isEditPage,
            maxVisitedUploadStep,
            musicEdit,
            onUploadStepClick,
            uploadSteps
        } = this.props;
        const { label, number } = currentUploadStep;
        const stepList = uploadSteps.map((uploadStep) => uploadStep.name);
        const uploadList = this.getUploadList();

        let stepNumber = number;
        let subtitle = label;

        if (currentUploadStep.number === 2) {
            if (uploadList.nonFinished.length) {
                subtitle = 'Your album is uploading...';
            } else if (
                uploadList.nonFinished.length === 0 &&
                uploadList.completed.length
            ) {
                subtitle = 'Your upload is complete!';
            }
        }

        if (currentUploadStep.number === 5) {
            stepNumber = previousUploadStep.number;
            subtitle = previousUploadStep.label;
        }

        return (
            <header>
                <h1 className={styles.title}>
                    Step {stepNumber}:{' '}
                    <span className={styles.subtitle}>{subtitle}</span>
                </h1>
                <StepProgressBar
                    backgroundColor="gray"
                    basePath={
                        isEditPage ? `edit/album/${musicEdit.info.id}` : null
                    }
                    currentStep={number}
                    maxVisitedStep={maxVisitedUploadStep}
                    onStepClick={onUploadStepClick}
                    stepList={stepList}
                />
            </header>
        );
    }

    renderUploadStep() {
        const { currentUploadStep, isEditPage, uploadAlbum } = this.props;
        const { isAddingSingles } = uploadAlbum;

        if (isEditPage) {
            return this.renderAlbumEdit();
        }

        switch (currentUploadStep.number) {
            case 1:
                return this.renderAlbumDetails();

            case 2:
                return isAddingSingles ? (
                    <AddSingleScreen />
                ) : (
                    this.renderSongUploads()
                );

            case 3:
            case 4:
            case 5:
                return this.renderAlbumEdit();

            default:
                return null;
        }
    }

    renderFooter() {
        const {
            currentUploadStep,
            previousUploadStep,
            isEditPage,
            musicEdit,
            onAlbumEditSave,
            onUploadStepClick,
            maxVisitedUploadStep,
            uploadAlbum
        } = this.props;
        const { isAddingSingles } = uploadAlbum;
        const uploadList = this.getUploadList();

        // AddSingleScreen has its own instance of the FixedActionBarContainer footer because it also needs to work on AlbumEditPage
        if (isAddingSingles) {
            return null;
        }

        let disableNextButton;
        let disableSaveButton;

        let nextButtonText = 'Next Step';
        let saveButtonText = 'Save & Exit';

        let deleteButton;
        let saveButton;

        let previousButton = (
            <button
                className={styles.previousButton}
                data-step-number={currentUploadStep.number - 1}
                data-testid="previousUploadStep"
                onClick={onUploadStepClick}
            >
                Back
            </button>
        );

        if (!isEditPage) {
            deleteButton = (
                <button className={styles.closeThinIcon} type="reset">
                    <CloseThinIcon />
                </button>
            );
        }

        switch (currentUploadStep.number) {
            case 1:
                disableNextButton =
                    !musicEdit.info.artist ||
                    !musicEdit.info.title ||
                    !musicEdit.info.genre;

                previousButton = null;

                break;

            case 2:
                // enable if no uploads are incomplete and at least one upload is complete (not canceled or failed)
                disableNextButton = !(
                    uploadList.nonFinished.length === 0 &&
                    uploadList.completed.length
                );

                if (maxVisitedUploadStep > currentUploadStep.number) {
                    disableNextButton = false;
                }

                break;

            case 4:
                nextButtonText = 'Finish';

                break;

            case 5:
                if (previousUploadStep.number === 4) {
                    nextButtonText = 'Finishing...';
                }

                saveButtonText = 'Saving...';

                break;

            default:
                disableNextButton = false;

                break;
        }

        if (isEditPage) {
            disableSaveButton =
                !musicEdit.info.artist ||
                !musicEdit.info.title ||
                !musicEdit.info.genre;

            disableNextButton = disableSaveButton;

            saveButton = (
                <button
                    className={styles.saveButton}
                    data-testid="saveAlbum"
                    disabled={disableSaveButton}
                    onClick={onAlbumEditSave}
                >
                    {saveButtonText}
                </button>
            );
        }

        return (
            <FixedActionBarContainer>
                {previousButton}
                <button
                    className={classnames(
                        'button',
                        'button--pill',
                        'button--med',
                        styles.button
                    )}
                    data-testid="nextUploadStep"
                    disabled={disableNextButton}
                    type="submit"
                >
                    <span>{nextButtonText}</span>
                    <ArrowIcon className={styles.arrowIcon} />
                </button>
                {saveButton}
                {deleteButton}
            </FixedActionBarContainer>
        );
    }

    render() {
        const { onCancelUploadStep, onNextUploadStep } = this.props;

        if (process.env.MAINTENANCE_MODE === 'true') {
            return <UploadMaintenance />;
        }

        return (
            <Fragment>
                <form
                    encType="multipart/form-data"
                    onReset={onCancelUploadStep}
                    onSubmit={onNextUploadStep}
                >
                    {this.renderHeader()}
                    {this.renderUploadStep()}
                    {this.renderFooter()}
                </form>
                <div
                    className="row"
                    style={{ marginBottom: 50, maxWidth: 940 }}
                >
                    <div className="column">
                        <TunecoreAd
                            className="u-text-center u-spacing-top"
                            linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=%20all_afpup%20_su&utm_content=ghoammot"
                            eventLabel={eventLabel.albumUpload}
                            variant="condensed"
                        />
                    </div>
                </div>
            </Fragment>
        );
    }
}
