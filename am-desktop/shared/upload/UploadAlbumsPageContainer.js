import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';

import analytics, { eventCategory, eventAction } from 'utils/analytics';
import { yesBool, debounce } from 'utils/index';
import { handleMusicInputChange } from 'utils/music/edit';
import { allGenresMap, tagSections, UPLOAD_STATUS } from 'constants/index'; // eslint-disable-line no-unused-vars

import requireAuth from '../hoc/requireAuth';
import hideSidebarForComponent from '../hoc/hideSidebarForComponent';

import {
    showModal,
    hideModal,
    MODAL_TYPE_ALBUM_FINISH_UPLOAD,
    MODAL_TYPE_CHOOSE_PREVIOUS_SONGS,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_SUGGEST_TAG
} from '../redux/modules/modal';

import {
    getGlobalGeoTags,
    getMusicTags,
    addMusicTags,
    deleteMusicTags,
    suggestMusicTag
} from '../redux/modules/music/tags';
import { addToast } from '../redux/modules/toastNotification';
import {
    reset as resetAlbumUpload,
    getAlbumById,
    queuePreExistingTracks,
    removeAlbumTrack,
    setIsAddingSingles
} from '../redux/modules/upload/album';
import {
    reset as resetUploadList,
    cancelUpload
} from '../redux/modules/upload/index';
import { getUserUploads } from '../redux/modules/user/uploads';
import {
    updateItemInfo,
    setInfo,
    reset as resetMusicEdit
} from '../redux/modules/music/edit';
import { deleteAlbumTrackItem, deleteItem } from '../redux/modules/music/index';

import ChoosePreviousSongsModal from '../modal/ChoosePreviousSongsModal';
import SuggestTagModal from '../modal/SuggestTagModal';
import UploadAlbumsPage from './UploadAlbumsPage';

class UploadAlbumsPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        upload: PropTypes.object,
        history: PropTypes.object,
        uploadAlbum: PropTypes.object,
        uploadSteps: PropTypes.array,
        currentUserUploads: PropTypes.object,
        musicEdit: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        // route: PropTypes.object,
        currentUser: PropTypes.object,
        isEditPage: PropTypes.bool,
        tags: PropTypes.object
    };

    static defaultProps = {
        uploadSteps: [
            {
                label: 'Enter Album Details',
                name: 'Album Details',
                number: 1
            },
            {
                label: 'Upload Your Album',
                name: 'Add Music',
                number: 2
            },
            {
                label: 'Edit & Reorder Album',
                name: 'Edit/Reorder',
                number: 3
            },
            {
                label: 'Choose Your Release',
                name: 'Release',
                number: 4
            },
            {
                label: 'Your upload is now live!',
                name: 'Finish',
                number: 5
            }
        ]
    };

    constructor(props) {
        super(props);

        const currentStep =
            props.uploadSteps[parseInt(props.match.params.step, 10) - 1] ||
            props.uploadSteps[0];

        this.state = {
            currentUploadStep: currentStep,
            previousUploadStep:
                props.uploadSteps[parseInt(props.match.params.step, 10) - 2] ||
                props.uploadSteps[0],
            maxVisitedUploadStep: props.isEditPage ? 4 : currentStep.number,
            errors: {},
            fromMobile: !!parse(props.location.search).fromMobile,
            noLeaveModal: false,
            temporaryAlbumTags: [],
            temporaryAlbumTagSuggestion: ''
        };
    }

    componentDidMount() {
        this.fetchTags();
    }

    componentDidUpdate(prevProps) {
        // Just received an album ID. poll for tracks
        if (
            this.props.uploadAlbum.info.id &&
            !(prevProps.uploadAlbum.info.tracks || []).length
        ) {
            if (!this._albumPollTimer) {
                this.pollForAlbumTracks(this.props.uploadAlbum.info);
            }
        }

        if (
            prevProps.currentUserUploads.query &&
            prevProps.currentUserUploads.query.trim() !==
                this.props.currentUserUploads.query.trim()
        ) {
            this.performUploadSearch(this.props.currentUserUploads.query);
        }

        if (this.props.musicEdit.info.id && !prevProps.musicEdit.info.id) {
            const actionQueue = this.state.temporaryAlbumTags.map((tag) => {
                const action = addMusicTags(
                    this.props.musicEdit.info,
                    tag.normalizedKey
                );
                return this.props.dispatch(action);
            });

            if (this.state.temporaryAlbumTagSuggestion) {
                const action = suggestMusicTag(
                    this.props.musicEdit.info,
                    this.state.temporaryAlbumTagSuggestion
                );

                actionQueue.push(this.props.dispatch(action));
            }

            Promise.all(actionQueue)
                .then(() => {
                    this.fetchTags();
                    this.setState({
                        temporaryAlbumTags: [],
                        temporaryAlbumTagSuggestion: ''
                    });

                    return;
                })
                .catch((err) => {
                    this.props.dispatch(
                        addToast({
                            action: 'message',
                            item: err.message
                        })
                    );

                    console.log(err);
                });
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        clearTimeout(this._albumPollTimer);
        this._albumPollTimer = null;

        dispatch(resetUploadList());
        dispatch(resetAlbumUpload());
        dispatch(resetMusicEdit());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAlbumDelete = () => {
        const {
            currentUser,
            dispatch,
            history,
            isEditPage,
            musicEdit
        } = this.props;
        const { id: albumId } = musicEdit.info;

        this.setState({ noLeaveModal: true }, () => {
            dispatch(hideModal());

            if (albumId) {
                dispatch(deleteItem(albumId));
            }

            dispatch(resetUploadList());
            dispatch(resetAlbumUpload());
            dispatch(resetMusicEdit());

            if (isEditPage) {
                history.push(`/artist/${currentUser.profile.url_slug}`);
            } else {
                history.push('/upload');
            }
        });
    };

    handleAlbumInfoInput = (e) => {
        const { dispatch, musicEdit } = this.props;
        const input = e.currentTarget;
        const change = handleMusicInputChange(input);
        const musicId = musicEdit.info.id;
        const errors = this.state.errors[musicId] || {};

        errors[change.name] = !!change.error;

        this.setState((prevState) => {
            return {
                errors: {
                    ...prevState.errors,
                    [musicId]: errors
                }
            };
        });

        dispatch(updateItemInfo(change.name, change.value));
    };

    handleAlbumEditSave = (e) => {
        e.preventDefault();

        const { isEditPage, uploadSteps } = this.props;
        const { currentUploadStep } = this.state;

        if (
            !isEditPage ||
            currentUploadStep.number >= uploadSteps.length ||
            currentUploadStep.number < 1
        ) {
            return;
        }

        const lastUploadStep = uploadSteps[uploadSteps.length - 1];

        this.setState({
            currentUploadStep: lastUploadStep,
            previousUploadStep: currentUploadStep
        });
    };

    handleAlbumSave = (finalUrl, forcePageReload) => {
        const { dispatch, history, isEditPage, musicEdit } = this.props;
        const uploadSteps = this.props.uploadSteps.map(
            (uploadStep) => uploadStep.name
        );

        analytics.track(
            eventCategory.upload,
            {
                eventAction: eventAction.albumUploadSave
            },
            ['ga', 'fb']
        );

        if (forcePageReload) {
            window.location.href = finalUrl;
            return null;
        }

        history.push(finalUrl);

        if (!isEditPage) {
            dispatch(
                showModal(MODAL_TYPE_ALBUM_FINISH_UPLOAD, {
                    musicEdit,
                    uploadSteps
                })
            );
        }

        return null;
    };

    handleCancelUploadStep = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Delete album',
                message:
                    'Are you sure you want to delete this album? This action cannot be undone.',
                handleConfirm: () => this.handleAlbumDelete(),
                confirmButtonText: 'Delete',
                confirmButtonProps: {
                    className: 'button button--pill button--danger',
                    'data-testid': 'deleteConfirmButton'
                }
            })
        );
    };

    handleDropdownChange = async (text, e) => {
        const { dispatch, musicEdit, tags } = this.props;
        const { temporaryAlbumTags } = this.state;
        const genres = Object.values(allGenresMap);
        const button = e.target;
        const dropdown = button.closest('[data-dropdown]');
        const section = dropdown.getAttribute('data-name');
        let value = button.getAttribute('data-value');
        let list;

        if (!value) {
            return;
        }

        if (section === 'genre') {
            const event = {
                ...e,
                currentTarget: {
                    ...e.currentTarget,
                    type: 'dropdown',
                    name: 'genre',
                    value: value === musicEdit.info.genre ? '' : value
                }
            };

            this.handleAlbumInfoInput(event);
            return;
        }

        value = value.replace(/\-\>/g, ' ');

        if (!musicEdit.info.id) {
            list = temporaryAlbumTags;

            try {
                switch (section) {
                    case tagSections.locations: {
                        break;
                    }

                    case tagSections.mood: {
                        const removeMood = list.find(
                            (tag) => tag.normalizedKey === value
                        );

                        if (removeMood) {
                            const newList = list.filter(
                                (tag) => tag.normalizedKey !== value
                            );

                            this.setState({
                                temporaryAlbumTags: newList
                            });

                            return;
                        }

                        const currentMoods = list.filter(
                            (tag) => tag.section === tagSections.mood
                        );

                        if (currentMoods.length === 2) {
                            throw new Error(
                                'You can only have 2 tags from this category.'
                            );
                        }

                        break;
                    }

                    case tagSections.subgenre: {
                        const removeSubgenre = list.find(
                            (tag) => tag.normalizedKey === value
                        );

                        if (removeSubgenre) {
                            const newList = list.filter(
                                (tag) => tag.normalizedKey !== value
                            );

                            this.setState({
                                temporaryAlbumTags: newList
                            });

                            return;
                        }

                        const currentSubgenres = list.filter(
                            (tag) =>
                                tag.section === tagSections.subgenre ||
                                genres.includes(tag.section)
                        );

                        if (currentSubgenres.length === 2) {
                            throw new Error(
                                'You can only have 2 tags from this category.'
                            );
                        }

                        break;
                    }

                    default:
                        break;
                }

                const options = [...tags.options.geo, ...tags.options.music];
                const newTag = options.find(
                    (tag) => tag.normalizedKey === value
                );

                if (!newTag) {
                    return;
                }

                this.setState((prevState) => ({
                    temporaryAlbumTags: [
                        ...prevState.temporaryAlbumTags,
                        newTag
                    ]
                }));
            } catch (error) {
                dispatch(
                    addToast({
                        action: 'message',
                        item: error.message
                    })
                );
            }

            return;
        }

        list = tags.id[musicEdit.info.id] || [];

        try {
            switch (section) {
                case tagSections.locations: {
                    break;
                }

                case tagSections.mood: {
                    const removeMood = list.find(
                        (tag) => tag.normalizedKey === value
                    );

                    if (removeMood) {
                        await dispatch(
                            deleteMusicTags(
                                musicEdit.info,
                                removeMood.normalizedKey
                            )
                        );

                        return;
                    }

                    const currentMoods = list.filter(
                        (tag) => tag.section === tagSections.mood
                    );

                    if (currentMoods.length === 2) {
                        throw new Error(
                            'You can only have 2 tags from this category.'
                        );
                    }

                    break;
                }

                case tagSections.subgenre: {
                    const removeSubgenre = list.find(
                        (tag) => tag.normalizedKey === value
                    );

                    if (removeSubgenre) {
                        await dispatch(
                            deleteMusicTags(
                                musicEdit.info,
                                removeSubgenre.normalizedKey
                            )
                        );

                        return;
                    }

                    const currentSubgenres = list.filter(
                        (tag) =>
                            tag.section === tagSections.subgenre ||
                            genres.includes(tag.section)
                    );

                    if (currentSubgenres.length === 2) {
                        throw new Error(
                            'You can only have 2 tags from this category.'
                        );
                    }

                    break;
                }

                default:
                    break;
            }

            await dispatch(addMusicTags(musicEdit.info, value));
        } catch (error) {
            dispatch(
                addToast({
                    action: 'message',
                    item: error.message
                })
            );
        }
    };

    handleModalChoiceClick = (e) => {
        const { dispatch } = this.props;
        const chooseSongs =
            e.currentTarget.getAttribute('data-choice') === 'choose';

        dispatch(setIsAddingSingles(chooseSongs));
        dispatch(hideModal());

        if (chooseSongs) {
            dispatch(getUserUploads());
        } else {
            dispatch(queuePreExistingTracks([]));
        }
    };

    handleNextUploadStep = (e) => {
        e.preventDefault();

        const {
            currentUser,
            dispatch,
            isEditPage,
            uploadAlbum,
            uploadSteps
        } = this.props;
        const { currentUploadStep, maxVisitedUploadStep } = this.state;
        const { isAddingSingles } = uploadAlbum;

        if (
            currentUploadStep.number >= uploadSteps.length ||
            currentUploadStep.number < 1
        ) {
            return;
        }

        // array indices are zero-based while the upload step numbers are one-based
        // therefore we can use currentUploadStep.number as the index for nextUploadStep
        const nextIndex = currentUploadStep.number;
        const nextUploadStep = uploadSteps[nextIndex];

        if (currentUploadStep.number === 1 && !isEditPage) {
            if (currentUser.profile.upload_count_excluding_reups) {
                dispatch(showModal(MODAL_TYPE_CHOOSE_PREVIOUS_SONGS));
            } else {
                dispatch(queuePreExistingTracks([]));
            }
        }

        if (currentUploadStep.number === 2 && isAddingSingles) {
            return;
        }

        this.setState({
            currentUploadStep: nextUploadStep,
            previousUploadStep: currentUploadStep,
            noLeaveModal: true
        });

        if (nextUploadStep.number > maxVisitedUploadStep) {
            this.setState({
                maxVisitedUploadStep: nextUploadStep.number
            });
        }

        if (nextUploadStep.number !== 2 && isAddingSingles) {
            dispatch(setIsAddingSingles(false));
        }
    };

    handlePreExistingCancelClick = (e) => {
        e.preventDefault();

        const { dispatch, uploadAlbum } = this.props;
        const { preExistingTrackQueue } = uploadAlbum;
        const { id: albumId } = uploadAlbum.info;
        const musicId = e.currentTarget.getAttribute('data-upload-id');
        const newList = preExistingTrackQueue.filter(
            (track) => track.id.toString() !== musicId
        );

        dispatch(queuePreExistingTracks(newList));
        dispatch(removeAlbumTrack(musicId));
        dispatch(deleteAlbumTrackItem(albumId, musicId));
    };

    handleSuggestTagClick = (e) => {
        const { dispatch } = this.props;

        e.preventDefault();

        dispatch(showModal(MODAL_TYPE_SUGGEST_TAG));
    };

    handleSuggestTagSubmit = (input) => {
        this.setState({
            temporaryAlbumTagSuggestion: input
        });
    };

    handleUploadCancelClick = (e) => {
        e.preventDefault();

        const { dispatch, uploadAlbum } = this.props;
        const { id: albumId, tracks } = uploadAlbum.info;
        const uploadId = e.currentTarget.getAttribute('data-upload-id');
        const albumEntityTrack = tracks.find((track) => track.id === uploadId);

        let musicId = e.currentTarget.getAttribute('data-music-id');

        if (albumEntityTrack) {
            musicId = albumEntityTrack.song_id;
        }

        dispatch(cancelUpload(uploadId));
        dispatch(removeAlbumTrack(musicId));
        dispatch(deleteAlbumTrackItem(albumId, musicId));
    };

    handleUploadStepClick = (e) => {
        e.preventDefault();

        const { dispatch, uploadSteps, uploadAlbum } = this.props;
        const { currentUploadStep, maxVisitedUploadStep } = this.state;
        const { isAddingSingles } = uploadAlbum;
        const stepNumber = e.currentTarget.getAttribute('data-step-number');
        const index = parseInt(stepNumber, 10) - 1;

        if (
            stepNumber <= maxVisitedUploadStep &&
            stepNumber !== currentUploadStep.number
        ) {
            if (isAddingSingles) {
                dispatch(setIsAddingSingles(false));
            }

            this.setState({
                currentUploadStep: uploadSteps[index],
                previousUploadStep: currentUploadStep,
                noLeaveModal: true
            });
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    fetchTags() {
        const { dispatch, musicEdit, tags } = this.props;

        // check tags.ui.geo instead of tags.options.geo because root-level (country) tags are not valid options
        if (!tags.ui.geo.length) {
            dispatch(getGlobalGeoTags());
        }

        if (musicEdit.info.id) {
            dispatch(getMusicTags(musicEdit.info));
        }
    }

    performUploadSearch = debounce((query) => {
        const { dispatch } = this.props;

        dispatch(
            getUserUploads({
                page: 1,
                query: query,
                incompletes: 'yes'
            })
        );
    }, 500);

    pollForAlbumTracks(album) {
        this._albumPollTimer = true;
        const { dispatch, currentUser, upload } = this.props;

        dispatch(getAlbumById(album.id))
            .then((action) => {
                const tracks = action.resolved.results.tracks || [];
                const completedUploads = upload.list.filter((obj) => {
                    return obj.status === UPLOAD_STATUS.COMPLETED;
                });

                // Check for completed uploads in order to keep querying for album
                // until we have the tracks we expect. This fixes a condition where
                // you can upload 2 tracks and querying for the album returns 1 track
                if (tracks.length && tracks.length >= completedUploads.length) {
                    const results = action.resolved.results;
                    const keepExistingInfo = true;
                    const info = {
                        ...results,
                        stream_only:
                            typeof results.stream_only === 'boolean'
                                ? results.stream_only
                                : true,
                        video_ad: yesBool(currentUser.profile.video_ads)
                    };

                    dispatch(setInfo(info, keepExistingInfo));

                    this._albumPollTimer = null;
                    return;
                }

                this._albumPollTimer = setTimeout(
                    () => this.pollForAlbumTracks(album),
                    2000
                );
                return;
            })
            .catch((err) => console.error(err));
    }

    render() {
        const {
            upload,
            uploadAlbum,
            uploadSteps,
            currentUser,
            history,
            isEditPage,
            musicEdit
        } = this.props;

        // Uncomment to test album view

        // uploadAlbum.info.id = 3;
        // uploadAlbum.info.released = Date.now() / 1000;
        // uploadAlbum.info.url_slug = 'some-shit'; // eslint-disable-line
        // uploadAlbum.info.artist = 'Some artist';
        // uploadAlbum.info.title = 'Some title';
        // uploadAlbum.info.tracks = [
        //     {
        //         title: 'Chainsmoker ish',
        //         'song_id': 1,
        //         video_ad: 'yes'
        //     },
        //     {
        //         title: 'Dumb and dumber',
        //         'song_id': 2
        //     },
        //     {
        //         title: 'Dancing in the rain',
        //         'song_id': 3
        //     },
        //     {
        //         title: 'Good times',
        //         'song_id': 4
        //     },
        //     {
        //         title: 'Really really',
        //         'song_id': 5
        //     }
        // ];

        // upload.list = [
        //     {
        //         title: 'Some other ish',
        //         bytesUploaded: 3201231,
        //         bytesTotal: 4501231,
        //         errors: ['Some error message'],
        //         status: UPLOAD_STATUS.FAILED
        //     }
        // ];

        return (
            <Fragment>
                <UploadAlbumsPage
                    upload={upload}
                    currentUser={currentUser}
                    musicEdit={musicEdit}
                    noLeaveModal={this.state.noLeaveModal}
                    uploadAlbum={uploadAlbum}
                    currentUserUploads={this.props.currentUserUploads}
                    onAlbumInfoInput={this.handleAlbumInfoInput}
                    onAlbumDelete={this.handleAlbumDelete}
                    onAlbumEditSave={this.handleAlbumEditSave}
                    onAlbumSave={this.handleAlbumSave}
                    onPreExistingCancelClick={this.handlePreExistingCancelClick}
                    fromMobile={this.state.fromMobile}
                    history={history}
                    isEditPage={isEditPage}
                    currentUploadStep={this.state.currentUploadStep}
                    previousUploadStep={this.state.previousUploadStep}
                    maxVisitedUploadStep={this.state.maxVisitedUploadStep}
                    onUploadStepClick={this.handleUploadStepClick}
                    errors={this.state.errors}
                    uploadSteps={uploadSteps}
                    onCancelUploadStep={this.handleCancelUploadStep}
                    onNextUploadStep={this.handleNextUploadStep}
                    onUploadCancelClick={this.handleUploadCancelClick}
                    onDropdownChange={this.handleDropdownChange}
                    onSuggestTagClick={this.handleSuggestTagClick}
                    tags={this.props.tags}
                    temporaryAlbumTags={this.state.temporaryAlbumTags}
                />
                <ChoosePreviousSongsModal
                    onChoiceClick={this.handleModalChoiceClick}
                />
                <SuggestTagModal
                    onTagSubmit={this.handleSuggestTagSubmit}
                    musicItem={musicEdit.info}
                />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        musicEdit: state.musicEdit,
        uploadAlbum: state.uploadAlbum,
        currentUser: state.currentUser,
        currentUserUploads: state.currentUserUploads,
        tags: state.musicTags
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps),
    hideSidebarForComponent
)(UploadAlbumsPageContainer);
