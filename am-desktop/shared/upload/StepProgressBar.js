import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './StepProgressBar.module.scss';

class StepProgressBar extends Component {
    static propTypes = {
        className: PropTypes.string,
        stepClassName: PropTypes.string,
        backgroundColor: PropTypes.string.isRequired,
        currentStep: PropTypes.number.isRequired,
        maxVisitedStep: PropTypes.number.isRequired,
        stepList: PropTypes.array.isRequired,
        basePath: PropTypes.string,
        location: PropTypes.object,
        history: PropTypes.object,
        historyMethod: PropTypes.oneOf(['replace', 'push']),
        onStepClick: PropTypes.func
    };

    static defaultProps = {
        historyMethod: 'replace'
    };

    componentDidMount() {
        const fromMount = true;

        this.updateUrl(fromMount);
    }

    componentDidUpdate(prevProps) {
        if (this.props.currentStep !== prevProps.currentStep) {
            this.updateUrl();
        }
    }

    updateUrl(fromMount) {
        const {
            basePath,
            currentStep,
            history,
            historyMethod,
            location
        } = this.props;

        if (!basePath || !history) {
            return;
        }

        const method = fromMount ? 'replace' : historyMethod;

        history[method](`/${basePath}/steps/${currentStep}${location.search}`);
    }

    render() {
        const {
            backgroundColor,
            currentStep,
            maxVisitedStep,
            onStepClick,
            stepList,
            className: klass,
            stepClassName
        } = this.props;

        const containerClass = classnames(styles.bar, {
            [klass]: klass
        });
        const steps = stepList.map((name, index) => {
            const number = index + 1;
            const clickable = onStepClick && number <= maxVisitedStep;
            const className = classnames(styles.step, styles[backgroundColor], {
                [styles.clickable]: clickable,
                [styles.stepActive]: number === currentStep,
                [styles.stepComplete]: number <= maxVisitedStep,
                [stepClassName]: stepClassName
            });

            return (
                <button
                    className={className}
                    data-step-number={number}
                    key={index}
                    onClick={onStepClick}
                    type="button"
                    tabIndex={clickable ? 0 : -1}
                    disabled={!clickable}
                >
                    {name}
                </button>
            );
        });

        return <div className={containerClass}>{steps}</div>;
    }
}

export default withRouter(StepProgressBar);
