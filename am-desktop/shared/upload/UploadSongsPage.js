import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { eventLabel } from 'utils/analytics';

import { UPLOAD_STATUS } from 'constants/index';

import AndroidLoader from '../loaders/AndroidLoader';

import SongUploadContainer from './SongUploadContainer';
import UploadForm from './UploadForm';
import UploadItem from './UploadItem';
import UploadMaintenance from './UploadMaintenance';
import TunecoreAd from '../components/Tunecore';

import styles from './UploadSongsPage.module.scss';

export default class UploadSongsPage extends Component {
    static propTypes = {
        inProgress: PropTypes.bool,
        upload: PropTypes.object,
        uploadSong: PropTypes.object,
        currentUser: PropTypes.object,
        onSaveClick: PropTypes.func,
        onCancelClick: PropTypes.func,
        fromMobile: PropTypes.bool,
        uploadType: PropTypes.string
    };

    handleReset = (e) => {
        e.preventDefault();
    };

    handleSubmit = (e) => {
        e.preventDefault();
    };

    getUploadList() {
        const { upload } = this.props;

        return upload.list.reduce(
            (acc, item) => {
                if (!!item.replaceId) {
                    return acc;
                } else if (item.status === UPLOAD_STATUS.FAILED) {
                    acc.failed.push(item);
                } else if (item.status === UPLOAD_STATUS.CANCELED) {
                    acc.canceled.push(item);
                } else if (item.status !== UPLOAD_STATUS.COMPLETED) {
                    acc.nonFinished.push(item);
                } else if (item.status === UPLOAD_STATUS.COMPLETED) {
                    acc.completed.push(item);
                }

                return acc;
            },
            { canceled: [], completed: [], failed: [], nonFinished: [] }
        );
    }

    renderHeader() {
        const { inProgress, uploadType } = this.props;
        const list = this.getUploadList();
        const { completed, failed, nonFinished } = list;
        const uploads = [...completed, ...failed, ...nonFinished];

        let boldSubtitle;
        let title;
        let subtitle;

        if (!uploads.length) {
            title = 'Upload your music to Audiomack!';
            subtitle = 'Audiomack gives you unlimited storage, for free.';

            if (uploadType === 'podcast') {
                title = 'Upload your podcast to Audiomack!';
            }
        } else if (nonFinished.length) {
            const count = completed.length + nonFinished.length;
            const s = count !== 1 ? 's' : '';

            title = 'Your music is uploading...';
            boldSubtitle = `${count} Upload${s}`;
            subtitle = `(${nonFinished.length} in progress, ${
                completed.length
            } complete)`;
        } else if (!inProgress) {
            title = 'Your music has uploaded!';
            subtitle = 'Follow these steps to complete your upload';
        }

        return (
            <header className={styles.header}>
                <h1 className={styles.title}>{title}</h1>
                <span className={styles.subtitle}>
                    <span className={styles.bold}>{boldSubtitle} </span>
                    {subtitle}
                </span>
            </header>
        );
    }

    renderTunecoreAd() {
        return (
            <div className="row" style={{ maxWidth: 940 }}>
                <div className="column">
                    <TunecoreAd
                        className="u-text-center u-spacing-top"
                        linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=%20all_afpup%20_su&utm_content=ghoammot"
                        eventLabel={eventLabel.songUpload}
                        variant="condensed"
                    />
                </div>
            </div>
        );
    }

    renderSongUploads() {
        const {
            onCancelClick,
            onSaveClick,
            currentUser,
            uploadSong
        } = this.props;

        const uploadedTracks = uploadSong.list || [];

        const list = this.getUploadList();
        const { completed, failed, nonFinished } = list;
        const uploads = [...completed, ...failed, ...nonFinished] || [];

        if (!uploads.length) {
            return null;
        }

        const uploadList = uploadedTracks
            .filter((track) => {
                return !track.isDeleting;
            })
            .map((uploadItem) => {
                return (
                    <li className={styles.songUploadItem} key={uploadItem.id}>
                        <SongUploadContainer
                            onSaveClick={onSaveClick}
                            musicItem={uploadItem}
                            isEditing={false}
                            fromMobile={this.props.fromMobile}
                            uploadPage={true}
                        />
                    </li>
                );
            });

        const uploadingAndFailed = nonFinished
            .concat(failed)
            .map((uploadItem) => {
                return (
                    <UploadItem
                        key={uploadItem.id}
                        currentUser={currentUser}
                        uploadItem={uploadItem}
                        onCancelClick={onCancelClick}
                        uploadPage={true}
                    />
                );
            });

        let uploading;

        if (uploadingAndFailed.length) {
            uploading = <div>{uploadingAndFailed}</div>;
        }

        let copy;

        if (uploadList.length) {
            copy = (
                <p className={styles.copy}>
                    By uploading to Audiomack, you agree to our Terms of
                    Service. DO NOT upload any music you don't own the
                    <br />
                    copyrights to or have explicit permission to distribute.
                    Offending accounts will be deleted and permanently banned.
                </p>
            );
        }

        return (
            <ul className={styles.songUploadList}>
                {uploadList}
                {uploading}
                {copy}
            </ul>
        );
    }

    renderUploadForm() {
        const { upload, uploadType } = this.props;
        const { loading, policyError } = upload;

        if (loading) {
            return <AndroidLoader />;
        }

        return (
            <UploadForm
                error={policyError}
                headerText="Browse for files or drag and drop them here"
                inputId="file-input"
                inputType="audio/*,application/zip"
                multiple
                uploadType={uploadType}
                withForm={false}
            />
        );
    }

    render() {
        if (process.env.MAINTENANCE_MODE === 'true') {
            return <UploadMaintenance />;
        }

        return (
            <Fragment>
                <form onReset={this.handleReset} onSubmit={this.handleSubmit}>
                    {this.renderHeader()}
                    {this.renderSongUploads()}
                    {this.renderUploadForm()}
                    {this.renderTunecoreAd()}
                </form>
            </Fragment>
        );
    }
}
