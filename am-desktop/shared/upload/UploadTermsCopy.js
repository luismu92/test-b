import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class UploadTermsCopy extends Component {
    render() {
        return (
            <p className="upload-intro__tos">
                Uploading constitutes your acceptance of our{' '}
                <Link to="/about/terms-of-service">Terms of Service</Link> and{' '}
                <Link to="/about/privacy-policy">Privacy Policy</Link>.
                Uploading music is reserved for content creators only. DO NOT
                upload any content which infringes on the rights of 3rd parties
                - offending accounts will be blocked and banned immediately.
            </p>
        );
    }
}
