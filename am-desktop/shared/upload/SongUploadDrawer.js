import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import moment from 'moment';

import {
    getTwitterShareLink,
    getFacebookShareLink,
    getMusicUrl,
    yesBool,
    buildDynamicImage
} from 'utils/index';
import { getTagOptions, getUserTags } from 'utils/tags';
import { allGenresMap, GENRE_TYPE_OTHER, tagSections } from 'constants/index';

import ArrowNext from '../icons/arrow-next';
import FacebookIcon from '../icons/facebook-letter-logo';
import TwitterIcon from '../icons/twitter-logo-new';

import AddArtwork from '../components/AddArtwork';
import Dropdown from '../components/Dropdown';
import LabeledInput, {
    inputTypes as labeledInputTypes
} from '../components/LabeledInput';
import ReleaseOptions from '../components/ReleaseOptions';
import StepProgressBar from '../upload/StepProgressBar';
// import TagListDropdown from '../components/TagListDropdown';

import styles from './SongUploadDrawer.module.scss';

export default class SongUploadDrawer extends Component {
    static propTypes = {
        uploadItem: PropTypes.object,
        uploadSteps: PropTypes.array,
        // moment date
        releaseDate: PropTypes.object,
        errors: PropTypes.object,
        descriptionCharsLeft: PropTypes.number,
        className: PropTypes.string,
        currentUser: PropTypes.object,
        history: PropTypes.object,
        onFinishClick: PropTypes.func,
        autosave: PropTypes.bool,
        dirty: PropTypes.bool,
        songDescriptionMaxChars: PropTypes.number,
        songAdminDescriptionMaxChars: PropTypes.number,
        adminDescriptionCharsLeft: PropTypes.number,
        saveButtonClicked: PropTypes.bool,
        selectedReleaseOption: PropTypes.string,
        isAutoSaving: PropTypes.bool,
        futureReleaseDate: PropTypes.bool,
        onReleaseOptionChange: PropTypes.func,
        onDropdownChange: PropTypes.func,
        onInputChange: PropTypes.func,
        onDateBlur: PropTypes.func,
        onDateChange: PropTypes.func,
        onFileInputChange: PropTypes.func,
        onFinishSocialClick: PropTypes.func,
        showErrorMessage: PropTypes.bool,
        fromMobile: PropTypes.bool,
        onCopyClick: PropTypes.func,
        onPrivateShareClick: PropTypes.func,
        onSuggestTagClick: PropTypes.func,
        onViewSongClick: PropTypes.func,
        newUrlSlug: PropTypes.object,
        uploadPage: PropTypes.bool,
        tags: PropTypes.object,
        addMusicTag: PropTypes.func,
        removeMusicTag: PropTypes.func,
        showTagError: PropTypes.func
    };

    static defaultProps = {
        uploadSteps: [
            {
                label: 'Basic Information',
                name: 'Basic Info',
                number: 1
            },
            {
                label: 'Enter Your Metadata',
                name: 'Metadata',
                number: 2
            },
            {
                label: 'Choose Your Release',
                name: 'Release',
                number: 3
            },
            {
                label: 'Your upload is complete',
                name: 'Finish',
                number: 4
            }
        ]
    };

    constructor(props) {
        super(props);

        this.state = {
            currentUploadStep: props.uploadSteps[0],
            isSaving: false,
            maxVisitedUploadStep:
                props.uploadPage && !props.uploadItem.alreadyExists
                    ? props.uploadSteps[0].number
                    : 3,
            errrors: {}
        };
    }

    ////////////////////
    // Event Handlers //
    ////////////////////

    handleNextUploadStep = (e) => {
        e.preventDefault();

        const {
            onFinishClick,
            onViewSongClick,
            uploadPage,
            uploadSteps
        } = this.props;
        const { currentUploadStep, maxVisitedUploadStep } = this.state;

        // the zero-based index for the next upload step is equal to the one-based index of the current upload step number
        const nextIndex = currentUploadStep.number;

        switch (currentUploadStep.number) {
            case 1:
            case 2:
                this.setState({
                    currentUploadStep: uploadSteps[nextIndex]
                });

                break;

            case 3:
                if (uploadPage) {
                    this.setState({
                        currentUploadStep: uploadSteps[nextIndex]
                    });
                }

                onFinishClick(e);

                break;

            case 4:
                onViewSongClick(e);

                break;

            default:
                break;
        }

        if (
            uploadSteps[nextIndex] &&
            uploadSteps[nextIndex].number > maxVisitedUploadStep
        ) {
            this.setState({
                maxVisitedUploadStep: uploadSteps[nextIndex].number
            });
        }
    };

    handleUploadStepClick = (e) => {
        e.preventDefault();

        const { uploadSteps } = this.props;
        const { currentUploadStep, maxVisitedUploadStep } = this.state;
        const stepNumber = e.currentTarget.getAttribute('data-step-number');
        const index = parseInt(stepNumber, 10) - 1;

        if (
            stepNumber <= maxVisitedUploadStep &&
            stepNumber !== currentUploadStep.number
        ) {
            this.setState({
                currentUploadStep: uploadSteps[index]
            });
        }
    };

    handleSaveButtonClick = (e) => {
        e.persist();

        const { onFinishClick } = this.props;

        this.setState({
            isSaving: true
        });

        onFinishClick(e);
    };

    renderShareUpload(uploadItem) {
        const shareOn = ['twitter', 'facebook'].map((item, i) => {
            let shareButton = (
                <a
                    href={getTwitterShareLink(process.env.AM_URL, uploadItem)}
                    className="social-button social-button--twitter upload-complete__social-button"
                    target="_blank"
                    rel="nofollow noopener"
                    data-social-type="twitter"
                    onClick={this.props.onFinishSocialClick}
                >
                    <TwitterIcon className="u-text-icon social-button__icon" />{' '}
                    Share on Twitter
                </a>
            );

            if (item === 'facebook') {
                shareButton = (
                    <a
                        href={getFacebookShareLink(
                            process.env.AM_URL,
                            uploadItem,
                            process.env.FACEBOOK_APP_ID
                        )}
                        className="social-button social-button--facebook upload-complete__social-button"
                        target="_blank"
                        rel="nofollow noopener"
                        data-social-type="facebook"
                        onClick={this.props.onFinishSocialClick}
                    >
                        <FacebookIcon className="u-text-icon social-button__icon" />{' '}
                        Share on Facebook
                    </a>
                );
            }

            let headingIcon = <TwitterIcon />;

            if (item === 'facebook') {
                headingIcon = <FacebookIcon />;
            }

            const artwork = buildDynamicImage(uploadItem.image, {
                width: 125,
                height: 125,
                max: true
            });

            const retinaArtwork = buildDynamicImage(uploadItem.image, {
                width: 250,
                height: 250,
                max: true
            });

            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            const image = <img src={artwork} srcSet={srcSet} alt="" />;

            return (
                <div key={i} className="upload-share-wrap">
                    <h3 className="upload-share__heading u-fs-18 u-lh-24 u-ls-n-05 u-fw-700 u-spacing-bottom-10">
                        <span
                            className={`upload-share__heading-icon upload-share__heading-icon--${item}`}
                        >
                            {headingIcon}
                        </span>
                        Share on <span className="u-tt-cap">{item}</span>
                    </h3>

                    <div className={`upload-share upload-share--${item}`}>
                        <div className="upload-share__image">{image}</div>
                        <div className="upload-share__details u-padding-x-10">
                            <p className="upload-share__top-am u-fs-13 u-lh-22 u-tt-uppercase">
                                Audiomack.com
                            </p>
                            <h3 className="upload-share__title u-fs-15 u-lh-22 u-ls-n-05 u-fw-600 u-trunc">
                                {uploadItem.artist} - {uploadItem.title}
                            </h3>
                            <p className="upload-share__desc u-fs-13 u-ls-n-025">
                                Listen to {uploadItem.title}, the new{' '}
                                {uploadItem.type} from{' '}
                                {uploadItem.uploader.name}
                            </p>
                            <p className="upload-share__bottom-am u-fs-14 u-fw-600 u-lh-22 u-ls-n-05 u-text-gray8">
                                audiomack.com
                            </p>
                        </div>
                    </div>
                    <div className="u-spacing-top-10">{shareButton}</div>
                </div>
            );
        });

        return (
            <div className="u-padding-y-20 u-text-left share-upload">
                {shareOn}
            </div>
        );
    }

    renderSavedPrivateScreen(uploadItem) {
        if (!yesBool(uploadItem.private)) {
            return null;
        }

        const { fromMobile } = this.props;

        const link = getMusicUrl(uploadItem, {
            host: process.env.AM_URL
        });
        const finishLinkClass =
            'upload-complete__track-info upload-complete__track-info--link u-trunc u-d-inline-block';

        let finishLink = (
            <Link
                className={finishLinkClass}
                to={`${link.split(process.env.AM_URL)[1]}`}
            >
                {link}
            </Link>
        );

        if (fromMobile) {
            finishLink = (
                <a
                    className={finishLinkClass}
                    href={`${link.split(process.env.AM_URL)[1]}`}
                >
                    {link}
                </a>
            );
        }

        return (
            <div className="u-text-center">
                <div className="upload-complete__col-inner">
                    <p className="upload-complete__title">
                        Your upload is set to private!
                    </p>
                    <div>
                        {finishLink}
                        <div className="upload-complete__promo-link u-spacing-top-15 u-padding-20 u-padding-bottom-30 u-bg-gray11">
                            <h3 className="u-fs-16 u-ls-n-05">
                                Share your music privately with a custom link.
                            </h3>
                            <p className="u-spacing-bottom-10 u-padding-x-60 u-ls-n-03 u-lh-14">
                                Allow select people to listen to any private
                                song by creating a custom private link.
                            </p>
                            <div className="u-spacing-top-5">
                                <button
                                    className="button button--large u-fs-14 upload-complete__promo-button"
                                    onClick={this.props.onPrivateShareClick}
                                >
                                    Create
                                    <ArrowNext className="upload-complete__promo-button-icon" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderBasicInfo() {
        const {
            errors,
            onDropdownChange,
            onFileInputChange,
            onInputChange,
            onSuggestTagClick,
            uploadItem
        } = this.props;

        const mapper = (object) =>
            Object.entries(object).map(([key, value]) => ({
                value: key,
                text: value
            }));

        const genres = mapper(allGenresMap);
        const tagOptions = getTagOptions(this.props.tags);
        const userTags = getUserTags(this.props.tags.id[uploadItem.id]);

        return (
            <Fragment>
                <section className={styles.containerRow}>
                    <AddArtwork
                        image={uploadItem.image}
                        onChange={onFileInputChange}
                        type="song"
                    />
                    <div
                        className={classnames(
                            styles.inputColumn,
                            styles.metadataColumn
                        )}
                    >
                        <LabeledInput
                            data-testid="songArtist"
                            error={errors.artist}
                            id={`artist-${uploadItem.id}`}
                            label="Artist"
                            maxLength={75}
                            name="artist"
                            onChange={onInputChange}
                            type={labeledInputTypes.text}
                            value={uploadItem.artist}
                            required
                        />
                        <LabeledInput
                            data-testid="songTitle"
                            error={errors.title}
                            id={`title-${uploadItem.id}`}
                            label="Song Title"
                            maxLength={75}
                            name="title"
                            onChange={onInputChange}
                            type={labeledInputTypes.text}
                            value={uploadItem.title}
                            required
                        />
                        <div className={styles.inputRow}>
                            <LabeledInput
                                id={`featuring-${uploadItem.id}`}
                                label="Featuring"
                                name="featuring"
                                onChange={onInputChange}
                                subtitle="(Type the artists and hit the enter button)"
                                type={labeledInputTypes.tags}
                                value={uploadItem.featuring || ''}
                            />

                            <LabeledInput
                                id={`producer-${uploadItem.id}`}
                                label="Producer(s)"
                                name="producer"
                                onChange={onInputChange}
                                type={labeledInputTypes.tags}
                                value={uploadItem.producer || ''}
                            />
                        </div>
                    </div>
                </section>

                <div className={styles.dropdown}>
                    <LabeledInput
                        data-testid="songGenre"
                        error={errors.genre}
                        id={`genre-${uploadItem.id}`}
                        label="Genre"
                        name="genre"
                        onChange={onDropdownChange}
                        options={genres}
                        type={labeledInputTypes.dropdown}
                        value={
                            uploadItem.genre !== GENRE_TYPE_OTHER
                                ? uploadItem.genre
                                : ''
                        }
                        required
                    />
                </div>

                <div className={styles.tagsLabelRow}>
                    <span className={styles.tagsLabel}>Tags</span>

                    <span className={styles.suggestTag}>
                        Want to{' '}
                        <button
                            className="button-link"
                            onClick={onSuggestTagClick}
                        >
                            suggest a new tag?
                        </button>
                    </span>
                </div>

                <div className={styles.inputRow}>
                    <div className={styles.dropdown}>
                        <Dropdown
                            id={`subgenres-${uploadItem.id}`}
                            multiple
                            name={tagSections.subgenre}
                            onChange={onDropdownChange}
                            options={tagOptions.subgenres}
                            sections
                            showDeselectButton
                            type="tag"
                            value={userTags.subgenres}
                        />
                    </div>

                    <div className={styles.dropdown}>
                        <Dropdown
                            id={`mood-${uploadItem.id}`}
                            multiple
                            name={tagSections.mood}
                            onChange={onDropdownChange}
                            options={tagOptions.moods}
                            showDeselectButton
                            type="tag"
                            value={userTags.moods}
                        />
                    </div>

                    {/*
                    <TagListDropdown
                        inputProps={{
                            id: `locations-${uploadItem.id}`,
                            name: tagSections.locations
                        }}
                        nestedOptionDelimiter="->"
                        onDropdownChange={this.props.onDropdownChange}
                        options={tagOptions.locations}
                        tags={userTags.locations}
                    />
                    */}
                </div>

                <LabeledInput
                    checked={yesBool(uploadItem.explicit)}
                    error={errors.explicit}
                    id={`explicit-${uploadItem.id}`}
                    label="This song is explicit"
                    name="explicit"
                    onChange={onInputChange}
                    type={labeledInputTypes.checkbox}
                />
            </Fragment>
        );
    }

    renderMetadata() {
        const { currentUser, onInputChange, uploadItem } = this.props;
        const disabledAmp = ['yes-disabled', 'no-disabled'].includes(
            currentUser.profile.video_ads
        );
        let monetization = (
            <div className={styles.inputRow}>
                <LabeledInput
                    id={`upc-${uploadItem.id}`}
                    label="UPC"
                    name="upc"
                    onChange={onInputChange}
                    tooltip="Enter UPC code if this item has one"
                    type={labeledInputTypes.text}
                    value={uploadItem.upc}
                />

                <LabeledInput
                    id={`isrc-${uploadItem.id}`}
                    label="ISRC"
                    name="isrc"
                    onChange={onInputChange}
                    tooltip="Enter ISRC code if this item has one"
                    type={labeledInputTypes.text}
                    value={uploadItem.isrc}
                />
            </div>
        );

        if (uploadItem.amp) {
            monetization = (
                <Fragment>
                    <LabeledInput
                        checked={yesBool(uploadItem.video_ad)}
                        disabled={disabledAmp}
                        id={`monetize-${uploadItem.id}`}
                        label="Monetize this song"
                        name="video_ad"
                        onChange={onInputChange}
                        type={labeledInputTypes.toggle}
                        value={currentUser.profile.label_name}
                    />

                    <div className={styles.inputRow}>
                        <LabeledInput
                            id={`upc-${uploadItem.id}`}
                            label="UPC"
                            name="upc"
                            onChange={onInputChange}
                            tooltip="Enter UPC code if this item has one"
                            type={labeledInputTypes.text}
                            value={uploadItem.upc}
                        />

                        <LabeledInput
                            id={`isrc-${uploadItem.id}`}
                            label="ISRC"
                            name="isrc"
                            onChange={onInputChange}
                            tooltip="Enter ISRC code if this item has one"
                            type={labeledInputTypes.text}
                            value={uploadItem.isrc}
                        />

                        <LabeledInput
                            id={`accounting_code-${uploadItem.id}`}
                            label="Accounting code"
                            name="accounting_code"
                            onChange={onInputChange}
                            tooltip="Optional. You can use this to enter a this items code in your accounting system."
                            type={labeledInputTypes.text}
                            value={uploadItem.accounting_code}
                        />
                    </div>
                </Fragment>
            );
        }

        return (
            <Fragment>
                <section className={styles.inputRow}>
                    <LabeledInput
                        id={`description-${uploadItem.id}`}
                        label="Song Description"
                        maxLength={800}
                        name="description"
                        onChange={onInputChange}
                        type={labeledInputTypes.textarea}
                        value={uploadItem.description}
                    />
                    <div className={styles.inputColumn}>
                        <LabeledInput
                            id={`album-${uploadItem.id}`}
                            label="Album"
                            name="album"
                            onChange={onInputChange}
                            type={labeledInputTypes.text}
                            value={uploadItem.album}
                        />
                        <LabeledInput
                            id={`video-${uploadItem.id}`}
                            label="YouTube / Vimeo URL"
                            subtitle="(Note: Video URLs only. This entry will link to a music video or song promo video of your choice.)"
                            name="video"
                            onChange={onInputChange}
                            type={labeledInputTypes.text}
                            value={uploadItem.video}
                        />
                    </div>
                </section>
                {monetization}
            </Fragment>
        );
    }

    renderRelease() {
        const {
            currentUser,
            onDateBlur,
            onDateChange,
            onInputChange,
            onReleaseOptionChange,
            futureReleaseDate,
            releaseDate,
            selectedReleaseOption,
            uploadItem
        } = this.props;
        const errors = this.props.errors[uploadItem.id] || {};
        let currentDate =
            releaseDate ||
            (uploadItem.released
                ? moment(uploadItem.released * 1000)
                : moment());

        if (!currentDate.isValid()) {
            currentDate = moment().utcOffset('-400');
        }

        return (
            <Fragment>
                <ReleaseOptions
                    currentDate={currentDate}
                    selectedReleaseOption={selectedReleaseOption}
                    futureReleaseDate={futureReleaseDate}
                    onDateBlur={onDateBlur}
                    onDateChange={onDateChange}
                    onReleaseOptionChange={onReleaseOptionChange}
                />

                <div className={styles.linkInputsWrapper}>
                    <LabeledInput
                        id={`url_slug-${uploadItem.id}`}
                        error={errors.url_slug}
                        label="Song URL"
                        name="url_slug"
                        onChange={onInputChange}
                        prepend={`${process.env.AM_URL.split('//')[1]}/song/${
                            currentUser.profile.url_slug
                        }/`}
                        type={labeledInputTypes.slug}
                        value={uploadItem.url_slug || ''}
                    />

                    <LabeledInput
                        id={`buy_link-${uploadItem.id}`}
                        error={errors.buy_link}
                        label="Buy / Custom Link URL"
                        name="buy_link"
                        onChange={onInputChange}
                        tooltip="You can enter a URL for a 3rd party site or service (iTunes, Spotify, HypeMachine, etc) here."
                        type={labeledInputTypes.url}
                        value={uploadItem.buy_link || ''}
                    />
                </div>
            </Fragment>
        );
    }

    renderFinish() {
        const { onCopyClick, uploadItem } = this.props;
        const link = getMusicUrl(uploadItem, {
            host: process.env.AM_URL
        });

        return (
            <Fragment>
                <div className={styles.copyLink}>
                    <button className="button-link" onClick={onCopyClick}>
                        {link}
                    </button>
                    <p>
                        Click to copy the URL to your clipboard. Please allow
                        1-2 minutes for your content to appear on your profile.
                    </p>
                </div>
                <section className={styles.social}>
                    {this.renderShareUpload(uploadItem)}
                </section>
            </Fragment>
        );
    }

    renderHeader() {
        const { history, uploadItem, uploadPage, uploadSteps } = this.props;
        const { currentUploadStep, maxVisitedUploadStep } = this.state;
        const { label, number } = currentUploadStep;
        const stepList = uploadSteps.map((uploadStep) => uploadStep.name);

        return (
            <header className={styles.header}>
                <h1 className={styles.title}>
                    Step {number}:{' '}
                    <span className={styles.subtitle}>{label}</span>
                </h1>
                <StepProgressBar
                    backgroundColor="white"
                    basePath={uploadPage ? null : `edit/song/${uploadItem.id}`}
                    currentStep={number}
                    history={history}
                    maxVisitedStep={maxVisitedUploadStep}
                    onStepClick={this.handleUploadStepClick}
                    stepList={stepList}
                />
            </header>
        );
    }

    renderUploadStep() {
        const { currentUploadStep } = this.state;

        switch (currentUploadStep.number) {
            case 1:
                return this.renderBasicInfo();

            case 2:
                return this.renderMetadata();

            case 3:
                return this.renderRelease();

            case 4:
                return this.renderFinish();

            default:
                return null;
        }
    }

    renderFooter() {
        const { uploadItem, uploadPage } = this.props;
        const { currentUploadStep, isSaving } = this.state;
        const saveButtonText = isSaving ? 'Saving...' : 'Save & Exit';

        let arrowIcon = <ArrowNext className={styles.arrowIcon} />;
        let disableNextButton;
        let disableSaveButton;
        let nextButtonText = 'Next Step';
        let saveButton;

        let previousButton = (
            <button
                className={styles.previousButton}
                data-step-number={currentUploadStep.number - 1}
                data-testid="previousUploadStep"
                onClick={this.handleUploadStepClick}
            >
                Back
            </button>
        );

        if (!uploadPage) {
            disableSaveButton =
                !uploadItem.artist ||
                !uploadItem.genre ||
                uploadItem.genre === GENRE_TYPE_OTHER ||
                !uploadItem.title;

            disableNextButton = disableSaveButton;

            saveButton = (
                <button
                    className={styles.saveButton}
                    disabled={disableSaveButton}
                    onClick={this.handleSaveButtonClick}
                >
                    {saveButtonText}
                </button>
            );
        }

        switch (currentUploadStep.number) {
            case 1:
                disableNextButton =
                    !uploadItem.artist ||
                    !uploadItem.genre ||
                    uploadItem.genre === GENRE_TYPE_OTHER ||
                    !uploadItem.title;

                previousButton = null;

                break;

            case 3:
                nextButtonText = 'Finish';

                break;

            case 4:
                arrowIcon = null;
                nextButtonText = 'VIEW SONG';
                previousButton = null;

                break;

            default:
                break;
        }

        return (
            <footer className={styles.footer}>
                {previousButton}
                <button
                    className={classnames(
                        styles.nextButton,
                        'button',
                        'button--pill',
                        'button--med'
                    )}
                    data-testid="nextUploadStep"
                    disabled={disableNextButton}
                    onClick={this.handleNextUploadStep}
                    type="submit"
                >
                    <span>{nextButtonText}</span>
                    {arrowIcon}
                </button>
                {saveButton}
            </footer>
        );
    }

    render() {
        const { uploadItem } = this.props;
        const { currentUploadStep } = this.state;

        if (currentUploadStep.number === 4 && yesBool(uploadItem.private)) {
            return (
                <section className={styles.savedScreen}>
                    {this.renderHeader()}
                    {this.renderSavedPrivateScreen(uploadItem)}
                </section>
            );
        }

        return (
            <section className={styles.content}>
                {this.renderHeader()}
                {this.renderUploadStep()}
                {this.renderFooter()}
            </section>
        );
    }
}
