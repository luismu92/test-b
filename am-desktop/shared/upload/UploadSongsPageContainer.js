import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';

import { yesBool } from 'utils/index';
import analytics, {
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';
import { UPLOAD_STATUS } from 'constants/index'; // eslint-disable-line no-unused-vars

import requireAuth from '../hoc/requireAuth';

import {
    reset as resetUploadList,
    cancelUpload
} from '../redux/modules/upload';
import { reset as resetSongUpload } from '../redux/modules/upload/song';
import hideSidebarForComponent from '../hoc/hideSidebarForComponent';
import UploadSongsPage from './UploadSongsPage';

class UploadSongsPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        location: PropTypes.object,
        upload: PropTypes.object,
        uploadSong: PropTypes.object,
        uploader: PropTypes.object,
        uploadType: PropTypes.string
    };

    static defaultProps = {
        uploadType: 'song'
    };

    constructor(props) {
        super(props);

        this.state = {
            fromMobile: !!parse(props.location.search).fromMobile
        };
    }

    componentDidMount() {
        analytics.track(eventCategory.upload, {
            eventLabel: eventLabel.verifiedEmail,
            eventValue: yesBool(this.props.currentUser.profile.verified_email)
                ? 1
                : 0
        });
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(resetUploadList());
        dispatch(resetSongUpload());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCancelClick = (e) => {
        const id = e.currentTarget.getAttribute('data-upload-id');

        const { dispatch } = this.props;

        dispatch(cancelUpload(id));
    };

    handleSaveClick = () => {
        analytics.track(
            eventCategory.upload,
            {
                eventAction: eventAction.songUploadSave
            },
            ['ga', 'fb']
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { upload, currentUser, uploadSong, uploadType } = this.props;

        // Use these to test different upload states

        // uploadSong.list = [
        //     {
        //         title: 'Chainsmoker ish',
        //         bytesUploaded: 4201231,
        //         bytesTotal: 4201231,
        //         errors: [],
        //         id: 1,
        //         status: UPLOAD_STATUS.COMPLETED,
        //         released: Date.now(),
        //         video_ad: 'yes',
        //         url_slug: 'some-shit' // eslint-disable-line
        //     },
        //     {
        //         title: 'Another song',
        //         bytesUploaded: 4201231,
        //         bytesTotal: 4201231,
        //         errors: [],
        //         id: 2,
        //         isSaving: true,
        //         status: UPLOAD_STATUS.COMPLETED,
        //         released: Date.now(),
        //         video_ad: 'yes',
        //         url_slug: 'some-shit2' // eslint-disable-line
        //     },
        //     {
        //         title: 'Other song',
        //         bytesUploaded: 4201231,
        //         bytesTotal: 4201231,
        //         errors: [],
        //         id: 3,
        //         type: 'song',
        //         uploader: {
        //             url_slug: 'artist-slug'
        //         },
        //         image: 'https://assets.audiomack.com/hustle-hearted/bodak-yellow-275-275-1497641605.jpg',
        //         url_slug: 'other-song',
        //         saved: true,
        //         status: UPLOAD_STATUS.COMPLETED,
        //         released: Date.now(),
        //         video_ad: 'yes',
        //         url_slug: 'some-shit2' // eslint-disable-line
        //     }
        // ];

        // upload.list = [
        //     {
        //         title: 'Dancing in the rain',
        //         bytesUploaded: 0,
        //         bytesTotal: 5201231,
        //         errors: [],
        //         id: 4,
        //         status: UPLOAD_STATUS.SUBMITTING
        //     },
        //     {
        //         title: 'Dancing in the rain',
        //         bytesUploaded: 1201231,
        //         bytesTotal: 5201231,
        //         errors: [],
        //         id: 5,
        //         status: UPLOAD_STATUS.UPLOADING
        //     },
        //     {
        //         title: 'Dancing in the rain',
        //         bytesUploaded: 1201231,
        //         bytesTotal: 5201231,
        //         errors: [],
        //         id: 6,
        //         status: UPLOAD_STATUS.CANCELED
        //     },
        //     {
        //         title: 'Some other ish',
        //         bytesUploaded: 3201231,
        //         bytesTotal: 4501231,
        //         id: 7,
        //         errors: ['Some error message'],
        //         status: UPLOAD_STATUS.FAILED
        //     },
        //     {
        //         title: 'Dancing in the rain',
        //         bytesUploaded: 5201231,
        //         bytesTotal: 5201231,
        //         id: 8,
        //         errors: [],
        //         status: UPLOAD_STATUS.PROCESSING
        //     }
        // ];

        return (
            <UploadSongsPage
                inProgress={upload.inProgress}
                upload={upload}
                uploadSong={uploadSong}
                currentUser={currentUser}
                onCancelClick={this.handleCancelClick}
                onSaveClick={this.handleSaveClick}
                fromMobile={this.state.fromMobile}
                uploadType={uploadType}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        uploadSong: state.uploadSong,
        currentUser: state.currentUser
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps),
    hideSidebarForComponent
)(UploadSongsPageContainer);
