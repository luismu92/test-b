import React, { Component } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { eventLabel } from 'utils/analytics';

import TunecoreAd from '../components/Tunecore';

import MusicIcon from '../icons/music';
import AlbumIcon from '../icons/albums-new';
import PodcastIcon from '../icons/podcast';
import CheckIcon from '../icons/check-mark';

import UploadMaintenance from './UploadMaintenance';

import styles from './UploadStartPage.module.scss';

export default class UploadStartPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        activeUploadType: PropTypes.string,
        mixOverlay: PropTypes.bool,
        onUploadTypeClick: PropTypes.func,
        onAuthLinkClick: PropTypes.func,
        fromMobile: PropTypes.bool
    };

    renderUploadTypes(activeUploadType, mixOverlay, onUploadTypeClick) {
        const options = [
            {
                text: 'Song',
                type: 'song',
                Icon: MusicIcon
            },
            {
                text: 'Album / EP',
                type: 'album',
                Icon: AlbumIcon
            },
            {
                text: 'Podcast',
                type: 'podcast',
                Icon: PodcastIcon
            }
        ];

        return options.map((option, i) => {
            const { type, Icon, text } = option;
            const klass = classnames(styles.option, {
                [styles.selected]: activeUploadType === type
            });

            return (
                <button
                    className={klass}
                    type="button"
                    onClick={onUploadTypeClick}
                    data-type={type}
                    data-testid={type}
                    key={i}
                >
                    <div
                        className={classnames(styles.optionIcon, styles[type])}
                    >
                        <Icon />
                    </div>
                    <div className={styles.optionContent}>
                        <h2 className={styles.optionTitle}>{text}</h2>
                        <span className={styles.optionCheck}>
                            <CheckIcon />
                        </span>
                    </div>
                </button>
            );
        });
    }

    render() {
        const {
            currentUser,
            activeUploadType,
            mixOverlay,
            onUploadTypeClick,
            fromMobile
        } = this.props;

        const urlQuery = fromMobile ? '?fromMobile=1' : '';
        let uploadLink = `/upload/${activeUploadType}s${urlQuery}`;
        let uploadHandler;
        let uploadModal;

        if (activeUploadType === 'podcast') {
            uploadLink = `/upload/${activeUploadType + urlQuery}`;
        }

        if (!currentUser.isLoggedIn) {
            uploadLink = `/login?redirectTo=${encodeURIComponent(uploadLink)}`;
            uploadHandler = this.props.onAuthLinkClick;
            uploadModal = 'login';
        }

        const title = 'Upload Unlimited Music & Podcasts For Free on Audiomack';
        const description =
            'Upload unlimited songs, albums, mixtapes and podcasts for free on Audiomack.';

        if (process.env.MAINTENANCE_MODE === 'true') {
            return <UploadMaintenance />;
        }

        return (
            <form className={styles.form}>
                <Helmet>
                    <title>{title}</title>
                    <meta name="description" content={description} />
                    <meta property="og:title" content={title} />
                    <meta property="og:description" content={description} />
                    <meta name="twitter:title" content={title} />
                    <meta name="twitter:description" content={description} />
                    <meta name="robots" content="noindex" />
                </Helmet>

                <header>
                    <h1 className={styles.title}>
                        Please select your upload type:
                    </h1>
                    <p className={styles.subtitle}>
                        Audiomack gives you unlimited storage, for free.
                    </p>
                </header>

                <main className={styles.optionList}>
                    {this.renderUploadTypes(
                        activeUploadType,
                        mixOverlay,
                        onUploadTypeClick
                    )}
                </main>

                <Link
                    className={styles.button}
                    to={uploadLink}
                    disabled={!activeUploadType}
                    onClick={uploadHandler}
                    data-modal={uploadModal}
                    data-testid="nextUploadStep"
                >
                    Next
                </Link>

                <footer className={styles.copy}>
                    <p>
                        <strong>Supported file types:</strong> Zip, MP3, WAV,
                        AIFF, OGG, FLAC and M4A. <strong>File Size:</strong> Up
                        to 250MB per song.
                    </p>
                    <div className="row" style={{ marginBottom: 30 }}>
                        <div className="column">
                            <TunecoreAd
                                className="u-text-center u-spacing-top"
                                linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=%20all_afpup%20_su&utm_content=ghoammot"
                                eventLabel={eventLabel.songUpload}
                                variant="condensed"
                            />
                        </div>
                    </div>
                    <p>
                        Audiomack uploads are for artists and content creators
                        only. DO NOT upload your personal music collection or
                        unauthorized content. Accounts which upload infringing
                        material will be banned and blocked immediately.
                    </p>
                    <p>
                        <strong>Copyright compliance by</strong>
                        <a
                            href="https://t.sidekickopen07.com/s1t/c/5/f18dQhb0S7lC8dDMPbW2n0x6l2B9nMJW7t5XX43MP9hMW4Y92Qn7fRYjgW7fRL1x56dLX4f3HFJrF02?t=https%3A%2F%2Fwww.acrcloud.com%2F&si=4638617263931392&pi=d08678ff-651e-496a-a340-a4ab3afe4d6d"
                            target="_blank"
                            rel="noindex noopener"
                            className={styles.acr}
                        >
                            <img
                                src="/static/images/desktop/acr-logo.png"
                                srcSet="/static/images/desktop/acr-logo@2x.png 2x"
                                alt="ACR"
                            />
                        </a>
                    </p>
                </footer>
            </form>
        );
    }
}
