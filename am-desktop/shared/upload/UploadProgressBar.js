import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class UploadProgressBar extends Component {
    static propTypes = {
        progress: PropTypes.number.isRequired,
        hasError: PropTypes.bool
    };

    render() {
        const { progress, hasError } = this.props;
        const fill = hasError ? 100 : Math.round(progress * 100);
        const fillStyle = {
            width: `${fill}%`
        };
        const klass = classnames('upload-progress-bar', {
            'upload-progress-bar--failed': hasError
        });
        let tooltip;

        if (fill < 100 && fill > 0 && !hasError) {
            tooltip = (
                <span
                    className="upload-progress-bar__tooltip"
                    data-tooltip={`${fill}%`}
                    data-tooltip-active
                    style={{ left: `${fill}%` }}
                />
            );
        }

        return (
            <div className={klass}>
                {tooltip}
                <div
                    role="progressbar"
                    aria-valuenow={fill}
                    aria-valuemin="0"
                    aria-valuemax="100"
                    className="upload-progress-bar__fill"
                    style={fillStyle}
                />
            </div>
        );
    }
}
