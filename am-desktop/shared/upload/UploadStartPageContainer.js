import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';

import { setDefaultGenre } from '../redux/modules/upload/index';
import { showModal, MODAL_TYPE_AUTH } from '../redux/modules/modal';
import hideSidebarForComponent from '../hoc/hideSidebarForComponent';

import UploadStartPage from './UploadStartPage';

class UploadStartPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            activeUploadType: null,
            mixOverlay: false,
            winWidth: 0,
            winHeight: 0,
            fromMobile: !!parse(props.location.search).fromMobile
        };
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAuthLinkClick = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const link = e.currentTarget;
        const value = link.getAttribute('data-modal');

        // Using window history to not update the content under the modal
        window.history.pushState(null, null, `${link.pathname}${link.search}`);

        dispatch(showModal(MODAL_TYPE_AUTH, { type: value }));
    };

    handleUploadTypeClick = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const type = button.getAttribute('data-type');

        this.setState({
            activeUploadType: type
        });

        if (type === 'mix' && this.state.winWidth > 640) {
            this.setState({
                activeUploadType: null,
                mixOverlay: true
            });
        }

        if (type === 'close') {
            e.stopPropagation();

            this.setState({
                activeUploadType: 'mix',
                mixOverlay: false
            });
        }

        switch (type) {
            case 'podcast':
                dispatch(setDefaultGenre('podcast'));
                break;

            case 'mix':
                dispatch(setDefaultGenre('dj-mix'));
                break;

            default:
                dispatch(setDefaultGenre(null));
                break;
        }
    };

    handleWindowResize = () => {
        this.updateWindowDimensions();
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    updateWindowDimensions() {
        this.setState({
            winWidth: window.innerWidth,
            winHeight: window.innerHeight
        });
    }

    render() {
        return (
            <UploadStartPage
                currentUser={this.props.currentUser}
                activeUploadType={this.state.activeUploadType}
                mixOverlay={this.state.mixOverlay}
                onUploadTypeClick={this.handleUploadTypeClick}
                adSrc={this.state.adSrc}
                adSrcset={this.state.adSrcset}
                adAction={this.state.adAction}
                adLink={this.state.adLink}
                onAuthLinkClick={this.handleAuthLinkClick}
                fromMobile={this.state.fromMobile}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        currentUser: state.currentUser
    };
}

export default compose(
    connect(mapStateToProps),
    hideSidebarForComponent
)(UploadStartPageContainer);
