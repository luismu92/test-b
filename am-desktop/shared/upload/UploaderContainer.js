import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import analytics, { eventCategory, eventAction } from 'utils/analytics';
import { UploadFailedException } from 'utils/errors';
import { formatBytes, canUserUpload, uuid, batchPromises } from 'utils/index';
import { UPLOAD_TYPE, UPLOAD_STATUS } from 'constants/index';

import {
    createEntity,
    failUpload,
    uploadCompleted,
    uploadStarted,
    updateProgress,
    updateTotalProgress,
    addUpload,
    resetFileInput,
    getPresignedUrl
} from '../redux/modules/upload';
import { replaceMusic } from '../redux/modules/music/index';
import {
    addAlbumTrack,
    addPreExistingAlbumTrack
} from '../redux/modules/upload/album';
import { addToast } from '../redux/modules/toastNotification';
import { showModal, MODAL_TYPE_ALERT } from '../redux/modules/modal';

import UploadPlusIcon from '../icons/upload-plus-icon';

const MAX_FILE_UPLOAD_LIMIT = 50;
const MAX_CONCURRENT_UPLOADS = 3;

class UploaderContainer extends Component {
    static propTypes = {
        upload: PropTypes.object,
        uploadAlbum: PropTypes.object,
        musicEdit: PropTypes.object,
        currentUser: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func,
        maxFileSize: PropTypes.number,
        maxZipSize: PropTypes.number
    };

    static defaultProps = {
        maxFileSize: 250 * 1024 * 1024,
        maxZipSize: 500 * 1024 * 1024
    };

    componentDidMount() {
        this._dragEnterCount = 0;

        window.addEventListener('mousedown', this.handleMouseDown);
        window.addEventListener('mouseup', this.handleMouseUp);
        document.addEventListener('dragenter', this.handleDragEnter);
        document.addEventListener('dragleave', this.handleDragLeave);
        document.addEventListener('dragover', this.handleDragOver);
        document.addEventListener('drop', this.handleDragDrop);

        this.reset();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.upload.list.length && !this.props.upload.list.length) {
            this.reset();
        }

        this.checkForCancelations(prevProps, this.props);
        this.checkForFileInputChanges(prevProps, this.props);
    }

    componentWillUnmount() {
        document.body.classList.remove('dragging');
        window.removeEventListener('mousedown', this.handleMouseDown);
        window.removeEventListener('mouseup', this.handleMouseUp);
        document.removeEventListener('dragenter', this.handleDragEnter);
        document.removeEventListener('dragleave', this.handleDragLeave);
        document.removeEventListener('dragover', this.handleDragOver);
        document.removeEventListener('drop', this.handleDragDrop);
        this._isMouseDown = null;
        this._dragEnterCount = null;
        this._uploads = null;

        this._albumTrackQueue = null;
        this._albumTrackQueueProcessCount = null;
        this._albumTrackPromiseChain = null;

        this._processingPreExistingTrackQueue = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleMouseDown = () => {
        this._isMouseDown = true;
    };

    handleMouseUp = () => {
        this._isMouseDown = false;
    };

    handleDragEnter = (e) => {
        e.preventDefault();

        if (this._dragEnterCount === 0 && this.canUpload()) {
            document.body.classList.add('dragging');
        }

        this._dragEnterCount += 1;
    };

    handleDragLeave = () => {
        this._dragEnterCount -= 1;

        if (this._dragEnterCount === 0) {
            document.body.classList.remove('dragging');
        }
    };

    handleDragOver = (e) => {
        e.preventDefault();
    };

    handleDragDrop = (e) => {
        e.preventDefault();

        document.body.classList.remove('dragging');
        this._dragEnterCount = 0;

        if (!this.canUpload()) {
            return;
        }

        this.handleFileSelect(e);
    };

    handleFileSelect = (e) => {
        e.preventDefault();

        const droppedFiles = e.dataTransfer
            ? e.dataTransfer.files
            : e.target.files;

        this.uploadFiles(droppedFiles);
    };

    handleComplete = (id) => {
        let retPromise = Promise.resolve();
        const {
            dispatch,
            location,
            upload: uploadModule,
            musicEdit
        } = this.props;
        const uploadType = this.getUploadTypeForPath(location.pathname);
        // const etag = xhr.getResponseHeader('ETag');
        // const location = xhr.getResponseHeader('Location');
        // const bucket = this._uploader.getBucket(id);
        const uploadObj = this._uploads[id];
        const uploadData = uploadObj.data;
        const key = uploadObj.key;
        const completedUpload = uploadModule.list.find((obj) => obj.id === id);

        // There should only be 1 zip file upload if we've correctly filtered
        // in the beginning of the process
        const zipFileUpload = uploadModule.list.filter((file) => {
            return (
                file.title.substr(-4) === '.zip' &&
                ![UPLOAD_STATUS.CANCELED, UPLOAD_STATUS.FAILED].includes(
                    file.status
                )
            );
        })[0];
        const zipFileUploadExists = !!zipFileUpload;
        const shouldReplaceFile = !!completedUpload.replaceId;
        const isZipUpload =
            zipFileUploadExists && zipFileUpload.id === uploadObj.id;

        if (!uploadObj || completedUpload.status !== UPLOAD_STATUS.UPLOADING) {
            dispatch(failUpload(id));
            return retPromise;
        }

        if (shouldReplaceFile) {
            dispatch(replaceMusic(completedUpload.replaceId, key)).catch(
                (err) => {
                    let message = 'There was an error replacing your music.';

                    if (err.errorcode === 1033) {
                        message = err.message;
                    }

                    dispatch(
                        addToast({
                            action: 'message',
                            message
                        })
                    );
                }
            );
            return retPromise;
        }

        if (uploadType === UPLOAD_TYPE.ALBUM) {
            // We want to wait until the zip file is completed before we let
            // uploads of individual tracks be uploaded
            //
            // We also want to make sure if we are uploading an
            // album via individual tracks, the first track
            // is what creates the album entity while the
            // subsequent tracks are added after.
            //
            // This is something that needs to be cleaned up
            // but may require API updates so we can send all
            // the keys at once instead of one by one.

            if (
                (zipFileUploadExists && isZipUpload) ||
                (!zipFileUploadExists &&
                    uploadData.index === 0 &&
                    // On album edit page and album exists, we want to just add tracks
                    !(musicEdit.info.id && musicEdit.info.tracks.length))
            ) {
                console.warn('Queuing album entity creation');
                retPromise = retPromise.then(() => {
                    return this.queueAlbumTrackCreation(
                        id,
                        createEntity(id, uploadType, key),
                        zipFileUploadExists,
                        isZipUpload
                    );
                });
                console.warn(id, this._albumTrackQueue);
            } else {
                console.warn('Queuing album track addition');
                retPromise = retPromise.then(() => {
                    return this.queueAlbumTrackCreation(
                        id,
                        addAlbumTrack(id, key),
                        zipFileUploadExists,
                        isZipUpload
                    );
                });
                console.warn(id, this._albumTrackQueue);
            }
        } else {
            retPromise = retPromise.then(() => {
                return dispatch(createEntity(id, uploadType, key));
            });
        }

        this.trackUpload(uploadType);

        return retPromise.then(() => {
            const { bytesUploaded, bytesTotal } = this.props.upload;

            if (bytesUploaded === bytesTotal) {
                this.handleAllComplete();
            }
            return;
        });
    };

    handleAllComplete = () => {
        const { dispatch } = this.props;

        console.info('handleAllComplete');
        // console.log(succeeded, failed);

        dispatch(uploadCompleted());

        const processAll = true;

        this.processAlbumQueue(processAll)
            .then(() => {
                this._albumTrackPromiseChain = Promise.resolve();
                this._albumTrackQueue = [];
                this._albumTrackQueueProcessCount = 0;
                console.warn('Processed all uploads', this._uploads);
                return;
            })
            .then(() => {
                this.processPreExistingTrackQueue();
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    };

    handleError = (id, name, e, xhr) => {
        console.info('handleError');
        console.log(id, name, xhr);

        // We won't get an error object if user aborted
        if (e) {
            const error = new UploadFailedException(e.message);

            this.props.dispatch(failUpload(id));
            analytics.error(error);
        }
    };

    handleProgress = (id, name, uploadedBytes, totalBytes) => {
        const { dispatch } = this.props;

        console.info('handleProgress');
        console.log(id, name, uploadedBytes, totalBytes);

        dispatch(updateProgress(id, uploadedBytes, totalBytes));

        const { upload } = this.props;

        if (!upload.inProgress) {
            dispatch(uploadStarted());
        }

        const totals = upload.list.reduce(
            (acc, obj) => {
                acc.uploaded += obj.bytesUploaded;
                acc.total += obj.bytesTotal;

                return acc;
            },
            { uploaded: 0, total: 0 }
        );

        dispatch(updateTotalProgress(totals.uploaded, totals.total));
    };

    handleSubmitted = (id, title) => {
        const { dispatch, location, upload } = this.props;
        const uploadType = this.getUploadTypeForPath(location.pathname);
        const { fileInput } = upload;

        console.info('handleSubmitted');
        console.log(id, title);

        let replaceId;

        if (fileInput.replaceId) {
            replaceId = fileInput.replaceId;
        }

        dispatch(
            addUpload({
                id,
                title,
                type: uploadType,
                replaceId
            })
        );
        dispatch(resetFileInput());
    };

    handleValidate = ({ name, size }) => {
        console.info('handleValidate');
        console.log(name, size);

        const { maxFileSize, maxZipSize, dispatch } = this.props;
        let maxSize;
        let fileText;

        if (name.substr(-4) === '.zip') {
            maxSize = maxZipSize;
            fileText = `The max file size for zip files is ${formatBytes(
                maxSize
            )}.`;
        } else {
            maxSize = maxFileSize;
            fileText = `The max file size allowed is ${formatBytes(maxSize)}.`;
        }

        const overLimit = size > maxSize;

        if (overLimit) {
            dispatch(
                showModal(MODAL_TYPE_ALERT, {
                    message: `File with name "${name}" is too large. ${fileText}`
                })
            );
            return false;
        }

        return true;
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getTypeFromFile(file) {
        const { dispatch } = this.props;

        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();

            fileReader.onerror = (err) => {
                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        title: 'File Error',
                        message: err.target.error.message
                    })
                );
                reject(err);
            };
            fileReader.onloadend = (e) => {
                const firstFourBytes = new Uint8Array(e.target.result);
                let fileSignature = '';

                for (let i = 0; i < firstFourBytes.length; i++) {
                    const byte = firstFourBytes[i];

                    fileSignature += (byte < 10 ? '0' : '') + byte.toString(16);
                }

                let type;

                // https://www.filesignatures.net/index.php
                if (
                    fileSignature.startsWith('494433') ||
                    fileSignature.startsWith('fff2') ||
                    fileSignature.startsWith('fff3') ||
                    fileSignature.startsWith('fffa') ||
                    fileSignature.startsWith('fffb')
                ) {
                    resolve('audio/mpeg');
                    return;
                }

                if (
                    fileSignature.startsWith('fff1') ||
                    fileSignature.startsWith('fff9')
                ) {
                    resolve('audio/aac');
                    return;
                }

                if (fileSignature === '00000020' && file.name.endsWith('m4a')) {
                    // avoid confusion between 3gp (0000002066747970) and m4a (00000020667479704D3441)
                    resolve('audio/mp4');
                    return;
                }

                if (
                    [
                        'application/x-zip-compressed',
                        'application/zip'
                    ].includes(file.type)
                ) {
                    resolve(file.type);
                    return;
                }

                // We really only need to add file signatures for some files
                // that dont provide a valid `file.type`. For example, the
                // mkv file I have results in an empty string.
                switch (fileSignature) {
                    case '89504e47':
                        type = 'image/png';
                        break;

                    case 'ffd8ffe0':
                    case 'ffd8ffe1':
                    case 'ffd8ffe2':
                        type = 'image/jpeg';
                        break;

                    case '464f524d':
                        type = 'audio/aiff';
                        break;

                    case '664c6143':
                        type = 'audio/flac';
                        break;

                    case '574d4d50':
                        type = 'audio/mpeg';
                        break;

                    case '4f676753':
                        type = 'audio/ogg';
                        break;

                    case '52494646':
                    case '57415645':
                        type = 'audio/wav';
                        break;

                    case '00000018':
                    case '0000001c':
                        type = 'audio/mp4';
                        break;

                    default: {
                        const errorObj = {
                            title: 'File Error',
                            message: 'Invalid file type'
                        };

                        const error = new Error(errorObj.message);
                        error.name = errorObj.title;

                        dispatch(showModal(MODAL_TYPE_ALERT, errorObj));
                        reject(error);
                        break;
                    }
                }

                resolve(type);
            };

            // Get first 4 bytes
            fileReader.readAsArrayBuffer(file.slice(0, 4));
        }).then((mime) => {
            if (mime.match('audio.*')) {
                return {
                    type: 'song',
                    mime
                };
            }

            const uploadType = this.getUploadTypeForPath(
                this.props.location.pathname
            );
            const zipTypes = [
                'application/x-zip-compressed',
                'application/zip'
            ];
            const isZip = zipTypes.includes(mime);

            if (uploadType === UPLOAD_TYPE.ALBUM && isZip) {
                return {
                    type: 'zip',
                    mime
                };
            }

            // Probably will use image detection for later uploads
            if (mime.match('image.*')) {
                console.warn('Image selected. Not implemented yet');
                return null;
            }

            return null;
        });
    }

    getUploadTypeForPath(pathname) {
        const isAlbum = ['/upload/albums', '/edit/album/'].some((path) => {
            return pathname.indexOf(path) === 0;
        });

        if (isAlbum) {
            return UPLOAD_TYPE.ALBUM;
        }

        return UPLOAD_TYPE.SONG;
    }

    reset() {
        Object.keys(this._uploads || {}).forEach((upload) => {
            try {
                upload.xhr.abort();
            } catch (err) {} // eslint-disable-line
        });

        this._uploads = {};
        this._albumTrackQueue = [];
        this._albumTrackQueueProcessCount = 0;
        this._albumTrackPromiseChain = Promise.resolve();
    }

    trackUpload(uploadType) {
        analytics.track(
            eventCategory.upload,
            {
                eventAction:
                    uploadType === UPLOAD_TYPE.ALBUM
                        ? eventAction.albumUpload
                        : eventAction.songUpload
            },
            ['ga', 'fb']
        );
    }

    queueAlbumTrackCreation(
        id,
        action,
        hasZipUpload = false,
        isZipUpload = false
    ) {
        let { index } = this._uploads[id].data;

        // Allow for zip file to be first in the queue
        if (hasZipUpload) {
            index += 1;

            if (isZipUpload) {
                index = 0;
            }
        }

        this._albumTrackQueue[index] = action;

        if (index === 0 || this._albumTrackQueueProcessCount > 0) {
            return this.processAlbumQueue();
        }

        return Promise.resolve();
    }

    processAlbumQueue(processAll = false) {
        const { dispatch } = this.props;

        if (!this._albumTrackQueue.length) {
            return this._albumTrackPromiseChain;
        }

        if (processAll) {
            console.warn('Processing the rest', this._albumTrackQueue);
            this._albumTrackQueue.forEach((action, i) => {
                if (!action) {
                    return;
                }

                this._albumTrackQueue[i] = null;

                this._albumTrackPromiseChain = this._albumTrackPromiseChain
                    .then(() => {
                        return dispatch(action);
                    })
                    .catch((err) => {
                        console.error(err);
                    });
            });

            return this._albumTrackPromiseChain;
        }

        let actionIndex = this._albumTrackQueueProcessCount;

        while (this._albumTrackQueue[actionIndex]) {
            const action = this._albumTrackQueue[actionIndex];

            this._albumTrackQueue[actionIndex] = null;

            console.warn('Processing index', actionIndex, action);

            this._albumTrackPromiseChain = this._albumTrackPromiseChain
                .then(() => {
                    return dispatch(action);
                })
                .catch((err) => {
                    console.error(err);
                })
                .finally(() => {
                    this._albumTrackQueueProcessCount += 1;
                });

            actionIndex += 1;
        }

        return this._albumTrackPromiseChain;
    }

    processPreExistingTrackQueue() {
        const { dispatch, uploadAlbum } = this.props;
        const { id: albumId } = uploadAlbum.info;

        if (
            !uploadAlbum.preExistingTrackQueue.length ||
            this._processingPreExistingTrackQueue
        ) {
            return;
        }

        this._processingPreExistingTrackQueue = true;

        console.warn(
            'Processing pre-existing tracks',
            uploadAlbum.preExistingTrackQueue
        );

        const actionQueue = uploadAlbum.preExistingTrackQueue.map((track) => {
            const { id: trackId } = track;
            const action = addPreExistingAlbumTrack(albumId, trackId);
            return dispatch(action);
        });

        Promise.all(actionQueue)
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                // this variable should be set to false even if addPreExistingAlbumTrack fails for some tracks
                this._processingPreExistingTrackQueue = false;
                console.warn('Processed pre-existing tracks');
            });
    }

    canUpload() {
        const { location, musicEdit, currentUser } = this.props;
        const uploadType = this.getUploadTypeForPath(location.pathname);

        if (this._isMouseDown) {
            return false;
        }

        if (uploadType === UPLOAD_TYPE.ALBUM) {
            if (!musicEdit.info.title || !musicEdit.info.artist) {
                return false;
            }
        }

        return canUserUpload(currentUser);
    }

    async uploadFiles(fileList) {
        const { dispatch } = this.props;
        const files = Array.from(fileList);

        if (files.length > MAX_FILE_UPLOAD_LIMIT) {
            dispatch(
                showModal(MODAL_TYPE_ALERT, {
                    message: `Uploads are limited to ${MAX_FILE_UPLOAD_LIMIT} items at a time.`
                })
            );
            return;
        }

        try {
            const typeObjs = await Promise.all(
                files.map((file) => this.getTypeFromFile(file))
            );
            const { uploadAlbum, musicEdit, location } = this.props;
            const currentUploadCount = Object.keys(this._uploads).length;
            const filtered = files.reduce(
                (acc, file, i) => {
                    if (!!typeObjs[i]) {
                        acc.data.push({
                            index: i + currentUploadCount,
                            name: file.name,
                            mime: typeObjs[i].mime,
                            id: uuid()
                        });
                        acc.files.push(file);
                    }
                    return acc;
                },
                { files: [], data: [] }
            );
            const uploadType = this.getUploadTypeForPath(location.pathname);

            if (!filtered.files.length) {
                let validTypes = 'MP3, WAV, AIFF, OGG, & M4A';

                if (uploadType === UPLOAD_TYPE.ALBUM) {
                    validTypes = `Zip, ${validTypes}`;
                }

                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        message: `No valid files found. Accepted file types are ${validTypes}. Limit of 250MB per file.`
                    })
                );
                return;
            }

            const newZipFiles = typeObjs.filter((obj) => {
                return obj && obj.type === 'zip';
            });
            const alreadyUploadedZip = this.props.upload.list.some((upload) => {
                return (
                    upload.title.substr(-4) === '.zip' &&
                    upload.status !== UPLOAD_STATUS.CANCELED
                );
            });

            if (
                (newZipFiles.length && alreadyUploadedZip) ||
                newZipFiles.length > 1
            ) {
                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        message: 'Only a single zip file is allowed.'
                    })
                );
                return;
            }

            // If they have already created an album entity we dont want to allow
            // zip files through
            if (newZipFiles.length && !!uploadAlbum.info.id) {
                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        message:
                            'Zip files are only allowed in the beginning of the album upload process.'
                    })
                );
                return;
            }

            if (newZipFiles.length && !!musicEdit.info.id) {
                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        message:
                            'Zip files are not allowed to be added as extra tracks.'
                    })
                );
                return;
            }

            const hasInvalidFile = filtered.files.some((file) => {
                return !this.handleValidate(file);
            });

            if (hasInvalidFile) {
                return;
            }

            filtered.files.forEach((file, i) => {
                const id = filtered.data[i].id;

                this.handleSubmitted(id, file.name);
            });

            await batchPromises(
                filtered.files.length,
                async (i) => {
                    const file = filtered.files[i];
                    const { id, name: fileName } = filtered.data[i];
                    const encodedFilename = encodeURIComponent(fileName);

                    try {
                        const action = await dispatch(
                            getPresignedUrl(encodedFilename)
                        );
                        const uploadUrl = action.resolved.uploadUri;

                        await this.uploadFile(
                            file,
                            uploadUrl,
                            id,
                            filtered.data[i]
                        );

                        // Purposely not await'ing these last couple
                        // functions as we want the uploads to be batched
                        // while the progress/complete functions just fire and
                        // finish as they will
                        this.handleProgress(
                            id,
                            file.name,
                            file.size,
                            file.size
                        );
                        this.handleComplete(id);
                    } catch (err) {
                        const { name, xhr, error = err } =
                            this._uploads[id] || {};

                        this.handleError(id, name, error, xhr);
                    }
                },
                {
                    batchCount: MAX_CONCURRENT_UPLOADS,
                    parallel: true,
                    parallelAfterFirstCompletion:
                        uploadType === UPLOAD_TYPE.ALBUM
                }
            );
        } catch (err) {
            analytics.error(err);
            console.error(err);
        }
    }

    uploadFile(file, signedEndpoint, id, data = {}) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();

            xhr.open('PUT', signedEndpoint, true);
            xhr.onabort = (e) => {
                console.warn('Upload canceled:', e);
                this._uploads[id].abort = e;

                reject(id);
            };
            xhr.onload = () => {
                resolve(id);
            };
            xhr.onerror = (e) => {
                this._uploads[id].error = e;

                reject(id);
            };

            // Listen to the upload progress.
            xhr.upload.onprogress = (e) => {
                if (e.lengthComputable) {
                    this.handleProgress(id, file.name, e.loaded, e.total);
                }
            };

            this._uploads[id] = {
                name: file.name,
                key: signedEndpoint
                    .split('?')[0]
                    .split('/')
                    .pop(),
                xhr,
                id,
                data,
                abort: null,
                error: null
            };

            xhr.setRequestHeader('Content-Type', data.mime);
            xhr.send(file);
        });
    }

    checkForCancelations(currentProps, nextProps) {
        nextProps.upload.list.forEach((uploadItem, i) => {
            const lastUpload = currentProps.upload.list[i];

            if (
                lastUpload &&
                lastUpload.id === uploadItem.id &&
                lastUpload.status !== UPLOAD_STATUS.CANCELED &&
                uploadItem.status === UPLOAD_STATUS.CANCELED
            ) {
                const uploadData = this._uploads[uploadItem.id];

                if (uploadData) {
                    uploadData.xhr.abort();
                }
            }
        });
    }

    checkForFileInputChanges(currentProps, nextProps) {
        const lastFileInputChange = currentProps.upload.fileInput.lastUpdated;
        const nextFileInput = nextProps.upload.fileInput;
        const nextFileInputChange = nextFileInput.lastUpdated;

        if (lastFileInputChange !== nextFileInputChange) {
            const inputElement = document.getElementById(nextFileInput.id);

            this.uploadFiles(inputElement.files);
        }
    }

    render() {
        if (!this.props.currentUser.isLoggedIn) {
            return null;
        }

        return (
            <div className="upload-overlay">
                <UploadPlusIcon className="upload-overlay__plus" />
                <p className="upload-overlay__title">
                    Drag and drop files to upload here
                </p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        musicEdit: state.musicEdit,
        uploadAlbum: state.uploadAlbum,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(UploaderContainer);
