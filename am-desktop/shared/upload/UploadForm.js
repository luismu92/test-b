import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { canUserUpload } from 'utils/index';

import { fileInput } from '../redux/modules/upload';
import { setIsAddingSingles } from '../redux/modules/upload/album';
import { resendEmail } from '../redux/modules/user/index';
import { addToast } from '../redux/modules/toastNotification';

import FolderAddIcon from '../icons/folder-add';
import PlusIcon from '../icons/plus';
import WarningIcon from '../icons/warning';
import styles from './UploadForm.module.scss';

class UploadForm extends Component {
    static propTypes = {
        uploadType: PropTypes.string.isRequired,
        inputId: PropTypes.string.isRequired,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        multiple: PropTypes.bool,
        inputType: PropTypes.string,
        buttonText: PropTypes.string,
        headerText: PropTypes.string,
        subtext: PropTypes.string,
        withForm: PropTypes.bool
    };

    static defaultProps = {
        headerText: 'Browse for files to upload or drag and drop them here',
        subtext:
            'Accepted file types are MP3, FLAC, WAV, AIFF, OGG, & M4A. Limit of 250MB per file.',
        buttonText: 'Browse to your file',
        withForm: true
    };

    constructor(props) {
        super(props);

        this.state = {
            emailResending: false,
            emailResent: false,
            emailResentError: null,
            supportError: false
        };

        this.inputRef = React.createRef();
    }

    componentDidMount() {
        this.checkSupport();
    }
    ////////////////////
    // Event handlers //
    ////////////////////

    handleAddExistingUploadsClick = () => {
        const { dispatch } = this.props;

        dispatch(setIsAddingSingles(true));
    };

    handleBrowseClick = (e) => {
        e.preventDefault();
        this.inputRef.current.click();
    };

    handleInputChange = (e) => {
        const { dispatch } = this.props;
        const id = e.currentTarget.id;
        const uploadType = e.currentTarget.getAttribute('data-upload-type');

        dispatch(fileInput(id, uploadType));
    };

    handleResendEmailClick = () => {
        const { dispatch, currentUser } = this.props;

        this.setState({
            emailResending: true
        });

        dispatch(resendEmail(window.location.pathname))
            .then(() => {
                this.setState({
                    emailResent: true
                });
                dispatch(
                    addToast({
                        action: 'message',
                        message: `An email confirmation has been sent to ${
                            currentUser.profile.email
                        }.`
                    })
                );
                return;
            })
            .catch(() => {
                this.setState({
                    emailResent: true,
                    emailResentError: true
                });
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    checkSupport() {
        if (!window.FileReader) {
            this.setState({
                supportError: true
            });
        }
    }

    renderContent() {
        const {
            inputId,
            headerText,
            inputType,
            uploadType,
            buttonText,
            multiple
        } = this.props;

        let addExistingUploads;
        let addIcon;
        let browseIcon;

        if (
            uploadType === 'albumUpload' ||
            uploadType === 'podcast' ||
            uploadType === 'song'
        ) {
            addIcon = (
                <span className={styles.folderAddIcon}>
                    <FolderAddIcon />
                </span>
            );

            browseIcon = (
                <span className={styles.plusIcon}>
                    <PlusIcon />
                </span>
            );
        }

        if (uploadType === 'albumEdit' || uploadType === 'albumUpload') {
            const { currentUser } = this.props;

            if (currentUser.profile.upload_count_excluding_reups) {
                addExistingUploads = (
                    <button
                        className={classnames(
                            styles.addExistingUploads,
                            styles.button,
                            styles.buttonOutline,
                            'button',
                            'button--pill',
                            'button--input',
                            'button--outline'
                        )}
                        onClick={this.handleAddExistingUploadsClick}
                        type="button"
                    >
                        {addIcon}
                        Add existing uploads
                    </button>
                );
            }
        }

        return (
            <Fragment>
                <p className={styles.headerText}>{headerText}</p>
                <div className={styles.buttonWrap}>
                    <button
                        className={classnames(
                            styles.button,
                            'button',
                            'button--pill',
                            'button--input'
                        )}
                        onClick={this.handleBrowseClick}
                    >
                        {browseIcon}
                        {buttonText}
                    </button>
                    {addExistingUploads}
                    <input
                        className={styles.input}
                        id={inputId}
                        type="file"
                        name="songs"
                        data-testid="uploadInput"
                        data-upload-type={uploadType}
                        accept={inputType}
                        onChange={this.handleInputChange}
                        multiple={multiple}
                        ref={this.inputRef}
                    />
                </div>
            </Fragment>
        );
    }

    renderSubText() {
        const { uploadType } = this.props;

        let rssLink;

        if (uploadType === 'podcast') {
            rssLink = (
                <Link className={styles.rss} to="/dashboard/manage/podcast">
                    Or import your existing RSS feed
                </Link>
            );
        }

        if (uploadType === 'albumEdit') {
            return (
                <small className={styles.subText}>
                    Accepted file types are Zip, MP3, FLAC, WAV, AIFF, OGG, &
                    M4A. Limit of 50MB per song.
                </small>
            );
        } else if (
            uploadType === 'albumUpload' ||
            uploadType === 'podcast' ||
            uploadType === 'song'
        ) {
            return (
                <Fragment>
                    {rssLink}
                    <small className={styles.subText}>
                        Accepted file types are Zip, MP3, FLAC, WAV, AIFF, OGG,
                        & M4A. Limit of 250MB per file.
                    </small>
                    <p className={styles.terms}>
                        Uploading constitutes your acceptance of our{' '}
                        <Link target="_blank" to="/about/terms-of-service">
                            Terms of Service
                        </Link>{' '}
                        and{' '}
                        <Link target="_blank" to="/about/privacy-policy">
                            Privacy Policy
                        </Link>
                        . Uploading music is reserved for Artists, DJs, and
                        Labels. Audiomack is not for storing or sharing your
                        personal music collection or files. DO NOT upload any
                        content which infringes on the rights of 3rd parties.
                        Users who upload 3rd party content will be banned from
                        Audiomack immediately.
                    </p>
                </Fragment>
            );
        }

        return null;
    }

    render() {
        const { inputId, uploadType, currentUser, withForm } = this.props;
        const { supportError } = this.state;

        if (supportError) {
            return (
                <p className={classnames(styles[uploadType], styles.dropzone)}>
                    Your browser does not support uploading. Please consider{' '}
                    <a
                        href="https://browsehappy.com/"
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        upgrading or changing your browser
                    </a>
                    .
                </p>
            );
        } else if (!canUserUpload(currentUser)) {
            return (
                <div className={styles.error}>
                    <span className={styles.errorIcon}>
                        <WarningIcon />
                    </span>
                    <p className={styles.errorParagraph}>
                        Your account has been banned from uploading to
                        Audiomack. You may still use the rest of Audiomack
                        without restriction, but you are no longer able to
                        upload content to Audiomack.
                    </p>
                </div>
            );
        } else if (!withForm) {
            return (
                <label
                    htmlFor={inputId}
                    className={classnames(styles[uploadType], styles.dropzone)}
                >
                    {this.renderContent()}
                    {this.renderSubText()}
                </label>
            );
        }

        return (
            <form
                className={styles[uploadType]}
                method="POST"
                encType="mutipart/form-data"
            >
                <label htmlFor={inputId} className={styles.dropzone}>
                    {this.renderContent()}
                    {this.renderSubText()}
                </label>
            </form>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(connect(mapStateToProps))(UploadForm);
