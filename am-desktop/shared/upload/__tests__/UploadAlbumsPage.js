/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import UploadAlbumsPage from '../UploadAlbumsPage';

Enzyme.configure({ adapter: new Adapter() });

test('should show 1st album step when there are only canceled uploads', () => {
    const upload = {
        policy:
            'eyJleHBpcmF0aW9uIjoiMjAxNy0wOS0wN1QwNToyMjo1My4wMDBaIiwiY29uZGl0aW9ucyI6W3siYnVja2V0IjoiYXVkaW9tYWNrLmRldi51cGxvYWRzIn0seyJhY2wiOiJwcml2YXRlIn0sWyJzdGFydHMtd2l0aCIsIiRrZXkiLCJtYWNsaS00NiJdLFsic3RhcnRzLXdpdGgiLCIkQ29udGVudC1UeXBlIiwiIl0sWyJzdGFydHMtd2l0aCIsIiRzdWNjZXNzX2FjdGlvbl9zdGF0dXMiLCIiXSxbInN0YXJ0cy13aXRoIiwiJG5hbWUiLCIiXSxbInN0YXJ0cy13aXRoIiwiJEZpbGVuYW1lIiwiIl1dfQ==',
        signature: 'tENA9DIctNjcOjjEHfFft/bQmvk=',
        defaultGenre: null,
        loading: false,
        inProgress: true,
        policyError: false,
        fileInput: {
            lastUpdated: 0,
            id: null,
            uploadType: null
        },
        bytesUploaded: 1671168,
        bytesTotal: 11325026,
        list: [
            {
                id: 0,
                bytesUploaded: 1671168,
                bytesTotal: 11325026,
                title: 'album.zip',
                type: 'album',
                musicId: null,
                saving: false,
                errors: [],
                status: 0 // Canceled value
            }
        ]
    };

    const uploadAlbum = {
        info: {},
        isAddingSingles: false,
        preExistingTrackQueue: []
    };

    const musicEdit = {
        info: {
            errors: {
                id: 'testError'
            },
            id: 'testId',
            title: 'testTitle'
        }
    };

    const currentUser = {
        profile: {}
    };

    const uploadSteps = [
        {
            label: 'Enter Album Details',
            name: 'Album Details',
            number: 1
        }
    ];

    const currentUploadStep = uploadSteps[0];

    const tags = {
        options: {
            music: []
        },
        ui: {
            geo: []
        },
        id: {}
    };

    function handleDropdownChange() {
        return;
    }

    const component = (
        <UploadAlbumsPage
            currentUploadStep={currentUploadStep}
            previousUploadStep={currentUploadStep}
            maxVisitedUploadStep={4}
            errors={{}}
            upload={upload}
            uploadAlbum={uploadAlbum}
            uploadSteps={uploadSteps}
            musicEdit={musicEdit}
            currentUser={currentUser}
            tags={tags}
            onDropdownChange={handleDropdownChange}
        />
    );
    const wrapper = shallow(component);
    const wrapperHasUploadHeaderText = wrapper
        .text()
        .includes(currentUploadStep.label);

    expect(wrapperHasUploadHeaderText).toBe(true);
});
