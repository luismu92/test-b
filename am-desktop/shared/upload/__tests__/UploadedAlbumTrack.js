/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import UploadedAlbumTrack from '../UploadedAlbumTrack';

Enzyme.configure({ adapter: new Adapter() });

function noop() {}

test('Should show last updated values after close a track with a fail updated', () => {
    const uploadItem = {
        title: 'test song',
        streaming_url: 'url',
        volume_data: '[3,9]',
        duration: '218',
        genre: 'podcast',
        artist: 'test artist',
        producer: '',
        featuring: 'Test1',
        status: 'complete',
        video_ad: 'no',
        isrc: '',
        url_slug: 'test-song',
        accounting_code: '',
        album_track_only: true,
        released: '1585937488',
        geo_restricted: false,
        song_id: 1,
        id: 1
    };
    const currentUser = {
        profile: {
            video_ads: ''
        }
    };
    const albumInfo = {
        tracks: []
    };

    const tags = {
        options: {
            geo: [],
            music: [
                {
                    display: 'Feel Good',
                    addedon: 1546985347,
                    tag: 'feelgood',
                    section: 'Sub-Genre',
                    normalizedKey: 'feelgood'
                }
            ]
        },
        ui: { geo: [235] },
        id: { 1: [], 2: [] },
        suggestions: []
    };

    const component = (
        <UploadedAlbumTrack
            uploadItem={uploadItem}
            currentUser={currentUser}
            albumInfo={albumInfo}
            onDeleteClick={noop}
            onSaveClick={noop}
            onStateChange={noop}
            tags={tags}
            onDropdownChange={noop}
        />
    );

    const wrapper = shallow(component);

    // update value of track but not successfully saved
    wrapper.setState({
        item: {
            ...uploadItem,
            title: 'updated random title'
        }
    });

    expect(wrapper.state().item.title).toEqual('updated random title');

    const closeOpenButton = wrapper.find('button');

    expect(closeOpenButton.length).toEqual(1);

    closeOpenButton.simulate('click', {
        preventDefault: () => ''
    });

    expect(wrapper.state().item.title).toEqual(uploadItem.title);
});
