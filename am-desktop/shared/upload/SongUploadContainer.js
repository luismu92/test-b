import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';

import analytics, {
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';
import {
    copyToClipboard,
    getMusicUrl,
    previewFile,
    getArtistName,
    uploadSlugify
} from 'utils/index';
import { allGenresMap, tagSections } from 'constants/index';

import requireAuth from '../hoc/requireAuth';

import { addToast } from '../redux/modules/toastNotification';
import { fileInput } from '../redux/modules/upload/index';
import {
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_ALERT,
    MODAL_TYPE_PROMO,
    MODAL_TYPE_SUGGEST_TAG,
    showModal,
    hideModal
} from '../redux/modules/modal';
import { updateMetadata, deleteItem } from '../redux/modules/music/index';

import {
    getGlobalGeoTags,
    getGlobalMusicTags,
    getMusicTags,
    addMusicTags,
    deleteMusicTags
} from '../redux/modules/music/tags';

import PromoModal from '../modal/PromoModal';
import SongUpload from './SongUpload';
import SuggestTagModal from '../modal/SuggestTagModal';

const songDescriptionMaxChars = 1200;
const adminDescriptionMaxChars = 1500;

class SongUploadContainer extends Component {
    static propTypes = {
        musicItem: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        autosave: PropTypes.bool,
        onSongDelete: PropTypes.func,
        onSaveClick: PropTypes.func,
        upload: PropTypes.object,
        location: PropTypes.object,
        isEditing: PropTypes.bool,
        fromMobile: PropTypes.bool,
        uploadPage: PropTypes.bool,
        history: PropTypes.object,
        tags: PropTypes.object
    };

    static defaultProps = {
        autosave: true,
        isEditing: true,
        onSaveClick() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            musicItem: props.musicItem,
            releaseDate: null,
            urlSlugManuallyUpdated: false,
            dirty: false,
            saveButtonClicked: false,
            activeTabIndex: 0,
            selectedReleaseOption: '1',
            futureReleaseDate:
                moment(props.musicItem.released * 1000).diff(moment()) > 0,
            isAutoSaving: false,
            open: true,
            descriptionCharsLeft: songDescriptionMaxChars,
            adminDescriptionCharsLeft: adminDescriptionMaxChars,
            errors: {},
            showErrorMessage: false,
            newUrlSlug: { slug: props.musicItem.url_slug, error: '' },
            trackIsFinished: false
        };
    }

    componentDidMount() {
        this._unblock = this.props.history.block(
            this.handleHistoryLocationChange
        );
        this.fetchTags();
    }

    componentWillUnmount() {
        clearTimeout(this._autosaveTimer);
        this._autosaveTimer = null;

        if (this._unblock) {
            this._unblock();
        }
        this._unblock = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCopyClick = (e) => {
        const { dispatch } = this.props;
        const url = e.currentTarget.text;
        const copied = copyToClipboard(url);

        let message = `Error copying song URL ${url} to clipboard`;

        if (copied) {
            message = 'URL copied to clipboard!';
        }

        dispatch(
            addToast({
                action: 'message',
                item: message
            })
        );
    };

    handleHistoryLocationChange = (nextLocation) => {
        const { dispatch, autosave, location } = this.props;
        const { trackIsFinished } = this.state;
        const isEditPage = location.pathname.match(/^\/edit\/song\/\d+/);

        if (
            (!this.state.dirty && !autosave) ||
            (isEditPage && autosave) ||
            trackIsFinished
        ) {
            if (this._unblock) {
                this._unblock();
                this._unblock = null;
            }
            return true;
        }

        const message = autosave
            ? 'Your changes have been automatically saved but your songs will not be live until you press "Finish".'
            : 'You have unsaved changes.';

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure you want to leave?',
                message,
                handleConfirm: () => this.confirmLeave(nextLocation),
                confirmButtonText: 'Leave anyway',
                confirmButtonProps: {
                    className: 'button button--pill'
                }
            })
        );

        return false;
    };

    handleViewSongClick = (e) => {
        e.preventDefault();

        const { history, musicItem } = this.props;
        const link = getMusicUrl(musicItem, {
            host: process.env.AM_URL
        });
        const nextLocation = link.split(process.env.AM_URL)[1];

        if (this._unblock) {
            this._unblock();
            this._unblock = null;
        }

        history.push(nextLocation);
    };

    handleDrawerToggle = () => {
        this.setState((prevState) => {
            return {
                open: !prevState.open
            };
        });
    };

    handleDropdownChange = async (text, e) => {
        const { dispatch, tags } = this.props;
        const { musicItem } = this.state;
        const genres = Object.values(allGenresMap);
        const button = e.target;
        const dropdown = button.closest('[data-dropdown]');
        const section = dropdown.getAttribute('data-name');
        const value = button.getAttribute('data-value').replace(/\-\>/g, ' ');
        const list = tags.id[musicItem.id] || [];

        if (!value) {
            return;
        }

        if (section === 'genre') {
            const event = {
                ...e,
                currentTarget: {
                    ...e.currentTarget,
                    type: 'dropdown',
                    name: 'genre',
                    value: value === musicItem.genre ? '' : value
                }
            };

            this.handleInputChange(event);
            return;
        }

        try {
            switch (section) {
                case tagSections.locations: {
                    break;
                }

                case tagSections.mood: {
                    const removeMood = list.find(
                        (tag) => tag.normalizedKey === value
                    );

                    if (removeMood) {
                        await dispatch(
                            deleteMusicTags(musicItem, removeMood.normalizedKey)
                        );

                        return;
                    }

                    const currentMoods = list.filter(
                        (tag) => tag.section === tagSections.mood
                    );

                    if (currentMoods.length === 2) {
                        throw new Error(
                            'You can only have 2 tags from this category.'
                        );
                    }

                    break;
                }

                case tagSections.subgenre: {
                    const removeSubgenre = list.find(
                        (tag) => tag.normalizedKey === value
                    );

                    if (removeSubgenre) {
                        await dispatch(
                            deleteMusicTags(
                                musicItem,
                                removeSubgenre.normalizedKey
                            )
                        );

                        return;
                    }

                    const currentSubgenres = list.filter(
                        (tag) =>
                            tag.section === tagSections.subgenre ||
                            genres.includes(tag.section)
                    );

                    if (currentSubgenres.length === 2) {
                        throw new Error(
                            'You can only have 2 tags from this category.'
                        );
                    }

                    break;
                }

                default:
                    break;
            }

            await dispatch(addMusicTags(musicItem, value));
        } catch (error) {
            dispatch(
                addToast({
                    action: 'message',
                    item: error.message
                })
            );
        }
    };

    handleReplaceInputChange = (e) => {
        const { dispatch } = this.props;
        const { musicItem } = this.state;
        const id = e.currentTarget.id;
        const uploadType = e.currentTarget.getAttribute('data-upload-type');
        const replaceId = musicItem.id;

        dispatch(fileInput(id, uploadType, replaceId));
    };

    handleInputChange = (e) => {
        const { dispatch, autosave, isEditing } = this.props;
        const { musicItem, errors } = this.state;
        const input = e.currentTarget;
        const inputName = input.name;
        let inputValue = input.value;
        let urlSlugManuallyUpdated = this.state.urlSlugManuallyUpdated;

        switch (input.type) {
            case 'checkbox': {
                inputValue = input.checked;

                if (input.name === 'private' || input.name === 'explicit') {
                    inputValue = input.checked ? 'yes' : 'no';
                }

                if (input.name === 'stream_only') {
                    inputValue = input.checked ? 'no' : 'yes';
                }
                break;
            }

            case 'select-one': {
                const value = input.options[input.selectedIndex].value;

                inputValue = value;

                if (!!value && errors[input.name]) {
                    errors[input.name] = false;
                }

                this.setState({
                    errors
                });
                break;
            }

            default: {
                if (input.name === 'url_slug') {
                    if (!inputValue.match('^[a-zA-Z0-9-]*$')) {
                        return;
                    }

                    inputValue = inputValue.toLowerCase();
                    urlSlugManuallyUpdated = true;
                }

                let newCharactersLeft = this.state.descriptionCharsLeft;
                let newAdminCharactersLeft = this.state
                    .adminDescriptionCharsLeft;

                if (input.name === 'description') {
                    newCharactersLeft =
                        songDescriptionMaxChars - inputValue.length;
                }

                if (input.name === 'admin_description') {
                    newAdminCharactersLeft =
                        adminDescriptionMaxChars - inputValue.length;
                }

                if (!!input.value && errors[input.name]) {
                    errors[input.name] = false;
                }

                this.setState({
                    errors,
                    descriptionCharsLeft: newCharactersLeft,
                    adminDescriptionCharsLeft: newAdminCharactersLeft,
                    urlSlugManuallyUpdated: urlSlugManuallyUpdated
                });

                break;
            }
        }

        const newMusicItem = {
            ...musicItem,
            [inputName]: inputValue,
            isEditing
        };

        // Update url slug of the track only if this is a new upload
        if (inputName === 'title' && !urlSlugManuallyUpdated && !isEditing) {
            newMusicItem.url_slug = uploadSlugify(inputValue);
        }

        this.setState({
            musicItem: newMusicItem
        });

        if (autosave) {
            clearTimeout(this._autosaveTimer);
            this.setState({
                isAutoSaving: true,
                showErrorMessage: false
            });

            const autosaveErrors = this.getInvalidFields(newMusicItem);

            if (autosaveErrors) {
                this.setState({
                    errors: autosaveErrors,
                    isAutoSaving: false,
                    showErrorMessage: true
                });

                return;
            }

            this._autosaveTimer = setTimeout(() => {
                dispatch(updateMetadata(musicItem.id, newMusicItem))
                    .then((result) => {
                        if (typeof result.resolved.url_slug !== 'undefined') {
                            let urlMessage = '';
                            if (
                                result.resolved.url_slug !==
                                newMusicItem.url_slug
                            ) {
                                urlMessage =
                                    `The song from ${
                                        newMusicItem.url_slug
                                    } slug is already loaded by you.` +
                                    ` The next slug will be applied: ${
                                        result.resolved.url_slug
                                    }.`;
                            }
                            this.setState({
                                newUrlSlug: {
                                    slug: result.resolved.url_slug,
                                    error: urlMessage
                                }
                            });
                        }

                        return;
                    })
                    .catch((err) => {
                        console.log(err);
                        this.setErrorsFromUpdateRequest(err.errors);
                    })
                    .finally(() => {
                        this.setState({
                            isAutoSaving: false
                        });
                    });
            }, 3000);
            return;
        }

        this.setState({
            dirty: true
        });
    };

    handleDeleteClick = (e) => {
        e.preventDefault();

        const id = e.currentTarget.getAttribute('data-music-id');
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Delete song',
                message:
                    'Are you sure you want to delete this song? This action cannot be undone.',
                handleConfirm: () => this.deleteSong(id),
                confirmButtonText: 'Delete',
                confirmButtonProps: {
                    className: 'button button--pill button--danger',
                    'data-testid': 'deleteConfirmButton'
                }
            })
        );
    };

    handleSongFinish = (e) => {
        e.preventDefault();
        const { dispatch, autosave, isEditing, onSaveClick } = this.props;
        const { musicItem } = this.state;
        const errors = this.getInvalidFields(musicItem);

        if (errors) {
            const tabIndex = this.getTabIndexForErrors(
                errors,
                this.state.activeTabIndex
            );

            this.setState({
                activeTabIndex: tabIndex,
                errors,
                showErrorMessage: true
            });
            return;
        }

        const metadata = {
            ...musicItem,
            status: 'complete',
            isSingleTrack: 'yes',
            isEditing
        };

        this.setState({
            saveButtonClicked: true,
            showErrorMessage: false,
            trackIsFinished: true
        });

        dispatch(updateMetadata(musicItem.id, metadata))
            .then((action) => {
                this.setState({
                    dirty: false
                });

                // If were on the song upload page
                if (!isEditing) {
                    const { currentUser } = this.props;
                    const { mixpanel } = window;

                    // @todo move this tracking/api stuff to redux middleware
                    // for the UPDATE_METADATA event
                    // https://github.com/audiomack/audiomack-js/issues/843
                    if (typeof mixpanel !== 'undefined') {
                        const userId = currentUser.profile.id;
                        const signupDate = new Date(
                            currentUser.profile.created * 1000
                        ).toISOString();
                        const firstUploadDate = new Date(
                            currentUser.profile.first_upload_date * 1000
                        ).toISOString();
                        const username = currentUser.profile.url_slug;
                        const uploadCount =
                            currentUser.profile.upload_count_excluding_reups;

                        mixpanel.opt_in_tracking();
                        mixpanel.register({
                            'User ID': userId
                        });
                        mixpanel.people.set({
                            // These are user properties within the Create Account event
                            'User ID': userId,
                            Username: username,
                            // 'Authentication Type': 'Facebook',
                            'Sign Up Date': signupDate,
                            'First Upload Date': firstUploadDate,
                            // These are user properties within the upload event
                            '# of Songs Uploaded': uploadCount
                        });
                        mixpanel.identify(userId);

                        const trackObj = {
                            'Upload Name': action.resolved.title,
                            'Music ID': action.resolved.id,
                            'Artist Name': getArtistName(action.resolved),
                            'Upload Type': 'Song',
                            Genre: action.resolved.genre,
                            Tags: action.resolved.usertags
                        };

                        mixpanel.track(eventCategory.upload, trackObj);
                    }
                }

                if (autosave) {
                    this.setState({
                        musicItem: {
                            ...metadata,
                            ...action.resolved,
                            saved: true
                        }
                    });
                }

                if (typeof onSaveClick === 'function') {
                    onSaveClick();
                }

                if (isEditing) {
                    this.handleViewSongClick(e);
                }
                return;
            })
            .catch((err) => {
                console.log(err);

                const ERROR_MUSIC_AMP_FORBIDDEN = 3039;

                let songTitle = 'your song';

                if (musicItem.title) {
                    songTitle = `"${musicItem.title}"`;
                }

                let errorMessage = `There was an error saving ${songTitle}.`;

                if (err.errorcode === ERROR_MUSIC_AMP_FORBIDDEN) {
                    errorMessage = `Please contact ${
                        err.errors[0]
                    } to change monetization fields or contact contentops@audiomack.com.`;
                } else if (err.errors) {
                    const invalidFields = Object.keys(err.errors).filter(
                        (key) => {
                            return err.errors[key].length;
                        }
                    );

                    this.setState((prevState) => {
                        return {
                            ...prevState,
                            errors: invalidFields.reduce((obj, field) => {
                                obj[field] = true;

                                return obj;
                            }, prevState.errors)
                        };
                    });

                    errorMessage += ` Please check the following fields: ${invalidFields.join(
                        ', '
                    )}.`;
                }

                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        title: 'Error',
                        message: errorMessage
                    })
                );

                this.setState({
                    saveButtonClicked: false
                });
            });
    };

    handleFinishSocialClick = (e) => {
        const button = e.currentTarget;
        const socialType = button.getAttribute('data-social-type');

        const label =
            socialType === 'facebook'
                ? eventAction.facebookSocial
                : eventAction.twitterSocial;

        analytics.track(eventCategory.socialShare, {
            eventAction: label,
            eventLabel: eventLabel.uploadFinishSocial
        });
    };

    handleFileInputChange = (e) => {
        const { name } = e.currentTarget;

        previewFile(e.target.files[0])
            .then((value) => {
                this.setState({
                    dirty: true
                });
                return this.handleInputChange({
                    currentTarget: {
                        name,
                        value
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            });
    };

    handleDateChange = (date) => {
        const { musicItem, releaseDate } = this.state;
        const isMomentDate = moment.isMoment(date);

        let newDate = date;

        if (!isMomentDate) {
            const currentDate =
                releaseDate || moment(musicItem.released * 1000);
            const isPM = currentDate.hours() >= 12;
            const { name, value } = date.target;

            if (name === 'hour') {
                const hourToSet = parseInt(value, 10) + (isPM ? 12 : 0);

                newDate = currentDate.hour(hourToSet);
            } else if (name === 'minute') {
                newDate = currentDate.minute(parseInt(value, 10));
            } else if (name === 'ampm') {
                const currentHour = currentDate.hours();
                const am = -12;
                const pm = 12;
                const newHour = currentHour + (value === 'am' ? am : pm);

                newDate = currentDate.hour(newHour);
            }
        }

        this.setState({
            dirty: true,
            futureReleaseDate: newDate.diff(moment()) > 0,
            releaseDate: newDate
        });
    };

    handleDateSubmit = (date) => {
        if (date) {
            this.handleDateChange(date);
        }

        this.handleInputChange({
            currentTarget: {
                name: 'released',
                value: this.state.releaseDate.unix()
            }
        });
    };

    handleReleaseOptionChange = (e) => {
        const value = e.target.value;

        this.setState({
            dirty: true,
            selectedReleaseOption: value
        });

        const currentDate = moment();

        switch (value) {
            // option 1, release date set to now, set private=no
            case '1':
                this.handleInputChange({
                    currentTarget: {
                        name: 'released',
                        value: currentDate.unix()
                    }
                });

                this.handleInputChange({
                    currentTarget: {
                        name: 'private',
                        value: 'no'
                    }
                });

                break;

            // option 2, set release date now, but send private=yes
            case '2':
                this.handleInputChange({
                    currentTarget: {
                        name: 'released',
                        value: currentDate.unix()
                    }
                });

                this.handleInputChange({
                    currentTarget: {
                        name: 'private',
                        value: 'yes'
                    }
                });

                break;

            // option 3, open calendar and get custom date, set private=no
            case '3':
                this.handleInputChange({
                    currentTarget: {
                        name: 'private',
                        value: 'no'
                    }
                });

                break;

            default:
                break;
        }
    };

    handlePrivateShareClick = (e) => {
        const { dispatch, musicItem } = this.props;

        e.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_PROMO, {
                music: musicItem
            })
        );
    };

    handleSuggestTagClick = (e) => {
        const { dispatch } = this.props;

        e.preventDefault();

        dispatch(showModal(MODAL_TYPE_SUGGEST_TAG));
    };

    ////////////////////
    // Helper methods //
    ////////////////////
    getInvalidFields(musicItem) {
        // Validate current input before sending field data
        const required = ['artist', 'title', 'genre', 'url_slug'];

        const invalidFields = required.filter((field) => {
            if (field === 'genre') {
                return (
                    !musicItem.genre ||
                    !Object.keys(allGenresMap).includes(musicItem.genre)
                );
            }

            return !musicItem[field];
        });

        if (invalidFields.length) {
            const errors = invalidFields.reduce((acc, field) => {
                acc[field] = true;
                return acc;
            }, {});

            return errors;
        }

        return null;
    }

    getTabIndexForErrors(errors, activeTabIndex) {
        const errorKeys = Object.keys(errors);
        const onlyUrlSlugError =
            errorKeys.length === 1 && errorKeys[0] === 'url_slug';
        const urlSlugTab = 2;
        const otherErrorTab = 0;

        // Don't jump around if we're already on a tab with an error
        if (activeTabIndex === urlSlugTab && errorKeys.includes('url_slug')) {
            return activeTabIndex;
        }

        if (activeTabIndex === otherErrorTab && !onlyUrlSlugError) {
            return activeTabIndex;
        }

        return onlyUrlSlugError ? 2 : 0;
    }

    getErrorsFromUpdateRequest(errors) {
        const invalidFields = Object.keys(errors).filter((key) => {
            return errors[key].length;
        });
        return invalidFields;
    }

    setErrorsFromUpdateRequest(errors) {
        const invalidFields = this.getErrorsFromUpdateRequest(errors);

        this.setState((prevState) => {
            return {
                ...prevState,
                errors: invalidFields.reduce((obj, field) => {
                    obj[field] = true;

                    return obj;
                }, prevState.errors)
            };
        });
    }

    addMusicTag = (tagValue, tagSection) => {
        const { dispatch, musicItem, tags } = this.props;
        const tag = tagValue.replace(/\-\>/g, ' ');

        if (tagSection === tagSections.locations) {
            const tagComponents = tagValue.split('->');
            let parentTagObject;
            let targetTagObject;

            while (tagComponents.length) {
                const component = tagComponents.shift();
                let tagObject;

                if (parentTagObject) {
                    this.removeMusicTag(
                        parentTagObject.display.replace(/\-\>/g, ' ')
                    );
                } else {
                    tagObject = tags.ui.geo.find((t) => {
                        return t.display === component;
                    });
                }

                if (
                    parentTagObject &&
                    parentTagObject.hasChildren &&
                    parentTagObject.children.length
                ) {
                    // eslint-disable-next-line
                    tagObject = parentTagObject.children.find((t) => {
                        return (
                            t.display ===
                            `${parentTagObject.display}->${component}`
                        );
                    });
                }

                parentTagObject = tagObject;

                if (!tagComponents.length) {
                    targetTagObject = tagObject;
                }
            }

            if (
                targetTagObject.hasChildren &&
                !targetTagObject.children.length
            ) {
                dispatch(getGlobalGeoTags(...tagValue.split('->')));
            }

            if (targetTagObject.type === 'country') {
                return;
            }
        }

        dispatch(addMusicTags(musicItem, tag)).catch((error) => {
            return dispatch(
                addToast({
                    action: 'message',
                    item: error.message
                })
            );
        });
    };

    removeMusicTag = (tagValue) => {
        const { dispatch, musicItem } = this.props;
        const tag = tagValue.replace(/\-\>/g, ' ');

        dispatch(deleteMusicTags(musicItem, tag)).catch((error) => {
            return dispatch(
                addToast({
                    action: 'message',
                    item: error.message
                })
            );
        });
    };

    showTagError = (message) => {
        const { dispatch } = this.props;

        dispatch(
            addToast({
                action: 'message',
                item: message
            })
        );
    };

    confirmLeave(nextLocation) {
        const { history, dispatch } = this.props;

        if (this._unblock) {
            this._unblock();
            this._unblock = null;
        }

        dispatch(hideModal());
        history.push(nextLocation);
    }

    deleteSong(musicId) {
        const { dispatch, currentUser } = this.props;
        const { musicItem } = this.state;

        this.setState({
            musicItem: {
                ...musicItem,
                isDeleting: true
            }
        });

        let callback = () => {}; // eslint-disable-line

        if (typeof this.props.onSongDelete === 'function') {
            callback = this.props.onSongDelete;
        }

        dispatch(hideModal());
        dispatch(deleteItem(musicId))
            .then(() => {
                this.setState({
                    musicItem: {
                        ...musicItem,
                        isDeleting: false
                    }
                });

                return callback();
            })
            .catch((err) => {
                console.log(err);
                // If music not found error just call callback to go to artist page
                if (err.errorcode === 1005) {
                    callback();
                    return;
                }

                let errorMsg = 'There was a problem deleting this music item.';

                if (currentUser.isAdmin) {
                    errorMsg = `There was a problem deleting this music item. ${
                        err.message
                    }`;
                }

                dispatch(
                    addToast({
                        action: 'message',
                        item: errorMsg
                    })
                );

                this.setState({
                    musicItem: {
                        ...musicItem,
                        isDeleting: false
                    }
                });
            });
    }

    itemIsSaving(musicEdit) {
        if (!musicEdit.info) {
            return false;
        }

        if (musicEdit.isSaving) {
            return true;
        }

        if (musicEdit.info.tracks && musicEdit.info.tracks.length) {
            return musicEdit.info.tracks.some((track) => track.isSaving);
        }

        return false;
    }

    fetchTags() {
        const { dispatch, musicItem, tags } = this.props;

        // check tags.ui.geo instead of tags.options.geo because root-level (country) tags are not valid options
        if (!tags.ui.geo.length) {
            dispatch(getGlobalGeoTags());
        }

        if (!tags.options.music.length) {
            dispatch(getGlobalMusicTags());
        }

        dispatch(getMusicTags(musicItem));
    }

    render() {
        const { currentUser, history, location, upload, autosave } = this.props;
        const { musicItem, releaseDate } = this.state;
        const replaceUploadItem = Array.from(upload.list)
            .reverse()
            .find((obj) => obj.replaceId === musicItem.id);

        return (
            <Fragment>
                <SongUpload
                    editMode
                    autosave={autosave}
                    history={history}
                    location={location}
                    saveButtonClicked={this.state.saveButtonClicked}
                    onReplaceInputChange={this.handleReplaceInputChange}
                    currentUser={currentUser}
                    uploadItem={musicItem}
                    releaseDate={releaseDate}
                    replaceUploadItem={replaceUploadItem}
                    className="u-box-shadow"
                    onDropdownChange={this.handleDropdownChange}
                    onInputChange={this.handleInputChange}
                    onFinishClick={this.handleSongFinish}
                    onDeleteClick={this.handleDeleteClick}
                    onFileInputChange={this.handleFileInputChange}
                    songDescriptionMaxChars={songDescriptionMaxChars}
                    adminDescriptionMaxChars={adminDescriptionMaxChars}
                    onDateBlur={this.handleDateSubmit}
                    onDateChange={this.handleDateChange}
                    selectedReleaseOption={this.state.selectedReleaseOption}
                    futureReleaseDate={this.state.futureReleaseDate}
                    onReleaseOptionChange={this.handleReleaseOptionChange}
                    errors={this.state.errors}
                    open={this.state.open}
                    dirty={this.state.dirty}
                    isAutoSaving={this.state.isAutoSaving}
                    onDrawerToggle={this.handleDrawerToggle}
                    descriptionCharsLeft={this.state.descriptionCharsLeft}
                    adminDescriptionCharsLeft={
                        this.state.adminDescriptionCharsLeft
                    }
                    onFinishSocialClick={this.handleFinishSocialClick}
                    showErrorMessage={this.state.showErrorMessage}
                    fromMobile={this.props.fromMobile}
                    onCopyClick={this.handleCopyClick}
                    onPrivateShareClick={this.handlePrivateShareClick}
                    onSuggestTagClick={this.handleSuggestTagClick}
                    onViewSongClick={this.handleViewSongClick}
                    newUrlSlug={this.state.newUrlSlug}
                    uploadPage={this.props.uploadPage}
                    tags={this.props.tags}
                    addMusicTag={this.addMusicTag}
                    removeMusicTag={this.removeMusicTag}
                    showTagError={this.showTagError}
                />
                <PromoModal
                    onClose={this.handleModalClose}
                    active={musicItem.private}
                    music={musicItem}
                />
                <SuggestTagModal musicItem={musicItem} />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        upload: state.upload,
        tags: state.musicTags
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(SongUploadContainer);
