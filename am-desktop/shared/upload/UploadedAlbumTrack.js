import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {
    formatBytes,
    yesBool,
    getUploadStatusData,
    renderTrackTitle
} from 'utils/index';
import { getTagOptions, getUserTags } from 'utils/tags';
import {
    handleMusicInputChange,
    getErroredKeysFromUpdateRequest
} from 'utils/music/edit';

import { tagSections, UPLOAD_STATUS } from 'constants/index';

import ChevronDownIcon from '../icons/chevron-down';
import CloseThinIcon from '../icons/close-thin';
import CogIcon from '../icons/cog';
import TrashIconSolid from 'icons/trash-solid';

import Dropdown from '../components/Dropdown';
import LabeledInput, {
    inputTypes as labeledInputTypes
} from '../components/LabeledInput';

import styles from './UploadedAlbumTrack.module.scss';

export default class UploadedAlbumTrack extends Component {
    static propTypes = {
        uploadItem: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        autosave: PropTypes.bool,
        open: PropTypes.bool,
        dirty: PropTypes.bool,
        genreExplicitlySet: PropTypes.bool,
        activeTabIndex: PropTypes.number,
        saveRequestErrors: PropTypes.object,
        onDeleteClick: PropTypes.func.isRequired,
        onSaveClick: PropTypes.func.isRequired,
        onStateChange: PropTypes.func,
        getCachedTrackState: PropTypes.func,
        onTagListResize: PropTypes.func,
        onPropagateTrackHack: PropTypes.func,
        albumInfo: PropTypes.object,
        onAlbumTrackReplaceInputChange: PropTypes.func,
        replaceAudioSuccess: PropTypes.func,
        replaceUploadItem: PropTypes.object,
        tagListAdditionalHeights: PropTypes.object,
        trackNumber: PropTypes.number,
        onDropdownChange: PropTypes.func,
        onSuggestTagClick: PropTypes.func,
        tags: PropTypes.object
    };

    static defaultProps = {
        getCachedTrackState() {
            return {};
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            item: props.uploadItem,
            open: props.open,
            isSaving: false,
            dirty: props.dirty || false,
            lastUpdatedItem: props.uploadItem,
            genreExplicitlySet: props.genreExplicitlySet || false,
            activeTabIndex: props.activeTabIndex || 0,
            errors: props.saveRequestErrors || {},
            ...props.getCachedTrackState(props.uploadItem.song_id)
        };

        this._metadataTabs = {
            basic: 'Basic Information'
        };

        if (yesBool(props.currentUser.profile.video_ads)) {
            this._metadataTabs.monetization = 'Monetization';
        }
    }

    componentDidMount() {
        this._mounted = true;
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.albumInfo.genre !== this.props.albumInfo.genre &&
            !this.state.genreExplicitlySet &&
            !this.state.item.genre
        ) {
            // eslint-disable-next-line
            this.setAndPropagateState((prevState) => {
                return {
                    item: {
                        ...prevState.item,
                        genre: this.props.albumInfo.genre
                    }
                };
            });
        }

        if (
            prevProps.replaceUploadItem &&
            prevProps.replaceUploadItem.status !== UPLOAD_STATUS.COMPLETED &&
            this.props.replaceUploadItem &&
            this.props.replaceUploadItem.status === UPLOAD_STATUS.COMPLETED
        ) {
            this.props.replaceAudioSuccess(this.props.replaceUploadItem.title);
        }
    }

    componentWillUnmount() {
        this._mounted = false;
        clearTimeout(this._trackAutosaveTimer);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContentToggle = (e) => {
        e.preventDefault();

        this.setAndPropagateState((prevState) => {
            const { lastUpdatedItem } = this.state;
            const newOpenState = !prevState.open;

            // Show last successfully updated value of track item
            return {
                open: newOpenState,
                item: lastUpdatedItem
            };
        });
    };

    handleInputChange = (e) => {
        const { item } = this.state;
        const { autosave, trackNumber } = this.props;
        const input = e.currentTarget;

        const change = handleMusicInputChange(input);
        const newItem = {
            ...item,
            [change.name]: change.value
        };

        this.setAndPropagateState((prevState) => {
            const newState = {
                ...prevState,
                dirty: true,
                item: newItem
            };

            if (input.name === 'genre') {
                newState.genreExplicitlySet = true;
            }

            newState.errors[change.name] = !!change.error;

            return newState;
        });

        this.props.onPropagateTrackHack(newItem);

        if (autosave) {
            // Add timeout so that we will get an updated version uploadItem
            clearTimeout(this._trackAutosaveTimer);

            if (change.error) {
                return;
            }

            this._trackAutosaveTimer = setTimeout(() => {
                this.props
                    .onSaveClick(item.song_id, newItem, change, trackNumber)
                    .then(() => {
                        this.setAndPropagateState(() => {
                            const newState = {
                                lastUpdatedItem: newItem,
                                isSaving: false
                            };

                            return newState;
                        });

                        return;
                    })
                    .catch(() => {
                        this.setAndPropagateState({
                            isSaving: false
                        });
                    });
            }, 1000);
        }
    };

    handleTrackSaveClick = (e) => {
        e.preventDefault();

        const { trackNumber } = this.props;
        const { item } = this.state;

        this.setAndPropagateState({
            isSaving: true
        });

        this.props
            .onSaveClick(item.song_id, item, null, trackNumber)
            .then(() => {
                this.setAndPropagateState(() => {
                    const newState = {
                        dirty: false,
                        open: false,
                        lastUpdatedItem: item,
                        isSaving: false
                    };

                    return newState;
                });

                return;
            })
            .catch((err) => {
                if (!err.errors) {
                    return;
                }

                const errorKeys = getErroredKeysFromUpdateRequest(err.errors);
                const tabTwoErrors = ['accounting_code', 'isrc'];
                const containsTabTwoErrors = tabTwoErrors.some((key) => {
                    return errorKeys.includes(key);
                });
                const containsTabOneErrors =
                    errorKeys.length &&
                    (!containsTabTwoErrors ||
                        (containsTabTwoErrors &&
                            errorKeys.length > tabTwoErrors.length));

                let activeTabToGoTo = this.state.activeTabIndex;

                if (this.state.activeTabIndex === 0 && !containsTabOneErrors) {
                    activeTabToGoTo = 1;
                } else if (
                    this.state.activeTabIndex === 1 &&
                    !containsTabTwoErrors
                ) {
                    activeTabToGoTo = 0;
                }

                this.setAndPropagateState({
                    // All required fields are on tab 0 as of now
                    activeTabIndex: activeTabToGoTo,
                    isSaving: false
                });
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    setAndPropagateState(newStateOrFn) {
        if (!this._mounted) {
            return;
        }

        this.setState(newStateOrFn, () => {
            if (this._mounted) {
                this.props.onStateChange(this.state, this.state.item);
            }
        });
    }

    renderLowerContent(open) {
        if (!open) {
            return null;
        }

        const { albumInfo, currentUser, tags } = this.props;
        const { isSaving } = this.state;
        const finishText = isSaving ? 'Saving...' : 'Save and Close';

        const { item, errors } = this.state;
        const allErrors = {
            ...errors,
            ...this.props.saveRequestErrors
        };
        const albumTrackOnly =
            typeof item.album_track_only === 'undefined' ||
            yesBool(item.album_track_only);

        const tagOptions = getTagOptions(tags);
        const userTags = getUserTags(tags.id[item.song_id]);

        let upc;

        if (item.upc) {
            upc = (
                <LabeledInput
                    id={`upc-${item.song_id}`}
                    label="UPC"
                    name="upc"
                    onChange={this.handleInputChange}
                    tooltip="Album UPC code"
                    type={labeledInputTypes.text}
                    value={item.upc}
                    disabled
                />
            );
        }

        let monetization = (
            <section className={styles.inputGroup}>
                <LabeledInput
                    id={`isrc-${item.song_id}`}
                    label="ISRC"
                    name="isrc"
                    onChange={this.handleInputChange}
                    tooltip="Enter ISRC code if this item has one"
                    type={labeledInputTypes.text}
                    value={item.isrc}
                />

                {upc}
            </section>
        );

        if (albumInfo.amp) {
            monetization = (
                <Fragment>
                    <LabeledInput
                        checked={yesBool(item.video_ad)}
                        heading
                        id={`track-monetize-${item.song_id}`}
                        label="Monetize"
                        name="video_ad"
                        onChange={this.handleInputChange}
                        type={labeledInputTypes.toggle}
                        value={currentUser.profile.label_name}
                    />

                    <section className={styles.inputGroup}>
                        <LabeledInput
                            id={`isrc-${item.song_id}`}
                            label="ISRC"
                            name="isrc"
                            onChange={this.handleInputChange}
                            tooltip="Enter ISRC code if this item has one"
                            type={labeledInputTypes.text}
                            value={item.isrc}
                        />

                        {upc}

                        <LabeledInput
                            id={`accounting_code-${item.song_id}`}
                            label="Accounting code"
                            name="accounting_code"
                            onChange={this.handleInputChange}
                            tooltip="Optional. You can use this to enter a this items code in your accounting system."
                            type={labeledInputTypes.text}
                            value={item.accounting_code}
                        />
                    </section>
                </Fragment>
            );
        }

        return (
            <section className={styles.drawer}>
                <header className={styles.drawerHeader}>
                    <h3 className={styles.drawerHeading}>Basic Information</h3>

                    <LabeledInput
                        checked={!albumTrackOnly}
                        className={styles.singleToggle}
                        id={`save_as_single-${item.song_id}`}
                        label="Display as a single on your profile"
                        name="save_as_single"
                        onChange={this.handleInputChange}
                        showText
                        textExtraclassNames="song-upload__save-as-single--normal-text"
                        tooltip="The track will be shown as a single as well."
                        type={labeledInputTypes.toggle}
                    />
                </header>

                <LabeledInput
                    error={allErrors.artist}
                    id={`artist-${item.song_id}`}
                    label="Artist"
                    maxLength={75}
                    name="artist"
                    onChange={this.handleInputChange}
                    type={labeledInputTypes.text}
                    value={item.artist || ''}
                    required
                />

                <LabeledInput
                    error={allErrors.title}
                    id={`title-${item.song_id}`}
                    label="Song Title"
                    maxLength={75}
                    name="title"
                    onChange={this.handleInputChange}
                    type={labeledInputTypes.text}
                    value={item.title || ''}
                    required
                />

                <section className={styles.inputGroup}>
                    <LabeledInput
                        id={`featuring-${item.song_id}`}
                        label="Featuring"
                        name="featuring"
                        onChange={this.handleInputChange}
                        onResize={this.props.onTagListResize}
                        subtitle="(Type the artists and hit the enter button)"
                        tagListAdditionalHeight={
                            (this.props.tagListAdditionalHeights &&
                                this.props.tagListAdditionalHeights
                                    .featuring) ||
                            0
                        }
                        type={labeledInputTypes.tags}
                        value={item.featuring || ''}
                    />

                    <LabeledInput
                        id={`producer-${item.song_id}`}
                        label="Producer(s)"
                        name="producer"
                        onChange={this.handleInputChange}
                        onResize={this.props.onTagListResize}
                        tagListAdditionalHeight={
                            (this.props.tagListAdditionalHeights &&
                                this.props.tagListAdditionalHeights.producer) ||
                            0
                        }
                        type={labeledInputTypes.tags}
                        value={item.producer || ''}
                    />
                </section>

                <div className={styles.tagsLabelRow}>
                    <span className={styles.tagsLabel}>Tags</span>

                    <span className={styles.suggestTag}>
                        Want to{' '}
                        <button
                            className="button-link"
                            onClick={this.props.onSuggestTagClick}
                        >
                            suggest a new tag?
                        </button>
                    </span>
                </div>

                <section className={styles.inputGroup}>
                    <div className={styles.dropdown}>
                        <Dropdown
                            id={`subgenres-${item.song_id}`}
                            multiple
                            musicId={item.song_id.toString()}
                            name={tagSections.subgenre}
                            onChange={this.props.onDropdownChange}
                            options={tagOptions.subgenres}
                            sections
                            showDeselectButton
                            type="tag"
                            value={userTags.subgenres}
                        />
                    </div>

                    <div className={styles.dropdown}>
                        <Dropdown
                            id={`mood-${item.song_id}`}
                            multiple
                            musicId={item.song_id.toString()}
                            name={tagSections.mood}
                            onChange={this.props.onDropdownChange}
                            options={tagOptions.moods}
                            showDeselectButton
                            type="tag"
                            value={userTags.moods}
                        />
                    </div>
                </section>

                <LabeledInput
                    checked={yesBool(item.explicit)}
                    error={allErrors.explicit}
                    id={`explicit-${item.song_id}`}
                    label="This song is explicit"
                    name="explicit"
                    onChange={this.handleInputChange}
                    type={labeledInputTypes.checkbox}
                />

                {monetization}

                <footer className={styles.drawerFooter}>
                    <button
                        className={classnames(
                            'button',
                            'button--pill',
                            'button--med'
                        )}
                        onClick={this.handleTrackSaveClick}
                        type="button"
                        disabled={isSaving || !this.state.dirty}
                    >
                        {finishText}
                    </button>
                    <div className={styles.secondaryActions}>
                        {this.renderReplaceAudioButton()}
                        {this.renderDeleteButton()}
                    </div>
                </footer>
            </section>
        );
    }

    renderTopRight(status, errors = []) {
        if (!errors.length) {
            return (
                <button
                    className="button button--link song-upload__edit"
                    type="button"
                    onClick={this.handleContentToggle}
                >
                    {this.state.open ? 'Collapse' : 'Edit this track'}
                </button>
            );
        }

        // I dont think we ever reach this??
        const list = errors.map((error, i) => {
            return (
                <p className="u-text-red" key={i}>
                    {error}
                </p>
            );
        });

        return <div className="song-upload__errors">{list}</div>;
    }

    renderDeleteButton() {
        const { onDeleteClick } = this.props;
        const { item } = this.state;
        const albumTrackOnly = yesBool(item.album_track_only);
        const action = albumTrackOnly ? 'Delete' : 'Remove';

        return (
            <div className={styles.deleteButtonWrapper}>
                <button
                    className={styles.deleteButton}
                    data-album-track-only={albumTrackOnly}
                    data-music-id={item.song_id}
                    data-testid="deleteAlbumTrackUpload"
                    data-tooltip={`${action} Album Track`}
                    data-tooltip-bottom-right
                    onClick={onDeleteClick}
                    type="button"
                >
                    <TrashIconSolid />
                </button>
            </div>
        );
    }

    renderReplaceAudioButton() {
        const { item } = this.state;
        const { replaceUploadItem } = this.props;
        const replacing =
            replaceUploadItem &&
            replaceUploadItem.status !== UPLOAD_STATUS.COMPLETED;
        const text = replacing ? 'Replacing...' : 'Replace audio file';

        const input = (
            <input
                id={`replace-song-${item.song_id}`}
                type="file"
                name="songs"
                data-upload-type="song"
                data-replace-id={item.song_id}
                accept="audio/*"
                disabled={replacing}
                onChange={this.props.onAlbumTrackReplaceInputChange}
            />
        );

        return (
            <button className={styles.replaceAudio} type="button">
                {text}
                {input}
            </button>
        );
    }

    renderMetadataToggle(shouldRender) {
        if (!shouldRender) {
            return null;
        }

        return (
            <button
                onClick={this.handleContentToggle}
                className="song-upload__toggle u-text-center"
                type="button"
            >
                <ChevronDownIcon />
            </button>
        );
    }

    renderBytes(bytesUploaded, bytesTotal) {
        if (bytesUploaded === 0 && bytesTotal === 1) {
            return null;
        }

        return (
            <span>
                {formatBytes(bytesUploaded)} of {formatBytes(bytesTotal)} |
            </span>
        );
    }

    renderStatusText(status) {
        const { text } = getUploadStatusData(status, UPLOAD_STATUS);

        return text;
    }

    render() {
        const { item, open } = this.state;
        const { albumInfo, currentUser, trackNumber } = this.props;
        let originalTitle;

        if (item.originalTitle) {
            originalTitle = (
                <span className="song-upload__original-title">
                    {item.originalTitle}
                </span>
            );
        }

        const className = classnames(styles.trackListItemWrapper, {
            [styles.drawerOpen]: open,
            [styles.lastItem]: albumInfo.tracks.length === trackNumber
        });

        let contentToggleIcon;

        if (open) {
            contentToggleIcon = <CloseThinIcon />;
        } else {
            contentToggleIcon = <CogIcon />;
        }

        return (
            <li className={className} data-testid="albumTrackUpload">
                <section className={styles.trackListItem}>
                    <span className={styles.trackNumber}>{trackNumber}.</span>
                    <span className={styles.trackName}>
                        {renderTrackTitle(item, currentUser)} {originalTitle}
                    </span>
                    <button
                        className={styles.trackActionIcon}
                        data-tooltip={open ? 'Close' : 'Edit'}
                        onClick={this.handleContentToggle}
                    >
                        {contentToggleIcon}
                    </button>
                </section>
                {this.renderLowerContent(open)}
            </li>
        );
    }
}
