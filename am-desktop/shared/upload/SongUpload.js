import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { formatBytes, getUploadStatusData } from 'utils/index';

import { UPLOAD_STATUS } from 'constants/index';

import CheckIcon from '../icons/check-mark';
import ChevronDownIcon from '../icons/chevron-down';
import TrashIcon from 'icons/trash-solid';

import AndroidLoader from '../loaders/AndroidLoader';
import SongUploadDrawer from './SongUploadDrawer';
import UploadProgressBar from './UploadProgressBar';

import styles from './SongUpload.module.scss';

export default class SongUpload extends Component {
    static propTypes = {
        uploadItem: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        onDeleteClick: PropTypes.func.isRequired,
        onDropdownChange: PropTypes.func.isRequired,
        onInputChange: PropTypes.func.isRequired,
        onFileInputChange: PropTypes.func.isRequired,
        onFinishClick: PropTypes.func.isRequired,
        releaseDate: PropTypes.object,
        replaceUploadItem: PropTypes.object,
        onReplaceInputChange: PropTypes.func,
        onDateBlur: PropTypes.func,
        onDateChange: PropTypes.func,
        onReleaseOptionChange: PropTypes.func,
        onDrawerToggle: PropTypes.func,
        history: PropTypes.object,
        location: PropTypes.object,
        errors: PropTypes.object,
        descriptionCharsLeft: PropTypes.number,
        songDescriptionMaxChars: PropTypes.number,
        songAdminDescriptionMaxChars: PropTypes.number,
        adminDescriptionCharsLeft: PropTypes.number,
        selectedReleaseOption: PropTypes.string,
        isAutoSaving: PropTypes.bool,
        futureReleaseDate: PropTypes.bool,
        dirty: PropTypes.bool,
        open: PropTypes.bool,
        autosave: PropTypes.bool,
        saveButtonClicked: PropTypes.bool,
        editMode: PropTypes.bool,
        onFinishSocialClick: PropTypes.func,
        showErrorMessage: PropTypes.bool,
        fromMobile: PropTypes.bool,
        onCopyClick: PropTypes.func,
        onPrivateShareClick: PropTypes.func,
        onSuggestTagClick: PropTypes.func,
        onViewSongClick: PropTypes.func,
        newUrlSlug: PropTypes.object,
        uploadPage: PropTypes.bool,
        tags: PropTypes.object,
        addMusicTag: PropTypes.func,
        removeMusicTag: PropTypes.func,
        showTagError: PropTypes.func
    };

    renderReplaceAudioButton() {
        const {
            onReplaceInputChange,
            replaceUploadItem,
            uploadItem,
            uploadPage
        } = this.props;

        if (uploadPage) {
            return null;
        }

        const replacing =
            replaceUploadItem &&
            replaceUploadItem.status !== UPLOAD_STATUS.COMPLETED;
        const text = replacing ? 'Replacing...' : 'Replace audio file';

        const input = (
            <input
                id={`replace-song-${uploadItem.id}`}
                type="file"
                name="songs"
                data-upload-type="song"
                accept="audio/*"
                onChange={onReplaceInputChange}
            />
        );

        return (
            <button className={styles.replaceAudio} type="button">
                {text}
                {input}
            </button>
        );
    }

    renderStatusText(status) {
        const { klass, text } = getUploadStatusData(status, UPLOAD_STATUS);

        let icon;
        let loader;

        if (status === UPLOAD_STATUS.COMPLETED) {
            icon = (
                <span className={classnames(styles.icon, styles.checkIcon)}>
                    <CheckIcon />
                </span>
            );
        }

        if (status === UPLOAD_STATUS.PROCESSING) {
            loader = <AndroidLoader size={20} />;
        }

        return (
            <span className={classnames(styles.progressStatus, klass)}>
                {text} {loader} {icon}
            </span>
        );
    }

    renderDeleteButton() {
        const { onDeleteClick, uploadItem, uploadPage } = this.props;

        if (uploadItem.alreadyExists) {
            return null;
        }

        const className = classnames(styles.deleteButtonWrapper, {
            [styles.showDivider]: !uploadPage
        });

        return (
            <div className={className}>
                <button
                    className={classnames(styles.icon, styles.deleteButton)}
                    data-music-id={uploadItem.id}
                    data-testid="deleteSongUpload"
                    onClick={onDeleteClick}
                >
                    <TrashIcon />
                </button>
            </div>
        );
    }

    renderLowerContent(open) {
        if (!open) {
            return null;
        }

        return (
            <SongUploadDrawer
                saveButtonClicked={this.props.saveButtonClicked}
                autosave={this.props.autosave}
                currentUser={this.props.currentUser}
                uploadItem={this.props.uploadItem}
                history={this.props.history}
                releaseDate={this.props.releaseDate}
                onFinishClick={this.props.onFinishClick}
                className="song-upload__metadata--shadow"
                onDropdownChange={this.props.onDropdownChange}
                onInputChange={this.props.onInputChange}
                onFileInputChange={this.props.onFileInputChange}
                onDateBlur={this.props.onDateBlur}
                onDateChange={this.props.onDateChange}
                selectedReleaseOption={this.props.selectedReleaseOption}
                futureReleaseDate={this.props.futureReleaseDate}
                errors={this.props.errors}
                dirty={this.props.dirty}
                isAutoSaving={this.props.isAutoSaving}
                descriptionCharsLeft={this.props.descriptionCharsLeft}
                songAdminDescriptionMaxChars={
                    this.props.songAdminDescriptionMaxChars
                }
                adminDescriptionCharsLeft={this.props.adminDescriptionCharsLeft}
                onReleaseOptionChange={this.props.onReleaseOptionChange}
                songDescriptionMaxChars={this.props.songDescriptionMaxChars}
                onFinishSocialClick={this.props.onFinishSocialClick}
                showErrorMessage={this.props.showErrorMessage}
                fromMobile={this.props.fromMobile}
                onCopyClick={this.props.onCopyClick}
                onPrivateShareClick={this.props.onPrivateShareClick}
                onSuggestTagClick={this.props.onSuggestTagClick}
                onViewSongClick={this.props.onViewSongClick}
                newUrlSlug={this.props.newUrlSlug}
                uploadPage={this.props.uploadPage}
                tags={this.props.tags}
                addMusicTag={this.props.addMusicTag}
                removeMusicTag={this.props.removeMusicTag}
                showTagError={this.props.showTagError}
            />
        );
    }

    render() {
        const { uploadItem, replaceUploadItem, editMode, open } = this.props;
        const { title, errors, status } = uploadItem;
        let { bytesUploaded = 0, bytesTotal = 1 } = uploadItem;
        const klass = classnames(
            'song-upload',
            'u-box-shadow',
            styles.songUpload,
            {
                'song-upload--open': open || editMode
            }
        );
        let toggleText = 'Edit this track';

        if (open) {
            toggleText = 'Collapse';
        }

        let statusSuffix;

        if (uploadItem.alreadyExists) {
            statusSuffix = (
                <span data-testid="existingSongUpload">
                    (Previously uploaded)
                </span>
            );
        }

        let statusText = (
            <strong className="u-text-green">Completed {statusSuffix}</strong>
        );

        if (replaceUploadItem) {
            bytesUploaded = replaceUploadItem.bytesUploaded;
            bytesTotal = replaceUploadItem.bytesTotal || 1;
            statusText = this.renderStatusText(replaceUploadItem.status);
        }

        let chevron;
        let topRightButton;
        let bytes = (
            <span>
                {formatBytes(bytesUploaded)} of {formatBytes(bytesTotal)} |{' '}
            </span>
        );
        let progress = bytesUploaded / bytesTotal;

        if (!editMode) {
            chevron = (
                <button
                    onClick={this.props.onDrawerToggle}
                    className="song-upload__toggle u-text-center"
                >
                    <ChevronDownIcon />
                </button>
            );

            topRightButton = (
                <button
                    className="button button--link song-upload__edit"
                    onClick={this.props.onDrawerToggle}
                >
                    {toggleText}
                </button>
            );
        } else {
            if (!replaceUploadItem) {
                progress = 1;
                bytes = null;
            }

            topRightButton = (
                <div className={styles.topRight}>
                    {this.renderReplaceAudioButton()}
                    {this.renderDeleteButton()}
                </div>
            );
        }

        const uploadProgressClassName = classnames(
            'upload__progress-wrap',
            'song-upload__progress-wrap',
            styles.uploadProgressWrap
        );

        return (
            <div className={klass} data-testid="songUpload">
                <div className="song-upload__top">
                    <p className="song-upload__title">{title}</p>
                    <p className="song-upload__progress">
                        {bytes}
                        {statusText}
                    </p>
                    {topRightButton}
                    <div className={uploadProgressClassName}>
                        <UploadProgressBar
                            progress={progress}
                            hasError={
                                status === UPLOAD_STATUS.FAILED ||
                                status === UPLOAD_STATUS.CANCELED
                            }
                        />
                    </div>
                    {chevron}
                </div>
                {this.renderLowerContent(open, errors)}
            </div>
        );
    }
}
