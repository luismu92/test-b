import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import requireAuth from '../hoc/requireAuth';
import {
    queuePreExistingTracks,
    setIsAddingSingles
} from '../redux/modules/upload/album';
import { getUserUploads, setQuery } from '../redux/modules/user/uploads';

import FixedActionBarContainer from '../components/FixedActionBarContainer';
import MultiSelectMusicList from '../list/MultiSelectMusicList';
import RoundedSearchInput from '../components/RoundedSearchInput';

import ArrowIcon from 'icons/arrow-next';
import CloseThinIcon from '../icons/close-thin';

import styles from './AddSingleScreen.module.scss';

class AddSingleScreen extends Component {
    static propTypes = {
        currentUserUploads: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired
    };

    state = {
        preSelectedSongs: []
    };

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getUserUploads());
    }

    handleAddSelectionClick = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const { preSelectedSongs } = this.state;

        dispatch(setIsAddingSingles(false));

        dispatch(queuePreExistingTracks(preSelectedSongs));
    };

    handleCancelAddSongsClick = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;

        dispatch(setIsAddingSingles(false));

        this.setState({
            preSelectedSongs: []
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
    };

    handleSelectSong = (item, list) => {
        this.setState({
            preSelectedSongs: list
        });
    };

    handleUploadSearchInput = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setQuery(val));
    };

    render() {
        const { currentUserUploads } = this.props;
        const { preSelectedSongs } = this.state;

        let buttonText = 'Choose songs to add';
        let buttonIcon = null;

        if (preSelectedSongs.length) {
            let suffix = 'track';

            if (preSelectedSongs.length !== 1) {
                suffix = 'tracks';
            }

            buttonText = `Continue with ${preSelectedSongs.length} ${suffix}`;
            buttonIcon = <ArrowIcon className="u-text-icon" />;
        }

        return (
            <div className="row">
                <div className="column small-24">
                    <h3 className={styles.selectCopy}>
                        Select your pre-existing songs you want to add to your
                        album.
                    </h3>
                </div>
                <div className="column small-20 small-offset-2">
                    <RoundedSearchInput
                        onFormSubmit={this.handleFormSubmit}
                        onInputChange={this.handleUploadSearchInput}
                        value={currentUserUploads.query || ''}
                        placeholder="Search your music and select the tracks you want to add to your album"
                    />
                </div>
                <div className="column small-24">
                    <div
                        className="u-box-shadow u-padded u-spacing-top"
                        style={{ paddingTop: 0 }}
                    >
                        <div className="row">
                            <div className="column small-24">
                                <MultiSelectMusicList
                                    list={currentUserUploads.list}
                                    selectedItems={preSelectedSongs}
                                    onSelect={this.handleSelectSong}
                                    loading={currentUserUploads.loading}
                                    shouldRetainSelected
                                    hasExpandableTracks
                                    shouldSeparateTypes
                                />
                            </div>
                        </div>
                    </div>
                    <FixedActionBarContainer>
                        <button
                            className="button button--pill button--med"
                            data-testid="addPreExistingTracks"
                            disabled={preSelectedSongs.length < 1}
                            onClick={this.handleAddSelectionClick}
                        >
                            {buttonText} {buttonIcon}
                        </button>
                        <button
                            className={styles.closeThinIcon}
                            onClick={this.handleCancelAddSongsClick}
                        >
                            <CloseThinIcon />
                        </button>
                    </FixedActionBarContainer>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        currentUserUploads: state.currentUserUploads
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(AddSingleScreen);
