import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {
    formatBytes,
    renderTrackTitle,
    getUploadStatusData
} from 'utils/index';
import { UPLOAD_STATUS } from 'constants/index';

import CheckIcon from '../icons/check-mark';
import CloseIcon from '../icons/close-thin';
import TrashIcon from 'icons/trash-solid';

import AndroidLoader from '../loaders/AndroidLoader';
import UploadProgressBar from './UploadProgressBar';

import styles from './UploadItem.module.scss';

export default class UploadItem extends Component {
    static propTypes = {
        uploadItem: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        onCancelClick: PropTypes.func,
        preExistingTrack: PropTypes.bool,
        uploadPage: PropTypes.bool
    };

    static defaultProps = {
        uploadPage: false
    };

    renderStatusText(status) {
        const { uploadPage } = this.props;
        const { klass, text } = getUploadStatusData(
            status,
            UPLOAD_STATUS,
            uploadPage
        );

        let icon;
        let loader;

        if (uploadPage) {
            icon = this.renderStatusIcon(status);
        }

        if (status === UPLOAD_STATUS.PROCESSING) {
            loader = <AndroidLoader size={20} />;
        }

        return (
            <span className={classnames(styles.progressStatus, klass)}>
                {text} {loader} {icon}
            </span>
        );
    }

    renderBytes(bytesUploaded, bytesTotal, status) {
        if (status === UPLOAD_STATUS.EXISTING) {
            return (
                <div className="song-upload__progress">
                    {this.renderStatusText(status)}
                </div>
            );
        }
        return (
            <div className={styles.progress}>
                {formatBytes(bytesUploaded)} of {formatBytes(bytesTotal)} |{' '}
                {this.renderStatusText(status)}
            </div>
        );
    }

    renderTopRight(status, errors = []) {
        const { uploadPage } = this.props;

        if (!errors.length || uploadPage) {
            return null;
        }

        const list = errors.map((error, i) => {
            return (
                <p className="u-text-red" key={i}>
                    {error}
                </p>
            );
        });

        return <div className={styles.errors}>{list}</div>;
    }

    renderStatusIcon(status) {
        const { onCancelClick, uploadItem, uploadPage } = this.props;

        if (status === UPLOAD_STATUS.COMPLETED) {
            return (
                <span
                    className={classnames(styles.icon, styles.checkIcon)}
                    data-testid="uploadItemCompleted"
                >
                    <CheckIcon />
                </span>
            );
        }

        if (uploadPage) {
            return null;
        }

        if (
            status === UPLOAD_STATUS.FAILED ||
            status === UPLOAD_STATUS.CANCELED
        ) {
            return (
                <span
                    className={classnames(
                        styles.icon,
                        styles.failedIcon,
                        'u-text-red'
                    )}
                >
                    <CloseIcon />
                </span>
            );
        }

        return (
            <button
                className={classnames(styles.icon, styles.closeIcon)}
                data-tooltip="Cancel"
                data-music-id={uploadItem.musicId}
                data-upload-id={uploadItem.id}
                onClick={onCancelClick}
            >
                <CloseIcon />
            </button>
        );
    }

    renderTrashIcon() {
        const { onCancelClick, uploadItem, uploadPage } = this.props;

        if (!uploadPage) {
            return null;
        }

        return (
            <button
                className={classnames(styles.icon, styles.trashIcon)}
                data-music-id={uploadItem.musicId}
                data-upload-id={uploadItem.id}
                onClick={onCancelClick}
            >
                <TrashIcon />
            </button>
        );
    }

    renderProgressBar(bytesUploaded, bytesTotal, status) {
        const { uploadPage } = this.props;
        let progress = bytesUploaded / (bytesTotal || 1);

        if (status === UPLOAD_STATUS.EXISTING) {
            progress = 1;
        }

        if (uploadPage) {
            return (
                <UploadProgressBar
                    progress={progress}
                    hasError={
                        status === UPLOAD_STATUS.FAILED ||
                        status === UPLOAD_STATUS.CANCELED
                    }
                    uploadPage={uploadPage}
                />
            );
        }

        return (
            <div className="upload__progress-wrap song-upload__progress-wrap">
                <UploadProgressBar
                    progress={progress}
                    hasError={
                        status === UPLOAD_STATUS.FAILED ||
                        status === UPLOAD_STATUS.CANCELED
                    }
                />
                {this.renderStatusIcon(status)}
            </div>
        );
    }

    render() {
        const {
            uploadItem,
            currentUser,
            preExistingTrack,
            uploadPage
        } = this.props;
        const { bytesUploaded = 0, bytesTotal = 1, errors } = uploadItem;
        let { status } = uploadItem;

        if (preExistingTrack) {
            status = UPLOAD_STATUS.EXISTING;
        }

        const className = classnames(styles.container, {
            [styles.uploadPage]: uploadPage
        });

        return (
            <div className={className} data-testid="uploadItem">
                <p className={styles.title}>
                    {renderTrackTitle(uploadItem, currentUser)}
                </p>
                {this.renderTrashIcon()}
                {this.renderBytes(bytesUploaded, bytesTotal, status)}
                {this.renderTopRight(status, errors)}
                {this.renderProgressBar(bytesUploaded, bytesTotal, status)}
            </div>
        );
    }
}
