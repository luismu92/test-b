import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { NavLink, Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    renderCappedDisplayCount,
    shouldShowDashboard,
    shouldShowAdmin
} from 'utils/index';

import HomeIcon from '../icons/home';
import GraphIcon from '../icons/graph';
import FireIcon from '../icons/fire';
import AlbumsIcon from '../icons/albums';
import PlaylistIcon from '../icons/playlist';
import RecentIcon from '../icons/recent';
import UserIcon from '../icons/avatar';
import FeedIcon from '../icons/feed';
import HeartIcon from 'icons/heart';
import PlusIcon from '../icons/plus';
import InboxOutIcon from '../icons/inbox-out';
import CogIcon from '../icons/cog';
// import ChartLineIcon from '../icons/chart-line';
import WrenchIcon from '../icons/wrench';
import DiscIcon from '../icons/disc';
import BarChartIcon from '../icons/bar-chart';
import MessageIcon from '../icons/message';
import CommentIcon from 'icons/comment';
import FlagIcon from 'icons/flag';
import HelpIcon from '../icons/help';
import PowerIcon from '../icons/power';
import AmWorld from '../icons/am-world';

import Avatar from '../components/Avatar';
import AppBadge from '../components/AppBadge';
import SocialLinks from '../components/SocialLinks';
import Verified from 'components/Verified';

export default class Sidebar extends Component {
    static propTypes = {
        onLogoutClick: PropTypes.func,
        onModalClick: PropTypes.func,
        onTextModalClick: PropTypes.func,
        activeGenre: PropTypes.string,
        currentUser: PropTypes.object,
        location: PropTypes.object
    };

    static defaultProps = {
        onModalClick() {}
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderAvatarBlock() {
        const { currentUser, location } = this.props;

        const verified = <Verified user={currentUser.profile} size={16} />;

        let button = (
            <Link className="button button--pill" to="/edit/profile">
                Edit Profile
            </Link>
        );

        if (location.pathname.match(/^\/edit\/profile/)) {
            button = (
                <Link
                    className="button button--pill"
                    to={`/artist/${currentUser.profile.url_slug}`}
                >
                    View Profile
                </Link>
            );
        }

        return (
            <div className="dashboard-sidebar__avatar-wrap site-sidebar__widget widget u-text-center">
                <div className="u-pos-relative">
                    <Avatar
                        type="artist"
                        className="dashboard-sidebar__avatar"
                        image={
                            currentUser.profile.image_base ||
                            currentUser.profile.image
                        }
                        size={80}
                        rounded
                    />
                    {verified}
                </div>
                <h2 className="dashboard-sidebar__name u-trunc">
                    {currentUser.profile.name}
                </h2>
                {button}
            </div>
        );
    }

    renderDashboardLinks() {
        const { currentUser, location } = this.props;
        const ampPartner = currentUser.profile.label_owner;
        const labelManager = currentUser.profile.label_manage;
        const isAdmin = currentUser.profile.is_admin;

        let monetizationSubLinks = [];

        if (ampPartner && location.pathname.indexOf('/monetization') === 0) {
            monetizationSubLinks = [
                {
                    text: 'Payment Info',
                    href: '/monetization/info',
                    sublink: true
                },
                // {
                //     text: 'Publishing Royalties',
                //     href: '/monetization/royalties',
                //     sublink: true
                // },
                {
                    text: 'Payment History',
                    href: '/monetization/history',
                    sublink: true
                },
                {
                    text: 'AMP FAQ',
                    href: '/monetization/faq',
                    sublink: true
                }
            ];
        }

        const statsSubLinks = [
            {
                text: 'Geography',
                href: '/dashboard/geography/countries',
                sublink: true
            },
            {
                text: 'Sources',
                href: '/dashboard/play-sources',
                sublink: true
            },
            {
                text: 'Fans',
                href: '/dashboard/fans',
                sublink: true,
                isActive: (match, loc) => {
                    return (
                        loc.pathname.match(/^\/dashboard\/fans/) ||
                        loc.pathname.match(/^\/dashboard\/influencers/)
                    );
                }
            }
        ];

        const items = [
            {
                text: 'Dashboard',
                href: '/dashboard',
                icon: <CogIcon className="widget__icon" />
            },
            // {
            //     text: 'Stats',
            //     href: '/dashboard/stats',
            //     icon: <ChartLineIcon className="widget__icon" />
            // },
            ...statsSubLinks,
            {
                text: 'Manage Content',
                href: '/dashboard/manage',
                icon: <WrenchIcon className="widget__icon" />
            },
            labelManager
                ? {
                      text: 'Label Management',
                      href: '/dashboard/label',
                      icon: <DiscIcon className="widget__icon" />
                  }
                : null,
            isAdmin
                ? {
                      text: 'Flagged Comments',
                      href: '/dashboard/comment',
                      icon: <FlagIcon className="widget__icon" />
                  }
                : null,
            isAdmin
                ? {
                      text: 'Recent Comments',
                      href: '/dashboard/comment-recent',
                      icon: <CommentIcon className="widget__icon" />
                  }
                : null,
            ampPartner
                ? {
                      text: 'Monetization',
                      href: '/monetization',
                      icon: <BarChartIcon className="widget__icon" />
                  }
                : null,
            ...monetizationSubLinks,
            {
                text: 'Notifications',
                href: '/notifications',
                icon: <MessageIcon className="widget__icon" />
            },
            {
                text: 'Support',
                href: '/contact-us',
                icon: <HelpIcon className="widget__icon" />
            },
            {
                text: 'Sign out',
                href: '/logout',
                icon: <PowerIcon className="widget__icon" />
            }
        ]
            .filter(Boolean)
            .map((item, i) => {
                let link;

                switch (item.href) {
                    case '/contact-us':
                        link = (
                            <Link
                                to="/contact-us"
                                onClick={this.props.onModalClick}
                                data-page="contact"
                                className="vertical-menu__link"
                            >
                                {item.icon} {item.text}
                            </Link>
                        );
                        break;

                    case '/logout':
                        link = (
                            <button
                                className="vertical-menu__link"
                                onClick={this.props.onLogoutClick}
                            >
                                {item.icon} {item.text}
                            </button>
                        );
                        break;

                    default: {
                        const klass = classnames('vertical-menu__link', {
                            'vertical-menu__link--sublink': item.sublink
                        });

                        link = (
                            <NavLink
                                exact
                                to={item.href}
                                className={klass}
                                activeClassName="vertical-menu__link--current"
                                // eslint-disable-next-line react/jsx-no-bind
                                isActive={
                                    item.isActive ||
                                    ((match, loc) => loc.pathname === item.href)
                                }
                            >
                                {item.icon} {item.text}
                            </NavLink>
                        );
                        break;
                    }
                }

                return <li key={i}>{link}</li>;
            });

        return (
            <div className="site-sidebar__widget widget">
                <ul className="dashboard-sidebar__links vertical-menu">
                    {items}
                </ul>
            </div>
        );
    }

    renderAdminSection() {
        const defaultIcon = <CogIcon className="widget__icon" />;
        const items = [
            {
                text: 'Reserved Emails',
                href: '/the-backwoods/reserved-email',
                icon: defaultIcon
            },
            {
                text: 'Follow List',
                href: '/the-backwoods/follow-list',
                icon: defaultIcon
            },
            {
                text: 'Song Migration',
                href: '/the-backwoods/song-stats-migration',
                icon: defaultIcon
            },
            {
                text: 'Trending',
                href: '/the-backwoods/trending',
                icon: defaultIcon
            },
            {
                text: 'Accounts',
                href: '/the-backwoods/accounts/dashboard',
                icon: defaultIcon
            },
            {
                text: 'Music Admin',
                href: '/the-backwoods/music',
                icon: defaultIcon
            },
            {
                text: 'Recent',
                href: '/the-backwoods/recent',
                icon: defaultIcon
            },
            {
                text: 'Unplayable Music',
                href: '/the-backwoods/unplayable-tracks',
                icon: defaultIcon
            },
            {
                text: 'Authenticate Artist',
                href: '/the-backwoods/authenticate',
                icon: defaultIcon
            },
            {
                text: 'Image resize',
                href: '/the-backwoods/image-resize',
                icon: defaultIcon
            }
        ].map((item, i) => {
            return (
                <li key={i}>
                    <NavLink
                        exact
                        to={item.href}
                        className="vertical-menu__link"
                        activeClassName="vertical-menu__link--current"
                        // eslint-disable-next-line react/jsx-no-bind
                        isActive={(match, location) =>
                            location.pathname === item.href
                        }
                    >
                        {item.icon} {item.text}
                    </NavLink>
                </li>
            );
        });

        return (
            <nav className="site-sidebar__widget widget">
                <h3 className="widget-title">Admin</h3>
                <ul className="vertical-menu">{items}</ul>
            </nav>
        );
    }

    renderBrowseSection(activeGenre) {
        let genre = '';

        if (activeGenre) {
            genre = `/${activeGenre}`;
        }

        const items = [
            {
                text: 'Home',
                href: '/',
                icon: <HomeIcon className="widget__icon" />
            },
            {
                text: 'Trending',
                href: '/trending-now',
                icon: <GraphIcon className="widget__icon" />
            },
            {
                text: 'Top Songs',
                href: `${genre}/songs/week`,
                icon: <FireIcon className="widget__icon" />
            },
            {
                text: 'Top Albums',
                href: `${genre}/albums/week`,
                icon: <AlbumsIcon className="widget__icon" />
            },
            {
                text: 'Playlists',
                href: '/playlists/browse',
                icon: <PlaylistIcon className="widget__icon" />,
                inexact: true
            },
            {
                text: 'Accounts to Follow',
                href: '/artists/popular',
                icon: <UserIcon className="widget__icon" />
            },
            {
                text: 'Recently Added',
                href: `${genre}/recent`,
                icon: <RecentIcon className="widget__icon" />
            }
        ].map((item, i) => {
            return (
                <li key={i}>
                    <NavLink
                        exact={!item.inexact}
                        to={item.href}
                        className="vertical-menu__link"
                        activeClassName="vertical-menu__link--current"
                        // eslint-disable-next-line react/jsx-no-bind
                        isActive={(match, location) =>
                            location.pathname === item.href
                        }
                    >
                        {item.icon} {item.text}
                    </NavLink>
                </li>
            );
        });

        return (
            <nav className="site-sidebar__widget widget">
                <h3 className="widget-title">Browse</h3>
                <ul className="vertical-menu">
                    {items}
                    <li>
                        <Link to="/world" className="vertical-menu__link">
                            <AmWorld className="widget__icon widget__icon--am-world" />
                            Audiomack World
                        </Link>
                    </li>
                </ul>
            </nav>
        );
    }

    renderMyLibrary(profile) {
        if (!profile) {
            return null;
        }

        const feedLink = `/artist/${profile.url_slug}/feed`;
        const favoritesLink = `/artist/${profile.url_slug}/favorites`;
        const playlistsLink = `/artist/${profile.url_slug}/playlists`;
        const uploadsLink = `/artist/${profile.url_slug}/uploads`;

        return (
            <nav className="site-sidebar__widget widget">
                <h3 className="widget-title">My Library</h3>
                <ul className="vertical-menu">
                    <li>
                        <NavLink
                            to={feedLink}
                            className="vertical-menu__link vertical-menu__link--condensed"
                            activeClassName="vertical-menu__link--current"
                            // eslint-disable-next-line react/jsx-no-bind
                            isActive={(match, location) =>
                                location.pathname === feedLink
                            }
                            data-alert-count={renderCappedDisplayCount(
                                profile.new_feed_items
                            )}
                        >
                            <FeedIcon className="widget__icon" /> Feed
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            exact
                            to={favoritesLink}
                            className="vertical-menu__link vertical-menu__link--condensed"
                            activeClassName="vertical-menu__link--current"
                            // eslint-disable-next-line react/jsx-no-bind
                            isActive={(match, location) =>
                                location.pathname === favoritesLink
                            }
                        >
                            <HeartIcon className="widget__icon" /> Favorites
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            exact
                            to={playlistsLink}
                            className="vertical-menu__link vertical-menu__link--condensed"
                            activeClassName="vertical-menu__link--current"
                            // eslint-disable-next-line react/jsx-no-bind
                            isActive={(match, location) =>
                                location.pathname === playlistsLink
                            }
                        >
                            <PlusIcon className="widget__icon" /> Playlists
                        </NavLink>
                    </li>

                    <li>
                        <NavLink
                            exact
                            to={uploadsLink}
                            className="vertical-menu__link vertical-menu__link--condensed"
                            activeClassName="vertical-menu__link--current"
                            // eslint-disable-next-line react/jsx-no-bind
                            isActive={(match, location) =>
                                location.pathname === uploadsLink
                            }
                        >
                            <InboxOutIcon className="widget__icon" /> Uploads
                        </NavLink>
                    </li>
                </ul>
            </nav>
        );
    }

    renderAppBadges() {
        return (
            <ul className="app-badges site-sidebar__app-badges u-text-center">
                <li>
                    <AppBadge
                        type="sms"
                        onClick={this.props.onTextModalClick}
                    />
                </li>
                <li>
                    <AppBadge type="ios" style="image" />
                </li>
                <li>
                    <AppBadge type="android" style="image" />
                </li>
            </ul>
        );
    }

    renderSocialLinks() {
        const links = {
            twitter: 'audiomack',
            facebook: 'https://www.facebook.com/audiomack',
            instagram: 'audiomack',
            twitch: 'audiomack'
        };

        return (
            <SocialLinks
                links={links}
                className="u-text-center u-spacing-bottom-20"
                allowNonVerified
            />
        );
    }

    renderFooterLinks() {
        const { location } = this.props;

        return (
            <ul className="menu site-sidebar__menu u-text-center">
                <li
                    className={
                        process.env.NODE_ENV !== 'development'
                            ? 'mobile-only'
                            : ''
                    }
                >
                    <a href={`${location.pathname}?showDesktop=false`}>
                        Mobile Website
                    </a>
                </li>
                <li>
                    <Link to="/world">Blog</Link>
                </li>
                <li>
                    <Link to="/about/wordpress">Wordpress</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
                <li>
                    <Link
                        to="/contact-us"
                        onClick={this.props.onModalClick}
                        data-page="contact"
                    >
                        Business Inquiries
                    </Link>
                </li>
                <li>
                    <a
                        href="https://audiomack.zendesk.com"
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        Support
                    </a>
                </li>
                <li>
                    <a
                        href="https://styleguide.audiomack.com"
                        target="_blank"
                        rel="noopener"
                    >
                        Style Guide
                    </a>
                </li>
                <li>
                    <a
                        href={`${
                            process.env.AM_URL
                        }/audiomack-2.0-media-kit.zip`}
                        download="audiomack-2.0-media-kit.zip"
                    >
                        Press Kit
                    </a>
                </li>
                <li>
                    <Link to="/about/terms-of-service">Terms of Service</Link>
                </li>
                <li>
                    <Link to="/about/privacy-policy#accessing-updating">
                        Do not sell my info
                    </Link>
                </li>
                <li>
                    <Link to="/about/privacy-policy">Privacy Policy</Link>
                </li>
                <li>
                    <Link
                        to="/about/legal"
                        onClick={this.props.onModalClick}
                        data-page="legal"
                    >
                        Legal &amp; DMCA
                    </Link>
                </li>
            </ul>
        );
    }

    renderSideBarContent() {
        const { activeGenre, location, currentUser } = this.props;
        const {
            currentUser: { profile }
        } = this.props;
        const showDashboard = shouldShowDashboard(location, currentUser);
        const showAdmin = shouldShowAdmin(location, currentUser);

        if (showDashboard) {
            return (
                <Fragment>
                    {this.renderAvatarBlock()}
                    {this.renderDashboardLinks()}
                    <div className="site-sidebar__meta">
                        {this.renderAppBadges()}
                    </div>
                </Fragment>
            );
        }
        if (showAdmin) {
            return this.renderAdminSection();
        }

        return (
            <Fragment>
                {this.renderBrowseSection(activeGenre)}
                {this.renderMyLibrary(profile)}
                <div className="site-sidebar__meta">
                    {this.renderAppBadges()}
                    {this.renderSocialLinks()}
                    {this.renderFooterLinks()}
                </div>
            </Fragment>
        );
    }

    render() {
        const content = this.renderSideBarContent();

        return (
            <div
                id="site-sidebar"
                className="site-sidebar site-sidebar--main u-scrollable"
            >
                <div className="site-sidebar__inner">{content}</div>
            </div>
        );
    }
}
