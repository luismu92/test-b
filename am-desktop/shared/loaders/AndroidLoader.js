import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class AndroidLoader extends Component {
    static propTypes = {
        className: PropTypes.string,
        color: PropTypes.string,
        center: PropTypes.bool,
        size: PropTypes.number
    };

    static defaultProps = {
        strokeWidth: 3,
        size: 54,
        color: '#ffa200'
    };

    render() {
        const scale = this.props.size / AndroidLoader.defaultProps.size;
        const size = AndroidLoader.defaultProps.size;
        const containerStyle = {
            width: `${size}px`,
            height: `${size}px`,
            transform: `scale(${scale})`
        };

        const style = {
            width: `${AndroidLoader.defaultProps.size}px`,
            height: `${AndroidLoader.defaultProps.size}px`
        };

        const klass = classnames('loader-container', {
            'loader-container--center': this.props.center,
            [this.props.className]: this.props.className
        });

        return (
            <div className={klass} style={containerStyle}>
                <svg className="loader-android" style={style}>
                    <circle
                        className="loader-android__path"
                        stroke={this.props.color}
                        cx="27"
                        cy="27"
                        r="20"
                        fill="none"
                        strokeWidth="4"
                        strokeMiterlimit="10"
                    />
                </svg>
            </div>
        );
    }
}
