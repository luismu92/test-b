import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { shouldNotShowAd } from 'utils/index';
import FreestarContainer from 'components/ad/FreestarContainer';
import GooglePublisherTag from 'components/ad/GooglePublisherTag';

export default class InContentAd extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        // A unique key usually an index within a list
        adCount: PropTypes.number.isRequired,
        showAlternate: PropTypes.bool
    };

    render() {
        const { location, currentUser, showAlternate, adCount } = this.props;
        const disabled = shouldNotShowAd(location.pathname, currentUser);
        const extraData = {
            isUploader:
                currentUser.isLoggedIn &&
                currentUser.profile.upload_count_excluding_reups > 0
        };

        if (
            showAlternate &&
            location.pathname.startsWith('/playground/freestar')
        ) {
            let freestarData;

            // Freestar wants us to do this a little custom
            if (extraData.isUploader) {
                freestarData = {
                    Category: 'uploader'
                };
            }

            return (
                <FreestarContainer
                    location={location}
                    type="incontent"
                    disabled={disabled}
                    slotSuffix={`incontent${adCount}`}
                    extraData={freestarData}
                />
            );
        }

        const slot = {
            adUnitPath: '/72735579/HTTPS-Desktop-300x250-InContent',
            sizes: [[300, 250]],
            divId: `div-gpt-ad-1554930699208-${adCount}`
        };
        const props = {
            slot,
            disabled,
            targetingKey: 'uploader',
            targetingValue: extraData.isUploader ? 'Yes' : 'No',
            containerWidth: 'auto'
        };

        return <GooglePublisherTag {...props} />;
    }
}
