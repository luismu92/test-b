import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { renderFeaturingLinks, buildDynamicImage, shuffle } from 'utils/index';
import analyics, {
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';
export default class MusicFeedBlocks extends Component {
    static propTypes = {
        featuredItems: PropTypes.array
    };

    constructor(props) {
        super(props);

        this.state = {
            isMounted: false
        };
    }

    componentDidMount() {
        // eslint-disable-next-line
        this.setState({
            isMounted: true
        });
    }

    shouldComponentUpdate(nextProps) {
        const differentFeaturedItems =
            JSON.stringify(this.props.featuredItems) !==
            JSON.stringify(nextProps.featuredItems);

        return differentFeaturedItems;
    }

    ////////////////////
    // Event Handlers //
    ////////////////////

    handleHouseAdClick = (e) => {
        const button = e.currentTarget;
        const ad = button.getAttribute('data-ad');

        const action =
            ad === 'app' ? eventAction.appLinkClick : eventAction.amWorld;

        analyics.track(eventCategory.ad, {
            eventAction: action,
            eventLabel: eventLabel.featuredAd
        });
    };

    renderFeaturedItem(feature, key) {
        if (!feature || !feature.ref) {
            return null;
        }

        const imageSize = 265;
        const artwork = buildDynamicImage(
            feature.ref.image_base || feature.ref.image,
            {
                width: imageSize,
                height: imageSize,
                max: true
            }
        );

        const retinaArtwork = buildDynamicImage(
            feature.ref.image_base || feature.ref.image,
            {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            }
        );
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        const itemArtwork = (
            <img
                src={artwork}
                srcSet={srcSet}
                alt={feature.ref.title}
                loading="lazy"
            />
        );

        let itemLink;

        if (feature.ref.type !== 'playlist') {
            itemLink = `/${feature.ref.type}/${feature.ref.uploader.url_slug}/${
                feature.ref.url_slug
            }`;
        } else {
            itemLink = `/playlist/${feature.ref.artist.url_slug}/${
                feature.ref.url_slug
            }`;
        }

        let artistText;

        if (feature.ref.type !== 'playlist') {
            artistText = (
                <span className="music__heading--artist u-trunc u-d-block">
                    {feature.ref.artist}
                </span>
            );
        }

        let itemFeat;

        if (feature.ref.featuring) {
            itemFeat = (
                <span className="u-trunc u-ls-n-05 u-lh-13 u-d-block">
                    <strong>
                        {renderFeaturingLinks(feature.ref.featuring)}
                    </strong>
                </span>
            );
        }

        let trackCount;
        let itemUploader;

        if (feature.ref.type === 'playlist') {
            trackCount = (
                <span className="u-trunc u-ls-n-05 u-lh-15 u-fw-600 u-d-block">
                    Total Songs:{' '}
                    <span className="u-text-orange">
                        {feature.ref.track_count}
                    </span>
                </span>
            );

            itemUploader = (
                <span className="u-trunc u-ls-n-05 u-lh-13 u-fw-600 u-d-block">
                    by{' '}
                    <Link to={`/artist/${feature.ref.artist.url_slug}`}>
                        {feature.ref.artist.name}
                    </Link>
                </span>
            );
        }

        let type = feature.ref.type;

        if (feature.ref.genre === 'podcast') {
            type = 'podcast';
        }

        return (
            <div className="column" style={{ padding: '0 10px' }} key={key}>
                <div className="music-card-container u-box-shadow">
                    <div className="music-card music-card--featured u-pos-relative">
                        <div className="music-card__artwork u-d-block u-pos-relative">
                            <Link to={itemLink}>{itemArtwork}</Link>
                        </div>
                        <div className="music-card__content u-padding-em">
                            <p className="music-card__tag u-tt-uppercase u-fs-11 u-fw-700 u-text-orange">
                                Featured {type}
                            </p>
                            <h2 className="music__heading">
                                <Link to={itemLink} className="u-text-dark">
                                    {artistText}
                                    <span className="music__heading--title u-trunc u-d-block">
                                        {feature.ref.title}
                                    </span>
                                </Link>
                            </h2>
                            {itemFeat}
                            {trackCount}
                            {itemUploader}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { featuredItems } = this.props;
        const { isMounted } = this.state;

        if (isMounted) {
            const blocks = shuffle(featuredItems)
                .slice(0, 4)
                .map((item, key) => {
                    return this.renderFeaturedItem(item, key);
                });

            return (
                <div className="row" style={{ margin: '-20px -10px 60px' }}>
                    {blocks}
                </div>
            );
        }

        return null;
    }
}
