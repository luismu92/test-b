import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CloseIcon from '../icons/close-thin';

import analyics, { eventCategory } from 'utils/analytics';

export default class UploadStartAd extends Component {
    static propTypes = {
        linkHref: PropTypes.string.isRequired,
        eventAction: PropTypes.string.isRequired,
        eventLabel: PropTypes.string.isRequired,
        className: PropTypes.string,
        adRef: PropTypes.string,
        img: PropTypes.string,
        srcImg: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };
    }

    handlePartnerClick = () => {
        const { eventLabel, eventAction } = this.props;

        analyics.track(eventCategory.ad, {
            eventAction: eventAction,
            eventLabel: eventLabel
        });

        console.log(eventAction);
    };

    handleClosePartnerClick = () => {
        this.setState({ visible: false });
    };

    render() {
        const { linkHref, className, adRef, img, srcImg } = this.props;
        const klass = classnames(`partner partner--${adRef}`, {
            [className]: className
        });

        if (!this.state.visible) {
            return null;
        }

        const ClosePartner = (
            <CloseIcon
                className="partner__close"
                onClick={this.handleClosePartnerClick}
            />
        );

        const imgProps = {
            src: img,
            srcSet: `${srcImg} 2x`,
            alt: adRef
        };

        const layout = (
            <div className={klass}>
                <span className="partner__sponsor-tag">Advertisement</span>
                {ClosePartner}
                <a
                    onClick={this.handlePartnerClick}
                    href={linkHref}
                    target="_blank"
                    rel="nofollow noopener"
                >
                    {/* eslint-disable-next-line jsx-a11y/alt-text */}
                    <img {...imgProps} />
                </a>
            </div>
        );

        return layout;
    }
}
