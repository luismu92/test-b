/* global test, expect */
import React from 'react';
import FixedAd from '../FixedAd';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

Enzyme.configure({ adapter: new Adapter() });

test('should not show on certain routes', () => {
    const dontShowOnRoutes = [
        '/about',
        '/upload',
        '/login',
        '/amp',
        '/join',
        '/forgot-password',
        '/dashboard',
        '/dashboard/stats',
        '/dashboard/manage',
        '/upload/songs',
        '/upload/albums',
        '/edit/pins',
        '/edit/song/324',
        '/edit/album/234223',
        '/stats/music/24123',
        '/notifications',
        '/monetization',
        '/monetization/2017/8',
        '/artist/macli-45',
        '/artist/macli-45/uploads',
        '/premium-partner-agreement',
        '/switch-account',
        '/oauth/authenticate',
        '/monetization/info',
        '/monetization/history',
        '/playlist/justa-test/new-playlist-by-another-user',
        '/playground/freestar-noads'
    ];

    dontShowOnRoutes.forEach((path) => {
        const location = {
            pathname: path
        };
        const currentUser = {
            isLoggedIn: false
        };
        const wrapper = shallow(
            <FixedAd currentUser={currentUser} location={location} disabled />
        );
        const wrapperIsNull = wrapper.html() === '';

        if (!wrapperIsNull) {
            console.log('Wrong for path', path);
        }

        expect(wrapperIsNull).toBe(true);
    });
});

test('should not show when disabled is false', () => {
    const location = {
        pathname: '/'
    };
    const currentUser = {
        isLoggedIn: false
    };

    const wrapper = shallow(
        <FixedAd
            currentUser={currentUser}
            location={location}
            disabled={false}
        />
    );
    const wrapperIsNull = wrapper.html() === '';

    expect(wrapperIsNull).toBe(true);
});
