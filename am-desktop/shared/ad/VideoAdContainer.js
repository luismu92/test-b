import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { addToast } from '../redux/modules/toastNotification';

import VideoAdContainer from 'components/ad/VideoAdContainer';

class VideoAdContainerWrapper extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        isEmbed: PropTypes.bool
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAdStarted = () => {
        this.props.dispatch(
            addToast({
                action: 'message',
                item: 'Advertisement: Your content will begin shortly'
            })
        );
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        return (
            <VideoAdContainer
                onAdStarted={this.handleAdStarted}
                location={this.props.location}
                isEmbed={this.props.isEmbed}
                showSkipButton
            />
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(VideoAdContainerWrapper);
