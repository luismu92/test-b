import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { shouldNotShowAd } from 'utils/index';
import FreestarContainer from 'components/ad/FreestarContainer';
import GooglePublisherTag from 'components/ad/GooglePublisherTag';

import styles from './FixedAd.module.scss';

export default class FixedAd extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        showAlternate: PropTypes.bool
    };

    render() {
        const { currentUser, location, showAlternate } = this.props;
        const disabled = shouldNotShowAd(location.pathname, currentUser);
        const isUploader =
            currentUser.isLoggedIn &&
            currentUser.profile.upload_count_excluding_reups;
        const extraData = {
            uploader: isUploader > 0 ? 'Yes' : 'No'
        };
        let content;

        if (showAlternate) {
            let freestarData;

            // Freestar wants us to do this a little custom
            if (isUploader) {
                freestarData = {
                    Category: 'uploader'
                };
            }
            content = (
                <FreestarContainer
                    location={location}
                    type="leaderboard"
                    disabled={disabled}
                    slotSuffix="fixedAd"
                    extraData={freestarData}
                />
            );
        } else {
            // This placeholder doesnt exist
            // const placeholder = '/static/images/desktop/am-partner-placeholder-728x90.jpg';
            const slot = {
                adUnitPath: '/72735579/HTTPS-Desktop-728x90',
                sizes: [[728, 90]],
                divId: 'div-gpt-ad-1547238469056-0'
            };
            const props = {
                slot,
                disabled
            };

            props.targetingKey = 'uploader';
            props.targetingValue = extraData.uploader;
            // props.placeholder = placeholder;

            content = <GooglePublisherTag {...props} />;
        }

        // Let ad tag handle its own disabled state
        if (disabled) {
            return content;
        }

        const klass = classnames(styles.container, 'u-text-center');

        return <div className={klass}>{content}</div>;
    }
}
