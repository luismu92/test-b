import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { shouldNotShowAd } from 'utils/index';
import FreestarContainer from 'components/ad/FreestarContainer';
import GooglePublisherTag from 'components/ad/GooglePublisherTag';

export default class SquareAd extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        showAlternate: PropTypes.bool,
        adCount: PropTypes.number
    };

    static defaultProps = {
        adCount: 1
    };

    render() {
        const { location, currentUser, showAlternate, adCount } = this.props;
        const disabled = shouldNotShowAd(location.pathname, currentUser);
        const extraData = {
            isUploader:
                currentUser.isLoggedIn &&
                currentUser.profile.upload_count_excluding_reups > 0
        };

        if (
            showAlternate &&
            location.pathname.startsWith('/playground/freestar')
        ) {
            return (
                <FreestarContainer
                    location={location}
                    type="incontent"
                    extraData={extraData}
                    slotSuffix={`squaread${adCount}`}
                    disabled={disabled}
                />
            );
        }

        const placeholder =
            '/static/images/desktop/am-partner-placeholder-300x250.jpg';
        const slot = {
            adUnitPath: '/72735579/HTTPS-Desktop-300x250',
            sizes: [[300, 250]],
            divId: `div-gpt-ad-1522415358206-${adCount}`
        };
        const props = {
            slot,
            disabled
        };

        props.targetingKey = 'uploader';
        props.targetingValue = extraData.isUploader ? 'Yes' : 'No';
        props.placeholder = placeholder;

        return <GooglePublisherTag {...props} />;
    }
}
