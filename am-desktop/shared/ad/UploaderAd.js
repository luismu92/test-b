import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CloseIcon from '../icons/close-thin';

import analyics, {
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';

export default class UploaderAd extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        className: PropTypes.string,
        containerClass: PropTypes.string,
        visible: PropTypes.bool,
        onClose: PropTypes.func,
        location: PropTypes.object.isRequired
    };

    handleLinkClick = () => {
        analyics.track(eventCategory.ad, {
            eventAction: eventAction.tunecore,
            eventLabel: eventLabel.playerAd
        });
    };

    render() {
        const {
            currentUser,
            className,
            containerClass,
            visible,
            onClose
        } = this.props;

        if (
            !visible ||
            !currentUser.isLoggedIn ||
            (currentUser.isLoggedIn && !currentUser.profile.upload_count > 0)
        ) {
            return null;
        }

        const adClass = classnames(
            'uploader-ad u-d-flex u-d-flex--align-center u-d-flex--justify-center',
            {
                [className]: className
            }
        );

        const containerKlass = classnames('', {
            [containerClass]: containerClass
        });

        return (
            <div className={containerKlass}>
                <div className={adClass}>
                    <a
                        href="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_naap_su&utm_content=gymosamam_a"
                        target="_blank"
                        rel="nofollow noopener"
                        onClick={this.handleLinkClick}
                        style={{ width: '100%' }}
                    >
                        <span className="uploader-ad__logo u-d-inline-block">
                            <img
                                src="/static/images/desktop/tc-logo-white-blue-bg.png"
                                srcSet="/static/images/desktop/tc-logo-white-blue-bg@2x.png 2x"
                                alt="TuneCore"
                            />
                        </span>
                        <p className="u-text-white u-fs-13 u-text-center ">
                            Get your music on Spotify, Apple Music, and more.{' '}
                            <strong>Keep 100% of the money you make.</strong>
                            <span className="button button--radius">
                                Get Started
                            </span>
                        </p>
                    </a>
                    <button className="uploader-ad__close" onClick={onClose}>
                        <CloseIcon />
                    </button>
                </div>
            </div>
        );
    }
}
