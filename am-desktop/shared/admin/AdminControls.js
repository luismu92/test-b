import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AdminControls extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        recognition: PropTypes.object,
        onTakedownClick: PropTypes.func.isRequired,
        onRestoreClick: PropTypes.func.isRequired,
        onCensorClick: PropTypes.func,
        onTrendingClick: PropTypes.func,
        onUnsuspendClick: PropTypes.func,
        onSuspendClick: PropTypes.func,
        onClearStatsClick: PropTypes.func,
        onExcludeStatsClick: PropTypes.func,
        onDownloadSourceClick: PropTypes.func,
        onDownloadMp3Click: PropTypes.func,
        onRecognitionClick: PropTypes.func,
        onRecognitionRefreshClick: PropTypes.func,
        onRepairUrlClick: PropTypes.func,
        onClearUploadErrorsClick: PropTypes.func
    };

    renderRecognition(recognition) {
        if (
            typeof recognition.result === 'undefined' ||
            typeof recognition.result.label === 'undefined'
        ) {
            return null;
        }

        let rescanButtonTitle = 'Re-scan and Refresh';

        if (recognition.isLoading) {
            rescanButtonTitle = 'Refreshing, please wait...';
        }

        const refreshRecognitionButton = (
            <button
                className="button button--small"
                onClick={this.props.onRecognitionRefreshClick}
                title="Re-scan and Refresh Recognition Details"
                disabled={recognition.isLoading}
            >
                {rescanButtonTitle}
            </button>
        );

        return (
            <div>
                <strong>Content Recognition</strong>
                <ul>
                    {Object.keys(recognition.result).map((key, index) => (
                        <li key={index}>
                            {key}: {recognition.result[key]}
                        </li> // eslint-disable-line
                    ))}
                </ul>
                {refreshRecognitionButton}
            </div>
        );
    }

    render() {
        const {
            item,
            recognition,
            onCensorClick,
            onTrendingClick,
            onUnsuspendClick,
            onSuspendClick,
            onClearStatsClick,
            onExcludeStatsClick,
            onDownloadSourceClick,
            onDownloadMp3Click,
            onRecognitionClick,
            onRepairUrlClick,
            onClearUploadErrorsClick
        } = this.props;

        if (!item || Object.keys(item).length === 0) {
            return null;
        }

        let edit;
        let takedownOrRestore;
        let censorArtwork;
        let suspend;
        let trending;
        let monetization;
        let recognitionButton;

        if (item.featured_genres) {
            trending = (
                <button
                    className="button button--small"
                    onClick={onTrendingClick}
                    title="Trending / Tag Controls"
                    data-trending={JSON.stringify(item.featured_genres)}
                >
                    Trending / Tag Controls
                </button>
            );
        }

        if (item.type === 'song' || item.type === 'album') {
            const editUrl = `/admin/${item.type}s/edit/${
                item.uploader.url_slug
            }/${item.url_slug}`;
            let action;
            let title;
            let takedownOrRestoreFn;

            if (
                item.status === 'complete' ||
                item.status === 'suspended' ||
                item.status === 'unplayable'
            ) {
                action = 'takedown';
                title = 'Takedown';
                takedownOrRestoreFn = this.props.onTakedownClick;
            } else if (
                item.status === 'removed' ||
                item.status === 'takedown'
            ) {
                action = 'restore';
                title = 'Restore';
                takedownOrRestoreFn = this.props.onRestoreClick;
            }

            edit = (
                <a
                    className="button button--small"
                    href={editUrl}
                    title="Admin edit"
                >
                    Admin Edit
                </a>
            );

            if (action) {
                takedownOrRestore = (
                    <button
                        className="button button--small"
                        onClick={takedownOrRestoreFn}
                        title={title}
                    >
                        {title}
                    </button>
                );
            }

            censorArtwork = (
                <a
                    className="button button--small"
                    onClick={onCensorClick}
                    href="/admin/songs/censor-artwork"
                    title="Censor artwork"
                >
                    Censor Artwork
                </a>
            );

            if (item.status === 'suspended') {
                suspend = (
                    <button
                        className="button button--small"
                        onClick={onUnsuspendClick}
                        title="Un-suspend this song"
                    >
                        Un-suspend this song
                    </button>
                );
            } else {
                suspend = (
                    <button
                        className="button button--small"
                        onClick={onSuspendClick}
                        title="Suspend this song"
                    >
                        Suspend this song
                    </button>
                );
            }
        }

        if (item.amp) {
            monetization = <div>Monetization on - {item.amp_partner}</div>;
        } else {
            monetization = <div>Monetization off</div>;
        }

        if (item.type === 'song') {
            recognitionButton = (
                <button
                    className="button button--small"
                    onClick={onRecognitionClick}
                    title="View Recognition Details"
                >
                    View Recognition
                </button>
            );
        }

        const repairUrlButton = (
            <button
                className="button button--small"
                onClick={onRepairUrlClick}
                title="View Recognition Details"
            >
                Repair Url
            </button>
        );

        let excludeLabel = 'Exclude stats';

        if (item.stats_excluded) {
            excludeLabel = 'Include stats';
        }

        return (
            <div className="admin-controls owner-controls">
                <div className="admin-controls__icon-group">
                    {edit}
                    {takedownOrRestore}
                    {censorArtwork}
                    {trending}
                    {suspend}
                    <button
                        className="button button--small"
                        onClick={onClearStatsClick}
                        title="Clear stats"
                    >
                        Clear Stats
                    </button>
                    <button
                        className="button button--small"
                        onClick={onExcludeStatsClick}
                        title={excludeLabel}
                    >
                        {excludeLabel}
                    </button>
                    <button
                        className="button button--small"
                        onClick={onDownloadSourceClick}
                        title="Download Source File"
                    >
                        Download Source File
                    </button>
                    <button
                        className="button button--small"
                        onClick={onDownloadMp3Click}
                        title="Download Mp3 Stream File"
                    >
                        Download Streaming File
                    </button>
                    <button
                        className="button button--small"
                        onClick={onClearUploadErrorsClick}
                        title="Clear Upload Errors"
                    >
                        Clear Upload Errors
                    </button>
                    {recognitionButton}
                    {repairUrlButton}
                </div>
                <div>
                    <strong>Music Id: </strong>
                    <input value={item.id} readOnly />
                </div>
                {monetization}
                {this.renderRecognition(recognition)}
            </div>
        );
    }
}
