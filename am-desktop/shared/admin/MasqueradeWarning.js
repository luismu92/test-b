import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { logoutOfUser } from '../redux/modules/admin';

class MasqueradeWarning extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        admin: PropTypes.object
    };

    //////////////////////
    // Lifecyle methods //
    //////////////////////

    componentDidMount() {
        document.body.classList.add('is-masquerading');
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.currentUser.isLoggedIn &&
            !this.props.currentUser.isLoggedIn
        ) {
            document.body.classList.remove('is-masquerading');
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLogoutClick = () => {
        const { dispatch } = this.props;

        dispatch(logoutOfUser());

        document.body.classList.remove('is-masquerading');

        window.location.reload();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        if (!this.props.currentUser || !this.props.currentUser.profile) {
            return null;
        }

        return (
            <div className="masquerade-warning u-flex-center">
                <p>
                    Careful, you are currently acting on behalf of the{' '}
                    <strong>{this.props.currentUser.profile.name}</strong>{' '}
                    account.{' '}
                    <button
                        className="button button--pill"
                        onClick={this.handleLogoutClick}
                    >
                        Get back to doing me
                    </button>
                </p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        admin: state.admin
    };
}

export default connect(mapStateToProps)(MasqueradeWarning);
