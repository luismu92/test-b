import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import analytics from 'utils/analytics';

import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_TRENDING
} from '../redux/modules/modal';
import { getSongInfo } from '../redux/modules/music/song';
import { getAlbumInfo } from '../redux/modules/music/album';
import { addToast } from '../redux/modules/toastNotification';
import {
    suspendMusic,
    unsuspendMusic,
    takedownMusic,
    restoreMusic,
    censorArtwork,
    trendMusic,
    excludeStats,
    clearStats,
    getMusicSource,
    getMusicRecognition,
    refreshMusicRecognition,
    repairMusicUrl,
    clearUploadErrors
} from '../redux/modules/admin';

import requireAuth from '../hoc/requireAuth';

import AdminControls from './AdminControls';

class AdminControlsContainer extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
        onTakedownSuccess: PropTypes.func,
        onCensorSuccess: PropTypes.func,
        onClearStatsSuccess: PropTypes.func,
        onExcludeSuccess: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            recognition: {
                isLoading: false
            }
        };
    }

    componentDidMount() {
        const { currentUser } = this.props;

        if (!currentUser.isLoggedIn || !currentUser.isAdmin) {
            return;
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSelectChange = () => {
        // To do here:
        // Once API Endpoints for Admin actions are in go ahead and
        // Upon options change we change what form is in the modal
        // In respect to the current item
        return null;
    };

    handleTakedownClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Takedown',
                message: `Are you sure you want to takedown "${item.title}"?`,
                handleConfirm: () => this.doTakedown(item)
            })
        );
    };

    handleRestoreClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Restore',
                message: `Are you sure you want to restore "${item.title}"?`,
                handleConfirm: () => this.doRestore(item)
            })
        );
    };

    handleCensorClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Censor artwork',
                message: `Are you sure you want to censor the artwork for this ${
                    item.title
                }?`,
                handleConfirm: () => this.doCensorArtwork(item)
            })
        );
    };

    handleTrendingClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        const trendGenres = JSON.parse(
            event.currentTarget.getAttribute('data-trending')
        );

        dispatch(
            showModal(MODAL_TYPE_TRENDING, {
                title: `Trend ${item.type}`,
                musicItem: item,
                trendData: {
                    id: item.id,
                    trendGenres,
                    handleCheckboxToggle: (genre, trend) =>
                        this.handleTrendingToggle(genre, trend, item)
                }
            })
        );
    };

    handleTrendingToggle = (genre, trend, item) => {
        const { dispatch } = this.props;

        dispatch(trendMusic(item.id, genre, trend))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.trending !== 'undefined') {
                    dispatch(
                        addToast({
                            type: item.type,
                            action: 'trend',
                            item: `${item.artist} - ${item.title}`,
                            data: { genre: genre }
                        })
                    );

                    if (item.type === 'album') {
                        dispatch(
                            getAlbumInfo(item.uploader.url_slug, item.url_slug)
                        );
                    } else {
                        dispatch(
                            getSongInfo(item.uploader.url_slug, item.url_slug)
                        );
                    }
                }
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    };

    handleTagClick = (event) => {
        // const { dispatch } = this.props;

        event.preventDefault();
    };

    handleSuspendClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Suspend song',
                message: `Are you sure you want to suspend the ${item.type} ${
                    item.title
                }?`,
                handleConfirm: () => this.suspendItem(item)
            })
        );
    };

    handleUnsuspendClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Un-suspend song',
                message: `Are you sure you want to un-suspend the ${
                    item.type
                } ${item.title}?`,
                handleConfirm: () => this.unsuspendItem(item)
            })
        );
    };

    handleExcludeStatsClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        let confirmMessage = `Are you sure you want to exclude this ${
            item.type
        } from stats rankings?`;

        if (item.stats_excluded) {
            confirmMessage = `Are you sure you want to include this ${
                item.type
            } in stats rankings?`;
        }

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Exclude stats',
                message: confirmMessage,
                handleConfirm: () => this.doExcludeStats(item)
            })
        );
    };

    handleClearStatsClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Clear stats',
                message: `Are you sure you want to clear stats for this ${
                    item.type
                }?  This action is irreversible.`,
                handleConfirm: () => this.doStatsClear(item)
            })
        );
    };

    handleDownloadSourceClick = (event) => {
        event.preventDefault();
        this.doGetMusicSource(false);
    };

    handleDownloadMp3Click = (event) => {
        event.preventDefault();
        this.doGetMusicSource(true);
    };

    handleRecognitionClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(getMusicRecognition(item.id))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.result.label === 'undefined') {
                    this.setState({
                        recognition: {
                            result: {
                                label: 'No content recognition found.'
                            }
                        }
                    });
                } else {
                    this.setState({
                        recognition: {
                            result: json.result
                        }
                    });
                }
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem to get music recognition: ${
                            error.message
                        }`
                    })
                );
            });
    };

    handleRecognitionRefreshClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        this.setState((state) => {
            return {
                recognition: {
                    result: state.recognition.result,
                    isLoading: true
                }
            };
        });

        dispatch(refreshMusicRecognition(item.id))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.result.label === 'undefined') {
                    this.setState({
                        recognition: {
                            result: {
                                label: 'No content recognition found.'
                            },
                            isLoading: false
                        }
                    });
                } else {
                    this.setState({
                        recognition: {
                            result: json.result
                        },
                        isLoading: false
                    });
                }
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem to get music recognition: ${
                            error.message
                        }`
                    })
                );

                this.setState({
                    recognition: {
                        isLoading: false
                    }
                });
            });
    };

    handleRepairMusicUrlClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(repairMusicUrl(item.id))
            .then((action) => {
                const json = action.resolved;

                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            typeof json.result !== 'undefined'
                                ? 'Finished repair music url'
                                : 'There has been a problem to repair music url'
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem to repair music url: ${
                            error.message
                        }`
                    })
                );
            });
    };

    handleClearUploadErrorsClick = (event) => {
        const { dispatch, item } = this.props;

        event.preventDefault();

        dispatch(clearUploadErrors(item.id))
            .then((action) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: action.resolved.success
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    doGetMusicSource(isStream = false) {
        const { dispatch, item } = this.props;
        const type = isStream ? 'mp3' : 'source';

        dispatch(getMusicSource(item.id, isStream))
            .then((action) => {
                const json = action.resolved;

                dispatch(hideModal());
                if (item.type === 'album') {
                    for (const musicId in json) {
                        if (json[musicId]) {
                            window.open(json[musicId]);
                        }
                    }
                } else {
                    window.location.href = json.url;
                }
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem to get music ${type} url: ${
                            error.message
                        }`
                    })
                );
            });
    }

    doStatsClear = (item) => {
        const { dispatch, onClearStatsSuccess } = this.props;

        dispatch(clearStats(item.id))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.success !== 'undefined') {
                    dispatch(hideModal());

                    if (typeof onClearStatsSuccess === 'function') {
                        onClearStatsSuccess();
                    }
                }
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );

                analytics.error(error);
            });
    };

    doExcludeStats = (item) => {
        const { dispatch, onExcludeSuccess } = this.props;

        const operation = item.stats_excluded ? 'include' : 'exclude';

        dispatch(excludeStats(item.id, operation))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.success !== 'undefined') {
                    dispatch(hideModal());

                    dispatch(
                        addToast({
                            action: 'message',
                            item: json.success
                        })
                    );

                    if (typeof onExcludeSuccess === 'function') {
                        onExcludeSuccess();
                    }
                } else {
                    throw new Error(json.error);
                }
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    };

    suspendItem(item) {
        const { dispatch } = this.props;

        dispatch(suspendMusic(item.id));
        dispatch(hideModal());
        dispatch(
            addToast({
                type: item.type,
                action: 'suspend',
                item: `${item.artist} - ${item.title}`
            })
        );
    }

    unsuspendItem(item) {
        const { dispatch } = this.props;

        dispatch(unsuspendMusic(item.id));
        dispatch(hideModal());
        dispatch(
            addToast({
                type: item.type,
                action: 'un-suspend',
                item: `${item.artist} - ${item.title}`
            })
        );
    }

    doCensorArtwork = (item) => {
        const { dispatch, onCensorSuccess } = this.props;

        dispatch(censorArtwork(item.id))
            .then((action) => {
                const json = action.resolved;

                if (typeof json.success !== 'undefined') {
                    dispatch(
                        addToast({
                            type: item.type,
                            action: 'censor',
                            item: `${item.artist} - ${item.title}`
                        })
                    );

                    dispatch(hideModal());

                    if (typeof onCensorSuccess === 'function') {
                        onCensorSuccess();
                    }
                }
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There was a problem censoring this artwork: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    };

    doTakedown(item) {
        const { dispatch, onTakedownSuccess } = this.props;

        dispatch(takedownMusic(item.id))
            .then((action) => {
                const json = action.resolved;

                if (json.status === 'takedown') {
                    dispatch(
                        addToast({
                            type: item.type,
                            action: 'takedown',
                            item: `${item.artist} - ${item.title}`
                        })
                    );

                    if (typeof onTakedownSuccess === 'function') {
                        onTakedownSuccess();
                    }
                } else {
                    dispatch(
                        addToast({
                            action: 'message',
                            item: `Error: ${json.message}`
                        })
                    );
                }

                dispatch(hideModal());
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    }

    doRestore(item) {
        const { dispatch } = this.props;

        dispatch(restoreMusic(item.id))
            .then((action) => {
                const json = action.resolved;

                if (json.status === 'complete') {
                    dispatch(
                        addToast({
                            type: item.type,
                            action: 'restored',
                            item: `${item.artist} - ${item.title}`
                        })
                    );
                } else {
                    dispatch(
                        addToast({
                            action: 'message',
                            item: `Error: ${json.message}`
                        })
                    );
                }

                dispatch(hideModal());
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    }

    render() {
        const { currentUser, item } = this.props;

        if (!currentUser.isLoggedIn || !currentUser.isAdmin) {
            return null;
        }

        return (
            <AdminControls
                item={item}
                currentUser={currentUser}
                recognition={this.state.recognition}
                onSelectChange={this.handleSelectChange}
                onTakedownClick={this.handleTakedownClick}
                onRestoreClick={this.handleRestoreClick}
                onCensorClick={this.handleCensorClick}
                onTrendingClick={this.handleTrendingClick}
                onFeaturedClick={this.handleFeaturedClick}
                onSuspendClick={this.handleSuspendClick}
                onUnsuspendClick={this.handleUnsuspendClick}
                onExcludeStatsClick={this.handleExcludeStatsClick}
                onClearStatsClick={this.handleClearStatsClick}
                onDownloadSourceClick={this.handleDownloadSourceClick}
                onDownloadMp3Click={this.handleDownloadMp3Click}
                onRecognitionClick={this.handleRecognitionClick}
                onRecognitionRefreshClick={this.handleRecognitionRefreshClick}
                onRepairUrlClick={this.handleRepairMusicUrlClick}
                onClearUploadErrorsClick={this.handleClearUploadErrorsClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        admin: state.admin
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(AdminControlsContainer);
