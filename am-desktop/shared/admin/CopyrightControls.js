import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { suspendMusic, unsuspendMusic } from '../redux/modules/admin';
import { addToast } from '../redux/modules/toastNotification';

import FlagIcon from '../icons/flag';

class CopyrightControls extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        music: PropTypes.object,
        dispatch: PropTypes.func
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSuspendClick = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;

        e.preventDefault();
        dispatch(suspendMusic(button.getAttribute('data-id')));

        dispatch(
            addToast({
                type: button.getAttribute('data-type'),
                action: 'suspend',
                item: `${button.getAttribute(
                    'data-artist'
                )} - ${button.getAttribute('data-title')}`
            })
        );
    };

    handleUnsuspendClick = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;

        e.preventDefault();

        dispatch(unsuspendMusic(button.getAttribute('data-id')));

        dispatch(
            addToast({
                type: button.getAttribute('data-type'),
                action: 'un-suspend',
                item: `${button.getAttribute(
                    'data-artist'
                )} - ${button.getAttribute('data-title')}`
            })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { currentUser, music } = this.props;

        if (!currentUser.isLoggedIn || !currentUser.isSuspension) {
            return null;
        }

        let message;
        let action;

        if (music.uploader.suspension_whitelist === 'yes') {
            message = (
                <p>
                    This account is whitelisted, if you believe this content is
                    infringing, please send a{' '}
                    <Link to="/legal">DMCA notice</Link>.
                </p>
            );
        } else if (music.is_soundcloud) {
            message = (
                <p>
                    This song is streaming through the SoundCloud link located{' '}
                    <a
                        href={music.sourceUrl}
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        here
                    </a>
                    .
                </p>
            );
        } else if (music.status === 'complete') {
            const toolTip = `Suspend this ${music.type}`;

            action = (
                <button
                    onClick={this.handleSuspendClick}
                    data-tooltip={toolTip}
                    data-id={music.id}
                    data-type={music.type}
                    data-artist={music.artist}
                    data-title={music.title}
                >
                    <FlagIcon className="admin-controls__icon" />
                </button>
            );
        } else if (music.status === 'incomplete') {
            const toolTip = `Suspend this ${music.type}`;

            message = <p>This {music.type} is incomplete</p>;

            action = (
                <button
                    onClick={this.handleSuspendClick}
                    data-tooltip={toolTip}
                    data-id={music.id}
                    data-type={music.type}
                    data-artist={music.artist}
                    data-title={music.title}
                >
                    <FlagIcon className="admin-controls__icon" />
                </button>
            );
        } else {
            const toolTip = `Un-suspend this ${music.type}`;

            message = <p>This {music.type} has been suspended</p>;
            action = (
                <button
                    onClick={this.handleUnsuspendClick}
                    data-tooltip={toolTip}
                    data-id={music.id}
                    data-type={music.type}
                    data-artist={music.artist}
                    data-title={music.title}
                >
                    <FlagIcon className="admin-controls__icon" />
                </button>
            );
        }

        return (
            <div className="row">
                <div className="column small-24">
                    <div className="row expanded column small-24 admin-controls copyright-controls">
                        <div className="column small-24 admin-controls__inner">
                            <h2>Copyright Controls</h2>
                            {message}
                            {action}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(CopyrightControls);
