import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CogIcon from '../icons/cog';

import BodyClickListener from '../components/BodyClickListener';

export default class ArtistAdminControls extends Component {
    static propTypes = {
        artist: PropTypes.object.isRequired,
        onEditClick: PropTypes.func,
        onCopyClick: PropTypes.func,
        onActionClick: PropTypes.func,
        onBlockClick: PropTypes.func.isRequired,
        onLoginClick: PropTypes.func.isRequired,
        editActive: PropTypes.bool,
        onFollowListClick: PropTypes.func
    };

    componentWillUnmount() {
        this._tooltipButton = null;
    }

    render() {
        const {
            artist,
            onBlockClick,
            onLoginClick,
            onFollowListClick,
            onCopyClick,
            onActionClick
        } = this.props;
        const listUrl = `/admin/artists/list/${artist.profile.url_slug}`;
        const artistSearchUrl = `/admin/artists/search?q=${encodeURIComponent(
            artist.profile.name
        )}`;
        const editUserUrl = `/admin/users/edit/user/${artist.profile.user_id}`; // @TODO get user ID
        const editArtistUrl = `/admin/artists/edit/artist/${artist.profile.id}`;
        const verifiedText =
            artist.profile.verified === 'yes' ? 'Verified' : 'Not verified';
        const tastemakerText =
            artist.profile.verified === 'tastemaker'
                ? 'Tastemaker'
                : 'Not Tastemaker';
        const blockTitle =
            typeof artist.profile.can_upload !== 'undefined' &&
            artist.profile.can_upload === true
                ? 'Block'
                : 'Un-block';
        const followListTitle = artist.profile.followList
            ? 'Remove from follow list'
            : 'Add to follow list';
        const followListAction = artist.profile.followList ? 'remove' : 'add';

        const buttonsKlass = classnames(
            'sub-menu sub-menu--two-col',
            'tooltip',
            'edit-music__tooltip',
            {
                'tooltip--active': this.props.editActive
            }
        );

        const buttons = (
            <ul className={buttonsKlass}>
                <li>
                    <a
                        className="button--admin"
                        title="List"
                        target="_blank"
                        href={listUrl}
                    >
                        List
                    </a>
                </li>
                <li>
                    <button
                        className="button--admin"
                        title={`Login as ${artist.profile.name}`}
                        data-id={artist.profile.id}
                        onClick={onLoginClick}
                        type="button"
                    >
                        Login
                    </button>
                </li>
                <li>
                    <button
                        className="button--admin"
                        onClick={onCopyClick}
                        type="button"
                    >
                        Copy Artist Id
                    </button>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title="Edit user"
                        target="_blank"
                        href={editUserUrl}
                    >
                        Edit User
                    </a>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title="Edit artist"
                        target="_blank"
                        href={editArtistUrl}
                    >
                        Edit Artist
                    </a>
                </li>
                <li>
                    <button
                        className="button--admin"
                        data-action="verify"
                        onClick={onActionClick}
                    >
                        {verifiedText}
                    </button>
                </li>
                <li>
                    <button
                        className="button--admin"
                        data-action="tastemaker"
                        onClick={onActionClick}
                    >
                        {tastemakerText}
                    </button>
                </li>
                <li>
                    <button
                        className="button--admin"
                        data-action="verify-email"
                        onClick={onActionClick}
                    >
                        Verify Email
                    </button>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title={blockTitle}
                        href={editUserUrl}
                        onClick={onBlockClick}
                        data-id={artist.profile.id}
                        data-artist={artist.profile.name}
                    >
                        {blockTitle}
                    </a>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title="Revoke suspension admin"
                        target="_blank"
                        href={artistSearchUrl}
                    >
                        Revoke Suspension
                    </a>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title="Not confirmed"
                        target="_blank"
                        href={artistSearchUrl}
                    >
                        Not Confirmed
                    </a>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title="Disable"
                        target="_blank"
                        href={artistSearchUrl}
                    >
                        Disable
                    </a>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title="Censor artwork"
                        target="_blank"
                        href={artistSearchUrl}
                    >
                        Censor Artwork
                    </a>
                </li>
                <li>
                    <a
                        className="button--admin"
                        title={followListTitle}
                        target="_blank"
                        href={artistSearchUrl}
                        onClick={onFollowListClick}
                        data-action={followListAction}
                    >
                        {followListTitle}
                    </a>
                </li>
            </ul>
        );

        const buttonClass = classnames('edit-music-button', {
            'edit-music-button--active': this.props.editActive
        });

        return (
            <div className="edit-music-wrap">
                <BodyClickListener
                    shouldListen={this.props.editActive}
                    onClick={this.props.onEditClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <button
                    className={buttonClass}
                    onClick={this.props.onEditClick}
                    ref={(e) => {
                        this._tooltipButton = e;
                    }}
                >
                    <span className="edit-music-button__icon">
                        <CogIcon />
                    </span>
                    <span className="edit-music-button__label">Admin</span>
                </button>
                {buttons}
            </div>
        );
    }
}
