import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import analytics from 'utils/analytics';
import { copyToClipboard } from 'utils/index';

import {
    MODAL_TYPE_BLOCK,
    MODAL_TYPE_CONFIRM,
    showModal,
    hideModal
} from '../redux/modules/modal';
import {
    loginAsUser,
    verifyArtist,
    verifyArtistEmail
} from '../redux/modules/admin';
import {
    addArtistToFollowList,
    removeArtistFromFollowList
} from '../redux/modules/artist';

import { addToast } from '../redux/modules/toastNotification';

import ArtistAdminControls from './ArtistAdminControls';

class ArtistAdminControlsContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        artist: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            editActive: false
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleEditClick = () => {
        this.setState({ editActive: !this.state.editActive });
    };

    handleLoginClick = (e) => {
        const { dispatch } = this.props;
        const id = e.currentTarget.getAttribute('data-id');

        dispatch(loginAsUser(id))
            .then(() => {
                return window.location.reload();
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleBlockClick = (event) => {
        const { dispatch, artist } = this.props;

        event.preventDefault();

        const id = event.currentTarget.getAttribute('data-id');
        const artistName = event.currentTarget.getAttribute('data-artist');
        const type = event.currentTarget.innerText.toLowerCase();

        dispatch(
            showModal(MODAL_TYPE_BLOCK, {
                title: `Are you sure you want to ${type} this artist?`,
                artistId: id,
                artist: artistName,
                confirmData: {
                    id,
                    artist,
                    type
                }
            })
        );
    };

    handleFollowListClick = (event) => {
        const { dispatch, artist } = this.props;

        event.preventDefault();

        const action = event.currentTarget.getAttribute('data-action');
        const message =
            action === 'add'
                ? `Are you sure you want to add ${
                      artist.profile.name
                  } to the follow list?`
                : `Are you sure you want to remove ${
                      artist.profile.name
                  } from the follow list?`;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure?',
                message,
                handleConfirm: () =>
                    action === 'add'
                        ? this.doFollowListAdd()
                        : this.doFollowListRemove()
            })
        );
    };

    handleCopyClick = () => {
        const { artist, dispatch } = this.props;
        const copied = copyToClipboard(artist.profile.id);

        let message = `Error copying artist id ${
            artist.profile.id
        } to clipboard`;

        if (copied) {
            message = 'ID copied to clipboard!';
        }

        dispatch(
            addToast({
                action: 'message',
                item: message
            })
        );
    };

    handleActionClick = (e) => {
        const { dispatch, artist } = this.props;
        const action = e.currentTarget.getAttribute('data-action');

        switch (action) {
            case 'verify-email':
                dispatch(verifyArtistEmail(artist.profile, 'verified'));
                break;

            case 'verify':
                dispatch(verifyArtist(artist.profile, 'verified'));
                break;

            case 'tastemaker':
                dispatch(verifyArtist(artist.profile, 'tastemaker'));
                break;

            default:
                break;
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    doFollowListAdd() {
        const { dispatch, artist } = this.props;

        dispatch(addArtistToFollowList(artist.profile.id))
            .then(() => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'followlistadd',
                        item: `${artist.profile.name}`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    }

    doFollowListRemove() {
        const { dispatch, artist } = this.props;

        dispatch(removeArtistFromFollowList(artist.profile.id))
            .then(() => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'followlistremove',
                        item: `${artist.profile.name}`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
            });
    }

    render() {
        return (
            <ArtistAdminControls
                artist={this.props.artist}
                editActive={this.state.editActive}
                onEditClick={this.handleEditClick}
                onBlockClick={this.handleBlockClick}
                onActionClick={this.handleActionClick}
                onCopyClick={this.handleCopyClick}
                onLoginClick={this.handleLoginClick}
                onFollowListClick={this.handleFollowListClick}
            />
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(ArtistAdminControlsContainer);
