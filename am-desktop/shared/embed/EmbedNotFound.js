import React, { Component } from 'react';

export default class EmbedNotFound extends Component {
    render() {
        return (
            <div className="missing-embed">
                <div className="missing-embed__inner">
                    <h1 className="missing-embed__title">
                        Oops, something went wrong
                    </h1>
                    <p className="missing-embed__message">
                        It looks like this link is broken, and we can't find
                        what you're looking for
                    </p>
                    <div className="button-group u-text-center">
                        <a
                            className="button button--pill"
                            target="_blank"
                            href={process.env.AM_URL}
                        >
                            Go to Audiomack.com
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
