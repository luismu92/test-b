import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    setVolume,
    seek,
    setRepeat,
    mute,
    unmute,
    editQueue,
    play,
    pause
} from '../redux/modules/player';

class EmbedlyApi extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        player: PropTypes.object.isRequired,
        musicItem: PropTypes.object.isRequired
    };

    //////////////////////
    // Lifecyle methods //
    //////////////////////

    componentDidMount() {
        this._isReady = false;
        this._context = 'player.js';
        this._version = null;
        this._listeners = [];

        this.listenForEvents();
        this.readyUp();
    }

    componentDidUpdate(prevProps) {
        const prevPlayer = prevProps.player;
        const currentPlayer = this.props.player;

        if (
            prevPlayer.paused &&
            !currentPlayer.paused &&
            this.hasListener('play')
        ) {
            this.sendMessage({
                event: 'play'
            });
        }

        if (
            !prevPlayer.paused &&
            currentPlayer.paused &&
            this.hasListener('pause')
        ) {
            this.sendMessage({
                event: 'pause'
            });
        }

        if (
            !prevPlayer.ended &&
            currentPlayer.ended &&
            this.hasListener('ended')
        ) {
            this.sendMessage({
                event: 'ended'
            });
        }

        if (
            !prevPlayer.currentTime !== currentPlayer.currentTime &&
            this.hasListener('timeupdate')
        ) {
            this.sendMessage({
                event: 'timeupdate',
                value: {
                    seconds: currentPlayer.currentTime,
                    duration: currentPlayer.duration
                }
            });
        }
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    listenForEvents() {
        window.addEventListener('message', (e) => {
            // if (e.origin !== process.env.AM_URL) {
            //     return;
            // }

            let data = {};

            try {
                data = JSON.parse(e.data);
            } catch (e) {} // eslint-disable-line

            if (data.context !== 'player.js') {
                return;
            }

            if (!this._version && data.version) {
                this._version = data.version;
            }

            console.log(e);
            const { musicItem, player, dispatch } = this.props;

            if (!musicItem || !this._isReady) {
                return;
            }

            console.warn('EmbedlyApi: <- message', data);

            switch (data.method) {
                case 'play': {
                    dispatch(editQueue(musicItem));
                    dispatch(play()).catch((err) => {
                        console.log(err);
                        if (this.hasListener('error')) {
                            this.sendMessage({
                                event: 'error',
                                value: {
                                    code: -1,
                                    msg: 'There was an error playing this media'
                                }
                            });
                        }
                    });
                    break;
                }

                case 'pause':
                    dispatch(pause());
                    break;

                case 'getPaused':
                    this.sendMessage({
                        event: data.method,
                        listener: data.listener,
                        value: player.paused
                    });
                    break;

                case 'mute':
                    dispatch(mute());
                    break;

                case 'unmute':
                    dispatch(unmute());
                    break;

                case 'getMuted':
                    this.sendMessage({
                        event: data.method,
                        listener: data.listener,
                        value: player.muted
                    });
                    break;

                case 'setVolume':
                    dispatch(setVolume(data.value / 100));
                    break;

                case 'getVolume':
                    this.sendMessage({
                        event: data.method,
                        listener: data.listener,
                        value: Math.round(player.volume * 100)
                    });
                    break;

                case 'getDuration':
                    this.sendMessage({
                        event: data.method,
                        listener: data.listener,
                        value: player.duration
                    });
                    break;

                case 'setCurrentTime':
                    dispatch(seek(data.value));
                    break;

                case 'getCurrentTime':
                    this.sendMessage({
                        event: data.method,
                        listener: data.listener,
                        value: player.currentTime
                    });
                    break;

                case 'setLoop':
                    dispatch(setRepeat(data.value));
                    break;

                case 'getLoop':
                    this.sendMessage({
                        event: data.method,
                        listener: data.listener,
                        value: player.repeat
                    });
                    break;

                case 'addEventListener':
                    this.addListener(data.value);
                    break;

                case 'removeEventListener':
                    this.removeListener(data.value);
                    break;

                default:
                    if (this.hasListener('error')) {
                        this.sendMessage({
                            event: 'error',
                            value: {
                                code: 2,
                                msg: 'Method not supported'
                            }
                        });
                    }
                    break;
            }
        });
    }

    addListener(event) {
        this._listeners.push(event);

        console.warn('EmbedlyApi: addListener', event);

        if (event === 'ready' && this._isReady) {
            this.sendReadyMessage();
        }
    }

    removeListener(event) {
        const index = this._listeners.indexOf(event);

        console.warn('EmbedlyApi: removeListener', event);
        if (index !== -1) {
            this._listeners.splice(index, 1);
        }
    }

    hasListener(event) {
        return this._listeners.indexOf(event) !== -1;
    }

    sendReadyMessage() {
        const forceSend = true;

        this.sendMessage(
            {
                event: 'ready',
                value: {
                    src: window.location.href,
                    events: [
                        'ready',
                        'timeupdate',
                        'play',
                        'pause',
                        'ended',
                        'error'
                    ],
                    methods: [
                        'play',
                        'pause',
                        'getPaused',
                        'mute',
                        'unmute',
                        'getMuted',
                        'setVolume',
                        'getVolume',
                        'getDuration',
                        'setCurrentTime',
                        'getCurrentTime',
                        'setLoop',
                        'getLoop',
                        'addEventListener',
                        'removeEventListener'
                    ]
                }
            },
            forceSend
        );
    }

    readyUp() {
        if (this._isReady) {
            return;
        }

        this.sendReadyMessage();

        this._isReady = true;
    }

    sendMessage(data = {}, forceSend = false) {
        if (!this._isReady && !forceSend) {
            return;
        }

        console.warn('EmbedlyApi: -> sendMessage', data);
        window.parent.postMessage(
            JSON.stringify({
                context: 'player.js',
                version: this._version,
                ...data
            }),
            '*'
        );
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player,
        playlist: state.playlist
    };
}

export default connect(mapStateToProps)(EmbedlyApi);
