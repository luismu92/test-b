import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    getTwitterShareLink,
    getFacebookShareLink,
    getMusicUrl
} from 'utils/index';

import EmbedTextarea from '../components/EmbedTextarea';
import AppBadge from '../components/AppBadge';

import TwitterLogo from '../icons/twitter-logo-new';
import FacebookLogo from '../icons/facebook-letter-logo';
import HeartIcon from '../../../am-shared/icons/heart';
import RetweetIcon from '../icons/retweet';
import PlusIcon from '../icons/plus';
import CloseIcon from '../icons/close-thin';

import styles from './EmbedActions.module.scss';

export default class EmbedActions extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        referer: PropTypes.string,
        currentUser: PropTypes.object,
        onActionClick: PropTypes.func,
        onMenuClick: PropTypes.func,
        color: PropTypes.string,
        showBackground: PropTypes.bool
    };

    static defaultProps = {
        onActionClick() {}
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderPlaylistButton(item, currentUser, onActionClick) {
        const { referer } = this.props;
        const suffix = referer ? `?referer=${encodeURIComponent(referer)}` : '';
        const href = `${getMusicUrl(item)}${suffix}`;

        return (
            <li key="playlist">
                <a
                    href={href}
                    target="_blank"
                    onClick={onActionClick}
                    data-action="playlist"
                    className={styles.button}
                >
                    <span className={styles.buttonIcon}>
                        <PlusIcon />
                    </span>
                    {item.stats['playlists-raw']}
                </a>
            </li>
        );
    }

    renderReupButton(item, currentUser, onActionClick) {
        const { referer } = this.props;
        const props = {
            onClick: onActionClick,
            'data-action': 'reup',
            className: styles.button
        };
        const tag = currentUser.isLoggedIn ? 'button' : 'a';

        if (!currentUser.isLoggedIn) {
            const suffix = referer
                ? `?referer=${encodeURIComponent(referer)}`
                : '';
            const href = `${getMusicUrl(item)}${suffix}`;

            props.href = href;
            props.target = '_blank';
        }

        const icon = (
            <span className={styles.buttonIconReup}>
                <RetweetIcon />
            </span>
        );
        const text = item.stats['reposts-raw'];

        return (
            <li key="reup">{React.createElement(tag, props, [icon, text])}</li>
        );
    }

    renderFavoriteButton(item, currentUser, onActionClick) {
        const { referer } = this.props;
        const props = {
            onClick: onActionClick,
            'data-action': 'favorite',
            className: styles.button
        };
        const tag = currentUser.isLoggedIn ? 'button' : 'a';

        if (!currentUser.isLoggedIn) {
            const suffix = referer
                ? `?referer=${encodeURIComponent(referer)}`
                : '';
            const href = `${getMusicUrl(item)}${suffix}`;

            props.href = href;
            props.target = '_blank';
        }

        const icon = (
            <span className={styles.buttonIcon}>
                <HeartIcon />
            </span>
        );
        const text = item.stats['favorites-raw'];

        return (
            <li key="favorite">
                {React.createElement(tag, props, [icon, text])}
            </li>
        );
    }

    renderEmbedPanel(item) {
        const queryOptions = {
            background: this.props.showBackground ? 1 : 0,
            color: this.props.color ? this.props.color.substr(1) : null
        };

        return (
            <div className={styles.embed}>
                <EmbedTextarea
                    item={item}
                    queryOptions={queryOptions}
                    hideWordpressToggle
                />
            </div>
        );
    }

    renderAppBadges() {
        return (
            <ul className={styles.badges}>
                <li key="ios">
                    <AppBadge type="ios" style="image" />
                </li>
                <li key="android">
                    <AppBadge type="android" style="image" />
                </li>
            </ul>
        );
    }

    render() {
        const { item, onActionClick, currentUser } = this.props;

        let reupButton;
        let playlistButton;

        if (item.type !== 'playlist') {
            reupButton = this.renderReupButton(
                item,
                currentUser,
                onActionClick
            );
        }

        if (item.type === 'song') {
            playlistButton = this.renderPlaylistButton(
                item,
                currentUser,
                onActionClick
            );
        }

        return (
            <div className={styles.container}>
                <button
                    className={styles.close}
                    onClick={this.props.onMenuClick}
                >
                    <CloseIcon />
                </button>
                <ul className={styles.buttons}>
                    {this.renderFavoriteButton(
                        item,
                        currentUser,
                        onActionClick
                    )}
                    {playlistButton}
                    {reupButton}
                    <li key="twitter">
                        <a
                            href={getTwitterShareLink(process.env.AM_URL, item)}
                            target="_blank"
                            rel="noopener nofollow"
                            onClick={onActionClick}
                            className={styles.buttonTwitter}
                        >
                            <span className={styles.buttonIconTwitter}>
                                <TwitterLogo />
                            </span>
                            Twitter
                        </a>
                    </li>
                    <li key="facebook">
                        <a
                            href={getFacebookShareLink(
                                process.env.AM_URL,
                                item,
                                process.env.FACEBOOK_APP_ID
                            )}
                            target="_blank"
                            rel="noopener nofollow"
                            onClick={onActionClick}
                            className={styles.buttonFacebook}
                        >
                            <span className={styles.buttonIcon}>
                                <FacebookLogo />
                            </span>
                            Facebook
                        </a>
                    </li>
                </ul>
                {this.renderEmbedPanel(item)}
                {this.renderAppBadges()}
            </div>
        );
    }
}
