import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { favorite, unfavorite, repost, unrepost } from '../redux/modules/user';
import {
    favoritePlaylist,
    unfavoritePlaylist
} from '../redux/modules/playlist';
import EmbedActions from './EmbedActions';

class EmbedActionsContainer extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        dispatch: PropTypes.func,
        referer: PropTypes.string,
        onEmbedClick: PropTypes.func,
        currentUser: PropTypes.object,
        onMenuClick: PropTypes.func,
        color: PropTypes.string,
        showBackground: PropTypes.bool,
        overlayActive: PropTypes.bool
    };

    static defaultProps = {
        onEmbedClick() {}
    };

    constructor(props) {
        super(props);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleActionClick = (e) => {
        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        switch (action) {
            case 'embed':
                this.props.onEmbedClick();
                break;

            case 'reup':
                this.reupItem(this.props.item);
                break;

            case 'favorite':
                this.favoriteItem(this.props.item);
                break;

            default:
                console.error(`Not doing anything for action: ${action}`);
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    favoriteItem(item) {
        const { dispatch, currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            return;
        }

        const user = currentUser.profile;

        if (item.type === 'playlist') {
            if (user.favorite_playlists.indexOf(item.id) === -1) {
                dispatch(favoritePlaylist(item.id));
            } else {
                dispatch(unfavoritePlaylist(item.id));
            }
        } else if (user.favorite_music.indexOf(item.id) === -1) {
            dispatch(favorite(item.id));
        } else {
            dispatch(unfavorite(item.id));
        }
    }

    reupItem(item) {
        const { dispatch, currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            return;
        }

        const user = currentUser.profile;
        const itemId = item.id;

        if (user.reups.indexOf(itemId) === -1) {
            dispatch(repost(itemId));
        } else {
            dispatch(unrepost(itemId));
        }
    }

    render() {
        const { currentUser, item } = this.props;

        if (!this.props.overlayActive) {
            return null;
        }

        return (
            <EmbedActions
                item={item}
                referer={this.props.referer}
                currentUser={currentUser}
                onActionClick={this.handleActionClick}
                onMenuClick={this.props.onMenuClick}
                color={this.props.color}
                showBackground={this.props.showBackground}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player,
        album: state.musicAlbum,
        referer: state.stats.referer
    };
}

export default connect(mapStateToProps)(EmbedActionsContainer);
