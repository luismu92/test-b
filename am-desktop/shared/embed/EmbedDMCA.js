import React, { Component } from 'react';
import PropTypes from 'prop-types';
import analytics, { eventCategory, eventAction } from 'utils/analytics';

import AudiomackLogo from '../icons/am-logo-full';

export default class EmbedDMCA extends Component {
    static propTypes = {
        itemType: PropTypes.string.isRequired
    };

    handleLinkClick = () => {
        analytics.track(eventCategory.dcma, {
            eventAction: eventAction.embedTakedown,
            eventLabel: this.props.itemType
        });
    };

    render() {
        return (
            <div className="music-removed">
                <p>
                    This {this.props.itemType} has been removed due to a DMCA
                    Complaint. Check out our{' '}
                    <a
                        href={`${process.env.AM_URL}/songs/week`}
                        target="_blank"
                    >
                        Top Songs
                    </a>{' '}
                    or{' '}
                    <a
                        href={`${process.env.AM_URL}/albums/week`}
                        target="_blank"
                    >
                        Top Albums
                    </a>{' '}
                    section instead!
                </p>
                <a
                    className="embed__am-link embed__am-link--removed"
                    target="_blank"
                    href={process.env.AM_URL}
                >
                    <AudiomackLogo />
                </a>
            </div>
        );
    }
}
