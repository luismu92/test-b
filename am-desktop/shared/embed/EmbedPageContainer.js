import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { parse } from 'query-string';
import tinycolor from 'tinycolor2';
import GlobalMeta from 'components/GlobalMeta';

import {
    isCurrentMusicItem,
    uuid,
    yesBool,
    convertTimeStringToSeconds,
    redirectNonEmbeddedEmbeds,
    getQueueIndexForSong,
    buildQueryString,
    clamp
} from 'utils/index';
import { favorite, unfavorite, repost, unrepost } from '../redux/modules/user';
import {
    favoritePlaylist,
    unfavoritePlaylist
} from '../redux/modules/playlist';
import storage, { STORAGE_ACTIVE_EMBED_ID } from 'utils/storage';
import connectDataFetchers from 'lib/connectDataFetchers';
import { ENVIRONMENT_EMBED } from 'constants/stats/environment';

import { getSongInfo } from '../redux/modules/music/song';
import { getInfoWithSlugs } from '../redux/modules/playlist';
import { getAlbumInfo } from '../redux/modules/music/album';
import { pause, editQueue, play, seek } from '../redux/modules/player';
import {
    getStatsToken,
    setEnvironment,
    trackAction,
    setReferer
} from '../redux/modules/stats';

import VideoAdContainer from '../ad/VideoAdContainer';

import EmbedPage from './EmbedPage';

/**
 * Query parameters
 *
 * autoplay: bool
 * Autoplay the embed on page load
 *
 * twitterEmbed: bool
 * Coming from twitter embed player
 *
 * key: string
 * Key used by bloggers as a reference point of where the embed is located
 *
 * background: bool
 * Use the music artwork as the background of the embed
 *
 * t: string
 * Time to seek to
 *
 * track: number
 * Track number starting at 1 to set (only valid for albums and playlists)
 *
 * color: string
 * Accent 3 or 6 digit hex color to use throughout embed
 *
 * playback: single|multiple
 * Allow for this embed to be stopped when another embed plays
 */

class EmbedPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        match: PropTypes.object,
        player: PropTypes.object,
        song: PropTypes.object,
        album: PropTypes.object,
        playlist: PropTypes.object,
        currentUser: PropTypes.object,
        referer: PropTypes.string,
        statsToken: PropTypes.string
    };

    constructor(props) {
        super(props);

        const query = parse(props.location.search);

        this._query = {
            autoplay: yesBool(query.autoplay),
            twitterEmbed: yesBool(query.twitterEmbed),
            key: query.key,
            color: this.getColorStringFromQueryValue(query.color),
            background: yesBool(query.background),
            trackIndex: clamp(parseInt(query.track, 10) || 1, 1, Infinity) - 1,
            seekTo: convertTimeStringToSeconds(query.t),
            playback: query.playback === 'multiple' ? 'multiple' : 'single'
        };
        this.state = {
            embedOpen: false,
            showAppPrompt: false,
            overlayActive: false,
            tooltipActive: false
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;

        this._uuid = uuid();
        this._autoplayStarted = false;
        this._embedViewTracked = false;

        dispatch(getStatsToken(this._uuid));
        dispatch(setEnvironment(ENVIRONMENT_EMBED));
        dispatch(setReferer(window.__REFERER__ || document.referrer));

        if (
            this._query
                .twitterEmbed /* && process.env.NODE_ENV !== 'development' */
        ) {
            redirectNonEmbeddedEmbeds();
        }

        if (this._query.background) {
            document.body.classList.add('has-background');
        }

        window.addEventListener('storage', this.handleStorageChange, false);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, statsToken, referer } = this.props;
        const prevMusicType = prevProps.match.params.musicType;
        const currentMusicType = this.props.match.params.musicType;
        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;
        const trackIndex = this._query.trackIndex;
        const prevMusicItem = this.getMusicItem(prevMusicType, prevProps);
        const currentMusicItem = this.getMusicItem(
            currentMusicType,
            this.props
        );
        const justGotMusic = !prevMusicItem && currentMusicItem;

        let queue;

        if (justGotMusic) {
            const playerState = dispatch(editQueue(currentMusicItem));

            queue = playerState.queue;
        }

        if (
            (autoplay || seekToTime) &&
            !this._autoplayStarted &&
            justGotMusic
        ) {
            this._autoplayStarted = true;

            const tracks = currentMusicItem.tracks || [];
            const clampedTrackIndex = clamp(trackIndex, 0, tracks.length - 1);

            this.play({
                music: currentMusicItem,
                seekTo: seekToTime,
                trackIndex: clampedTrackIndex,
                queue
            });
        }

        if (!this._embedViewTracked && currentMusicItem && statsToken) {
            this._embedViewTracked = true;
            dispatch(
                trackAction(statsToken, 'ev', currentMusicItem.id, referer)
            );
        }
    }

    componentWillUnmount() {
        window.removeEventListener('storage', this.handleStorageChange);
        this._autoplayStarted = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleShowPrompt = () => {
        this.setState({ showAppPrompt: true });
    };

    handlePlayClick = (e) => {
        const button = e.currentTarget;
        const trackIndex = parseInt(
            button.getAttribute('data-track-index'),
            10
        );
        const music = null;

        this.play({ music, trackIndex });
    };

    handleEmbedClick = () => {
        this.setState({
            embedOpen: !this.state.embedOpen
        });
    };

    handleStorageChange = (e) => {
        if (e.key === STORAGE_ACTIVE_EMBED_ID) {
            const isSamePlayer = e.newValue === this._uuid;
            const shouldPause =
                !isSamePlayer && this._query.playback === 'single';

            if (shouldPause) {
                this.props.dispatch(pause());
            }
        }
    };

    handleExternalLinkClick = (e) => {
        e.preventDefault();

        const {
            player,
            dispatch,
            match: { params }
        } = this.props;
        const time = player.currentTime > 0 ? player.currentTime : null;
        const queryOptions = {
            t: Math.floor(time)
        };

        if (params.musicType !== 'song' && player.currentSong) {
            const track = !isNaN(player.currentSong.trackIndex)
                ? player.currentSong.trackIndex
                : 0;

            queryOptions.track = track + 1;
        }

        let qs = buildQueryString(queryOptions);

        if (qs) {
            qs = `?${qs}`;
        }

        dispatch(pause());
        window.open(`${e.currentTarget.href}${qs}`, '_blank');
    };

    handleCountdownFinish = () => {
        const {
            match: { params },
            dispatch
        } = this.props;
        const options = {
            key: this._query.key
        };

        switch (params.musicType) {
            case 'song':
                dispatch(
                    getSongInfo(params.artistId, params.musicSlug, options)
                );
                break;

            case 'album':
                dispatch(
                    getAlbumInfo(params.artistId, params.musicSlug, options)
                );
                break;

            case 'playlist':
                dispatch(getInfoWithSlugs(params.artistId, params.musicSlug));
                break;

            default:
                break;
        }
    };

    handleClosePromptClick = () => {
        this.setState({ showAppPrompt: false });
    };

    handleMenuClick = () => {
        this.setState({ overlayActive: !this.state.overlayActive });
    };

    handleTooltipClick = () => {
        this.setState({ tooltipActive: !this.state.tooltipActive });
    };

    handleTrackActionClick = (e) => {
        const { match } = this.props;
        const button = e.currentTarget;
        const action = button.getAttribute('data-track-action');
        const index = parseInt(button.getAttribute('data-track-index'), 10);
        const item = this.getMusicItem(match.params.musicType);
        const track = item.tracks[index];

        this.doAction(action, track, item, e);

        button.blur();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getColorStringFromQueryValue(color = '') {
        const match = color.match(/[0-9a-fA-F]{3,6}/);

        if (!match) {
            return undefined;
        }

        const matchedColor = match[0];
        const isValid = matchedColor.length === 3 || matchedColor.length === 6;

        if (!isValid) {
            return undefined;
        }

        return `#${matchedColor}`;
    }

    getMusicItem(musicType, props = this.props) {
        switch (musicType) {
            case 'song':
                return props.song.info;

            case 'album':
                return props.album.info;

            case 'playlist':
                return props.playlist.data;

            default:
                return null;
        }
    }

    getMusicErrors(musicType, props = this.props) {
        switch (musicType) {
            case 'song':
                return props.song.errors;

            case 'album':
                return props.album.errors;

            case 'playlist':
                return props.playlist.errors;

            default:
                return null;
        }
    }

    getMusicLoading(musicType, props = this.props) {
        switch (musicType) {
            case 'song':
                return props.song.loading;

            case 'album':
                return props.album.loading;

            case 'playlist':
                return props.playlist.loading;

            default:
                return false;
        }
    }

    doAction(action, track, item) {
        switch (action) {
            case 'favorite': {
                this.favoriteItem(track, item);
                break;
            }

            case 'reup': {
                this.reupItem(track, item);
                break;
            }

            case 'playlist': {
                this.playlistItem(track, item);
                break;
            }

            default:
                break;
        }
    }

    play({ music, trackIndex, seekTo = 0, queue } = {}) {
        const { dispatch, player, match } = this.props;
        const musicItem = music || this.getMusicItem(match.params.musicType);
        const currentSong = player.currentSong;
        const clickedOnTrack = !isNaN(trackIndex);
        const shouldCountTrack = !clickedOnTrack;
        const isAlreadyCurrentSong = isCurrentMusicItem(
            currentSong,
            musicItem,
            shouldCountTrack
        );

        if (player.paused) {
            storage.set(STORAGE_ACTIVE_EMBED_ID, this._uuid);
        }

        if (isAlreadyCurrentSong) {
            if (!player.paused) {
                dispatch(pause());
            } else {
                dispatch(play());
            }
            return;
        }

        const theQueue = queue || player.queue;
        const index = getQueueIndexForSong(musicItem, theQueue, {
            trackIndex: trackIndex,
            currentQueueIndex: player.queueIndex,
            ignoreHistory: false
        });

        dispatch(play(index))
            .then(() => {
                if (seekTo) {
                    dispatch(seek(seekTo));
                }
                return;
            })
            .catch((err) => console.error(err));
    }

    favoriteItem(track, parent) {
        const { dispatch, currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            this.loggedOutAction(track, parent);
            return;
        }

        const user = currentUser.profile;

        if (track.type === 'playlist') {
            if (user.favorite_playlists.indexOf(track.id) === -1) {
                dispatch(favoritePlaylist(track.id));
            } else {
                dispatch(unfavoritePlaylist(track.id));
            }
        } else if (user.favorite_music.indexOf(track.id) === -1) {
            dispatch(favorite(track.id));
        } else {
            dispatch(unfavorite(track.id));
        }
    }

    reupItem(track, parent) {
        const { dispatch, currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            this.loggedOutAction(track, parent);
        }

        const user = currentUser.profile;
        const trackId = track.id;

        if (user.reups.indexOf(trackId) === -1) {
            dispatch(repost(trackId));
        } else {
            dispatch(unrepost(trackId));
        }
    }

    playlistItem(track, parent) {
        this.loggedOutAction(track, parent);
    }

    loggedOutAction(track, parent) {
        const { referer } = this.props;
        const artistSlug =
            parent.type === 'playlist'
                ? track.uploader.url_slug
                : parent.uploader.url_slug;
        const trackSlug = `/song/${artistSlug}/${track.url_slug}`;
        const suffix = referer ? `?referer=${encodeURIComponent(referer)}` : '';

        window.open(`${trackSlug}${suffix}`, '_blank');
        return;
    }

    renderCustomColorStyle(color) {
        if (!color) {
            return null;
        }

        const ogColor = tinycolor(color);
        let alternate = ogColor.clone().lighten(10);

        if (ogColor.isLight()) {
            alternate = ogColor.clone().darken(10);
        }

        return `
            a { color: ${color}; }
            a:hover, a:focus { color: ${alternate} }
            .music-artwork__overlay { color: ${color}; }
            .embed__actions { color: ${color}; }
            .sub-menu__icon { color: ${color}; }
            .interaction-stat-icon { color: ${color}; }
            .music-detail__track-list-button:focus,
            .music-detail__track-list-button:hover,
            .music-detail__track-list-item--active,
            .music-detail__track-list-item--active .music-detail__track-list-button { color: ${color}; }
            .sub-menu [role=button]:hover, .sub-menu a:hover, .sub-menu button:hover { color: ${color}; }
            .loader-container { color: ${color}; }
            .u-orange-check { background: ${color}; }
        `;
    }

    render() {
        const { match, location, currentUser, player } = this.props;
        const musicType = match.params.musicType;
        const musicItem = this.getMusicItem(musicType);

        let videoAd;

        if (musicItem && !musicItem.amp && musicType !== 'playlist') {
            videoAd = <VideoAdContainer location={location} isEmbed />;
        }

        return (
            <Fragment>
                <GlobalMeta location={location} embed />
                <style
                    type="text/css"
                    dangerouslySetInnerHTML={{
                        __html: this.renderCustomColorStyle(this._query.color)
                    }}
                />
                <EmbedPage
                    musicItem={musicItem}
                    loading={this.getMusicLoading(musicType)}
                    errors={this.getMusicErrors(musicType)}
                    showBackground={this._query.background}
                    color={this._query.color}
                    songKey={this._query.key}
                    currentUser={currentUser}
                    player={player}
                    onShowPrompt={this.handleShowPrompt}
                    onPlayClick={this.handlePlayClick}
                    embedOpen={this.state.embedOpen}
                    showAppPrompt={this.state.showAppPrompt}
                    onClosePromptClick={this.handleClosePromptClick}
                    onEmbedClick={this.handleEmbedClick}
                    onCloseEmbedClick={this.handleEmbedClick}
                    onExternalLinkClick={this.handleExternalLinkClick}
                    onCountdownFinish={this.handleCountdownFinish}
                    overlayActive={this.state.overlayActive}
                    onMenuClick={this.handleMenuClick}
                    referer={this.props.referer}
                    location={this.props.location}
                    onTooltipClick={this.handleTooltipClick}
                    tooltipActive={this.state.tooltipActive}
                    onTrackActionClick={this.handleTrackActionClick}
                />
                {videoAd}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player,
        song: state.musicSong,
        playlist: state.playlist,
        album: state.musicAlbum,
        statsToken: state.stats.statsToken,
        referer: state.stats.referer
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(EmbedPageContainer, [
        (params, query) => {
            const options = {
                key: query.key
            };

            switch (params.musicType) {
                case 'song':
                    return getSongInfo(
                        params.artistId,
                        params.musicSlug,
                        options
                    );

                case 'album':
                    return getAlbumInfo(
                        params.artistId,
                        params.musicSlug,
                        options
                    );

                case 'playlist':
                    return getInfoWithSlugs(params.artistId, params.musicSlug);

                default:
                    return null;
            }
        }
    ])
);
