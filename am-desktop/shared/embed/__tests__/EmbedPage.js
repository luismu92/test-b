/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import EmbedPage from '../EmbedPage';

Enzyme.configure({ adapter: new Adapter() });

function getMusicItem(data = {}) {
    return {
        id: 13533,
        soundcloud_follow: '',
        dominant_color: '2e2428',
        released: '1519744999',
        download_url: '',
        volume_data:
            '[16,19,16,22,18,21,15,21,17,19,14,17,16,16,18,12,10,31,30,30,37,30,32,29,40,33,33,31,37,31,33,30,33,22,31,24,30,26,31,22,29,26,24,23,25,28,25,25,31,23,23,27,18,33,23,31,22,37,25,32,23,31,27,29,32,35,37,34,24,32,32,33,26,30,36,29,28,25,35,25,28,30,39,18,30,22,30,24,32,20,29,25,30,26,23,29,28,30,28,30,31,30,27,39,29,32,28,41,33,33,27,39,31,34,29,32,18,29,27,30,30,28,26,30,31,26,24,26,27,22,27,24,21,27,30,19,32,19,34,23,37,24,31,24,24,29,24,30,32,35,34,27,29,34,31,29,27,37,30,30,25,31,26,30,26,35,19,21,14,22,17,24,14,21,18,21,15,16,18,16,18,20,1,9,12,5,10,5,7,2,4,2,3,0,1,0,0,0,0,0,0]',
        producer: '',
        url_slug: 'qb-all-day-everyday',
        override_buy: '',
        video: 'https://www.youtube.com/watch?v=cl4CBRipZCg',
        is_featured: '',
        image:
            'https://assets.dev.audiomack.com/kevin-cool/qb-all-day-everyday-275-275-1519745152.jpg',
        genre: 'afrobeats',
        video_ad: 'yes',
        image_processing: '',
        suspension_whitelist: '',
        album: '',
        private: 'no',
        ui_color: '2e2428',
        default_follow: '',
        status: 'suspended',
        uploaded: '1519744886',
        featured_text: '',
        trend_genres: '',
        isrc: '',
        file320: '',
        album_track_only: '',
        artist: 'Kevin K Rool 3',
        updated: '1523730840',
        uploader: {
            id: '471',
            name: 'Kevin K Rool 3',
            url_slug: 'kevin-cool',
            image:
                'https://assets.dev.audiomack.com/kevin-cool/kevin-cool-275-275-1519746452.jpg',
            image_processing: '',
            hometown: 'MOBILE HOMETOWN 2',
            bio: 'TESTING MOBILE BIO UPDATE2',
            twitter: 'KevinManLegends',
            facebook: 'http://www.facebook.com/kevin',
            instagram: 'KevinManLegends',
            label: 'Mobile label 2',
            url: 'http://google.com',
            verified: 'yes',
            updated: '1519746457',
            created: '1462198829',
            status: 'active',
            video_ads: 'yes',
            follow_download: 'yes',
            suspension_whitelist: '',
            genre: 'afrobeats',
            label_owner: true,
            image_banner:
                'https://assets.dev.audiomack.com/kevin-cool/kevin-cool-1500-500-1519745461.jpg',
            followers_count: 11,
            following_count: 26,
            can_upload: true,
            type: 'artist'
        },
        soundcloud_user_name: '',
        type: 'song',
        buy_link: '',
        follow_download: '',
        description: '',
        title: 'QB-All Day Everyday',
        soundcloud_user: '',
        stream_only: 'yes',
        upc: '',
        featuring: '',
        duration: '201',
        time_ago: '1 month ago',
        live: false,
        stats: {
            pageviews: 0,
            'plays-raw': 66,
            plays: 66,
            'downloads-raw': 0,
            downloads: 0,
            embedviews: 0,
            track_dls: 0,
            'favorites-raw': 2,
            favorites: 2,
            'reposts-raw': 4,
            reposts: 4,
            'playlists-raw': 0,
            playlists: 0,
            rankings: {
                daily: 'N/A',
                weekly: 'N/A',
                monthly: 'N/A',
                yearly: 'N/A',
                total: 'N/A'
            }
        },
        released_offset: -3987243,
        amp: true,
        amp_duration: 30,
        accounting_code: 'song-qb-all-day-everyday-13533',
        ...data
    };
}

test('should show dmca notice when item status is suspended', () => {
    const musicItem = getMusicItem({
        status: 'suspended'
    });
    const component = (
        <EmbedPage musicItem={musicItem} errors={[]} player={{}} />
    );
    const wrapper = shallow(component).text();

    expect(wrapper).toBe('<EmbedDMCA />');
});

test('should show dmca notice when item status is takedown', () => {
    const musicItem = getMusicItem({
        status: 'takedown'
    });
    const component = (
        <EmbedPage musicItem={musicItem} errors={[]} player={{}} />
    );
    const wrapper = shallow(component).text();

    expect(wrapper).toBe('<EmbedDMCA />');
});
