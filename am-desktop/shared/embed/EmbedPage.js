import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';

import MusicPageMeta from 'components/MusicPageMeta';
import { DEFAULT_DATE_FORMAT } from 'constants/index';
import {
    convertSecondsToTimecode,
    renderFeaturingLinks,
    getUploader,
    getArtistName,
    getFeaturing,
    buildDynamicImage
} from 'utils/index';
import PlayerAudioContainer from 'components/player/PlayerAudioContainer';
import WaveformContainer from 'components/WaveformContainer';

import MusicStats from '../components/MusicStats';
import AndroidLoader from '../loaders/AndroidLoader';
import Avatar from '../components/Avatar';
import PlayButton from '../components/PlayButton';
import EmbedTextarea from '../components/EmbedTextarea';
import Countdown from '../components/Countdown';
import AppBadge from '../components/AppBadge';

import ThreeDotsFat from '../icons/three-dots-fat';
import AmLogo from '../icons/am-logo-full';
import CloseIcon from '../icons/close-thin';
import VolumeHighIcon from '../icons/volume-high';
import AudiomackLogo from '../icons/am-logo-full';
import AudiomackMark from 'icons/am-mark-color';
import PlusIcon from '../icons/plus-thin';

import EmbedlyApi from './EmbedlyApi';
import EmbedDMCA from './EmbedDMCA';
import EmbedNotFound from './EmbedNotFound';
import EmbedActionsContainer from './EmbedActionsContainer';
import TrackListingActions from '../components/TrackListingActions';
import Verified from '../../../am-shared/components/Verified';

export default class EmbedPage extends Component {
    static propTypes = {
        onExternalLinkClick: PropTypes.func,
        onPlayClick: PropTypes.func,
        onCountdownFinish: PropTypes.func,
        errors: PropTypes.array,
        loading: PropTypes.bool,
        color: PropTypes.string,
        songKey: PropTypes.string,
        showBackground: PropTypes.bool,
        embedOpen: PropTypes.bool,
        onShowPrompt: PropTypes.func,
        onClosePromptClick: PropTypes.func,
        showAppPrompt: PropTypes.bool,
        onEmbedClick: PropTypes.func,
        onCloseEmbedClick: PropTypes.func,
        location: PropTypes.object,
        player: PropTypes.object,
        musicItem: PropTypes.object,
        currentUser: PropTypes.object,
        onMenuClick: PropTypes.func,
        overlayActive: PropTypes.bool,
        referer: PropTypes.string,
        onTooltipClick: PropTypes.func,
        mounted: PropTypes.bool,
        onTrackActionClick: PropTypes.func
    };

    static defaultProps = {
        onAudioTimeUpdate() {},
        onAudioLoadedMetadata() {},
        onAudioEnded() {},
        onPlayClick() {}
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    isNotReleased(item) {
        let hasStreamingUrl = !!item.streaming_url;

        if (item.tracks && item.tracks.length) {
            hasStreamingUrl = item.tracks.some((t) => t.streaming_url);
        }

        return (
            item.released_offset > 0 && !(this.props.songKey && hasStreamingUrl)
        );
    }

    renderFeaturing(musicItem) {
        const featuring = getFeaturing(musicItem);

        if (!featuring) {
            return null;
        }

        return (
            <li className="u-trunc music__meta-featuring">
                <strong>
                    {renderFeaturingLinks(featuring, { target: '_blank' })}
                </strong>
            </li>
        );
    }

    renderProducer(musicItem) {
        if (!musicItem.producer) {
            return null;
        }

        return (
            <li className="u-trunc music__meta-producer">
                Producer: <strong>{musicItem.producer}</strong>
            </li>
        );
    }

    renderReleased(released, uploader) {
        if (!released) {
            return null;
        }

        const date = moment(released * 1000).format(DEFAULT_DATE_FORMAT);

        const check = (
            <Verified user={uploader} small color={this.props.color} />
        );

        return (
            <li className="u-trunc music__meta-added">
                Added on {date} by{' '}
                <a
                    href={`/artist/${uploader.url_slug}`}
                    target="_blank"
                    className="u-pos-relative"
                >
                    <strong>{uploader.name}</strong> {check}
                </a>
            </li>
        );
    }

    renderStats(stats) {
        if (!stats) {
            return null;
        }

        return <MusicStats stats={stats} />;
    }

    renderTrackList(musicItem, player) {
        if (!musicItem.tracks || this.isNotReleased(musicItem)) {
            return null;
        }

        const filteredTracks = musicItem.tracks.filter(
            (track) => !track.geo_restricted
        );

        const listItems = filteredTracks.map((track, i) => {
            const activeTrack =
                !!player.currentSong && player.currentSong.id === track.id;
            const klass = classnames(
                'music-detail__track-list-item u-clearfix',
                {
                    'music-detail__track-list-item--active':
                        !player.paused && activeTrack
                }
            );

            let featuring = renderFeaturingLinks(track.featuring, {
                featText: 'feat. ',
                removeLinks: true
            });

            if (featuring) {
                featuring = (
                    <span className="tracklist__track-featuring">
                        ({featuring})
                    </span>
                );
            }

            return (
                <li key={`${track.id}:${i}`} className={klass}>
                    <VolumeHighIcon className="icon" />
                    {this.renderPlaylistAddButton(track)}
                    <button
                        onClick={this.props.onPlayClick}
                        className="music-detail__track-list-button"
                        disabled={activeTrack}
                        data-track-index={i}
                    >
                        <span className="music-detail__track-list-title">
                            {track.title} {featuring}
                        </span>
                        <span className="music-detail__track-list-duration u-right">
                            {convertSecondsToTimecode(track.duration)}
                        </span>
                    </button>
                    <TrackListingActions
                        index={i}
                        musicItem={musicItem}
                        track={track}
                        currentUser={this.props.currentUser}
                        onTrackActionClick={this.props.onTrackActionClick}
                        showRemoveFromQueue={false}
                        hideQueueAddActions={true}
                        hideQueueLastAction={true}
                        hideQueueRemoveAction={true}
                        isEmbed
                    />
                </li>
            );
        });

        return <ol className="music-detail__track-list">{listItems}</ol>;
    }

    renderWaveformOrEmbedPanel(embedOpen, item, elapsedDisplay, player) {
        const { paused, loading } = player;
        const stillProcessing = item.streaming_url === '';
        const queryOptions = {
            background: this.props.showBackground ? 1 : 0,
            color: this.props.color ? this.props.color.substr(1) : null
        };

        if (!item) {
            return null;
        }

        if (embedOpen) {
            return (
                <EmbedTextarea
                    item={item}
                    onCloseButtonClick={this.props.onCloseEmbedClick}
                    queryOptions={queryOptions}
                    hideWordpressToggle
                    showCloseButton
                />
            );
        }

        if (this.isNotReleased(item)) {
            return (
                <div className="countdown countdown--dark u-box-shadow u-spacing-top-em">
                    <Countdown
                        seconds={item.released_offset}
                        type={item.type}
                        onFinish={this.props.onCountdownFinish}
                    />
                </div>
            );
        }

        const buttonProps = {
            'data-tooltip': stillProcessing
                ? 'This item is still processing'
                : null,
            'data-tooltip-active': stillProcessing ? true : null
        };

        const waveformProps = {};
        if (this.props.showBackground) {
            waveformProps.color = 'rgba(199, 199, 199, 0.4)';
        }

        return (
            <div className="music-detail__waveform-wrap waveform-wrap waveform-wrap--has-button">
                <span {...buttonProps}>
                    <PlayButton
                        className="waveform-wrap__play-button"
                        loading={loading || stillProcessing}
                        paused={paused}
                        fillColor={this.props.color}
                        onButtonClick={this.props.onPlayClick}
                    />
                </span>
                <WaveformContainer
                    musicItem={item}
                    fillColor={this.props.color}
                    {...waveformProps}
                />

                <span className="waveform__elapsed waveform__time">
                    {convertSecondsToTimecode(elapsedDisplay)}
                </span>
                <span className="waveform__duration waveform__time">
                    {convertSecondsToTimecode(item.duration || player.duration)}
                </span>
            </div>
        );
    }

    renderSinglePlayButton(item, player) {
        const { paused, loading } = player;
        const stillProcessing = item.streaming_url === '';

        const buttonProps = {
            'data-tooltip': stillProcessing
                ? 'This item is still processing'
                : null,
            'data-tooltip-active': stillProcessing ? true : null
        };

        return (
            <span className="embed__slim-play" {...buttonProps}>
                <PlayButton
                    className="single-play-button waveform-wrap__play-button"
                    loading={loading || stillProcessing}
                    paused={paused}
                    fillColor={this.props.color}
                    onButtonClick={this.props.onPlayClick}
                />
            </span>
        );
    }

    renderAppPrompt() {
        const { showAppPrompt, onClosePromptClick } = this.props;

        const klass = classnames('embed-prompt', {
            'embed-prompt--active': showAppPrompt
        });

        return (
            <div className={klass}>
                <button
                    className="embed-prompt__close"
                    onClick={onClosePromptClick}
                >
                    <CloseIcon />
                </button>
                <div className="embed-prompt__inner u-text-center">
                    <div>
                        <div className="embed-prompt__logo u-spacing-x-auto u-spacing-bottom-15">
                            <AmLogo />
                        </div>
                        <p className="embed-prompt__blurb u-spacing-bottom-15 u-fw-700 u-lh-14">
                            Stream and download breaking new music free, save
                            data, and discover millions of new songs on the
                            Audiomack app.
                        </p>
                        <a
                            className="button button--pill"
                            target="_blank"
                            rel="nofollow noopener"
                            href="https://audiomack.app.link/embed-overlay"
                        >
                            Download the Audiomack app
                        </a>
                        <ul className="app-badges embed-prompt__app-badges u-text-center u-inline-list">
                            <li className="u-spacing-x-10">
                                <AppBadge type="ios" style="image" />
                            </li>
                            <li className="u-spacing-x-10">
                                <AppBadge type="android" style="image" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }

    renderPlaylistAddButton(item) {
        const { musicItem, referer } = this.props;
        const suffix = referer ? `?referer=${encodeURIComponent(referer)}` : '';

        const artistSlug =
            musicItem.type === 'playlist'
                ? musicItem.artist.url_slug
                : musicItem.uploader.url_slug;
        const href = `${process.env.AM_URL}/song/${artistSlug}/${
            item.url_slug
        }${suffix}`;

        return (
            <a className="tracklist__track-add" target="_blank" href={href}>
                <PlusIcon />
            </a>
        );
    }

    renderMenuButton() {
        const { color } = this.props;

        const styles = {};

        if (color) {
            styles.color = color;
        }

        return (
            <button
                className="embed__dots"
                onClick={this.props.onMenuClick}
                style={styles}
            >
                <ThreeDotsFat />
            </button>
        );
    }

    render() {
        const {
            musicItem,
            player,
            embedOpen,
            errors,
            showBackground,
            color,
            onExternalLinkClick,
            onShowPrompt,
            currentUser
        } = this.props;

        if (errors.length) {
            return <EmbedNotFound />;
        }

        if (this.props.loading) {
            return (
                <div className="u-vh u-flex-center">
                    <AndroidLoader />
                </div>
            );
        }

        if (!musicItem) {
            return null;
        }

        if (
            musicItem.status === 'takedown' ||
            musicItem.status === 'suspended'
        ) {
            return <EmbedDMCA itemType={musicItem.type} />;
        }

        const { image, title, released, stats } = musicItem;
        const uploader = getUploader(musicItem);
        const { currentTime } = player;
        const elapsedDisplay = currentTime || 0;
        let background;

        if (showBackground) {
            const bgImg = buildDynamicImage(image, {
                width: 800,
                height: 800,
                max: true
            });
            background = (
                <div
                    className="embed__background"
                    style={{
                        backgroundImage: `url(${bgImg})`
                    }}
                />
            );
        }

        const klass = classnames(
            `music-detail music-detail--${musicItem.type} embed embed--${
                musicItem.type
            }`,
            {
                'embed--with-background': showBackground,
                'embed--custom-color': color
            }
        );

        const musicLink = `${process.env.AM_URL}/${musicItem.type}/${
            uploader.url_slug
        }/${musicItem.url_slug}`;

        return (
            <div className={klass}>
                {background}
                <MusicPageMeta
                    musicItem={musicItem}
                    currentUser={currentUser}
                    location={this.props.location}
                />
                <EmbedlyApi musicItem={musicItem} />
                <div className="embed__inner">
                    <div className="music-detail__top u-clearfix">
                        <Avatar
                            type={musicItem.type}
                            image={musicItem.image_base || musicItem.image}
                            className="music-artwork music-detail__image"
                            rounded={false}
                            size={170}
                        />
                        {this.renderSinglePlayButton(musicItem, player)}
                        <div className="music-detail__content">
                            <a
                                className="music-detail__link"
                                href={musicLink}
                                target="_blank"
                                onClick={onExternalLinkClick}
                            >
                                <h2 className="music__heading">
                                    <span className="music__heading--artist u-trunc u-d-block">
                                        {getArtistName(musicItem)}
                                    </span>
                                    <span className="music__heading--title u-trunc u-d-block">
                                        {title}
                                    </span>
                                </h2>
                            </a>
                            <ul className="music__meta embed__music-meta">
                                {this.renderFeaturing(musicItem)}
                                {this.renderProducer(musicItem)}
                                {this.renderReleased(
                                    released,
                                    musicItem.uploader
                                )}
                            </ul>
                            {this.renderStats(stats)}
                        </div>
                        {this.renderMenuButton()}
                        <a
                            className="embed__am-link"
                            target="_blank"
                            href={musicLink}
                            onClick={onExternalLinkClick}
                        >
                            <span className="embed__am-link-mark">
                                <AudiomackMark className="u-no-fill" />
                            </span>
                            <span className="embed__am-link-full">
                                <AudiomackLogo />
                            </span>
                        </a>
                        <EmbedActionsContainer
                            item={musicItem}
                            onEmbedClick={this.props.onEmbedClick}
                            onMenuClick={this.props.onMenuClick}
                            color={this.props.color}
                            showBackground={this.props.showBackground}
                            overlayActive={this.props.overlayActive}
                        />
                    </div>
                    {this.renderWaveformOrEmbedPanel(
                        embedOpen,
                        musicItem,
                        elapsedDisplay,
                        player
                    )}
                    {this.renderTrackList(musicItem, player)}
                    <PlayerAudioContainer onStop={onShowPrompt} />
                </div>
                {this.renderAppPrompt()}
            </div>
        );
    }
}
