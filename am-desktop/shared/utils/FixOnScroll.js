import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { passiveOption, throttle } from 'utils/index';

const MAX_RETRIES = 3;

export default class FixOnScroll extends Component {
    static propTypes = {
        fixToId: PropTypes.string.isRequired,
        style: PropTypes.object,
        offset: PropTypes.number,
        offsetGap: PropTypes.number,
        fixedClassName: PropTypes.string,
        className: PropTypes.string,
        onBeforeFixChange: PropTypes.func,
        onFixChange: PropTypes.func,
        elementType: PropTypes.string,
        children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
    };

    static defaultProps = {
        elementType: 'div',
        onBeforeFixChange() {},
        onFixChange() {},
        style: {},
        offsetGap: 0,
        offset: 0
    };

    constructor(props) {
        super(props);

        this.state = {
            fixed: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const option = passiveOption();

        window.addEventListener('scroll', this.handleWindowScroll, option);
        setTimeout(() => this.setInternals(), 500);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleWindowScroll);
        this._fixToElementOffsetTop = null;
        this._componentRect = null;
        this._componentComputed = null;
        this._componentTop = null;
        this._component = null;
        this._observerRetries = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowScroll = throttle(() => {
        this.checkFixedNav();
    }, 250);

    ////////////////////
    // Helper methods //
    ////////////////////

    setInternals() {
        const { fixToId } = this.props;
        const el = document.getElementById(fixToId);

        if (!el) {
            if (this._observerRetries === MAX_RETRIES) {
                console.warn(`Cant find element to observe: ${fixToId}`);
                return;
            }

            this._observerRetries += 1;
            // If for some reason the element isnt mounted yet
            setTimeout(() => this.setInternals(), 500);
            return;
        }

        const rect = el.getBoundingClientRect();

        let computed;

        try {
            computed = window.getComputedStyle(this._component);
        } catch (err) {
            return;
        }

        this._fixToElementOffsetTop =
            rect.top + rect.height + this.props.offsetGap;
        this._componentRect = this._component.getBoundingClientRect();
        this._componentComputed = computed;
        this._componentTop = this._componentRect.top + window.pageYOffset;
        this._observerRetries = 0;
    }

    checkFixedNav() {
        if (!this._fixToElementOffsetTop) {
            return;
        }

        const atScrollThreshold =
            window.pageYOffset >=
            this._componentTop -
                this._fixToElementOffsetTop +
                this.props.offset;

        if (atScrollThreshold && !this.state.fixed) {
            const state = true;

            this.props.onBeforeFixChange(state, this._component);
            this.setState(
                {
                    fixed: state
                },
                () => {
                    // Would like to use requestIdleCallback but there doesn
                    // seem to be a great polyfill available. Might be able
                    // to get around this when we merge in react 16
                    // https://github.com/facebook/react/pull/8833
                    window.requestAnimationFrame(() => {
                        this.props.onFixChange(state, this._component);
                    });
                }
            );
        } else if (this.state.fixed && !atScrollThreshold) {
            const state = false;

            this.props.onBeforeFixChange(state, this._component);
            this.setState(
                {
                    fixed: state
                },
                () => {
                    // Would like to use requestIdleCallback but there doesn
                    // seem to be a great polyfill available. Might be able
                    // to get around this when we merge in react 16
                    // https://github.com/facebook/react/pull/8833
                    window.requestAnimationFrame(() => {
                        this.setInternals();
                        this.props.onFixChange(state, this._component);
                    });
                }
            );
        }
    }

    render() {
        const { elementType, className, fixedClassName, style } = this.props;
        const props = {
            className: classnames(className, {
                [fixedClassName]: this.state.fixed
            }),
            style,
            ref: (c) => (this._component = c)
        };

        if (this.state.fixed) {
            const { marginLeft, marginRight } = this._componentComputed;
            const left = parseInt(marginLeft, 10);
            const right = parseInt(marginRight, 10);
            const margins = left + right;

            props.style = {
                ...style,
                position: 'fixed',
                top: `${this._fixToElementOffsetTop}px`,
                left: `${this._componentRect.left - left}px`,
                width: `${this._componentRect.width + margins}px`
            };
        }

        return React.createElement(elementType, props, this.props.children);
    }
}
