// Intersection Observer does seem to work well for locking
// things to say the header bar. Saving this here anyway

import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';

// The polyfill needs window to be present
// yarn add `intersection-observer`
// if (process.env.BROWSER) {
//     require('intersection-observer');
// }

const MAX_RETRIES = 3;

export default class FixOnScroll extends Component {
    static propTypes = {
        rootId: PropTypes.string.isRequired,
        fixedClassName: PropTypes.string,
        className: PropTypes.string,
        elementType: PropTypes.string,
        children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
    };

    static defaultProps = {
        elementType: 'div'
    };

    constructor(props) {
        super(props);

        this.state = {
            fixed: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        setTimeout(() => this.setObserver(), 500);
    }

    componentWillUnmount() {
        this._fixToElement = null;
        this._intersectionObserver.disconnect();
        this._intersectionObserver = null;
        this._observerRetries = null;
        this._lastEntryValue = 1;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleIntersection = (entries, observer) => {
        console.warn(entries, observer);

        entries.forEach((entry) => {
            if (entry.intersectionRatio < this._lastEntryValue) {
                this.setState({
                    fixed: true
                });
            } else {
                this.setState({
                    fixed: false
                });
            }
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getElementToFixTo(id) {
        if (this._fixToElement) {
            return this._fixToElement;
        }

        this._fixToElement = document.getElementById(id);

        return this._fixToElement;
    }

    setObserver() {
        const { rootId } = this.props;
        const el = document.getElementById(rootId);

        if (!el) {
            if (this._observerRetries === MAX_RETRIES) {
                console.warn(`Cant find element to observe: ${rootId}`);
                return;
            }

            this._observerRetries += 1;
            // If for some reason the element isnt mounted yet
            setTimeout(() => this.setObserver(), 500);
            return;
        }

        const rect = el.getBoundingClientRect();
        const observerOptions = {
            root: null,
            rootMargin: `0px 0px -${rect.height + rect.top}px 0px`,
            threshold: [0, 0.5, 1.0]
        };

        const component = findDOMNode(this);

        this._observerRetries = 0;
        this._intersectionObserver = new IntersectionObserver(
            this.handleIntersection,
            observerOptions
        );
        this._intersectionObserver.POLL_INTERVAL = 100; // Time in milliseconds.
        this._intersectionObserver.observe(component);
    }

    render() {
        const { elementType, className, fixedClassName } = this.props;
        const props = {
            className: classnames(className, {
                [fixedClassName]: fixedClassName
            })
        };

        return React.createElement(elementType, props, this.props.children);
    }
}
