import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import styles from './Button.module.scss';

function Button({
    width,
    height,
    style,
    href,
    onClick,
    icon,
    text,
    disabled,
    buttonProps,
    hasChevron
}) {
    let tag = 'button';

    if (href) {
        tag = Link;
    }

    const klass = classnames(styles.button, {
        [styles.hasChevron]: hasChevron
    });

    const props = {
        className: klass,
        onClick: onClick,
        style: style || {},
        ...buttonProps
    };

    if (width) {
        props.style.width = `${width}px`;
    }

    if (height) {
        props.style.height = `${height}px`;
    }

    if (href) {
        props.to = href;
    }

    if (disabled) {
        props.disabled = disabled;
    }

    let buttonIcon;

    if (icon) {
        buttonIcon = <span className={styles.icon}>{icon}</span>;
    }

    let chevron;
    if (hasChevron) {
        chevron = <span className={styles.chevron} />;
    }

    return React.createElement(
        tag,
        props,
        <Fragment>
            {buttonIcon}
            {text}
            {chevron}
        </Fragment>
    );
}

Button.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    href: PropTypes.string,
    onClick: PropTypes.func,
    icon: PropTypes.element,
    text: PropTypes.string,
    style: PropTypes.object,
    disabled: PropTypes.bool,
    buttonProps: PropTypes.object,
    hasChevron: PropTypes.bool
};

Button.defaultProps = {
    onClick() {}
};

export default Button;
