import React from 'react';
import PropTypes from 'prop-types';

import baseStyles from './Button.module.scss';
import variantStyles from './TagButton.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

function TagButton({ text, action }) {
    return (
        <button
            className={`${styles.base.button} ${styles.variant.button}`}
            data-action={action}
        >
            {text}
        </button>
    );
}

TagButton.propTypes = {
    text: PropTypes.string,
    action: PropTypes.string
};

export default TagButton;
