import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import styles from './ContextButton.module.scss';

function ContextButton({ href, text, onClick, context, active }) {
    let tag = 'button';

    if (href) {
        tag = Link;
    }

    const klass = classnames(styles.button, {
        [styles.active]: active
    });

    const buttonProps = {
        className: klass,
        onClick: onClick,
        'data-context': context
    };

    if (href) {
        buttonProps.to = href;
    }

    return React.createElement(tag, buttonProps, <Fragment>{text}</Fragment>);
}

ContextButton.propTypes = {
    href: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    context: PropTypes.string,
    active: PropTypes.bool
};

export default ContextButton;
