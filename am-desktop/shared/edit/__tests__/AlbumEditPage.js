/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import AlbumEditPage from '../AlbumEditPage';
import UploadItem from '../../upload/UploadItem';

Enzyme.configure({ adapter: new Adapter() });

function noop() {}

test.skip('should show upload items that are in progress', () => {
    const upload = {
        policy:
            'eyJleHBpcmF0aW9uIjoiMjAxNy0wOS0wN1QwNToyMjo1My4wMDBaIiwiY29uZGl0aW9ucyI6W3siYnVja2V0IjoiYXVkaW9tYWNrLmRldi51cGxvYWRzIn0seyJhY2wiOiJwcml2YXRlIn0sWyJzdGFydHMtd2l0aCIsIiRrZXkiLCJtYWNsaS00NiJdLFsic3RhcnRzLXdpdGgiLCIkQ29udGVudC1UeXBlIiwiIl0sWyJzdGFydHMtd2l0aCIsIiRzdWNjZXNzX2FjdGlvbl9zdGF0dXMiLCIiXSxbInN0YXJ0cy13aXRoIiwiJG5hbWUiLCIiXSxbInN0YXJ0cy13aXRoIiwiJEZpbGVuYW1lIiwiIl1dfQ==',
        signature: 'tENA9DIctNjcOjjEHfFft/bQmvk=',
        defaultGenre: null,
        currentUploadType: 'album',
        loading: false,
        inProgress: true,
        policyError: false,
        fileInput: {
            lastUpdated: 0,
            id: null,
            uploadType: null
        },
        bytesUploaded: 1671168,
        bytesTotal: 11325026,
        list: [
            {
                id: 0,
                bytesUploaded: 1671168,
                bytesTotal: 11325026,
                title: 'album.zip',
                type: 'album',
                musicId: null,
                saving: false,
                errors: [],
                status: 2 // uploading value
            }
        ]
    };

    const uploadAlbum = {
        isAddingSingles: false,
        preExistingTrackQueue: []
    };

    const musicEdit = {
        info: {
            title: 'test'
        }
    };

    const currentUser = {
        profile: {}
    };

    const wrapper = shallow(
        <AlbumEditPage
            currentUploadStep={{ number: 2 }}
            upload={upload}
            uploadAlbum={uploadAlbum}
            musicEdit={musicEdit}
            currentUser={currentUser}
            onUploadCancelClick={noop}
        />
    );

    const wrapperHasSongUpload = wrapper.find(UploadItem).length;

    expect(wrapperHasSongUpload).toBe(1);
});
