import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { allGenresMap, tagSections } from 'constants/index';
import { getArtistName, uploadSlugify } from 'utils/index';
import {
    handleMusicInputChange,
    getBasicInvalidFields,
    getErroredKeysFromUpdateRequest
} from 'utils/music/edit';
import { eventCategory } from 'utils/analytics';
import requireAuth from '../hoc/requireAuth';

import {
    reset as resetUploadList,
    cancelUpload
} from '../redux/modules/upload/index';
import {
    reset as resetAlbumUpload,
    addPreExistingAlbumTrack,
    queuePreExistingTracks
} from '../redux/modules/upload/album';
import {
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_ALERT,
    showModal,
    hideModal
} from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import {
    updateMetadata,
    deleteItem,
    deleteAlbumTrackItem,
    reorderAlbumTracks
} from '../redux/modules/music/index';
import { updateItemInfo } from '../redux/modules/music/edit';
import {
    getGlobalGeoTags,
    getMusicTags,
    addMusicTags,
    deleteMusicTags
} from '../redux/modules/music/tags';
import { fileInput } from '../redux/modules/upload/index';

import AlbumEditPage from './AlbumEditPage';

class AlbumEditPageContainer extends Component {
    static propTypes = {
        musicEdit: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        currentUploadStep: PropTypes.object,
        previousUploadStep: PropTypes.object,
        noLeaveModal: PropTypes.bool,
        sidebarButtons: PropTypes.array,
        dispatch: PropTypes.func,
        autosave: PropTypes.bool,
        finishButtonText: PropTypes.string,
        finishingButtonText: PropTypes.string,
        onAlbumDelete: PropTypes.func,
        onAlbumSave: PropTypes.func,
        preExistingTrackQueue: PropTypes.array,
        isEditPage: PropTypes.bool,
        upload: PropTypes.object,
        uploadAlbum: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        fromMobile: PropTypes.bool,
        AlbumDetails: PropTypes.element,
        SongUploads: PropTypes.element,
        tags: PropTypes.object,
        onSuggestTagClick: PropTypes.func
    };

    static defaultProps = {
        autosave: true,
        onAlbumDelete() {},
        onAlbumSave() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            urlSlugManuallyUpdated: false,
            activeTabIndex: 0,
            needsReordering: false,
            isPreviewing: false,
            isFinishing: false,
            isAutoSaving: false,
            dirty: false,
            errors: {}
        };
    }

    componentDidMount() {
        this._unblock = this.props.history.block(
            this.handleHistoryLocationChange
        );
        this.fetchTags();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.isAutoSaving) {
            const prevAutoSavingState = this.itemIsSaving(prevProps.musicEdit);
            const nextAutoSavingState = this.itemIsSaving(this.props.musicEdit);

            if (
                prevAutoSavingState !== nextAutoSavingState &&
                !nextAutoSavingState
            ) {
                // eslint-disable-next-line
                this.setState({
                    isAutoSaving: false
                });
            }
        }

        const { currentUploadStep, isEditPage } = this.props;
        const previousUploadStep = prevProps.currentUploadStep;

        if (previousUploadStep.number === 4 && currentUploadStep.number === 5) {
            this.finishAlbum();
        }

        if (
            isEditPage &&
            previousUploadStep.number !== 5 &&
            currentUploadStep.number === 5
        ) {
            this.finishAlbum();
        }

        const wasAddingSingles = prevProps.uploadAlbum.isAddingSingles;
        const { isAddingSingles } = this.props.uploadAlbum;

        if (wasAddingSingles && !isAddingSingles) {
            this.processPreExistingTrackQueue();
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        if (this._unblock) {
            this._unblock();
        }
        this._unblock = null;
        clearTimeout(this._albumAutosaveTimer);
        clearTimeout(this._trackAutosaveTimer);
        this._albumAutosaveTimer = null;
        this._trackAutosaveTimer = null;

        this._processingPreExistingTrackQueue = null;

        dispatch(resetUploadList());
        dispatch(resetAlbumUpload());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleDropdownChange = async (text, e) => {
        const { dispatch, musicEdit, tags } = this.props;
        const genres = Object.values(allGenresMap);
        const button = e.target;
        const dropdown = button.closest('[data-dropdown]');
        const id = parseInt(dropdown.getAttribute('data-music-id'), 10);
        const section = dropdown.getAttribute('data-name');
        const value = button.getAttribute('data-value').replace(/\-\>/g, ' ');
        const list = tags.id[id] || [];
        const musicItem = musicEdit.info.tracks.find(
            (track) => track.song_id === id
        );

        musicItem.id = id;

        if (!value) {
            return;
        }

        try {
            switch (section) {
                case tagSections.locations: {
                    break;
                }

                case tagSections.mood: {
                    const removeMood = list.find(
                        (tag) => tag.normalizedKey === value
                    );

                    if (removeMood) {
                        await dispatch(
                            deleteMusicTags(musicItem, removeMood.normalizedKey)
                        );

                        return;
                    }

                    const currentMoods = list.filter(
                        (tag) => tag.section === tagSections.mood
                    );

                    if (currentMoods.length === 2) {
                        throw new Error(
                            'You can only have 2 tags from this category.'
                        );
                    }

                    break;
                }

                case tagSections.subgenre: {
                    const removeSubgenre = list.find(
                        (tag) => tag.normalizedKey === value
                    );

                    if (removeSubgenre) {
                        await dispatch(
                            deleteMusicTags(
                                musicItem,
                                removeSubgenre.normalizedKey
                            )
                        );

                        return;
                    }

                    const currentSubgenres = list.filter(
                        (tag) =>
                            tag.section === tagSections.subgenre ||
                            genres.includes(tag.section)
                    );

                    if (currentSubgenres.length === 2) {
                        throw new Error(
                            'You can only have 2 tags from this category.'
                        );
                    }

                    break;
                }

                default:
                    break;
            }

            await dispatch(addMusicTags(musicItem, value));
        } catch (error) {
            dispatch(
                addToast({
                    action: 'message',
                    item: error.message
                })
            );
        }
    };

    handleHistoryLocationChange = (nextLocation) => {
        const { dispatch, autosave, location, noLeaveModal } = this.props;
        const isEditPage = location.pathname.match(/^\/edit\/album\/\d+/);

        if ((!this.state.dirty && !autosave) || (isEditPage && autosave)) {
            if (this._unblock) {
                this._unblock();
                this._unblock = null;
            }
            return true;
        }

        if (noLeaveModal) {
            return true;
        }

        const message = autosave
            ? 'Your changes have been automatically saved but the album will not be live until you press "Finish".'
            : 'You have unsaved changes.';

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure you want to leave?',
                message,
                handleConfirm: () => this.confirmLeave(nextLocation),
                confirmButtonText: 'Leave anyway',
                confirmButtonProps: {
                    className: 'button button--pill'
                }
            })
        );

        return false;
    };

    handleCancelClick = (e) => {
        const id = e.currentTarget.getAttribute('data-upload-id');

        const { dispatch } = this.props;

        dispatch(cancelUpload(id));
    };

    handlePreExistingCancelClick = (e) => {
        const { dispatch, uploadAlbum } = this.props;
        const { preExistingTrackQueue } = uploadAlbum;
        const id = e.currentTarget.getAttribute('data-upload-id');
        const newList = preExistingTrackQueue.filter(
            (track) => track.id.toString() !== id
        );

        dispatch(queuePreExistingTracks(newList));
    };

    handleAlbumInfoSubmit = (e) => {
        e.preventDefault();

        this.setState({
            hasSubmittedAlbumInfo: true
        });
    };

    handleAlbumInfoEdit = () => {
        this.setState({
            hasSubmittedAlbumInfo: false
        });
    };

    handleAlbumInfoInput = (e) => {
        const { dispatch, autosave, musicEdit } = this.props;
        const input = e.currentTarget;
        const musicId = musicEdit.info.id;
        const errors = this.state.errors[musicId] || {};
        const change = handleMusicInputChange(input);
        let urlSlugManuallyUpdated = this.state.urlSlugManuallyUpdated;

        if (change.name === 'url_slug') {
            if (change.error) {
                return Promise.reject(change.error);
            }

            urlSlugManuallyUpdated = true;
        }

        errors[change.name] = !!change.error;

        this.setState((prevState) => {
            return {
                errors: {
                    ...prevState.errors,
                    [musicId]: errors
                },
                urlSlugManuallyUpdated
            };
        });

        dispatch(updateItemInfo(change.name, change.value));

        // Update url slug bassed on title if user hasnt touched it yet
        if (change.name === 'title' && !urlSlugManuallyUpdated) {
            dispatch(updateItemInfo('url_slug', uploadSlugify(change.value)));
        }

        if (autosave) {
            clearTimeout(this._albumAutosaveTimer);

            this.setState({
                isAutoSaving: true
            });

            const metadata = {
                ...musicEdit.info,
                [change.name]: change.value
            };

            // Delete potential status numbers from tracks
            metadata.tracks = (metadata.tracks || []).map((track) => {
                delete track.status;

                return track;
            });

            const autosaveErrors = getBasicInvalidFields(metadata);

            if (autosaveErrors) {
                this.setState((prevState) => {
                    return {
                        errors: {
                            ...prevState.errors,
                            [musicId]: {
                                ...(prevState.errors[musicId] || {}),
                                ...autosaveErrors
                            }
                        },
                        isAutoSaving: false
                    };
                });

                return Promise.reject(autosaveErrors);
            }

            return new Promise((resolve, reject) => {
                this._albumAutosaveTimer = setTimeout(() => {
                    dispatch(updateMetadata(musicEdit.info.id, metadata))
                        .then(resolve)
                        .catch((err) => {
                            this.setErrorsFromUpdateRequest(
                                musicEdit.info.id,
                                err
                            );
                            reject(err);
                        });
                }, 3000);
            });
        }

        this.setState({
            dirty: true
        });

        return Promise.resolve();
    };

    handleAlbumDeleteClick = (e) => {
        e.preventDefault();

        console.log('delete', e);
        const { dispatch, musicEdit } = this.props;
        const title = e.currentTarget.getAttribute('data-modal-title');

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title,
                message:
                    'Are you sure you want to delete this album? This action cannot be undone.',
                handleConfirm: () => this.deleteAlbum(musicEdit.info.id),
                confirmButtonText: 'Delete',
                confirmButtonProps: {
                    className: 'button button--pill button--danger',
                    'data-testid': 'deleteConfirmButton'
                }
            })
        );
    };

    handleAlbumTrackDeleteClick = (e) => {
        const albumTrackOnly =
            e.currentTarget.getAttribute('data-album-track-only') === 'true';
        const trackId = e.currentTarget.getAttribute('data-music-id');
        const { dispatch, musicEdit } = this.props;

        let title = 'Remove album track';
        let message =
            'Are you sure you want to remove this track from the album?';
        let confirmButtonText = 'Remove';

        if (albumTrackOnly) {
            title = 'Delete album track';
            message =
                'Are you sure you want to delete this track from the album? This action cannot be undone.';
            confirmButtonText = 'Delete';
        }

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title,
                message,
                handleConfirm: () =>
                    this.deleteAlbumTrack(musicEdit.info.id, trackId),
                confirmButtonText,
                confirmButtonProps: {
                    className: 'button button--pill button--danger',
                    'data-testid': 'deleteConfirmButton'
                }
            })
        );
    };

    handleTrackReplaceInputChange = (e) => {
        const { dispatch } = this.props;
        const id = e.currentTarget.id;
        const uploadType = e.currentTarget.getAttribute('data-upload-type');
        const replaceId = e.currentTarget.getAttribute('data-replace-id');

        dispatch(fileInput(id, uploadType, replaceId));
    };

    handleAlbumTrackSaveClick = (musicId, info, change, trackNumber) => {
        const { dispatch, autosave, isEditPage } = this.props;

        // Validate current input before sending field data
        const required = ['artist', 'title', 'genre'];
        const errors = getBasicInvalidFields(info, { required });

        if (errors) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    errors: {
                        ...prevState.errors,
                        [info.id]: {
                            ...(prevState.errors[info.id] || {}),
                            ...errors
                        }
                    }
                };
            });

            return Promise.reject(errors);
        }

        return new Promise((resolve, reject) => {
            if (!autosave) {
                resolve();
                return;
            }

            clearTimeout(this._trackAutosaveTimer);
            this._trackAutosaveTimer = setTimeout(() => {
                // Delete the UPLOAD STATUS ENUM
                // eslint-disable-next-line no-unused-vars
                const { status, ...rest } = info;

                if (!this.state.isAutoSaving) {
                    this.setState({
                        isAutoSaving: true
                    });
                }

                // Update url slug of the album track only if this is a new upload
                if (
                    change &&
                    !change.error &&
                    change.name === 'title' &&
                    !isEditPage
                ) {
                    rest.url_slug = uploadSlugify(change.value);
                }

                dispatch(updateMetadata(musicId, rest))
                    .then(() => {
                        if (this.state.isAutoSaving) {
                            this.setState({
                                isAutoSaving: false
                            });
                        }
                        return resolve();
                    })
                    .catch((err) => {
                        let message = err.message;

                        if (err.errorcode === 1029) {
                            const invalidFields = Object.entries(err.errors)
                                .filter((field) => field[1].length)
                                .map((field) => field[0]);

                            const s = invalidFields.length > 1 ? 's' : '';

                            message = `${err.message} for ${invalidFields.join(
                                ', '
                            )} field${s}`;
                        }

                        this.props.dispatch(
                            addToast({
                                action: 'message',
                                item: `Sorry, could not update track ${trackNumber}: ${message}`
                            })
                        );
                        this.setErrorsFromUpdateRequest(musicId, err);
                        reject(err);
                    });
            }, 2000);
        });
    };

    handleAlbumFinishClick = (e) => {
        this.finishAlbum(e);
    };

    handleAlbumPreviewClick = (e) => {
        const previewOnly = true;

        return this.finishAlbum(e, previewOnly);
    };

    handleAlbumTrackReorder = (fromIndex, toIndex) => {
        const { musicEdit, dispatch } = this.props;
        const tracks = Array.from(musicEdit.info.tracks);
        const fromTrack = tracks.splice(fromIndex, 1)[0];

        tracks.splice(toIndex, 0, fromTrack);

        dispatch(updateItemInfo('tracks', tracks));

        this.setState({
            dirty: true,
            needsReordering: true
        });

        // Actual reordering on server happens when previewing/finishing the album
    };

    handleUploadCancelClick = (e) => {
        const id = e.currentTarget.getAttribute('data-upload-id');

        const { dispatch } = this.props;

        dispatch(cancelUpload(id));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getTabIndexForErrors(errors, activeTabIndex) {
        const errorKeys = Object.keys(errors);
        const onlyUrlSlugError =
            errorKeys.length === 1 && errorKeys[0] === 'url_slug';
        const urlSlugTab = 2;
        const otherErrorTab = 0;

        // Don't jump around if we're already on a tab with an error
        if (activeTabIndex === urlSlugTab && errorKeys.includes('url_slug')) {
            return activeTabIndex;
        }

        if (activeTabIndex === otherErrorTab && !onlyUrlSlugError) {
            return activeTabIndex;
        }

        return onlyUrlSlugError ? 2 : 0;
    }

    setErrorsFromUpdateRequest(musicId, error) {
        let invalidFields = [];
        // This can happen when trying to edit an AMP track
        if (Array.isArray(error.errors) && error.errorcode === 3039) {
            this.props.dispatch(
                addToast({
                    action: 'message',
                    item: `${error.message}: ${error.errors.join(', ')}`
                })
            );
            return;
        }

        if (error.errors) {
            invalidFields = getErroredKeysFromUpdateRequest(error.errors);
        }

        this.setState((prevState) => {
            return {
                ...prevState,
                errors: {
                    ...prevState.errors,
                    [musicId]: invalidFields.reduce((obj, field) => {
                        obj[field] = true;
                        return obj;
                    }, prevState.errors[musicId] || {})
                }
            };
        });
    }

    fetchTags() {
        const { dispatch, musicEdit, tags } = this.props;
        const musicItem = musicEdit.info;

        // check tags.ui.geo instead of tags.options.geo because root-level (country) tags are not valid options
        if (!tags.ui.geo.length) {
            dispatch(getGlobalGeoTags());
        }

        dispatch(getMusicTags(musicItem));

        musicItem.tracks.map((track) => {
            track.id = track.song_id;
            dispatch(getMusicTags(track));
        });
    }

    itemIsSaving(musicEdit) {
        if (!musicEdit.info) {
            return false;
        }

        if (musicEdit.isSaving) {
            return true;
        }

        if (musicEdit.info.tracks && musicEdit.info.tracks.length) {
            return musicEdit.info.tracks.some((track) => track.isSaving);
        }

        return false;
    }

    confirmLeave(nextLocation) {
        const { history, dispatch } = this.props;

        if (this._unblock) {
            this._unblock();
            this._unblock = null;
        }

        dispatch(hideModal());
        history.push(nextLocation);
    }

    finishAlbum(e, previewOnly = false) {
        const { dispatch, currentUser, musicEdit, autosave } = this.props;
        const errors = getBasicInvalidFields(musicEdit.info);

        if (errors) {
            const tabIndex = this.getTabIndexForErrors(
                errors,
                this.state.activeTabIndex
            );

            this.setState((prevState) => {
                return {
                    errors: {
                        ...prevState.errors,
                        [musicEdit.info.id]: {
                            ...(prevState.errors[musicEdit.info.id] || {}),
                            ...errors
                        }
                    },
                    // All required fields are on tab 0 as of now
                    activeTabIndex: tabIndex,
                    isPreviewing: false
                };
            });

            if (e) {
                e.preventDefault();
            }
            return false;
        }

        if (autosave && previewOnly) {
            if (errors) {
                return false;
            }

            return true;
        }

        const metadata = {
            ...musicEdit.info,
            status: 'complete'
        };

        if (previewOnly) {
            this.setState({
                isPreviewing: true
            });

            delete metadata.status;
        } else {
            this.setState({
                isFinishing: true
            });
        }

        const unsavedTracks = musicEdit.info.tracks
            .filter((track) => {
                return (
                    typeof track.saved === 'undefined' || track.saved === false
                );
            })
            .map((track) => {
                return dispatch(
                    updateMetadata(track.song_id, {
                        ...track,
                        genre: track.genre || musicEdit.info.genre,
                        status: previewOnly ? undefined : 'complete'
                    })
                ).catch((err) => {
                    // Only catch the error in order to update the error state
                    // but we still want to cause the save promise to fail.
                    this.setErrorsFromUpdateRequest(track.song_id, err);
                    throw err;
                });
            });

        Promise.all([
            dispatch(updateMetadata(musicEdit.info.id, metadata)).catch(
                (err) => {
                    // Only catch the error in order to update the error state
                    // but we still want to cause the save promise to fail.
                    this.setErrorsFromUpdateRequest(musicEdit.info.id, err);
                    throw err;
                }
            ),
            this.state.needsReordering
                ? dispatch(
                      reorderAlbumTracks(
                          musicEdit.info.id,
                          musicEdit.info.tracks
                      )
                  )
                : null,
            ...unsavedTracks
        ])
            .then((values) => {
                const albumMetadataAction = values[0];
                const slug = albumMetadataAction.resolved.url_slug;
                const redirectUrl = `/album/${
                    albumMetadataAction.resolved.uploader.url_slug
                }/${slug}`;

                if (previewOnly) {
                    this.setState({
                        isPreviewing: false
                    });

                    return null;
                }

                if (!this.props.isEditPage) {
                    const { mixpanel } = window;

                    // @todo move this tracking/api stuff to redux middleware
                    // for the UPDATE_METADATA event
                    // https://github.com/audiomack/audiomack-js/issues/843
                    if (typeof mixpanel !== 'undefined') {
                        const userId = currentUser.profile.id;
                        const signupDate = new Date(
                            currentUser.profile.created * 1000
                        ).toISOString();
                        const firstUploadDate = new Date(
                            currentUser.profile.first_upload_date * 1000
                        ).toISOString();
                        const username = currentUser.profile.url_slug;
                        const uploadCount =
                            currentUser.profile.upload_count_excluding_reups;

                        mixpanel.opt_in_tracking();
                        mixpanel.register({
                            'User ID': userId
                        });
                        mixpanel.people.set({
                            // These are user properties within the Create Account event
                            'User ID': userId,
                            Username: username,
                            // 'Authentication Type': 'Facebook',
                            'Sign Up Date': signupDate,
                            'First Upload Date': firstUploadDate,
                            // These are user properties within the upload event
                            '# of Songs Uploaded': uploadCount
                        });
                        mixpanel.identify(userId);

                        const trackObj = {
                            'Upload Name': albumMetadataAction.resolved.title,
                            '# of Album Tracks': musicEdit.info.tracks.length,
                            'Music ID': albumMetadataAction.resolved.id,
                            'Artist Name': getArtistName(
                                albumMetadataAction.resolved
                            ),
                            'Upload Type': 'Album',
                            Genre: albumMetadataAction.resolved.genre,
                            Tags: albumMetadataAction.resolved.usertags
                        };

                        mixpanel.track(eventCategory.upload, trackObj);
                    }
                }

                if (this._unblock) {
                    this._unblock();
                }

                this.setState({
                    isFinishing: false,
                    needsReordering: false,
                    dirty: false
                });

                return this.props.onAlbumSave(
                    redirectUrl,
                    this.props.fromMobile
                );
            })
            .catch((err) => {
                console.error(err);

                const ERROR_MUSIC_AMP_FORBIDDEN = 3039;
                let errorMessage = ['There was an error saving your album.'];

                if (err.errorcode === ERROR_MUSIC_AMP_FORBIDDEN) {
                    errorMessage = [
                        `Please contact ${
                            err.errors[0]
                        } to change monetization fields or contact contentops@audiomack.com.`
                    ];
                } else {
                    const albumLevelErrors = Object.keys(
                        this.state.errors[musicEdit.info.id] || {}
                    ).filter(
                        (key) => !!this.state.errors[musicEdit.info.id][key]
                    );

                    // Only album level errors if appropriate. This is to prevent
                    // multiple track level errors in case the genre isnt set for
                    // each track which, will be set automatically once the
                    // album level genre is set. This lets us prevent
                    // overwhelming the user with multiple genre errors
                    // for each track
                    if (albumLevelErrors.length) {
                        errorMessage[0] += ` Please check the following fields: ${albumLevelErrors.join(
                            ', '
                        )}.`;
                    } else {
                        const trackErrors = musicEdit.info.tracks.reduce(
                            (acc, track, i) => {
                                const errs = Object.keys(
                                    this.state.errors[track.song_id] || {}
                                ).filter(
                                    (key) =>
                                        !!this.state.errors[track.song_id][key]
                                );

                                if (errs.length) {
                                    let trackMessage = `Track ${i + 1}`;

                                    if (track.title) {
                                        trackMessage = `${trackMessage} (${
                                            track.title
                                        })`;
                                    }

                                    trackMessage += `: ${errs.join(', ')}`;

                                    acc.push(trackMessage);
                                    acc.push(<br />);
                                }

                                return acc;
                            },
                            []
                        );
                        errorMessage.push(
                            ' Please check the following track errors:'
                        );
                        errorMessage.push(<br />, <br />);
                        errorMessage = errorMessage.concat(trackErrors);
                    }
                }

                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        title: 'Error',
                        message: errorMessage
                    })
                );

                this.setState({
                    isFinishing: false,
                    isPreviewing: false
                });
            });

        return true;
    }

    deleteAlbum(musicId) {
        const { dispatch, currentUser } = this.props;

        dispatch(hideModal());

        this.setState({
            isPreviewing: false,
            activeTabIndex: 0,
            dirty: false,
            errors: {}
        });

        dispatch(deleteItem(musicId))
            .then(() => {
                return this.props.onAlbumDelete(musicId);
            })
            .catch((err) => {
                let errorMsg = 'There was a problem deleting this music item.';

                if (currentUser.isAdmin) {
                    errorMsg = `There was a problem deleting this music item. ${
                        err.message
                    }`;
                }

                dispatch(
                    addToast({
                        action: 'message',
                        item: errorMsg
                    })
                );

                console.log(err);
            });
    }

    deleteAlbumTrack(albumId, musicId) {
        const { dispatch } = this.props;

        dispatch(hideModal());
        dispatch(deleteAlbumTrackItem(albumId, musicId));
    }

    processPreExistingTrackQueue() {
        const { dispatch, musicEdit, uploadAlbum } = this.props;
        const { id: albumId } = musicEdit.info;

        if (
            !uploadAlbum.preExistingTrackQueue.length ||
            this._processingPreExistingTrackQueue
        ) {
            return;
        }

        this._processingPreExistingTrackQueue = true;

        console.warn(
            'Processing pre-existing tracks',
            uploadAlbum.preExistingTrackQueue
        );

        const actionQueue = uploadAlbum.preExistingTrackQueue.map((track) => {
            const { id: trackId } = track;
            const action = addPreExistingAlbumTrack(albumId, trackId);
            return dispatch(action);
        });

        Promise.all(actionQueue)
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                // this variable should be set to false even if addPreExistingAlbumTrack fails for some tracks
                this._processingPreExistingTrackQueue = false;
                console.warn('Processed pre-existing tracks');
            });
    }

    replaceAudioSuccess = (filename) => {
        const { dispatch } = this.props;

        dispatch(
            addToast({
                action: 'message',
                item: `Successfully replaced audio file with ${filename}`
            })
        );
    };

    render() {
        return (
            <AlbumEditPage
                isEditPage={this.props.isEditPage}
                autosave={this.props.autosave}
                currentUser={this.props.currentUser}
                currentUploadStep={this.props.currentUploadStep}
                previousUploadStep={this.props.previousUploadStep}
                upload={this.props.upload}
                uploadAlbum={this.props.uploadAlbum}
                preExistingTrackQueue={this.props.preExistingTrackQueue}
                isAutoSaving={this.state.isAutoSaving}
                isPreviewing={this.state.isPreviewing}
                isFinishing={this.state.isFinishing}
                dirty={this.state.dirty}
                activeTabIndex={this.state.activeTabIndex}
                onTabClick={this.handleTabClick}
                musicEdit={this.props.musicEdit}
                sidebarButtons={this.props.sidebarButtons}
                finishButtonText={this.props.finishButtonText}
                finishingButtonText={this.props.finishingButtonText}
                errors={this.state.errors}
                replaceAudioSuccess={this.replaceAudioSuccess}
                onAlbumDeleteClick={this.handleAlbumDeleteClick}
                onAlbumInfoInput={this.handleAlbumInfoInput}
                onAlbumPreviewClick={this.handleAlbumPreviewClick}
                onAlbumFinishClick={this.handleAlbumFinishClick}
                onAlbumTrackReorder={this.handleAlbumTrackReorder}
                onAlbumTrackSaveClick={this.handleAlbumTrackSaveClick}
                onAlbumTrackDeleteClick={this.handleAlbumTrackDeleteClick}
                onAlbumTrackReplaceInputChange={
                    this.handleTrackReplaceInputChange
                }
                onUploadCancelClick={this.handleUploadCancelClick}
                onPreExistingCancelClick={this.handlePreExistingCancelClick}
                fromMobile={this.props.fromMobile}
                AlbumDetails={this.props.AlbumDetails}
                SongUploads={this.props.SongUploads}
                onDropdownChange={this.handleDropdownChange}
                onSuggestTagClick={this.props.onSuggestTagClick}
                tags={this.props.tags}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        musicEdit: state.musicEdit,
        currentUser: state.currentUser,
        tags: state.musicTags,
        upload: state.upload,
        uploadAlbum: state.uploadAlbum
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(AlbumEditPageContainer);
