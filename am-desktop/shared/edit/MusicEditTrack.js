import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import {
    isCurrentMusicItem,
    getArtistName,
    buildDynamicImage
} from 'utils/index';

import RemoveIcon from '../icons/remove';
import DownArrowIcon from '../icons/chevron-down';
import PlusIcon from '../../../am-shared/icons/plus-alt';
import TrashIcon from '../../../am-shared/icons/trash-alt';
import AlbumIcon from '../icons/albums';

import PlayButton from '../components/PlayButton';

import styles from './MusicEditTrack.module.scss';

export default class MusicEditTrack extends Component {
    static propTypes = {
        track: PropTypes.object,
        index: PropTypes.number,
        player: PropTypes.object,
        currentUserPinned: PropTypes.object,
        style: PropTypes.object,
        onPinClick: PropTypes.func,
        onRemoveClick: PropTypes.func,
        onExpandClick: PropTypes.func,
        onSelect: PropTypes.func,
        shouldHideImage: PropTypes.bool,
        hasExpandableTracks: PropTypes.bool,
        isExpanded: PropTypes.bool,
        isSelectable: PropTypes.bool,
        selectedItems: PropTypes.array,
        isPinnable: PropTypes.bool,
        isPinned: PropTypes.bool,
        onPlayClick: PropTypes.func,
        showAlbumIndicator: PropTypes.bool,
        titleLink: PropTypes.bool
    };

    static defaultProps = {
        isSelectable: false,
        isPinnable: false,
        selectedItems: [],
        hasExpandableTracks: false
    };

    handleCheckboxClick = (e) => {
        this.props.onSelect(this.props.track, e);
    };

    renderExpandButton() {
        const { track, hasExpandableTracks, isExpanded } = this.props;

        const isTrackItem =
            hasExpandableTracks && track.tracks && track.tracks.length;

        if (isTrackItem) {
            const dropdownClass = classnames(styles.dropdownButton, {
                [styles.expanded]: isExpanded
            });

            return (
                <button
                    aria-label={`Expand track list for ${track.title} by ${
                        track.artist
                    }`}
                    className={dropdownClass}
                    onClick={this.props.onExpandClick}
                >
                    <DownArrowIcon />
                </button>
            );
        }

        return null;
    }

    renderSelectButton() {
        const {
            isSelectable,
            selectedItems,
            track,
            hasExpandableTracks
        } = this.props;
        const selectedIds = selectedItems.map((t) => t.id);
        const isSelected = selectedIds.includes(track.id);
        const isTrackItem = hasExpandableTracks && track.tracks;

        if (!isSelectable || isTrackItem) {
            return null;
        }

        const klass = classnames(styles.selectButton);

        return (
            <input
                type="checkbox"
                className={klass}
                data-testid="selectPreviousUpload"
                aria-label={`Select ${track.title} by ${track.artist}`}
                checked={isSelected}
                onChange={this.handleCheckboxClick}
            />
        );
    }

    renderRemoveButton() {
        const { track, onRemoveClick } = this.props;

        if (typeof onRemoveClick !== 'function') {
            return null;
        }

        return (
            <button
                className={styles.remove}
                title="Remove"
                aria-label="Remove"
                data-music-id={track.id}
                onClick={onRemoveClick}
            >
                <RemoveIcon />
            </button>
        );
    }

    renderPinButton() {
        const { track, isPinned, isPinnable, currentUserPinned } = this.props;

        if (!isPinnable && !isPinned) {
            return null;
        }

        const alreadyPinned = currentUserPinned.list.find(
            (pin) => pin.id === track.id
        );
        const klass = classnames(styles.pin, {
            [styles.pinned]: isPinned || alreadyPinned
        });

        if (isPinned || alreadyPinned) {
            return (
                <button
                    className={klass}
                    title="Unpin"
                    aria-label="Unpin"
                    data-action="unpin"
                    onClick={this.props.onPinClick}
                >
                    <TrashIcon />
                </button>
            );
        }

        return (
            <button
                className={klass}
                title="Pin"
                aria-label="Pin"
                data-action="pin"
                onClick={this.props.onPinClick}
            >
                <PlusIcon />
            </button>
        );
    }

    render() {
        const {
            track,
            player,
            hasExpandableTracks,
            index,
            isExpanded,
            onPlayClick,
            shouldHideImage
        } = this.props;
        const isCurrentSong = isCurrentMusicItem(player.currentSong, track);
        const isPaused = !isCurrentSong || (isCurrentSong && player.paused);
        const isLoading = isCurrentSong && player.loading;
        const buttonProps = {
            'data-track-index': index
        };
        const artist = getArtistName(track);
        const imageSize = 30;

        let imageEl;

        if (!shouldHideImage && track.image) {
            const image = buildDynamicImage(track.image, {
                width: imageSize,
                height: imageSize,
                max: true
            });
            const image2x = buildDynamicImage(track.image, {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            });

            imageEl = (
                <div className={styles.image}>
                    <img src={image} srcSet={`${image2x} 2x`} alt="" />
                    <PlayButton
                        className={styles.play}
                        size={10}
                        loading={isLoading}
                        paused={isPaused}
                        onButtonClick={onPlayClick}
                        buttonProps={buttonProps}
                        shadow
                    />
                </div>
            );
        }

        let expandedTracks;

        if (this.props.isExpanded && track.tracks && track.tracks.length) {
            expandedTracks = track.tracks.map((musicTrack, i) => {
                const newTrack = {
                    ...musicTrack,
                    image: musicTrack.image || track.image,
                    id: musicTrack.song_id
                };

                return (
                    <MusicEditTrack
                        key={i}
                        track={newTrack}
                        index={this.props.index}
                        player={this.props.player}
                        currentUserPinned={this.props.currentUserPinned}
                        onPinClick={this.props.onPinClick}
                        onRemoveClick={this.props.onRemoveClick}
                        onExpandClick={this.props.onExpandClick}
                        onSelect={this.props.onSelect}
                        isExpanded={this.props.isExpanded}
                        isSelectable={this.props.isSelectable}
                        selectedItems={this.props.selectedItems}
                        isPinnable={this.props.isPinnable}
                        isPinned={this.props.isPinned}
                        onPlayClick={this.props.onPlayClick}
                        shouldHideImage
                    />
                );
            });
        }

        const className = classnames(styles.container, {
            [styles.expanded]: hasExpandableTracks && isExpanded
        });

        const titleClassName = classnames(styles.title, 'u-text-dark');

        const albumIndicator =
            this.props.showAlbumIndicator && track.type === 'album' ? (
                <span
                    className="u-text-orange u-text-icon u-d-inline-block"
                    data-tooltip="Album"
                >
                    <AlbumIcon />
                </span>
            ) : null;

        console.log(track);
        const titleElement = this.props.titleLink ? (
            <Link
                className="music-detail__link"
                to={`/${track.type}/${track.uploader.url_slug}/${
                    track.url_slug
                }`}
                target="_blank"
            >
                <span className={titleClassName}>
                    {track.title} {albumIndicator}
                </span>
            </Link>
        ) : (
            <span className={titleClassName}>
                {track.title} {albumIndicator}
            </span>
        );

        return (
            <Fragment>
                <div className={className} style={this.props.style}>
                    {this.renderSelectButton()}
                    {imageEl}
                    <div className={styles.text}>
                        {titleElement}
                        <span className={styles.artist}>{artist}</span>
                    </div>
                    {this.renderRemoveButton()}
                    {this.renderPinButton()}
                    {this.renderExpandButton()}
                </div>
                {expandedTracks}
            </Fragment>
        );
    }
}
