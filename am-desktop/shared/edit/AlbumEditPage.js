import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';

import analytics from 'utils/analytics';
import { previewFile, throttle } from 'utils/index';

import ArrowsUpDownIcon from '../icons/arrows-up-down';
import DragHandle from '../icons/hamburger-small';
import TrashIcon from 'icons/trash-solid';

import AddSingleScreen from '../upload/AddSingleScreen';
import DraggableList from '../list/DraggableList';
import LabeledInput, {
    inputTypes as labeledInputTypes
} from '../components/LabeledInput';
import UploadedAlbumTrack from '../upload/UploadedAlbumTrack';
import ReleaseOptions from '../components/ReleaseOptions';

import styles from './AlbumEditPage.module.scss';

const albumDescriptionMaxChars = 1200;
const adminDescriptionMaxChars = 1500;

export default class AlbumEditPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        musicEdit: PropTypes.object.isRequired,
        currentUploadStep: PropTypes.object,
        previousUploadStep: PropTypes.object,
        finishButtonText: PropTypes.string,
        finishingButtonText: PropTypes.string,
        upload: PropTypes.object,
        uploadAlbum: PropTypes.object,
        preExistingTrackQueue: PropTypes.array,
        errors: PropTypes.object,
        autosave: PropTypes.bool,
        dirty: PropTypes.bool,
        isEditPage: PropTypes.bool,
        isFinishing: PropTypes.bool,
        isAutoSaving: PropTypes.bool,
        isPreviewing: PropTypes.bool,
        activeTabIndex: PropTypes.number,
        replaceAudioSuccess: PropTypes.func,
        sidebarButtons: PropTypes.array,
        onTabClick: PropTypes.func,
        onAlbumTrackReorder: PropTypes.func,
        onAlbumInfoInput: PropTypes.func,
        onAlbumPreviewClick: PropTypes.func,
        onAlbumFinishClick: PropTypes.func,
        onAlbumTrackSaveClick: PropTypes.func,
        onAlbumTrackDeleteClick: PropTypes.func,
        onAlbumTrackReplaceInputChange: PropTypes.func,
        onUploadCancelClick: PropTypes.func,
        onPreExistingCancelClick: PropTypes.func,
        onAlbumDeleteClick: PropTypes.func,
        AlbumDetails: PropTypes.element,
        SongUploads: PropTypes.element,
        onDropdownChange: PropTypes.func,
        onSuggestTagClick: PropTypes.func,
        tags: PropTypes.object
    };

    static defaultProps = {
        sidebarButtons: ['preview', 'finish', 'start-over'],
        finishButtonText: 'Finish',
        finishingButtonText: 'Finishing'
    };

    constructor(props) {
        super(props);

        this.state = {
            description: props.musicEdit.info.description || '',
            descriptionCharsLeft: albumDescriptionMaxChars,
            adminDescriptionCharsLeft: adminDescriptionMaxChars,
            selectedReleaseOption: '1',
            futureReleaseDate:
                moment(props.musicEdit.info.released * 1000).diff(moment()) > 0,
            releaseDate: null
        };
        this._tagListAdditionalHeights = {};
        this._trackStates = {};
        this._tracks = {};
    }

    componentDidUpdate(prevProps) {
        const prevInfo = prevProps.musicEdit.info;
        const currentInfo = this.props.musicEdit.info;
        const currentTracks = currentInfo.tracks || [];
        const prevTracks = prevInfo.tracks || [];

        if (currentTracks.length !== prevTracks.length && this.DraggableList) {
            // Force a resize because when uploading an album,
            // the metadata section at the top messes with the
            // height state within the draggable list and items
            // get cut off too early.
            this.DraggableList.updateWindow();
        }

        if (this.props.upload.inProgress === true) {
            this.updateDraggableList();
        }

        // Checking isSaving change to avoid the more expensive JSON.stringify
        // call that only needs to really be checked after a save
        if (
            currentInfo.genre !== prevInfo.genre ||
            (!currentInfo.isSaving &&
                prevInfo.isSaving &&
                JSON.stringify(this.props.errors) !==
                    Object.keys(prevProps.errors))
        ) {
            this.updateDraggableList();
        }
    }

    componentWillUnmount() {
        this._tagListAdditionalHeights = null;
        this._trackStates = null;
        this._tracks = null;
        this.DraggableList = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleDropdownChange = (textValue, e) => {
        const { onDropdownChange } = this.props;

        onDropdownChange(textValue, e).finally(() =>
            this.updateDraggableList()
        );
    };

    handleFileInputChange = (e) => {
        const { name } = e.currentTarget;

        previewFile(e.target.files[0])
            .then((value) => {
                return this.props.onAlbumInfoInput({
                    currentTarget: {
                        name,
                        value
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            });
    };

    handleDateChange = (date) => {
        const isMomentDate = moment.isMoment(date);

        let newDate = date;

        if (!isMomentDate) {
            const { releaseDate } = this.state;
            const { musicEdit } = this.props;
            const currentDate =
                releaseDate || moment(musicEdit.info.released * 1000);
            const isPM = currentDate.hours() >= 12;
            const { name, value } = date.target;

            if (name === 'hour') {
                const hourToSet = parseInt(value, 10) + (isPM ? 12 : 0);

                newDate = currentDate.hour(hourToSet);
            } else if (name === 'minute') {
                newDate = currentDate.minute(parseInt(value, 10));
            } else if (name === 'ampm') {
                const currentHour = currentDate.hours();
                const am = -12;
                const pm = 12;
                const newHour = currentHour + (value === 'am' ? am : pm);

                newDate = currentDate.hour(newHour);
            }
        }

        this.setState({
            futureReleaseDate: newDate.diff(moment()) > 0,
            releaseDate: newDate
        });
    };

    handleDateSubmit = (date) => {
        if (date) {
            this.handleDateChange(date);
        }

        this.props.onAlbumInfoInput({
            currentTarget: {
                name: 'released',
                value: this.state.releaseDate.unix()
            }
        });
    };

    handleReleaseOptionChange = (e) => {
        const { onAlbumInfoInput } = this.props;
        const value = e.target.value;

        this.setState({
            dirty: true,
            selectedReleaseOption: value
        });

        const currentDate = moment();

        switch (value) {
            // option 1, release date set to now, set private=no
            case '1':
                onAlbumInfoInput({
                    currentTarget: {
                        name: 'released',
                        value: currentDate.unix()
                    }
                });

                onAlbumInfoInput({
                    currentTarget: {
                        name: 'private',
                        value: 'no'
                    }
                });

                break;

            // option 2, set release date now, but send private=yes
            case '2':
                onAlbumInfoInput({
                    currentTarget: {
                        name: 'released',
                        value: currentDate.unix()
                    }
                });

                onAlbumInfoInput({
                    currentTarget: {
                        name: 'private',
                        value: 'yes'
                    }
                });

                break;

            // option 3, open calendar and get custom date, set private=no
            case '3':
                onAlbumInfoInput({
                    currentTarget: {
                        name: 'private',
                        value: 'no'
                    }
                });

                break;

            default:
                break;
        }
    };

    handleAlbumDescriptionChange = (e) => {
        const key = e.target.name;
        const charLeft =
            key === 'description'
                ? 'descriptionCharsLeft'
                : 'adminDescriptionCharsLeft';

        this.setState({
            // Using local state for description because
            // on the upload album page, the cursor will jump to the
            // end of the textarea on every change for some reason
            [key]: e.target.value,
            [charLeft]: albumDescriptionMaxChars - e.target.value.length
        });

        this.props.onAlbumInfoInput(e);
    };

    handleTagListResize = (inputId, inputName, height) => {
        // extract songId from inputId formatted as "inputName-songId"
        const songId = inputId.split('-')[1];

        if (!this._tagListAdditionalHeights[songId]) {
            this._tagListAdditionalHeights[songId] = {};
        }

        this._tagListAdditionalHeights[songId][inputName] = height;

        if (this.DraggableList !== null) {
            this.DraggableList.recomputeRowHeights();
        }
    };

    handleTrackStateChange = (state, track) => {
        const id = track.song_id;

        // We need to keep the state because when reordering,
        // the component gets remounted
        this._trackStates[id] = state;

        if (this.DraggableList) {
            this.DraggableList.recomputeRowHeights();
        }
    };

    // Since the state of the album track stays local to avoid this bug:
    // https://github.com/audiomack/audiomack-js/issues/1074 we need to
    // save the current state of the uploaded album tracks in case the
    // user is editing tracks while uploads are still going. Reason being,
    // after an upload is finished, we have to force update the
    // DraggableList which remounts the draggable list items, causing
    // edited items to look reverted back to when the draggable list was
    // created, even though they might already be saved to the server
    //
    // Putting hack in this function name as we need to decide in the
    // future whether or not to use a different draggable list lib
    // or design around this limitation. Things this list component provides:
    //
    // - Drag and drop functionality (obviously)
    // - Infinite items in the list (perfect for the queue)
    // - Scrolling behavior while you drag
    //
    // As mentioned in the long note within DraggableList, this component
    // does not fair well when having editable fields within the item
    // because the DraggableList component itself trys to avoid re-renders
    // at all costs for optimization purposes.
    handlePropagateTrackHack = (track) => {
        this._tracks[track.song_id] = track;
    };

    handleRowHeight = (track) => {
        const { musicEdit } = this.props;
        const itemHeight = 45;
        const padding = 0;
        const id = track.props.uploadItem.song_id;

        if (this._trackStates[id] && this._trackStates[id].open) {
            let tagListAdditionalHeight = 0;

            if (this._tagListAdditionalHeights[id]) {
                tagListAdditionalHeight = Math.max(
                    ...Object.values(this._tagListAdditionalHeights[id])
                );
            }

            const drawerHeight =
                (musicEdit.info.amp ? 780 : 700) + tagListAdditionalHeight;

            return itemHeight + padding + drawerHeight;
        }

        return itemHeight + padding;
    };

    handleAlbumTrackReorder = (fromIndex, toIndex) => {
        this.props.onAlbumTrackReorder(fromIndex, toIndex);

        this.DraggableList.recomputeRowHeights();
    };

    handleAlbumTrackSave = (id, item, change, trackNumber) => {
        return this.props
            .onAlbumTrackSaveClick(id, item, change, trackNumber)
            .then(() => {
                this.updateDraggableList();
                return;
            })
            .catch((err) => {
                this.updateDraggableList();
                // Event on an error, we want to re-render the DraggableList
                // but we also want to propagate the error to the caller which
                // is why we cant refactor this with .finally().
                throw err;
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getCachedTrackState = (trackId) => {
        return (this._trackStates || {})[trackId] || {};
    };

    updateDraggableList = throttle(() => {
        // The DraggableList component doesnt really update
        // unless scrolled or dragged. I'm sure there's a better
        // way around this but this'll do for now.

        if (this.DraggableList) {
            this.DraggableList.forceUpdateGrid();
        }
    }, 40);

    renderEditReorder() {
        const {
            musicEdit,
            currentUser,
            upload,
            autosave,
            replaceAudioSuccess,
            onAlbumDeleteClick
        } = this.props;

        const albumTrackList = (musicEdit.info.tracks || [])
            .filter((track) => {
                return !track.isDeleting;
            })
            .map((track, i) => {
                const replaceAlbumTrackUploadItem = Array.from(upload.list)
                    .reverse()
                    .find((obj) => obj.replaceId === track.song_id);

                return (
                    <UploadedAlbumTrack
                        getCachedTrackState={this.getCachedTrackState}
                        autosave={autosave}
                        key={`${i}:${track.song_id}`}
                        albumInfo={musicEdit.info}
                        saveRequestErrors={
                            this.props.errors[track.song_id] || {}
                        }
                        currentUser={currentUser}
                        uploadItem={{
                            ...track,
                            ...(this._tracks[track.song_id] || {})
                        }}
                        onPropagateTrackHack={this.handlePropagateTrackHack}
                        replaceAudioSuccess={replaceAudioSuccess}
                        replaceUploadItem={replaceAlbumTrackUploadItem}
                        onStateChange={this.handleTrackStateChange}
                        onSaveClick={this.handleAlbumTrackSave}
                        onTagListResize={this.handleTagListResize}
                        onDeleteClick={this.props.onAlbumTrackDeleteClick}
                        onAlbumTrackReplaceInputChange={
                            this.props.onAlbumTrackReplaceInputChange
                        }
                        trackNumber={i + 1}
                        uploadPage
                        onDropdownChange={this.handleDropdownChange}
                        onSuggestTagClick={this.props.onSuggestTagClick}
                        tags={this.props.tags}
                    />
                );
            });

        return (
            <main className={classnames(styles.content, styles.editReorder)}>
                <h2 className={styles.trackListHeading}>Album Track List</h2>

                <div className={styles.topRight}>
                    <Link
                        to={`/album/${currentUser.profile.url_slug}/${
                            musicEdit.info.url_slug
                        }?preview=true`}
                        target="_blank"
                        className={styles.previewAlbum}
                        disabled={false}
                        type="button"
                    >
                        Preview album
                    </Link>

                    <div className={styles.trashIconWrapper}>
                        <button
                            className={styles.trashIcon}
                            data-modal-title="Delete album"
                            data-testid="deleteAlbumUpload"
                            data-tooltip="Delete Album"
                            data-upload-id="cancelUpload"
                            data-tooltip-bottom-right
                            onClick={onAlbumDeleteClick}
                            type="button"
                        >
                            <TrashIcon />
                        </button>
                    </div>
                </div>

                <ol className={styles.trackList}>
                    <li className={styles.trackListItemWrapper}>
                        <div
                            className={classnames(
                                styles.trackListLabel,
                                styles.trackListItem
                            )}
                        >
                            <span className={styles.reorder}>
                                <ArrowsUpDownIcon
                                    className={styles.arrowsIcon}
                                />
                            </span>
                            <span className={styles.numbers}>#</span>
                            <span className={styles.songName}>Song Name</span>
                            <span className={styles.actions}>Actions</span>
                        </div>
                    </li>

                    <DraggableList
                        containerProps={{
                            className: styles.draggableList
                        }}
                        onDragFinish={this.handleAlbumTrackReorder}
                        items={albumTrackList}
                        dragHandle={
                            <span className={styles.dragHandle}>
                                <DragHandle className={styles.hamburgerIcon} />
                            </span>
                        }
                        ref={(instance) => {
                            this.DraggableList = instance;
                        }}
                        draggingClass="song-upload--dragging"
                        onRowHeight={this.handleRowHeight}
                        useWindowAsScrollContainer
                    />
                </ol>
            </main>
        );
    }

    renderRelease() {
        const { musicEdit, currentUser, onAlbumDeleteClick } = this.props;

        const { releaseDate } = this.state;

        const currentDate =
            releaseDate || moment(musicEdit.info.released * 1000);
        const errors = this.props.errors[musicEdit.info.id] || {};

        return (
            <main className={styles.content}>
                <div className={styles.topRight}>
                    <Link
                        to={`/album/${currentUser.profile.url_slug}/${
                            musicEdit.info.url_slug
                        }?preview=true`}
                        target="_blank"
                        className={styles.previewAlbum}
                        disabled={false}
                        type="button"
                    >
                        Preview album
                    </Link>

                    <div className={styles.trashIconWrapper}>
                        <button
                            className={styles.trashIcon}
                            data-modal-title="Delete album"
                            data-testid="deleteAlbumUpload"
                            data-tooltip="Delete Album"
                            data-upload-id="cancelUpload"
                            data-tooltip-bottom-right
                            onClick={onAlbumDeleteClick}
                            type="button"
                        >
                            <TrashIcon />
                        </button>
                    </div>
                </div>

                <ReleaseOptions
                    currentDate={currentDate}
                    onDateBlur={this.handleDateSubmit}
                    onDateChange={this.handleDateChange}
                    onReleaseOptionChange={this.handleReleaseOptionChange}
                    selectedReleaseOption={this.state.selectedReleaseOption}
                    futureReleaseDate={this.state.futureReleaseDate}
                />

                <div className={styles.linkInputsWrapper}>
                    <LabeledInput
                        id="album-url_slug"
                        error={errors.url_slug}
                        label="Album URL"
                        name="url_slug"
                        onChange={this.props.onAlbumInfoInput}
                        prepend={`${process.env.AM_URL.split('//')[1]}/album/${
                            this.props.currentUser.profile.url_slug
                        }/`}
                        type={labeledInputTypes.slug}
                        value={musicEdit.info.url_slug || ''}
                    />

                    <LabeledInput
                        id="album-buy_link"
                        error={errors.buy_link}
                        label="Buy / Custom Link URL"
                        name="buy_link"
                        onChange={this.props.onAlbumInfoInput}
                        tooltip="You can enter a URL for a 3rd party site or service (iTunes, Spotify, HypeMachine, etc) here."
                        type={labeledInputTypes.url}
                        value={musicEdit.info.buy_link || ''}
                    />
                </div>
            </main>
        );
    }

    renderUploadStep(stepNumber) {
        const {
            currentUploadStep,
            previousUploadStep,
            uploadAlbum,
            AlbumDetails,
            SongUploads
        } = this.props;
        const { isAddingSingles } = uploadAlbum;

        const number = stepNumber || currentUploadStep.number;

        switch (number) {
            case 1:
                return AlbumDetails;

            case 2:
                return isAddingSingles ? <AddSingleScreen /> : SongUploads;

            case 3:
                return this.renderEditReorder();

            case 4:
                return this.renderRelease();

            case 5:
                return previousUploadStep.number === 5
                    ? this.renderUploadStep(1)
                    : this.renderUploadStep(previousUploadStep.number);

            default:
                return null;
        }
    }

    render() {
        const { currentUploadStep, isEditPage } = this.props;

        if (isEditPage) {
            return this.renderUploadStep();
        }

        if (currentUploadStep.number === 3) {
            return this.renderEditReorder();
        }

        if (currentUploadStep.number === 4 || currentUploadStep.number === 5) {
            return this.renderRelease();
        }

        return null;
    }
}
