import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import Helmet from 'react-helmet';

import {
    allGenresMap,
    podcastCategories,
    podcastLanguages
} from 'constants/index';
import { settingsLabelsMap } from 'constants/notifications/index';
import {
    SOCIAL_NETWORK_LINK_TWITTER,
    SOCIAL_NETWORK_LINK_INSTAGRAM
} from 'constants/user/social';
import { getPodcastUrl } from 'utils/index';

import Select from '../components/Select';
import Avatar from '../components/Avatar';
import Truncate from 'components/Truncate';
import FollowButtonContainer from '../components/FollowButtonContainer';
import FixedActionBarContainer from '../components/FixedActionBarContainer';
import SocialLinks from '../components/SocialLinks';
import FeedBar from '../widgets/FeedBar';
import EditableImageBanner from '../widgets/EditableImageBanner';
import DeleteAccountModal from '../modal/DeleteAccountModal';

import Verified from 'components/Verified';
import ToggleSwitch from 'components/ToggleSwitch';
import SocialLinkButton from 'buttons/SocialLinkButton';

import PasswordHide from '../icons/eye-hide';
import PasswordView from '../icons/eye-view';
import YouTubeIcon from '../icons/youtube';
import FacebookIcon from 'icons/facebook-letter-logo';

import styles from './ProfileEditPage.module.scss';

const profileBiographyMaxChars = 800;
const allTabs = ['basic', 'password', 'email', 'url', 'apps', 'notifications'];

export default class ProfileEditPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        oauth: PropTypes.object,
        initialTab: PropTypes.string,
        editableFieldValues: PropTypes.object,
        onRevokeApp: PropTypes.func,
        onUpdateUrlSlugInput: PropTypes.func,
        onUpdatePasswordInput: PropTypes.func,
        onUpdatePasswordFormSubmit: PropTypes.func,
        onUpdateUrlSlugFormSubmit: PropTypes.func,
        onUpdateNotifications: PropTypes.func,
        onDeleteAccountClick: PropTypes.func,
        onEditFieldInput: PropTypes.func,
        urlSlugInput: PropTypes.string,
        onUpdateEmailInput: PropTypes.func,
        onBannerEditApply: PropTypes.func,
        onBannerRemoveClick: PropTypes.func,
        onUpdateEmailFormSubmit: PropTypes.func,
        newEmail: PropTypes.object,
        onFormSubmit: PropTypes.func,
        onSocialLink: PropTypes.func,
        onSocialLinkError: PropTypes.func,
        isEditingBanner: PropTypes.bool,
        isSavingBanner: PropTypes.bool,
        onPasswordToggle: PropTypes.func,
        showPassword: PropTypes.bool,
        notificationSettings: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            tab: this.getTab(props.initialTab)
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTabClick = (value) => {
        this.setState({
            tab: value
        });
    };

    handleRssInputClick = (e) => {
        e.currentTarget.focus();
        e.currentTarget.select();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getTab(initialTab) {
        return allTabs.find((tab) => tab === initialTab) || allTabs[0];
    }

    getSubmitStateForTab(tab) {
        const { currentUser, isSavingBanner } = this.props;

        let text;

        switch (tab) {
            case 'password': {
                const { loading } = currentUser.updatePassword;

                text = 'Change Password';

                if (loading) {
                    text = 'Updating…';
                }

                return {
                    submitButtonText: text,
                    submitFormLoading: loading
                };
            }

            case 'email': {
                const { loading } = currentUser.updateEmail;

                text = 'Update Email';

                if (loading) {
                    text = 'Updating…';
                }

                return {
                    submitButtonText: text,
                    submitFormLoading: loading
                };
            }

            case 'url': {
                const { loading } = currentUser.updateSlug;

                text = 'Update Artist URL';

                if (loading) {
                    text = 'Updating…';
                }

                return {
                    submitButtonText: text,
                    submitFormLoading: loading
                };
            }

            default: {
                const { loading } = currentUser;

                text = 'Update Profile';

                if (loading && !isSavingBanner) {
                    text = 'Saving…';
                }

                return {
                    submitButtonText: text,
                    submitFormLoading: loading
                };
            }
        }
    }

    getSubmitFunctionForTab(tab) {
        switch (tab) {
            case 'basic':
                return this.props.onFormSubmit;

            case 'password':
                return this.props.onUpdatePasswordFormSubmit;

            case 'email':
                return this.props.onUpdateEmailFormSubmit;

            case 'url':
                return this.props.onUpdateUrlSlugFormSubmit;

            case 'notifications': // Settings are saved as they are clicked. But people might feel more comfortable with an actual button to press
                return this.props.onFormSubmit;

            default:
                return null;
        }
    }

    getErrorMessagesObject(apiErrorsObj = {}) {
        return Object.keys(apiErrorsObj).reduce((acc, key) => {
            const obj = apiErrorsObj[key];
            const keys = Object.keys(obj);

            acc[key] = {
                description: obj[keys[0]]
            };

            return acc;
        }, {});
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="auth__error">{error.description}</p>;
    }

    renderBannerForm(banner) {
        const {
            currentUser,
            isSavingBanner,
            isEditingBanner,
            onBannerRemoveClick,
            onBannerEditApply
        } = this.props;
        const profile = currentUser.profile || {};

        return (
            <EditableImageBanner
                banner={banner}
                isEditing={isEditingBanner}
                isSaving={isSavingBanner}
                onRemove={onBannerRemoveClick}
                onApply={onBannerEditApply}
                closeLink={`/artist/${profile.url_slug}`}
            />
        );
    }

    renderProfileSocial() {
        const { currentUser } = this.props;
        const profile = currentUser.profile || {};

        return (
            <SocialLinks
                links={profile}
                className="u-inline-list u-list-reset user-profile__social-icons"
                allowNonVerified
            />
        );
    }

    renderProfileInfo() {
        const { currentUser } = this.props;

        const profile = currentUser.profile || {};

        const check = <Verified user={profile} size={19} />;

        return (
            <div>
                <h2 className="user-profile__name">
                    {profile.name} {check}
                </h2>
                <Truncate
                    className="user-profile__bio"
                    lines={1}
                    ellipsis={'…'}
                    text={profile.bio}
                />
                {this.renderProfileSocial()}
            </div>
        );
    }

    renderSocialButton(network) {
        return (
            <div className="column small-24 medium-12">
                <SocialLinkButton
                    network={network}
                    className="u-spacing-bottom-25"
                    onSocialLink={this.props.onSocialLink}
                    onSocialLinkError={this.props.onSocialLinkError}
                />
            </div>
        );
    }

    renderBasicForm() {
        const {
            currentUser,
            onEditFieldInput,
            editableFieldValues
        } = this.props;
        const profile = currentUser.profile || {};
        const bio = editableFieldValues.bio || profile.bio || '';

        const errors = currentUser.errors;

        const errorObj =
            errors && errors[0] && errors[0].errors ? errors[0].errors : {};
        const messages = Object.keys(errorObj).reduce((obj, key) => {
            const keyObj = errors[0].errors[key];
            const keyObjKey = Object.keys(keyObj)[0];
            const newObj = obj;

            newObj[key] = {
                description: keyObj[keyObjKey]
            };

            return newObj;
        }, {});

        const genreOptions = Object.keys(allGenresMap).map((genre) => {
            return { value: genre, text: allGenresMap[genre] };
        });
        const genreProps = {
            value:
                typeof editableFieldValues.genre !== 'undefined'
                    ? editableFieldValues.genre
                    : currentUser.profile.genre || ''
        };

        let podcastCategory;
        let hasPodcast = false;

        if (genreProps.value === 'podcast') {
            const podcastCategoryOptions = [
                { value: '', text: 'Choose category' }
            ].concat(
                podcastCategories.map(({ category }) => {
                    return { value: category, text: category };
                })
            );
            const podcastLangOptions = Object.keys(podcastLanguages).map(
                (langCode) => {
                    const text = podcastLanguages[langCode];

                    return { value: langCode, text: text };
                }
            );
            const podcastCategoryProps = {
                value:
                    typeof editableFieldValues.podcast_category !== 'undefined'
                        ? editableFieldValues.podcast_category
                        : currentUser.profile.podcast_category || ''
            };
            const podcastLanguageProps = {
                value:
                    typeof editableFieldValues.podcast_language !== 'undefined'
                        ? editableFieldValues.podcast_language
                        : currentUser.profile.podcast_language || 'en'
            };

            hasPodcast = true;
            podcastCategory = (
                <Fragment>
                    <div className="column small-24 medium-6">
                        <label htmlFor="podcast_category">
                            Podcast Category:
                        </label>

                        <Select
                            onChange={onEditFieldInput}
                            options={podcastCategoryOptions}
                            name="podcast_category"
                            id="podcast_category"
                            selectProps={podcastCategoryProps}
                        />
                    </div>
                    <div className="column small-24 medium-6">
                        <label htmlFor="podcast_language">
                            Podcast Language:
                        </label>

                        <Select
                            onChange={onEditFieldInput}
                            options={podcastLangOptions}
                            name="podcast_language"
                            id="podcast_language"
                            selectProps={podcastLanguageProps}
                        />
                    </div>
                    <div className="column small-24 medium-6">
                        <label htmlFor="genre">RSS Feed:</label>

                        <input
                            readOnly
                            type="text"
                            defaultValue={getPodcastUrl(
                                currentUser.profile.url_slug
                            )}
                            onClick={this.handleRssInputClick}
                        />
                    </div>
                </Fragment>
            );
        }

        let webPrompt;

        if (!profile.url && !editableFieldValues.url) {
            webPrompt = (
                <p className="user-profile__web-prompt u-pos-absolute u-fs-11 u-fw-700 u-ls-n-05">
                    Look like a Pro Artist Online. Build a stunning music
                    website with Bandzoogle.
                    <a
                        href="https://bandzoogle.com/?pc=audiomack&utm_source=audiomack&utm_medium=website&utm_campaign=partners&utm_content=website_field"
                        target="_blank"
                        rel="nofollow"
                    >
                        {' '}
                        Try it Free.
                    </a>{' '}
                </p>
            );
        }

        const genreColumnClass = classnames('column', {
            'small-24 medium-6': hasPodcast
        });

        const linkageError = !this.props.currentUser.profile.verified_email ? (
            <p className="auth__error">
                Verify your email address in order to link your social accounts
            </p>
        ) : null;

        return (
            <form
                encType="multipart/form-data"
                method="post"
                action=""
                className="user-profile__feed row u-margin-0"
            >
                <div className="column small-24 medium-12">
                    <label htmlFor="name" className="required">
                        Name
                    </label>

                    <input
                        type="text"
                        name="name"
                        id="name"
                        defaultValue={profile.name}
                        className="form-control"
                        onChange={onEditFieldInput}
                        required
                    />
                    {this.renderError(messages.name)}
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="label">Label</label>

                    <input
                        type="text"
                        name="label"
                        id="label"
                        onChange={onEditFieldInput}
                        defaultValue={profile.label}
                        className="form-control"
                    />
                    {this.renderError(messages.label)}
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="hometown">Hometown</label>

                    <input
                        type="text"
                        name="hometown"
                        id="hometown"
                        onChange={onEditFieldInput}
                        defaultValue={profile.hometown}
                        className="form-control"
                    />
                    {this.renderError(messages.hometown)}
                </div>
                <div className="column small-24 medium-12 u-pos-relative">
                    <label htmlFor="url">Website URL</label>
                    <input
                        type="text"
                        name="url"
                        id="url"
                        onChange={onEditFieldInput}
                        defaultValue={profile.url}
                        className="form-control"
                    />
                    {this.renderError(messages.url)}
                    {webPrompt}
                </div>
                <div className="column small-24">
                    <label htmlFor="bio">Short Biography</label>
                    <textarea
                        name="bio"
                        id="bio"
                        rows="4"
                        className="form-control"
                        cols="80"
                        style={{ minHeight: 95 }}
                        onChange={onEditFieldInput}
                        maxLength={profileBiographyMaxChars}
                        defaultValue={bio}
                    />
                    <em>
                        <span className="charsleft u-d-inline-block">
                            You have{' '}
                            <span>{profileBiographyMaxChars - bio.length}</span>{' '}
                            character
                            {profileBiographyMaxChars - bio.length === 1
                                ? ''
                                : 's'}{' '}
                            left.
                        </span>
                    </em>
                    {this.renderError(messages.bio)}
                </div>
                <div className={genreColumnClass}>
                    <label htmlFor="genre">Genre:</label>

                    <Select
                        onChange={onEditFieldInput}
                        options={genreOptions}
                        name="genre"
                        id="genre"
                        selectProps={genreProps}
                    />
                    {this.renderError(messages.genre)}
                </div>
                {podcastCategory}
                <div className="column small-24">{linkageError}</div>
                {this.renderSocialButton(SOCIAL_NETWORK_LINK_TWITTER)}
                {this.renderSocialButton(SOCIAL_NETWORK_LINK_INSTAGRAM)}
                <div className="column small-24 medium-12">
                    <label
                        htmlFor="facebook"
                        style={{
                            position: 'absolute',
                            left: '-999em',
                            width: '1em',
                            overflow: 'hidden'
                        }}
                    >
                        Facebook URL
                    </label>
                    <div className="facebook-input-container">
                        <input
                            type="text"
                            name="facebook"
                            id="facebook"
                            placeholder="https://facebook.com/username"
                            onChange={onEditFieldInput}
                            defaultValue={profile.facebook}
                            className="form-control"
                        />
                        <FacebookIcon className="text-icon" />
                    </div>
                    {this.renderError(messages.facebook)}
                </div>
                <div className="column small-24 medium-12">
                    <label
                        htmlFor="youtube"
                        style={{
                            position: 'absolute',
                            left: '-999em',
                            width: '1em',
                            overflow: 'hidden'
                        }}
                    >
                        YouTube URL
                    </label>
                    <div className="youtube-input-container">
                        <input
                            type="text"
                            name="youtube"
                            id="youtube"
                            onChange={onEditFieldInput}
                            defaultValue={profile.youtube}
                            placeholder="Enter a YouTube URL"
                            className="form-control"
                        />
                        <YouTubeIcon className="text-icon" />
                    </div>
                    {this.renderError(messages.youtube)}
                </div>
            </form>
        );
    }

    renderNewPasswordForm() {
        const {
            currentUser,
            onUpdatePasswordFormSubmit,
            onUpdatePasswordInput,
            showPassword
        } = this.props;

        const { error } = currentUser.updatePassword;

        let messages;

        if (error) {
            messages = <p className="auth__error">{error.message}</p>;
        }

        let inputType = 'password';
        let passwordIcon = <PasswordView />;

        if (showPassword) {
            inputType = 'text';
            passwordIcon = <PasswordHide />;
        }

        const eyeClass = classnames('form__password-toggle', {
            'form__password-toggle--hide': showPassword
        });

        return (
            <form
                onSubmit={onUpdatePasswordFormSubmit}
                className="form--show-labels column"
            >
                <div className="u-pos-relative">
                    <label htmlFor="current">Current password</label>
                    <input
                        id="current"
                        name="current"
                        key="current"
                        type={inputType}
                        autoComplete="current-password"
                        required
                        onChange={onUpdatePasswordInput}
                    />
                    <button
                        className={eyeClass}
                        onClick={this.props.onPasswordToggle}
                        tabIndex="-1"
                        type="button"
                    >
                        {passwordIcon}
                    </button>
                </div>

                <div className="u-pos-relative">
                    <label htmlFor="new1">New password</label>
                    <input
                        id="new1"
                        name="new1"
                        key="new1"
                        type={inputType}
                        autoComplete="new-password"
                        required
                        onChange={onUpdatePasswordInput}
                    />
                    <button
                        className={eyeClass}
                        onClick={this.props.onPasswordToggle}
                        tabIndex="-1"
                        type="button"
                    >
                        {passwordIcon}
                    </button>
                </div>
                {messages}
            </form>
        );
    }

    renderNewEmailForm() {
        const {
            currentUser,
            onUpdateEmailInput,
            newEmail,
            showPassword
        } = this.props;

        const { error } = currentUser.updateEmail;

        let messages = {};

        if (error) {
            messages = this.getErrorMessagesObject(error.errors);
        }

        let inputType = 'password';
        let passwordIcon = <PasswordView />;

        if (showPassword) {
            inputType = 'text';
            passwordIcon = <PasswordHide />;
        }

        const eyeClass = classnames('form__password-toggle', {
            'form__password-toggle--hide': showPassword
        });

        return (
            <form className="column form--show-labels">
                <label htmlFor="currentEmail">Current email</label>
                <input
                    id="currentEmail"
                    type="email"
                    value={currentUser.profile.email}
                    name="email"
                    key="currentEmail"
                    autoComplete="off"
                    autoCapitalize="none"
                    disabled
                    readOnly
                />

                <label htmlFor="newEmail">New email</label>
                <input
                    id="newEmail"
                    type="email"
                    value={newEmail.email || ''}
                    name="email"
                    key="email"
                    autoComplete="off"
                    autoCapitalize="none"
                    required
                    onChange={onUpdateEmailInput}
                />
                {this.renderError(messages.email)}
                <div className="u-pos-relative">
                    <label htmlFor="newEmailPassword">Current password</label>
                    <input
                        id="newEmailPassword"
                        type={inputType}
                        value={newEmail.password || ''}
                        name="password"
                        key="emailPassword"
                        autoComplete="current-password"
                        required
                        onChange={onUpdateEmailInput}
                    />
                    <button
                        className={eyeClass}
                        onClick={this.props.onPasswordToggle}
                        tabIndex="-1"
                        type="button"
                    >
                        {passwordIcon}
                    </button>
                    {this.renderError(messages.password)}
                </div>
            </form>
        );
    }

    renderNewSlugForm() {
        const { currentUser, onUpdateUrlSlugInput, urlSlugInput } = this.props;

        const { error } = currentUser.updateSlug;

        let message;

        if (error) {
            message = error.message;
        }

        return (
            <form className="column">
                <p>
                    Note: When you change your Artist URL, references to your
                    music under the old URL automatically change to the new URL.
                    This means any links shared with the old URL will no longer
                    work.
                </p>
                <div className="u-d-flex u-spacing-top-em">
                    <span className="input input--prepend">
                        {`${process.env.AM_URL.split('//')[1]}/artist/`}
                    </span>
                    <input
                        type="text"
                        value={urlSlugInput || ''}
                        pattern="^[a-zA-Z0-9\-]*$"
                        name="url_slug"
                        required
                        onChange={onUpdateUrlSlugInput}
                    />
                </div>
                <p className="auth__error">{message}</p>
            </form>
        );
    }

    renderAppsForm() {
        const { oauth, onRevokeApp } = this.props;

        const list = oauth.apps.map((app, i) => {
            let revokeText = 'Revoke Access';

            if (app.isRevoking) {
                revokeText = 'Revoking app…';
            }

            let error;

            if (app.revokeError) {
                error = (
                    <p className="auth__error column small-24">
                        {app.revokeError.message}
                    </p>
                );
            }

            let logoContex;

            if (app.data.logo) {
                logoContex = (
                    <img src={app.data.logo} alt="app_logo" height={100} />
                );
            }

            return (
                <li className="row" key={i}>
                    <div className="column small-24 medium-flex-child-grow">
                        <p>
                            <strong>{app.name}</strong>
                        </p>
                        <h3>{app.data.appname}</h3>
                        <p>
                            {logoContex}
                            {app.data.description}
                        </p>
                        <small>
                            Approved on {moment(app.created).format('LLLL')}
                        </small>
                    </div>
                    <div className="column small-24 medium-5 medium-text-right">
                        <button
                            className="button button--pill"
                            disabled={app.isRevoking}
                            data-app={app.name}
                            onClick={onRevokeApp}
                        >
                            {revokeText}
                        </button>
                    </div>
                    {error}
                </li>
            );
        });

        let content;

        if (list.length) {
            content = <ul>{list}</ul>;
        } else {
            content = (
                <p className="u-spacing-bottom-20">
                    There are no third party apps authorized to use your
                    account.
                </p>
            );
        }

        return <div className="column">{content}</div>;
    }

    renderSetting(toggle, key) {
        return (
            <div className={styles.setting}>
                <p className={styles.settingLabel}>
                    {settingsLabelsMap[toggle.type]}
                </p>
                <ToggleSwitch
                    checked={toggle.value}
                    label={toggle.type}
                    name={toggle.type}
                    id={`toggle-${key}`}
                    onChange={this.props.onUpdateNotifications}
                />
            </div>
        );
    }

    renderNotificationsForm() {
        const { notificationSettings } = this.props;
        const { settings } = notificationSettings;

        if (!settings) {
            return null;
        }

        const sections = ['email', 'push'].map((section, i) => {
            const toggles = Object.entries(settings).map(
                ([type, value], key) => {
                    if (type.includes(section)) {
                        const item = {
                            type: type,
                            value: value
                        };

                        if (
                            Object.keys(settingsLabelsMap).indexOf(
                                item.type
                            ) === -1
                        ) {
                            return null;
                        }

                        return (
                            <div
                                className="column small-24 medium-12"
                                key={key}
                            >
                                {this.renderSetting(item, key)}
                            </div>
                        );
                    }

                    return null;
                }
            );

            return (
                <div className="row u-spacing-bottom-50" key={`section-${i}`}>
                    <h2 className={`column small-24 ${styles.sectionTitle}`}>
                        {section} Notifications
                    </h2>
                    {toggles}
                </div>
            );
        });

        return <form className="column">{sections}</form>;
    }

    renderContent(tab) {
        switch (tab) {
            case 'password':
                return this.renderNewPasswordForm();

            case 'email':
                return this.renderNewEmailForm();

            case 'url':
                return this.renderNewSlugForm();

            case 'apps':
                return this.renderAppsForm();

            case 'notifications':
                return this.renderNotificationsForm();

            case 'basic':
            default:
                return this.renderBasicForm();
        }
    }

    renderNav(tab) {
        const items = [
            {
                text: 'Basic Information',
                active: tab === 'basic',
                href: '/edit/profile/basic',
                value: 'basic'
            },
            {
                text: 'Change Password',
                active: tab === 'password',
                href: '/edit/profile/password',
                value: 'password'
            },
            {
                text: 'Update Email',
                active: tab === 'email',
                href: '/edit/profile/email',
                value: 'email'
            },
            {
                text: 'Update Artist URL',
                active: tab === 'url',
                href: '/edit/profile/url',
                value: 'url'
            },
            {
                text: 'Apps',
                active: tab === 'apps',
                href: '/edit/profile/apps',
                value: 'apps'
            },
            {
                text: 'Notifications',
                active: tab === 'notifications',
                href: '/edit/profile/notifications',
                value: 'notifications'
            },
            {
                text: 'Delete Account',
                active: tab === 'delete',
                value: 'delete',
                buttonProps: {
                    style: { color: 'red' },
                    'data-testid': 'deleteAccount',
                    onClick: this.props.onDeleteAccountClick
                },
                listItemProps: {
                    className: 'feed-bar__list-item feed-bar__list-item--delete'
                }
            }
        ];

        return (
            <FeedBar
                activeMarker={tab}
                items={items}
                onContextSwitch={this.handleTabClick}
                showBorderMarker
                underline
            />
        );
    }

    render() {
        const {
            currentUser,
            editableFieldValues,
            onEditFieldInput
        } = this.props;

        if (!currentUser.isLoggedIn) {
            return null;
        }

        const profile = currentUser.profile || {};
        const errors = currentUser.errors;

        const errorObj =
            errors && errors[0] && errors[0].errors ? errors[0].errors : {};
        const messages = this.getErrorMessagesObject(errorObj);
        const { tab } = this.state;
        const {
            submitFormLoading,
            submitButtonText
        } = this.getSubmitStateForTab(tab);
        const submitFunction = this.getSubmitFunctionForTab(tab);

        return (
            <Fragment>
                <Helmet>
                    <title>Edit Profile on Audiomack</title>
                    <meta name="robots" content="nofollow, noindex" />
                </Helmet>
                <div
                    id="profile-page"
                    className="image-banner-page user-profile user-profile--edit"
                >
                    {this.renderBannerForm(
                        typeof editableFieldValues.image_banner === 'undefined'
                            ? profile.image_banner
                            : editableFieldValues.image_banner
                    )}
                    <div className="row">
                        <div className="column">
                            <div className="user-profile__main">
                                <div className="row u-margin-0">
                                    <div className="column small-24 medium-16 small-text-center medium-text-left user-profile__main-info">
                                        <Avatar
                                            type="artist"
                                            image={
                                                editableFieldValues.image ||
                                                profile.image_base ||
                                                profile.image
                                            }
                                            onInputChange={onEditFieldInput}
                                            dirty={!!editableFieldValues.image}
                                            editable
                                            rounded
                                        />
                                        {this.renderError(messages.image)}
                                        {this.renderProfileInfo()}
                                    </div>
                                    <div className="column small-24 medium-8 small-text-center medium-text-right u-nowrap">
                                        <FollowButtonContainer
                                            className="user-profile__follow follow-button--large u-tt-uppercase"
                                            artist={profile}
                                            showCount
                                        />
                                    </div>
                                    <div className="column small-24">
                                        {this.renderNav(tab)}
                                    </div>
                                </div>
                                <div className="row user-profile__main-content">
                                    <div className="column">
                                        {this.renderContent(tab)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <FixedActionBarContainer>
                        <button
                            className="button button--pill button--large u-tt-uppercase"
                            disabled={submitFormLoading}
                            onClick={submitFunction}
                        >
                            {submitButtonText}
                        </button>
                        {tab !== 'notifications' && (
                            <Link
                                to={`/artist/${profile.url_slug}`}
                                className="button button--pill button--black button--large u-tt-uppercase"
                            >
                                Cancel
                            </Link>
                        )}
                    </FixedActionBarContainer>
                    <DeleteAccountModal />
                </div>
            </Fragment>
        );
    }
}
