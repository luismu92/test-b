import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import PinIcon from 'icons/pin';

import ArtistHeader from '../artist/ArtistHeader';
import DraggableList from '../list/DraggableList';
import AndroidLoader from '../loaders/AndroidLoader';
import FixedActionBarContainer from '../components/FixedActionBarContainer';
import FeedBar from '../widgets/FeedBar';
import MusicEditTrackContainer from './MusicEditTrackContainer';

import SearchIcon from '../icons/search';
import DragHandle from '../../../am-shared/icons/drag-handle';
import CloseIcon from '../../../am-shared/icons/close-thin';

export default class PinEditPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        currentUserUploads: PropTypes.object,
        currentUserPinned: PropTypes.object,
        onSearchFormSubmit: PropTypes.func,
        onUploadTypeClick: PropTypes.func,
        onUploadSearchInput: PropTypes.func,
        onLoadMoreUploadsClick: PropTypes.func,
        onTrackReorder: PropTypes.func,
        onSavePinsClick: PropTypes.func,
        isSaving: PropTypes.bool
    };

    componentDidUpdate(prevProps) {
        const currentList = this.props.currentUserPinned.list;
        const lastList = prevProps.currentUserPinned.list;

        const changedIds =
            currentList.length !== lastList.length ||
            currentList.some((item, i) => item.id !== lastList[i].id);

        if (changedIds && this.DraggableList) {
            this.DraggableList.forceUpdateGrid();
        }
    }

    renderPinnedItems(currentUserPinned) {
        const userPins = currentUserPinned.list;

        let loader;
        let pinnedItems;

        if (currentUserPinned.loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (userPins.length) {
            pinnedItems = userPins.map((musicItem, i) => {
                return (
                    <MusicEditTrackContainer
                        key={`${musicItem.id}:${i}`}
                        index={i}
                        track={musicItem}
                        isPinned
                    />
                );
            });
        }

        if (!userPins.length) {
            return (
                <p className="column small-24 u-spacing-bottom-20 u-padding-left-45 u-text-gray7 u-ls-n-07">
                    It looks like you have no highlighted content. You can
                    highlight up to four (4) items! Just tap on the plus icon
                    next to a song or podcast to highlight it on your profile!
                </p>
            );
        }

        return (
            <Fragment>
                {loader}
                <DraggableList
                    containerProps={{
                        className: 'column small-24 manage-pins__pins'
                    }}
                    dragHandle={
                        <span className="u-drag-handle u-drag-handle--music-edit u-text-gray9">
                            <DragHandle />
                        </span>
                    }
                    draggingClass="music-edit-track--dragging"
                    onDragFinish={this.props.onTrackReorder}
                    items={pinnedItems}
                    onRowHeight={this.handleRowHeight}
                    ref={(instance) => {
                        this.DraggableList = instance;
                    }}
                    useWindowAsScrollContainer
                />
            </Fragment>
        );
    }

    renderUserUploads() {
        const {
            currentUserUploads,
            onLoadMoreUploadsClick,
            onUploadSearchInput,
            onUploadTypeClick,
            currentUser
        } = this.props;
        const hasNoUploads =
            !currentUserUploads.list.length && currentUserUploads.page === 1;
        let loader;
        let loadMoreButton;

        if (currentUserUploads.loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (!currentUserUploads.onLastPage && currentUserUploads.list.length) {
            loadMoreButton = (
                <button
                    className="button button--link"
                    onClick={onLoadMoreUploadsClick}
                >
                    <span className="u-fw-700">Load More Content +</span>
                </button>
            );
        }

        const list = currentUserUploads.list.map((item, i) => {
            return (
                <MusicEditTrackContainer
                    key={`${item.id}:${i}`}
                    index={i}
                    track={item}
                    currentUser={currentUser}
                    isPinnable
                />
            );
        });

        let content;
        let noUploadsMessage;

        if (hasNoUploads && !currentUserUploads.loading) {
            if (
                currentUserUploads.query.trim() === '' &&
                currentUserUploads.show === 'all'
            ) {
                noUploadsMessage = (
                    <p>You have no uploads that can be highlighted yet.</p>
                );
            } else {
                noUploadsMessage = <p>No uploads matched your search query.</p>;
            }
        } else {
            content = (
                <Fragment>
                    {list}
                    <div className="u-spacing-top u-spacing-bottom-20 u-text-center">
                        {loadMoreButton}
                    </div>
                </Fragment>
            );
        }

        const contextItems = [
            { value: 'all', text: 'Your Uploads' },
            { value: 'songs', text: 'Songs' },
            { value: 'albums', text: 'Albums' },
            { value: 'playlists', text: 'Playlists' }
        ].map((item) => {
            return {
                ...item,
                active: item.value === currentUserUploads.show
            };
        });

        return (
            <div className="column small-24 u-pos-relative">
                <div className="row u-d-flex--align-center">
                    <div className="column small-24 medium-18 u-padding-right-0">
                        <FeedBar
                            className="highlights-feed-bar feed-bar--align-right"
                            activeMarker={currentUserUploads.show}
                            items={contextItems}
                            onContextSwitch={onUploadTypeClick}
                        />
                    </div>
                    <div className="column small-24 medium-6">
                        <form
                            onSubmit={this.props.onSearchFormSubmit}
                            className="search-form highlights-search-form u-spacing-top-15"
                        >
                            <button
                                type="submit"
                                className="search-form__submit"
                                value="Search"
                            >
                                <SearchIcon className="" />
                            </button>
                            <input
                                type="text"
                                className="search-form__input"
                                placeholder="Search your music..."
                                onChange={onUploadSearchInput}
                                value={currentUserUploads.query || ''}
                            />
                        </form>
                    </div>
                </div>
                <div className="row">
                    <div className="manage-pins__uploads column small-24">
                        {content}
                        {loader}
                        {noUploadsMessage}
                    </div>
                </div>
            </div>
        );
    }

    renderActionButtons() {
        const { currentUser, isSaving } = this.props;

        let buttonText = 'Save Items';

        if (isSaving) {
            buttonText = 'Saving Items...';
        }

        return (
            <FixedActionBarContainer>
                <button
                    onClick={this.props.onSavePinsClick}
                    type="button"
                    className="button button--pill button--large u-tt-cap"
                >
                    {buttonText}
                </button>
                <Link
                    to={`/artist/${currentUser.profile.url_slug}`}
                    className="button button--pill button--black button--large u-tt-cap"
                >
                    Go to your Profile
                </Link>
                <p className="u-d-inline-block u-spacing-left-20">
                    <Link
                        className="edit__cancel"
                        to={`/artist/${currentUser.profile.url_slug}`}
                    >
                        <CloseIcon />
                    </Link>
                </p>
            </FixedActionBarContainer>
        );
    }

    render() {
        const { currentUser, currentUserPinned } = this.props;

        return (
            <Fragment>
                <div className="row">
                    <div className="column small-24">
                        <div className="user-profile__main">
                            <ArtistHeader artist={this.props.currentUser} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="column small-24">
                        <div className="u-bg-white u-padding-y-10">
                            <div className="row column small-24 u-padding-x-20">
                                <div className="row u-margin-0">
                                    <h2 className="column small-24 medium-12 pinned-title u-fs-16 u-lh-16 u-ls-n-05 u-spacing-bottom-10">
                                        <PinIcon className="u-text-icon u-brand-color pinned-title__icon" />
                                        Highlighted Items
                                    </h2>
                                    <p className="column small-24 medium-12 u-text-right u-fs-13 u-lh-19 u-ls-n-05 u-text-gray-5">
                                        <span className="u-text-orange">*</span>
                                        You can highlight up to four (4) items.
                                    </p>
                                </div>
                                {this.renderPinnedItems(currentUserPinned)}
                                {this.renderUserUploads(currentUser)}
                            </div>
                        </div>
                    </div>
                </div>
                {this.renderActionButtons()}
            </Fragment>
        );
    }
}
