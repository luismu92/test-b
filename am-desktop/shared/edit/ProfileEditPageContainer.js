import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import analytics from 'utils/analytics';
import { previewFile, ucfirst } from 'utils/index';

import requireAuth from '../hoc/requireAuth';
import {
    saveUserDetails,
    updatePassword,
    logOut,
    updateSlug,
    updateEmail,
    clearErrors
} from '../redux/modules/user/index';
import {
    getUserSettings,
    updateUserSettings
} from '../redux/modules/user/notificationSettings';
import { getAuthorizedApps, revokeApp } from '../redux/modules/oauth';
import { MODAL_TYPE_DELETE_ACCOUNT, showModal } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';

import ProfileEditPage from './ProfileEditPage';

class ProfileEditPageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        oauth: PropTypes.object.isRequired,
        match: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        onFormSubmit: PropTypes.func,
        notificationSettings: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editableFieldValues: {},
            updatePassword: {},
            showPassword: false,
            newUrlSlug: props.currentUser.profile.url_slug,
            newEmail: {
                email: '',
                emailPassword: ''
            }
        };
    }

    componentWillUnmount() {
        this.props.dispatch(clearErrors());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleEditFieldInput = (e) => {
        const input = e.currentTarget;
        const { name, type } = input;
        let { value } = input;

        if (name === 'image') {
            previewFile(e.target.files[0])
                .then((result) => {
                    this.setState((prevState) => {
                        const newState = {
                            ...prevState
                        };

                        newState.editableFieldValues[name] = result;
                        return newState;
                    });
                    return;
                })
                .catch((err) => {
                    console.log(err);
                    analytics.error(err);
                });
            return;
        }

        if (type === 'select-one') {
            value = input.options[input.selectedIndex].value;
        }

        const newState = {
            ...this.state
        };

        newState.editableFieldValues[name] = value;

        this.setState(newState);
    };

    handleUpdatePasswordInput = (e) => {
        const { name, value } = e.currentTarget;

        this.setState((prevState) => {
            return {
                ...prevState,
                updatePassword: {
                    ...prevState.updatePassword,
                    [name]: value
                }
            };
        });
    };

    handleUpdateUrlSlugInput = (e) => {
        const { value } = e.currentTarget;

        this.setState((prevState) => {
            return {
                ...prevState,
                newUrlSlug: (value || '').toLowerCase()
            };
        });
    };

    handleUpdateEmailInput = (e) => {
        const { name, value } = e.currentTarget;

        this.setState((prevState) => {
            let finalValue = value;

            if (name === 'email') {
                finalValue = finalValue.toLowerCase();
            }

            return {
                ...prevState,
                newEmail: {
                    ...prevState.newEmail,
                    [name]: finalValue
                }
            };
        });
    };

    handleUpdateNotifications = (e) => {
        const { dispatch } = this.props;
        const { name, checked } = e.currentTarget;

        dispatch(updateUserSettings(name, checked)).catch((err) => {
            dispatch(
                addToast({
                    action: 'message',
                    message:
                        'There was a problem updating your preferences. Please try again.'
                })
            );
            console.log(err);
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, onFormSubmit, history, currentUser } = this.props;

        dispatch(saveUserDetails(this.state.editableFieldValues))
            .then(() => {
                if (onFormSubmit) {
                    onFormSubmit();
                }
                history.push(`/artist/${currentUser.profile.url_slug}`);
                return;
            })
            .catch((err) => console.log(err));
    };

    handleUpdatePasswordFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, onFormSubmit, history } = this.props;
        const { current: oldPass, new1: newPass } = this.state.updatePassword;

        dispatch(updatePassword(oldPass, newPass, newPass))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            'Your password has been changed. You can now log in with your new password.'
                    })
                );
                dispatch(logOut());
                history.push('/login');

                if (onFormSubmit) {
                    onFormSubmit();
                }
                return;
            })
            .catch((err) => {
                let message = 'There was an error updating your password.';

                if (err.errorcode === 1004) {
                    message = err.message;
                }

                dispatch(
                    addToast({
                        action: 'message',
                        item: message
                    })
                );
                console.log(err);
            });
    };

    handleUpdateUrlSlugFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, onFormSubmit, history } = this.props;

        dispatch(updateSlug(this.state.newUrlSlug))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Your URL slug has been updated.'
                    })
                );
                if (onFormSubmit) {
                    onFormSubmit();
                }
                history.push(`/artist/${this.state.newUrlSlug}`);
                return;
            })
            .catch((err) => console.log(err));
    };

    handleUpdateEmailFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, onFormSubmit, history } = this.props;
        const { email, password } = this.state.newEmail;

        dispatch(updateEmail(email, password))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            'Your email has been updated. You can now log in with your new email.'
                    })
                );

                dispatch(logOut());

                history.push('/login');

                if (onFormSubmit) {
                    onFormSubmit();
                }
                return;
            })
            .catch((err) => console.log(err));
    };

    handleDeleteAccountClick = () => {
        this.props.dispatch(showModal(MODAL_TYPE_DELETE_ACCOUNT));
    };

    handleBannerEditApply = (result) => {
        const { dispatch } = this.props;

        return new Promise((resolve) => {
            this.setState(
                (prevState) => {
                    const newState = {
                        ...prevState,
                        editableFieldValues: {
                            ...prevState.editableFieldValues,
                            image_banner: result
                        }
                    };

                    return newState;
                },
                () => {
                    dispatch(saveUserDetails({ image_banner: result }))
                        .then(() => {
                            resolve();
                            return;
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                }
            );
        });
    };

    handleBannerRemoveClick = () => {
        const { dispatch, currentUser } = this.props;
        const currentBanner =
            this.state.editableFieldValues.image_banner ||
            currentUser.profile.image_banner;

        this.setState((prevState) => {
            return {
                editableFieldValues: {
                    ...prevState.editableFieldValues,
                    image_banner: ''
                }
            };
        });

        dispatch(
            saveUserDetails({
                image_banner: 'remove'
            })
        ).catch((err) => {
            console.log(err);

            this.setState((prevState) => {
                return {
                    editableFieldValues: {
                        ...prevState.editableFieldValues,
                        image_banner: currentBanner
                    }
                };
            });
        });
    };

    handleRevokeApp = (e) => {
        const button = e.currentTarget;
        const app = button.getAttribute('data-app');

        this.props.dispatch(revokeApp(app));
    };

    handlePasswordToggle = () => {
        this.setState({
            showPassword: !this.state.showPassword
        });
    };

    handleSocialLinkSuccess = (action, network) => {
        this.props.dispatch(
            addToast({
                action: 'message',
                message: `Your ${ucfirst(network)} was successfully linked.`
            })
        );
    };

    handleSocialLinkError = (err, action, network) => {
        this.props.dispatch(
            addToast({
                action: 'message',
                message:
                    err.message ||
                    `There was an error ${action}ing your ${ucfirst(network)}`
            })
        );
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        return (
            <ProfileEditPage
                initialTab={this.props.match.params.tab}
                oauth={this.props.oauth}
                editableFieldValues={this.state.editableFieldValues}
                player={this.props.player}
                currentUser={this.props.currentUser}
                onEditFieldInput={this.handleEditFieldInput}
                onFormSubmit={this.handleFormSubmit}
                onUpdatePasswordInput={this.handleUpdatePasswordInput}
                onUpdateUrlSlugInput={this.handleUpdateUrlSlugInput}
                onUpdatePasswordFormSubmit={this.handleUpdatePasswordFormSubmit}
                onUpdateUrlSlugFormSubmit={this.handleUpdateUrlSlugFormSubmit}
                onUpdateNotifications={this.handleUpdateNotifications}
                onDeleteAccountClick={this.handleDeleteAccountClick}
                onSocialLink={this.handleSocialLinkSuccess}
                onSocialLinkError={this.handleSocialLinkError}
                urlSlugInput={this.state.newUrlSlug}
                newEmail={this.state.newEmail}
                onUpdateEmailFormSubmit={this.handleUpdateEmailFormSubmit}
                onUpdateEmailInput={this.handleUpdateEmailInput}
                onBannerEditApply={this.handleBannerEditApply}
                onBannerRemoveClick={this.handleBannerRemoveClick}
                onRevokeApp={this.handleRevokeApp}
                onPasswordToggle={this.handlePasswordToggle}
                showPassword={this.state.showPassword}
                notificationSettings={this.props.notificationSettings}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        notificationSettings: state.currentUserNotificationSettings,
        player: state.player,
        oauth: state.oauth
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(ProfileEditPageContainer, [
        () => getAuthorizedApps(),
        () => getUserSettings()
    ])
);
