import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';

import requireAuth from '../hoc/requireAuth';
import { eventLabel } from 'utils/analytics';

import SongUploadContainer from '../upload/SongUploadContainer';
import TunecoreAd from '../components/Tunecore';

class SongEditPageContainer extends Component {
    static propTypes = {
        musicItem: PropTypes.object.isRequired,
        onSongDelete: PropTypes.func,
        fromMobile: PropTypes.bool
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    render() {
        const { musicItem, onSongDelete } = this.props;

        return (
            <div>
                <h1 className="u-text-center upload-intro__title upload__main-title u-spacing-top">
                    Edit this song
                </h1>
                <SongUploadContainer
                    musicItem={musicItem}
                    onSongDelete={onSongDelete}
                    fromMobile={this.props.fromMobile}
                />
                <div className="row" style={{ maxWidth: 940 }}>
                    <div className="column">
                        <TunecoreAd
                            className="u-text-center u-spacing-top"
                            linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=%20all_afpup%20_su&utm_content=ghoammot"
                            eventLabel={eventLabel.songEdit}
                            variant="condensed"
                        />
                    </div>
                </div>
                <div className="album-actions">
                    <Link
                        to={`/song/${musicItem.uploader.url_slug}/${
                            musicItem.url_slug
                        }`}
                        className="button button--pill"
                    >
                        Go to song ›
                    </Link>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        upload: state.upload
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(SongEditPageContainer);
