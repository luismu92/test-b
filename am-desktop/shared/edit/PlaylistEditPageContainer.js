import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { allGenresMap } from 'constants/index';

import requireAuth from '../hoc/requireAuth';

import {
    MODAL_TYPE_CONFIRM,
    showModal,
    hideModal
} from '../redux/modules/modal';
import {
    savePlaylistDetails,
    deleteSong,
    deletePlaylist
} from '../redux/modules/playlist';
import { addToast } from '../redux/modules/toastNotification';

import PlaylistEditPage from './PlaylistEditPage';

const playlistDescriptionMaxChars = 1200;
const metadataTabMap = {
    basic: 'Basic Information',
    metadata: 'Metadata',
    release: 'Release',
    monetization: 'Monetization'
};

class PlaylistEditPageContainer extends Component {
    static propTypes = {
        musicEdit: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        dispatch: PropTypes.func,
        finishButtonText: PropTypes.string,
        finishingButtonText: PropTypes.string,
        onPlaylistDelete: PropTypes.func,
        onPlaylistSave: PropTypes.func,
        player: PropTypes.object,
        history: PropTypes.object
    };

    static defaultProps = {
        onPlaylistDelete() {},
        onPlaylistSave() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            isFinishing: false,
            dirty: false,
            errors: {},
            musicEditLocal: props.musicEdit,
            descriptionCharsLeft: playlistDescriptionMaxChars,
            selectedItems: [],
            rangeStart: null
        };
    }

    componentDidMount() {
        window.addEventListener('keyup', this.handleSelectKeyUp, false);
        this._unblock = this.props.history.block(
            this.handleHistoryLocationChange
        );
    }

    componentWillUnmount() {
        window.removeEventListener('keyup', this.handleSelectKeyUp, false);
        if (this._unblock) {
            this._unblock();
        }
        this._unblock = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSelectKeyUp = (e) => {
        e.preventDefault();

        const pressed = e.keyCode;
        const escape = 27;

        if (pressed === escape) {
            this.setState({
                selectedItems: []
            });
        }
    };

    handleHistoryLocationChange = (nextLocation) => {
        const { dispatch } = this.props;

        if (!this.state.dirty) {
            if (this._unblock) {
                this._unblock();
                this._unblock = null;
            }
            return true;
        }

        const message = 'You have unsaved changes.';

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure you want to leave?',
                message,
                handleConfirm: () => this.confirmLeave(nextLocation),
                confirmButtonText: 'Leave anyway',
                confirmButtonProps: {
                    className: 'button button--pill'
                }
            })
        );

        return false;
    };

    handlePlaylistInfoInput = (e) => {
        const errors = this.state.errors;
        const input = e.currentTarget;
        const inputName = input.name;
        let inputValue = input.value;

        if (inputName === 'description') {
            this.setState({
                descriptionCharsLeft:
                    playlistDescriptionMaxChars - input.value.length
            });
        }

        switch (input.type) {
            case 'checkbox':
                inputValue = input.checked;
                break;

            case 'select-one': {
                const value = input.options[input.selectedIndex].value;

                if (!!value && errors[input.name]) {
                    errors[input.name] = false;
                }

                this.setState({
                    errors
                });

                inputValue = value;
                break;
            }

            default: {
                const value = input.value;

                if (
                    input.name === 'url_slug' &&
                    !input.value.match('^[a-zA-Z0-9_-]*$')
                ) {
                    return;
                }

                if (!!value && errors[input.name]) {
                    errors[input.name] = false;
                }

                this.setState({
                    errors
                });
                break;
            }
        }

        this.setState((prevState) => {
            return {
                ...prevState,
                musicEditLocal: {
                    ...prevState.musicEditLocal,
                    info: {
                        ...prevState.musicEditLocal.info,
                        [inputName]: inputValue
                    }
                },
                dirty: true
            };
        });
    };

    handlePlaylistDeleteClick = (e) => {
        console.log('delete', e);
        const { dispatch } = this.props;
        const { musicEditLocal } = this.state;
        const title =
            e.currentTarget.getAttribute('data-modal-title') ||
            'Delete playlist';

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title,
                message:
                    'Are you sure you want to delete this playlist? This action cannot be undone.',
                handleConfirm: () =>
                    this.deletePlaylist(musicEditLocal.info.id),
                confirmButtonText: 'Delete',
                confirmButtonProps: {
                    className: 'button button--pill button--danger'
                }
            })
        );
    };

    handleTrackRemoveClick = (e) => {
        const { dispatch } = this.props;
        const { musicEditLocal, selectedItems } = this.state;

        let songId;
        let modalTitle = 'Delete playlist track';
        let modalMessage = 'this track';

        if (selectedItems.length) {
            const selectedItemIds = selectedItems.map(
                (item) => item.props.track.id
            );

            songId = selectedItemIds;
            modalTitle = 'Delete playlist tracks';
            modalMessage = 'these tracks';
        } else {
            songId = e.currentTarget.getAttribute('data-music-id');
        }

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: modalTitle,
                message: `Are you sure you want to delete ${modalMessage} from the playlist? This action cannot be undone.`,
                handleConfirm: () =>
                    this.deletePlaylistTrack(musicEditLocal.info.id, songId),
                confirmButtonText: 'Delete',
                confirmButtonProps: {
                    className: 'button button--pill button--danger'
                }
            })
        );
    };

    handlePlaylistSaveClick = () => {
        this.savePlaylist();
    };

    handleTrackReorder = (fromIndex, toIndex) => {
        const { musicEditLocal, selectedItems } = this.state;
        let newTracks = Array.from(musicEditLocal.info.tracks);

        if (selectedItems.length) {
            const selectedItemObj = selectedItems.map(
                (item) => item.props.track
            );
            const selectedItemIds = selectedItems.map(
                (item) => item.props.track.id
            );

            const items = this.state.musicEditLocal.info.tracks.filter(
                (value) => !selectedItemIds.includes(value.id)
            );

            newTracks = [
                ...items.slice(0, toIndex),
                ...selectedItemObj,
                ...items.slice(toIndex, items.length)
            ];
        } else {
            const fromTrack = newTracks.splice(fromIndex, 1)[0];

            newTracks.splice(toIndex, 0, fromTrack);
        }

        const newMusicEdit = {
            ...this.state.musicEditLocal,
            info: {
                ...this.state.musicEditLocal.info,
                tracks: newTracks
            }
        };

        this.setState({
            dirty: true,
            musicEditLocal: newMusicEdit,
            selectedItems: []
        });

        // Actual reordering on server happens when previewing/finishing
    };

    handleMoveToTopClick = () => {
        this.handleTrackReorder(null, 0);
    };

    handleMoveToBottomClick = () => {
        const { selectedItems } = this.state;

        const selectedItemIds = selectedItems.map(
            (item) => item.props.track.id
        );

        const items = this.state.musicEditLocal.info.tracks.filter(
            (value) => !selectedItemIds.includes(value.id)
        );

        this.handleTrackReorder(null, items.length);
    };

    handleBannerEditApply = (result) => {
        const currentBanner = this.state.musicEditLocal.image_banner;

        return new Promise((resolve, reject) => {
            this.setState(
                (prevState) => {
                    return {
                        ...prevState,
                        musicEditLocal: {
                            ...prevState.musicEditLocal,
                            info: {
                                ...prevState.musicEditLocal.info,
                                image_banner: result
                            }
                        }
                    };
                },
                () => {
                    this.savePlaylist()
                        .then(resolve)
                        .catch((err) => {
                            console.error(err);

                            this.setState((prevState) => {
                                return {
                                    musicEditLocal: {
                                        ...prevState.musicEditLocal,
                                        info: {
                                            ...prevState.musicEditLocal.info,
                                            image_banner: currentBanner
                                        }
                                    }
                                };
                            });
                            reject(err);
                        });
                }
            );
        });
    };

    handleBannerRemoveClick = () => {
        const currentBanner = this.state.musicEditLocal.image_banner;

        this.setState(
            (prevState) => {
                return {
                    musicEditLocal: {
                        ...prevState.musicEditLocal,
                        info: {
                            ...prevState.musicEditLocal.info,
                            image_banner: ''
                        }
                    }
                };
            },
            () => {
                this.savePlaylist({
                    image_banner: 'remove'
                }).catch((err) => {
                    console.error(err);

                    this.setState((prevState) => {
                        return {
                            musicEditLocal: {
                                ...prevState.musicEditLocal,
                                info: {
                                    ...prevState.musicEditLocal.info,
                                    image_banner: currentBanner
                                }
                            }
                        };
                    });
                });
            }
        );
    };

    handleItemSelect = (item, list, e) => {
        const { rangeStart } = this.state;

        if (e.nativeEvent.shiftKey && rangeStart) {
            if (item.props.index < rangeStart) {
                this.selectRange(
                    this.setRange(item.props.index, rangeStart),
                    list
                );
            } else {
                this.selectRange(
                    this.setRange(rangeStart, item.props.index),
                    list
                );
            }
        }
        // No range start is defined
        else {
            this.setState({
                rangeStart: item.props.index
            });

            this.selectItems(item);
        }
    };

    handleClearSelectedItemsClick = () => {
        this.setState({
            selectedItems: []
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getInvalidFields(musicEdit) {
        // Validate current input before sending field data
        const required = ['title', 'genre', 'url_slug'];

        const invalidFields = required.filter((field) => {
            if (field === 'genre') {
                return (
                    !musicEdit.info.genre ||
                    !Object.keys(allGenresMap).includes(musicEdit.info.genre)
                );
            }

            return !musicEdit.info[field];
        });

        if (invalidFields.length) {
            const errors = invalidFields.reduce((acc, field) => {
                acc[field] = true;
                return acc;
            }, {});

            return errors;
        }

        return null;
    }

    // Set a range of integers based on rangeStart and rangeEnd
    setRange(start, end) {
        return Array(end - start + 1)
            .fill()
            .map((_, idx) => start + idx);
    }

    // Original select items function
    selectItems(item) {
        this.setState(({ selectedItems }) => {
            const selectedItemIds = selectedItems.map(
                (track) => track.props.track.id
            );

            if (selectedItemIds.includes(item.props.track.id)) {
                return {
                    selectedItems: selectedItems.filter(
                        (value) => value.props.track.id !== item.props.track.id
                    )
                };
            }

            return {
                selectedItems: [...selectedItems, item]
            };
        });
    }

    // Select a range of items
    selectRange(range, list) {
        const newTracks = list.filter((value, i) => range.includes(i));

        this.setState({
            selectedItems: newTracks
        });
    }

    itemIsSaving(musicEdit) {
        if (!musicEdit.info) {
            return false;
        }

        if (musicEdit.isSaving) {
            return true;
        }

        if (musicEdit.info.tracks && musicEdit.info.tracks.length) {
            return musicEdit.info.tracks.some((track) => track.isSaving);
        }

        return false;
    }

    confirmLeave(nextLocation) {
        const { history, dispatch } = this.props;

        if (this._unblock) {
            this._unblock();
            this._unblock = null;
        }

        history.push(nextLocation);
        dispatch(hideModal());
    }

    savePlaylist(extraState = {}) {
        const { dispatch, currentUser } = this.props;
        const { musicEditLocal } = this.state;
        const errors = this.getInvalidFields(musicEditLocal);

        if (errors) {
            this.setState({
                errors
            });

            return Promise.reject(errors);
        }

        this.setState({
            isFinishing: true
        });

        const metadata = {
            ...musicEditLocal.info,
            // Handle track reorder
            music_id: (musicEditLocal.info.tracks || [])
                .map((track) => track.id)
                .join(','),
            ...extraState
        };

        return new Promise((resolve, reject) => {
            dispatch(savePlaylistDetails(musicEditLocal.info.id, metadata))
                .then((action) => {
                    const slug = action.resolved.url_slug;
                    const redirectUrl = `/playlist/${
                        currentUser.profile.url_slug
                    }/${slug}`;

                    if (this._unblock) {
                        this._unblock();
                    }

                    this.setState({
                        dirty: false,
                        isFinishing: false
                    });

                    dispatch(
                        addToast({
                            action: 'message',
                            message: 'Your playlist was successfully updated.'
                        })
                    );

                    resolve(redirectUrl);

                    return this.props.onPlaylistSave(redirectUrl);
                })
                .catch((err) => {
                    console.error(err);

                    this.setState({
                        isFinishing: false
                    });

                    dispatch(
                        addToast({
                            action: 'message',
                            message:
                                'There was an error updating this playlist.',
                            type: 'warning'
                        })
                    );
                    reject(err);
                });
        });
    }

    deletePlaylist(musicId) {
        const { dispatch } = this.props;

        dispatch(hideModal());

        this.setState({
            dirty: false,
            errors: {}
        });

        dispatch(deletePlaylist(musicId))
            .then(() => {
                return this.props.onPlaylistDelete(musicId);
            })
            .catch((err) => console.log(err));
    }

    deletePlaylistTrack(playlistId, songId) {
        const { dispatch } = this.props;
        const { selectedItems } = this.state;

        dispatch(hideModal());
        dispatch(deleteSong(playlistId, songId))
            .then((action) => {
                this.setState((prevState) => {
                    const newMusicEdit = {
                        ...prevState.musicEditLocal,
                        info: {
                            ...prevState.musicEditLocal.info,
                            tracks: prevState.musicEditLocal.info.tracks.filter(
                                (track) => {
                                    if (selectedItems.length) {
                                        return (
                                            action.songId.indexOf(track.id) ===
                                            -1
                                        );
                                    }

                                    return track.id !== action.songId;
                                }
                            )
                        }
                    };

                    return {
                        ...prevState,
                        musicEditLocal: newMusicEdit,
                        selectedItems: [],
                        dirty: true
                    };
                });
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    }

    render() {
        return (
            <PlaylistEditPage
                currentUser={this.props.currentUser}
                metadataTabMap={metadataTabMap}
                isAutoSaving={this.state.isAutoSaving}
                player={this.props.player}
                isFinishing={this.state.isFinishing}
                dirty={this.state.dirty}
                musicEdit={this.state.musicEditLocal}
                descriptionCharsLeft={this.state.descriptionCharsLeft}
                playlistDescriptionMaxChars={playlistDescriptionMaxChars}
                finishButtonText={this.props.finishButtonText}
                finishingButtonText={this.props.finishingButtonText}
                onTrackReorder={this.handleTrackReorder}
                errors={this.state.errors}
                onPlaylistSaveClick={this.handlePlaylistSaveClick}
                onPlaylistDeleteClick={this.handlePlaylistDeleteClick}
                onPlaylistInfoInput={this.handlePlaylistInfoInput}
                onTrackRemoveClick={this.handleTrackRemoveClick}
                onBannerEditApply={this.handleBannerEditApply}
                onBannerRemoveClick={this.handleBannerRemoveClick}
                selectedItems={this.state.selectedItems}
                onItemSelect={this.handleItemSelect}
                onMoveToTopClick={this.handleMoveToTopClick}
                onMoveToBottomClick={this.handleMoveToBottomClick}
                onClearSelectedItemsClick={this.handleClearSelectedItemsClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        musicEdit: state.musicEdit,
        currentUser: state.currentUser,
        player: state.player
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(PlaylistEditPageContainer);
