import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import { ownsMusic } from 'utils/index';

import AndroidLoader from '../loaders/AndroidLoader';
import requireAuth from '../hoc/requireAuth';
import { reset as resetUploadList } from '../redux/modules/upload/index';
import {
    getMusicById,
    reset as resetMusicEdit
} from '../redux/modules/music/edit';

import NotFound from '../NotFound';

import SongEditPageContainer from './SongEditPageContainer';
// import AlbumEditPageContainer from './AlbumEditPageContainer';
import PlaylistEditPageContainer from './PlaylistEditPageContainer';
import UploadAlbumsPageContainer from '../upload/UploadAlbumsPageContainer';

class MusicEditContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        musicEdit: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
        dispatch: PropTypes.func
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        const { dispatch, match } = this.props;

        if (prevProps.match.params.musicId !== match.params.musicId) {
            dispatch(
                getMusicById(match.params.musicId, match.params.musicType)
            );
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(resetUploadList());
        dispatch(resetMusicEdit());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleMusicDelete = () => {
        this.goToArtistPage();
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    goToArtistPage() {
        const { history, currentUser } = this.props;

        history.push(`/artist/${currentUser.profile.url_slug}`);
    }

    renderPageForMusicItem(musicItem) {
        if (musicItem.type === 'song') {
            return (
                <SongEditPageContainer
                    musicItem={musicItem}
                    onSongDelete={this.handleMusicDelete}
                />
            );
        }

        if (musicItem.type === 'album') {
            return <UploadAlbumsPageContainer isEditPage={true} />;
        }

        if (musicItem.type === 'playlist') {
            return (
                <PlaylistEditPageContainer
                    finishButtonText="Update Playlist"
                    finishingButtonText="Updating Playlist"
                    onPlaylistDelete={this.handleMusicDelete}
                />
            );
        }

        return null;
    }

    render() {
        const { musicEdit, currentUser } = this.props;

        if (musicEdit.loading) {
            return (
                <div>
                    <AndroidLoader className="music-feed__loader" />
                </div>
            );
        }

        const ownMusic = ownsMusic(currentUser, musicEdit.info);

        if (musicEdit.errors.length || (!ownMusic && !currentUser.isAdmin)) {
            return <NotFound errors={musicEdit.errors} type="editMusic" />;
        }

        const musicItem = {
            ...musicEdit.info,
            isSaving: musicEdit.isSaving,
            saved: musicEdit.saved,
            isDeleting: musicEdit.isDeleting
        };

        return <Fragment>{this.renderPageForMusicItem(musicItem)}</Fragment>;
    }
}

function mapStateToProps(state) {
    return {
        musicEdit: state.musicEdit,
        upload: state.upload,
        currentUser: state.currentUser
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(MusicEditContainer, [
        (params, query, props) => {
            const options = {};
            const {
                currentUser: { token, secret }
            } = props;

            options.token = token;
            options.secret = secret;

            return getMusicById(params.musicId, params.musicType, options);
        }
    ])
);
