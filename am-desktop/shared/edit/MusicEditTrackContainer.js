import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    getArtistName,
    isCurrentMusicItem,
    getQueueIndexForSong
} from 'utils/index';
import { play, pause, editQueue } from '../redux/modules/player';

import { addToast } from '../redux/modules/toastNotification';
import { addPinned, deletePinned } from '../redux/modules/user/pinned';

import MusicEditTrack from './MusicEditTrack';

class MusicEditTrackContainer extends Component {
    static propTypes = {
        track: PropTypes.object.isRequired,
        index: PropTypes.number.isRequired,
        currentUserPinned: PropTypes.object,
        player: PropTypes.object.isRequired,
        musicItem: PropTypes.object,
        onPlayClick: PropTypes.func,
        onRemoveClick: PropTypes.func,
        onSelect: PropTypes.func,
        hasExpandableTracks: PropTypes.bool,
        isPinnable: PropTypes.bool,
        isSelectable: PropTypes.bool,
        selectedItems: PropTypes.array,
        isPinned: PropTypes.bool,
        autosave: PropTypes.bool,
        dispatch: PropTypes.func,
        showAlbumIndicator: PropTypes.bool,
        titleLink: PropTypes.bool
    };

    static defaultProps = {
        autosave: false,
        showAlbumIndicator: false,
        titleLink: false,
        onPlayClick() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            isExpanded: false
        };
    }

    handlePinClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');
        const { track } = this.props;

        this.doAction(track, action, e);
    };

    handlePlayClick = (event) => {
        const { dispatch, player, musicItem } = this.props;
        const musicList = [musicItem];
        const trackIndex =
            parseInt(
                event.currentTarget.getAttribute('data-track-index'),
                10
            ) || 0;
        const currentSong = player.currentSong;
        const shouldCountTrack = false;
        const isAlreadyCurrentSong = isCurrentMusicItem(
            currentSong,
            musicItem,
            shouldCountTrack
        );

        this.props.onPlayClick(event);

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        const { queue: newQueue } = dispatch(editQueue(musicList));
        const newIndex = getQueueIndexForSong(musicItem, newQueue, {
            trackIndex: trackIndex,
            currentQueueIndex: player.queueIndex,
            ignoreHistory: false
        });

        dispatch(play(newIndex));
    };

    handleExpandClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        this.setState((prevState) => {
            return {
                isExpanded: !prevState.isExpanded
            };
        });
    };

    doAction(track, action) {
        switch (action) {
            case 'unpin': {
                this.unpinItem(track);
                break;
            }
            case 'pin': {
                this.pinItem(track);
                break;
            }

            default:
                break;
        }
    }

    unpinItem(track) {
        const { dispatch, autosave } = this.props;

        dispatch(deletePinned(track, autosave));

        if (autosave) {
            dispatch(
                addToast({
                    action: 'unpin',
                    item: `${getArtistName(track)} - ${track.title}`
                })
            );
        }
    }

    pinItem(track) {
        const { dispatch, autosave } = this.props;

        dispatch(addPinned(track, autosave));

        if (autosave) {
            dispatch(
                addToast({
                    action: 'pin',
                    item: `${getArtistName(track)} - ${track.title}`
                })
            );
        }
    }

    render() {
        return (
            <MusicEditTrack
                track={this.props.track}
                index={this.props.index}
                currentUserPinned={this.props.currentUserPinned}
                player={this.props.player}
                isPinnable={this.props.isPinnable}
                isPinned={this.props.isPinned}
                isSelectable={this.props.isSelectable}
                selectedItems={this.props.selectedItems}
                onSelect={this.props.onSelect}
                onRemoveClick={this.props.onRemoveClick}
                onPinClick={this.handlePinClick}
                onPlayClick={this.handlePlayClick}
                onExpandClick={this.handleExpandClick}
                hasExpandableTracks={this.props.hasExpandableTracks}
                isExpanded={this.state.isExpanded}
                showAlbumIndicator={this.props.showAlbumIndicator}
                titleLink={this.props.titleLink}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        currentUserPinned: state.currentUserPinned
    };
}

export default connect(mapStateToProps)(MusicEditTrackContainer);
