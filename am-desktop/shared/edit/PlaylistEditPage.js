import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import analytics from 'utils/analytics';
import { previewFile, yesBool, getMusicUrl } from 'utils/index';
import { allGenresMap } from 'constants/index';

import CameraIcon from '../icons/camera';
import CloseIcon from 'icons/close-thin';
import DragHandle from 'icons/drag-handle';
import TrashIcon from 'icons/trash-solid';
import QueueNextIcon from '../icons/queue-next';
import QueueEndIcon from '../icons/queue-end';

import Select from '../components/Select';
import FixedActionBarContainer from '../components/FixedActionBarContainer';
import EditableImageBanner from '../widgets/EditableImageBanner';
import DraggableList from '../list/DraggableList';
import MusicEditTrackContainer from './MusicEditTrackContainer';

export default class PlaylistEditPage extends Component {
    static propTypes = {
        musicEdit: PropTypes.object.isRequired,
        finishButtonText: PropTypes.string,
        finishingButtonText: PropTypes.string,
        player: PropTypes.object,
        errors: PropTypes.object,
        dirty: PropTypes.bool,
        isFinishing: PropTypes.bool,
        playlistDescriptionMaxChars: PropTypes.number,
        descriptionCharsLeft: PropTypes.number,
        onTrackRemoveClick: PropTypes.func,
        onTrackReorder: PropTypes.func,
        onPlaylistSaveClick: PropTypes.func,
        onBannerRemoveClick: PropTypes.func,
        onBannerEditApply: PropTypes.func,
        onPlaylistInfoInput: PropTypes.func,
        onPlaylistDeleteClick: PropTypes.func,
        selectedItems: PropTypes.array,
        onItemSelect: PropTypes.func,
        onMoveToTopClick: PropTypes.func,
        onMoveToBottomClick: PropTypes.func,
        onClearSelectedItemsClick: PropTypes.func
    };

    static defaultProps = {
        finishButtonText: 'Update Playlist',
        finishingButtonText: 'Updating Playlist...'
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFileInputChange = (e) => {
        const { name } = e.currentTarget;

        previewFile(e.target.files[0])
            .then((value) => {
                return this.props.onPlaylistInfoInput({
                    currentTarget: {
                        name,
                        value
                    }
                });
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            });
    };

    handleRowHeight = () => {
        return 56;
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderUrlInput(errorClass) {
        const { musicEdit } = this.props;
        const urlInputClass = this.props.errors.url_slug ? errorClass : '';

        return (
            <div className="column small-24">
                <label htmlFor="playlist-url">Playlist URL</label>
                <div className="u-d-flex">
                    <span className="input input--prepend">
                        {`${process.env.AM_URL.split('//')[1]}/playlist/${
                            musicEdit.info.artist.url_slug
                        }/`}
                    </span>
                    <input
                        type="text"
                        className={urlInputClass}
                        value={musicEdit.info.url_slug || ''}
                        pattern="^[a-zA-Z0-9\-]*$"
                        name="url_slug"
                        onChange={this.props.onPlaylistInfoInput}
                        id="playlist-url"
                    />
                </div>
            </div>
        );
    }

    renderHeader() {
        const { musicEdit } = this.props;
        const errorClass = 'form__input--has-error';
        const titleInputClass = classnames('edit__field edit__field--title', {
            [errorClass]: this.props.errors.title
        });
        const genreOptions = Object.keys(allGenresMap).map((genre) => {
            return { value: genre, text: allGenresMap[genre] };
        });
        const genreProps = {
            required: true,
            value: musicEdit.info.genre
        };
        const selectClass = this.props.errors.genre ? errorClass : '';
        const defaultThumb =
            'https://assets.dev.audiomack.com/default-album-image.jpg';
        const deleteText = this.props.musicEdit.isDeleting
            ? 'Deleting...'
            : 'Delete this playlist';

        const style = {
            backgroundImage: `url(${musicEdit.info.image || defaultThumb})`
        };

        return (
            <div className="edit__header column small-24 u-padding-top-20 u-bg-white u-pos-relative">
                <div className="row">
                    <div className="edit__image edit__image--playlist column small-24 medium-6">
                        <div
                            className="u-pos-relative u-bg-cover u-bg-center"
                            style={style}
                        >
                            <div className="edit__image-overlay u-overlay u-d-flex u-d-flex--align-center u-d-flex--justify-center u-text-center u-text-white">
                                <input
                                    type="file"
                                    name="image"
                                    accept="image/*"
                                    onChange={this.handleFileInputChange}
                                />
                                <div>
                                    <span className="edit__image-icon u-d-inline-block">
                                        <CameraIcon />
                                    </span>
                                    <span className="u-fs-12 u-fw-700 u-ls-n-05 u-lh-14 u-d-block">
                                        Minimum 800x800px <br />
                                        JPG or PNG
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="column small-24 medium-18">
                        <div className="row">
                            <div className="column small-24 medium-12">
                                <label
                                    className="label--required"
                                    htmlFor="playlist-title"
                                >
                                    Playlist Name
                                </label>
                                <input
                                    type="text"
                                    className={titleInputClass}
                                    value={musicEdit.info.title || ''}
                                    name="title"
                                    id="playlist-title"
                                    onChange={this.props.onPlaylistInfoInput}
                                />
                            </div>
                            <div className="column small-24 medium-12">
                                <label
                                    className="label--required"
                                    htmlFor="playlist-genre"
                                >
                                    Genre
                                </label>
                                <Select
                                    className={selectClass}
                                    onChange={this.props.onPlaylistInfoInput}
                                    options={genreOptions}
                                    name="genre"
                                    id="playlist-genre"
                                    selectProps={genreProps}
                                />
                            </div>
                            <div className="column small-24">
                                <div>
                                    <label htmlFor="playlist-description">
                                        Playlist description
                                        <em>
                                            <span className="charsleft u-d-inline-block u-fs-12 u-text-gray-4">
                                                You have{' '}
                                                {
                                                    this.props
                                                        .descriptionCharsLeft
                                                }{' '}
                                                character
                                                {this.props
                                                    .descriptionCharsLeft === 1
                                                    ? ''
                                                    : 's'}{' '}
                                                left.
                                            </span>
                                        </em>
                                    </label>
                                    <textarea
                                        value={musicEdit.info.description || ''}
                                        name="description"
                                        className="edit__field edit__field--textarea u-margin-0"
                                        id="playlist-description"
                                        maxLength={
                                            this.props
                                                .playlistDescriptionMaxChars
                                        }
                                        onChange={
                                            this.props.onPlaylistInfoInput
                                        }
                                    />
                                </div>
                                <div className="u-spacing-top-20">
                                    <label className="u-d-inline-block u-spacing-right-em">
                                        <input
                                            type="radio"
                                            value="no"
                                            checked={
                                                !yesBool(musicEdit.info.private)
                                            }
                                            name="private"
                                            onChange={
                                                this.props.onPlaylistInfoInput
                                            }
                                        />
                                        Public - anyone can view
                                    </label>
                                    <label className="u-d-inline-block u-spacing-right-em">
                                        <input
                                            type="radio"
                                            value="yes"
                                            checked={yesBool(
                                                musicEdit.info.private
                                            )}
                                            name="private"
                                            onChange={
                                                this.props.onPlaylistInfoInput
                                            }
                                        />
                                        Private - only you can view
                                    </label>
                                    <button
                                        className="u-fw-700 u-text-red u-right"
                                        onClick={
                                            this.props.onPlaylistDeleteClick
                                        }
                                    >
                                        {deleteText}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.renderUrlInput(errorClass)}
                </div>
            </div>
        );
    }

    renderActionButtons() {
        const {
            musicEdit,
            finishingButtonText,
            finishButtonText,
            dirty,
            isFinishing,
            player,
            selectedItems
        } = this.props;

        if (!musicEdit.info.id) {
            return null;
        }

        const finishText = isFinishing ? finishingButtonText : finishButtonText;
        const disabledFinish = isFinishing || !dirty;

        return (
            <FixedActionBarContainer
                aboveAll={!!player.currentSong && !!selectedItems.length}
            >
                <button
                    onClick={this.props.onPlaylistSaveClick}
                    disabled={disabledFinish}
                    type="button"
                    className="button button--pill button--large u-tt-cap"
                >
                    {finishText}
                </button>
                <Link
                    to={`/playlist/${musicEdit.info.artist.url_slug}/${
                        musicEdit.info.url_slug
                    }`}
                    className="button button--pill button--black button--large u-tt-cap"
                >
                    Go to Playlist
                </Link>
                <p className="u-d-inline-block u-spacing-left-20">
                    <Link
                        className="edit__cancel"
                        to={`/playlist/${musicEdit.info.artist.url_slug}/${
                            musicEdit.info.url_slug
                        }`}
                    >
                        <CloseIcon />
                    </Link>
                </p>
            </FixedActionBarContainer>
        );
    }

    renderSortToolbar() {
        const { selectedItems, onClearSelectedItemsClick } = this.props;

        const label = selectedItems.length > 1 ? 'Tracks' : 'Track';

        const klass = classnames('sort-toolbar u-padding-x-30', {
            'sort-toolbar--active': selectedItems.length
        });

        return (
            <div className={klass}>
                <div className="sort-toolbar__inner">
                    <div className="sort-toolbar__count">
                        <p className="u-fw-600 u-ls-n-05">
                            <span className="u-text-white u-bg-orange u-lh-17">
                                {selectedItems.length}
                            </span>{' '}
                            {label} Selected
                        </p>
                    </div>
                    <div className="sort-toolbar__buttons u-text-center">
                        <button
                            className="sort-toolbar__button sort-toolbar__button--top"
                            title="Move to top"
                            data-tooltip="Move to top"
                            onClick={this.props.onMoveToTopClick}
                        >
                            <QueueNextIcon />
                        </button>
                        <button
                            className="sort-toolbar__button sort-toolbar__button--delete"
                            title="Delete"
                            data-tooltip="Delete"
                            onClick={this.props.onTrackRemoveClick}
                        >
                            <TrashIcon />
                        </button>
                        <button
                            className="sort-toolbar__button sort-toolbar__button--bottom"
                            title="Move to bottom"
                            data-tooltip="Move to bottom"
                            onClick={this.props.onMoveToBottomClick}
                        >
                            <QueueEndIcon />
                        </button>
                    </div>
                    <div className="sort-toolbar__clear-wrap u-text-right">
                        <button
                            className="sort-toolbar__button sort-toolbar__button--clear"
                            onClick={onClearSelectedItemsClick}
                            data-tooltip="Clear selected items"
                            data-tooltip-bottom-right
                            title="Clear selected items"
                        >
                            <CloseIcon />
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { musicEdit, selectedItems } = this.props;
        const trackList = (musicEdit.info.tracks || [])
            .filter((track) => {
                return !track.isDeleting;
            })
            .map((track, i) => {
                return (
                    <MusicEditTrackContainer
                        musicItem={musicEdit.info}
                        key={`${track.id}:${i}`}
                        index={i}
                        track={track}
                        onRemoveClick={this.props.onTrackRemoveClick}
                    />
                );
            });

        const draggingClass = classnames(
            'playlist-edit-track--dragging u-dragging',
            {
                'u-dragging--multiple': selectedItems.length
            }
        );

        return (
            <div className="edit edit--playlist">
                <EditableImageBanner
                    banner={musicEdit.info.image_banner}
                    onRemove={this.props.onBannerRemoveClick}
                    onApply={this.props.onBannerEditApply}
                    closeLink={getMusicUrl(musicEdit.info)}
                />
                <div className="row">
                    <div className="column small-24">
                        {this.renderHeader()}
                        {this.renderActionButtons()}
                        <DraggableList
                            containerProps={{
                                className: 'small-24 u-bg-white u-padding-x-20'
                            }}
                            dragHandle={
                                <span className="u-drag-handle u-drag-handle--playlist u-text-gray9">
                                    <DragHandle />
                                </span>
                            }
                            draggingClass={draggingClass}
                            onDragFinish={this.props.onTrackReorder}
                            items={trackList}
                            onRowHeight={this.handleRowHeight}
                            useWindowAsScrollContainer
                            multiSelect
                            selectedItems={this.props.selectedItems}
                            onItemSelect={this.props.onItemSelect}
                        />
                    </div>
                </div>
                {this.renderSortToolbar()}
            </div>
        );
    }
}
