import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import { debounce } from 'utils/index';
import {
    getUserUploads,
    nextPage,
    setQuery
} from '../redux/modules/user/uploads';
import { getPinned } from '../redux/modules/user/pinned';
import { savePinned } from '../redux/modules/user/pinned';
import { addToast } from '../redux/modules/toastNotification';
import requireAuth from '../hoc/requireAuth';
import PinEditPage from './PinEditPage';

class PinEditPageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        currentUserUploads: PropTypes.object,
        currentUserPinned: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            isSaving: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        if (
            prevProps.currentUserUploads.query &&
            prevProps.currentUserUploads.query.trim() !==
                this.props.currentUserUploads.query.trim()
        ) {
            this.performUploadSearch(this.props.currentUserUploads.query);
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTrackReorder = (fromIndex, toIndex) => {
        const { currentUserPinned, dispatch } = this.props;
        const newList = Array.from(currentUserPinned.list);
        const itemToMove = newList.splice(fromIndex, 1)[0];
        const autosave = false;

        newList.splice(toIndex, 0, itemToMove);

        dispatch(savePinned(newList, autosave));
    };

    handleSavePinsClick = () => {
        const { currentUserPinned, dispatch } = this.props;
        const newList = Array.from(currentUserPinned.list);

        this.setState({
            isSaving: true
        });

        return new Promise((resolve, reject) => {
            dispatch(savePinned(newList))
                .then(() => {
                    this.setState({
                        isSaving: false
                    });

                    resolve(this.state.isSaving);

                    dispatch(
                        addToast({
                            action: 'message',
                            message:
                                'Your highlighted items were successfully updated.'
                        })
                    );

                    return;
                })
                .catch((err) => {
                    console.error(err);

                    this.setState({
                        isSaving: false
                    });

                    dispatch(
                        addToast({
                            action: 'message',
                            message:
                                'There was a problem updating your highlighted items'
                        })
                    );
                    reject(err);
                });
        });
    };

    handleSearchFormSubmit = (e) => {
        e.preventDefault();
    };

    handleLoadMoreUploadsClick = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(
            getUserUploads({
                limit: 20,
                pinnable: true
            })
        );
    };

    handleUploadSearchInput = (e) => {
        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setQuery(val));
    };

    handleUploadTypeClick = (value) => {
        const { dispatch } = this.props;

        dispatch(
            getUserUploads({
                page: 1,
                limit: 20,
                show: value,
                pinnable: true
            })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    performUploadSearch = debounce((query) => {
        const { dispatch } = this.props;

        dispatch(
            getUserUploads({
                page: 1,
                query: query,
                limit: 20,
                pinnable: true
            })
        );
    }, 500);

    render() {
        return (
            <div className="edit-pins u-padding-y-80">
                <PinEditPage
                    currentUser={this.props.currentUser}
                    currentUserUploads={this.props.currentUserUploads}
                    currentUserPinned={this.props.currentUserPinned}
                    onLoadMoreUploadsClick={this.handleLoadMoreUploadsClick}
                    onUploadTypeClick={this.handleUploadTypeClick}
                    onUploadSearchInput={this.handleUploadSearchInput}
                    onSearchFormSubmit={this.handleSearchFormSubmit}
                    onTrackReorder={this.handleTrackReorder}
                    onSavePinsClick={this.handleSavePinsClick}
                    isSaving={this.state.isSaving}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        currentUserUploads: state.currentUserUploads,
        currentUserPinned: state.currentUserPinned,
        player: state.player
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(PinEditPageContainer, [
        () => getPinned(),
        () =>
            getUserUploads({
                limit: 20,
                pinnable: true
            })
    ])
);
