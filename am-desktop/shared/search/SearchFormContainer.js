import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { parse } from 'query-string';

import { searchInput } from '../redux/modules/search/results';
import { getSuggestions } from '../redux/modules/search/suggestions';

import SearchForm from './SearchForm';

class SearchFormContainer extends Component {
    static propTypes = {
        searchResults: PropTypes.object,
        searchSuggestions: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func,
        placeholder: PropTypes.string,
        id: PropTypes.string,
        account: PropTypes.bool,
        history: PropTypes.object
    };

    static defaultProps = {
        account: false
    };

    componentDidMount() {
        const query = parse(this.props.location.search);

        this.queryFromPageLoad = query.q;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();

        const input = e.currentTarget.querySelector('input');
        const query = input.value;

        input.blur();

        this.goToSearchPage(query);
    };

    handleSearchChange = (event, query) => {
        const { dispatch, account } = this.props;

        this.queryFromPageLoad = '';

        dispatch(searchInput(query));

        if (account) {
            console.log('No autocomplete for account search');
            return;
        }

        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(() => {
            const SPHINX_SEARCH_MIN_LENGTH = 2;

            if (!query || query.length < SPHINX_SEARCH_MIN_LENGTH) {
                return;
            }

            dispatch(getSuggestions(query));
        }, 200);
    };

    handleSearchSelect = (query) => {
        this.goToSearchPage(query);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    goToSearchPage = (query) => {
        if (!query) {
            return;
        }

        const { dispatch } = this.props;

        dispatch(searchInput(query));

        this.props.history.push(`/search?q=${encodeURIComponent(query)}`);
    };

    render() {
        const {
            location,
            searchResults,
            searchSuggestions,
            placeholder
        } = this.props;
        const { q } = parse(encodeURIComponent(location.search));
        const { query } = searchResults;
        const { list: suggestions } = searchSuggestions;

        return (
            <SearchForm
                placeholder={placeholder}
                search={query}
                query={q}
                suggestions={suggestions}
                id={this.props.id}
                onFormSubmit={this.handleFormSubmit}
                onSearchSelect={this.handleSearchSelect}
                onSearchChange={this.handleSearchChange}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        searchResults: state.searchResults,
        searchSuggestions: state.searchSuggestions
    };
}

export default withRouter(connect(mapStateToProps)(SearchFormContainer));
