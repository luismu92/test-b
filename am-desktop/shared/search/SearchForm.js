import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Autocomplete from 'react-autocomplete';
import classnames from 'classnames';

import SearchIcon from '../icons/search';

export default class SearchForm extends Component {
    static propTypes = {
        placeholder: PropTypes.string,
        search: PropTypes.string,
        query: PropTypes.string,
        id: PropTypes.string,
        suggestions: PropTypes.array,
        onFormSubmit: PropTypes.func,
        onSearchSelect: PropTypes.func,
        onSearchChange: PropTypes.func,
        onInputFocus: PropTypes.func,
        onInputBlur: PropTypes.func
    };

    static defaultProps = {
        placeholder: 'Search for artists, songs, albums!',
        suggestions: [],
        onFormSubmit() {},
        onSearchSelect() {},
        onSearchChange() {},
        onInputFocus() {},
        onInputBlur() {}
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getItemValue = (item) => {
        return item.text;
    };

    renderSearchItem = (item, isHighlighted) => {
        const klass = classnames('search-form__result', {
            'search-form__result--highlighted': isHighlighted
        });

        return (
            <div className={klass} key={`rsi${item.text}`} title={item.text}>
                <span className="search-form__result-icon">
                    <SearchIcon />
                </span>
                {item.text}
            </div>
        );
    };

    renderItems(items) {
        return items;

        // @todo once we have rich search results we can revisit the stuff below
        // const titles = {
        //     0: 'Artists',
        //     2: 'Songs',
        //     4: 'Albums',
        //     6: 'Playlists'
        // };

        // return items.map((item, index) => {
        //     // const text = item.props.children;
        //     const title = titles[index];

        //     if (title) {
        //         return [<div className="search-form__result search-form__result-title" key={`t${index}`}>{title}</div>, item];
        //     }

        //     return item;
        // });
    }

    renderInput = (props) => {
        return <input {...props} id={this.props.id} />;
    };

    renderMenu = (items, value, style) => {
        const { suggestions } = this.props;

        if (!suggestions.length) {
            return <div />;
        }

        style.top = 43;
        style.left = 0;

        return (
            <div
                className="search-form__autocomplete u-box-shadow u-scrollable"
                style={{ ...style }}
                children={this.renderItems(items)}
            />
        );
    };

    render() {
        const { placeholder, search, suggestions } = this.props;
        const inputProps = {
            className: 'search-form__input',
            name: 'q',
            required: true,
            minLength: 2,
            placeholder
        };
        const wrapperProps = {
            className: 'search-form__input-wrapper',
            style: {
                display: 'block'
            }
        };

        // @todo make search suggestions more rich with data
        // so we can populate the autocomplete menu appropriately
        const items = suggestions.map((item) => {
            return {
                text: item,
                type: 'artist'
            };
        });

        let label;

        if (this.props.id) {
            label = (
                <label
                    htmlFor={this.props.id}
                    style={{ position: 'absolute', top: -9999 }}
                >
                    Search
                </label>
            );
        }

        return (
            <form
                className="search-form"
                action="/search"
                method="get"
                onSubmit={this.props.onFormSubmit}
                autoComplete="off"
            >
                {label}
                <Autocomplete
                    autoHighlight={false}
                    wrapperProps={wrapperProps}
                    inputProps={inputProps}
                    value={search}
                    items={items}
                    getItemValue={this.getItemValue}
                    onSelect={this.props.onSearchSelect}
                    onChange={this.props.onSearchChange}
                    renderMenu={this.renderMenu}
                    renderItem={this.renderSearchItem}
                    renderInput={this.renderInput}
                />
                <button
                    type="submit"
                    className="search-form__submit"
                    value="Search"
                    aria-label="Submit search"
                >
                    <SearchIcon className="" />
                </button>
            </form>
        );
    }
}
