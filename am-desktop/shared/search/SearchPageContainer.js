import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import { buildSearchUrl, passiveOption } from 'utils/index';
import { getTagOptions } from 'utils/tags';
import connectDataFetchers from 'lib/connectDataFetchers';

import {
    getSearch,
    setQueryType,
    setQueryContext,
    setQueryGenre,
    setQueryTag,
    setVerified,
    setPage,
    nextPage,
    clearList
} from '../redux/modules/search/results';

import SearchPage from './SearchPage';

const SIDEBAR_GAP = 15;
const SCROLL_THRESHOLD = 300;
let displayPreviousLink;

class SearchPageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        history: PropTypes.object,
        searchResults: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        match: PropTypes.object,
        location: PropTypes.object,
        tags: PropTypes.object
    };

    constructor(props) {
        super(props);

        const initialPage =
            Math.max(parseInt(parse(props.location.search).page, 10), 1) || 1;

        if (typeof displayPreviousLink === 'undefined') {
            displayPreviousLink = initialPage > 1;
        }

        this.state = {
            filterBarMaxHeight: 1500,
            filterBarFixed: false
        };
    }

    //////////////////////
    // Lifecyle methods //
    //////////////////////

    componentDidMount() {
        const option = passiveOption();

        this.setInternalValues();
        window.addEventListener('scroll', this.handleWindowScroll, option);
        window.addEventListener('resize', this.handleWindowResize, option);
        this._searchSidebar = document.getElementById('search-filter');
        this._searchResults = document.getElementById('search-results');
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        window.removeEventListener('resize', this.handleWindowResize);
        window.removeEventListener('scroll', this.handleWindowScroll);
        this._scrollTimer = null;
        this._scrollLockPosition = null;
        this._searchSidebar = null;
        this._searchResults = null;
        displayPreviousLink = null;
        dispatch(clearList());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFilterChange = (e) => {
        const { dispatch, searchResults, history } = this.props;
        const {
            query,
            activeContext,
            activeGenre,
            tag,
            verified
        } = searchResults;
        const input = e.currentTarget;
        const filter = input.getAttribute('data-filter');

        let url = `/search?q=${encodeURIComponent(
            query
        )}&show=${filter}&sort=${activeContext}`;

        if (activeGenre) {
            url = `${url}&genre=${activeGenre}`;
        }

        if (tag) {
            url = `${url}&tag=${tag}`;
        }

        if (verified) {
            url = `${url}&verified=on`;
        }

        window.scrollTo(0, 0);
        history.push(url);

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setQueryType(filter));
        dispatch(getSearch());
    };

    handleSortClick = (e) => {
        const { dispatch, searchResults, history } = this.props;
        const { query, activeType, activeGenre, tag, verified } = searchResults;
        const input = e.currentTarget;
        const filter = input.getAttribute('data-sort');

        let url = `/search?q=${encodeURIComponent(
            query
        )}&show=${activeType}&sort=${filter}`;

        if (activeGenre) {
            url = `${url}&genre=${activeGenre}`;
        }

        if (tag) {
            url = `${url}&tag=${tag}`;
        }

        if (verified) {
            url = `${url}&verified=on`;
        }

        window.scrollTo(0, 0);
        history.push(url);

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setQueryContext(filter));
        dispatch(getSearch());
    };

    handleGenreChange = (text, e) => {
        const { dispatch, searchResults, history } = this.props;
        const {
            query,
            activeContext,
            activeType,
            tag,
            verified
        } = searchResults;
        const input = e.currentTarget;
        const genre = input.getAttribute('data-value');

        let url = `/search?q=${encodeURIComponent(
            query
        )}&show=${activeType}&sort=${activeContext}`;

        if (genre) {
            url = `${url}&genre=${genre}`;
        }

        if (tag) {
            url = `${url}&tag=${tag}`;
        }

        if (verified) {
            url = `${url}&verified=on`;
        }

        window.scrollTo(0, 0);
        history.push(url);

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setQueryGenre(genre));
        dispatch(getSearch());
    };

    handleTagChange = (text, e) => {
        const { dispatch, searchResults, history } = this.props;
        const {
            query,
            activeContext,
            activeType,
            activeGenre,
            tag,
            verified
        } = searchResults;
        const input = e.currentTarget;
        const newTag = input.getAttribute('data-value');

        let newQueryTag;

        let url = `/search?q=${encodeURIComponent(
            query
        )}&show=${activeType}&sort=${activeContext}`;

        if (newTag) {
            if (tag) {
                if (tag.includes(newTag)) {
                    newQueryTag = [...tag.split(',')]
                        .filter((t) => t !== newTag)
                        .join(',');
                } else {
                    newQueryTag = [...tag.split(','), newTag].join(',');
                }
            } else {
                newQueryTag = newTag;
            }

            url = `${url}&tag=${newQueryTag}`;
        }

        if (activeGenre) {
            url = `${url}&genre=${activeGenre}`;
        }

        if (verified) {
            url = `${url}&verified=on`;
        }

        window.scrollTo(0, 0);
        history.push(url);

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setQueryTag(newQueryTag));
        dispatch(getSearch());
    };

    handleVerifiedClick = (e) => {
        const { dispatch, searchResults, history } = this.props;
        const {
            query,
            activeType,
            activeContext,
            activeGenre,
            tag
        } = searchResults;
        const checked = e.currentTarget.checked;

        let url = `/search?q=${encodeURIComponent(
            query
        )}&show=${activeType}&sort=${activeContext}`;

        if (activeGenre) {
            url = `${url}&genre=${activeGenre}`;
        }

        if (tag) {
            url = `${url}&tag=${tag}`;
        }

        if (checked) {
            url = `${url}&verified=on`;
        }

        window.scrollTo(0, 0);
        history.push(url);

        dispatch(clearList());
        dispatch(setPage(1));
        dispatch(setVerified(checked));
        dispatch(getSearch());
    };

    handleWindowScroll = () => {
        const { searchResults } = this.props;
        const { loading, onLastPage } = searchResults;
        const scroll = window.pageYOffset;

        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (loading || onLastPage) {
                return;
            }

            if (
                document.body.clientHeight - (window.innerHeight + scroll) <
                SCROLL_THRESHOLD
            ) {
                this.fetchNextPage();
            }
        }, 200);
    };

    handleWindowResize = () => {
        this.setInternalValues();
    };

    handleFixedNavChange = (fixed) => {
        this.setState({
            filterBarFixed: fixed
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setInternalValues() {
        const { top, height } = document
            .getElementById('main-header')
            .getBoundingClientRect();

        this.setState({
            // Twice SIDEBAR_GAP to account for bottom padding
            filterBarMaxHeight:
                window.innerHeight - (top + height + SIDEBAR_GAP + SIDEBAR_GAP),
            windowWidth: window.innerWidth
        });
    }

    fetchNextPage = () => {
        const { dispatch, searchResults } = this.props;
        const {
            query,
            page,
            activeContext,
            activeType,
            activeGenre,
            verified
        } = searchResults;
        const url = buildSearchUrl(query, {
            type: activeType,
            genre: activeGenre,
            context: activeContext,
            page: page + 1,
            verified
        });

        dispatch(nextPage());
        dispatch(getSearch())
            .then((action) => {
                const hasNextPage = action.resolved.results.length;

                if (hasNextPage) {
                    // @todo figure out how to use react-router to replace url without
                    // refreshing this whole component. displayPreviousLink could be in
                    // state if router.replace(url) didnt refresh things.
                    window.history.replaceState(null, null, url);
                }
                return;
            })
            .catch((err) => console.log(err));
    };

    refetchIfNecessary(currentProps, nextProps) {
        const changedPage =
            parse(currentProps.location.search).page !==
            parse(nextProps.location.search).page;
        const changedSearch =
            parse(currentProps.location.search).q !==
            parse(nextProps.location.search).q;
        const changedGenre =
            parse(currentProps.location.search).genre !==
            parse(nextProps.location.search).genre;
        const changedTag =
            parse(currentProps.location.search).tag !==
            parse(nextProps.location.search).tag;
        const { dispatch } = currentProps;

        if (changedPage || changedSearch || changedGenre || changedTag) {
            dispatch(clearList());
            dispatch(setPage(parse(nextProps.location.search).page));
            dispatch(
                getSearch(parse(nextProps.location.search).q, {
                    genre: parse(nextProps.location.search).genre,
                    tag: parse(nextProps.location.search).tag
                })
            );
        }
    }

    render() {
        const {
            location,
            searchResults,
            currentUser,
            match,
            player,
            tags
        } = this.props;
        const {
            query,
            activeContext,
            activeType,
            list: results,
            activeGenre,
            tag,
            verified,
            loading,
            page,
            authenticatedArtist,
            verifiedArtist,
            verifiedTastemaker,
            verifiedPlaylist,
            tastemakerPlaylist,
            related
        } = searchResults;
        const totals = {
            music: searchResults.musicCount,
            artist: searchResults.artistCount,
            song: searchResults.songCount,
            album: searchResults.albumCount,
            playlist: searchResults.playlistCount
        };
        const { q } = parse(location.search);
        const filteredResults = results.filter(
            (result) => !result.geo_restricted
        );

        const { moods, subgenres } = getTagOptions(tags);
        const filterTagOptions = {
            ...subgenres,
            Mood: moods.filter((mood) => !!mood.value)
        };

        filterTagOptions.Default.text = 'Choose tags';

        return (
            <SearchPage
                filterBy={activeType}
                sortBy={activeContext}
                location={location}
                results={filteredResults}
                page={page}
                match={match}
                loading={loading}
                player={player}
                search={query}
                currentUser={currentUser}
                query={q || ''}
                onFilterChange={this.handleFilterChange}
                onSortClick={this.handleSortClick}
                onGenreChange={this.handleGenreChange}
                onTagChange={this.handleTagChange}
                totals={totals}
                genre={activeGenre}
                tag={tag}
                tagOptions={filterTagOptions}
                filterBarGap={SIDEBAR_GAP}
                filterBarMaxHeight={this.state.filterBarMaxHeight}
                onFilterFixChange={this.handleFixedNavChange}
                filterBarFixed={this.state.filterBarFixed}
                windowWidth={this.state.windowWidth}
                displayPreviousLink={displayPreviousLink}
                onVerifiedClick={this.handleVerifiedClick}
                verified={verified}
                authenticatedArtist={authenticatedArtist}
                verifiedArtist={verifiedArtist}
                verifiedTastemaker={verifiedTastemaker}
                verifiedPlaylist={verifiedPlaylist}
                tastemakerPlaylist={tastemakerPlaylist}
                related={related}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        searchResults: state.searchResults,
        player: state.player,
        ad: state.ad,
        tags: state.musicTags
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(SearchPageContainer, [
            (params, query) => setQueryType(query.show),
            (params, query) => setQueryContext(query.sort),
            (params, query) => setQueryGenre(query.genre),
            (params, query) => setQueryTag(query.tag),
            (params, query) => setPage(query.page),
            (params, query) =>
                setVerified(typeof query.verified !== 'undefined'),
            (params, query) => {
                if (!query.q && !query.genre && !query.tag) {
                    return null;
                }

                return getSearch(query.q, {
                    genre: query.genre,
                    tag: query.tag
                });
            }
        ])
    )
);
