import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { buildSearchUrl } from 'utils/index';
import { pluralize } from 'utils/string';
import SearchPageMeta from 'components/SearchPageMeta';

import ToggleSwitch from 'components/ToggleSwitch';
import Dropdown from '../components/Dropdown';

import {
    allGenresMap,
    liveGenres,
    SEARCH_CONTEXT_TYPE_RELEVANCE,
    SEARCH_CONTEXT_TYPE_RECENT,
    SEARCH_CONTEXT_TYPE_POPULAR,
    SEARCH_QUERY_TYPE_MUSIC,
    SEARCH_QUERY_TYPE_ARTISTS,
    SEARCH_QUERY_TYPE_SONGS,
    SEARCH_QUERY_TYPE_ALBUMS,
    SEARCH_QUERY_TYPE_PLAYLISTS
} from 'constants/index';

import MusicDetailContainer from '../browse/MusicDetailContainer';
import UserDetail from '../browse/UserDetail';
import UserCard from '../browse/UserCard';
import AndroidLoader from '../loaders/AndroidLoader';
import FixOnScroll from '../utils/FixOnScroll';

import SearchFormContainer from './SearchFormContainer';
// import ChartSponsoredAd from 'components/ad/ChartSponsoredAd';

import styles from './SearchPage.module.scss';

// Hardcoding this for now
const MEDIUM_BREAKPOINT = 640;

export default class SearchPage extends Component {
    static propTypes = {
        location: PropTypes.object,
        player: PropTypes.object,
        match: PropTypes.object,
        results: PropTypes.array,
        filterBy: PropTypes.string,
        sortBy: PropTypes.string,
        search: PropTypes.string,
        query: PropTypes.string,
        loading: PropTypes.bool,
        page: PropTypes.number,
        filterBarLeft: PropTypes.number,
        filterBarGap: PropTypes.number,
        windowWidth: PropTypes.number,
        filterBarFixed: PropTypes.bool,
        filterBarMaxHeight: PropTypes.number,
        onFilterChange: PropTypes.func,
        onSortClick: PropTypes.func,
        authenticatedArtist: PropTypes.object,
        verifiedArtist: PropTypes.object,
        verifiedTastemaker: PropTypes.object,
        verifiedPlaylist: PropTypes.object,
        tastemakerPlaylist: PropTypes.object,
        totals: PropTypes.object,
        onGenreChange: PropTypes.func,
        onFilterFixChange: PropTypes.func,
        onTagChange: PropTypes.func,
        genre: PropTypes.string,
        tag: PropTypes.string,
        tagOptions: PropTypes.object,
        onVerifiedClick: PropTypes.func,
        displayPreviousLink: PropTypes.bool,
        verified: PropTypes.bool,
        related: PropTypes.bool
    };

    static defaultProps = {
        onFilterChange() {},
        onSortClick() {},
        onGenreChange() {},
        onVerifiedClick() {}
    };

    renderCount(count) {
        if (count >= 1000) {
            return '1000+';
        }

        return count;
    }

    renderFilterMenu(
        filterBy,
        onFilterChange,
        counts,
        verified,
        onVerifiedClick
    ) {
        return (
            <Fragment>
                <div className="column small-8 medium-24 search-page__widget">
                    <h3 className="widget-title search__widget-title">
                        Filter results
                    </h3>
                    <ul>
                        <li>
                            <label className="widget__label">
                                <input
                                    type="radio"
                                    name="filter"
                                    className="radio--small"
                                    data-filter={SEARCH_QUERY_TYPE_MUSIC}
                                    onChange={onFilterChange}
                                    checked={
                                        filterBy === SEARCH_QUERY_TYPE_MUSIC
                                    }
                                />{' '}
                                All music{' '}
                                <span className="u-brand-color u-margin-0">
                                    <strong>
                                        ({this.renderCount(counts.music)})
                                    </strong>
                                </span>
                            </label>
                        </li>
                        <li>
                            <label className="widget__label">
                                <input
                                    type="radio"
                                    name="filter"
                                    className="radio--small"
                                    data-filter={SEARCH_QUERY_TYPE_PLAYLISTS}
                                    onChange={onFilterChange}
                                    checked={
                                        filterBy === SEARCH_QUERY_TYPE_PLAYLISTS
                                    }
                                />{' '}
                                Playlists{' '}
                                <span className="u-brand-color u-margin-0">
                                    <strong>
                                        ({this.renderCount(counts.playlist)})
                                    </strong>
                                </span>
                            </label>
                        </li>
                        <li>
                            <label className="widget__label">
                                <input
                                    type="radio"
                                    name="filter"
                                    className="radio--small"
                                    data-filter={SEARCH_QUERY_TYPE_ARTISTS}
                                    onChange={onFilterChange}
                                    checked={
                                        filterBy === SEARCH_QUERY_TYPE_ARTISTS
                                    }
                                />{' '}
                                Artists{' '}
                                <span className="u-brand-color u-margin-0">
                                    <strong>
                                        ({this.renderCount(counts.artist)})
                                    </strong>
                                </span>
                            </label>
                        </li>
                        <li>
                            <label className="widget__label">
                                <input
                                    type="radio"
                                    name="filter"
                                    className="radio--small"
                                    data-filter={SEARCH_QUERY_TYPE_SONGS}
                                    onChange={onFilterChange}
                                    checked={
                                        filterBy === SEARCH_QUERY_TYPE_SONGS
                                    }
                                />{' '}
                                Songs{' '}
                                <span className="u-brand-color u-margin-0">
                                    <strong>
                                        ({this.renderCount(counts.song)})
                                    </strong>
                                </span>
                            </label>
                        </li>
                        <li>
                            <label className="widget__label">
                                <input
                                    type="radio"
                                    name="filter"
                                    className="radio--small"
                                    data-filter={SEARCH_QUERY_TYPE_ALBUMS}
                                    onChange={onFilterChange}
                                    checked={
                                        filterBy === SEARCH_QUERY_TYPE_ALBUMS
                                    }
                                />{' '}
                                Albums{' '}
                                <span className="u-brand-color u-margin-0">
                                    <strong>
                                        ({this.renderCount(counts.album)})
                                    </strong>
                                </span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div className="column small-8 medium-24 search-page__verified-toggle">
                    <ToggleSwitch
                        className="u-d-flex u-d-flex--align-center u-d-flex--justify-between u-pos-relative u-margin-0 u-border-top u-border-bottom u-padding-top-10 u-padding-bottom-10"
                        label="Verified Only"
                        onChange={onVerifiedClick}
                        checked={verified}
                        showText
                    />
                </div>
            </Fragment>
        );
    }

    renderSortByMenu(activeContext, onSortClick) {
        const popularClass = classnames('vertical-menu__link', {
            'vertical-menu__link--current':
                activeContext === SEARCH_CONTEXT_TYPE_POPULAR
        });
        const relevantClass = classnames('vertical-menu__link', {
            'vertical-menu__link--current':
                activeContext === SEARCH_CONTEXT_TYPE_RELEVANCE
        });
        const recentClass = classnames('vertical-menu__link', {
            'vertical-menu__link--current':
                activeContext === SEARCH_CONTEXT_TYPE_RECENT
        });

        return (
            <div className="column small-8 medium-24 search-page__widget">
                <h3 className="widget-title search__widget-title">Sort by</h3>
                <ul className="vertical-menu">
                    <li>
                        <button
                            className={relevantClass}
                            onClick={onSortClick}
                            data-sort={SEARCH_CONTEXT_TYPE_RELEVANCE}
                        >
                            Most Relevant
                        </button>
                    </li>
                    <li>
                        <button
                            className={popularClass}
                            onClick={onSortClick}
                            data-sort={SEARCH_CONTEXT_TYPE_POPULAR}
                        >
                            Most Popular
                        </button>
                    </li>
                    <li>
                        <button
                            className={recentClass}
                            onClick={onSortClick}
                            data-sort={SEARCH_CONTEXT_TYPE_RECENT}
                        >
                            Most Recent
                        </button>
                    </li>
                </ul>
            </div>
        );
    }

    // eslint-disable-next-line
    renderResults(
        results = [],
        loading = false,
        authenticatedArtist,
        verifiedArtist,
        verifiedTastemaker,
        verifiedPlaylist,
        tastemakerPlaylist,
        filterBy
    ) {
        const list = results.map((result, i) => {
            const isUser = typeof result.bio !== 'undefined';

            if (isUser) {
                return (
                    <div
                        className="column small-24 medium-12 large-8 xlarge-6"
                        key={i}
                    >
                        <UserCard className="user-card--search" user={result} />
                    </div>
                );
            }

            return (
                <div className="u-spacing-bottom-60" key={i}>
                    <MusicDetailContainer
                        search
                        feed
                        shouldLinkArtwork
                        index={i}
                        item={result}
                        musicList={results}
                        hideWaveform={filterBy === SEARCH_QUERY_TYPE_PLAYLISTS}
                        // onItemClick={this.props.onItemClick}
                    />
                </div>
            );
        });

        if (verifiedPlaylist) {
            list.unshift(
                <div className="u-spacing-bottom-60" key="verifiedplaylist">
                    <MusicDetailContainer
                        search
                        feed
                        isVerified
                        shouldLinkArtwork
                        item={verifiedPlaylist}
                        musicList={results}
                        hideWaveform
                    />
                </div>
            );
        }

        if (!verifiedPlaylist && tastemakerPlaylist) {
            list.unshift(
                <div className="u-spacing-bottom-60" key="tastemakerplaylist">
                    <MusicDetailContainer
                        search
                        feed
                        isVerified
                        shouldLinkArtwork
                        key="tastemakerplaylist"
                        item={tastemakerPlaylist}
                        musicList={results}
                        hideWaveform
                    />
                </div>
            );
        }

        if (authenticatedArtist) {
            list.unshift(
                <UserDetail
                    key="authenticatedArtist"
                    showVerifiedLabel
                    user={authenticatedArtist}
                />
            );
        }

        if (verifiedArtist) {
            list.unshift(
                <UserDetail
                    key="verifiedArtist"
                    showVerifiedLabel
                    user={verifiedArtist}
                />
            );
        }

        if (!verifiedArtist && verifiedTastemaker) {
            list.unshift(
                <UserDetail
                    key="verifiedTastemaker"
                    showVerifiedLabel
                    user={verifiedTastemaker}
                />
            );
        }

        if (loading) {
            list.push(
                <div className="column small-24 u-text-center" key="loader">
                    <AndroidLoader className="music-feed__loader" />
                </div>
            );
        }

        if (!list.length) {
            return (
                <div className="empty-state">
                    <p>No results. Try another search.</p>
                    {/* <div key="sponsored-ad" className="music-detail u-clearfix" style={{ padding: 0 }}>
                        <ChartSponsoredAd />
                    </div> */}
                </div>
            );
        }

        // const sponsoredAd = (
        //     <div className="column small-24 u-text-center" key="sponsored-ad">
        //         <div className="music-detail u-clearfix" style={{ padding: 0 }}>
        //             <ChartSponsoredAd />
        //         </div>
        //     </div>
        // );

        // list.splice(3, 0, sponsoredAd);

        return list;
    }

    renderGenreMenu(activeGenre = '') {
        const options = liveGenres.map((type) => {
            return {
                value: type,
                text: allGenresMap[type]
            };
        });

        return (
            <div
                className={classnames(
                    'column small-8 medium-24 search-page__widget',
                    styles.genreFilter
                )}
            >
                <h3 className="widget-title search__widget-title">
                    Filter by Genre
                </h3>
                <Dropdown
                    options={options}
                    value={activeGenre}
                    onChange={this.props.onGenreChange}
                    className="c-dropdown--button c-dropdown--full"
                    buttonClassName="button button--pill"
                    menuClassName="c-dropdown__list--above tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
                />
            </div>
        );
    }

    renderTagMenu(tag = '') {
        const { tagOptions } = this.props;
        const tags = tag.split(',').filter(Boolean);

        return (
            <div
                className={classnames(
                    'column small-8 medium-24 search-page__widget',
                    styles.tagFilter
                )}
            >
                <h3 className="widget-title search__widget-title">
                    Filter by Tag
                </h3>
                <Dropdown
                    buttonClassName="button button--pill"
                    className="c-dropdown--button c-dropdown--full"
                    menuClassName="c-dropdown__list--above tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
                    multiple
                    onChange={this.props.onTagChange}
                    options={tagOptions}
                    sections
                    showDeselectButton
                    type="filter"
                    value={tags}
                />
            </div>
        );
    }

    renderPreviousLink(shouldDisplay, searchResults) {
        if (!shouldDisplay) {
            return null;
        }

        const {
            query,
            page,
            activeContext,
            activeType,
            activeGenre,
            verified,
            loading
        } = searchResults;
        const url = buildSearchUrl(query, {
            type: activeType,
            genre: activeGenre,
            context: activeContext,
            page: page - 1,
            verified
        });

        if (page === 1) {
            return null;
        }
        // const style = {
        //     position: 'relative',
        //     top: '-40px'
        // };

        return (
            <p>
                <Link to={`${url}`} disabled={loading}>
                    « Page {page - 1}
                </Link>
            </p>
        );
    }

    renderSearchResultHeader(search, totals, filterBy) {
        let count = totals.music;
        let noun = 'result';

        switch (filterBy) {
            case SEARCH_QUERY_TYPE_ARTISTS: {
                count = totals.artist;
                noun = 'artist';
                break;
            }

            case SEARCH_QUERY_TYPE_SONGS: {
                count = totals.song;
                noun = 'song';
                break;
            }

            case SEARCH_QUERY_TYPE_ALBUMS: {
                count = totals.album;
                noun = 'album';
                break;
            }

            case SEARCH_QUERY_TYPE_PLAYLISTS: {
                count = totals.playlist;
                noun = 'playlist';
                break;
            }

            default:
                break;
        }

        const stringOnly = true;
        const text = pluralize(count, noun, stringOnly);

        let inner;

        if (search) {
            inner = (
                <Fragment>
                    <strong>{count}</strong> {text} matching{' '}
                    <strong className="u-brand-color">"{search}"</strong>
                </Fragment>
            );
        }

        return <p className="search-page__search-title">{inner}</p>;
    }

    renderSearchHeader() {
        const { search, totals, filterBy, location, match } = this.props;

        return (
            <div className="search-page__search u-box-shadow">
                <div className="row">
                    <div className="column small-24">
                        {this.renderSearchResultHeader(
                            search,
                            totals,
                            filterBy
                        )}
                        <SearchFormContainer
                            match={match}
                            location={location}
                        />
                    </div>
                </div>
            </div>
        );
    }

    renderReplacementText() {
        const { search, related } = this.props;

        if (!related) {
            return null;
        }

        return (
            <div className="row u-text-center u-spacing-bottom-55 u-padding-x-130">
                <p className="u-fs-20 u-lh-16 u-ls-n-09">
                    <strong>
                        We don't have music matching{' '}
                        <span className="u-text-orange">"{search}"</span>
                    </strong>{' '}
                    but we have selected some great music from similar artists
                    you might enjoy. Discover something new!
                </p>
            </div>
        );
    }

    render() {
        const {
            loading,
            results,
            filterBy,
            sortBy,
            onFilterChange,
            onSortClick,
            totals,
            genre,
            tag,
            verified,
            onVerifiedClick,
            query,
            displayPreviousLink,
            page,
            filterBarGap,
            filterBarMaxHeight,
            filterBarFixed,
            authenticatedArtist,
            verifiedArtist,
            verifiedTastemaker,
            verifiedPlaylist,
            tastemakerPlaylist,
            windowWidth,
            onFilterFixChange
        } = this.props;
        const searchObject = {
            query,
            activeType: filterBy,
            activeGenre: genre,
            activeContext: sortBy,
            page,
            verified
        };
        const filterBarStyle = {};

        if (filterBarFixed) {
            filterBarStyle.maxHeight = filterBarMaxHeight;
        }

        const klass = classnames('search-page__content row', {
            'search-page__content--fixed': filterBarFixed
        });

        let resultList = this.renderResults(
            results,
            loading,
            authenticatedArtist,
            verifiedArtist,
            verifiedTastemaker,
            verifiedPlaylist,
            tastemakerPlaylist,
            filterBy
        );

        if (filterBy !== SEARCH_QUERY_TYPE_ARTISTS) {
            resultList = <div className="column small-24">{resultList}</div>;
        }

        return (
            <div className="search-page">
                <SearchPageMeta
                    query={query}
                    genre={genre}
                    type={filterBy}
                    page={page}
                    context={sortBy}
                    verified={verified}
                />
                {this.renderPreviousLink(displayPreviousLink, searchObject)}
                <div id="search-content" className={klass}>
                    <FixOnScroll
                        className="u-spacing-top search-page__filter column small-24 u-scrollable u-scrollable--fade-in"
                        fixedClassName=""
                        fixToId="main-header"
                        onFixChange={onFilterFixChange}
                        enabled={windowWidth >= MEDIUM_BREAKPOINT}
                        offsetGap={filterBarGap}
                        style={filterBarStyle}
                    >
                        <div className="u-box-shadow row">
                            {this.renderFilterMenu(
                                filterBy,
                                onFilterChange,
                                totals,
                                verified,
                                onVerifiedClick
                            )}
                            {this.renderSortByMenu(sortBy, onSortClick)}
                            {this.renderGenreMenu(genre)}
                            {this.renderTagMenu(tag)}
                        </div>
                    </FixOnScroll>
                    <div
                        id="search-results"
                        className="search-page__results u-spacing-top column medium-child-grow"
                    >
                        <div className="row expanded">
                            <div className="column">
                                {this.renderSearchHeader()}
                            </div>
                        </div>
                        <div className="search-results-wrap row expanded">
                            {this.renderReplacementText()}
                            {resultList}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
