import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import {
    convertTimeStringToSeconds,
    yesBool,
    clamp,
    getQueueIndexForSong
} from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';

import { getAlbumInfo, reset } from '../redux/modules/music/album';
import { getArtist } from '../redux/modules/artist/index';
import {
    getArtistUploads,
    setPage,
    nextPage
} from '../redux/modules/artist/uploads';
import { editQueue, play, seek } from '../redux/modules/player';
import {
    showModal,
    hideModal,
    MODAL_TYPE_AUTH,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import { fetchSongList as fetchTrending } from '../redux/modules/home/trending';
import { getFeatured } from '../redux/modules/featured';
import { getFeaturedPosts } from '../redux/modules/world/featured';

import RemovedPage from '../components/RemovedPage';
import AndroidLoader from '../loaders/AndroidLoader';
import AlbumPage from './AlbumPage';
import NotFound from '../NotFound';

class AlbumPageContainer extends Component {
    static propTypes = {
        album: PropTypes.object,
        artist: PropTypes.object,
        artistUploads: PropTypes.object,
        currentUser: PropTypes.object,
        featured: PropTypes.object,
        worldFeatured: PropTypes.object,
        homeTrending: PropTypes.object,
        match: PropTypes.object,
        location: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);

        const query = parse(props.location.search);

        this._query = {
            key: query.key,
            autoplay: yesBool(query.autoplay),
            seekTo: convertTimeStringToSeconds(query.t),
            trackIndex: clamp(parseInt(query.track, 10) || 1, 1, Infinity) - 1
        };
        this._autoplayStarted = false;
    }

    componentDidUpdate(prevProps) {
        const { dispatch, match } = prevProps;
        const currentParams = this.props.match.params;
        const changedArtistSlug =
            match.params.artistId !== currentParams.artistId &&
            currentParams.artistId;
        const changedAlbumSlug =
            match.params.albumSlug !== currentParams.albumSlug &&
            currentParams.albumSlug;
        const changedUserToken =
            this.props.currentUser.token &&
            prevProps.currentUser.token !== this.props.currentUser.token;
        const prevAlbumInfo = prevProps.album.info || {};
        const currentAlbumInfo = this.props.album.info || {};
        const changedAlbumId = prevAlbumInfo.id !== currentAlbumInfo.id;

        if (changedArtistSlug || changedAlbumSlug || changedUserToken) {
            dispatch(
                getAlbumInfo(currentParams.artistId, currentParams.albumSlug)
            );

            if (changedArtistSlug) {
                dispatch(getArtist(currentParams.artistId));
                dispatch(setPage(1));
                dispatch(
                    getArtistUploads(currentParams.artistId, {
                        limit: 5
                    })
                );
            }
        }

        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;

        if (
            changedAlbumId ||
            ((autoplay || seekToTime) &&
                !this._autoplayStarted &&
                this.props.album.info &&
                this.props.player.queue.length)
        ) {
            this.playFromQueryString(this.props.album.info);
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(reset());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoadMoreTracksClick = () => {
        const { dispatch, artist } = this.props;

        dispatch(nextPage());
        dispatch(getArtistUploads(artist.profile.url_slug, { limit: 5 }))
            .then((action) => {
                const newItems = action.resolved.results;

                if (newItems.length) {
                    const filtered = newItems.filter((upload) => {
                        return (
                            upload.type === 'song' || upload.type === 'album'
                        );
                    });

                    dispatch(editQueue(filtered, { append: true }));
                }
                return;
            })
            .catch((err) => console.error(err));
    };

    handleCensorSuccess = () => {
        const { dispatch, album, currentUser } = this.props;
        const { token, secret } = currentUser;

        dispatch(
            getAlbumInfo(album.info.uploader.url_slug, album.info.url_slug, {
                secret: secret,
                token: token
            })
        );
        dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    handleTakedownSuccess = () => {
        const { dispatch, album, currentUser } = this.props;
        const { token, secret } = currentUser;

        dispatch(
            getAlbumInfo(album.info.uploader.url_slug, album.info.url_slug, {
                secret: secret,
                token: token
            })
        );

        dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    handleClearStatsSuccess = () => {
        const { dispatch, album, currentUser } = this.props;

        const { token, secret } = currentUser;

        dispatch(hideModal(MODAL_TYPE_CONFIRM));

        dispatch(
            getAlbumInfo(album.info.artist.url_slug, album.info.url_slug, {
                secret: secret,
                token: token
            })
        );

        dispatch(
            addToast({
                type: album.info.type,
                action: 'feature',
                item: `${album.info.artist} - ${album.info.title}`
            })
        );
    };

    handleExcludeStatsSuccess = () => {
        const { dispatch, album, currentUser } = this.props;
        const { token, secret } = currentUser;

        dispatch(hideModal(MODAL_TYPE_CONFIRM));
        dispatch(
            getAlbumInfo(album.info.artist.url_slug, album.info.url_slug, {
                secret: secret,
                token: token
            })
        );
        dispatch(
            addToast({
                type: album.info.type,
                action: 'exclude-stats',
                item: `${album.info.artist} - ${album.info.title}`
            })
        );
    };

    handleCountdownFinish = () => {
        const {
            currentUser: { token, secret },
            dispatch,
            match: { params }
        } = this.props;
        const options = {};

        options.key = this._query.key;
        options.token = token;
        options.secret = secret;

        dispatch(getAlbumInfo(params.artistId, params.albumSlug, options));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    playFromQueryString(musicItem) {
        const { dispatch, player, artistUploads } = this.props;

        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;
        const trackIndex = this._query.trackIndex;

        if ((autoplay || seekToTime) && !this._autoplayStarted) {
            this._autoplayStarted = true;

            const tracks = musicItem.tracks || [];
            const clampedTrackIndex = clamp(trackIndex, 0, tracks.length - 1);
            let queueIndex = getQueueIndexForSong(musicItem, player.queue, {
                trackIndex: clampedTrackIndex,
                currentQueueIndex: player.queueIndex
            });

            if (queueIndex === -1) {
                const uploads = artistUploads.list.filter((item) => {
                    return item.id !== musicItem.id;
                });
                const queue = [musicItem].concat(uploads);

                const { queue: newQueue } = dispatch(editQueue(queue));

                queueIndex = getQueueIndexForSong(musicItem, newQueue, {
                    trackIndex: clampedTrackIndex,
                    currentQueueIndex: player.queueIndex
                });
            }

            dispatch(play(queueIndex))
                .then(() => {
                    if (seekToTime) {
                        dispatch(seek(seekToTime));
                    }
                    return;
                })
                .catch((err) => {
                    console.error(err);
                });
        }
    }

    showLoginModal() {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_AUTH, { type: 'login' }));
    }

    render() {
        const {
            album,
            player,
            artist,
            artistUploads,
            currentUser,
            homeTrending,
            featured,
            location,
            worldFeatured
        } = this.props;

        if (album.loading) {
            return (
                <div>
                    <AndroidLoader className="music-feed__loader" />
                </div>
            );
        }

        if (!album.info) {
            // @todo maybe put a skeleton view of the music detail item here?
            if (album.errors.length) {
                return <NotFound errors={album.errors} type="album" />;
            }

            return null;
        }

        if (
            album.info.status === 'suspended' ||
            album.info.status === 'takedown' ||
            album.info.status === 'unplayable'
        ) {
            return (
                <RemovedPage
                    musicType="album"
                    trending={homeTrending}
                    music={album.info}
                    dispatch={this.props.dispatch}
                    currentUser={this.props.currentUser}
                />
            );
        }

        return (
            <AlbumPage
                dispatch={this.props.dispatch}
                location={location}
                album={album}
                artist={artist}
                currentUser={currentUser}
                artistUploads={artistUploads}
                player={player}
                featured={featured}
                worldFeatured={worldFeatured}
                onLoadMoreTracksClick={this.handleLoadMoreTracksClick}
                onTakedownSuccess={this.handleTakedownSuccess}
                onCensorSuccess={this.handleCensorSuccess}
                onClearStatsSuccess={this.handleClearStatsSuccess}
                onExcludeSuccess={this.handleExcludeStatsSuccess}
                onCountdownFinish={this.handleCountdownFinish}
                songKey={this._query.key}
                singleCommentUuid={parse(this.props.location.search).comment}
                singleCommentThread={parse(this.props.location.search).thread}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        album: state.musicAlbum,
        admin: state.admin,
        artist: state.artist,
        artistUploads: state.artistUploads,
        player: state.player,
        homeTrending: state.homeTrending,
        featured: state.featured,
        errors: state.musicAlbum.errors,
        currentUser: state.currentUser,
        statsToken: state.stats.statsToken,
        worldFeatured: state.worldFeatured
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(AlbumPageContainer, [
        (params, query, props) => {
            const options = {};
            const {
                currentUser: { token, secret }
            } = props;

            options.key = query.key;
            options.token = token;
            options.secret = secret;

            return getAlbumInfo(params.artistId, params.albumSlug, options);
        },
        (params) => getArtist(params.artistId),
        () => setPage(1),
        (params) => getArtistUploads(params.artistId, { limit: 5 }),
        () => getFeatured(),
        () => fetchTrending(),
        () => getFeaturedPosts()
    ])
);
