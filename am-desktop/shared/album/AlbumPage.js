import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import MusicPageMeta from 'components/MusicPageMeta';
import { isCurrentMusicItem } from 'utils/index';
import analytics from 'utils/analytics';

import MusicDetailContainer from '../browse/MusicDetailContainer';
import BlogCarousel from '../components/BlogCarousel';
import CommentsWrapper from '../components/CommentsWrapper';
import MusicAlertContainer from '../components/MusicAlertContainer';

import MusicPageContent from '../browse/MusicPageContent';
import MusicInfo from '../browse/MusicInfo';

import { KIND_ALBUM } from 'constants/comment';

export default class AlbumPage extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        album: PropTypes.object,
        currentUser: PropTypes.object,
        location: PropTypes.object,
        worldFeatured: PropTypes.object,
        artistUploads: PropTypes.object,
        songKey: PropTypes.string,
        player: PropTypes.object,
        onTakedownSuccess: PropTypes.func,
        onCensorSuccess: PropTypes.func,
        onClearStatsSuccess: PropTypes.func,
        onCountdownFinish: PropTypes.func,
        onExcludeSuccess: PropTypes.func,
        singleCommentUuid: PropTypes.string,
        singleCommentThread: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            CopyrightControls: null,
            OwnerControlsContainer: null,
            AdminControlsContainer: null
        };
    }

    componentDidMount() {
        this.loadAdminModules(this.props)
            .then(({ CopyrightControls, OwnerControlsContainer }) => {
                this.setState({
                    CopyrightControls,
                    OwnerControlsContainer
                });
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    shouldShowInteractions(item) {
        if (item.released_offset > 0) {
            return false;
        }

        if (item.status === 'takedown') {
            return false;
        }

        if (item.status === 'suspended') {
            return false;
        }

        return true;
    }

    shouldShowPlayButton(item) {
        if (item.released_offset > 0) {
            return false;
        }

        if (item.status === 'takedown') {
            return false;
        }

        if (item.status === 'suspended') {
            return false;
        }

        if (item.status === 'unplayable') {
            return false;
        }

        return true;
    }

    isOwner() {
        const { currentUser, album } = this.props;

        return (
            currentUser.isLoggedIn &&
            album.info &&
            currentUser.profile.id === album.info.uploader.id
        );
    }

    loadAdminModules(props) {
        return new Promise((resolve) => {
            require.ensure([], (require) => {
                let OwnerControlsContainer = null;

                if (
                    (props.currentUser.isLoggedIn &&
                        props.currentUser.profile.id ===
                            props.album.info.uploader.id) ||
                    props.currentUser.isAdmin
                ) {
                    OwnerControlsContainer = require('../components/OwnerControlsContainer')
                        .default;
                }

                let CopyrightControls = null;

                if (
                    props.currentUser.isLoggedIn &&
                    props.currentUser.isSuspension
                ) {
                    CopyrightControls = require('../admin/CopyrightControls')
                        .default;
                }

                let AdminControlsContainer = null;

                if (props.currentUser.isAdmin) {
                    AdminControlsContainer = require('../admin/AdminControlsContainer')
                        .default;
                }

                resolve({
                    OwnerControlsContainer,
                    CopyrightControls,
                    AdminControlsContainer
                });
            });
        });
    }

    renderContent(albumInfo) {
        let content;

        if (
            albumInfo.status === 'takedown' ||
            albumInfo.status === 'suspended'
        ) {
            content = (
                <p>
                    This album has been removed due to a DMCA Complaint.
                    Discover your next favorite song on our{' '}
                    <Link to="/songs/week">Top Songs</Link>,{' '}
                    <Link to="/albums/week">Top Albums</Link> or{' '}
                    <Link to="/playlists/browse">Top Playlists Charts!</Link>
                </p>
            );
        }

        if (!content) {
            return null;
        }

        return (
            <div className="listen listen--album">
                <div className="listen__content">{content}</div>
            </div>
        );
    }

    render() {
        const {
            album,
            artistUploads,
            player,
            currentUser,
            onTakedownSuccess,
            onCensorSuccess,
            onClearStatsSuccess,
            onExcludeSuccess,
            worldFeatured,
            singleCommentThread,
            singleCommentUuid
        } = this.props;
        const isCurrentSong = isCurrentMusicItem(
            player.currentSong,
            album.info
        );
        const albumContent = this.renderContent(
            album.info,
            player,
            currentUser,
            isCurrentSong
        );

        let {
            CopyrightControls,
            OwnerControlsContainer,
            AdminControlsContainer
        } = this.state;

        if (CopyrightControls) {
            CopyrightControls = <CopyrightControls music={album.info} />;
        }

        if (OwnerControlsContainer) {
            OwnerControlsContainer = (
                <OwnerControlsContainer music={album.info} />
            );
        }

        if (AdminControlsContainer) {
            AdminControlsContainer = (
                <AdminControlsContainer
                    item={album.info}
                    onTakedownSuccess={onTakedownSuccess}
                    onCensorSuccess={onCensorSuccess}
                    onClearStatsSuccess={onClearStatsSuccess}
                    onExcludeSuccess={onExcludeSuccess}
                />
            );
        }

        const filteredArtistUploads = artistUploads.list.filter((item) => {
            return item.id !== album.info.id;
        });
        const musicList = [album.info].concat(filteredArtistUploads);

        return (
            <div className="music-page u-spacing-top-60">
                <MusicPageMeta
                    musicItem={album.info}
                    currentUser={currentUser}
                    location={this.props.location}
                />
                <div className="row u-spacing-bottom-30">
                    <div className="column small-24">
                        <MusicAlertContainer
                            dispatch={this.props.dispatch}
                            item={album.info}
                            currentUser={this.props.currentUser}
                            onCountdownFinish={this.props.onCountdownFinish}
                        />
                    </div>
                </div>
                <div className="row">
                    <div
                        className="column small-24"
                        style={{ padding: '0 var(--contentPadding)' }}
                    >
                        <div className="row expanded column small-24">
                            <MusicDetailContainer
                                item={album.info}
                                musicList={musicList}
                                onCountdownFinish={this.props.onCountdownFinish}
                                songKey={this.props.songKey}
                                hideLoadMoreTracksButton
                                hideInteractions
                                shouldFixOnScroll
                                large
                                allowZoom
                                removeHeaderLinks
                            />
                        </div>
                    </div>
                </div>
                <MusicPageContent
                    artist={album.info.uploader}
                    artistUploads={this.props.artistUploads}
                    item={album.info}
                >
                    <MusicInfo
                        currentUser={currentUser}
                        item={album.info}
                        artist={album.info.uploader}
                    />
                    <CommentsWrapper
                        item={album.info}
                        kind={KIND_ALBUM}
                        id={album.info.id}
                        total={album.info.stats.comments}
                        singleCommentUuid={singleCommentUuid}
                        singleCommentThread={singleCommentThread}
                    />
                </MusicPageContent>
                {albumContent}
                {CopyrightControls}
                {AdminControlsContainer}
                <BlogCarousel
                    featured={worldFeatured}
                    className="u-spacing-top-60"
                />
            </div>
        );
    }
}
