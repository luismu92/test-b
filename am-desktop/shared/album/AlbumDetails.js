import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { playlistGenreMap, DEFAULT_DATE_FORMAT } from 'constants/index';
import { renderFeaturingLinks } from 'utils/index';

import MusicStats from '../components/MusicStats';

import CheckIcon from '../icons/check-mark';

import moment from 'moment';

class AlbumDetails extends Component {
    static propTypes = {
        album: PropTypes.object.isRequired
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    friendlyGenre(genre) {
        return playlistGenreMap[genre];
    }

    renderFeaturing(featuring) {
        if (!featuring) {
            return null;
        }

        return (
            <li className="music__meta-featuring">
                <strong>{renderFeaturingLinks(featuring)}</strong>
            </li>
        );
    }

    renderProducer(producer) {
        if (!producer) {
            return null;
        }

        return (
            <li className="music__meta-producer">
                <strong>Producer:</strong> {producer}
            </li>
        );
    }

    renderReleased(released, uploader) {
        if (!released) {
            return null;
        }

        const date = moment(released * 1000).format(DEFAULT_DATE_FORMAT);

        let check;

        if (uploader.verified === 'yes') {
            check = (
                <span className="u-orange-check music__meta-check">
                    <CheckIcon title="Verified artist" />
                </span>
            );
        }

        return (
            <li className="music__meta-added">
                <strong>Release Date:</strong> {date} by{' '}
                <span className="music-meta__uploader u-d-inline-block u-pos-relative">
                    <Link to={`/artist/${uploader.url_slug}`}>
                        {uploader.name}
                    </Link>{' '}
                    {check}
                </span>
            </li>
        );
    }

    renderStats(stats) {
        if (!stats) {
            return null;
        }

        return <MusicStats stats={stats} />;
    }

    renderDetailText(detail) {
        if (!detail) {
            return null;
        }

        return <div className="music__subtext">{detail}</div>;
    }

    render() {
        const { album } = this.props;
        const uploader = album.info.uploader;

        return (
            <div className="listen__details-content">
                <h2 className="listen__heading music__heading u-text-center">
                    <span className="music__heading--artist u-d-block">
                        {album.info.artist}
                    </span>
                    <span className="music__heading--title u-d-block">
                        {album.info.title}
                    </span>
                </h2>

                <ul className="music__meta">
                    {this.renderFeaturing(album.info.featuring)}
                    {this.renderProducer(album.info.producer)}
                    {this.renderReleased(album.info.released, uploader)}
                </ul>

                {this.renderStats(album.info.stats)}
            </div>
        );
    }
}

export default AlbumDetails;
