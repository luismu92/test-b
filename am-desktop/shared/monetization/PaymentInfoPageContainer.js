import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';

import connectDataFetchers from 'lib/connectDataFetchers';

import requireAuth from '../hoc/requireAuth';
import { getPaymentInfo } from '../redux/modules/monetization/paymentInfo';

import PaymentInfoPage from './PaymentInfoPage';

class PaymentInfoPageContainer extends Component {
    static propTypes = {
        monetizationPaymentInfo: PropTypes.object,
        location: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            iframeHeight: null
        };
    }

    componentDidMount() {
        document.body.classList.add('light');
        window.addEventListener('message', this.handleWindowMessage, false);
    }

    componentWillUnmount() {
        document.body.classList.remove('light');
        window.removeEventListener('message', this.handleWindowMessage);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowMessage = (e) => {
        if (e.data && e.data.TipaltiIframeInfo) {
            const { height } = e.data.TipaltiIframeInfo;

            this.setState({
                iframeHeight: height
            });
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { location } = this.props;
        let backPage = 'dashboard';

        if (parse(location.search).ref === 'monetization') {
            backPage = 'monetization';
        }

        return (
            <PaymentInfoPage
                monetizationPaymentInfo={this.props.monetizationPaymentInfo}
                backPage={backPage}
                iframeHeight={this.state.iframeHeight}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        monetizationPaymentInfo: state.monetizationPaymentInfo
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return (
                    currentUser.isLoggedIn && currentUser.profile.label_owner
                );
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(connectDataFetchers(PaymentInfoPageContainer, [() => getPaymentInfo()]));
