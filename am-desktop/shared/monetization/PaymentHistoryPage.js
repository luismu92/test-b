import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import AndroidLoader from '../loaders/AndroidLoader';
import BarChartIcon from '../icons/bar-chart';

export default class PaymentHistoryPage extends Component {
    static propTypes = {
        monetizationPaymentInfo: PropTypes.object,
        iframeHeight: PropTypes.number
    };

    render() {
        const { monetizationPaymentInfo, iframeHeight } = this.props;

        if (monetizationPaymentInfo.error) {
            return <p>${monetizationPaymentInfo.error}</p>;
        }

        if (!monetizationPaymentInfo.info || monetizationPaymentInfo.loading) {
            return (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <Fragment>
                <Helmet>
                    <title>Payment History on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <section className="row u-spacing-bottom u-spacing-top">
                    <div className="column">
                        <div className="u-box-shadow u-left-right u-padding-em align-middle">
                            <h2>
                                <BarChartIcon className="u-text-icon u-text-orange" />
                                Payment History
                            </h2>
                        </div>
                    </div>
                </section>
                <section className="row">
                    <div className="column">
                        <iframe
                            title="Payment History"
                            src={monetizationPaymentInfo.info.history}
                            frameBorder="0"
                            width="100%"
                            height="100%"
                            className="payment-iframe"
                            style={{ height: iframeHeight }}
                        />
                    </div>
                </section>
            </Fragment>
        );
    }
}
