import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import requireAuth from '../hoc/requireAuth';
import AmpCodes from './AmpCodes';
import { updateMetadata } from '../redux/modules/music/index';
import { replaceItem } from '../redux/modules/user/uploads';
import { replaceAssociatedItem } from '../redux/modules/monetization/associatedMusic';
import { hideModal, MODAL_TYPE_CONFIRM } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import analytics from 'utils/analytics';

class AmpCodesContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onFormSubmit: PropTypes.func,
        item: PropTypes.object,
        contextType: PropTypes.string
    };

    static defaultProps = {
        onFormSubmit() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            inputFields: {
                video_ad: this.props.item.video_ad,
                accounting_code: this.props.item.accounting_code,
                isrc: this.props.item.isrc,
                upc: this.props.item.upc
            }
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleInputChange = (e) => {
        const input = e.currentTarget;
        const inputName = input.name;
        let inputValue = input.value;

        switch (input.type) {
            case 'checkbox': {
                inputValue = input.checked ? '1' : '';
                break;
            }

            case 'select-one': {
                const value = input.options[input.selectedIndex].value;

                inputValue = value;
                break;
            }

            default: {
                break;
            }
        }

        this.setState((prevState) => {
            return {
                inputFields: {
                    ...prevState.inputFields,
                    [inputName]: inputValue
                }
            };
        });
    };

    handleFormSubmit = (e, contextType) => {
        e.preventDefault();

        const { dispatch } = this.props;

        dispatch(updateMetadata(this.props.item.id, this.state.inputFields))
            .then((ret) => {
                if (contextType === 'uploads') {
                    dispatch(replaceItem(ret.resolved));
                } else if (contextType === 'associated') {
                    dispatch(replaceAssociatedItem(ret.resolved));
                }

                this.props.onFormSubmit();
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));

                const ERROR_MUSIC_AMP_FORBIDDEN = 3039;
                let errorMessage = '';
                let messageDuration;

                if (error.errorcode === ERROR_MUSIC_AMP_FORBIDDEN) {
                    errorMessage = `Please contact ${
                        error.errors[0]
                    } to change this field or contact contentops@audiomack.com.`;
                    messageDuration = 20000;
                } else {
                    errorMessage =
                        'There was a problem saving codes for this item.';
                }

                dispatch(
                    addToast(
                        {
                            action: 'message',
                            item: errorMessage
                        },
                        messageDuration
                    )
                );
                analytics.error(error);
                console.log(error);
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <AmpCodes
                inputFields={this.state.inputFields}
                onInputChange={this.handleInputChange}
                onFormSubmit={this.handleFormSubmit}
                contextType={this.props.contextType}
            />
        );
    }
}

function mapStateToProps() {
    return {};
}

export default requireAuth(connect(mapStateToProps)(AmpCodesContainer));
