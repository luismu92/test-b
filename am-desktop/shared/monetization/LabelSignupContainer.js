import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { signup } from '../redux/modules/monetization/label';
import { getSummary } from '../redux/modules/monetization/summary';

import LabelSignup from './LabelSignup';

class LabelSignupContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        inviteCode: PropTypes.string,
        shouldUpdate: PropTypes.bool,
        ignoreTos: PropTypes.bool,
        monetizationLabel: PropTypes.object,
        onFormSubmit: PropTypes.func,
        submitButtonIcon: PropTypes.object,
        submitButtonText: PropTypes.string,
        submitButtonClass: PropTypes.string
    };

    static defaultProps = {
        onFormSubmit() {},
        shouldUpdate: false
    };

    constructor(props) {
        super(props);

        this.state = {
            inputFields: {
                first_name: '',
                last_name: '',
                company_name: '',
                isIndividual: true,
                agree: ''
            }
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleInputChange = (e) => {
        const input = e.currentTarget;
        const inputName = input.name;
        let inputValue = input.value;

        switch (input.type) {
            case 'checkbox': {
                inputValue = input.checked ? '1' : '';
                break;
            }

            case 'radio': {
                inputValue = input.value === 'true';
                break;
            }

            case 'select-one': {
                const value = input.options[input.selectedIndex].value;

                inputValue = value;
                break;
            }

            default: {
                break;
            }
        }

        this.setState((prevState) => {
            return {
                inputFields: {
                    ...prevState.inputFields,
                    [inputName]: inputValue
                }
            };
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, inviteCode, shouldUpdate, ignoreTos } = this.props;
        const data = {
            ...this.state.inputFields,
            type: this.state.inputFields.isIndividual
                ? 'individual'
                : 'company',
            update: shouldUpdate
        };

        if (inviteCode) {
            data.code = inviteCode;
        }

        if (ignoreTos) {
            data.agree = '1';
        }

        dispatch(signup(data))
            .then(() => {
                dispatch(getSummary());
                this.props.onFormSubmit(e);
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <LabelSignup
                inputFields={this.state.inputFields}
                ignoreTos={this.props.ignoreTos}
                onInputChange={this.handleInputChange}
                onFormSubmit={this.handleFormSubmit}
                loading={this.props.monetizationLabel.signupIsLoading}
                error={this.props.monetizationLabel.signupError}
                inviteCode={this.props.inviteCode}
                submitButtonIcon={this.props.submitButtonIcon}
                submitButtonText={this.props.submitButtonText}
                submitButtonClass={this.props.submitButtonClass}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        monetizationLabel: state.monetizationLabel
    };
}

export default connect(mapStateToProps)(LabelSignupContainer);
