/* eslint react/jsx-no-bind: 0 */ // --> OFF
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { yesBool } from 'utils/index';

export default class AmpCodes extends Component {
    static propTypes = {
        onInputChange: PropTypes.func,
        onFormSubmit: PropTypes.func,
        inputFields: PropTypes.object,
        contextType: PropTypes.string // uploads || associated
    };

    static defaultProps = {
        contextType: 'uploads'
    };

    submitForm(e, contextType) {
        this.props.onFormSubmit(e, contextType);
    }

    render() {
        const { inputFields, onInputChange, contextType } = this.props;

        return (
            <form
                className="row"
                onSubmit={(e) => this.submitForm(e, contextType)}
            >
                <div className="column small-24">
                    <label htmlFor="video_ad">Monetization enabled?</label>
                    <input
                        id="video_ad"
                        required
                        type="checkbox"
                        name="video_ad"
                        checked={yesBool(inputFields.video_ad)}
                        onChange={onInputChange}
                        disabled={yesBool(inputFields.video_ad)}
                    />
                </div>
                <div className="column small-24">
                    <label htmlFor="accounting_code">Accounting Code</label>
                    <input
                        id="accounting_code"
                        type="text"
                        value={inputFields.accounting_code}
                        name="accounting_code"
                        onChange={onInputChange}
                    />
                </div>
                <div className="column small-24">
                    <label htmlFor="isrc">ISRC</label>
                    <input
                        id="isrc"
                        type="text"
                        value={inputFields.isrc}
                        name="isrc"
                        onChange={onInputChange}
                    />
                </div>
                <div className="column small-24">
                    <label htmlFor="upc">UPC</label>
                    <input
                        id="upc"
                        type="text"
                        value={inputFields.upc}
                        name="upc"
                        onChange={onInputChange}
                    />
                </div>
                <div className="column small-24">
                    <input
                        required
                        type="submit"
                        className="button"
                        value="Save"
                    />
                </div>
            </form>
        );
    }
}
