import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import requireAuth from '../hoc/requireAuth';
import { getPaymentInfo } from '../redux/modules/monetization/paymentInfo';

import PaymentHistoryPage from './PaymentHistoryPage';

class PaymentHistoryPageContainer extends Component {
    static propTypes = {
        monetizationPaymentInfo: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            iframeHeight: null
        };
    }

    componentDidMount() {
        document.body.classList.add('light');
        window.addEventListener('message', this.handleWindowMessage, false);
    }

    componentWillUnmount() {
        document.body.classList.remove('light');
        window.removeEventListener('message', this.handleWindowMessage);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowMessage = (e) => {
        if (e.data && e.data.TipaltiIframeInfo) {
            const { height } = e.data.TipaltiIframeInfo;

            this.setState({
                iframeHeight: height
            });
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <PaymentHistoryPage
                monetizationPaymentInfo={this.props.monetizationPaymentInfo}
                iframeHeight={this.state.iframeHeight}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        monetizationPaymentInfo: state.monetizationPaymentInfo
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return (
                    currentUser.isLoggedIn && currentUser.profile.label_owner
                );
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(connectDataFetchers(PaymentHistoryPageContainer, [() => getPaymentInfo()]));
