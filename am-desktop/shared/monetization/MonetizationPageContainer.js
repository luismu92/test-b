import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import { showModal, MODAL_TYPE_LABEL_SIGNUP } from '../redux/modules/modal';

import requireAuth from '../hoc/requireAuth';
import { getSummary } from '../redux/modules/monetization/summary';
import { getMonthlyRevenue } from '../redux/modules/monetization/monthlyRevenue';
import { getDailyRevenue } from '../redux/modules/monetization/dailyRevenue';
import { getPaymentInfo } from '../redux/modules/monetization/paymentInfo';
import { showTopAd, hideTopAd } from '../redux/modules/global';

import LabelSignupModal from '../modal/LabelSignupModal';
import MonetizationPage from './MonetizationPage';

class MonetizationPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        match: PropTypes.object,
        history: PropTypes.object,
        monetizationSummary: PropTypes.object,
        monetizationDailyRevenue: PropTypes.object,
        monetizationPaymentInfo: PropTypes.object,
        monetizationMonthlyRevenue: PropTypes.object
    };

    constructor(props) {
        super(props);

        const { month: monthStr, year: yearStr } = props.match.params;
        const startYear = new Date(
            props.currentUser.profile.created * 1000
        ).getFullYear();
        let month = parseInt(monthStr, 10) - 1;
        let year = parseInt(yearStr, 10);

        if (isNaN(month) || month < 0) {
            month = new Date().getMonth();
        }

        if (isNaN(year) || year < startYear) {
            year = new Date().getFullYear();
        }

        this.state = {
            ampSummaryMonth: month,
            ampSummaryYear: year,
            ampRevenueMonth: month,
            ampRevenueYear: year,
            paymentMessageOpen: true
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(hideTopAd());
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(showTopAd());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAmpRevenueChange = (text, e) => {
        const { dispatch, history } = this.props;
        const name = e.currentTarget.getAttribute('data-name');
        const value = e.currentTarget.getAttribute('data-value');
        const date = new Date();

        let year = this.state.ampSummaryYear;
        let month = this.state.ampSummaryMonth;
        // Months in JS are zero index based while the API is not
        let monthForRequest = month + 1;

        if (name === 'year') {
            year = parseInt(value, 10);

            if (year === date.getFullYear() && month > date.getMonth()) {
                month = date.getMonth();
                monthForRequest = month + 1;
            }

            this.setState({
                ampSummaryMonth: month,
                ampRevenueMonth: month,
                ampRevenueYear: year,
                ampSummaryYear: year
            });

            dispatch(
                getSummary({
                    month: monthForRequest,
                    year: year
                })
            );
            dispatch(
                getMonthlyRevenue({
                    month: monthForRequest,
                    year: year
                })
            );
            dispatch(
                getDailyRevenue({
                    month: monthForRequest,
                    year: year
                })
            );
        } else if (name === 'month') {
            month = parseInt(value, 10);

            if (year === date.getFullYear() && month > date.getMonth()) {
                year -= 1;
            }

            monthForRequest = month + 1;

            this.setState({
                ampSummaryMonth: month,
                ampRevenueMonth: month,
                ampRevenueYear: year,
                ampSummaryYear: year
            });

            dispatch(
                getSummary({
                    month: monthForRequest,
                    year: year
                })
            );

            dispatch(
                getMonthlyRevenue({
                    month: monthForRequest,
                    year: year
                })
            );

            dispatch(
                getDailyRevenue({
                    month: monthForRequest,
                    year: year
                })
            );
        }

        history.replace({
            pathname: `/monetization/${year}/${monthForRequest}`
        });
    };

    handleCsvClick = () => {
        const { dispatch } = this.props;

        dispatch(
            getMonthlyRevenue({
                month: this.state.ampRevenueMonth + 1,
                year: this.state.ampRevenueYear,
                csv: true
            })
        )
            .then((action) => {
                const status = action.resolved.status;

                if (status !== 200) {
                    throw new Error(
                        `Expected CSV status to be 200 but got ${status}`
                    );
                }

                return action.resolved.blob();
            })
            .then((b) => {
                const blob = b.slice(0, b.size, {
                    type: 'text/csv;charset=utf-8;'
                });

                // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
                if (window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, 'report.csv');
                    return;
                }

                const a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(blob);
                a.download = 'report.csv';

                // FF requires link to be in DOM
                // related https://github.com/audiomack/audiomack-js/issues/513
                document.body.appendChild(a);
                // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
                a.click();
                document.body.removeChild(a);
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handlePaymentCloseClick = () => {
        this.setState({
            paymentMessageOpen: false
        });
    };

    handleLabelSignup = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <Fragment>
                <MonetizationPage
                    currentUser={this.props.currentUser}
                    monetizationSummary={this.props.monetizationSummary}
                    monetizationDailyRevenue={
                        this.props.monetizationDailyRevenue
                    }
                    monetizationMonthlyRevenue={
                        this.props.monetizationMonthlyRevenue
                    }
                    monetizationPaymentInfo={this.props.monetizationPaymentInfo}
                    ampSummaryYear={this.state.ampSummaryYear}
                    ampSummaryMonth={this.state.ampSummaryMonth}
                    ampRevenueMonth={this.state.ampRevenueMonth}
                    ampRevenueYear={this.state.ampRevenueYear}
                    paymentMessageOpen={this.state.paymentMessageOpen}
                    onAmpRevenueChange={this.handleAmpRevenueChange}
                    onPaymentCloseClick={this.handlePaymentCloseClick}
                    onCsvClick={this.handleCsvClick}
                    onLabelSignup={this.handleLabelSignup}
                />
                <LabelSignupModal />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        monetizationSummary: state.monetizationSummary,
        monetizationDailyRevenue: state.monetizationDailyRevenue,
        monetizationMonthlyRevenue: state.monetizationMonthlyRevenue,
        monetizationPaymentInfo: state.monetizationPaymentInfo
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return (
                    currentUser.isLoggedIn && currentUser.profile.label_owner
                );
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(
    connectDataFetchers(MonetizationPageContainer, [
        () => getPaymentInfo(),
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            const month = parseInt(params.month, 10);
            const year = parseInt(params.year, 10);

            return getSummary({
                month: isNaN(month) ? undefined : month,
                year: isNaN(year) ? undefined : year
            });
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            const month = parseInt(params.month, 10);
            const year = parseInt(params.year, 10);

            return getDailyRevenue({
                month: isNaN(month) ? undefined : month,
                year: isNaN(year) ? undefined : year
            });
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            const month = parseInt(params.month, 10);
            const year = parseInt(params.year, 10);

            return getMonthlyRevenue({
                month: isNaN(month) ? undefined : month,
                year: isNaN(year) ? undefined : year
            });
        }
    ])
);
