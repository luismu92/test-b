import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import AmpCodesList from './AmpCodesList';

import Button from '../buttons/Button';
import AmMark from '../icons/am-mark';
import AndroidLoader from '../loaders/AndroidLoader';

import styles from './AmpCodeGenerator.module.scss';

export default function AmpCodeGenerator({
    monetizationAmpCode,
    onGenerateClick,
    onPrefixChange,
    onCodeFocusClick,
    onLoadMoreClick
}) {
    const { generated } = monetizationAmpCode;
    const { code, loading, error } = generated;

    let errorMessage;
    if (error) {
        errorMessage = (
            <p className="u-text-center u-spacing-top-20 u-fw-600">
                There was an error generating the AMP code
            </p>
        );
    }

    let loader;
    if (loading) {
        loader = (
            <div className="u-text-center">
                <AndroidLoader size={30} />
            </div>
        );
    }

    let generatedCode;
    if (code) {
        const codeUrl = `${process.env.AM_URL}/amp?ampCode=${code}`;
        generatedCode = (
            <Fragment>
                <input
                    className={styles.code}
                    type="text"
                    value={codeUrl}
                    onFocus={onCodeFocusClick}
                    readOnly
                />
                <p className={styles.instruction} style={{ marginTop: '1em' }}>
                    Copy this link and send to the artist
                </p>
            </Fragment>
        );
    }
    return (
        <Fragment>
            <div className="amp-banner" style={{ height: 240 }}>
                <AmMark className="amp-banner__logo" />
                <div className="row amp-banner__text">
                    <h1 className="column small-24">Generate an AMP code</h1>
                </div>
            </div>
            <div className="row" style={{ marginTop: '-78px' }}>
                <div className="column">
                    <div
                        className="u-box-shadow"
                        style={{ padding: '40px 20px' }}
                    >
                        <div className={styles.buttonWrap}>
                            <input
                                className={styles.prefix}
                                type="text"
                                placeholder="AMP Code Prefix (Optional)"
                                onChange={onPrefixChange}
                            />
                            <Button
                                width={200}
                                height={45}
                                text="Generate Code"
                                onClick={onGenerateClick}
                                disabled={loading}
                            />
                        </div>
                        <div className="u-text-center">
                            {generatedCode}
                            {loader}
                            {errorMessage}
                        </div>
                        <div style={{ marginTop: 30 }}>
                            <AmpCodesList
                                codes={monetizationAmpCode}
                                onLoadMoreClick={onLoadMoreClick}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

AmpCodeGenerator.propTypes = {
    monetizationAmpCode: PropTypes.object,
    onGenerateClick: PropTypes.func,
    onPrefixChange: PropTypes.func,
    onCodeFocusClick: PropTypes.func,
    onLoadMoreClick: PropTypes.func
};
