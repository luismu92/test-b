import React, { Component, Fragment } from 'react';
// import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

// import AndroidLoader from '../loaders/AndroidLoader';
import BarChartIcon from '../icons/bar-chart';

export default class RoyaltiesPage extends Component {
    static propTypes = {};

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Publishing Royalties</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <section className="row u-spacing-bottom u-spacing-top">
                    <div className="column">
                        <div className="u-box-shadow u-left-right u-padding-em align-middle">
                            <h2>
                                <BarChartIcon className="u-text-icon u-text-orange" />
                                Publishing Royalties (SongTrust)
                            </h2>
                        </div>
                    </div>
                </section>
                <section className="row">
                    <div className="column">
                        <button className="button st-trigger">
                            Publish Your Songs
                        </button>
                    </div>
                </section>
            </Fragment>
        );
    }
}
