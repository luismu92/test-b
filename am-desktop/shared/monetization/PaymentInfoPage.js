import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import AndroidLoader from '../loaders/AndroidLoader';
import BarChartIcon from '../icons/bar-chart';

export default class PaymentInfoPage extends Component {
    static propTypes = {
        monetizationPaymentInfo: PropTypes.object,
        iframeHeight: PropTypes.number
    };

    render() {
        const { monetizationPaymentInfo, iframeHeight } = this.props;

        let content = (
            <section className="row">
                <div className="column">
                    <iframe
                        title="Payment details"
                        src={monetizationPaymentInfo.info.payments}
                        frameBorder="0"
                        width="100%"
                        height="100%"
                        className="payment-iframe"
                        style={{ height: iframeHeight }}
                    />
                </div>
            </section>
        );

        if (monetizationPaymentInfo.error) {
            content = (
                <p className="u-text-center">
                    {monetizationPaymentInfo.error.message}
                </p>
            );
        }

        if (!monetizationPaymentInfo.info || monetizationPaymentInfo.loading) {
            content = (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <Fragment>
                <Helmet>
                    <title>Payment Details on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <section className="row u-spacing-bottom u-spacing-top">
                    <div className="column">
                        <div className="u-box-shadow u-left-right u-padding-em align-middle">
                            <h2>
                                <BarChartIcon className="u-text-icon u-text-orange" />
                                Payment Info
                            </h2>
                        </div>
                    </div>
                </section>
                {content}
            </Fragment>
        );
    }
}
