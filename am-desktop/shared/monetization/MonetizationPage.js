import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
    humanizeNumber,
    formatCurrency,
    parseRevenueString
} from 'utils/index';
import { months } from 'constants/index';
import { eventLabel } from 'utils/analytics';

import PlayIcon from '../icons/play';
import DollarIcon from '../icons/dollar';
import BarChartIcon from '../icons/bar-chart';
import AndroidLoader from '../loaders/AndroidLoader';
import TunecoreAd from '../components/Tunecore';
import Dropdown from '../components/Dropdown';
import ConfirmMessage from '../components/ConfirmMessage';
import BarChart from '../components/BarChart';
import Table from '../components/Table';
import StatBlock from '../stats/StatBlock';

export default class MonetizationPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        monetizationSummary: PropTypes.object,
        monetizationDailyRevenue: PropTypes.object,
        monetizationMonthlyRevenue: PropTypes.object,
        monetizationPaymentInfo: PropTypes.object,
        ampSummaryYear: PropTypes.number,
        ampSummaryMonth: PropTypes.number,
        ampRevenueMonth: PropTypes.number,
        ampRevenueYear: PropTypes.number,
        paymentMessageOpen: PropTypes.bool,
        onCsvClick: PropTypes.func,
        onPaymentCloseClick: PropTypes.func,
        onAmpRevenueChange: PropTypes.func,
        onLabelSignup: PropTypes.func
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getSelectOptions(currentUser, ampYear) {
        const startYear = new Date(
            currentUser.profile.created * 1000
        ).getFullYear();
        const currentYear = new Date().getFullYear();
        const years = [];

        for (let i = startYear; i <= currentYear; i++) {
            years.push(i);
        }

        const yearOptions = years.map((year) => {
            return {
                text: year,
                value: year
            };
        });

        const currentMonth = new Date().getMonth();
        const monthOptions = months
            .map((month, i) => {
                if (currentYear === parseInt(ampYear, 10) && i > currentMonth) {
                    return null;
                }

                return {
                    text: month,
                    value: i
                };
            })
            .filter(Boolean);

        return {
            monthOptions,
            yearOptions
        };
    }

    usingThirtySecondPlays() {
        return (
            parseInt(
                `${this.props.ampSummaryYear}${this.props.ampSummaryMonth + 1}`,
                10
            ) >= 20185
        );
    }

    renderLoader(loading) {
        if (loading) {
            return (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        return null;
    }

    renderDailyTable(revenueObject) {
        const { ampRevenueMonth, ampRevenueYear, currentUser } = this.props;
        const { error, loading, data } = revenueObject;
        let content;
        let title = 'Earnings';

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        if (error) {
            if (error.errorcode === 14003) {
                content = <p>There's nothing to show here yet.</p>;
            } else {
                content = <p>There was an error fetching revenue.</p>;
            }
        } else {
            // use old AMP data for anything before August 2017
            if (
                ampRevenueYear < 2017 ||
                (ampRevenueYear === 2017 && ampRevenueMonth < 7)
            ) {
                title = 'Ad Opportunities';
            }

            const values = Object.keys(data.days || {}).reduce(
                (obj, day) => {
                    const value = parseFloat(data.days[day]);

                    obj.labels.push(day);
                    obj.data.push(value);

                    return obj;
                },
                { labels: [], data: [] }
            );

            content = (
                <BarChart
                    dataKey={`${ampRevenueYear}:${ampRevenueMonth}:${loading}`}
                    preventUpdate={loading}
                    labels={values.labels}
                    datasets={[
                        {
                            data: values.data
                        }
                    ]}
                />
            );
        }

        let footnote;

        if (data.estimated) {
            footnote = (
                <p className="u-fs-14 u-ls-n-04 u-text-gray-5 u-lh-12">
                    Values shown are estimated. Actual values will be available
                    later.
                </p>
            );
        }

        return (
            <Fragment>
                <div className="u-spacing-bottom-20 column small-24">
                    <h4 className="u-ls-n-06">{title} By Day (Estimated)</h4>
                    {footnote}
                </div>
                <div className="column small-24">
                    {this.renderLoader(loading)}
                    <div className="u-box-shadow u-padded">{content}</div>
                </div>
            </Fragment>
        );
    }

    renderMonthlyTable(revenueObject) {
        const { currentUser } = this.props;
        const { error, loading, data } = revenueObject;
        let content;

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        if (error) {
            let message;

            switch (error.errorcode) {
                case 14003:
                    message = "There's nothing to show here yet.";
                    break;

                case 14001:
                    message = error.message;
                    break;

                default:
                    message = 'There was an error fetching revenue.';
                    break;
            }

            content = <p>{message}</p>;
        } else if (!data.length) {
            content = <p>No historical data to display.</p>;
        } else {
            let playHeader = 'Plays';

            if (this.usingThirtySecondPlays()) {
                playHeader = 'Monetized Plays';
            }

            const headerCells = [
                'Account',
                'Artist',
                'Title',
                playHeader,
                'Revenue'
            ];
            const rows = Array.from(data)
                .sort((x, y) => {
                    const first = parseRevenueString(x.revenue);
                    const second = parseRevenueString(y.revenue);

                    return second - first;
                })
                .map((row) => {
                    const accountLink = row.account ? (
                        <Link to={`/artist/${row.uploaderUrl}`} target="_blank">
                            {row.account}
                        </Link>
                    ) : (
                        '-'
                    );
                    const musicLink = (
                        <a href={row.musicUrl} target="_blank">
                            {row.title}
                        </a>
                    );

                    return [
                        accountLink,
                        row.artist,
                        musicLink,
                        isNaN(row.plays) ? '-' : humanizeNumber(row.plays),
                        row.revenue
                    ];
                });

            content = (
                <Table
                    className="dashboard-table dashboard-table--monetization"
                    headerCells={headerCells}
                    bodyRows={rows}
                />
            );
        }

        return (
            <div className="u-box-shadow u-padded">
                {this.renderLoader(loading)}
                {content}
            </div>
        );
    }

    renderSelects() {
        const {
            onAmpRevenueChange,
            ampRevenueMonth,
            ampRevenueYear,
            currentUser
        } = this.props;
        const { monthOptions, yearOptions } = this.getSelectOptions(
            currentUser
        );

        return (
            <div className="dashboard-dropdowns u-d-flex u-d-flex--align-center">
                <Dropdown
                    name="month"
                    value={`${ampRevenueMonth}`}
                    options={monthOptions}
                    onChange={onAmpRevenueChange}
                    className="c-dropdown--button c-dropdown--full"
                    buttonClassName="button button--pill u-margin-0"
                    menuClassName="tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
                />
                <Dropdown
                    name="year"
                    value={`${ampRevenueYear}`}
                    options={yearOptions}
                    onChange={onAmpRevenueChange}
                    className="c-dropdown--button u-spacing-left-10"
                    buttonClassName="button button--pill"
                    menuClassName="tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
                />
            </div>
        );
    }

    renderSummaryBlocks() {
        const {
            monetizationSummary,
            ampSummaryYear,
            ampSummaryMonth
        } = this.props;
        const { error, loading: loadingSummary, summary } = monetizationSummary;

        let firstFieldHeader = 'Plays';

        // months are zero index based
        if (this.usingThirtySecondPlays()) {
            firstFieldHeader = 'Monetized Plays';
        }

        let firstFieldValue = summary.total_plays;
        let FirstIcon = PlayIcon;

        // use old AMP data for anything before August 2017
        if (
            ampSummaryYear < 2017 ||
            (ampSummaryYear === 2017 && ampSummaryMonth < 7)
        ) {
            firstFieldHeader = 'Ad Opportunities';
            firstFieldValue = summary.ad_opportunities;
            FirstIcon = BarChartIcon;
        }

        let estimated;

        if (summary.estimated) {
            estimated = <span className="u-fw-500">(Estimated Revenue)</span>;
        }

        return (
            <Fragment>
                <div className="column">
                    <StatBlock
                        text={firstFieldHeader}
                        value={firstFieldValue}
                        type="number"
                        loading={loadingSummary}
                        error={error}
                        icon={<FirstIcon className="stat-block__icon" />}
                    />
                </div>
                <div className="column">
                    <StatBlock
                        text={<Fragment>Revenue {estimated}</Fragment>}
                        type="currency"
                        value={parseRevenueString(summary.total_revenue)}
                        valueFormatter={formatCurrency}
                        loading={loadingSummary}
                        error={error}
                        icon={<DollarIcon className="stat-block__icon" />}
                    />
                </div>
            </Fragment>
        );
    }

    render() {
        const {
            ampRevenueYear,
            ampRevenueMonth,
            monetizationMonthlyRevenue,
            monetizationDailyRevenue,
            monetizationSummary,
            monetizationPaymentInfo,
            onPaymentCloseClick,
            paymentMessageOpen
        } = this.props;
        const { loading, loadingCsv } = monetizationMonthlyRevenue;
        const { error } = monetizationSummary;
        const { estimated } = monetizationSummary.summary;
        const isLoading = loading || loadingCsv;
        let loader;

        if (isLoading) {
            loader = <AndroidLoader size={20} />;
        }

        let csvButton;

        if (!error) {
            csvButton = (
                <button
                    onClick={this.props.onCsvClick}
                    className="button button--link"
                    disabled={isLoading}
                >
                    {loader}
                    <strong>Download CSV</strong>
                </button>
            );
        }

        const isPayable = monetizationPaymentInfo.info.payable;
        let paymentAlert;
        let acceptTos;

        if (!isPayable && !monetizationPaymentInfo.loading) {
            paymentAlert = (
                <div className="row u-spacing-top">
                    <div className="column small-24 u-text-center">
                        <ConfirmMessage
                            open={paymentMessageOpen}
                            onClose={onPaymentCloseClick}
                        >
                            <p>
                                Please enter your{' '}
                                <Link to="/monetization/info?ref=monetization">
                                    payment details here
                                </Link>
                                .
                            </p>
                        </ConfirmMessage>
                    </div>
                </div>
            );
        }

        if (error && error.errorcode === 14004) {
            acceptTos = (
                <div className="row">
                    <div className="column small-24 u-text-center">
                        <ConfirmMessage
                            open
                            hideCloseButton
                            className="u-spacing-top"
                        >
                            <p>
                                You have been approved for AMP. Please{' '}
                                <button
                                    className="button button--link"
                                    onClick={this.props.onLabelSignup}
                                >
                                    click here to agree to our Terms of Service
                                </button>{' '}
                                and start monetizing your content.
                            </p>
                        </ConfirmMessage>
                    </div>
                </div>
            );
        }

        return (
            <Fragment>
                {paymentAlert}
                {acceptTos}
                <section className="row">
                    <div className="column">
                        <TunecoreAd
                            className="u-text-center u-spacing-top u-spacing-bottom"
                            linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_afpd_su&utm_content=ghoagpwt"
                            eventLabel={eventLabel.dashboard}
                            variant="condensed"
                        />
                    </div>
                </section>
                <section className="row u-spacing-bottom">
                    <div className="column">
                        <div className="u-box-shadow u-left-right u-padding-em align-middle">
                            <h2>
                                <BarChartIcon className="u-text-icon u-text-orange" />
                                Monetization (AMP) Revenue{' '}
                                <span className="thin">
                                    - {months[ampRevenueMonth]} {ampRevenueYear}
                                </span>
                            </h2>
                            {this.renderSelects()}
                        </div>
                    </div>
                </section>

                <section className="row u-spacing-bottom u-pos-relative">
                    <div className="column small-24 u-left-right u-spacing-bottom-em align-middle">
                        <h4 className="u-ls-n-06">
                            Monetization Summary
                            {estimated ? ' (Estimated)' : ''}
                        </h4>
                        <Link
                            to="/monetization/faq"
                            className="button button--link"
                            disabled={loading}
                        >
                            <strong>AMP FAQ</strong>
                        </Link>
                    </div>
                    {this.renderSummaryBlocks()}
                </section>

                <section className="row u-spacing-bottom u-pos-relative">
                    {this.renderDailyTable(monetizationDailyRevenue)}
                </section>

                <section className="row u-spacing-bottom u-pos-relative">
                    <div className="column small-24 u-left-right u-spacing-bottom-em align-middle">
                        <h4 className="u-ls-n-06">
                            Revenue By Song / Album
                            {estimated ? ' (Estimated)' : ''}
                        </h4>
                        {csvButton}
                    </div>
                    <div className="column small-24">
                        {this.renderMonthlyTable(monetizationMonthlyRevenue)}
                    </div>
                </section>
            </Fragment>
        );
    }
}
