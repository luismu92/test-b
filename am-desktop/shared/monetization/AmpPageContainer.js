import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';

import analytics, { eventCategory } from 'utils/analytics';

import hideSidebarForComponent from '../hoc/hideSidebarForComponent';
import { validateCode } from '../redux/modules/monetization/label';

import AmpPage from './AmpPage';

class AmpPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        location: PropTypes.object,
        monetizationLabel: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            step: 1,
            code: null,
            showJoinForm: true
        };

        this._steps = [
            'Invite Code',
            'Create Account',
            'Join AMP',
            'Upload Music'
        ].filter((step) => {
            if (props.currentUser.isLoggedIn && step === 'Create Account') {
                return false;
            }

            return true;
        });

        if (
            props.currentUser.isLoggedIn &&
            props.currentUser.profile.label_owner
        ) {
            this.state.step = 3;
        }
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, currentUser } = this.props;
        const form = e.currentTarget;
        const stepText = this._steps[this.state.step - 1];

        switch (stepText) {
            case 'Invite Code': {
                const code = form.code.value;

                dispatch(validateCode(code))
                    .then(() => {
                        this.setState({
                            code
                        });
                        this.goToNextStep();
                        return;
                    })
                    .catch(() => {});
                break;
            }

            case 'Create Account': {
                if (currentUser.profile.label_owner) {
                    this.goToStep(this._steps.length);
                } else {
                    this.goToNextStep();
                }
                break;
            }

            case 'Join AMP': {
                analytics.track(eventCategory.ampInvite, {
                    eventAction: this.state.code.toLowerCase(),
                    eventLabel: `${currentUser.profile.url_slug}:${
                        currentUser.profile.id
                    }`
                });

                this.goToNextStep();
                break;
            }

            default:
                this.goToNextStep();
                break;
        }
    };

    handleLoginFormToggle = (e) => {
        e.preventDefault();

        this.setState((prevState) => {
            return {
                ...prevState,
                showJoinForm: !prevState.showJoinForm
            };
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    goToNextStep() {
        this.setState((prevState) => {
            return {
                ...prevState,
                step: prevState.step + 1
            };
        });
    }

    goToStep(step) {
        this.setState({
            step
        });
    }

    goToPreviousStep() {
        this.setState((prevState) => {
            return {
                ...prevState,
                step: prevState.step - 1
            };
        });
    }

    render() {
        return (
            <AmpPage
                ignoreTos={parse(this.props.location.search).tos === '1'}
                onFormSubmit={this.handleFormSubmit}
                step={this.state.step}
                inviteCode={this.state.code}
                showJoinForm={this.state.showJoinForm}
                onLoginFormToggle={this.handleLoginFormToggle}
                currentUser={this.props.currentUser}
                steps={this._steps}
                monetizationLabel={this.props.monetizationLabel}
                location={this.props.location}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        monetizationLabel: state.monetizationLabel
    };
}

export default compose(
    hideSidebarForComponent,
    connect(mapStateToProps)
)(AmpPageContainer);
