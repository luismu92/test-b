import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import moment from 'moment';
import { DEFAULT_DATE_FORMAT } from 'constants/index';
import { getArtistUrl } from 'utils/artist';

import Table from '../components/Table';
import AndroidLoader from '../loaders/AndroidLoader';

export default function AmpCodesList({ codes }) {
    const { list, error, loading } = codes;

    if (!list || !list.length) {
        return (
            <p style={{ textAlign: 'center' }}>
                No AMP codes have been generated.
            </p>
        );
    }

    if (error) {
        return (
            <p style={{ textAlign: 'center' }}>
                There was an error loading AMP codes.
            </p>
        );
    }

    const codeList = list.map((code, i) => {
        let redeemedOn = '--';
        let redeemedBy = '--';

        if (code.redemption) {
            redeemedOn = moment(code.redemption.date_redeemed * 1000).format(
                DEFAULT_DATE_FORMAT
            );
            redeemedBy = (
                <Link to={getArtistUrl(code.redemption.artist)}>
                    {code.redemption.artist.name}
                </Link>
            );
        }
        return (
            <tr key={`code-${i}`}>
                <td>
                    <pre>{code.code}</pre>
                </td>
                <td>
                    {moment(code.date_generated * 1000).format(
                        DEFAULT_DATE_FORMAT
                    )}
                </td>
                <td>{redeemedOn}</td>
                <td>{redeemedBy}</td>
            </tr>
        );
    });

    const headerCells = [
        {
            value: 'Code'
        },
        {
            value: 'Generated On'
        },
        {
            value: 'Redeemed On'
        },
        {
            value: 'Redeemed By'
        }
    ];

    let loader;
    if (loading) {
        loader = <AndroidLoader size={45} />;
    }

    return (
        <Fragment>
            <Table headerCells={headerCells} bodyRows={codeList} />
            {loader}
        </Fragment>
    );
}

AmpCodesList.propTypes = {
    codes: PropTypes.object
};
