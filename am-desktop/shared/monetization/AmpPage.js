import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { parse } from 'query-string';
import Helmet from 'react-helmet';

import ProgressBar from '../components/ProgressBar';
import AmMark from '../icons/am-mark';
import ArrowIcon from '../icons/arrow-next';
import InboxOutIcon from '../icons/inbox-out';

import JoinFormContainer from '../auth/JoinFormContainer';
import LoginFormContainer from '../auth/LoginFormContainer';
import LabelSignupContainer from '../monetization/LabelSignupContainer';

export default class AmpPage extends Component {
    static propTypes = {
        step: PropTypes.number,
        steps: PropTypes.array,
        inviteCode: PropTypes.string,
        onFormSubmit: PropTypes.func,
        showJoinForm: PropTypes.bool,
        ignoreTos: PropTypes.bool,
        onLoginFormToggle: PropTypes.func,
        currentUser: PropTypes.object,
        monetizationLabel: PropTypes.object,
        location: PropTypes.object
    };

    renderPreStepText(step, isLastStep) {
        if (isLastStep) {
            return 'Finished';
        }

        return `Step ${step}`;
    }

    renderStepText(stepText) {
        switch (stepText) {
            case 'Invite Code':
                return 'Enter Your Invite Code';

            case 'Create Account':
                return 'Create Your Audiomack Account';

            case 'Join AMP':
                return 'Join the AMP Program';

            case 'Upload Music':
                return 'Upload Your Music!';

            default:
                return null;
        }
    }

    renderTitleText(stepText) {
        switch (stepText) {
            case 'Invite Code':
                return 'Join Audiomack and Monetize Your Music in Minutes!';

            case 'Create Account':
                return 'Your invite code is accepted. Please create an account.';

            case 'Join AMP':
                return 'Please agree to the AMP Program terms of service.';

            case 'Upload Music':
                return 'Congrats! You are now part of the AMP Program.';

            default:
                return null;
        }
    }

    renderSubtitleText(stepText) {
        switch (stepText) {
            case 'Invite Code':
                return (
                    <Fragment>
                        Audiomack is a <strong>FREE</strong>, limitless music
                        sharing and discovery platform for artists, tastemakers,
                        labels, and fans.
                    </Fragment>
                );

            case 'Create Account':
                return 'If you already have an account please let your Audiomack representative know.';

            case 'Join AMP':
                return 'This allows us to monetize your music and send you payments.';

            default:
                return null;
        }
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p className="u-text-red">{error.message}</p>;
    }

    renderStepContent(step) {
        const {
            currentUser,
            monetizationLabel,
            onFormSubmit,
            inviteCode,
            onLoginFormToggle,
            showJoinForm,
            location
        } = this.props;

        switch (step) {
            case 'Invite Code': {
                let submitText = 'Next';

                if (monetizationLabel.codeIsValidating) {
                    submitText = 'Validating…';
                }

                const prefilledValue = location.search
                    ? parse(location.search).ampCode
                    : null;

                return (
                    <div className="amp-code-container u-text-center">
                        <p className="amp-large-text">
                            <strong>
                                The Audiomack AMP Program is in invite-only
                                beta.
                            </strong>
                        </p>
                        <p className="amp-large-text">
                            Please enter your invite code to get started
                        </p>
                        <form onSubmit={onFormSubmit}>
                            <input
                                type="text"
                                name="code"
                                placeholder="Enter invite code"
                                className={`amp-code-input u-spacing-top-em${
                                    monetizationLabel.codeError
                                        ? ' u-text-red'
                                        : ''
                                }`}
                                defaultValue={prefilledValue}
                                required
                            />
                            {this.renderError(monetizationLabel.codeError)}
                            <button
                                className="button button--pill button--padded amp-code-button u-spacing-top-em"
                                type="submit"
                                disabled={monetizationLabel.codeIsValidating}
                            >
                                {submitText}{' '}
                                <ArrowIcon className="u-text-icon" />
                            </button>
                        </form>
                    </div>
                );
            }

            case 'Create Account': {
                if (!showJoinForm) {
                    return (
                        <Fragment>
                            <h5 className="u-spacing-bottom-em">
                                Need to create a new account to be enrolled in
                                AMP? Create an account{' '}
                                <Link to="/join" onClick={onLoginFormToggle}>
                                    here
                                </Link>
                                .
                            </h5>
                            <LoginFormContainer
                                onLoginSuccess={onFormSubmit}
                                shouldCookieUser={!currentUser.isLoggedIn}
                                submitButtonText="Continue"
                                submitButtonIcon={
                                    <ArrowIcon className="u-text-icon" />
                                }
                                submitButtonClass="amp-code-button"
                                showLabels
                            />
                        </Fragment>
                    );
                }
                return (
                    <Fragment>
                        <h5 className="u-spacing-bottom-em">
                            Already have an account that should be enrolled in
                            AMP? Log in{' '}
                            <Link to="/login" onClick={onLoginFormToggle}>
                                here
                            </Link>
                            .
                        </h5>
                        <JoinFormContainer
                            onJoinSuccess={onFormSubmit}
                            shouldCookieUser={!currentUser.isLoggedIn}
                            submitButtonText="Continue"
                            submitButtonIcon={
                                <ArrowIcon className="u-text-icon" />
                            }
                            submitButtonClass="amp-code-button"
                            showLabels
                        />
                    </Fragment>
                );
            }

            case 'Join AMP':
                return (
                    <Fragment>
                        <p className="amp-large-text u-text-center">
                            <strong>
                                Fill in the form below to join the AMP program.
                            </strong>
                        </p>
                        <p className="amp-large-text u-text-center u-spacing-bottom">
                            Please use your real name.
                        </p>
                        <LabelSignupContainer
                            inviteCode={inviteCode}
                            onFormSubmit={onFormSubmit}
                            submitButtonText="Next"
                            ignoreTos={this.props.ignoreTos}
                            submitButtonIcon={
                                <ArrowIcon className="u-text-icon" />
                            }
                            submitButtonClass="amp-code-button"
                        />
                    </Fragment>
                );

            case 'Upload Music':
                return (
                    <div className="amp-complete-container u-text-center">
                        <h3>Next, please upload your music.</h3>
                        <div className="success-circle">
                            <span className="success-circle__check">
                                <svg
                                    enableBackground="new 0 0 24 24"
                                    id="Layer_1"
                                    version="1.0"
                                    viewBox="0 0 24 24"
                                >
                                    <polyline
                                        className="path"
                                        fill="none"
                                        points="20,12 15,17 4,6"
                                        stroke="#fff"
                                        strokeMiterlimit="10"
                                        strokeWidth="1.5"
                                    />
                                </svg>
                            </span>
                        </div>
                        <p className="u-fw-600 u-ls-n-025 u-spacing-bottom-20">
                            <a
                                className="amp-faq-link"
                                href="/monetization/faq"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                How To Monetize Past Uploads & Other Frequently
                                Asked Questions
                            </a>
                        </p>
                        <div className="button-group">
                            <Link
                                className="button button--pill button--padded amp-upload-button"
                                to="/upload"
                            >
                                <span className="button__icon">
                                    <InboxOutIcon />
                                </span>
                                Upload my music
                            </Link>
                        </div>
                    </div>
                );

            default:
                return null;
        }
    }

    render() {
        const { step, steps } = this.props;
        const stepText = steps[step - 1];

        let bottomLink = (
            <div className="u-text-center u-spacing-top-30">
                <p className="u-fw-600 u-ls-n-025">
                    <a
                        href="/monetization/faq"
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        Frequently Asked Questions
                    </a>
                </p>
            </div>
        );

        if (step === 3) {
            bottomLink = null;
        }

        return (
            <Fragment>
                <Helmet>
                    <title>Audiomack AMP Program</title>
                    <meta name="robots" content="noindex, nofollow" />
                </Helmet>
                <div className="amp-banner">
                    <AmMark className="amp-banner__logo" />
                    <div className="row amp-banner__text">
                        <h1 className="column small-24">
                            {this.renderTitleText(stepText)}
                        </h1>
                        <p className="column small-24">
                            {this.renderSubtitleText(stepText)}
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="column small-24 medium-20 medium-offset-2">
                        <div className="amp-steps u-box-shadow u-text-center">
                            <h2>
                                <span>
                                    {this.renderPreStepText(
                                        step,
                                        steps.length === step
                                    )}
                                    :
                                </span>{' '}
                                {this.renderStepText(stepText)}
                            </h2>
                            <div className="amp-steps__progress-wrap">
                                <ProgressBar steps={steps} activeStep={step} />
                            </div>
                        </div>
                    </div>
                    <div className="column small-24 medium-20 medium-offset-2">
                        <div className="amp-content-container u-spacing-top u-spacing-bottom u-box-shadow">
                            {this.renderStepContent(stepText)}
                            {bottomLink}
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
