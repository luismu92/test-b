import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { yesBool } from 'utils/index';

export default class LabelSignup extends Component {
    static propTypes = {
        onInputChange: PropTypes.func,
        onFormSubmit: PropTypes.func,
        inputFields: PropTypes.object,
        loading: PropTypes.bool,
        ignoreTos: PropTypes.bool,
        inviteCode: PropTypes.string,
        submitButtonIcon: PropTypes.object,
        submitButtonText: PropTypes.string,
        submitButtonClass: PropTypes.string,
        error: PropTypes.object
    };

    static defaultProps = {
        submitButtonText: 'Submit Application'
    };

    renderError(error) {
        if (!error) {
            return null;
        }

        return (
            <p className="u-text-center u-text-red u-spacing-bottom-em">
                {error.message}
            </p>
        );
    }

    render() {
        const {
            inputFields,
            onInputChange,
            onFormSubmit,
            submitButtonText,
            submitButtonIcon,
            submitButtonClass,
            inviteCode,
            ignoreTos
        } = this.props;
        const submitClass = classnames('button button--pill button--padded', {
            [submitButtonClass]: submitButtonClass
        });

        let companyInput;

        if (!inputFields.isIndividual) {
            companyInput = (
                <div className="column small-24">
                    <label htmlFor="company">Company</label>
                    <input
                        id="company"
                        type="text"
                        value={inputFields.company_name}
                        name="company_name"
                        onChange={onInputChange}
                        required
                    />
                </div>
            );
        }

        let codeInput;

        if (inviteCode) {
            codeInput = <input type="hidden" name="code" value={inviteCode} />;
        }

        let tos;

        if (!ignoreTos) {
            tos = (
                <div className="column small-24">
                    <label>
                        <input
                            required
                            type="checkbox"
                            name="agree"
                            checked={yesBool(inputFields.agree)}
                            onChange={onInputChange}
                        />
                        By submitting this application you agree to our{' '}
                        <a href="/premium-partner-agreement" target="_blank">
                            Premium Partner Agreement
                        </a>
                        .
                    </label>
                </div>
            );
        }

        return (
            <form className="row" onSubmit={onFormSubmit}>
                <div className="column small-24 u-spacing-bottom-em">
                    {codeInput}
                    <label className="u-d-inline-block u-spacing-right-em">
                        <input
                            type="radio"
                            name="isIndividual"
                            value="true"
                            checked={inputFields.isIndividual}
                            onChange={onInputChange}
                        />
                        Individual
                    </label>
                    <label className="u-d-inline-block">
                        <input
                            type="radio"
                            name="isIndividual"
                            value="false"
                            checked={!inputFields.isIndividual}
                            onChange={onInputChange}
                        />
                        Company
                    </label>
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="fname">Legal first name</label>
                    <input
                        required
                        id="fname"
                        type="text"
                        value={inputFields.first_name}
                        name="first_name"
                        onChange={onInputChange}
                    />
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="lname">Legal last name</label>
                    <input
                        required
                        id="lname"
                        type="text"
                        value={inputFields.last_name}
                        name="last_name"
                        onChange={onInputChange}
                    />
                </div>
                {companyInput}
                {tos}
                <div className="column small-24 u-text-center u-spacing-top-em">
                    {this.renderError(this.props.error)}
                    <button
                        type="submit"
                        className={submitClass}
                        disabled={this.props.loading}
                    >
                        {this.props.loading ? 'Submitting…' : submitButtonText}{' '}
                        {submitButtonIcon}
                    </button>
                </div>
            </form>
        );
    }
}
