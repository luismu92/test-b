import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import requireAuth from '../hoc/requireAuth';

import {
    generateCode,
    getAmpCodes
} from '../redux/modules/monetization/ampCode';

import AmpCodeGenerator from './AmpCodeGenerator';

class AmpCodeGeneratorContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        monetizationAmpCode: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            codePrefix: null
        };
    }

    handleGenerateClick = () => {
        const { dispatch } = this.props;
        const { codePrefix } = this.state;

        const prefix = codePrefix ? `${codePrefix}-` : null;

        dispatch(generateCode(prefix));
    };

    handlePrefixChange = (e) => {
        this.setState({
            codePrefix: e.currentTarget.value
        });
    };

    handleCodeFocusClick = (e) => {
        e.currentTarget.select();
    };

    render() {
        return (
            <AmpCodeGenerator
                monetizationAmpCode={this.props.monetizationAmpCode}
                onGenerateClick={this.handleGenerateClick}
                onPrefixChange={this.handlePrefixChange}
                onCodeFocusClick={this.handleCodeFocusClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        monetizationAmpCode: state.monetizationAmpCode
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return (
                    currentUser.isLoggedIn &&
                    (currentUser.isAdmin || currentUser.profile.is_amp_approver)
                );
            },
            redirectTo() {
                return '/';
            }
        }),
    connect(mapStateToProps)
)(connectDataFetchers(AmpCodeGeneratorContainer, [() => getAmpCodes()]));
