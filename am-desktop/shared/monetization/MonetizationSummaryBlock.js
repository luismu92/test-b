import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { easeOutSine } from 'utils/index';

import AndroidLoader from '../loaders/AndroidLoader';

export default class MonetizationSummaryBlock extends Component {
    static propTypes = {
        header: PropTypes.string,
        value: PropTypes.number,
        totalSteps: PropTypes.number,
        valueFormatter: PropTypes.func,
        icon: PropTypes.element,
        loading: PropTypes.bool
    };

    static defaultProps = {
        value: 0,
        totalSteps: 50,
        valueFormatter(val) {
            return val;
        }
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            currentValue: props.value
        };
    }

    componentDidMount() {
        this.spinDemNumbas(this.props.value, this.state.currentValue);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value) {
            this.spinDemNumbas(prevProps.value, this.props.value);
        }
    }

    componentWillUnmount() {
        window.cancelAnimationFrame(this._raf);
        this._raf = null;
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    spinDemNumbas(startValue, endValue) {
        window.cancelAnimationFrame(this._raf);

        this.spin(startValue, endValue);
    }

    spin(startValue, endValue, currentValue) {
        const { valueFormatter, totalSteps } = this.props;
        const increment = (endValue - startValue) / totalSteps;
        const current =
            typeof currentValue === 'undefined' ? startValue : currentValue;
        const nextValue = current + increment;
        let shouldStop = nextValue >= endValue;

        if (endValue < startValue) {
            shouldStop = nextValue <= endValue;
        }

        if (shouldStop) {
            const finalValue = valueFormatter(endValue);

            this._value.textContent = finalValue;

            this.setState({
                currentValue: endValue
            });
            return;
        }

        this._raf = window.requestAnimationFrame(() => {
            if (!this._value) {
                return;
            }

            let progress = nextValue / endValue;
            let totalValue = endValue;

            if (endValue < startValue) {
                progress = nextValue / startValue;
                totalValue = startValue;
            }

            // This doesnt really give the effect I wanted but ill leave
            // here for now.
            const modifier = easeOutSine(progress);

            this._value.textContent = valueFormatter(totalValue * modifier);

            this.spin(startValue, endValue, nextValue);
        });
    }

    renderIcon(icon, loading) {
        if (loading) {
            return (
                <div className="monetization-block__icon">
                    <AndroidLoader size={20} />
                </div>
            );
        }

        return icon;
    }

    render() {
        const { valueFormatter, header, icon, loading } = this.props;

        return (
            <div className="monetization-block">
                <h4 className="monetization-block__header">{header}</h4>
                <p
                    className="monetization-block__value"
                    ref={(c) => (this._value = c)}
                >
                    {valueFormatter(this.state.currentValue)}
                </p>
                {this.renderIcon(icon, loading)}
            </div>
        );
    }
}
