import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import requireAuth from '../hoc/requireAuth';

import { loadScript } from 'utils/index';

import RoyaltiesPage from './RoyaltiesPage';

class RoyaltiesPageContainer extends Component {
    static propTypes = {};

    componentDidMount() {
        window.songtrustSettings = {
            client_id: process.env.SONGTRUST_CLIENT_ID,
            discount_code: 'audiomack10'
        };

        loadScript(
            'https://d2j3eqdaf3h4a3.cloudfront.net/0.0.2/integration.js',
            'songtrust-sdk'
        );
    }

    render() {
        return <RoyaltiesPage />;
    }
}

function mapStateToProps() {
    return {};
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return (
                    currentUser.isLoggedIn && currentUser.profile.label_owner
                );
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(RoyaltiesPageContainer);
