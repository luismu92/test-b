import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { history } from 'redux/configureStore';

import globalReducer from './modules/global';

import artist from './modules/artist/index';
import artistFavorites from './modules/artist/favorites';
import artistFollowers from './modules/artist/followers';
import artistFollowing from './modules/artist/following';
import artistFeed from './modules/artist/feed';
import artistPlaylists from './modules/artist/playlists';
import artistUploads from './modules/artist/uploads';
import artistBrowse from './modules/artist/browse';
import artistPopover from './modules/artist/popover';
import artistPinned from './modules/artist/pinned';

import homeTrending from './modules/home/trending';
import homeSong from './modules/home/song';
import homeAlbum from './modules/home/album';

import music from './modules/music/index';
import musicEdit from './modules/music/edit';
import musicBrowse from './modules/music/browse';
import musicBrowsePlaylist from './modules/music/browsePlaylist';
import musicSong from './modules/music/song';
import musicAlbum from './modules/music/album';
import musicTags from './modules/music/tags';

import currentUser from './modules/user/index';
import currentUserFollowing from './modules/user/following';
import currentUserFeed from './modules/user/feed';
import currentUserUploads from './modules/user/uploads';
import currentUserPlaylists from './modules/user/playlists';
import currentUserNotifications from './modules/user/notifications';
import currentUserNotificationSettings from './modules/user/notificationSettings';
import currentUserPinned from './modules/user/pinned';
import currentUserPremium from './modules/user/premium';

import searchResults from './modules/search/results';
import searchSuggestions from './modules/search/suggestions';
import searchUserResults from './modules/search/userResults';

import upload from './modules/upload/index';
import uploadAlbum from './modules/upload/album';
import uploadSong from './modules/upload/song';

import comment from './modules/comment';

import worldPost from './modules/world/post';
import worldPostArtists from './modules/world/postArtists';
import worldFeatured from './modules/world/featured';
import worldLocation from './modules/world/location';
import worldPage from './modules/world/page';
import worldGlobePosts from './modules/world/globePosts';
import worldSettings from './modules/world/settings';

import message from './modules/message';
import ad from './modules/ad';
import email from './modules/email';
import modal from './modules/modal';
import playlist from './modules/playlist';
import player from './modules/player';
import toastNotification from './modules/toastNotification';
import featured from './modules/featured';

import stats from './modules/stats';

import statsArtistGeo from './modules/stats/artist/geo';
import statsArtistCountries from './modules/stats/artist/countries';
import statsArtistCities from './modules/stats/artist/cities';
import statsArtistPlaySource from './modules/stats/artist/playSource';
import statsArtistFans from './modules/stats/artist/fans';
import statsArtistInfluencers from './modules/stats/artist/influencers';

import statsMusicGeo from './modules/stats/music/geo';
import statsMusicCountries from './modules/stats/music/countries';
import statsMusicCities from './modules/stats/music/cities';
import statsMusicSummary from './modules/stats/music/summary';
import statsMusicDaily from './modules/stats/music/daily';
import statsMusicUrl from './modules/stats/music/url';
import statsMusicPlaySource from './modules/stats/music/playSource';
import statsMusicPlaylistAdds from './modules/stats/music/playlistAdds';
import statsMusicPromoLink from './modules/stats/music/promoLink';
// import statsMusicFans from './modules/stats/music/fans';
// import statsMusicInfluencers from './modules/stats/music/influencers';

import promoKey from './modules/promoKey';
import monetizationSummary from './modules/monetization/summary';
import monetizationDailyRevenue from './modules/monetization/dailyRevenue';
import monetizationLabel from './modules/monetization/label';
import monetizationMonthlyRevenue from './modules/monetization/monthlyRevenue';
import monetizationAssociatedMusic from './modules/monetization/associatedMusic';
import monetizationAssociatedArtists from './modules/monetization/associatedArtists';
import monetizationPaymentInfo from './modules/monetization/paymentInfo';
import monetizationAmpCode from './modules/monetization/ampCode';

import adminFollowList from './modules/theBackwoods/followList';
import adminReservedEmail from './modules/theBackwoods/reservedEmail';
import adminStatsSwap from './modules/theBackwoods/statsSwap';
import adminTrendingOrganizer from './modules/theBackwoods/trendingOrganizer';
import adminRecent from './modules/theBackwoods/recent';
import adminValidateArtist from './modules/theBackwoods/validateArtist';
import adminArtists from './modules/theBackwoods/artist';
import adminUnplayableTracks from './modules/theBackwoods/unplayableTracks';
import adminMusics from './modules/theBackwoods/music';

import admin from './modules/admin';
import oauth from './modules/oauth';

export default combineReducers({
    artist,
    artistFavorites,
    artistFollowers,
    artistFollowing,
    artistFeed,
    artistPlaylists,
    artistUploads,
    artistBrowse,
    artistPopover,
    artistPinned,

    homeTrending,
    homeSong,
    homeAlbum,

    music,
    musicEdit,
    musicBrowse,
    musicBrowsePlaylist,
    musicSong,
    musicAlbum,
    musicTags,

    currentUser,
    currentUserFollowing,
    currentUserUploads,
    currentUserFeed,
    currentUserNotifications,
    currentUserNotificationSettings,
    currentUserPlaylists,
    currentUserPinned,
    currentUserPremium,

    searchResults,
    searchSuggestions,
    searchUserResults,

    upload,
    uploadAlbum,
    uploadSong,

    comment,

    worldPost,
    worldPostArtists,
    worldFeatured,
    worldLocation,
    worldPage,
    worldGlobePosts,
    worldSettings,

    monetizationSummary,
    monetizationDailyRevenue,
    monetizationLabel,
    monetizationMonthlyRevenue,
    monetizationAssociatedMusic,
    monetizationAssociatedArtists,
    monetizationPaymentInfo,
    monetizationAmpCode,

    stats,

    statsArtistGeo,
    statsArtistCountries,
    statsArtistCities,
    statsArtistPlaySource,
    statsArtistFans,
    statsArtistInfluencers,

    statsMusicGeo,
    statsMusicCountries,
    statsMusicCities,
    statsMusicSummary,
    statsMusicDaily,
    statsMusicUrl,
    statsMusicPlaySource,
    statsMusicPlaylistAdds,
    statsMusicPromoLink,
    // statsMusicFans,
    // statsMusicInfluencers,

    oauth,
    admin,
    message,
    ad,
    email,
    modal,
    playlist,
    player,
    toastNotification,
    global: globalReducer,
    router: connectRouter(history),
    featured,
    promoKey,

    adminReservedEmail,
    adminFollowList,
    adminStatsSwap,
    adminTrendingOrganizer,
    adminRecent,
    adminValidateArtist,
    adminArtists,
    adminUnplayableTracks,
    adminMusics
});
