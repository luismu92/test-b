import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from '../user';
import { TRACK_PLAY } from '../stats';
import { BUMP_MUSIC, BUMP_MUSIC_ONE_SPOT } from '../admin';

const PREFIX = 'am/home/trending/';

const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const FETCH_MUSIC = `${PREFIX}FETCH_MUSIC`;

const defaultState = {
    loading: false,
    lastUpdated: null,
    errors: [],
    list: []
};

const CACHE_TIME = 1000 * 60 * 15; // 15 minutes

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(FETCH_MUSIC):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case failSuffix(FETCH_MUSIC):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case CLEAR_LIST:
            return Object.assign({}, state, {
                list: []
            });

        case FETCH_MUSIC: {
            return Object.assign({}, state, {
                list: action.resolved.results,
                lastUpdated: Date.now(),
                loading: false
            });
        }

        case failSuffix(UNFAVORITE_ITEM):
        case requestSuffix(FAVORITE_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(FAVORITE_ITEM):
        case requestSuffix(UNFAVORITE_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(UNREPOST_ITEM):
        case requestSuffix(REPOST_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(REPOST_ITEM):
        case requestSuffix(UNREPOST_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case TRACK_PLAY: {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };
                    const itemId = action.id;
                    const albumId = action.options.album_id;

                    if (
                        (obj.type === 'album' &&
                            typeof albumId === 'number' &&
                            obj.id === albumId) ||
                        (obj.type !== 'album' && obj.id === itemId)
                    ) {
                        // Only update if this is the first track play call, ie not the 30 second track call
                        if (!action.options.time) {
                            newObj.stats['plays-raw'] += 1;
                        }
                    }

                    return newObj;
                })
            });
        }

        // Since this is admin only, i wont worry about this failing and resetting
        // the list back to the previous state
        case requestSuffix(BUMP_MUSIC): {
            return {
                ...state,
                list: [action.item].concat(
                    state.list.filter((item) => item.id !== action.item.id)
                )
            };
        }

        // Since this is admin only, i wont worry about this failing and resetting
        // the list back to the previous state
        case requestSuffix(BUMP_MUSIC_ONE_SPOT): {
            const currentIndex = state.list.findIndex(
                (item) => item.id === action.item.id
            );

            if (currentIndex === -1) {
                return state;
            }

            const item = state.list[currentIndex];
            const firstHalf = state.list.slice(0, currentIndex);
            const secondHalf = state.list.slice(currentIndex + 1);
            const itemToSwitchWith = firstHalf.pop();

            firstHalf.push(item);
            firstHalf.push(itemToSwitchWith);

            const newList = [...firstHalf, ...secondHalf];

            return {
                ...state,
                list: newList
            };
        }

        default:
            return state;
    }
}

export function fetchSongList() {
    return (dispatch, getState) => {
        const state = getState().homeTrending;
        let promise;

        if (
            state.lastUpdated &&
            Date.now() - state.lastUpdated < CACHE_TIME &&
            state.list.length
        ) {
            promise = Promise.resolve({
                results: state.list
            });
        } else {
            promise = api.music.trending();
        }

        return dispatch({
            type: FETCH_MUSIC,
            promise
        });
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
