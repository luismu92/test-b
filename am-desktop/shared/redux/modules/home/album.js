import api from 'api/index';
import { suffix } from 'utils/index';
import { COLLECTION_TYPE_ALBUM, CHART_TYPE_DAILY } from 'constants/index';

import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from '../user';
import { TRACK_PLAY } from '../stats';

const PREFIX = 'am/home/album/';

const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const FETCH_MUSIC = `${PREFIX}FETCH_MUSIC`;

const defaultState = {
    loading: false,
    errors: [],
    list: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(FETCH_MUSIC, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(FETCH_MUSIC, 'FAILURE'):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case CLEAR_LIST:
            return Object.assign({}, state, {
                list: []
            });

        case FETCH_MUSIC: {
            return Object.assign({}, state, {
                list: action.resolved.results,
                loading: false
            });
        }

        case suffix(UNFAVORITE_ITEM, 'FAILURE'):
        case suffix(FAVORITE_ITEM, 'REQUEST'): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case suffix(FAVORITE_ITEM, 'FAILURE'):
        case suffix(UNFAVORITE_ITEM, 'REQUEST'): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case suffix(UNREPOST_ITEM, 'FAILURE'):
        case suffix(REPOST_ITEM, 'REQUEST'): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case suffix(REPOST_ITEM, 'FAILURE'):
        case suffix(UNREPOST_ITEM, 'REQUEST'): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case TRACK_PLAY: {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };
                    const itemId = action.id;
                    const albumId = action.options.album_id;

                    if (
                        (obj.type === 'album' &&
                            typeof albumId === 'number' &&
                            obj.id === albumId) ||
                        (obj.type !== 'album' && obj.id === itemId)
                    ) {
                        // Only update if this is the first track play call, ie not the 30 second track call
                        if (!action.options.time) {
                            newObj.stats['plays-raw'] += 1;
                        }
                    }

                    return newObj;
                })
            });
        }

        default:
            return state;
    }
}

export function fetchSongList() {
    return {
        type: FETCH_MUSIC,
        promise: api.chart.get(COLLECTION_TYPE_ALBUM, CHART_TYPE_DAILY)
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
