import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/reserved-email/';

export const SET_EMAIL = `${PREFIX}SET_EMAIL`;
export const GET_RESERVED_EMAIL = `${PREFIX}GET_RESERVED_EMAIL`;
export const POST_RESERVED_EMAIL = `${PREFIX}POST_RESERVED_EMAIL`;

const defaultState = {
    emails: [],
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case SET_EMAIL: {
            return {
                ...state,
                emails: action.emails
            };
        }

        case requestSuffix(GET_RESERVED_EMAIL): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_RESERVED_EMAIL): {
            return {
                ...state,
                error: 'Sorry, but failed to get reserved emails',
                loading: false
            };
        }

        case GET_RESERVED_EMAIL: {
            const newEmails = action.resolved;

            return {
                ...state,
                error: '',
                loading: false,
                emails: newEmails
            };
        }

        case requestSuffix(POST_RESERVED_EMAIL): {
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };
        }

        case failSuffix(POST_RESERVED_EMAIL): {
            return {
                ...state,
                error: 'Sorry, but failed to post reserved emails',
                loading: false
            };
        }

        case POST_RESERVED_EMAIL: {
            const newEmails = action.resolved.emails;

            if (action.lastEmailValue !== null) {
                newEmails.push(action.lastEmailValue);
            }

            return {
                ...state,
                emails: newEmails,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function newEmail() {
    return (dispatch, getState) => {
        const state = getState();
        const {
            adminReservedEmail: { emails }
        } = state;
        const list = Array.from(emails);

        list.push({
            email: '',
            default: false
        });

        return dispatch({
            type: SET_EMAIL,
            emails: list
        });
    };
}

export function cancelNew() {
    return (dispatch, getState) => {
        const state = getState();
        const {
            adminReservedEmail: { emails }
        } = state;
        const list = emails.slice(0, -1);

        return dispatch({
            type: SET_EMAIL,
            emails: list
        });
    };
}

export function removeEmail(emailIndex) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const {
            adminReservedEmail: { emails }
        } = state;
        const list = Array.from(emails);

        // Check if array list is handling with a "New Email"
        const lastNewEmail = list[list.length - 1].email === '';
        const lastEmailValue = lastNewEmail ? list[list.length - 1] : null;

        // remove the element
        list.splice(emailIndex, 1);

        const body = postModelReservedEmails(list);

        return dispatch({
            type: POST_RESERVED_EMAIL,
            lastEmailValue,
            promise: api.adminApiRequest.post(
                '/reserved-email',
                body,
                token,
                secret
            )
        });
    };
}

export function saveEmail(emailIndex, newEmailValue) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const {
            adminReservedEmail: { emails }
        } = state;
        const list = Array.from(emails);

        list[emailIndex] = {
            email: newEmailValue,
            default: list[emailIndex].default
        };

        // Check if array list is handling with a "New Email"
        const lastNewEmail = list[list.length - 1].email === '';
        const lastEmailValue = lastNewEmail ? list[list.length - 1] : null;

        const body = postModelReservedEmails(list);

        return dispatch({
            type: POST_RESERVED_EMAIL,
            lastEmailValue,
            promise: api.adminApiRequest.post(
                '/reserved-email',
                body,
                token,
                secret
            )
        });
    };
}

export function getReservedEmails() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_RESERVED_EMAIL,
            promise: api.adminApiRequest.get('/reserved-email', token, secret)
        });
    };
}

function postModelReservedEmails(emails) {
    // empty values are not send
    const body = {
        emails: emails
            .filter((email) => email.email !== '')
            .map((email) => email.email)
    };

    return body;
}
