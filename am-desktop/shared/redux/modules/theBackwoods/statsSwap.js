import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/stats-swap/';

export const SEARCH_SONGS = `${PREFIX}SEARCH_SONGS`;
export const GET_TASKS = `${PREFIX}GET_TASKS`;
export const GET_SUB_TASKS = `${PREFIX}GET_SUB_TASKS`;
export const MIGRATE_DATA = `${PREFIX}MIGRATE_DATA`;

const defaultState = {
    errorTop: null,
    errorContent: null,
    loading: false,
    tasks: [],
    subTasks: [],
    migrateSongs: [],
    next: '',
    previous: ''
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_TASKS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_TASKS): {
            return {
                ...state,
                errorContent: 'Sorry, but failed trying to get the tasks',
                loading: false
            };
        }

        case GET_TASKS: {
            return {
                ...state,
                tasks: action.resolved.data,
                next: action.resolved.navigation.nextToken,
                previous: action.resolved.navigation.prevToken,
                subTasks: [],
                migrateSongs: [],
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(GET_SUB_TASKS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_SUB_TASKS): {
            return {
                ...state,
                loading: false
            };
        }

        case GET_SUB_TASKS: {
            return {
                ...state,
                subTasks: action.resolved,
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(MIGRATE_DATA): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(MIGRATE_DATA): {
            return {
                ...state,
                errorContent: action.error.message,
                loading: false
            };
        }

        case MIGRATE_DATA: {
            return {
                ...state,
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(SEARCH_SONGS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(SEARCH_SONGS): {
            return {
                ...state,
                errorContent: action.error.message,
                loading: false
            };
        }

        case SEARCH_SONGS: {
            const songs = action.resolved.map((song) => {
                return {
                    ...song,
                    source: false,
                    target: false
                };
            });

            return {
                ...state,
                loading: false,
                errorContent: null,
                migrateSongs: songs
            };
        }

        default:
            return state;
    }
}

export function getTasks() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_TASKS,
            promise: api.adminApiRequest.get('/stats-swap', token, secret)
        });
    };
}

export function nextPagination(nextToken) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        return dispatch({
            type: GET_TASKS,
            promise: api.adminApiRequest.get(
                `/stats-swap?navToken=${nextToken}`,
                token,
                secret
            )
        });
    };
}

export function previousPagination(previousToken) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        return dispatch({
            type: GET_TASKS,
            promise: api.adminApiRequest.get(
                `/stats-swap?navToken=${previousToken}`,
                token,
                secret
            )
        });
    };
}

export function getSubTasks(taskId) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_SUB_TASKS,
            promise: api.adminApiRequest.get(
                `/stats-swap/task/${taskId}`,
                token,
                secret
            )
        });
    };
}

export function migrateByUrl(targetUrl, sourceUrls, reUp) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const body = {
            targetUrl,
            sourceUrls,
            reUp
        };

        return dispatch({
            type: MIGRATE_DATA,
            promise: api.adminApiRequest.post(
                '/stats-swap/url',
                body,
                token,
                secret
            )
        });
    };
}

export function getSearchData(value) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SEARCH_SONGS,
            promise: api.adminApiRequest.get(
                `/music/search?limit=200&q=${value}&type=song&album_tracks=true`,
                token,
                secret
            )
        });
    };
}

export function migrateById(targetId, sourceIds, reUp) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const body = {
            targetId,
            sourceIds,
            reUp
        };

        return dispatch({
            type: MIGRATE_DATA,
            promise: api.adminApiRequest.post(
                '/stats-swap',
                body,
                token,
                secret
            )
        });
    };
}
