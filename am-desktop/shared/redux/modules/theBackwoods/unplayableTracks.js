import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/unplayable-tracks/';

export const GET_UNPLAYABLE_TRACKS = `${PREFIX}GET_UNPLAYABLE_TRACKS`;
export const DELETE_UNPLAYABLE_TRACK = `${PREFIX}DELETE_UNPLAYABLE_TRACK`;
const defaultState = {
    errorTop: null,
    errorContent: null,
    loading: false,
    unplayableTracks: [],
    next: '',
    previous: ''
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case GET_UNPLAYABLE_TRACKS: {
            const responseNavigation = action.resolved.navigation;
            const responseUnplayableTracks = action.resolved.data;

            return {
                ...state,
                loading: false,
                unplayableTracks: responseUnplayableTracks,
                next: responseNavigation.nextToken,
                previous: responseNavigation.prevToken
            };
        }

        case requestSuffix(GET_UNPLAYABLE_TRACKS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_UNPLAYABLE_TRACKS): {
            return {
                ...state,
                errorContent: 'Sorry, but failed to list the unplayable tracks',
                loading: false
            };
        }

        case DELETE_UNPLAYABLE_TRACK: {
            const newUnplayableTracks = Array.from(state.unplayableTracks);
            const id = action.id;

            if (action.resolved.delete === id && action.resolved.status) {
                const index = state.unplayableTracks.findIndex(
                    (unplayableTrack) => unplayableTrack.id === id
                );

                // remove the artist from the list
                newUnplayableTracks.splice(index, 1);
            }
            return {
                ...state,
                loading: false,
                artists: newUnplayableTracks
            };
        }

        case requestSuffix(DELETE_UNPLAYABLE_TRACK): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(DELETE_UNPLAYABLE_TRACK): {
            return {
                ...state,
                errorContent:
                    'Sorry, but failed to delete the unplayable music',
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getUnplayableTracks() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        return dispatch({
            type: GET_UNPLAYABLE_TRACKS,
            promise: api.adminApiRequest.get(
                '/unplayable-tracks',
                token,
                secret
            )
        });
    };
}

export function nextUnplayableTracks(next) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        return dispatch({
            type: GET_UNPLAYABLE_TRACKS,
            promise: api.adminApiRequest.get(
                `/unplayable-tracks?navToken=${next}`,
                token,
                secret
            )
        });
    };
}

export function previousUnplayableTracks(previous) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        return dispatch({
            type: GET_UNPLAYABLE_TRACKS,
            promise: api.adminApiRequest.get(
                `/unplayable-tracks?navToken=${previous}`,
                token,
                secret
            )
        });
    };
}

export function removeItem(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: DELETE_UNPLAYABLE_TRACK,
            id,
            promise: api.adminApiRequest.delete(
                `/unplayable-tracks/${id}`,
                token,
                secret
            )
        });
    };
}
