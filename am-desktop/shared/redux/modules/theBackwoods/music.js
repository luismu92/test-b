import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/music/';

export const MUSIC_SEARCH = `${PREFIX}MUSIC_SEARCH`;

const defaultState = {
    errorContent: null,
    loading: false,
    musics: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(MUSIC_SEARCH): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(MUSIC_SEARCH): {
            return {
                ...state,
                errorContent: 'Sorry, but failed trying to get the musics',
                loading: false
            };
        }

        case MUSIC_SEARCH: {
            console.log(action.resolved);
            return {
                ...state,
                musics: action.resolved,
                errorContent: null,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function searchMusic(search) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: MUSIC_SEARCH,
            promise: api.adminApiRequest.get(
                `/music/search?limit=250&offset=0&q=${search}&album_tracks=true`,
                token,
                secret
            )
        });
    };
}
