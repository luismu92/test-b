import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/follow-list/';

export const NEW_URL = `${PREFIX}NEW_URL`;
export const GET_FOLLOW_LIST = `${PREFIX}GET_FOLLOW_LIST`;
export const ORDER_ITEMS_FOLLOW_LIST = `${PREFIX}ORDER_ITEMS_FOLLOW_LIST`;
export const DELETE_FOLLOWER = `${PREFIX}DELETE_FOLLOWER`;
const defaultState = {
    errorTop: null,
    errorContent: null,
    loading: false,
    artists: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(NEW_URL): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(NEW_URL): {
            const error = action.error.message;

            return {
                ...state,
                errorTop: error,
                loading: false
            };
        }

        case NEW_URL: {
            const newArtists = Array.from(state.artists);

            newArtists.push({
                id: action.resolved.id,
                name: action.resolved.name,
                urlSlug: action.resolved.url_slug
            });

            return {
                ...state,
                loading: false,
                artists: newArtists,
                errorTop: null
            };
        }

        case GET_FOLLOW_LIST: {
            const responseFollowList = action.resolved.map((artist) => {
                return {
                    id: artist.id,
                    name: artist.name,
                    urlSlug: artist.url_slug
                };
            });

            return {
                ...state,
                loading: false,
                artists: responseFollowList
            };
        }

        case requestSuffix(GET_FOLLOW_LIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_FOLLOW_LIST): {
            return {
                ...state,
                errorContent: 'Sorry, but failed to list the followers',
                loading: false
            };
        }

        case ORDER_ITEMS_FOLLOW_LIST: {
            return {
                ...state,
                loading: false,
                artists: action.newOrder
            };
        }

        case requestSuffix(ORDER_ITEMS_FOLLOW_LIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(ORDER_ITEMS_FOLLOW_LIST): {
            return {
                ...state,
                errorContent: 'Sorry, but failed to order the list',
                loading: false
            };
        }

        case DELETE_FOLLOWER: {
            const newArtists = Array.from(state.artists);

            if (action.resolved === '') {
                const id = action.id;
                const index = state.artists.findIndex(
                    (artist) => artist.id === id
                );

                // remove the artist from the list
                newArtists.splice(index, 1);
            }
            return {
                ...state,
                loading: false,
                artists: newArtists
            };
        }

        case requestSuffix(DELETE_FOLLOWER): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(DELETE_FOLLOWER): {
            return {
                ...state,
                errorContent: 'Sorry, but failed to delete the follower',
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getFollowList() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_FOLLOW_LIST,
            promise: api.adminApiRequest.get('/follow-list', token, secret)
        });
    };
}

export function addUrl(url) {
    return (dispatch, getState) => {
        const body = { artist_url: url };
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: NEW_URL,
            promise: api.adminApiRequest.post(
                '/follow-list',
                body,
                token,
                secret
            )
        });
    };
}

export function orderItem(arrFollowersId) {
    return (dispatch, getState) => {
        const body = { artists: arrFollowersId.map((artist) => artist.id) };
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: ORDER_ITEMS_FOLLOW_LIST,
            newOrder: arrFollowersId,
            promise: api.adminApiRequest.post(
                '/follow-list/order',
                body,
                token,
                secret
            )
        });
    };
}

export function removeItem(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: DELETE_FOLLOWER,
            id,
            promise: api.adminApiRequest.delete(
                `/follow-list/${id}`,
                token,
                secret
            )
        });
    };
}
