import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/artist/';

export const GET_ARTIST = `${PREFIX}GET_ARTIST`;
export const GET_ARTISTS = `${PREFIX}GET_ARTISTS`;
export const SEARCH_ARTISTS = `${PREFIX}SEARCH_ARTISTS`;
export const VERIFY_ARTIST = `${PREFIX}VERIFY_ARTIST`;
export const SUSPENSION_WHITELIST = `${PREFIX}SUSPENSION_WHITELIST`;
export const SUSPENSION_ADMIN = `${PREFIX}SUSPENSION_ADMIN`;
export const BLOCK_ARTIST = `${PREFIX}BLOCK_ARTIST`;
export const DELETE_ARTIST = `${PREFIX}DELETE_ARTIST`;
export const AMP_ACCOUNT = `${PREFIX}AMP_ACCOUNT`;
export const NEW_ARTIST = `${PREFIX}NEW_ARTIST`;
export const UPDATE_ARTIST = `${PREFIX}UPDATE_ARTIST`;

const defaultState = {
    errorContent: null,
    errorItemOption: null,
    loading: false,
    search: false,
    artists: [],
    editableArtist: {}
};

// eslint-disable-next-line complexity
export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ARTISTS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_ARTISTS): {
            return {
                ...state,
                errorContent: 'Sorry, but failed trying to get artists',
                loading: false
            };
        }

        case GET_ARTISTS: {
            const newestArtists = action.resolved.map((artist) => {
                return {
                    ...artist.artist,
                    admin_suspension: artist.admin_suspension,
                    email: artist.email
                };
            });

            return {
                ...state,
                artists: newestArtists,
                errorContent: null,
                loading: false,
                search: false
            };
        }

        case requestSuffix(SEARCH_ARTISTS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(SEARCH_ARTISTS): {
            return {
                ...state,
                errorContent: 'Sorry, but failed trying to get artists',
                loading: false
            };
        }

        case SEARCH_ARTISTS: {
            return {
                ...state,
                artists: action.resolved,
                errorContent: null,
                loading: false,
                search: true
            };
        }

        case requestSuffix(VERIFY_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(VERIFY_ARTIST): {
            return {
                ...state,
                errorItemOption: `Sorry, but failed updating verify status: ${
                    action.error.message
                }`,
                loading: false
            };
        }

        case VERIFY_ARTIST: {
            const artists = Array.from(state.artists);
            const response = action.resolved;

            for (let i = 0; i < artists.length; i++) {
                if (artists[i].id === response.artistId.toString()) {
                    artists[i].verified = response.verified;
                    break;
                }
            }

            return {
                ...state,
                artists: artists,
                errorItemOption: null,
                loading: false
            };
        }

        case requestSuffix(SUSPENSION_WHITELIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(SUSPENSION_WHITELIST): {
            return {
                ...state,
                errorItemOption: `Sorry, but failed adding artist to suspension whitelist: ${
                    action.error.message
                }`,
                loading: false
            };
        }

        case SUSPENSION_WHITELIST: {
            const artists = Array.from(state.artists);
            const response = action.resolved;

            for (let i = 0; i < artists.length; i++) {
                if (artists[i].id === response.artistId.toString()) {
                    artists[i].suspension_whitelist =
                        response.suspension_whitelist;
                    break;
                }
            }

            return {
                ...state,
                artists: artists,
                errorItemOption: null,
                loading: false
            };
        }

        case requestSuffix(SUSPENSION_ADMIN): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(SUSPENSION_ADMIN): {
            return {
                ...state,
                errorItemOption: `Sorry, but failed suspend by admin action: ${
                    action.error.message
                }`,
                loading: false
            };
        }

        case SUSPENSION_ADMIN: {
            const artists = Array.from(state.artists);
            const response = action.resolved;

            for (let i = 0; i < artists.length; i++) {
                if (artists[i].id === response.artistId.toString()) {
                    artists[i].admin_suspension = response.admin_suspension;
                    break;
                }
            }

            return {
                ...state,
                artists: artists,
                errorItemOption: null,
                loading: false
            };
        }

        case requestSuffix(BLOCK_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(BLOCK_ARTIST): {
            return {
                ...state,
                errorItemOption: `Sorry, but failed blocking the artist: ${
                    action.error.message
                }`,
                loading: false
            };
        }

        case BLOCK_ARTIST: {
            const artists = Array.from(state.artists);
            const response = action.resolved;

            for (let i = 0; i < artists.length; i++) {
                if (artists[i].id === response.artistId.toString()) {
                    artists[i].can_upload = response.can_upload === 'yes';
                    break;
                }
            }

            return {
                ...state,
                artists: artists,
                errorItemOption: null,
                loading: false
            };
        }

        case requestSuffix(DELETE_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(DELETE_ARTIST): {
            return {
                ...state,
                errorItemOption: `Sorry, but failed deleting the artist: ${
                    action.error.message
                }`,
                loading: false
            };
        }

        case DELETE_ARTIST: {
            const artists = Array.from(state.artists);
            const response = action.resolved;
            const index = artists.findIndex(
                (artist) => artist.id === response.artistId.toString()
            );

            // Remove the artist from the list
            artists.splice(index, 1);

            return {
                ...state,
                artists: artists,
                errorItemOption: null,
                loading: false
            };
        }

        case requestSuffix(AMP_ACCOUNT): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(AMP_ACCOUNT): {
            return {
                ...state,
                errorItemOption: `Sorry, but failed creating AMP account: ${
                    action.error.message
                }`,
                loading: false
            };
        }

        case AMP_ACCOUNT: {
            const artists = Array.from(state.artists);
            const response = action.resolved;

            for (let i = 0; i < artists.length; i++) {
                if (artists[i].id === response.artistId.toString()) {
                    artists[i].label_owner = response.amp || true;
                    break;
                }
            }

            return {
                ...state,
                artists: artists,
                errorItemOption: null,
                loading: false
            };
        }

        case requestSuffix(GET_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_ARTIST): {
            return {
                ...state,
                errorContent: action.error.message,
                loading: false
            };
        }

        case GET_ARTIST: {
            return {
                ...state,
                editableArtist: action.resolved,
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(NEW_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(NEW_ARTIST): {
            return {
                ...state,
                errorContent: action.error.message,
                loading: false
            };
        }

        case NEW_ARTIST: {
            return {
                ...state,
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(UPDATE_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(UPDATE_ARTIST): {
            return {
                ...state,
                errorContent: action.error.message,
                loading: false
            };
        }

        case UPDATE_ARTIST: {
            return {
                ...state,
                errorContent: null,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getArtist(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_ARTIST,
            promise: api.adminApiRequest.get(`/artist/${id}`, token, secret)
        });
    };
}

export function getArtists(limit, offset) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_ARTISTS,
            promise: api.adminApiRequest.get(
                `/artists/listnew?limit=${limit}&offset=${offset}`,
                token,
                secret
            )
        });
    };
}

export function getAllArtists() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SEARCH_ARTISTS,
            promise: api.adminApiRequest.get(
                '/artists/search?limit=20&offset=0&q=&genre=false&verified=false&ghost=false',
                token,
                secret
            )
        });
    };
}

export function searchArtists(search, genre, verified, ghost, tastemaker) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SEARCH_ARTISTS,
            promise: api.adminApiRequest.get(
                `/artists/search?limit=20&offset=0&q=${search}&genre=${genre}&verified=${verified}&ghost=${ghost}&tastemaker=${tastemaker}`,
                token,
                secret
            )
        });
    };
}

export function verifiedArtists() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SEARCH_ARTISTS,
            promise: api.adminApiRequest.get(
                '/artists/search?limit=20&offset=0&q=&genre=false&verified=true&ghost=false',
                token,
                secret
            )
        });
    };
}

export function ghostArtists() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SEARCH_ARTISTS,
            promise: api.adminApiRequest.get(
                '/artists/search?limit=20&offset=0&q=&genre=false&verified=false&ghost=true',
                token,
                secret
            )
        });
    };
}

export function tastemakerArtists() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SEARCH_ARTISTS,
            promise: api.adminApiRequest.get(
                '/artists/search?limit=20&offset=0&q=&genre=false&verified=false&ghost=false&tastemaker=true',
                token,
                secret
            )
        });
    };
}

export function verifyArtist(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: VERIFY_ARTIST,
            promise: api.adminApiRequest.put(
                `/artist/verify/${id}`,
                token,
                secret
            )
        });
    };
}

export function suspensionWhiteList(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SUSPENSION_WHITELIST,
            promise: api.adminApiRequest.put(
                `/artist/whitelist/${id}`,
                token,
                secret
            )
        });
    };
}

export function suspensionAdmin(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: SUSPENSION_ADMIN,
            promise: api.adminApiRequest.put(
                `/artist/suspend/${id}`,
                token,
                secret
            )
        });
    };
}

export function blockArtist(id, operation, clear) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const body = {
            id,
            operation,
            clear
        };

        return dispatch({
            type: BLOCK_ARTIST,
            promise: api.adminApiRequest.post(
                '/artist/upload',
                body,
                token,
                secret
            )
        });
    };
}

export function deleteArtist(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: DELETE_ARTIST,
            promise: api.adminApiRequest.delete(
                `/artist/delete/${id}`,
                token,
                secret
            )
        });
    };
}

export function ampAccount(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: AMP_ACCOUNT,
            promise: api.adminApiRequest.put(`/artist/amp/${id}`, token, secret)
        });
    };
}

export function newArtist(body) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: NEW_ARTIST,
            promise: api.adminApiRequest.post(
                '/artist/add',
                body,
                token,
                secret
            )
        });
    };
}

export function updateArtist(body) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: UPDATE_ARTIST,
            promise: api.adminApiRequest.post(
                '/artist/edit',
                body,
                token,
                secret
            )
        });
    };
}
