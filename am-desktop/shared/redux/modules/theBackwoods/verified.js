import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/verified/';

export const GET_VERIFIED_FEED = `${PREFIX}GET_VERIFIED_FEED`;

const defaultState = {
    verifiedUsers: {},
    error: null,
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_VERIFIED_FEED): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_VERIFIED_FEED): {
            const error = 'Sorry, but failed to list the verified Artists';
            console.log(error);

            return {
                ...state,
                error: error,
                loading: false
            };
        }

        case GET_VERIFIED_FEED: {
            const resultVerified = action.resolved;

            return {
                ...state,
                loading: false,
                verifiedUsers: resultVerified
            };
        }

        default:
            return state;
    }
}

export function getVerifiedFeed() {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_VERIFIED_FEED,
            promise: api.adminApiRequest.get('/feed/verified', token, secret)
        });
    };
}
