import api from 'api/index';

const PREFIX = 'am/the-backwoods/image-resize/';

export const POST_IMAGE_RESIZE = `${PREFIX}POST_IMAGE_RESIZE`;

const defaultState = {
    imageUri: null,
    loading: false,
    error: ''
};

export default function reducer(state = defaultState) {
    return state;
}

export function updateImageResizeResource(body) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: POST_IMAGE_RESIZE,
            promise: api.adminApiRequest.post(
                '/image-resize/image/_resize',
                body,
                token,
                secret
            )
        });
    };
}
