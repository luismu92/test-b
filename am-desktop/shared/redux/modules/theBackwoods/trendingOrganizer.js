import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/trending-organizer/';

export const SET_CONTEXT = `${PREFIX}SET_CONTEXT`;
export const GET_LIST = `${PREFIX}GET_LIST`;
export const SET_LIST = `${PREFIX}SET_LIST`;
export const SAVE_LIST = `${PREFIX}SAVE_LIST`;
export const GET_MUSIC = `${PREFIX}GET_MUSIC`;

const defaultState = {
    trendingList: [],
    error: null,
    context: 'organizer',
    genre: 'global',
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case SET_LIST: {
            return {
                ...state,
                trendingList: action.list
            };
        }

        case SET_CONTEXT: {
            return {
                ...state,
                context: action.context,
                genre: action.genre
            };
        }

        case GET_LIST: {
            const trendingList = action.resolved;

            return {
                ...state,
                error: '',
                loading: false,
                trendingList: trendingList
            };
        }

        case requestSuffix(GET_LIST): {
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };
        }

        case failSuffix(GET_LIST): {
            return {
                ...state,
                error: 'Sorry, but failed to get the trending list',
                loading: false
            };
        }

        case SAVE_LIST: {
            return {
                ...state,
                error: defaultState.error,
                loading: false
            };
        }

        case requestSuffix(SAVE_LIST): {
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };
        }

        case failSuffix(SAVE_LIST): {
            return {
                ...state,
                error: 'Sorry, but failed to save the trending list',
                loading: false
            };
        }

        default:
            return state;
    }
}

export function setList(list = []) {
    return (dispatch) => {
        return dispatch({
            type: SET_LIST,
            list: list
        });
    };
}

export function setContext(context = 'organizer', genre = 'global') {
    return (dispatch, getState) => {
        const state = getState();
        const currentContext = state.adminTrendingOrganizer.context;
        const currentGenre = state.adminTrendingOrganizer.genre;

        // Don't need to reset context
        if (currentContext === context && currentGenre === genre) {
            return false;
        }

        return dispatch({
            type: SET_CONTEXT,
            context: context,
            genre: genre
        });
    };
}

export function getMusic(url) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        url = url.replace(process.env.AM_URL, '');

        return dispatch({
            type: GET_MUSIC,
            promise: api.adminApiRequest.get(
                `/music-by-url${url}`,
                token,
                secret
            )
        });
    };
}

export function getList(context, genre = 'global') {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_LIST,
            promise: api.adminApiRequest.get(
                `/trending?context=${context}&genre=${genre}`,
                token,
                secret
            )
        });
    };
}

export function saveList(context, genre = 'global', listData) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        const body = {
            context,
            genre,
            listData
        };

        return dispatch({
            type: SAVE_LIST,
            promise: api.adminApiRequest.post('/trending', body, token, secret)
        });
    };
}
