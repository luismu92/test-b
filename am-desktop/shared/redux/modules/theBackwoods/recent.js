import api from 'api/index';
import { failSuffix, requestSuffix, parseApiObject } from 'utils/index';
import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from '../user';
import { ADD_SONG_TO_PLAYLIST, DELETE_SONG_FROM_PLAYLIST } from '../playlist';
import { TRACK_PLAY } from '../stats';
import { BUMP_MUSIC, BUMP_MUSIC_ONE_SPOT } from '../admin';

import {
    CHART_TYPE_TOTAL,
    COLLECTION_TYPE_RECENTLY_ADDED
} from 'constants/index';

const PREFIX = 'am/the-backwoods/recent/';

const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const FETCH_SONGS = `${PREFIX}FETCH_SONGS`;
const SET_GENRE = `${PREFIX}SET_GENRE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const PREV_PAGE = `${PREFIX}PREV_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;

const defaultState = {
    loading: false,
    activeGenre: '',
    activeContext: COLLECTION_TYPE_RECENTLY_ADDED,
    activeTimePeriod: CHART_TYPE_TOTAL,
    error: null,
    page: 1,
    limit: 20,
    onLastPage: false,
    showNumbers: false,
    list: []
};

export default function musicReducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(FETCH_SONGS):
            return {
                ...state,
                errors: defaultState.error,
                loading: true
            };

        case failSuffix(FETCH_SONGS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case FETCH_SONGS: {
            const results = action.resolved.results;
            let newData = results;
            let page = action.page;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (!results.length) {
                onLastPage = true;
                page = action.page - 1;
            }

            return {
                ...state,
                list: newData,
                loading: false,
                limit: action.limit,
                showNumbers: action.showNumbers,
                page,
                onLastPage
            };
        }

        case CLEAR_LIST:
            return {
                ...state,
                list: []
            };

        case SET_GENRE: {
            let page = state.page;
            let onLastPage = state.onLastPage;
            let list = state.list;

            if (state.activeGenre !== action.genre) {
                page = 1;
                list = [];
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeGenre: action.genre,
                list,
                page,
                onLastPage
            });
        }

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case PREV_PAGE:
            return Object.assign({}, state, {
                page: Math.max(1, state.page - 1)
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case failSuffix(UNFAVORITE_ITEM):
        case requestSuffix(FAVORITE_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(FAVORITE_ITEM):
        case requestSuffix(UNFAVORITE_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(UNREPOST_ITEM):
        case requestSuffix(REPOST_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(REPOST_ITEM):
        case requestSuffix(UNREPOST_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(DELETE_SONG_FROM_PLAYLIST):
        case requestSuffix(ADD_SONG_TO_PLAYLIST): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.songId) {
                        newObj.stats['playlists-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(ADD_SONG_TO_PLAYLIST):
        case requestSuffix(DELETE_SONG_FROM_PLAYLIST): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.songId) {
                        newObj.stats['playlists-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case TRACK_PLAY: {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };
                    const itemId = action.id;
                    const albumId = action.options.album_id;

                    if (
                        (obj.type === 'album' &&
                            typeof albumId === 'number' &&
                            obj.id === albumId) ||
                        (obj.type !== 'album' && obj.id === itemId)
                    ) {
                        // Only update if this is the first track play call, ie not the 30 second track call
                        if (!action.options.time) {
                            newObj.stats['plays-raw'] += 1;
                        }
                    }

                    return newObj;
                })
            });
        }

        // Since this is admin only, i wont worry about this failing and resetting
        // the list back to the previous state
        case requestSuffix(BUMP_MUSIC): {
            return {
                ...state,
                list: [action.item].concat(
                    state.list.filter((item) => item.id !== action.item.id)
                )
            };
        }

        // Since this is admin only, i wont worry about this failing and resetting
        // the list back to the previous state
        case requestSuffix(BUMP_MUSIC_ONE_SPOT): {
            const currentIndex = state.list.findIndex(
                (item) => item.id === action.item.id
            );

            if (currentIndex === -1) {
                return state;
            }

            const item = state.list[currentIndex];
            const firstHalf = state.list.slice(0, currentIndex);
            const secondHalf = state.list.slice(currentIndex + 1);
            const itemToSwitchWith = firstHalf.pop();

            firstHalf.push(item);
            firstHalf.push(itemToSwitchWith);

            const newList = [...firstHalf, ...secondHalf];

            return {
                ...state,
                list: newList
            };
        }

        default:
            return state;
    }
}

export function fetchSongs(options = {}) {
    return (dispatch, getState) => {
        const state = getState().adminRecent;
        const { token, secret } = getState().currentUser;

        const {
            genre = state.activeGenre,
            page = state.page,
            prependNewData = false
        } = options;

        const fetchPromise = (p) => {
            let url = `/music/recent/page/${p}`;

            if (genre) {
                url = `/music/${genre}/recent/page/${p}`;
            }

            return api.adminApiRequest.get(url, token, secret).then((data) => {
                return {
                    ...data,
                    results: data.results.map(parseApiObject)
                };
            });
        };

        const playerFetcher = (p) => {
            return fetchPromise(p).then((data) => {
                return data.results;
            });
        };

        return dispatch({
            type: FETCH_SONGS,
            prependNewData,
            page,
            playerFetcher,
            promise: fetchPromise(page)
        });
    };
}

export function setGenre(genre = defaultState.activeGenre) {
    return {
        type: SET_GENRE,
        genre
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function prevPage() {
    return {
        type: PREV_PAGE
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
