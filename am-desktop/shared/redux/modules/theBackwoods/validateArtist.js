import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/the-backwoods/validate-artist/';

export const GET_ARTISTS = `${PREFIX}GET_ARTISTS`;
export const UPDATE_ARTIST = `${PREFIX}UPDATE_ARTIST`;
export const CHECK_TWITTER = `${PREFIX}CHECK_TWITTER`;

const defaultState = {
    errorContent: null,
    loading: false,
    artists: [],
    nextPage: null,
    previousPage: null,
    totalPending: 0
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ARTISTS): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(GET_ARTISTS): {
            return {
                ...state,
                errorContent: 'Sorry, but failed trying to get the artists',
                loading: false
            };
        }

        case GET_ARTISTS: {
            return {
                ...state,
                artists: action.resolved.artists,
                nextPage: action.resolved.next,
                previousPage: action.resolved.previous,
                totalPending: action.resolved.totalPending,
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(UPDATE_ARTIST): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(UPDATE_ARTIST): {
            return {
                ...state,
                errorContent: 'Sorry, but failed trying to update the artist',
                loading: false
            };
        }

        case UPDATE_ARTIST: {
            const artists = Array.from(state.artists);
            const newArtists = artists.filter(
                (artist) => artist.artist.id !== action.resolved.artistId
            );

            return {
                ...state,
                artists: newArtists,
                errorContent: null,
                loading: false
            };
        }

        case requestSuffix(CHECK_TWITTER): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(CHECK_TWITTER): {
            return {
                ...state,
                loading: false
            };
        }

        case CHECK_TWITTER: {
            const artists = Array.from(state.artists);

            // Check the position of the artist in the list
            const index = artists.findIndex(
                (artist) => artist.artist.id === action.resolved.id
            );

            // Twitter Handle
            artists[index].artist = {
                ...artists[index].artist,
                twitter: action.resolved.twitter
            };

            return {
                ...state,
                artists: artists,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getArtists(page) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: GET_ARTISTS,
            promise: api.adminApiRequest.get(
                `/validate/artist/${page}`,
                token,
                secret
            )
        });
    };
}

export function updateArtistValidate(id, newStatus) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: UPDATE_ARTIST,
            promise: api.adminApiRequest.put(
                `/validate/artist/${id}/status/${newStatus}`,
                token,
                secret
            )
        });
    };
}

export function checkArtistTwitter(id) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;

        return dispatch({
            type: CHECK_TWITTER,
            promise: api.adminApiRequest.get(
                `/validate/twitter/${id}`,
                token,
                secret
            )
        });
    };
}
