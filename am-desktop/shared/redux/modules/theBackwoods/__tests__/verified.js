/* eslint-disable camelcase */
import adminFollowListReducer, { GET_VERIFIED_FEED } from '../verified';
import { configureStore } from 'redux/configureStore';
import rootReducer from '../../../reducer';
import { requestSuffix, failSuffix } from 'utils/index';

function getDefaultStore(initialState) {
    return configureStore(
        {
            adminFollowList: {
                ...adminFollowListReducer(undefined, {
                    type: 'gimmeDefaultState'
                }),
                ...adminFollowListReducer(initialState, {
                    type: 'gimmeDefaultState'
                })
            }
        },
        rootReducer
    );
}

function createFakeData(size) {
    const fakedata = [];

    for (let i = 0; i < size; i += 1) {
        fakedata.push({
            id: i,
            name: `fakeName${i}`,
            url_slug: `https://audiomacak.test/fakeName${i}`
        });
    }
    return fakedata;
}

describe('Redux actions, change final state', () => {
    test('GET_VERIFIED_FEED - resolved', () => {
        const fakeData = {};
        const fakeOutput = {};

        const store = getDefaultStore({
            verifiedUsers: fakeOutput,
            loading: false,
            error: ''
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: GET_VERIFIED_FEED,
                resolved: fakeData
            }
        );

        expect(output).toEqual(resultState);
    });

    test('GET_VERIFIED_FEED - rejected', () => {
        const fakeData = createFakeData(2);

        const store = getDefaultStore({
            verifiedUsers: fakeData,
            error: 'Sorry, but failed to list the verified Artists',
            loading: false
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: failSuffix(GET_VERIFIED_FEED)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('GET_VERIFIED_FEED - loading', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: requestSuffix(GET_VERIFIED_FEED)
            }
        );

        expect(output).toEqual(resultState);
    });
});
