/* global test, expect, describe */
import adminStatsSwapReducer, {
    GET_TASKS,
    GET_SUB_TASKS,
    MIGRATE_DATA,
    SEARCH_SONGS
} from '../statsSwap';
import { configureStore } from 'redux/configureStore';
import rootReducer from '../../../reducer';
import { requestSuffix, failSuffix } from 'utils/index';

function getDefaultStore(initialState) {
    return configureStore(
        {
            adminStatsSwap: {
                ...adminStatsSwapReducer(undefined, {
                    type: 'gimmeDefaultState'
                }),
                ...adminStatsSwapReducer(initialState, {
                    type: 'gimmeDefaultState'
                })
            }
        },
        rootReducer
    );
}

function createFakeTasksWithPag(size) {
    const prevToken = '';
    const nextToken = '';
    const fakeData = {};
    const fakeDataArr = [];
    const fakeNavigation = { prevToken, nextToken };

    for (let i = 0; i < size; i += 1) {
        fakeDataArr.push({
            id: `${i + 1}`,
            status: 'complete'
        });
    }

    fakeData.data = fakeDataArr;
    fakeData.navigation = fakeNavigation;
    return fakeData;
}

function createFakeTasks(size) {
    const fakeData = [];

    for (let i = 0; i < size; i += 1) {
        fakeData.push({
            id: `${i + 1}`,
            status: 'complete'
        });
    }
    return fakeData;
}

function createFakeSongs(size) {
    const fakeData = [];
    for (let i = 0; i < size; i += 1) {
        fakeData.push({
            id: `${i + 1}`,
            title: `Song_${i + 1}`,
            uploader: { name: `Artist_${i + 1}` },
            upload: Date.now()
        });
    }
    return fakeData;
}

describe('Redux actions, change final state', () => {
    test('GET_TASKS - resolved', () => {
        const fakeData = createFakeTasksWithPag(2);

        const store = getDefaultStore({
            tasks: fakeData.data,
            next: fakeData.navigation.nextToken,
            previous: fakeData.navigation.prevToken,
            loading: false,
            errorContent: null
        });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: GET_TASKS,
            resolved: fakeData
        });

        expect(output).toEqual(resultState);
    });

    test('GET_TASKS - rejected', () => {
        const store = getDefaultStore({
            loading: false,
            errorContent: 'Sorry, but failed trying to get the tasks'
        });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: failSuffix(GET_TASKS)
        });

        expect(output).toEqual(resultState);
    });

    test('GET_TASKS - request', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: requestSuffix(GET_TASKS)
        });

        expect(output).toEqual(resultState);
    });

    test('GET_SUB_TASKS - resolved', () => {
        const fakeData = createFakeTasks(3);

        const store = getDefaultStore({
            subTasks: fakeData,
            loading: false,
            errorContent: null
        });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: GET_SUB_TASKS,
            resolved: fakeData
        });

        expect(output).toEqual(resultState);
    });

    test('GET_SUB_TASKS - rejected', () => {
        const store = getDefaultStore({ loading: false });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: failSuffix(GET_SUB_TASKS)
        });

        expect(output).toEqual(resultState);
    });

    test('GET_SUB_TASKS - request', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: requestSuffix(GET_SUB_TASKS)
        });

        expect(output).toEqual(resultState);
    });

    test('MIGRATE_DATA - resolved', () => {
        const store = getDefaultStore({ loading: false, errorContent: null });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: MIGRATE_DATA
        });

        expect(output).toEqual(resultState);
    });

    test('MIGRATE_DATA - rejected', () => {
        const store = getDefaultStore({
            loading: false,
            errorContent: 'testError'
        });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: failSuffix(MIGRATE_DATA),
            error: { message: 'testError' }
        });

        expect(output).toEqual(resultState);
    });

    test('MIGRATE_DATA - request', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: requestSuffix(MIGRATE_DATA)
        });

        expect(output).toEqual(resultState);
    });

    test('SEARCH_SONGS - resolved', () => {
        const fakeSongs = createFakeSongs(1);
        const expected = [];
        fakeSongs.forEach((song) => {
            expected.push({
                ...song,
                source: false,
                target: false
            });
        });
        const store = getDefaultStore({
            loading: false,
            errorContent: null,
            migrateSongs: expected
        });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: SEARCH_SONGS,
            resolved: fakeSongs
        });

        expect(output).toEqual(resultState);
    });

    test('SEARCH_SONGS - rejected', () => {
        const store = getDefaultStore({
            loading: false,
            errorContent: 'testError'
        });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: failSuffix(SEARCH_SONGS),
            error: { message: 'testError' }
        });

        expect(output).toEqual(resultState);
    });

    test('SEARCH_SONGS - request', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminStatsSwap;
        const output = adminStatsSwapReducer(store.getState().adminStatsSwap, {
            type: requestSuffix(SEARCH_SONGS)
        });

        expect(output).toEqual(resultState);
    });
});
