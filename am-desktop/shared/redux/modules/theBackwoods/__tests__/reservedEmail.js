/* global test, expect, describe */
import adminReservedEmailReducer, * as reduxReservedEmail from '../reservedEmail';
import { configureStore } from 'redux/configureStore';
import rootReducer from '../../../reducer';
import { requestSuffix, failSuffix } from 'utils/index';

function getDefaultStore(initialState) {
    return configureStore(
        {
            adminReservedEmail: {
                ...adminReservedEmailReducer(undefined, {
                    type: 'gimmeDefaultState'
                }),
                ...adminReservedEmailReducer(initialState, {
                    type: 'gimmeDefaultState'
                })
            }
        },
        rootReducer
    );
}

function createFakeData(size) {
    const fakeEmail = [];

    for (let i = 0; i < size; i += 1) {
        fakeEmail.push({
            email: `fakeEmail${i + 1}@audiomack.com`,
            default: false
        });
    }
    return fakeEmail;
}

describe('Redux actions, change final state', () => {
    test('New email action, add a new email to the list', () => {
        // Email length of 2
        const store = getDefaultStore({
            emails: createFakeData(2),
            loading: false,
            error: ''
        });
        const expectedNewValue = {
            email: '',
            default: false
        };

        store.dispatch(reduxReservedEmail.newEmail());
        const resultState = store.getState().adminReservedEmail;

        expect(resultState.emails.length).toEqual(3);
        expect(resultState.emails[2]).toEqual(expectedNewValue);
    });

    test('CancelNew email action, cancel a new email', () => {
        // Email length of 2
        const store = getDefaultStore({
            emails: createFakeData(2),
            loading: false,
            error: ''
        });

        store.dispatch(reduxReservedEmail.cancelNew());
        const resultState = store.getState().adminReservedEmail;

        expect(resultState.emails.length).toEqual(1);
    });

    test('getReservedEmails promises - resolved', () => {
        const fakeArray = createFakeData(2);
        const store = getDefaultStore({
            emails: fakeArray,
            loading: false,
            error: ''
        });
        const resultState = store.getState().adminReservedEmail;

        // Import GET_RESERVED_EMAIL constant here - SC
        const output = adminReservedEmailReducer(
            store.getState().adminReservedEmail,
            {
                type: reduxReservedEmail.GET_RESERVED_EMAIL,
                // resolved because when a promise runs, and resolves successfully, it gets put
                // into this resolved key
                resolved: fakeArray
            }
        );

        expect(output).toEqual(resultState);
    });

    test('getReservedEmails promises - rejected', () => {
        const store = getDefaultStore({
            emails: createFakeData(0),
            loading: false,
            error: 'Sorry, but failed to get reserved emails'
        });
        const resultState = store.getState().adminReservedEmail;
        const output = adminReservedEmailReducer(
            store.getState().adminReservedEmail,
            {
                type: failSuffix(reduxReservedEmail.GET_RESERVED_EMAIL)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('getReservedEmails promises - loading', () => {
        const store = getDefaultStore({
            emails: createFakeData(0),
            loading: true,
            error: ''
        });
        const resultState = store.getState().adminReservedEmail;
        const output = adminReservedEmailReducer(
            store.getState().adminReservedEmail,
            {
                type: requestSuffix(reduxReservedEmail.GET_RESERVED_EMAIL)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('postReservedEmails promises - resolved', () => {
        const fakeArray = createFakeData(2);
        const store = getDefaultStore({
            emails: fakeArray,
            loading: false,
            error: ''
        });
        const resultState = store.getState().adminReservedEmail;
        const output = adminReservedEmailReducer(
            store.getState().adminReservedEmail,
            {
                type: reduxReservedEmail.POST_RESERVED_EMAIL,
                resolved: {
                    emails: fakeArray
                }
            }
        );

        expect(output).toEqual(resultState);
    });

    test('postReservedEmails promises - rejected', () => {
        const store = getDefaultStore({
            emails: createFakeData(0),
            loading: false,
            error: 'Sorry, but failed to post reserved emails'
        });
        const resultState = store.getState().adminReservedEmail;
        const output = adminReservedEmailReducer(
            store.getState().adminReservedEmail,
            {
                type: failSuffix(reduxReservedEmail.POST_RESERVED_EMAIL)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('postReservedEmails promises - loading', () => {
        const store = getDefaultStore({
            emails: createFakeData(0),
            loading: true
        });
        const resultState = store.getState().adminReservedEmail;
        const output = adminReservedEmailReducer(
            store.getState().adminReservedEmail,
            {
                type: requestSuffix(reduxReservedEmail.POST_RESERVED_EMAIL)
            }
        );

        expect(output).toEqual(resultState);
    });
});
