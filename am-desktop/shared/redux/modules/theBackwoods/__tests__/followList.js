/* eslint-disable camelcase */
import adminFollowListReducer, {
    GET_FOLLOW_LIST,
    NEW_URL,
    DELETE_FOLLOWER,
    ORDER_ITEMS_FOLLOW_LIST
} from '../followList';
import { configureStore } from 'redux/configureStore';
import rootReducer from '../../../reducer';
import { requestSuffix, failSuffix } from 'utils/index';

function getDefaultStore(initialState) {
    return configureStore(
        {
            adminFollowList: {
                ...adminFollowListReducer(undefined, {
                    type: 'gimmeDefaultState'
                }),
                ...adminFollowListReducer(initialState, {
                    type: 'gimmeDefaultState'
                })
            }
        },
        rootReducer
    );
}

function createFakeData(size) {
    const fakedata = [];

    for (let i = 0; i < size; i += 1) {
        fakedata.push({
            id: i,
            name: `fakeName${i}`,
            url_slug: `https://audiomacak.test/fakeName${i}`
        });
    }
    return fakedata;
}

describe('Redux actions, change final state', () => {
    test('GET_FOLLOW_LIST - resolved', () => {
        const fakeData = createFakeData(1);
        const fakeOutput = fakeData.map((data) => {
            return {
                urlSlug: data.url_slug,
                id: data.id,
                name: data.name
            };
        });

        const store = getDefaultStore({
            artists: fakeOutput,
            loading: false,
            errorTop: ''
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: GET_FOLLOW_LIST,
                resolved: fakeData
            }
        );

        expect(output).toEqual(resultState);
    });

    test('GET_FOLLOW_LIST - rejected', () => {
        const fakeData = createFakeData(2);

        const store = getDefaultStore({
            artists: fakeData,
            errorContent: 'Sorry, but failed to list the followers',
            loading: false
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: failSuffix(GET_FOLLOW_LIST)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('GET_FOLLOW_LIST - loading', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: requestSuffix(GET_FOLLOW_LIST)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('NEW_URL - resolved', () => {
        const fakeData = createFakeData(3);
        const fakeArtists = fakeData.map((data) => {
            return {
                urlSlug: data.url_slug,
                id: data.id,
                name: data.name
            };
        });

        const fakeArtist = {
            id: 2,
            name: 'fakeName2',
            url_slug: 'https://audiomacak.test/fakeName2'
        };

        const store = getDefaultStore({
            artists: [fakeArtists[0], fakeArtists[1]],
            errorTop: null
        });
        const resultState = store.getState().adminFollowList;

        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: NEW_URL,
                resolved: fakeArtist
            }
        );

        resultState.artists = fakeArtists;
        expect(output).toEqual(resultState);
    });

    test('NEW_URL - rejected', () => {
        const store = getDefaultStore({
            errorContent: 'Sorry, but failed to list the followers',
            errorTop: ''
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: failSuffix(NEW_URL),
                error: { message: '' }
            }
        );

        expect(output).toEqual(resultState);
    });

    test('NEW_URL - loading', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: requestSuffix(NEW_URL)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('ORDER_ITEMS_FOLLOW_LIST - resolved', () => {
        const fakeData = createFakeData(2);
        const store = getDefaultStore({
            artists: fakeData,
            loading: false,
            errorTop: ''
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: ORDER_ITEMS_FOLLOW_LIST,
                newOrder: fakeData
            }
        );

        expect(output).toEqual(resultState);
    });

    test('ORDER_ITEMS_FOLLOW_LIST - rejected', () => {
        const store = getDefaultStore({
            errorContent: 'Sorry, but failed to order the list',
            loading: false
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: failSuffix(ORDER_ITEMS_FOLLOW_LIST)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('ORDER_ITEMS_FOLLOW_LIST - loading', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: requestSuffix(ORDER_ITEMS_FOLLOW_LIST)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('DELETE_FOLLOWER - resolved', () => {
        const fakeData = createFakeData(2);
        const store = getDefaultStore({
            artists: fakeData,
            loading: false,
            errorTop: ''
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: DELETE_FOLLOWER,
                resolved: {
                    fakeData
                }
            }
        );

        expect(output).toEqual(resultState);
    });

    test('DELETE_FOLLOWER - rejected', () => {
        const store = getDefaultStore({
            errorContent: 'Sorry, but failed to delete the follower',
            loading: false
        });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: failSuffix(DELETE_FOLLOWER)
            }
        );

        expect(output).toEqual(resultState);
    });

    test('DELETE_FOLLOWER - loading', () => {
        const store = getDefaultStore({ loading: true });
        const resultState = store.getState().adminFollowList;
        const output = adminFollowListReducer(
            store.getState().adminFollowList,
            {
                type: requestSuffix(DELETE_FOLLOWER)
            }
        );

        expect(output).toEqual(resultState);
    });
});
