import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/monthlyRevenue/';

const GET_MONTHLY_REVENUE = `${PREFIX}GET_MONTHLY_REVENUE`;

const defaultState = {
    data: [],
    loading: false,
    loadingCsv: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_MONTHLY_REVENUE): {
            if (action.options.csv) {
                return {
                    ...state,
                    loadingCsv: true
                };
            }

            return {
                ...state,
                error: defaultState.error,
                loading: true
            };
        }

        case failSuffix(GET_MONTHLY_REVENUE): {
            if (action.options.csv) {
                return {
                    ...state,
                    loadingCsv: false
                };
            }

            return {
                ...state,
                error: action.error,
                loading: false
            };
        }

        case GET_MONTHLY_REVENUE: {
            if (action.options.csv) {
                return {
                    ...state,
                    loadingCsv: false
                };
            }

            return {
                ...state,
                data: action.resolved,
                loading: false
            };
        }

        default:
            return state;
    }
}

export function getMonthlyRevenue(options = {}) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;
        const opts = {
            ...options,
            type: 'monthly'
        };

        return dispatch({
            type: GET_MONTHLY_REVENUE,
            options,
            promise: api.monetization.revenue(token, secret, opts)
        });
    };
}
