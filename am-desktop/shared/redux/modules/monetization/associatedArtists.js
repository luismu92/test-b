import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/associatedArtists/';

const GET_ASSOCIATED_ARTISTS = `${PREFIX}GET_ASSOCIATED_ARTISTS`;
const SET_QUERY = `${PREFIX}SET_QUERY`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const REPLACE_MUSIC = `${PREFIX}REPLACE_MUSIC`;
const CREATE_NEW_ARTIST = `${PREFIX}CREATE_NEW_ARTIST`;
const SEND_CLAIM_PROFILE_MAIL = `${PREFIX}SEND_CLAIM_PROFILE_MAIL`;

const defaultState = {
    list: [],
    page: 1,
    query: '',
    onLastPage: false,
    loading: false,
    error: null,
    isCreatingAccount: false,
    createAccountError: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ASSOCIATED_ARTISTS):
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };

        case failSuffix(GET_ASSOCIATED_ARTISTS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case requestSuffix(CREATE_NEW_ARTIST):
            return {
                ...state,
                createAccountError: defaultState.createAccountError,
                isCreatingAccount: true
            };

        case failSuffix(CREATE_NEW_ARTIST):
            return {
                ...state,
                createAccountError: action.error,
                isCreatingAccount: false
            };

        case CREATE_NEW_ARTIST:
            return {
                ...state,
                isCreatingAccount: false,
                list: state.list
                    .filter((artist) => {
                        return artist.id !== action.resolved.id;
                    })
                    .concat(action.resolved)
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case GET_ASSOCIATED_ARTISTS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (action.page !== 1 && action.page >= state.page) {
                const currentIds = state.list.map((artist) => {
                    return artist.id;
                });

                newData = Array.from(state.list);
                results.forEach((artist) => {
                    if (!currentIds.includes(artist.id)) {
                        newData.push(artist);
                    }
                });
            }

            if (
                action.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return {
                ...state,
                list: newData,
                page: action.page,
                query: action.query,
                onLastPage,
                loading: false
            };
        }

        case SET_QUERY:
            return {
                ...state,
                query: action.query
            };

        case REPLACE_MUSIC:
            return {
                ...state,
                list: state.list.map((item) => {
                    if (item.id === action.resolved.id) {
                        return action.resolved;
                    }

                    return item;
                })
            };

        default:
            return state;
    }
}

export function getAssociatedArtists(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().monetizationAssociatedArtists;
        const { limit, page = state.page, query = state.query } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_ASSOCIATED_ARTISTS,
            page: finalPage,
            query,
            limit: finalLimit,
            promise: api.monetization.associatedArtists(token, secret, {
                query,
                page: finalPage,
                limit: finalLimit
            })
        });
    };
}

export function setAssociatedQuery(query = defaultState.query) {
    return {
        type: SET_QUERY,
        query
    };
}

export function nextAssociatedPage() {
    return {
        type: NEXT_PAGE
    };
}

export function createNewArtist(name, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: CREATE_NEW_ARTIST,
            promise: api.monetization.createNewArtist(
                token,
                secret,
                name,
                options
            )
        });
    };
}

export function sendClaimProfileEmail(artistId, email) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: SEND_CLAIM_PROFILE_MAIL,
            promise: api.monetization.sendClaimProfileEmail(
                token,
                secret,
                artistId,
                email
            )
        });
    };
}
