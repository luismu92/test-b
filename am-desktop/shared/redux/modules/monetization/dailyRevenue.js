import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/dailyRevenue/';

const GET_DAILY_REVENUE = `${PREFIX}GET_DAILY_REVENUE`;

const defaultState = {
    data: {},
    loading: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_DAILY_REVENUE):
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };

        case failSuffix(GET_DAILY_REVENUE):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_DAILY_REVENUE:
            return {
                ...state,
                data: action.resolved,
                loading: false
            };

        default:
            return state;
    }
}

export function getDailyRevenue(options = {}) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;
        const opts = {
            ...options,
            type: 'daily'
        };

        return dispatch({
            type: GET_DAILY_REVENUE,
            promise: api.monetization.revenue(token, secret, opts)
        });
    };
}
