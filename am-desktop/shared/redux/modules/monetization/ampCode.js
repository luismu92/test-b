import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/ampCode/';

const GENERATE_AMP_CODE = `${PREFIX}GENERATE_AMP_CODE`;
const GET_AMP_CODES = `${PREFIX}GET_AMP_CODES`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;

const defaultState = {
    generated: {
        code: null,
        loading: false,
        error: null
    },
    list: [],
    loading: false,
    error: null,
    page: 1,
    onLastPage: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GENERATE_AMP_CODE):
            return {
                ...state,
                generated: {
                    ...state.generated,
                    loading: true
                }
            };

        case GENERATE_AMP_CODE:
            return {
                ...state,
                generated: {
                    ...state.generated,
                    loading: false,
                    code: action.resolved.results.code
                }
            };

        case failSuffix(GENERATE_AMP_CODE):
            return {
                ...state,
                generated: {
                    ...state.generated,
                    loading: false,
                    error: action.error
                }
            };

        case requestSuffix(GET_AMP_CODES):
            return {
                ...state,
                loading: true
            };

        case GET_AMP_CODES: {
            let newData = action.resolved.results;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (
                !action.resolved.results.length ||
                action.resolved.results.length < 20
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                ...state,
                list: newData,
                loading: false,
                onLastPage
            });
        }

        case failSuffix(GET_AMP_CODES):
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        default:
            return state;
    }
}

export function generateCode(prefix = null) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: GENERATE_AMP_CODE,
            promise: api.monetization.generateCode(token, secret, prefix)
        });
    };
}

export function getAmpCodes(page = 1) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: GET_AMP_CODES,
            promise: api.monetization.getAmpCodes(token, secret, page)
        });
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}
