import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/paymentInfo/';

const GET_PAYMENT_INFO = `${PREFIX}GET_PAYMENT_INFO`;

const defaultState = {
    info: {
        payments: null,
        history: null,
        payable: true
    },
    loading: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_PAYMENT_INFO):
            return {
                ...state,
                info: {
                    ...state.info,
                    payable: defaultState.payable
                },
                error: defaultState.error,
                loading: true
            };

        case failSuffix(GET_PAYMENT_INFO):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_PAYMENT_INFO:
            return {
                ...state,
                info: action.resolved.results,
                loading: false
            };

        default:
            return state;
    }
}

export function getPaymentInfo() {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: GET_PAYMENT_INFO,
            promise: api.monetization.paymentInfo(token, secret)
        });
    };
}
