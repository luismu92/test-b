import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/label/';

export const LABEL_SIGNUP = `${PREFIX}LABEL_SIGNUP`;

const VALIDATE_CODE = `${PREFIX}VALIDATE_CODE`;

const defaultState = {
    signupSummary: {},
    signupIsLoading: false,
    signupError: null,
    codeIsValidating: false,
    codeError: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(LABEL_SIGNUP):
            return {
                ...state,
                signupError: defaultState.signupError,
                signupIsLoading: true
            };

        case failSuffix(LABEL_SIGNUP):
            return {
                ...state,
                signupError: action.error,
                signupIsLoading: false
            };

        case LABEL_SIGNUP:
            return {
                ...state,
                signupSummary: action.resolved,
                signupIsLoading: false
            };

        case requestSuffix(VALIDATE_CODE):
            return {
                ...state,
                codeError: defaultState.codeError,
                codeIsValidating: true
            };

        case failSuffix(VALIDATE_CODE):
            return {
                ...state,
                codeError: action.error,
                codeIsValidating: false
            };

        case VALIDATE_CODE:
            return {
                ...state,
                codeIsValidating: false
            };

        default:
            return state;
    }
}

export function signup(options = {}) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;
        const fn = options.update
            ? api.monetization.updateLabel
            : api.monetization.labelSignup;

        return dispatch({
            type: LABEL_SIGNUP,
            promise: fn(token, secret, options)
        });
    };
}

export function validateCode(code) {
    return {
        type: VALIDATE_CODE,
        promise: api.monetization.validateCode(code)
    };
}
