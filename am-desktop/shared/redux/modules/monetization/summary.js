import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/summary/';

const GET_SUMMARY = `${PREFIX}GET_SUMMARY`;

const defaultState = {
    summary: {},
    loading: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_SUMMARY):
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };

        case failSuffix(GET_SUMMARY):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_SUMMARY:
            return {
                ...state,
                summary: action.resolved,
                loading: false
            };

        default:
            return state;
    }
}

export function getSummary(options = {}) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: GET_SUMMARY,
            promise: api.monetization.summary(token, secret, options)
        });
    };
}
