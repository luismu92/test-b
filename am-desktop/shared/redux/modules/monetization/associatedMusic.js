import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/monetization/associatedMusic/';

const GET_ASSOCIATED_MUSIC = `${PREFIX}GET_ASSOCIATED_MUSIC`;
const SET_QUERY = `${PREFIX}SET_QUERY`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const REPLACE_MUSIC = `${PREFIX}REPLACE_MUSIC`;

const defaultState = {
    list: [],
    page: 1,
    show: 'all',
    query: '',
    loading: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_ASSOCIATED_MUSIC):
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };

        case failSuffix(GET_ASSOCIATED_MUSIC):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case GET_ASSOCIATED_MUSIC: {
            const results = action.resolved.results;
            let newData = results || [];
            let onLastPage = false;

            if (action.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                action.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return {
                ...state,
                list: newData,
                page: action.page,
                show: action.show,
                query: action.query,
                onLastPage,
                loading: false
            };
        }

        case SET_QUERY:
            return {
                ...state,
                query: action.query
            };

        case REPLACE_MUSIC:
            return {
                ...state,
                list: state.list.map((item) => {
                    if (item.id === action.resolved.id) {
                        return action.resolved;
                    }

                    return item;
                })
            };

        default:
            return state;
    }
}

export function getAssociatedMusic(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().monetizationAssociatedMusic;
        const {
            limit,
            page = state.page,
            show = state.show,
            query = state.query,
            incompletes
        } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_ASSOCIATED_MUSIC,
            page: finalPage,
            show,
            query,
            limit: finalLimit,
            promise: api.monetization.associated(token, secret, {
                show,
                query,
                incompletes,
                page: finalPage,
                limit: finalLimit
            })
        });
    };
}

export function setAssociatedQuery(query = defaultState.query) {
    return {
        type: SET_QUERY,
        query
    };
}

export function nextAssociatedPage() {
    return {
        type: NEXT_PAGE
    };
}

export function replaceAssociatedItem(item) {
    return {
        type: REPLACE_MUSIC,
        resolved: item
    };
}
