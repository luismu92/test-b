import api from 'api/index';
import { requestSuffix, failSuffix, shuffle } from 'utils/index';

// Actions
const GET_FEATURED = 'am/featured/GET_FEATURED';

const initialState = {
    loading: false,
    list: [],
    lastUpdated: Number.NEGATIVE_INFINITY
};

// Reducer
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case requestSuffix(GET_FEATURED):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_FEATURED):
            return {
                ...state,
                loading: false
            };

        case GET_FEATURED:
            return {
                ...state,
                loading: false,
                list: action.resolved.results,
                lastUpdated: Date.now()
            };

        default:
            return state;
    }
}

// Action Creators
export function getFeatured() {
    return {
        type: GET_FEATURED,
        promise: api.featured.get().then((data) => {
            // Shuffle on fetch so we dont have to do the random item
            // logic that we had at the time of this commit
            return {
                results: shuffle(data.results)
            };
        })
    };
}
