/* global test, expect */
import editReducer from '../edit';
import * as actions from '../edit';
import { configureStore } from 'redux/configureStore';
import { UPLOAD_TYPE } from 'constants/index';
import rootReducer from '../../../reducer';
import { ADD_ALBUM_TRACK } from '../../upload/album';

function getDefaultStore(initialState) {
    return configureStore(
        {
            musicEdit: {
                ...editReducer(undefined, { type: 'gimmeDefaultState' }),
                ...editReducer(initialState, { type: 'gimmeDefaultState' })
            }
        },
        rootReducer
    );
}

test('should apply updated genre to any tracks without a genre', () => {
    const info = {
        type: UPLOAD_TYPE.ALBUM,
        tracks: [
            { id: 2, genre: '' },
            { id: 3, genre: 'rap' },
            { id: 1, genre: '' }
        ]
    };
    const initialState = {
        info
    };

    const store = getDefaultStore(initialState);

    store.dispatch(actions.updateItemInfo('genre', 'electronic'));

    const nextState = store.getState().musicEdit;

    expect(nextState.info.tracks[0].genre).toBe('electronic');
    expect(nextState.info.tracks[1].genre).toBe('rap');
    expect(nextState.info.tracks[2].genre).toBe('electronic');
});

test('should apply album genre to any added tracks without a genre', () => {
    const info = {
        type: UPLOAD_TYPE.ALBUM,
        genre: 'electronic',
        tracks: [{ id: 3, genre: 'rap' }]
    };
    const initialState = {
        info
    };
    const action = {
        type: ADD_ALBUM_TRACK,
        resolved: { id: 2, genre: '' }
    };

    const store = getDefaultStore(initialState);

    store.dispatch(action);

    const nextState = store.getState().musicEdit;

    expect(nextState.info.tracks[1].id).toBe(2);
    expect(nextState.info.tracks[1].genre).toBe('electronic');
});
