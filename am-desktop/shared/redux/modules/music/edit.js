import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { UPLOAD_TYPE, allGenresMap } from 'constants/index';
import { UPDATE_METADATA, DELETE, REPLACE_MUSIC } from '../music/index';
import { ADD_ALBUM_TRACK, ADD_PRE_EXISTING_ALBUM_TRACK } from '../upload/album';
import { DELETE_SONG_FROM_PLAYLIST } from '../playlist';

const PREFIX = 'am/music/edit/';

const SET_INFO = `${PREFIX}SET_INFO`;
const GET_MUSIC_BY_ID = `${PREFIX}GET_MUSIC_BY_ID`;
const UPDATE_ITEM_INFO = `${PREFIX}UPDATE_ITEM_INFO`;
const UPDATE_ITEM_TRACK_INFO = `${PREFIX}UPDATE_ITEM_TRACK_INFO`;
const RESET = `${PREFIX}RESET`;

const initialState = {
    info: {},
    loading: false,
    isDeleting: false,
    isSaving: false,
    saved: false,
    errors: []
};

// eslint-disable-next-line complexity
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case requestSuffix(GET_MUSIC_BY_ID):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_MUSIC_BY_ID): {
            const error = 'There was an error.';

            if (action.error && typeof action.error.errorcode !== 'undefined') {
                switch (action.error.errorcode) {
                    // case 0:
                    //     error = action.error.message;

                    //     break;
                    // case 1029:
                    //     error = 'Could not complete song upload.';
                    //     break;

                    default:
                        break;
                }
            }

            return {
                ...state,
                errors: [error],
                loading: false
            };
        }

        case SET_INFO: {
            let newInfo = {
                ...action.info
            };

            if (action.keepExistingInfo) {
                newInfo = {
                    ...newInfo,
                    ...state.info
                };
            }

            return {
                ...state,
                info: newInfo
            };
        }

        case GET_MUSIC_BY_ID:
            return {
                ...state,
                loading: false,
                errors: [],
                info: action.resolved.results
            };

        case UPDATE_ITEM_INFO: {
            const info = {
                ...state.info,
                [action.input]: action.value
            };

            if (
                state.info.type === UPLOAD_TYPE.ALBUM &&
                action.input === 'genre' &&
                state.info.tracks
            ) {
                info.tracks = state.info.tracks.map((track) => {
                    if (
                        !track.genre ||
                        !Object.keys(allGenresMap).includes(track.genre)
                    ) {
                        return {
                            ...track,
                            saved: false,
                            genre: action.value
                        };
                    }

                    return track;
                });
            }

            return {
                ...state,
                info: info
            };
        }

        case UPDATE_ITEM_TRACK_INFO: {
            if (!state.info.tracks) {
                return state;
            }

            const newTracks = state.info.tracks.map((track) => {
                if (track.song_id === action.songId) {
                    track[action.input] = action.value;
                    track.saved = false;
                }

                return track;
            });

            return {
                ...state,
                info: {
                    ...state.info,
                    tracks: newTracks
                }
            };
        }

        case requestSuffix(UPDATE_METADATA): {
            const newTracks = Array.from(state.info.tracks || []);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let info = state.info;
            let isSaving = state.isSaving;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isSaving: true
                };

                info = {
                    ...info,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                info = {
                    ...state.info
                };

                isSaving = true;
            }

            return {
                ...state,
                info: info,
                isSaving
            };
        }

        case failSuffix(UPDATE_METADATA): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let info = state.info;
            let isSaving = state.isSaving;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isSaving: false
                };

                info = {
                    ...info,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                info = {
                    ...state.info
                };
                isSaving = false;
            }

            return {
                ...state,
                info: info,
                isSaving
            };
        }

        case UPDATE_METADATA: {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let info = state.info;
            let isSaving = state.isSaving;
            let saved = state.saved;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...action.resolved,
                    song_id: action.resolved.id,
                    album_track_only: action.resolved.album_track_only === '1',
                    isSaving: false,
                    saved: true
                };

                info = {
                    ...info,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                info = {
                    ...state.info
                };
                saved = true;
                isSaving = false;
            }

            return {
                ...state,
                info: info,
                isSaving,
                saved
            };
        }

        case REPLACE_MUSIC: {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []).map(
                (track) => {
                    return {
                        ...track,
                        isSaving: false,
                        isDeleting: false,
                        saved: false
                    };
                }
            );
            let info = state.info;

            if (newTracks.length) {
                info = {
                    ...info,
                    tracks: newTracks
                };
            }

            return {
                ...state,
                info: info,
                isSaving: false,
                isDeleting: false,
                saved: false
            };
        }

        case ADD_ALBUM_TRACK: {
            if (!state.info.tracks || !state.info.tracks.length) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const newTrack = {
                ...action.resolved,
                genre: action.resolved.genre || state.info.genre
            };

            newTracks.push(newTrack);

            return {
                ...state,
                info: {
                    ...state.info,
                    tracks: newTracks
                }
            };
        }

        case ADD_PRE_EXISTING_ALBUM_TRACK: {
            if (!state.info.tracks || !state.info.tracks.length) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const newTrack = {
                ...action.resolved,
                genre: action.resolved.genre || state.info.genre
            };

            newTracks.push(newTrack);

            return {
                ...state,
                info: {
                    ...state.info,
                    tracks: newTracks
                }
            };
        }

        case requestSuffix(DELETE): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let info = state.info;
            let isDeleting = state.isDeleting;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isDeleting: true
                };

                info = {
                    ...info,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                info = {
                    ...state.info
                };
                isDeleting = true;
            }

            return {
                ...state,
                info: info,
                isDeleting
            };
        }

        case failSuffix(DELETE): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let info = state.info;
            let isDeleting = state.isDeleting;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isDeleting: false
                };

                info = {
                    ...info,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                info = {
                    ...state.info
                };
                isDeleting = false;
            }

            return {
                ...state,
                info: info,
                isDeleting
            };
        }

        case DELETE_SONG_FROM_PLAYLIST: {
            if (!state.info || state.info.id !== action.playlistId) {
                return state;
            }

            if (state.info.type !== 'playlist') {
                return state;
            }

            return {
                ...state,
                info: {
                    ...state.info,
                    tracks: state.info.tracks.filter((track) => {
                        return track.id !== action.songId;
                    })
                }
            };
        }

        case DELETE: {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks || []);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let info = state.info;
            let isDeleting = state.isDeleting;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isDeleting: false
                };

                info = {
                    ...state.info,
                    tracks: (info.tracks || []).filter((track) => {
                        return track.song_id !== action.musicId;
                    })
                };
            } else if (state.info.id === action.musicId) {
                info = {
                    ...state.info
                };

                isDeleting = false;
            }

            return {
                ...state,
                info: info,
                isDeleting: isDeleting
            };
        }

        case RESET:
            return initialState;

        default:
            return state;
    }
}

export function reset() {
    return {
        type: RESET
    };
}

export function setInfo(info, keepExistingInfo = false) {
    return {
        type: SET_INFO,
        info,
        keepExistingInfo
    };
}

export function getMusicById(musicId, type, options = {}) {
    return {
        type: GET_MUSIC_BY_ID,
        promise:
            type === 'playlist'
                ? api.playlist.id(musicId, options.token, options.secret)
                : api.music.id(musicId, options)
    };
}

export function updateItemInfo(input, value) {
    return {
        type: UPDATE_ITEM_INFO,
        input,
        value
    };
}

export function updateItemTrackInfo(songId, input, value) {
    return {
        type: UPDATE_ITEM_TRACK_INFO,
        songId,
        input,
        value
    };
}
