import api from 'api/index';

const PREFIX = 'am/music/index/';

export const UPDATE_METADATA = `${PREFIX}UPDATE_METADATA`;
export const REORDER_TRACKS = `${PREFIX}REORDER_TRACKS`;
export const DELETE = `${PREFIX}DELETE`;
export const UPDATE_PRIVACY = `${PREFIX}UPDATE_PRIVACY`;
export const REPLACE_MUSIC = `${PREFIX}REPLACE_MUSIC`;

const initialState = {};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
    }
}

export function updateMetadata(musicId, metadata) {
    return (dispatch, getState) => {
        const state = getState().currentUser;
        const { token, secret } = state;

        return dispatch({
            type: UPDATE_METADATA,
            musicId,
            promise: api.music.update(musicId, metadata, token, secret)
        });
    };
}

export function deleteItem(musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: DELETE,
            musicId: parseInt(musicId, 10),
            promise: api.music.delete(musicId, token, secret)
        });
    };
}

export function deleteAlbumTrackItem(albumId, musicId) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: DELETE,
            musicId: parseInt(musicId, 10),
            albumId: parseInt(albumId, 10),
            promise: api.music.deleteTrack(albumId, musicId, token, secret)
        });
    };
}

export function alterItemPrivacy(musicId, isPrivate) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: UPDATE_PRIVACY,
            musicId: parseInt(musicId, 10),
            promise: api.music.privacy(musicId, isPrivate, token, secret)
        });
    };
}

export function reorderAlbumTracks(albumId, tracks) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: REORDER_TRACKS,
            albumId: parseInt(albumId, 10),
            promise: api.music.reorderAlbumTracks(
                albumId,
                tracks,
                token,
                secret
            )
        });
    };
}

export function replaceMusic(id, key) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: REPLACE_MUSIC,
            id,
            promise: api.music.update(id, { key }, token, secret)
        });
    };
}
