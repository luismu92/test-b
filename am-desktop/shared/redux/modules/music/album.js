import api from 'api/index';
import { suffix } from 'utils/index';
import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from '../user';
import { TRACK_PLAY } from '../stats';
import { CLEAR_UPLOAD_ERRORS } from '../admin';

const PREFIX = 'am/music/album/';

const GET_ALBUM_INFO = `${PREFIX}GET_ALBUM_INFO`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    loading: false,
    errors: [],
    info: null
};

// Should probably think about using immutableJS in this project
export default function musicReducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(GET_ALBUM_INFO, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(GET_ALBUM_INFO, 'FAILURE'):
            return Object.assign({}, state, {
                errors: [action.error],
                loading: false
            });

        case GET_ALBUM_INFO: {
            const album = action.resolved.results;

            if (typeof album.tracks === 'undefined') {
                album.tracks = [];
            }

            // Build out structure that complies with what the player needs
            // when the track is selected
            album.tracks = album.tracks.map((track) => {
                track.id = track.song_id;
                track.artist = track.artist || album.artist;
                track.image = album.image;
                track.trackType = 'album';
                return track;
            });

            return Object.assign({}, state, {
                info: album,
                loading: false
            });
        }

        case suffix(UNFAVORITE_ITEM, 'FAILURE'):
        case suffix(FAVORITE_ITEM, 'REQUEST'): {
            let album = state.info;

            if (album && album.id === action.id) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['favorites-raw'] += 1;
            }

            return Object.assign({}, state, {
                info: album
            });
        }

        case suffix(FAVORITE_ITEM, 'FAILURE'):
        case suffix(UNFAVORITE_ITEM, 'REQUEST'): {
            let album = state.info;

            if (album && album.id === action.id) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['favorites-raw'] -= 1;
            }

            return Object.assign({}, state, {
                info: album
            });
        }

        case suffix(UNREPOST_ITEM, 'FAILURE'):
        case suffix(REPOST_ITEM, 'REQUEST'): {
            let album = state.info;

            if (album && album.id === action.id) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['reposts-raw'] += 1;
            }

            return Object.assign({}, state, {
                info: album
            });
        }

        case suffix(REPOST_ITEM, 'FAILURE'):
        case suffix(UNREPOST_ITEM, 'REQUEST'): {
            let album = state.info;

            if (album && album.id === action.id) {
                album = { ...album, stats: { ...album.stats } };
                album.stats['reposts-raw'] -= 1;
            }

            return Object.assign({}, state, {
                info: album
            });
        }

        case TRACK_PLAY: {
            if (!state.info) {
                return state;
            }

            const album = {
                ...state.info
            };
            const albumId = action.options.album_id;

            if (typeof albumId === 'number' && album.id === albumId) {
                // Only update if this is the first track play call, ie not the 30 second track call
                if (!action.options.time) {
                    album.stats['plays-raw'] += 1;
                }
            }

            return Object.assign({}, state, {
                info: album
            });
        }

        case CLEAR_UPLOAD_ERRORS: {
            if (!state.info) {
                return state;
            }

            const album = {
                ...state.info
            };

            album.errors = [];

            return Object.assign({}, state, {
                info: album
            });
        }

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getAlbumInfo(artistSlug, albumSlug, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_ALBUM_INFO,
            force404OnError: /^\/album\//,
            promise: api.music.album(artistSlug, albumSlug, {
                token,
                secret,
                ...options
            })
        });
    };
}

export function reset() {
    return {
        type: RESET
    };
}
