import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from '../user';
import { DELETE_SONG_FROM_PLAYLIST, ADD_SONG_TO_PLAYLIST } from '../playlist';
import { TRACK_PLAY } from '../stats';
import { SUSPEND_MUSIC, UNSUSPEND_MUSIC, CLEAR_UPLOAD_ERRORS } from '../admin';
import { UPDATE_PRIVACY } from '../music/index';

const PREFIX = 'am/music/song/';
const GET_SONG_INFO = `${PREFIX}GET_SONG_INFO`;
const GET_SONG_BY_ID = `${PREFIX}GET_SONG_BY_ID`;
const RESET = `${PREFIX}RESET`;
const defaultState = {
    loading: false,
    errors: [],
    info: null
};

// eslint-disable-next-line complexity
export default function musicReducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_SONG_INFO):
        case requestSuffix(GET_SONG_BY_ID):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case failSuffix(GET_SONG_INFO):
        case failSuffix(GET_SONG_BY_ID):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case GET_SONG_INFO:
        case GET_SONG_BY_ID:
            return Object.assign({}, state, {
                info: action.resolved.results,
                loading: false
            });

        case failSuffix(UNFAVORITE_ITEM):
        case requestSuffix(FAVORITE_ITEM): {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };

            if (song.id === action.id) {
                song.stats['favorites-raw'] += 1;
            }

            return {
                ...state,
                info: song
            };
        }

        case failSuffix(FAVORITE_ITEM):
        case requestSuffix(UNFAVORITE_ITEM): {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };

            if (song.id === action.id) {
                song.stats['favorites-raw'] -= 1;
            }

            return {
                ...state,
                info: song
            };
        }

        case failSuffix(UNREPOST_ITEM):
        case requestSuffix(REPOST_ITEM): {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };

            if (song.id === action.id) {
                song.stats['reposts-raw'] += 1;
            }

            return {
                ...state,
                info: song
            };
        }

        case failSuffix(REPOST_ITEM):
        case requestSuffix(UNREPOST_ITEM): {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };

            if (song.id === action.id) {
                song.stats['reposts-raw'] -= 1;
            }

            return {
                ...state,
                info: song
            };
        }

        case failSuffix(DELETE_SONG_FROM_PLAYLIST):
        case requestSuffix(ADD_SONG_TO_PLAYLIST): {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };

            if (song.id === action.songId && song.stats) {
                song.stats['playlists-raw'] += 1;
            }

            return {
                ...state,
                info: song
            };
        }

        case failSuffix(ADD_SONG_TO_PLAYLIST):
        case requestSuffix(DELETE_SONG_FROM_PLAYLIST): {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };

            if (song.id === action.songId) {
                song.stats['playlists-raw'] -= 1;
            }

            return {
                ...state,
                info: song
            };
        }

        case SUSPEND_MUSIC: {
            return Object.assign({}, state, {
                info: action.resolved
            });
        }

        case UNSUSPEND_MUSIC: {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info };

            song.private = action.resolved.private;

            return Object.assign({}, state, {
                info: song
            });
        }

        case TRACK_PLAY: {
            if (!state.info) {
                return state;
            }

            const song = { ...state.info, stats: { ...state.info.stats } };
            const songId = action.id;

            if (typeof songId === 'number' && song.id === songId) {
                // Only update if this is the first track play call, ie not the 30 second track call
                if (!action.options.time) {
                    song.stats['plays-raw'] += 1;
                }
            }

            return Object.assign({}, state, {
                info: song
            });
        }

        case CLEAR_UPLOAD_ERRORS: {
            if (!state.info) {
                return state;
            }

            const song = {
                ...state.info
            };

            song.errors = [];

            return Object.assign({}, state, {
                info: song
            });
        }

        case RESET:
            return defaultState;

        case UPDATE_PRIVACY:
            return Object.assign({}, state, {
                info: action.resolved
            });

        default:
            return state;
    }
}

export function getSongInfo(artistSlug, songSlug, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_SONG_INFO,
            force404OnError: /^\/song\//,
            promise: api.music.song(artistSlug, songSlug, {
                token,
                secret,
                ...options
            })
        });
    };
}

export function getSongById(id, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_SONG_BY_ID,
            force404OnError: /^\/song\//,
            promise: api.music.id(id, {
                token,
                secret,
                ...options
            })
        });
    };
}

export function reset() {
    return {
        type: RESET
    };
}
