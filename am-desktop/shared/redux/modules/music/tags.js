import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { tagSections } from '../../../../../am-shared/constants';

const PREFIX = 'am/music/tags/';

const GET_GLOBAL_MUSIC_TAGS = `${PREFIX}GET_GLOBAL_MUSIC_TAGS`;
const GET_GLOBAL_GEO_TAGS = `${PREFIX}GET_GLOBAL_GEO_TAGS`;
const GET_MUSIC_TAGS = `${PREFIX}GET_MUSIC_TAGS`;
const ADD_MUSIC_TAGS = `${PREFIX}ADD_MUSIC_TAGS`;
const DELETE_MUSIC_TAGS = `${PREFIX}DELETE_MUSIC_TAGS`;
const SUGGEST_MUSIC_TAG = `${PREFIX}SUGGEST_MUSIC_TAG`;

const defaultState = {
    // valid tag options
    options: {
        geo: [],
        music: []
    },
    // valid & invalid tags formatted for display
    ui: {
        geo: []
    },
    // user tags associated with a musicId
    id: {},
    suggestions: [],
    isAdding: false,
    loading: false,
    error: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_GLOBAL_GEO_TAGS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_GLOBAL_GEO_TAGS):
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case GET_GLOBAL_GEO_TAGS: {
            let list = [...state.options.geo];
            let allItems = [];
            let newItems = [];
            let tags = [];

            // add countries
            if (!action.country) {
                tags = action.resolved.map((country) => {
                    return {
                        display: country,
                        normalizedKey: country.replace(/\s/g, '').toLowerCase(),
                        section: tagSections.locations,
                        type: 'country',
                        hasChildren: true,
                        children: []
                    };
                });
            }

            // add geo tag options to list of tag options for all sections
            // valid options are non-root level (non-country) tags
            if (action.country) {
                const UNITED_STATES = 'United States';
                const isUS = action.country === UNITED_STATES;
                const isUSState = isUS && !action.state;
                const isUSCity = isUS && action.state;

                const normalizedKeys = state.options.geo.reduce(
                    (set, tagObject) => set.add(tagObject.normalizedKey),
                    new Set()
                );

                const processedTags = action.resolved.reduce(
                    (items, item) => {
                        const location = [
                            action.country,
                            action.state,
                            item
                        ].filter(Boolean);

                        const formattedTag = {
                            display: location.join('->'),
                            normalizedKey: location
                                .join('')
                                .replace(/\s/g, '')
                                .toLowerCase(),
                            section: tagSections.locations,
                            type: isUSState ? 'state' : 'city',
                            hasChildren: isUSState,
                            children: []
                        };

                        if (!normalizedKeys.has(formattedTag.normalizedKey)) {
                            items.newItems.push(formattedTag);
                        }

                        items.allItems.push(formattedTag);

                        return items;
                    },
                    { newItems: [], allItems: [] }
                );

                allItems = processedTags.allItems;
                newItems = processedTags.newItems;

                // add US States
                // add non-US cities
                if (isUSState || !isUS) {
                    tags = state.ui.geo.map((country) => {
                        if (country.display === action.country) {
                            country.children = allItems;
                        }

                        return country;
                    });
                }

                // add US cities
                if (isUSCity) {
                    tags = state.ui.geo.map((country) => {
                        if (country.display === UNITED_STATES) {
                            country.children.map((USState) => {
                                if (USState.display === action.state) {
                                    USState.children = allItems;
                                }

                                return USState;
                            });
                        }

                        return country;
                    });

                    // add cities to states in the list of tag options for all sections
                    list = list.map((tag) => {
                        if (
                            tag.display === `${action.country}->${action.state}`
                        ) {
                            tag.children = allItems;
                        }

                        return tag;
                    });
                }
            }

            return {
                ...state,
                options: {
                    ...state.options,
                    geo: [...list, ...newItems]
                },
                ui: {
                    ...state.ui,
                    geo: tags
                }
            };
        }

        case requestSuffix(GET_GLOBAL_MUSIC_TAGS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_GLOBAL_MUSIC_TAGS):
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case GET_GLOBAL_MUSIC_TAGS:
            return {
                ...state,
                options: {
                    ...state.options,
                    music: action.resolved.filter(
                        (tag) => tag.section !== tagSections.adminOnly
                    )
                },
                loading: false
            };

        case requestSuffix(GET_MUSIC_TAGS):
            return {
                ...state,
                loading: true
            };

        case failSuffix(GET_MUSIC_TAGS):
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case GET_MUSIC_TAGS:
            return {
                ...state,
                id: {
                    ...state.id,
                    [action.musicItem.id]: action.resolved
                },
                loading: false
            };

        case requestSuffix(ADD_MUSIC_TAGS): {
            const list = state.id[action.musicItem.id] || [];

            return {
                ...state,
                isAdding: true,
                // Filter out existing duplicate tags in case we are retrying
                id: {
                    ...state.id,
                    [action.musicItem.id]: list.filter(
                        (tag) => !action.newTags.includes(tag.display)
                    )
                }
            };
        }

        case failSuffix(ADD_MUSIC_TAGS):
            return {
                ...state,
                error: action.error,
                isAdding: false
            };

        case ADD_MUSIC_TAGS: {
            const newNormalized = action.resolved.map((t) => t.normalizedKey);
            const options = [...state.options.geo, ...state.options.music];
            const newItems = action.resolved.map((tag) => {
                return options.find(
                    (t) => t.normalizedKey === tag.normalizedKey
                );
            });
            const list = state.id[action.musicItem.id] || [];
            const newList = list.filter((tag) => {
                return !newNormalized.includes(tag.normalizedKey);
            });

            return {
                ...state,
                isAdding: false,
                id: {
                    ...state.id,
                    [action.musicItem.id]: newList.concat(newItems)
                }
            };
        }

        case requestSuffix(DELETE_MUSIC_TAGS): {
            const list = state.id[action.musicItem.id] || [];
            return {
                ...state,
                id: {
                    ...state.id,
                    [action.musicItem.id]: list.map((tag) => {
                        return {
                            ...tag,
                            isDeleting: action.newTags.includes(tag.display)
                        };
                    })
                }
            };
        }

        case failSuffix(DELETE_MUSIC_TAGS): {
            const list = state.id[action.musicItem.id] || [];

            return {
                ...state,
                id: {
                    ...state.id,
                    [action.musicItem.id]: list.map((tag) => {
                        const newTag = {
                            ...tag
                        };

                        if (action.newTags.includes(tag.display)) {
                            newTag.isDeleting = false;
                        }

                        return newTag;
                    })
                }
            };
        }

        case DELETE_MUSIC_TAGS: {
            const resolvedNormalized = action.resolved
                .filter((tag) => tag.success)
                .map((tag) => tag.normalizedKey);
            const list = state.id[action.musicItem.id] || [];
            const newList = list
                .filter((tag) => {
                    return !resolvedNormalized.includes(tag.normalizedKey);
                })
                .map((tag) => {
                    const newTag = {
                        ...tag
                    };

                    const isExpected = action.newTags.includes(tag.display);
                    const wasSuccess = resolvedNormalized.includes(
                        tag.normalizedKey
                    );

                    if (isExpected && !wasSuccess) {
                        newTag.isDeleting = false;
                    }

                    return newTag;
                });

            return {
                ...state,
                id: {
                    ...state.id,
                    [action.musicItem.id]: newList
                }
            };
        }

        case requestSuffix(SUGGEST_MUSIC_TAG):
            return {
                ...state,
                loading: true
            };

        case failSuffix(SUGGEST_MUSIC_TAG):
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case SUGGEST_MUSIC_TAG: {
            return {
                ...state,
                suggestions: [...state.suggestions, action.resolved]
            };
        }

        default:
            return state;
    }
}

export function getGlobalMusicTags() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_GLOBAL_MUSIC_TAGS,
            promise: api.music.getGlobalTags(token, secret)
        });
    };
}

export function getGlobalGeoTags(country, state) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_GLOBAL_GEO_TAGS,
            country,
            state,
            promise: api.music.getGlobalGeoTags(country, state, token, secret)
        });
    };
}

export function getMusicTags(musicItem) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;

        return dispatch({
            type: GET_MUSIC_TAGS,
            musicItem: musicItem,
            promise: api.music.getTags(type, id, token, secret)
        });
    };
}

export function addMusicTags(musicItem, tags) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;
        const tagsArr = (Array.isArray(tags) ? tags : [tags]).filter(Boolean);

        return dispatch({
            type: ADD_MUSIC_TAGS,
            musicItem: musicItem,
            newTags: tagsArr,
            promise: api.music.addTags(type, id, tagsArr, token, secret)
        });
    };
}

export function deleteMusicTags(musicItem, tags) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;
        const tagsArr = (Array.isArray(tags) ? tags : [tags]).filter(Boolean);

        return dispatch({
            type: DELETE_MUSIC_TAGS,
            musicItem: musicItem,
            newTags: tagsArr,
            promise: api.music.deleteTags(type, id, tagsArr, token, secret)
        });
    };
}

export function suggestMusicTag(musicItem, tag) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const type = musicItem.type === 'playlist' ? 'playlist' : 'music';
        const id = musicItem.id;

        return dispatch({
            type: SUGGEST_MUSIC_TAG,
            tag,
            promise: api.music.suggestTag(type, id, tag, token, secret)
        });
    };
}
