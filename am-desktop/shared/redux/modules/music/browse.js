import api from 'api/index';
import { failSuffix, requestSuffix } from 'utils/index';
import {
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM
} from '../user';
import { ADD_SONG_TO_PLAYLIST, DELETE_SONG_FROM_PLAYLIST } from '../playlist';
import { TRACK_PLAY } from '../stats';
import { BUMP_MUSIC, BUMP_MUSIC_ONE_SPOT } from '../admin';

import {
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_PLAYLIST,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    CHART_TYPE_TOTAL,
    CHART_TYPE_DAILY,
    CHART_TYPE_WEEKLY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY
} from 'constants/index';

const PREFIX = 'am/music/browse/';

const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const FETCH_MUSIC = `${PREFIX}FETCH_MUSIC`;
const SET_CONTEXT = `${PREFIX}SET_CONTEXT`;
const SET_GENRE = `${PREFIX}SET_GENRE`;
const SET_TIME_PERIOD = `${PREFIX}SET_TIME_PERIOD`;
const SET_PLAYLIST_CONTEXT = `${PREFIX}SET_PLAYLIST_CONTEXT`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const PREV_PAGE = `${PREFIX}PREV_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;

const defaultState = {
    loading: false,
    activePlaylistContext: COLLECTION_TYPE_PLAYLIST,
    activeContext: COLLECTION_TYPE_TRENDING,
    activeGenre: '',
    activeTimePeriod: CHART_TYPE_TOTAL,
    errors: [],
    page: 1,
    limit: 20,
    onLastPage: false,
    showNumbers: false,
    list: []
};

export default function musicReducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(FETCH_MUSIC):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case failSuffix(FETCH_MUSIC):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case CLEAR_LIST:
            return Object.assign({}, state, {
                list: []
            });

        case FETCH_MUSIC: {
            const results = action.resolved.results;
            let newData = results;
            let page = action.page;
            let onLastPage = false;

            if (state.page !== 1) {
                if (action.prependNewData) {
                    // @todo prepend list and handle updating of urls
                    // appropriately. Currently clicking on the previous link
                    // will make the url go back a page, loading the appropriate
                    // data. However since the data from the originally loaded
                    // page (which was a page ahead) already exists on the page
                    // scrolling all the way down will load that data again.
                    // To Fix this we need to appropriately update the page urls
                    // on scroll. Then we can uncomment this line. For now
                    // we erase the whole list and render the previous page.
                    // newData = newData.concat(state.list);
                } else {
                    newData = state.list.concat(newData);
                }
            }

            if (!results.length) {
                onLastPage = true;
                page = action.page - 1;
            }

            return {
                ...state,
                list: newData,
                loading: false,
                limit: action.limit,
                showNumbers: action.showNumbers,
                page,
                onLastPage
            };
        }

        case SET_CONTEXT: {
            let page = state.page;
            let onLastPage = state.onLastPage;
            let list = state.list;

            if (state.activeContext !== action.context) {
                page = 1;
                list = [];
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeContext: action.context,
                page,
                list,
                onLastPage
            });
        }

        case SET_PLAYLIST_CONTEXT:
            return Object.assign({}, state, {
                activePlaylistContext: action.context
            });

        case SET_GENRE: {
            let page = state.page;
            let onLastPage = state.onLastPage;
            let list = state.list;

            if (state.activeGenre !== action.genre) {
                page = 1;
                list = [];
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeGenre: action.genre,
                list,
                page,
                onLastPage
            });
        }

        case SET_TIME_PERIOD: {
            let page = state.page;
            let onLastPage = state.onLastPage;

            if (state.activeTimePeriod !== action.timePeriod) {
                page = 1;
                onLastPage = false;
            }

            return Object.assign({}, state, {
                activeTimePeriod: action.timePeriod,
                page,
                onLastPage
            });
        }

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case PREV_PAGE:
            return Object.assign({}, state, {
                page: Math.max(1, state.page - 1)
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case failSuffix(UNFAVORITE_ITEM):
        case requestSuffix(FAVORITE_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(FAVORITE_ITEM):
        case requestSuffix(UNFAVORITE_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['favorites-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(UNREPOST_ITEM):
        case requestSuffix(REPOST_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(REPOST_ITEM):
        case requestSuffix(UNREPOST_ITEM): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.id) {
                        newObj.stats['reposts-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(DELETE_SONG_FROM_PLAYLIST):
        case requestSuffix(ADD_SONG_TO_PLAYLIST): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.songId) {
                        newObj.stats['playlists-raw'] += 1;
                    }

                    return newObj;
                })
            });
        }

        case failSuffix(ADD_SONG_TO_PLAYLIST):
        case requestSuffix(DELETE_SONG_FROM_PLAYLIST): {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.songId) {
                        newObj.stats['playlists-raw'] -= 1;
                    }

                    return newObj;
                })
            });
        }

        case TRACK_PLAY: {
            return Object.assign({}, state, {
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };
                    const itemId = action.id;
                    const albumId = action.options.album_id;

                    if (
                        (obj.type === 'album' &&
                            typeof albumId === 'number' &&
                            obj.id === albumId) ||
                        (obj.type !== 'album' && obj.id === itemId)
                    ) {
                        // Only update if this is the first track play call, ie not the 30 second track call
                        if (!action.options.time) {
                            newObj.stats['plays-raw'] += 1;
                        }
                    }

                    return newObj;
                })
            });
        }

        // Since this is admin only, i wont worry about this failing and resetting
        // the list back to the previous state
        case requestSuffix(BUMP_MUSIC): {
            return {
                ...state,
                list: [action.item].concat(
                    state.list.filter((item) => item.id !== action.item.id)
                )
            };
        }

        // Since this is admin only, i wont worry about this failing and resetting
        // the list back to the previous state
        case requestSuffix(BUMP_MUSIC_ONE_SPOT): {
            const currentIndex = state.list.findIndex(
                (item) => item.id === action.item.id
            );

            if (currentIndex === -1) {
                return state;
            }

            const item = state.list[currentIndex];
            const firstHalf = state.list.slice(0, currentIndex);
            const secondHalf = state.list.slice(currentIndex + 1);
            const itemToSwitchWith = firstHalf.pop();

            firstHalf.push(item);
            firstHalf.push(itemToSwitchWith);

            const newList = [...firstHalf, ...secondHalf];

            return {
                ...state,
                list: newList
            };
        }

        default:
            return state;
    }
}

export function fetchSongList(options = {}) {
    return (dispatch, getState) => {
        const state = getState().musicBrowse;

        const {
            context = state.activeContext,
            genre = state.activeGenre,
            timePeriod: activeTimePeriod = state.activeTimePeriod,
            page = state.page,
            prependNewData = false
        } = options;

        let promise;
        let showNumbers = false;

        const limit = context === COLLECTION_TYPE_ALBUM ? 10 : 20;
        let playerFetcher;

        switch (context) {
            case COLLECTION_TYPE_TRENDING:
                promise = api.music.trending(genre, page);
                playerFetcher = (p) =>
                    api.music.trending(genre, p).then((data) => {
                        return data.results;
                    });
                break;

            case COLLECTION_TYPE_RECENTLY_ADDED:
                promise = api.music.recent(genre, page);
                playerFetcher = (p) =>
                    api.music.recent(genre, p).then((data) => {
                        return data.results;
                    });
                break;

            case COLLECTION_TYPE_SONG:
            case COLLECTION_TYPE_ALBUM:
                showNumbers = true;
                promise = api.chart.get(
                    context,
                    activeTimePeriod,
                    genre,
                    page,
                    limit
                );
                playerFetcher = (p) =>
                    api.chart
                        .get(context, activeTimePeriod, genre, p, limit)
                        .then((data) => {
                            return data.results;
                        });
                break;

            default:
                throw new Error(`Context (${context}) not accounted for.`);
        }

        return dispatch({
            type: FETCH_MUSIC,
            prependNewData,
            page,
            limit,
            showNumbers,
            playerFetcher,
            promise
        });
    };
}

export function setPlaylistContext(context) {
    return {
        type: SET_PLAYLIST_CONTEXT,
        context
    };
}
export function setContext(context) {
    return {
        type: SET_CONTEXT,
        context
    };
}

export function setGenre(genre = defaultState.activeGenre) {
    return {
        type: SET_GENRE,
        genre
    };
}

export function setTimePeriod(timePeriod = defaultState.activeTimePeriod) {
    // Map what comes from the url to the actual API param
    const map = {
        day: CHART_TYPE_DAILY,
        week: CHART_TYPE_WEEKLY,
        month: CHART_TYPE_MONTHLY,
        year: CHART_TYPE_YEARLY,
        '': CHART_TYPE_TOTAL
    };

    return {
        type: SET_TIME_PERIOD,
        timePeriod: map[timePeriod] || timePeriod
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function prevPage() {
    return {
        type: PREV_PAGE
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
