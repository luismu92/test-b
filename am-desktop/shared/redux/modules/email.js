export { default } from 'redux/modules/email';

// Export all action dispatchers from the common reducer
export {
    sendContactForm,
    sendApiForm,
    sendLegalForm
} from 'redux/modules/email';
