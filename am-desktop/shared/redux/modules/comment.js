import commonReducer from 'redux/modules/comment';

export default function reducer(state, action) {
    return commonReducer(state, action);
}

// Export all action dispatchers from the common reducer
export {
    setCommentContext,
    getComments,
    postComment,
    getUserVoteStatus,
    postUserVotes,
    addUserVote,
    deleteComment,
    reportComment,
    banComment,
    unflagComment
} from 'redux/modules/comment';
