export { default } from 'redux/modules/user/pinned';

// Export all action dispatchers from the common reducer
export {
    getPinned,
    addPinned,
    savePinned,
    deletePinned
} from 'redux/modules/user/pinned';
