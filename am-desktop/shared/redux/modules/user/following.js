import api from 'api/index';
import { suffix } from 'utils/index';

import { FOLLOW_ARTIST, UNFOLLOW_ARTIST } from './index';

const PREFIX = 'am/user/following/';

const GET_USER_FOLLOWING = `${PREFIX}GET_USER_FOLLOWING`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const CYCLE_LIST = `${PREFIX}CYCLE_LIST`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    list: [],
    page: 1,
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(GET_USER_FOLLOWING, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(GET_USER_FOLLOWING, 'FAILURE'):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case CYCLE_LIST: {
            const start = state.list.slice(action.offset);
            const append = state.list.slice(0, action.offset);
            const newList = start.concat(append);

            return Object.assign({}, state, {
                list: newList
            });
        }

        case GET_USER_FOLLOWING: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (
                state.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                list: newData,
                onLastPage,
                loading: false
            });
        }

        case FOLLOW_ARTIST:
            return {
                ...state,
                list: Array.from(state.list).concat(action.artist)
            };

        case UNFOLLOW_ARTIST:
            return {
                ...state,
                list: state.list.filter((user) => {
                    return user.id !== action.artist.id;
                })
            };

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function getUserFollowing(options = {}) {
    return (dispatch, getState) => {
        const { profile, token, secret } = getState().currentUser;
        const slug = profile.url_slug;
        const state = getState().currentUserFollowing;
        const { limit = 20, page = state.page } = options;

        return dispatch({
            type: GET_USER_FOLLOWING,
            page,
            limit,
            promise: api.artist.following(slug, { page, limit, token, secret })
        });
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function cycleList(offset = 5) {
    return {
        type: CYCLE_LIST,
        offset
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}
