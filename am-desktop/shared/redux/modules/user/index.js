import commonReducer from 'redux/modules/user/index';
import { GET_USER_FEED } from './feed';
import { LOGIN_AS_USER, LOGOUT_OF_USER } from '../admin';
import { UPLOAD_COMPLETED } from '../upload/index';
import { LABEL_SIGNUP } from '../monetization/label';

// Export reducer specific to desktop using the common reducer to take
// care of shared actions between desktop and mobile

export default function reducer(state, action) {
    const partialState = commonReducer(state, action);

    switch (action.type) {
        case GET_USER_FEED:
            return {
                ...partialState,
                profile: {
                    ...partialState.profile,
                    new_feed_items: 0
                }
            };

        case LOGIN_AS_USER:
            return {
                ...partialState,
                profile: action.resolved.user,
                errors: [],
                token: action.resolved.oauth_token,
                secret: action.resolved.oauth_token_secret,
                loading: false,
                isLoggedIn: true,
                isAdmin: action.resolved.user.is_admin,
                isSuspension: action.resolved.user.is_suspension,
                isAmpApprover: action.resolved.user.is_amp_approver
            };

        case LOGOUT_OF_USER:
            return {
                ...partialState,
                token: action.token,
                secret: action.secret
            };

        case UPLOAD_COMPLETED:
            return {
                ...partialState,
                profile: {
                    ...partialState.profile,
                    upload_count: state.profile.upload_count + 1
                }
            };

        case LABEL_SIGNUP:
            return {
                ...partialState,
                profile: {
                    ...partialState.profile,
                    label_owner: true,
                    video_ads: 'yes'
                }
            };

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    LOG_IN,
    SAVE_USER_DETAILS,
    FAVORITE_ITEM,
    UNFAVORITE_ITEM,
    REPOST_ITEM,
    UNREPOST_ITEM,
    FOLLOW_ARTIST,
    UNFOLLOW_ARTIST,
    logIn,
    identityCheck,
    logInFacebook,
    logInGoogle,
    logInTwitter,
    logInInstagram,
    logInApple,
    getUserFromToken,
    getTwitterRequestToken,
    claimArtist,
    register,
    verifyHash,
    verifyForgotPasswordToken,
    recoverAccount,
    registerWithCaptcha,
    forgotpw,
    clearErrors,
    saveUserDetails,
    updatePassword,
    updateSlug,
    updateEmail,
    deleteAccount,
    logOut,
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction,
    dequeueAction,
    resendEmail,
    follow,
    unfollow,
    unlinkNetwork,
    linkNetwork,
    emailUnsubscribe,
    deleteExternalRssFeedUrl,
    getExternalRssFeedUrl,
    setExternalRssFeedUrl,
    getUserSettings,
    updateUserSettings
} from 'redux/modules/user/index';
