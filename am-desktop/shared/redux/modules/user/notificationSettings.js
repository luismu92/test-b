export { default } from 'redux/modules/user/notificationSettings';

// Export all action dispatchers from the common reducer
export {
    getUserSettings,
    updateUserSettings
} from 'redux/modules/user/notificationSettings';
