import commonReducer from 'redux/modules/user/feed';

export default function reducer(state, action) {
    return commonReducer(state, action);
}

// Export all action dispatchers from the common reducer
export {
    GET_USER_FEED,
    setUploadsOnlyState,
    getUserFeed,
    setPage,
    nextPage,
    reset
} from 'redux/modules/user/feed';
