import api from 'api/index';
import { suffix } from 'utils/index';
import { REPLACE_MUSIC } from '../music/index';

const PREFIX = 'am/user/uploads/';

const GET_USER_UPLOADS = `${PREFIX}GET_USER_UPLOADS`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const SET_CONTEXT = `${PREFIX}SET_CONTEXT`;
const SET_QUERY = `${PREFIX}SET_QUERY`;
const RESET = `${PREFIX}RESET`;
const REMOVE = `${PREFIX}REMOVE`;

const defaultState = {
    list: [],
    page: 1,
    show: 'all',
    query: '',
    loading: false,
    onLastPage: false,
    errors: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(GET_USER_UPLOADS, 'REQUEST'):
            return Object.assign({}, state, {
                errors: [],
                loading: true
            });

        case suffix(GET_USER_UPLOADS, 'FAILURE'):
            return Object.assign({}, state, {
                errors: state.errors.concat(action.error),
                loading: false
            });

        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            });

        case NEXT_PAGE:
            return Object.assign({}, state, {
                page: state.page + 1
            });

        case GET_USER_UPLOADS: {
            const results = action.resolved.results;
            let newData = results;
            let onLastPage = false;

            if (action.page !== 1 && action.page >= state.page) {
                newData = state.list.concat(newData);
            }

            if (
                action.page !== 1 &&
                !results.length /* || action.resolved.results.length < action.limit*/
            ) {
                onLastPage = true;
            }

            return Object.assign({}, state, {
                list: newData,
                page: action.page,
                show: action.show,
                query: action.query,
                onLastPage,
                loading: false
            });
        }

        case SET_QUERY:
            return {
                ...state,
                query: action.query
            };

        case SET_CONTEXT:
            return {
                ...state,
                show: action.show
            };

        case REPLACE_MUSIC:
            return {
                ...state,
                list: state.list.map((item) => {
                    if (item.id === action.resolved.id) {
                        return action.resolved;
                    }

                    return item;
                })
            };

        case RESET:
            return defaultState;

        case REMOVE:
            return {
                ...state,
                list: state.list.filter((item) => item.id !== action.id)
            };

        default:
            return state;
    }
}

export function getUserUploads(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().currentUserUploads;
        const {
            limit,
            sort,
            page = state.page,
            show = state.show,
            query = state.query,
            incompletes,
            pinnable
        } = options;

        const finalPage = Math.max(parseInt(page, 10), 1) || 1;
        const finalLimit = Math.max(parseInt(limit, 10), 1) || 20;

        return dispatch({
            type: GET_USER_UPLOADS,
            page: finalPage,
            show,
            query,
            sort,
            limit: finalLimit,
            promise: api.user.uploads({
                token,
                secret,
                show,
                query,
                sort,
                incompletes,
                pinnable,
                page: finalPage,
                limit: finalLimit
            })
        });
    };
}

export function setQuery(query = defaultState.query) {
    return {
        type: SET_QUERY,
        query
    };
}

export function setContext(show = defaultState.show) {
    return {
        type: SET_CONTEXT,
        show
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function reset() {
    return {
        type: RESET
    };
}

export function removeItem(id) {
    return {
        type: REMOVE,
        id
    };
}

export function replaceItem(item) {
    return {
        type: REPLACE_MUSIC,
        resolved: item
    };
}
