import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/user/notifications/';

const GET_NOTIFICATIONS = `${PREFIX}GET_NOTIFICATIONS`;
const MARK_NOTIFICATIONS_AS_SEEN = `${PREFIX}MARK_NOTIFICATIONS_AS_SEEN`;
const defaultState = {
    error: null,
    loading: false,
    list: [],
    pagingToken: null,
    onLastPage: false,
    lastUpdated: Number.NEGATIVE_INFINITY,
    unseen: 0,
    unread: 0,
    total: 0
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_NOTIFICATIONS):
            return {
                ...state,
                error: defaultState.error,
                loading: true
            };

        case failSuffix(GET_NOTIFICATIONS):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case GET_NOTIFICATIONS: {
            const newState = {
                loading: false,
                unseen: action.resolved.counters.unseen || 0,
                unread: action.resolved.counters.unread || 0,
                total: action.resolved.counters.total || 0,
                pagingToken: action.resolved.paging_token,
                lastUpdated: Date.now(),
                onLastPage: action.resolved.results.length < action.limit
            };

            let newList = action.resolved.results || [];

            if (action.pagingToken) {
                newList = state.list.concat(newList);
            }

            newState.list = newList;

            return {
                ...state,
                ...newState
            };
        }

        case MARK_NOTIFICATIONS_AS_SEEN: {
            if (action.options.all === true) {
                const newList = state.list.map((item) => {
                    return {
                        ...item,
                        seen: true
                    };
                });

                return {
                    ...state,
                    list: newList,
                    unseen: 0
                };
            } else if (action.options.uids && action.options.uids.length) {
                let seenDelta = 0;
                const newList = state.list.map((item) => {
                    if (action.options.uids.includes(item.uid)) {
                        seenDelta += 1;

                        return {
                            ...item,
                            seen: true
                        };
                    }

                    return item;
                });

                return {
                    ...state,
                    list: newList,
                    unseen: state.unseen - seenDelta
                };
            }

            return state;
        }

        default:
            return state;
    }
}

export function getNotifications(options = {}) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const {
            limit = 20,
            pagingToken = state.currentUserNotifications.pagingToken
        } = options;

        return dispatch({
            type: GET_NOTIFICATIONS,
            pagingToken,
            limit,
            promise: api.user.nativeNotifications(token, secret, options)
        });
    };
}

export function markNotificationsAsSeen(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: MARK_NOTIFICATIONS_AS_SEEN,
            options,
            promise: api.user.markNotificationsAsSeen(token, secret, options)
        });
    };
}
