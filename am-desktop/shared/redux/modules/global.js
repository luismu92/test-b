export const NAV_MENU_NOTIFICATIONS = 'header-notifications';
export const NAV_MENU_ACCOUNT = 'header-account';

const PREFIX = 'am/global/';

const HIDE_HEADER_MENU = `${PREFIX}HIDE_HEADER_MENU`;
const SHOW_HEADER_MENU = `${PREFIX}SHOW_HEADER_MENU`;
const SET_GLOBAL_BACKGROUND = `${PREFIX}SET_GLOBAL_BACKGROUND`;
const SET_ONLINE_STATUS = `${PREFIX}SET_ONLINE_STATUS`;
const HIDE_TOP_AD = `${PREFIX}HIDE_TOP_AD`;
const SHOW_TOP_AD = `${PREFIX}SHOW_TOP_AD`;

const defaultState = {
    activeNavMenu: null,
    background: null,
    sideBarFollowingOffset: 0,
    isOnline: true,
    topAdVisible: true
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case HIDE_HEADER_MENU:
            return Object.assign({}, state, {
                activeNavMenu: defaultState.activeNavMenu
            });

        case SHOW_HEADER_MENU:
            return Object.assign({}, state, {
                activeNavMenu: action.menu
            });

        case SET_GLOBAL_BACKGROUND:
            return Object.assign({}, state, {
                background: action.image
            });

        case SET_ONLINE_STATUS:
            return Object.assign({}, state, {
                isOnline: action.online
            });

        case HIDE_TOP_AD:
            return {
                ...state,
                topAdVisible: false
            };

        case SHOW_TOP_AD:
            return {
                ...state,
                topAdVisible: true
            };

        default:
            return state;
    }
}

export function hideHeaderMenu() {
    return {
        type: HIDE_HEADER_MENU
    };
}

export function showHeaderMenu(menu, target) {
    return {
        type: SHOW_HEADER_MENU,
        menu,
        target
    };
}

export function setGlobalBackground(image) {
    return {
        type: SET_GLOBAL_BACKGROUND,
        image
    };
}

export function setOnlineStatus(online) {
    return {
        type: SET_ONLINE_STATUS,
        online
    };
}

export function hideTopAd() {
    return {
        type: HIDE_TOP_AD
    };
}

export function showTopAd() {
    return {
        type: SHOW_TOP_AD
    };
}
