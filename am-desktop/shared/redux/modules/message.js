const PREFIX = 'am/message/';

const SHOW_MESSAGE = `${PREFIX}SHOW_MESSAGE`;
const HIDE_MESSAGE = `${PREFIX}HIDE_MESSAGE`;

export const MESSAGE_TYPE_OFFLINE = 'offline';
export const MESSAGE_TYPE_ROUTE_ERROR = 'route-error';

const initialState = {
    // Structure
    // {
    //      type: 'offline',
    //      data: { ...someExtraData }
    // }
    list: []
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SHOW_MESSAGE: {
            let index = -1;

            state.list.some((message, i) => {
                if (action.message === message.type) {
                    index = i;
                    return true;
                }

                return false;
            });

            const newMessages = Array.from(state.list);

            if (index === -1) {
                newMessages.push({
                    type: action.message,
                    data: action.extraData
                });
            } else {
                newMessages[index].data = action.extraData;
            }

            return Object.assign({}, state, {
                list: newMessages
            });
        }

        case HIDE_MESSAGE: {
            const newMessages = state.list.filter((message) => {
                return message.type !== action.message;
            });

            return Object.assign({}, state, {
                list: newMessages
            });
        }

        default:
            return state;
    }
}

export function showMessage(message, extraData = {}) {
    return {
        type: SHOW_MESSAGE,
        message,
        extraData
    };
}

export function hideMessage(message) {
    return {
        type: HIDE_MESSAGE,
        message
    };
}
