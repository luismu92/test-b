export { default } from 'redux/modules/stats/music/cities';

// Export all action dispatchers from the common reducer
export {
    getCityStats,
    nextPage,
    clearList
} from 'redux/modules/stats/music/cities';
