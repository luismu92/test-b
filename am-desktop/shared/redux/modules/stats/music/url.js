export { default } from 'redux/modules/stats/music/url';

// Export all action dispatchers from the common reducer
export { getUrls } from 'redux/modules/stats/music/url';
