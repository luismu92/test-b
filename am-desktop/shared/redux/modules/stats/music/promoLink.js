export { default } from 'redux/modules/stats/music/promoLink';

// Export all action dispatchers from the common reducer
export { getPromoLinks } from 'redux/modules/stats/music/promoLink';
