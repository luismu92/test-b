export { default } from 'redux/modules/stats/artist/fans';

// Export all action dispatchers from the common reducer
export { getFans, clearList } from 'redux/modules/stats/artist/fans';
