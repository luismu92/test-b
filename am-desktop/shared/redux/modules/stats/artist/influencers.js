export { default } from 'redux/modules/stats/artist/influencers';

// Export all action dispatchers from the common reducer
export {
    getInfluencers,
    clearList
} from 'redux/modules/stats/artist/influencers';
