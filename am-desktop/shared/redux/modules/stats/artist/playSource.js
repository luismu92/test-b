export { default } from 'redux/modules/stats/artist/playSource';

// Export all action dispatchers from the common reducer
export {
    getPlaySources,
    clearList
} from 'redux/modules/stats/artist/playSource';
