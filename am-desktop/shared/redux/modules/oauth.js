import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/oauth/';

const LOG_IN = `${PREFIX}LOG_IN`;
const VALIDATE_TOKEN = `${PREFIX}VALIDATE_TOKEN`;
const AUTHORIZE = `${PREFIX}AUTHORIZE`;
const DELETE_TOKEN = `${PREFIX}DELETE_TOKEN`;
const GET_AUTHORIZED_APPS = `${PREFIX}GET_AUTHORIZED_APPS`;
const REVOKE_APP = `${PREFIX}REVOKE_APP`;
const RESET = `${PREFIX}RESET`;

const defaultState = {
    error: null,
    isValidating: false,
    validationError: null,
    isAuthorizing: false,
    authorizeError: null,
    isLoadingApps: false,
    appsError: null,
    apps: [],
    loading: false,
    permissions: null,
    consumerName: null,
    token: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(LOG_IN):
            return {
                ...state,
                error: null,
                loading: true
            };

        case failSuffix(LOG_IN):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case LOG_IN:
            return {
                ...state,
                error: defaultState.error,
                loading: false
            };

        case requestSuffix(VALIDATE_TOKEN):
            return {
                ...state,
                error: null,
                token: defaultState.token,
                isValidating: true
            };

        case failSuffix(VALIDATE_TOKEN):
            return {
                ...state,
                validationError: action.error,
                isValidating: false
            };

        case VALIDATE_TOKEN:
            return {
                ...state,
                token: action.token,
                validationError: defaultState.validationError,
                consumerName: action.resolved.consumer_name,
                appname: action.resolved.appname,
                logo: action.resolved.logo,
                description: action.resolved.description,
                permissions: action.resolved.permissions,
                isValidating: false
            };

        case requestSuffix(AUTHORIZE):
            return {
                ...state,
                error: null,
                isAuthorizing: true
            };

        case failSuffix(AUTHORIZE):
            return {
                ...state,
                authorizeError: action.error,
                isAuthorizing: false
            };

        case AUTHORIZE:
            return {
                ...state,
                authorizeError: defaultState.authorizeError,
                isAuthorizing: false
            };

        case requestSuffix(GET_AUTHORIZED_APPS):
            return {
                ...state,
                appsError: defaultState.appsError,
                isLoadingApps: true
            };

        case failSuffix(GET_AUTHORIZED_APPS):
            return {
                ...state,
                appsError: action.error,
                isLoadingApps: false
            };

        case GET_AUTHORIZED_APPS:
            return {
                ...state,
                isLoadingApps: false,
                apps: action.resolved
            };

        case requestSuffix(REVOKE_APP):
            return {
                ...state,
                apps: state.apps.map((app) => {
                    if (app.name !== action.name) {
                        return app;
                    }

                    return {
                        ...app,
                        isRevoking: true
                    };
                })
            };

        case failSuffix(REVOKE_APP):
            return {
                ...state,
                apps: state.apps.map((app) => {
                    if (app.name !== action.name) {
                        return app;
                    }

                    return {
                        ...app,
                        revokeError: action.error,
                        isRevoking: false
                    };
                })
            };

        case REVOKE_APP:
            return {
                ...state,
                apps: state.apps.filter((app) => {
                    return app.name !== action.name;
                })
            };

        case RESET:
            return defaultState;

        default:
            return state;
    }
}

export function logIn(requestToken, options) {
    return {
        type: LOG_IN,
        promise: api.oauth.login(requestToken, options)
    };
}

export function validateToken(token) {
    return {
        type: VALIDATE_TOKEN,
        token,
        promise: api.oauth.validateToken(token)
    };
}

export function deleteToken(token) {
    return {
        type: DELETE_TOKEN,
        promise: api.oauth.deleteToken(token)
    };
}

export function getAuthorizedApps() {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_AUTHORIZED_APPS,
            promise: api.oauth.getAuthorizedApps(token, secret).then((apps) => {
                return Object.keys(apps).map((app) => {
                    return {
                        name: app,
                        created: apps[app].timestamp * 1000,
                        data: apps[app].data
                    };
                });
            })
        });
    };
}

export function revokeApp(name) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: REVOKE_APP,
            name,
            promise: api.oauth.revokeApp(name, token, secret)
        });
    };
}

export function reset() {
    return {
        type: RESET
    };
}
