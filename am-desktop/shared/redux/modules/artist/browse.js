import api from 'api/index';
import { suffix } from 'utils/index';
import { COLLECTION_TYPE_ARTIST } from 'constants/index';

import { FOLLOW_ARTIST, UNFOLLOW_ARTIST } from '../user';

const PREFIX = 'am/artist/browse/';

const FETCH_ARTISTS = `${PREFIX}FETCH_ARTISTS`;
const SET_GENRE = `${PREFIX}SET_GENRE`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const PREV_PAGE = `${PREFIX}PREV_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;

const defaultState = {
    loading: false,
    activeContext: COLLECTION_TYPE_ARTIST,
    activeGenre: '',
    currentDataType: null,
    errors: [],
    page: 1,
    onLastPage: false,
    showNumbers: false,
    list: []
};

export default function artistReducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(FETCH_ARTISTS, 'REQUEST'):
            return {
                ...state,
                errors: [],
                loading: true
            };

        case suffix(FETCH_ARTISTS, 'FAILURE'):
            return {
                ...state,
                errors: state.errors.concat(action.error),
                loading: false
            };

        case FETCH_ARTISTS: {
            let newData = action.resolved.results;
            let onLastPage = state.onLastPage;

            if (state.currentDataType === action.type && state.page !== 1) {
                newData = state.list.concat(newData);
            }

            if (state.page !== 1 && !action.resolved.results.length) {
                onLastPage = true;
            }

            return {
                ...state,
                list: newData,
                currentDataType: action.type,
                loading: false,
                showNumbers: action.showNumbers,
                onLastPage
            };
        }

        case SET_GENRE: {
            return {
                ...state,
                activeGenre: action.genre
            };
        }

        case SET_PAGE:
            return {
                ...state,
                page: action.page
            };

        case PREV_PAGE:
            return {
                ...state,
                page: Math.max(1, state.page - 1)
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case suffix(UNFOLLOW_ARTIST, 'FAILURE'):
        case suffix(FOLLOW_ARTIST, 'REQUEST'):
            return {
                ...state,
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.artist.id) {
                        newObj.followers_count += 1; // eslint-disable-line
                    }

                    return newObj;
                })
            };

        case suffix(FOLLOW_ARTIST, 'FAILURE'):
        case suffix(UNFOLLOW_ARTIST, 'REQUEST'):
            return {
                ...state,
                list: state.list.map((obj) => {
                    const newObj = {
                        ...obj
                    };

                    if (obj.id === action.artist.id) {
                        newObj.followers_count -= 1; // eslint-disable-line
                    }

                    return newObj;
                })
            };

        default:
            return state;
    }
}

export function fetchArtistList(options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const { page, activeContext } = getState().artistBrowse;

        const { context = activeContext } = options;

        let promise;
        const showNumbers = false;

        switch (context) {
            case COLLECTION_TYPE_ARTIST:
                promise = api.user.follow(token, secret, page);
                break;

            default:
                throw new Error(`Context (${context}) not accounted for.`);
        }

        return dispatch({
            type: FETCH_ARTISTS,
            showNumbers,
            promise
        });
    };
}

export function setGenre(genre = '') {
    return {
        type: SET_GENRE,
        genre
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: parseInt(page, 10)
    };
}

export function prevPage() {
    return {
        type: PREV_PAGE
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}
