export { default } from 'redux/modules/artist/playlists';

// Export all action dispatchers from the common reducer
export {
    getArtistPlaylists,
    setPage,
    nextPage,
    reset
} from 'redux/modules/artist/playlists';
