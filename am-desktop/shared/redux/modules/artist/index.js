import commonReducer, { PREFIX } from 'redux/modules/artist/index';
import { requestSuffix, failSuffix } from 'utils/index';
import api from 'api/index';
import { VERIFY_ARTIST } from '../admin/index';

const FOLLOW_LIST_ARTIST_ADD = `${PREFIX}FOLLOW_LIST_ARTIST_ADD`;
const FOLLOW_LIST_ARTIST_REMOVE = `${PREFIX}FOLLOW_LIST_ARTIST_REMOVE`;

export default function reducer(state, action) {
    const partialState = commonReducer(state, action);

    switch (action.type) {
        case FOLLOW_LIST_ARTIST_ADD:
            return {
                ...partialState,
                profile: {
                    ...partialState.profile,
                    followList: true
                }
            };

        case FOLLOW_LIST_ARTIST_REMOVE:
            return {
                ...state,
                profile: {
                    ...partialState.profile,
                    followList: false
                }
            };

        case failSuffix(VERIFY_ARTIST): {
            if (
                partialState.profile &&
                action.profile.id === partialState.profile.id
            ) {
                return {
                    ...partialState,
                    profile: {
                        ...partialState.profile,
                        verified: action.profile.verified
                    }
                };
            }

            return partialState;
        }

        case requestSuffix(VERIFY_ARTIST): {
            if (
                partialState.profile &&
                action.profile.id === partialState.profile.id
            ) {
                let values = action.profile.verified === 'yes' ? 'no' : 'yes';

                if (action.verified === 'tastemaker') {
                    values =
                        action.profile.verified === 'tastemaker'
                            ? 'no'
                            : 'tastemaker';
                }

                return {
                    ...partialState,
                    profile: {
                        ...partialState.profile,
                        verified: values
                    }
                };
            }

            return partialState;
        }

        default:
            return partialState;
    }
}

export { getArtist } from 'redux/modules/artist/index';

export function addArtistToFollowList(id) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: FOLLOW_LIST_ARTIST_ADD,
            promise: api.followlist.add(id, token, secret)
        });
    };
}

export function removeArtistFromFollowList(id) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: FOLLOW_LIST_ARTIST_REMOVE,
            promise: api.followlist.remove(id, token, secret)
        });
    };
}
