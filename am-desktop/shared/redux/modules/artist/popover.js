import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { FOLLOW_ARTIST, UNFOLLOW_ARTIST } from '../user/index';

const PREFIX = 'am/artist/popover/';

const SET_ARTIST = `${PREFIX}SET_ARTIST`;
const SET_POSITION = `${PREFIX}SET_POSITION`;
const ACTIVATE_POPOVER = `${PREFIX}ACTIVATE_POPOVER`;
const DEACTIVATE_POPOVER = `${PREFIX}DEACTIVATE_POPOVER`;

const defaultState = {
    data: {},
    artistSlug: '',
    loading: false,
    active: false,
    errors: null,
    x: 0,
    y: 0
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(SET_ARTIST):
            return {
                ...state,
                error: defaultState.error,
                artistSlug: defaultState.artistSlug,
                loading: true
            };

        case failSuffix(SET_ARTIST):
            return {
                ...state,
                error: action.error,
                loading: false
            };

        case ACTIVATE_POPOVER:
            return {
                ...state,
                active: true
            };

        case DEACTIVATE_POPOVER:
            return {
                ...state,
                active: false
            };

        case SET_POSITION:
            return {
                ...state,
                x: action.x,
                y: action.y
            };

        case SET_ARTIST: {
            const artist = action.resolved.results;
            const data = {
                ...state.data,
                [artist.url_slug]: artist
            };

            return {
                ...state,
                data,
                artistSlug: artist.url_slug,
                loading: false
            };
        }

        case failSuffix(UNFOLLOW_ARTIST):
        case requestSuffix(FOLLOW_ARTIST):
            return {
                ...state,
                data: Object.keys(state.data).reduce((obj, key) => {
                    const artist = {
                        ...state.data[key]
                    };

                    if (artist.id === action.artist.id) {
                        artist.followers_count += 1; // eslint-disable-line
                    }

                    obj[key] = artist;

                    return obj;
                }, {})
            };

        case failSuffix(FOLLOW_ARTIST):
        case requestSuffix(UNFOLLOW_ARTIST):
            return {
                ...state,
                data: Object.keys(state.data).reduce((obj, key) => {
                    const artist = {
                        ...state.data[key]
                    };

                    if (artist.id === action.artist.id) {
                        artist.followers_count -= 1; // eslint-disable-line
                    }

                    obj[key] = artist;

                    return obj;
                }, {})
            };

        default:
            return state;
    }
}

export function setArtist(slug) {
    return (dispatch, getState) => {
        const currentData = getState().artistPopover;
        const { token, secret } = getState().currentUser;
        const artist = currentData[slug];
        let promise = Promise.resolve({
            results: artist
        });

        if (!artist) {
            promise = api.artist.get(slug, token, secret);
        }

        return dispatch({
            type: SET_ARTIST,
            promise
        });
    };
}

export function setPosition(x, y) {
    return {
        type: SET_POSITION,
        x,
        y
    };
}

export function activatePopover() {
    return {
        type: ACTIVATE_POPOVER
    };
}

export function deactivatePopover() {
    return {
        type: DEACTIVATE_POPOVER
    };
}
