import commonReducer from 'redux/modules/artist/feed';

export default function reducer(state, action) {
    return commonReducer(state, action);
}

// Export all action dispatchers from the common reducer
export {
    setUploadsOnlyState,
    getArtistFeed,
    setPage,
    nextPage,
    reset
} from 'redux/modules/artist/feed';
