import commonReducer from 'redux/modules/artist/pinned';

export default function reducer(state, action) {
    return commonReducer(state, action);
}

// Export all action dispatchers from the common reducer
export { getPinned } from 'redux/modules/artist/pinned';
