import commonReducer, { PREFIX } from 'redux/modules/playlist';
import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const GET_PLAYLIST_TAGS = `${PREFIX}GET_PLAYLIST_TAGS`;
const SET_PLAYLIST_TAGS = `${PREFIX}SET_PLAYLIST_TAGS`;
const GET_PLAYLIST_TRENDING = `${PREFIX}GET_PLAYLIST_TRENDING`;
const SET_PLAYLIST_TRENDING = `${PREFIX}SET_PLAYLIST_TRENDING`;
const UPDATE_PRIVACY = `${PREFIX}UPDATE_PRIVACY`;
const GET_MIXED_PLAYLISTS = `${PREFIX}GET_MIXED_PLAYLISTS`;

const defaultState = {
    tags: [],
    trending: 'no',
    mixedList: [],
    mixedLoading: false,
    mixedError: null
};

export default function reducer(state = defaultState, action) {
    const partialState = {
        // Make sure we get both default states
        ...state,
        ...commonReducer(undefined, action),

        // Run common reducer
        ...commonReducer(state, action)
    };

    switch (action.type) {
        case GET_PLAYLIST_TAGS:
            return {
                ...partialState,
                tags: action.resolved
            };

        case GET_PLAYLIST_TRENDING:
            return {
                ...partialState,
                trending: action.resolved.trending
            };

        case SET_PLAYLIST_TRENDING:
            return {
                ...partialState,
                trending: action.resolved.trending
            };

        case UPDATE_PRIVACY: {
            const newState = {
                ...partialState,
                errors: [],
                loading: false
            };

            if (state.data) {
                newState.data = {
                    ...newState.data,
                    ...action.resolved
                };
            }

            return newState;
        }

        case requestSuffix(GET_MIXED_PLAYLISTS):
            return {
                ...state,
                mixedLoading: true,
                mixedError: defaultState.mixedError
            };

        case failSuffix(GET_MIXED_PLAYLISTS):
            return {
                ...state,
                mixedLoading: false,
                mixedError: action.error
            };

        case GET_MIXED_PLAYLISTS:
            return {
                ...state,
                mixedLoading: false,
                mixedList: action.resolved.results
            };

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    CREATE_PLAYLIST,
    ADD_SONG_TO_PLAYLIST,
    DELETE_SONG_FROM_PLAYLIST,
    DELETE_PLAYLIST,
    FAVORITE_PLAYLIST,
    UNFAVORITE_PLAYLIST,
    getInfo,
    getInfoWithSlugs,
    addSong,
    clearAddSong,
    reorderTracks,
    addSongToPlaylist,
    deleteSong,
    createPlaylist,
    savePlaylistDetails,
    deletePlaylist,
    favoritePlaylist,
    unfavoritePlaylist,
    getFeatured,
    reset
} from 'redux/modules/playlist';

export function getPlaylistTags(playlistId = null, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        options.token = token;
        options.secret = secret;

        const promise = api.playlist.getTags(playlistId, options);

        return dispatch({
            type: GET_PLAYLIST_TAGS,
            id: playlistId,
            promise
        });
    };
}

export function setPlaylistTags(playlistId, tags = [], options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        options.token = token;
        options.secret = secret;

        return dispatch({
            type: SET_PLAYLIST_TAGS,
            id: playlistId,
            promise: api.playlist.setTags(playlistId, tags, options)
        });
    };
}

export function getPlaylistTrending(playlistId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        options.token = token;
        options.secret = secret;

        return dispatch({
            type: GET_PLAYLIST_TRENDING,
            id: playlistId,
            promise: api.playlist.getTrendingStatus(playlistId, options)
        });
    };
}

export function setPlaylistTrending(playlistId, options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        options.token = token;
        options.secret = secret;

        return dispatch({
            type: SET_PLAYLIST_TRENDING,
            id: playlistId,
            promise: api.playlist.trendPlaylist(playlistId, options)
        });
    };
}

export function alterItemPrivacy(playlistId, isPrivate) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: UPDATE_PRIVACY,
            musicId: parseInt(playlistId, 10),
            promise: api.playlist.privacy(playlistId, isPrivate, token, secret)
        });
    };
}

export function getMixedPlaylists(ids = [], options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_MIXED_PLAYLISTS,
            ids,
            promise: api.playlist.getMixedPlaylists(ids, options, token, secret)
        });
    };
}
