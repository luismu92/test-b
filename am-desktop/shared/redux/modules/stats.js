import commonReducer, { PREFIX } from 'redux/modules/stats';
import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const GET_OVERALL_STATS = `${PREFIX}GET_OVERALL_STATS`;
const GET_SNAPSHOT = `${PREFIX}GET_SNAPSHOT`;
const GET_MUSIC_STATS = `${PREFIX}GET_MUSIC_STATS`;
const GET_DAILY = `${PREFIX}GET_DAILY`;
const defaultState = {
    overall: {},
    snapshot: {},
    daily: [],
    music: {}
};

export default function reducer(state = defaultState, action) {
    const partialState = {
        // Make sure we get both default states
        ...state,
        ...commonReducer(undefined, action),

        // Run common reducer
        ...commonReducer(state, action)
    };

    switch (action.type) {
        case requestSuffix(GET_OVERALL_STATS):
            return {
                ...partialState,
                overall: {
                    ...partialState.overall,
                    loading: true
                }
            };

        case failSuffix(GET_OVERALL_STATS):
            return {
                ...partialState,
                overall: {
                    ...partialState.overall,
                    loading: false
                }
            };

        case GET_OVERALL_STATS:
            return {
                ...partialState,
                overall: {
                    ...action.resolved,
                    loading: false
                }
            };

        case requestSuffix(GET_SNAPSHOT):
            return {
                ...partialState,
                snapshot: {
                    ...partialState.snapshot,
                    loading: true
                }
            };

        case failSuffix(GET_SNAPSHOT):
            return {
                ...partialState,
                snapshot: {
                    ...partialState.snapshot,
                    loading: false
                }
            };

        case GET_SNAPSHOT: {
            return {
                ...partialState,
                snapshot: {
                    ...action.resolved.data,
                    loading: false
                }
            };
        }

        case requestSuffix(GET_DAILY):
            return {
                ...partialState,
                snapshot: {
                    ...partialState.snapshot,
                    loading: true
                }
            };

        case failSuffix(GET_DAILY):
            return {
                ...partialState,
                snapshot: {
                    ...partialState.snapshot,
                    loading: false
                },
                daily: {
                    ...action.resolved.data
                }
            };

        case GET_DAILY:
            return {
                ...partialState,
                snapshot: {
                    ...partialState.snapshot,
                    loading: false
                },
                daily: action.resolved.data
            };

        case requestSuffix(GET_MUSIC_STATS):
            return {
                ...partialState,
                music: {
                    ...partialState.music,
                    loading: true
                }
            };

        case failSuffix(GET_MUSIC_STATS):
            return {
                ...partialState,
                music: {
                    ...partialState.music,
                    loading: false
                }
            };

        case GET_MUSIC_STATS:
            return {
                ...partialState,
                music: {
                    ...action.resolved,
                    loading: false
                }
            };

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    TRACK_PLAY,
    reset,
    trackPlay,
    getStatsToken,
    setSection,
    setEnvironment,
    trackAction,
    setReferer
} from 'redux/modules/stats';

export function getOverallStats(show = 'play', timespan = 'week') {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_OVERALL_STATS,
            promise: api.stats.overall(show, timespan, token, secret)
        });
    };
}

export function getSnapshot(start, end) {
    return (dispatch, getState) => {
        const { profile, token, secret } = getState().currentUser;
        const id = profile.id;

        return dispatch({
            type: GET_SNAPSHOT,
            promise: api.stats.snapshot(id, start, end, token, secret)
        });
    };
}

export function getDaily(start, end, attribute = 'play_song') {
    return (dispatch, getState) => {
        const { profile, token, secret } = getState().currentUser;
        const id = profile.id;

        return dispatch({
            type: GET_DAILY,
            promise: api.stats.daily(id, attribute, start, end, token, secret)
        });
    };
}

export function getMusicStats(id, { timespan, start, end, country } = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_MUSIC_STATS,
            promise: api.stats.music({
                id,
                timespan,
                start,
                end,
                country,
                token,
                secret
            })
        });
    };
}
