import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';

const PREFIX = 'am/blogPost/';
const GET_BLOG_POSTS = `${PREFIX}GET_BLOG_POSTS`;

const defaultState = {
    error: null,
    list: [],
    loading: false
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(GET_BLOG_POSTS):
            return {
                ...state,
                loading: true,
                error: defaultState.error
            };

        case failSuffix(GET_BLOG_POSTS):
            return {
                ...state,
                loading: true,
                error: action.error
            };

        case GET_BLOG_POSTS: {
            return Object.assign({}, state, {
                error: defaultState.error,
                loading: false,
                list: Array.from(state.list).concat(action.resolved)
            });
        }

        default:
            return state;
    }
}

export function getBlogPosts() {
    return (dispatch) => {
        return dispatch({
            type: GET_BLOG_POSTS,
            promise: api.external.blog()
        });
    };
}
