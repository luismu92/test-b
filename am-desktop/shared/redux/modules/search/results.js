import api from 'api/index';
import { failSuffix, requestSuffix } from 'utils/index';
import { FOLLOW_ARTIST, UNFOLLOW_ARTIST } from '../user';

const PREFIX = 'am/search/results/';

import {
    SEARCH_QUERY_TYPE_MUSIC,
    SEARCH_CONTEXT_TYPE_POPULAR
} from 'constants/index';

const SEARCH = `${PREFIX}SEARCH`;
const SEARCH_INPUT = `${PREFIX}SEARCH_INPUT`;
const SET_QUERY_CONTEXT = `${PREFIX}SET_QUERY_CONTEXT`;
const SET_QUERY_TYPE = `${PREFIX}SET_QUERY_TYPE`;
const SET_QUERY_GENRE = `${PREFIX}SET_QUERY_GENRE`;
const SET_QUERY_TAG = `${PREFIX}SET_QUERY_TAG`;
const SET_VERIFIED = `${PREFIX}SET_VERIFIED`;
const SET_PAGE = `${PREFIX}SET_PAGE`;
const NEXT_PAGE = `${PREFIX}NEXT_PAGE`;
const CLEAR_LIST = `${PREFIX}CLEAR_LIST`;
const GET_SPONSORED_SONG = `${PREFIX}GET_SPONSORED_SONG`;

const defaultState = {
    activeType: SEARCH_QUERY_TYPE_MUSIC,
    activeContext: SEARCH_CONTEXT_TYPE_POPULAR,
    activeGenre: '',
    tag: '',
    list: [],
    query: '',
    onLastPage: false,
    page: 1,
    loading: false,
    musicCount: 0,
    artistCount: 0,
    songCount: 0,
    albumCount: 0,
    playlistCount: 0,
    verified: false,
    authenticatedArtist: null,
    verifiedArtist: null,
    verifiedTastemaker: null,
    verifiedPlaylist: null,
    tastemakerPlaylist: null,
    related: false
};

const cache = {};
const CACHE_LENGTH = 1000 * 60; // cache for a minute

// eslint-disable-next-line complexity
export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case GET_SPONSORED_SONG: {
            const newData = Array.from(state.list);
            const results = action.resolved.results;

            let toSplice = true;
            let i;

            for (i = 0; i < newData.length; i++) {
                if (newData[i].id === results.id) {
                    toSplice = false;
                }
            }

            if (toSplice) {
                newData.splice(2, 0, action.resolved.results);
            }

            return {
                ...state,
                list: newData
            };
        }
        case requestSuffix(SEARCH): {
            return {
                ...state,
                loading: true
            };
        }

        case failSuffix(SEARCH):
            return {
                ...state,
                loading: false
            };

        case SEARCH: {
            const results = action.resolved.results || [];
            let onLastPage = false;
            let newData = action.resolved.results;
            let related = false;

            if (results.length && !cache[action.cacheKey]) {
                cache[action.cacheKey] = {
                    ...action.resolved,
                    lastUpdated: Date.now()
                };
            }

            if (
                state.activeType === action.activeType &&
                state.activeContext === action.activeContext &&
                state.page !== 1
            ) {
                newData = state.list.concat(newData);
            }

            if (!action.resolved.results.length) {
                onLastPage = true;
            }

            if (action.resolved.related) {
                related = true;
            }

            let authenticatedArtist =
                action.resolved.authenticated_artist ||
                defaultState.authenticatedArtist;
            let verifiedArtist =
                action.resolved.verified_artist || defaultState.verifiedArtist;
            let verifiedTastemaker =
                action.resolved.tastemaker_artist ||
                defaultState.verifiedTastemaker;
            let verifiedPlaylist =
                action.resolved.verified_playlist ||
                defaultState.verifiedPlaylist;
            let tastemakerPlaylist =
                action.resolved.tastemaker_playlist ||
                defaultState.tastemakerPlaylist;

            // Subsequent pages don't have the verified key
            if (action.page > 1) {
                authenticatedArtist = state.authenticatedArtist;
                verifiedArtist = state.verifiedArtist;
                verifiedTastemaker = state.verifiedTastemaker;
            }

            if (action.activeType !== 'music') {
                verifiedPlaylist = null;
                tastemakerPlaylist = null;
            }

            return {
                ...state,
                list: newData,
                onLastPage,
                searchActive: false,
                loading: false,
                query: action.query,
                authenticatedArtist,
                verifiedArtist,
                verifiedTastemaker,
                verifiedPlaylist,
                tastemakerPlaylist,
                related,
                musicCount: action.resolved.music_count || 0,
                artistCount: action.resolved.artist_count || 0,
                songCount: action.resolved.song_count || 0,
                albumCount: action.resolved.album_count || 0,
                playlistCount: action.resolved.playlist_count || 0
            };
        }

        case SET_PAGE:
            return {
                ...state,
                page: action.page
            };

        case NEXT_PAGE:
            return {
                ...state,
                page: state.page + 1
            };

        case SEARCH_INPUT:
            return {
                ...state,
                query: action.query
            };

        case SET_QUERY_TYPE:
            return {
                ...state,
                verifiedPlaylist: null,
                tastemakerPlaylist: null,
                activeType: action.queryType
            };

        case SET_QUERY_CONTEXT:
            return {
                ...state,
                activeContext: action.context
            };

        case FOLLOW_ARTIST:
            return {
                ...state,
                list: state.list.map((result) => {
                    if (result.id === action.artist.id) {
                        const newResult = Object.assign({}, result);

                        newResult.followers_count += 1; // eslint-disable-line
                        return newResult;
                    }

                    return result;
                })
            };

        case UNFOLLOW_ARTIST:
            return {
                ...state,
                list: state.list.map((result) => {
                    if (result.id === action.artist.id) {
                        const newResult = Object.assign({}, result);

                        newResult.followers_count -= 1; // eslint-disable-line
                        return newResult;
                    }

                    return result;
                })
            };

        case SET_QUERY_GENRE:
            return {
                ...state,
                activeGenre: action.queryGenre
            };

        case SET_QUERY_TAG: {
            return {
                ...state,
                tag: action.queryTag
            };
        }

        case SET_VERIFIED:
            return {
                ...state,
                verified: action.verified
            };

        case CLEAR_LIST:
            return {
                ...state,
                list: [],
                authenticatedArtist: null,
                verifiedArtist: null,
                verifiedTastemaker: null,
                verifiedPlaylist: null,
                tastemakerPlaylist: null,
                related: null
            };

        default:
            return state;
    }
}

function argRequired() {
    throw new Error('Arg required');
}

function getCacheKey({
    query = argRequired(),
    activeContext = argRequired(),
    activeType = argRequired(),
    activeGenre = argRequired(),
    tag = argRequired(),
    verified = argRequired(),
    page = argRequired(),
    limit = argRequired()
}) {
    return `${query}:${activeContext}:${activeType}:${activeGenre}:${tag}:${verified}:${page}:${limit}`;
}

export function getSearch(query = '', options = {}) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;
        const state = getState().searchResults;
        const {
            limit = 20,
            page = state.page,
            type = state.activeType,
            context = state.activeContext,
            genre = state.activeGenre,
            tag = state.tag,
            verified = state.verified
        } = options;
        const q = query || state.query;
        const key = getCacheKey({
            query: q,
            activeContext: context,
            activeType: type,
            activeGenre: genre,
            tag,
            page,
            limit,
            verified
        });

        let promise;

        if (cache[key] && Date.now() - cache[key].lastUpdated < CACHE_LENGTH) {
            promise = Promise.resolve(cache[key]);
        } else {
            delete cache[key];

            promise = api.search.get(q, {
                page,
                type,
                context,
                genre,
                tag,
                verified,
                limit,
                token,
                secret
            });
        } // disable this for now as it's not in use

        /* promise = promise.then((results) => {
            if (page === 1) {
                dispatch(getSponsoredTrackById(3277672));
            }
            return results;
        });*/
        return dispatch({
            type: SEARCH,
            cacheKey: key,
            query: q,
            activeContext: context,
            activeType: type,
            activeGenre: genre,
            tag,
            verified,
            page,
            limit,
            promise
        });
    };
}

export function getSponsoredTrackById(id, options = {}) {
    return {
        type: GET_SPONSORED_SONG,
        promise: api.music.songById(id, options).then((results) => {
            results.results.isFeatured = true;
            return results;
        })
    };
}

export function searchInput(query = '') {
    return {
        type: SEARCH_INPUT,
        query
    };
}

export function setQueryType(queryType = defaultState.activeType) {
    return {
        type: SET_QUERY_TYPE,
        queryType
    };
}

export function setQueryContext(context = defaultState.activeContext) {
    return {
        type: SET_QUERY_CONTEXT,
        context
    };
}

export function setQueryGenre(queryGenre = defaultState.activeGenre) {
    return {
        type: SET_QUERY_GENRE,
        queryGenre
    };
}

export function setQueryTag(queryTag = defaultState.tag) {
    return {
        type: SET_QUERY_TAG,
        queryTag
    };
}

export function setVerified(verified) {
    return {
        type: SET_VERIFIED,
        verified
    };
}

export function setPage(page = 1) {
    return {
        type: SET_PAGE,
        page: Math.max(parseInt(page, 10), 1) || 1
    };
}

export function nextPage() {
    return {
        type: NEXT_PAGE
    };
}

export function clearList() {
    return {
        type: CLEAR_LIST
    };
}
