import api from 'api/index';
import { suffix } from 'utils/index';

const PREFIX = 'am/search/suggestions/';

const SEARCH_SUGGEST = `${PREFIX}SEARCH_SUGGEST`;

const defaultState = {
    list: [],
    loading: false
};

const cache = {};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case suffix(SEARCH_SUGGEST, 'REQUEST'):
            return Object.assign({}, state, {
                loading: true
            });

        case suffix(SEARCH_SUGGEST, 'FAILURE'):
            return Object.assign({}, state, {
                loading: false
            });

        case SEARCH_SUGGEST: {
            const suggestions = action.resolved.results;

            cache[action.cacheKey] = suggestions;

            return Object.assign({}, state, {
                list: suggestions || [],
                loading: false
            });
        }

        default:
            return state;
    }
}

function argRequired() {
    throw new Error('Arg required');
}

function getCacheKey({
    query = argRequired(),
    activeContext = argRequired(),
    activeType = argRequired()
}) {
    return `${query}:${activeContext}:${activeType}`;
}

export function getSuggestions(query = '') {
    return (dispatch, getState) => {
        const state = getState().searchResults;
        const { activeContext, activeType } = state;
        const key = getCacheKey({ query, activeContext, activeType });
        let promise;

        if (cache[key]) {
            promise = Promise.resolve({
                results: cache[key]
            });
        } else {
            promise = api.search.suggest(query);
        }

        return dispatch({
            type: SEARCH_SUGGEST,
            cacheKey: key,
            promise,
            query
        });
    };
}
