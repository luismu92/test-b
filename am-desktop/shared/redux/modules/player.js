import commonReducer, { PREFIX } from 'redux/modules/player';

const SWAP_QUEUE_ITEMS = `${PREFIX}SWAP_QUEUE_ITEMS`;

// Export reducer specific to desktop using the common reducer to take
// care of shared actions between desktop and mobile

export default function reducer(state, action) {
    const partialState = commonReducer(state, action);

    switch (action.type) {
        case SWAP_QUEUE_ITEMS: {
            const queue = Array.from(state.queue);
            let queueIndex = state.queueIndex;
            const fromTrack = queue.splice(action.fromIndex, 1)[0];

            queue.splice(action.toIndex, 0, fromTrack);

            if (action.fromIndex < queueIndex && action.toIndex >= queueIndex) {
                queueIndex -= 1;
            } else if (
                action.fromIndex > queueIndex &&
                action.toIndex <= queueIndex
            ) {
                queueIndex += 1;
            }

            return {
                ...partialState,
                currentSong: queue[queueIndex],
                queueIndex,
                queue
            };
        }

        default:
            return partialState;
    }
}

// Export all action dispatchers from the common reducer
export {
    editQueue,
    addQueueItem,
    clearQueue,
    shuffleTracks,
    removeQueueItem,
    songLoaded,
    play,
    stop,
    ended,
    pause,
    seek,
    mute,
    unmute,
    setVolume,
    setRepeat,
    next,
    prev,
    timeUpdate,
    refreshCurrentPlaylistItem,
    loadMusicFromStorage,
    setChromecastState,
    setChromecastLoading
} from 'redux/modules/player';

export function swapQueueItems(fromIndex, toIndex) {
    return {
        type: SWAP_QUEUE_ITEMS,
        fromIndex,
        toIndex
    };
}
