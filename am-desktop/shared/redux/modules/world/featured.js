export { default } from 'redux/modules/world/featured';

// Export all action dispatchers from the common reducer
export {
    getFeaturedPosts,
    setIncludes,
    setFilter
} from 'redux/modules/world/featured';
