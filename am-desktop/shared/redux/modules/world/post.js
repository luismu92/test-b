export { default } from 'redux/modules/world/post';

// Export all action dispatchers from the common reducer
export { getById, getBySlug } from 'redux/modules/world/post';
