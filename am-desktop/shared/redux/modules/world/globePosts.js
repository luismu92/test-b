export { default } from 'redux/modules/world/globePosts';

// Export all action dispatchers from the common reducer
export { getAllPosts } from 'redux/modules/world/globePosts';
