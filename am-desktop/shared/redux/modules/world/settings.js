export { default } from 'redux/modules/world/settings';

// Export all action dispatchers from the common reducer
export { getWorldSettings } from 'redux/modules/world/settings';
