export { default } from 'redux/modules/world/location';

// Export all action dispatchers from the common reducer
export { getWorldLocations } from 'redux/modules/world/location';
