export { default } from 'redux/modules/world/page';

// Export all action dispatchers from the common reducer
export { getPages } from 'redux/modules/world/page';
