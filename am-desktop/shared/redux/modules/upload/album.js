import api from 'api/index';
import {
    requestSuffix,
    failSuffix,
    removeAudioExtension,
    uploadSlugify,
    convertSlugToTitle
} from 'utils/index';
import { UPDATE_METADATA, DELETE } from '../music/index';
import { UPLOAD_STATUS } from 'constants/index';

const PREFIX = 'am/upload/album/';

const RESET = `${PREFIX}RESET`;
const UPDATE_UPLOAD_ALBUM_INFO = `${PREFIX}UPDATE_UPLOAD_ALBUM_INFO`;
const UPDATE_UPLOAD_ALBUM_TRACK_INFO = `${PREFIX}UPDATE_UPLOAD_ALBUM_TRACK_INFO`;
const SET_ALBUM = `${PREFIX}SET_ALBUM`;
const SET_IS_ADDING_SINGLES = `${PREFIX}SET_IS_ADDING_SINGLES`;
const QUEUE_ADD_ALBUM_TRACK = `${PREFIX}QUEUE_ADD_ALBUM_TRACK`;
const FLUSH_QUEUED_ALBUM_TRACKS = `${PREFIX}FLUSH_QUEUED_ALBUM_TRACKS`;
const GET_ALBUM_BY_ID = `${PREFIX}GET_ALBUM_BY_ID`;
const QUEUE_PRE_EXISTING_TRACKS = `${PREFIX}QUEUE_PRE_EXISTING_TRACKS`;

export const ADD_ALBUM_TRACK = `${PREFIX}ADD_ALBUM_TRACK`;
export const ADD_PRE_EXISTING_ALBUM_TRACK = `${PREFIX}ADD_PRE_EXISTING_ALBUM_TRACK`;

const initialState = {
    loading: false,
    isAddingSingles: false,
    isSaving: false,
    isDeleting: false,

    info: {},
    // In case we are fetching the album info and adding more tracks
    // while the album is fetching
    trackRequestQueue: [],

    // Successfully requested tracks that we want to add after getting
    // the album info and its initial tracks
    trackQueue: [],

    preExistingTrackQueue: []
};

function defaultTrackObject(track = {}, currentAlbumInfo = {}) {
    let title = track.title;
    const validFileTypes = /\.(mp3|wav|aiff|ogg|m4a)$/i;

    // If the title is empty don't use the full file name for the title
    title = title.replace(validFileTypes, '');
    track.title = title;

    return {
        ...track,
        saved: typeof track.saved === 'undefined' ? false : track.saved,
        artist: track.artist || currentAlbumInfo.artist,
        genre: track.genre || currentAlbumInfo.genre
    };
}

// eslint-disable-next-line complexity
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_ALBUM: {
            const newAlbum = {
                ...action.album,
                ...state.info,
                tracks: action.album.tracks || []
            };
            const currentTracks = state.info.tracks || [];

            return {
                ...state,
                trackRequestQueue: newAlbum.id ? [] : state.trackRequestQueue,
                info: {
                    ...newAlbum,
                    tracks: newAlbum.tracks.map((track, i) => {
                        return defaultTrackObject(
                            {
                                ...(currentTracks[i] || {}),
                                ...track
                            },
                            newAlbum
                        );
                    })
                }
            };
        }

        case SET_IS_ADDING_SINGLES: {
            return {
                ...state,
                isAddingSingles: action.isAddingSingles
            };
        }

        case QUEUE_ADD_ALBUM_TRACK:
            return {
                ...state,
                trackRequestQueue: Array.from(state.trackRequestQueue).concat({
                    key: action.key,
                    uploadId: action.uploadId
                })
            };

        case FLUSH_QUEUED_ALBUM_TRACKS: {
            const newTracks = Array.from(state.info.tracks);
            const trackIds = newTracks.map((track) => track.song_id);

            state.trackQueue.forEach((track) => {
                if (!trackIds.includes(track.song_id)) {
                    newTracks.push(track);
                }
            });

            return {
                ...state,
                trackQueue: [],
                info: {
                    ...state.info,
                    tracks: newTracks
                }
            };
        }

        case ADD_ALBUM_TRACK: {
            const hasTracks = state.info.tracks && state.info.tracks.length;
            const newInfo = {
                ...state.info
            };
            let trackQueue = Array.from(state.trackQueue);

            if (hasTracks) {
                const newTracks = Array.from(state.info.tracks);
                const trackIds = newTracks.map((track) => track.song_id);

                newTracks.push(action.resolved);

                state.trackQueue.forEach((track) => {
                    if (!trackIds.includes(track.song_id)) {
                        newTracks.push(track);
                    }
                });
                newInfo.tracks = newTracks;
                trackQueue = [];
            } else {
                trackQueue.push(action.resolved);
            }

            return {
                ...state,
                trackQueue,
                info: newInfo
            };
        }

        case ADD_PRE_EXISTING_ALBUM_TRACK: {
            const hasTracks = state.info.tracks && state.info.tracks.length;
            const newInfo = {
                ...state.info
            };

            // flush the track individually instead of as part a batch in FLUSH_QUEUED_PRE_EXISTING_ALBUM_TRACKS
            const preExistingTrackQueue = Array.from(
                state.preExistingTrackQueue
            ).filter((track) => track.id !== action.resolved.id);

            if (hasTracks) {
                const newTracks = Array.from(state.info.tracks);
                const trackIds = newTracks.map(
                    (track) => track.id || track.song_id
                );

                state.preExistingTrackQueue.forEach((track) => {
                    // use track.id instead of track.song_id for pre-existing tracks
                    // the preExisting property is used so we can list pre-existing tracks in step 2 of the new upload process
                    // after preExistingTrackQueue has been flushed and before the user clicks on to the next step
                    // previously, this was unnecessary because the page would automatically navigate to AlbumEditPage
                    if (!trackIds.includes(track.id)) {
                        newTracks.push({ ...track, preExisting: true });
                    }
                });
                newInfo.tracks = newTracks;
            } else {
                preExistingTrackQueue.push(action.resolved);
            }

            return {
                ...state,
                preExistingTrackQueue,
                info: newInfo
            };
        }

        case requestSuffix(UPDATE_METADATA): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let albumInfo = state.info;
            let isSaving = state.isSaving;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isSaving: true
                };

                albumInfo = {
                    ...albumInfo,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                albumInfo = {
                    ...state.info
                };

                isSaving = true;
            }

            return {
                ...state,
                info: albumInfo,
                isSaving
            };
        }

        case failSuffix(UPDATE_METADATA): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let albumInfo = state.info;
            let isSaving = state.isSaving;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isSaving: false
                };

                albumInfo = {
                    ...albumInfo,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                albumInfo = {
                    ...state.info
                };
                isSaving = false;
            }

            return {
                ...state,
                info: albumInfo,
                isSaving
            };
        }

        case UPDATE_METADATA: {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks);
            const trackIndex = newTracks.findIndex(
                (track) => track.song_id === action.musicId
            );
            let albumInfo = state.info;
            let isSaving = state.isSaving;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isSaving: false,
                    saved: true
                };

                albumInfo = {
                    ...albumInfo,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                albumInfo = {
                    ...state.info
                };
                isSaving = false;
            }

            return {
                ...state,
                info: albumInfo,
                isSaving
            };
        }

        case requestSuffix(DELETE): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks);
            const trackIndex = newTracks.findIndex(
                (track) =>
                    track.song_id === action.musicId ||
                    track.id === action.musicId
            );
            let albumInfo = state.info;
            let isDeleting = state.isDeleting;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isDeleting: true
                };

                albumInfo = {
                    ...albumInfo,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                albumInfo = {
                    ...state.info
                };
                isDeleting = true;
            }

            return {
                ...state,
                info: albumInfo,
                isDeleting
            };
        }

        case failSuffix(DELETE): {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks);
            const trackIndex = newTracks.findIndex(
                (track) =>
                    track.song_id === action.musicId ||
                    track.id === action.musicId
            );
            let albumInfo = state.info;
            let isDeleting = state.isDeleting;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isDeleting: false
                };

                albumInfo = {
                    ...albumInfo,
                    tracks: newTracks
                };
            } else if (state.info.id === action.musicId) {
                albumInfo = {
                    ...state.info
                };
                isDeleting = false;
            }

            return {
                ...state,
                info: albumInfo,
                isDeleting
            };
        }

        case DELETE: {
            if (!state.info.id) {
                return state;
            }

            const newTracks = Array.from(state.info.tracks);
            const trackIndex = newTracks.findIndex(
                (track) =>
                    track.song_id === action.musicId ||
                    track.id === action.musicId
            );
            const isDeleting = state.isDeleting;
            let albumInfo = state.info;

            if (trackIndex === -1 && state.info.id !== action.musicId) {
                return state;
            }

            if (trackIndex !== -1) {
                newTracks[trackIndex] = {
                    ...newTracks[trackIndex],
                    isDeleting: false
                };

                albumInfo = {
                    ...state.info,
                    tracks: (albumInfo.tracks || []).filter(
                        (track) =>
                            track.song_id !== action.musicId &&
                            track.id !== action.musicId
                    )
                };
            }

            return {
                ...state,
                info: albumInfo,
                isDeleting: isDeleting
            };
        }

        case UPDATE_UPLOAD_ALBUM_INFO: {
            let newTracks = state.info.tracks;

            if (action.input === 'upc') {
                newTracks = newTracks.map((track) => {
                    track[action.input] = action.value;

                    return track;
                });
            }

            const newAlbumInfo = {
                ...state.info,
                [action.input]: action.value,
                tracks: newTracks
            };

            return {
                ...state,
                info: newAlbumInfo
            };
        }

        case UPDATE_UPLOAD_ALBUM_TRACK_INFO: {
            if (!state.info.tracks[action.trackIndex]) {
                return state;
            }

            const newTracks = state.info.tracks.map((track, i) => {
                if (i === action.trackIndex) {
                    track[action.input] = action.value;
                }

                return track;
            });

            return {
                ...state,
                info: {
                    ...state.info,
                    tracks: newTracks
                }
            };
        }

        case QUEUE_PRE_EXISTING_TRACKS: {
            return {
                ...state,
                preExistingTrackQueue: action.tracks
            };
        }

        case RESET:
            return initialState;

        default:
            return state;
    }
}

export function reset() {
    return {
        type: RESET
    };
}

function defaultUploadTrack(track = {}, upload = {}, extraData = {}) {
    return {
        ...upload,
        ...track,
        title: convertSlugToTitle(track.title || upload.title),
        originalTitle: upload.title,
        artist: track.artist || upload.artist,
        ...extraData
    };
}

function attachUploadMetadataToAlbumTracks(album = {}, uploads = []) {
    const trackIds = uploads.map((item) => item.musicId);
    const tracks = album.tracks || [];

    if (!tracks.length) {
        return album;
    }

    // If we start an album with a track, the id
    // of the album is set within the upload
    // object instead of the subsequent track ids
    // that are created from adding an album track
    const newTracks = tracks.map((track, i) => {
        // This might be a bit optimistic in that
        // the first upload is the actual first track
        //
        // Unfortuniately createEntity doesnt allow
        // us the same data as addAlbumTrack where the
        // musicId is set. Prob a better way to do this.
        if (i === 0) {
            const hasZipUpload = uploads.some((upload) => {
                return (
                    upload.title.substr(-4) === '.zip' &&
                    upload.status !== UPLOAD_STATUS.FAILED
                );
            });

            if (hasZipUpload) {
                return track;
            }

            const firstUpload = uploads[0] || {};

            return defaultUploadTrack(track, firstUpload);
        }

        if (trackIds.includes(track.song_id)) {
            const index = trackIds.indexOf(track.song_id);

            return defaultUploadTrack(track, uploads[index]);
        }

        return track;
    });

    return {
        ...album,
        tracks: newTracks
    };
}

export function setAlbum(album) {
    return (dispatch, getState) => {
        const state = getState();
        const { trackRequestQueue, trackQueue } = state.uploadAlbum;

        const ret = dispatch({
            type: SET_ALBUM,
            album: album
        });

        if (trackRequestQueue.length && typeof album.id !== 'undefined') {
            // Add tracks one by one in order
            // trackRequestQueue.reduce((promise, { uploadId, key }) => {
            //     promise = promise.then(() => {
            //         return dispatch(addAlbumTrack(uploadId, key));
            //     });
            //     return promise;
            // }, Promise.resolve());

            trackRequestQueue.forEach(({ uploadId, key }) => {
                dispatch(addAlbumTrack(uploadId, key));
            });
        }

        if (album.tracks && album.tracks.length && trackQueue.length) {
            dispatch({
                type: FLUSH_QUEUED_ALBUM_TRACKS
            });
        }

        return ret;
    };
}

export function setIsAddingSingles(isAddingSingles) {
    return {
        type: SET_IS_ADDING_SINGLES,
        isAddingSingles
    };
}

export function getAlbumById(albumId) {
    return (dispatch, getState) => {
        const state = getState();
        const { list } = state.upload;

        return dispatch({
            type: GET_ALBUM_BY_ID,
            promise: api.music.id(albumId).then((data) => {
                const alteredData = {
                    results: attachUploadMetadataToAlbumTracks(
                        data.results,
                        list
                    )
                };

                dispatch(setAlbum(alteredData.results));

                return alteredData;
            })
        });
    };
}

export function useAlbumEntity({ id: entityId }) {
    return (dispatch, getState) => {
        const state = getState();
        const { info } = state.uploadAlbum;
        const { token, secret } = state.currentUser;
        const { list } = state.upload;

        if (!info.id) {
            dispatch(
                setAlbum({
                    id: entityId
                })
            );

            return api.music
                .id(entityId, { token, secret })
                .then(({ results }) => {
                    return new Promise((resolve) => {
                        require.ensure([], (require) => {
                            const { slugify } = require('transliteration');
                            const album = attachUploadMetadataToAlbumTracks(
                                {
                                    ...results,
                                    stream_only:
                                        typeof results.stream_only === 'boolean'
                                            ? results.stream_only
                                            : true,
                                    url_slug:
                                        uploadSlugify(
                                            results.url_slug ||
                                                slugify(
                                                    removeAudioExtension(
                                                        results.title
                                                    )
                                                )
                                        ) || 'untitled'
                                },
                                list
                            );

                            dispatch(setAlbum(album));

                            resolve({
                                id: entityId
                            });
                        });
                    });
                });
        }

        return Promise.resolve({
            id: entityId
        });
    };
}

export function addAlbumTrack(uploadId, key) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { list } = state.upload;
        const { info: uploadAlbumInfo } = state.uploadAlbum;
        const { info: musicEditInfo } = state.musicEdit;
        const info = musicEditInfo.id ? musicEditInfo : uploadAlbumInfo;

        if (!info.id) {
            return dispatch({
                type: QUEUE_ADD_ALBUM_TRACK,
                uploadId,
                key
            });
        }

        return dispatch({
            type: ADD_ALBUM_TRACK,
            uploadId,
            key,
            promise: api.music
                .addTrackToAlbum(info.id, key, token, secret)
                .then((results) => {
                    const uploadInfo = list.find((obj) => {
                        return obj.id === uploadId;
                    });
                    // If the title is empty don't use the full file name for the title
                    const track = defaultTrackObject(
                        defaultUploadTrack(results, uploadInfo, {
                            song_id: results.id
                        }),
                        info
                    );

                    return track;
                })
        });
    };
}

export function addPreExistingAlbumTrack(uploadId, trackId) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret } = state.currentUser;
        const { list } = state.upload;
        const { info: uploadAlbumInfo } = state.uploadAlbum;
        const { info: musicEditInfo } = state.musicEdit;
        const info = musicEditInfo.id ? musicEditInfo : uploadAlbumInfo;

        if (!info.id) {
            return null;
        }

        return dispatch({
            type: ADD_PRE_EXISTING_ALBUM_TRACK,
            uploadId,
            trackId,
            promise: api.music
                .addPreExistingTrackToAlbum(info.id, trackId, token, secret)
                .then((results) => {
                    const uploadInfo = list.find((obj) => {
                        return obj.id === uploadId;
                    });
                    // If the title is empty don't use the full file name for the title
                    const track = defaultTrackObject(
                        defaultUploadTrack(results, uploadInfo, {
                            song_id: results.id
                        }),
                        info
                    );

                    return track;
                })
        });
    };
}

export function queuePreExistingTracks(tracks) {
    return {
        type: QUEUE_PRE_EXISTING_TRACKS,
        tracks
    };
}

export function removeAlbumTrack(musicId) {
    return {
        type: DELETE,
        musicId: parseInt(musicId, 10)
    };
}

export function updateAlbumInfo(input, value) {
    return {
        type: UPDATE_UPLOAD_ALBUM_INFO,
        input,
        value
    };
}

export function updateAlbumTrackInfo(trackIndex, input, value) {
    return {
        type: UPDATE_UPLOAD_ALBUM_TRACK_INFO,
        trackIndex,
        input,
        value
    };
}
