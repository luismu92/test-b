import api from 'api/index';
import {
    requestSuffix,
    failSuffix,
    removeAudioExtension,
    uploadSlugify
} from 'utils/index';
import { UPDATE_METADATA, DELETE } from '../music/index';

const PREFIX = 'am/upload/song/';

const RESET = `${PREFIX}RESET`;
const UPDATE_UPLOAD_TRACK_INFO = `${PREFIX}UPDATE_UPLOAD_TRACK_INFO`;
const SET_TRACK_INFO = `${PREFIX}SET_TRACK_INFO`;

const initialState = {
    list: []
};

function defaultTrackObject(track = {}) {
    return {
        ...track,
        saved: typeof track.saved === 'undefined' ? false : track.saved,
        stream_only:
            typeof track.stream_only === 'boolean' ? track.stream_only : true,
        private:
            track.private === '' ||
            track.private === false ||
            track.private === 'no'
                ? 'no'
                : 'yes'
    };
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_TRACK_INFO: {
            const newList = Array.from(state.list);
            const trackIndex = newList.findIndex(
                (track) => track.id === action.songId
            );

            if (trackIndex === -1) {
                newList.push(action.info);
            } else {
                newList[trackIndex] = {
                    ...newList[trackIndex],
                    ...action.info
                };
            }

            return {
                list: newList
            };
        }

        case requestSuffix(UPDATE_METADATA): {
            const newList = Array.from(state.list);
            const trackIndex = newList.findIndex(
                (track) => track.id === action.musicId
            );

            if (trackIndex === -1) {
                return state;
            }

            newList[trackIndex] = {
                ...newList[trackIndex],
                isSaving: true
            };

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(UPDATE_METADATA): {
            const newList = Array.from(state.list);
            const trackIndex = newList.findIndex(
                (track) => track.id === action.musicId
            );

            if (trackIndex === -1) {
                return state;
            }

            newList[trackIndex] = {
                ...newList[trackIndex],
                isSaving: false
            };

            return {
                ...state,
                list: newList
            };
        }

        case UPDATE_METADATA: {
            const newList = Array.from(state.list);
            const trackIndex = newList.findIndex(
                (track) => track.id === action.musicId
            );

            if (trackIndex === -1) {
                return state;
            }

            newList[trackIndex] = {
                ...newList[trackIndex],
                ...action.resolved,
                isSaving: false,
                saved: true
            };

            return {
                ...state,
                list: newList
            };
        }

        case requestSuffix(DELETE): {
            const newList = Array.from(state.list);
            const trackIndex = newList.findIndex(
                (track) => track.id === action.musicId
            );

            if (trackIndex === -1) {
                return state;
            }

            newList[trackIndex] = {
                ...newList[trackIndex],
                isDeleting: true
            };

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(DELETE): {
            const newList = Array.from(state.list);
            const trackIndex = newList.findIndex(
                (track) => track.id === action.musicId
            );

            if (trackIndex === -1) {
                return state;
            }

            newList[trackIndex] = {
                ...newList[trackIndex],
                isDeleting: false
            };

            return {
                ...state,
                list: newList
            };
        }

        case DELETE: {
            const newList = Array.from(state.list).filter((song) => {
                return song.id !== action.musicId;
            });

            return {
                ...state,
                list: newList
            };
        }

        case UPDATE_UPLOAD_TRACK_INFO: {
            const newList = state.list.map((track) => {
                if (track.id === action.uploadId) {
                    track[action.input] = action.value;
                }

                return track;
            });

            return {
                ...state,
                list: newList
            };
        }

        case RESET:
            return initialState;

        default:
            return state;
    }
}

export function reset() {
    return {
        type: RESET
    };
}

export function setTrackInfo(songId, info) {
    const title = removeAudioExtension(info.title);

    info.title = title;

    return {
        type: SET_TRACK_INFO,
        songId,
        info
    };
}

export function useSongEntity({ id: entityId, alreadyExists }, uploadId) {
    return (dispatch, getState) => {
        const state = getState();
        const { list: uploadList, defaultGenre } = state.upload;
        const { profile, token, secret } = state.currentUser;
        const uploadInfo = uploadList.find((info) => {
            return info.id === uploadId;
        });

        if (uploadInfo) {
            return api.music
                .id(entityId, { token, secret })
                .then(({ results }) => {
                    return new Promise((resolve) => {
                        require.ensure([], (require) => {
                            const { slugify } = require('transliteration');

                            const songInfo = defaultTrackObject({
                                ...uploadInfo,
                                ...results,
                                title: results.title || uploadInfo.title,
                                genre: defaultGenre || results.genre,
                                video_ad: profile.video_ads,
                                alreadyExists
                            });

                            songInfo.url_slug =
                                uploadSlugify(
                                    songInfo.url_slug ||
                                        slugify(
                                            removeAudioExtension(
                                                songInfo.title
                                            ),
                                            { replace: [[/'/g, '']] }
                                        )
                                ) || 'untitled'; // eslint-disable-line camelcase

                            dispatch(setTrackInfo(entityId, songInfo));

                            resolve({
                                id: entityId
                            });
                        });
                    });
                });
        }

        return Promise.resolve({
            id: entityId
        });
    };
}

export function updateTrackInfo(uploadId, input, value) {
    return {
        type: UPDATE_UPLOAD_TRACK_INFO,
        uploadId,
        input,
        value
    };
}
