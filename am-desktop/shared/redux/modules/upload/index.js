import api from 'api/index';
import { requestSuffix, failSuffix } from 'utils/index';
import { UPLOAD_STATUS, UPLOAD_TYPE } from 'constants/index';
import { UPDATE_METADATA, DELETE, REPLACE_MUSIC } from '../music/index';
import { useSongEntity } from './song';
import { useAlbumEntity, ADD_ALBUM_TRACK } from './album';

const PREFIX = 'am/upload/index/';

const GET_PRESIGNED_URL = `${PREFIX}GET_PRESIGNED_URL`;
const CREATE_ENTITY = `${PREFIX}CREATE_ENTITY`;
const UPLOAD_STARTED = `${PREFIX}UPLOAD_STARTED`;
const RESET = `${PREFIX}RESET`;
const ADD_UPLOAD = `${PREFIX}ADD_UPLOAD`;
const CANCEL_UPLOAD = `${PREFIX}CANCEL_UPLOAD`;
const FILE_INPUT = `${PREFIX}FILE_INPUT`;
const UPDATE_PROGRESS = `${PREFIX}UPDATE_PROGRESS`;
const UPDATE_TOTAL_PROGRESS = `${PREFIX}UPDATE_TOTAL_PROGRESS`;
const FAIL_UPLOAD = `${PREFIX}FAIL_UPLOAD`;
const SET_DEFAULT_GENRE = `${PREFIX}SET_DEFAULT_GENRE`;
const RESET_FILE_INPUT = `${PREFIX}RESET_FILE_INPUT`;

export const UPLOAD_COMPLETED = `${PREFIX}UPLOAD_COMPLETED`;

const defaultState = {
    policy: null,
    policyExpiration: null,
    signature: null,
    defaultGenre: null,
    currentUploadType: null,
    loading: false,
    inProgress: false,
    policyError: null,
    fileInput: {
        lastUpdated: 0,
        id: null,
        replaceId: null,
        uploadType: null
    },
    bytesUploaded: 0,
    bytesTotal: 0,
    list: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case requestSuffix(CREATE_ENTITY):
        case requestSuffix(ADD_ALBUM_TRACK): {
            const newList = Array.from(state.list);
            const index = newList.findIndex(
                (item) => item.id === action.uploadId
            );

            newList[index] = {
                ...newList[index],
                status: UPLOAD_STATUS.PROCESSING
            };

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(CREATE_ENTITY):
        case failSuffix(ADD_ALBUM_TRACK): {
            const newList = Array.from(state.list);
            const index = newList.findIndex(
                (item) => item.id === action.uploadId
            );
            let error = 'There was an error.';

            if (action.error && typeof action.error.errorcode !== 'undefined') {
                switch (action.error.errorcode) {
                    // case 0:
                    //     error = action.error.message;

                    //     break;
                    case 1004:
                        error = `There was an error: ${action.error.message}`;
                        break;

                    case 1029:
                        error = 'Could not complete song upload.';
                        break;

                    case 1009:
                        error = action.error.message;
                        break;

                    default:
                        break;
                }
            }

            newList[index] = {
                ...newList[index],
                errors: [error],
                status: UPLOAD_STATUS.FAILED
            };

            return {
                ...state,
                list: newList
            };
        }

        case FAIL_UPLOAD: {
            const newList = Array.from(state.list);
            const index = newList.findIndex(
                (item) => item.id === action.uploadId
            );

            newList[index] = {
                ...newList[index],
                status: UPLOAD_STATUS.FAILED
            };

            return {
                ...state,
                list: newList
            };
        }

        case ADD_ALBUM_TRACK:
        case CREATE_ENTITY: {
            const newList = Array.from(state.list);
            const index = newList.findIndex(
                (item) => item.id === action.uploadId
            );

            newList[index] = {
                ...newList[index],
                musicId: action.resolved.song_id
                    ? action.resolved.song_id
                    : action.resolved.id,
                status: UPLOAD_STATUS.COMPLETED
            };

            return {
                ...state,
                list: newList
            };
        }

        case requestSuffix(UPDATE_METADATA): {
            const newList = Array.from(state.list);
            const index = newList.findIndex(
                (item) => item.musicId === action.musicId
            );

            newList[index] = {
                ...newList[index],
                saving: true
            };

            return {
                ...state,
                list: newList
            };
        }

        case UPDATE_METADATA:
        case failSuffix(UPDATE_METADATA): {
            const newList = Array.from(state.list);
            const index = newList.findIndex(
                (item) => item.musicId === action.musicId
            );

            newList[index] = {
                ...newList[index],
                saving: false
            };

            return {
                ...state,
                list: newList
            };
        }

        case requestSuffix(DELETE): {
            const newList = state.list.map((item) => {
                if (item.musicId === action.musicId) {
                    item.isDeleting = true;
                }

                return item;
            });

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(DELETE): {
            const newList = state.list.map((item) => {
                if (item.musicId === action.musicId) {
                    item.isDeleting = false;
                }

                return item;
            });

            return {
                ...state,
                list: newList
            };
        }

        case DELETE: {
            const newList = state.list.filter((item) => {
                return item.musicId !== action.musicId;
            });

            return {
                ...state,
                list: newList
            };
        }

        case RESET_FILE_INPUT: {
            return {
                ...state,
                fileInput: {
                    ...state.fileInput,
                    replaceId: defaultState.replaceId
                }
            };
        }

        case FILE_INPUT: {
            return {
                ...state,
                fileInput: {
                    id: action.id,
                    uploadType: action.uploadType,
                    replaceId: action.replaceId,
                    lastUpdated: Date.now()
                }
            };
        }

        case UPLOAD_STARTED: {
            return {
                ...state,
                inProgress: true
            };
        }

        case UPLOAD_COMPLETED: {
            return {
                ...state,
                inProgress: false
            };
        }

        case ADD_UPLOAD: {
            const newList = Array.from(state.list).concat(action.upload);

            return {
                ...state,
                list: newList
            };
        }

        case CANCEL_UPLOAD: {
            const newList = Array.from(state.list);
            const index = newList.findIndex((item) => item.id === action.id);

            newList[index] = {
                ...newList[index],
                errors: ['Upload canceled'],
                status: UPLOAD_STATUS.CANCELED
            };

            return {
                ...state,
                list: newList
            };
        }

        case UPDATE_PROGRESS: {
            const newList = Array.from(state.list);
            const index = newList.findIndex((item) => item.id === action.id);

            if (
                [UPLOAD_STATUS.CANCELED, UPLOAD_STATUS.FAILED].includes(
                    newList[index].status
                )
            ) {
                return state;
            }

            newList[index] = {
                ...newList[index],
                status: UPLOAD_STATUS.UPLOADING,
                bytesUploaded: action.bytesUploaded,
                bytesTotal: action.bytesTotal
            };

            return {
                ...state,
                list: newList
            };
        }

        case UPDATE_TOTAL_PROGRESS: {
            return {
                ...state,
                bytesUploaded: action.bytesUploaded,
                bytesTotal: action.bytesTotal || 1,
                inProgress: !(
                    action.bytesUploaded === 0 && action.bytesTotal === 0
                )
            };
        }

        case SET_DEFAULT_GENRE: {
            return {
                ...state,
                defaultGenre: action.genre
            };
        }

        case requestSuffix(REPLACE_MUSIC): {
            const newList = Array.from(state.list).map((item) => {
                if (item.replaceId === action.id) {
                    return {
                        ...item,
                        status: UPLOAD_STATUS.PROCESSING
                    };
                }

                return item;
            });

            return {
                ...state,
                list: newList
            };
        }

        case failSuffix(REPLACE_MUSIC): {
            const newList = Array.from(state.list).map((item) => {
                if (item.replaceId === action.id) {
                    return {
                        ...item,
                        status: UPLOAD_STATUS.FAILED
                    };
                }

                return item;
            });

            return {
                ...state,
                list: newList
            };
        }

        case REPLACE_MUSIC: {
            const newList = Array.from(state.list).map((item) => {
                if (item.replaceId === action.id) {
                    return {
                        ...item,
                        status: UPLOAD_STATUS.COMPLETED
                    };
                }

                return item;
            });

            return {
                ...state,
                list: newList
            };
        }

        case RESET: {
            return {
                ...state,
                loading: defaultState.loading,
                list: defaultState.list,
                bytesUploaded: defaultState.bytesUploaded,
                bytesTotal: defaultState.bytesTotal,
                inProgress: defaultState.inProgress
            };
        }

        default:
            return state;
    }
}

export function addUpload({
    id,
    bytesUploaded = 0,
    bytesTotal = 1,
    title,
    type,
    replaceId
}) {
    return {
        type: ADD_UPLOAD,
        upload: {
            id,
            bytesUploaded,
            bytesTotal,
            title,
            type,
            replaceId,
            musicId: null,
            saving: false,
            errors: [],
            status: UPLOAD_STATUS.SUBMITTING
        }
    };
}

export function updateProgress(id, bytesUploaded, bytesTotal) {
    return {
        type: UPDATE_PROGRESS,
        id,
        bytesUploaded,
        bytesTotal
    };
}

export function cancelUpload(id) {
    return {
        type: CANCEL_UPLOAD,
        id
    };
}

export function updateTotalProgress(bytesUploaded, bytesTotal) {
    return {
        type: UPDATE_TOTAL_PROGRESS,
        bytesUploaded,
        bytesTotal
    };
}

export function fileInput(id, uploadType, replaceId = null) {
    return {
        type: FILE_INPUT,
        id,
        uploadType,
        replaceId: parseInt(replaceId, 10) || null
    };
}

export function resetFileInput() {
    return {
        type: RESET_FILE_INPUT
    };
}

export function reset() {
    return {
        type: RESET
    };
}

export function uploadStarted() {
    return {
        type: UPLOAD_STARTED
    };
}

export function uploadCompleted() {
    return {
        type: UPLOAD_COMPLETED
    };
}

export function setDefaultGenre(genre) {
    return {
        type: SET_DEFAULT_GENRE,
        genre
    };
}

export function createEntity(uploadId, uploadType, key) {
    return (dispatch, getState) => {
        const state = getState();
        const { token, secret, profile } = state.currentUser;
        const { info } = state.musicEdit;
        const { defaultGenre } = state.upload;

        return dispatch({
            type: CREATE_ENTITY,
            uploadType,
            uploadId,
            promise: api.upload
                .createEntity({
                    type: uploadType,
                    key,
                    artist: info.artist,
                    title: info.title,
                    genre: defaultGenre || profile.genre,
                    token,
                    secret
                })
                .then((results) => {
                    if (uploadType === UPLOAD_TYPE.ALBUM) {
                        return dispatch(
                            useAlbumEntity(results, uploadId, key)
                        ).then(() => {
                            return results;
                        });
                    }

                    return dispatch(useSongEntity(results, uploadId, key)).then(
                        () => {
                            return results;
                        }
                    );
                })
        });
    };
}

export function failUpload(uploadId) {
    return {
        type: FAIL_UPLOAD,
        uploadId
    };
}

export function getPresignedUrl(encodedFilename) {
    return (dispatch, getState) => {
        const { token, secret } = getState().currentUser;

        return dispatch({
            type: GET_PRESIGNED_URL,
            encodedFilename: encodedFilename,
            promise: api.upload.getSignedUrl(encodedFilename, token, secret)
        });
    };
}
