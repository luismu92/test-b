/* global test, expect */

import uploadSongReducer from '../song';
// import * as actions from '../song';
import { DELETE } from '../../music/index';
import { configureStore } from 'redux/configureStore';
import rootReducer from '../../../reducer';

function getDefaultStore(initialState) {
    return configureStore(
        {
            uploadSong: {
                ...uploadSongReducer(undefined, { type: 'gimmeDefaultState' }),
                ...uploadSongReducer(initialState, {
                    type: 'gimmeDefaultState'
                })
            }
        },
        rootReducer
    );
}

test('Deleted music should be removed from song uploads', async function() {
    const list = [
        {
            title: 1,
            type: 'song',
            id: 1,
            streaming_url: 'http://some.site/song.mp3'
        },
        {
            title: 2,
            type: 'song',
            id: 2,
            streaming_url: 'http://some.site/song.mp3'
        }
    ];
    const initialState = {
        list
    };

    const store = getDefaultStore(initialState);

    store.dispatch({
        type: DELETE,
        musicId: list[0].id
    });

    const nextState = store.getState().uploadSong;

    expect(nextState.list.length).toBe(1);
    expect(nextState.list[0].id).toBe(list[1].id);
});
