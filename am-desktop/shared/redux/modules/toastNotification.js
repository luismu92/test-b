const ADD_TOAST = 'am/toastNotification/ADD_TOAST';
const REMOVE_TOAST = 'am/toastNotification/REMOVE_TOAST';
const TOAST_DURATION = 5000;
const MAX_TOASTS = 3;

const toastQueue = [];
const initialState = {
    toasts: [
        // {
        //     type: 'song',
        //     action: 'favorite',
        //     item: 'Kodac Black - Tunnel Vision'
        // }
    ]
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case ADD_TOAST: {
            return Object.assign({}, state, {
                toasts: state.toasts.concat(action.toast)
            });
        }

        case REMOVE_TOAST: {
            return Object.assign({}, state, {
                toasts: state.toasts.slice(1)
            });
        }

        default:
            return state;
    }
}

export function addToast(toast, duration = TOAST_DURATION) {
    return (dispatch, getState) => {
        const notification = getState().toastNotification;
        const action = {
            type: ADD_TOAST,
            toast: {
                ...toast,
                id: Date.now(),
                duration
            }
        };

        function runToast() {
            setTimeout(() => dispatch(removeToast()), duration);

            return dispatch(action);
        }

        if (notification.toasts.length >= MAX_TOASTS) {
            toastQueue.push(runToast);
            return action;
        }

        return runToast();
    };
}

function removeToast() {
    return (dispatch) => {
        const ret = dispatch({
            type: REMOVE_TOAST
        });

        if (toastQueue.length) {
            const runToast = toastQueue[0];

            toastQueue.shift();

            runToast();
        }

        return ret;
    };
}
