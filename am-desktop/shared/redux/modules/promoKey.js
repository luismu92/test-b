export { default } from 'redux/modules/promoKey';

// Export all action dispatchers from the common reducer
export {
    getPromoKeys,
    addPromoKey,
    deletePromoKey,
    reset
} from 'redux/modules/promoKey';
