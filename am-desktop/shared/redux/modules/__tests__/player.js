/* global test, expect, describe */
import { combineReducers } from 'redux';
import { configureStore } from 'redux/configureStore';

import playerReducer from '../player';
import * as actions from '../player';

function getDefaultStore(initialState) {
    const state = {
        stats: {},
        currentUser: {},
        ad: {},
        router: {
            location: {
                key: 'adsfafsdf'
            }
        },
        player: {
            ...playerReducer(undefined, { type: 'gimmeDefaultState' }),
            ...playerReducer(initialState, { type: 'gimmeDefaultState' })
        }
    };

    return configureStore(
        state,
        combineReducers({
            player: playerReducer,
            // The player reducer requires reading properties on
            // these other reducers for some actions so we just
            // return a regular object for those.
            router: () => state.router,
            ad: () => state.ad,
            currentUser: () => state.currentUser,
            stats: () => state.stats
        })
    );
}

describe('#swapQueueItems', () => {
    test('it should swap track indicies successfully', async function() {
        const queue = [
            {
                title: 1,
                type: 'playlist',
                id: 1,
                tracks: [
                    {
                        id: 10,
                        streaming_url: 'http://some.site/song1.mp3'
                    },
                    {
                        id: 12,
                        streaming_url: 'http://some.site/song2.mp3'
                    }
                ]
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song.mp3'
            }
        ];
        const queueIndex = 0;
        const initialState = {
            currentSong: queue[0].tracks[queueIndex],
            queueIndex,
            currentTime: 10,
            paused: true
        };

        const store = getDefaultStore(initialState);

        store.dispatch(actions.editQueue(queue));

        const firstState = store.getState().player;

        expect(firstState.queue[0].id).toBe(10);
        expect(firstState.queue[1].id).toBe(12);
        expect(firstState.queue[2].id).toBe(2);

        store.dispatch(actions.swapQueueItems(1, 0));

        const nextState = store.getState().player;

        expect(nextState.queue[0].id).toBe(12);
        expect(nextState.queue[1].id).toBe(10);
        expect(nextState.queue[2].id).toBe(2);
    });

    test('it should increase the queue index if swapping a song before the currently playing song', async function() {
        const queue = [
            {
                title: 1,
                type: 'playlist',
                id: 1,
                tracks: [
                    {
                        id: 10,
                        streaming_url: 'http://some.site/song1.mp3'
                    },
                    {
                        id: 12,
                        streaming_url: 'http://some.site/song2.mp3'
                    }
                ]
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song.mp3'
            }
        ];
        const queueIndex = 0;
        const initialState = {
            currentSong: queue[0].tracks[queueIndex],
            queueIndex,
            currentTime: 10,
            paused: true
        };

        const store = getDefaultStore(initialState);

        store.dispatch(actions.editQueue(queue));

        const firstState = store.getState().player;

        expect(firstState.queue[0].id).toBe(10);
        expect(firstState.queue[1].id).toBe(12);
        expect(firstState.queue[2].id).toBe(2);

        store.dispatch(actions.swapQueueItems(1, 0));

        let nextState = store.getState().player;

        expect(nextState.queue[0].id).toBe(12);
        expect(nextState.queue[1].id).toBe(10);
        expect(nextState.queue[2].id).toBe(2);

        expect(nextState.queueIndex).toBe(queueIndex + 1);
        expect(nextState.currentSong.id).toBe(initialState.currentSong.id);

        store.dispatch(actions.swapQueueItems(2, 0));

        nextState = store.getState().player;

        expect(nextState.queue[0].id).toBe(2);
        expect(nextState.queue[1].id).toBe(12);
        expect(nextState.queue[2].id).toBe(10);

        expect(nextState.queueIndex).toBe(queueIndex + 2);
        expect(nextState.currentSong.id).toBe(initialState.currentSong.id);
    });

    test('it should decrease the queue index if swapping a song after the currently playing song', async function() {
        const queue = [
            {
                title: 1,
                type: 'playlist',
                id: 1,
                tracks: [
                    {
                        id: 10,
                        streaming_url: 'http://some.site/song1.mp3'
                    },
                    {
                        id: 12,
                        streaming_url: 'http://some.site/song2.mp3'
                    }
                ]
            },
            {
                title: 2,
                type: 'song',
                id: 2,
                streaming_url: 'http://some.site/song.mp3'
            }
        ];
        const queueIndex = 2;
        const initialState = {
            currentSong: queue[1],
            queueIndex,
            currentTime: 10,
            paused: true
        };

        const store = getDefaultStore(initialState);

        store.dispatch(actions.editQueue(queue));

        const firstState = store.getState().player;

        expect(firstState.queue[0].id).toBe(10);
        expect(firstState.queue[1].id).toBe(12);
        expect(firstState.queue[2].id).toBe(2);

        store.dispatch(actions.swapQueueItems(1, 2));

        let nextState = store.getState().player;

        expect(nextState.queue[0].id).toBe(10);
        expect(nextState.queue[1].id).toBe(2);
        expect(nextState.queue[2].id).toBe(12);

        expect(nextState.queueIndex).toBe(queueIndex - 1);
        expect(nextState.currentSong.id).toBe(initialState.currentSong.id);

        store.dispatch(actions.swapQueueItems(0, 2));

        nextState = store.getState().player;

        expect(nextState.queue[0].id).toBe(2);
        expect(nextState.queue[1].id).toBe(12);
        expect(nextState.queue[2].id).toBe(10);

        expect(nextState.queueIndex).toBe(queueIndex - 2);
        expect(nextState.currentSong.id).toBe(initialState.currentSong.id);
    });
});
