const SHOW_DRAWER = 'am/drawer/SHOW_DRAWER';
const HIDE_DRAWER = 'am/drawer/HIDE_DRAWER';
const ADD_ITEMS = 'am/drawer/ADD_ITEMS';

const initialState = {
    visible: false,
    items: []
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SHOW_DRAWER:
            return Object.assign({}, state, {
                visible: true
            });

        case HIDE_DRAWER:
            return Object.assign({}, state, {
                visible: false
            });

        case ADD_ITEMS:
            return Object.assign({}, state, {
                items: action.items
            });

        default:
            return state;
    }
}

export function showDrawer() {
    return {
        type: SHOW_DRAWER
    };
}

export function hideDrawer() {
    return {
        type: HIDE_DRAWER
    };
}

export function addItems(items) {
    return {
        type: ADD_ITEMS,
        items
    };
}
