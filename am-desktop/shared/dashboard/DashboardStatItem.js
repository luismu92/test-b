import React from 'react';
import PropTypes from 'prop-types';

import { humanizeNumber } from 'utils/index';
import { getCountryName } from 'utils/stats';

import Flag from 'components/Flag';

function DashboardStatItem({ item, context }) {
    const mainCellText =
        context === 'countries'
            ? getCountryName(item.geo_country)
            : item.geo_city;

    let textSuffix;

    if (context === 'cities' && item.geo_country) {
        textSuffix = (
            <span style={{ marginLeft: 10, color: '#ddd' }}>
                {getCountryName(item.geo_country)}
            </span>
        );
    }

    let flag;

    if (context === 'countries') {
        flag = <Flag country={item.geo_country} />;
    }

    return (
        <tbody>
            <tr>
                <td className="stats-table__cell flex-2">
                    {flag}
                    {mainCellText}
                    {textSuffix}
                </td>
                <td className="stats-table__cell" data-th="Plays">
                    {humanizeNumber(item.play_song || item.play)}
                </td>
                <td className="stats-table__cell" data-th="Favs">
                    {humanizeNumber(item.favorite)}
                </td>
                <td className="stats-table__cell" data-th="Playlist +">
                    {humanizeNumber(item.playlist_song_add)}
                </td>
                <td className="stats-table__cell" data-th="Re-Ups">
                    {humanizeNumber(item.repost)}
                </td>
            </tr>
        </tbody>
    );
}

DashboardStatItem.propTypes = {
    item: PropTypes.object,
    context: PropTypes.oneOf(['cities', 'countries'])
};

export default DashboardStatItem;
