import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { humanizeNumber } from 'utils/index';
import { getCountryName } from 'utils/stats';

import DashboardModuleTitle from './DashboardModuleTitle';
import ContextButton from '../buttons/ContextButton';

import AndroidLoader from 'components/loaders/AndroidLoader';
import Table from '../components/Table';
import DashboardStatItem from './DashboardStatItem';
import GeoChart from '../stats/GeoChart';

import MapMarker from 'icons/map-marker';

export default class StatsGeo extends Component {
    static propTypes = {
        dataSet: PropTypes.object,
        onContextButtonClick: PropTypes.func,
        activeContext: PropTypes.string,
        onLoadMoreStatsClick: PropTypes.func,
        contextButtonUrlPrefix: PropTypes.string
    };

    renderContextButtons() {
        const {
            onContextButtonClick,
            activeContext,
            contextButtonUrlPrefix
        } = this.props;

        const urlPrefix = contextButtonUrlPrefix || '/dashboard/geography';

        const buttons = [
            {
                text: 'Top Countries',
                context: 'countries'
            },
            {
                text: 'Top Cities',
                context: 'cities'
            }
        ].map((button, i) => {
            return (
                <div className="column small-24 medium-12 half-padding" key={i}>
                    <ContextButton
                        href={`${urlPrefix}/${button.context}`}
                        onClick={onContextButtonClick}
                        text={button.text}
                        context={button.context}
                        active={activeContext === button.context}
                    />
                </div>
            );
        });

        return <div className="row half-padding">{buttons}</div>;
    }

    renderDataTable(data) {
        if (!data.length) {
            return null;
        }

        const { activeContext } = this.props;
        const cellTitle = activeContext === 'cities' ? 'City' : 'Country';

        const headerCells = [
            {
                value: cellTitle,
                props: {
                    className: 'flex-2'
                }
            },
            {
                value: 'Plays'
            },
            {
                value: 'Favs'
            },
            {
                value: 'PLaylist+'
            },
            {
                value: 'Re-Ups'
            }
        ];

        const rows = data.map((item, i) => {
            return (
                <DashboardStatItem
                    key={i}
                    item={item}
                    context={activeContext}
                />
            );
        });

        return (
            <Table
                className="stats-table"
                headerCells={headerCells}
                bodyRows={rows}
                ignoreTbody
            />
        );
    }

    renderChart(data) {
        if (!data.length) {
            return null;
        }

        const { activeContext } = this.props;
        const chartData = data.map((obj) => {
            let plays = obj.play_song || obj.play;

            if (!plays) {
                plays = 0;
            }

            const regionName =
                activeContext === 'cities'
                    ? obj.geo_city
                    : getCountryName(obj.geo_country);

            return [
                { v: regionName, f: '' },
                { v: plays, f: humanizeNumber(plays) },
                `<p style="white-space: nowrap; font-family: 'Open Sans'; font-size: 1rem; color: #000; text-align: center;font-weight: bold;    line-height: 1.2;">${humanizeNumber(
                    plays
                )} <span style="color: #666; font-weight: 600;">plays</span></p><p style="white-space: nowrap; font-family: 'Open Sans'; color: #ffa200; font-size: 0.7rem; text-align: center;">${regionName}</p>`
            ];
        });
        const chartDataHead = [
            'Country',
            { label: 'Play Count', id: 'playCount', type: 'number' },
            { type: 'string', role: 'tooltip', p: { html: true } }
        ];
        chartData.unshift(chartDataHead);

        const displayMode = activeContext === 'cities' ? 'markers' : 'regions';

        const chartOptions = {
            colorAxis: {
                colors: ['#ffa200']
            },
            displayMode: displayMode,
            legend: 'none',
            tooltip: {
                isHtml: true
            }
        };

        return <GeoChart data={chartData} options={chartOptions} />;
    }

    render() {
        const { dataSet, activeContext } = this.props;
        const { loading, page, data, onLastPage } = dataSet;

        let loader;
        if (loading) {
            loader = (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        let loadMoreButton;
        if (!onLastPage && data.length) {
            loadMoreButton = (
                <div className="u-spacing-top u-text-center">
                    <button
                        className="button button--med"
                        data-action={activeContext}
                        data-page={page}
                        onClick={this.props.onLoadMoreStatsClick}
                    >
                        Load more
                    </button>
                </div>
            );
        }

        let emptyState;
        if (!data.length && !loading) {
            emptyState = (
                <p className="u-text-center">
                    Top {activeContext} will be available when you get your
                    first play
                </p>
            );
        }

        return (
            <Fragment>
                <div className="row u-spacing-top-40">
                    <div className="column small-24">
                        <DashboardModuleTitle
                            text="Activity by Geo"
                            suffixText="(Last 30 days)"
                            icon={<MapMarker />}
                        />
                        {this.renderContextButtons()}
                        <div className="u-box-shadow u-padded u-spacing-top-20">
                            {this.renderChart(data)}
                            {this.renderDataTable(data)}
                            {emptyState}
                            {loader}
                        </div>
                        {loadMoreButton}
                    </div>
                </div>
            </Fragment>
        );
    }
}
