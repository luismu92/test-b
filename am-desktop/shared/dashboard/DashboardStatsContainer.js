import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import analytics from 'utils/analytics';
import connectDataFetchers from 'lib/connectDataFetchers';

import { getOverallStats } from '../redux/modules/stats';
import { fileInput, reset as resetUploadList } from '../redux/modules/upload';
import { addToast } from '../redux/modules/toastNotification';
import { resendEmail } from '../redux/modules/user/index';
import {
    getUserUploads,
    nextPage,
    reset as resetUserUploads,
    setQuery,
    removeItem
} from '../redux/modules/user/uploads';
import {
    setAssociatedQuery,
    nextAssociatedPage
} from '../redux/modules/monetization/associatedMusic';

import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_LABEL_SIGNUP,
    MODAL_TYPE_AMP_CODES
} from '../redux/modules/modal';
import { getSummary } from '../redux/modules/monetization/summary';
import { getPaymentInfo } from '../redux/modules/monetization/paymentInfo';
import { deleteItem } from '../redux/modules/music/index';
import { getAssociatedMusic } from '../redux/modules/monetization/associatedMusic';

import requireAuth from '../hoc/requireAuth';
import DashboardStats from './DashboardStats';

class DashboardStatsContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUserUploads: PropTypes.object,
        stats: PropTypes.object,
        associatedMusic: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editActive: null,
            selectedVal: 'This Week',
            confirmMessageOpen: true,
            paymentMessageOpen: true,
            ampSummaryMonth: new Date().getMonth(),
            ampSummaryYear: new Date().getFullYear()
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        if (
            prevProps.currentUserUploads.query &&
            prevProps.currentUserUploads.query.trim() !==
                this.props.currentUserUploads.query.trim()
        ) {
            this.performSearch(this.props.currentUserUploads.query);
        }

        if (
            prevProps.associatedMusic.query &&
            prevProps.associatedMusic.query.trim() !==
                this.props.associatedMusic.query.trim()
        ) {
            this.performAssociatedSearch(this.props.associatedMusic.query);
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(resetUserUploads());
        dispatch(resetUploadList());

        this._timer = null;
        this._attachedBodyListener = null;
        this._attachedElement = null;

        this.removeBodyListener();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBodyClick = (e) => {
        const target = e.target;

        if (
            this._attachedElement.contains(target) ||
            target === this._attachedElement
        ) {
            return;
        }

        this.setState({
            editActive: null
        });

        this.removeBodyListener();
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
    };

    handleResendEmailClick = () => {
        const { dispatch } = this.props;

        this.setState({
            confirmMessageOpen: false
        });

        dispatch(resendEmail());
        dispatch(
            addToast({
                action: 'message',
                message: 'An email confirmation has been sent.'
            })
        );
    };

    handleTimespanClick = (e) => {
        const { dispatch, stats } = this.props;

        const timespan =
            e.currentTarget.getAttribute('data-timespan') || undefined;
        const show = stats.overall.show || undefined;
        const selected = e.currentTarget.getAttribute('data-value') || null;

        this.setState({
            selectedVal: selected
        });

        dispatch(getOverallStats(show, timespan));
    };

    handleShowClick = (e) => {
        const { dispatch, stats } = this.props;

        const show = e.currentTarget.getAttribute('data-show') || undefined;
        const timespan = stats.overall.timespan || undefined;

        dispatch(getOverallStats(show, timespan));
    };

    handleUploadTypeClick = (e) => {
        const { dispatch } = this.props;
        const show = e.currentTarget.getAttribute('data-show');

        dispatch(
            getUserUploads({
                page: 1,
                show,
                incompletes: 'yes'
            })
        );
    };

    handleAssociatedTypeClick = (e) => {
        const { dispatch } = this.props;
        const show = e.currentTarget.getAttribute('data-show');

        dispatch(
            getAssociatedMusic({
                page: 1,
                show,
                incompletes: 'yes'
            })
        );
    };

    handleLoadMoreUploadsClick = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(
            getUserUploads({
                incompletes: 'yes'
            })
        );
    };

    handleLoadMoreAssociatedClick = () => {
        const { dispatch } = this.props;

        dispatch(nextAssociatedPage());
        dispatch(
            getAssociatedMusic({
                incompletes: 'yes'
            })
        );
    };

    handleUploadSearchInput = (e) => {
        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setQuery(val));
    };

    handleAssociatedSearchInput = (e) => {
        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setAssociatedQuery(val));
    };

    handleDeleteClick = (music) => {
        const { dispatch } = this.props;

        let message;

        switch (music.type) {
            case 'song':
                message =
                    "If you need to change some of the information associated with this song, or replace the existing MP3, please cancel and use the 'Edit song' action instead. Deleting a song will break any links the song may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this song, click the 'Delete' button below.";
                break;

            case 'album':
                message =
                    "If you need to change some of the information associated with this album please cancel and use the 'Edit album' action instead. Deleting an album will break any links the album may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this album, click the 'Delete' button below.";
                break;

            default:
                break;
        }

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `Delete ${music.type}: '${music.title}'`,
                message,
                handleConfirm: () =>
                    this.handleDeleteConfirm(music.id, music.type),
                confirmButtonText: `Delete ${music.type}`,
                confirmButtonProps: {
                    className: 'button button--pill button--danger button--wide'
                }
            })
        );
    };

    handleDeleteConfirm = (musicId, type) => {
        const { dispatch } = this.props;

        dispatch(deleteItem(musicId))
            .then(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Your ${type} was deleted`
                    })
                );
                dispatch(removeItem(musicId));
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));

                const ERROR_MUSIC_AMP_FORBIDDEN = 3039;
                let errorMessage = '';
                let messageDuration;

                if (error.errorcode === ERROR_MUSIC_AMP_FORBIDDEN) {
                    errorMessage = `This content is monetized through ${
                        error.errors[0]
                    } and must be deleted by them.  Please contact them or you can reach out to contentops@audiomack.com for additional assistance.`;
                    messageDuration = 20000;
                } else {
                    errorMessage =
                        'There was a problem deleting this music item.';
                }

                dispatch(
                    addToast(
                        {
                            action: 'message',
                            item: errorMessage
                        },
                        messageDuration
                    )
                );
                analytics.error(error);
                console.log(error);
            });
    };

    handleReplaceInputChange = (e) => {
        const { dispatch } = this.props;
        const input = e.currentTarget;
        const id = input.id;
        const uploadType = e.currentTarget.getAttribute('data-upload-type');
        const replaceId = parseInt(
            e.currentTarget.getAttribute('data-music-id'),
            10
        );

        dispatch(fileInput(id, uploadType, replaceId));

        this.setState({
            editActive: null
        });

        this.removeBodyListener();
    };

    handleEditItemClick = (item, e) => {
        const sameItem = item.id === this.state.editActive;

        if (!sameItem) {
            this._attachedElement = e.currentTarget.parentElement;
            this.attachBodyListener();
        }

        this.setState({
            editActive: sameItem ? null : item.id
        });
    };

    handleAmpSelectChange = (e) => {
        const { dispatch } = this.props;
        const target = e.currentTarget;
        const name = target.name;
        const key = target.getAttribute('data-key');
        const value = target.options[target.selectedIndex].value;

        if (name === 'year') {
            this.setState({
                [key]: value
            });

            dispatch(
                getSummary({
                    // Months in JS are zero index based while the API is not
                    month: this.state.ampSummaryMonth + 1,
                    year: value
                })
            );
        } else if (name === 'month') {
            this.setState({
                [key]: parseInt(value, 10)
            });

            dispatch(
                getSummary({
                    // Months in JS are zero index based while the API is not
                    month: parseInt(value, 10) + 1,
                    year: this.state.ampSummaryYear
                })
            );
        }
    };

    handleLabelSignup = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
    };

    handleEditAmpCodes = (item) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_AMP_CODES, { item, contextType: 'uploads' })
        );
    };

    handleAmpAssociatedEditCodes = (item) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_AMP_CODES, { item, contextType: 'associated' })
        );
    };

    handlePaymentCloseClick = () => {
        this.setState({
            paymentMessageOpen: false
        });
    };

    handleConfirmCloseClick = () => {
        this.setState({
            confirmMessageOpen: false
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;

        document.body.removeEventListener('click', this.handleBodyClick, false);
    }

    performSearch(query) {
        const { dispatch } = this.props;

        clearTimeout(this._timer);
        this._timer = setTimeout(() => {
            dispatch(
                getUserUploads({
                    page: 1,
                    query: query,
                    incompletes: 'yes'
                })
            );
        }, 500);
    }

    performAssociatedSearch(query) {
        const { dispatch } = this.props;

        clearTimeout(this._timer);
        this._timer = setTimeout(() => {
            dispatch(
                getAssociatedMusic({
                    page: 1,
                    query: query,
                    incompletes: 'yes'
                })
            );
        }, 500);
    }

    render() {
        return <DashboardStats />;
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        currentUser: state.currentUser,
        monetizationPaymentInfo: state.monetizationPaymentInfo,
        monetizationSummary: state.monetizationSummary,
        currentUserUploads: state.currentUserUploads,
        email: state.email,
        stats: state.stats,
        associatedMusic: state.monetizationAssociatedMusic
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(DashboardStatsContainer, [
        () => getOverallStats(),
        () => getPaymentInfo(),
        () => getUserUploads({ incompletes: 'yes' }),
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            return getSummary();
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            return getAssociatedMusic();
        }
    ])
);
