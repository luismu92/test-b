import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import PercentageBar from '../components/PercentageBar';
import FollowButtonContainer from '../components/FollowButtonContainer';
import Avatar from 'components/Avatar';

import { getArtistUrl } from 'utils/artist';
import { getDynamicImageProps, humanizeNumber } from 'utils/index';

import styles from './DashboardArtistBlock.module.scss';

export default function DashboardArtistBlock({
    plays,
    followers,
    artist,
    percentage,
    className
}) {
    const avatarSize = 50;
    const [src, srcSet] = getDynamicImageProps(
        artist.image_base || artist.image,
        { size: avatarSize }
    );
    const klass = classnames(styles.container, {
        [className]: className
    });
    const followText = followers === 1 ? 'follower' : 'followers';
    const playText = plays === 1 ? 'play' : 'plays';

    let labelText;

    if (plays !== null) {
        labelText = (
            <Fragment>
                {humanizeNumber(plays)} {playText}
            </Fragment>
        );
    }

    if (followers !== null) {
        labelText = (
            <Fragment>
                {humanizeNumber(followers)} {followText}
            </Fragment>
        );
    }

    return (
        <div className={klass}>
            <div className="row">
                <div className={`column small-24 ${styles.artistBlock}`}>
                    <Link
                        to={getArtistUrl(artist)}
                        className={styles.avatarContainer}
                    >
                        <Avatar image={src} srcSet={srcSet} size={avatarSize} />
                    </Link>

                    <div className={styles.artistContent}>
                        <Link
                            to={getArtistUrl(artist)}
                            className={styles.artistName}
                        >
                            <h5>{artist.name}</h5>
                        </Link>
                        {/* <p>
                            <span className="u-text-orange">
                                {artist.followers_count.toLocaleString()}
                            </span>{' '}
                            {followText}
                        </p> */}
                    </div>
                    <FollowButtonContainer artist={artist} />
                </div>
                <div className="column small-24">
                    <div className={styles.progressContainer}>
                        <PercentageBar value={percentage} height={10} />
                        <strong className={styles.progressText}>
                            {labelText}
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    );
}

DashboardArtistBlock.propTypes = {
    plays: PropTypes.number,
    followers: PropTypes.number,
    percentage: PropTypes.number,
    className: PropTypes.string,
    artist: PropTypes.object
};

DashboardArtistBlock.defaultProps = {
    plays: null,
    followers: null
};
