import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
    humanizeNumber,
    getMusicUrl,
    ucfirst,
    yesBool,
    getDynamicImageProps,
    getArtistName
} from 'utils/index';
import classnames from 'classnames';

import MusicIcon from '../icons/music';
import SubmithubIcon from '../icons/submithub';
import CogIcon from '../icons/cog';
import DeleteIcon from '../icons/close-solid';
import CheckIcon from '../icons/check-mark';
import DollarIcon from '../icons/dollar';
import WarningIcon from '../icons/warning';
import LockIcon from 'icons/padlock';
import CloseIcon from '../icons/close-thin';
import CalendarIcon from '../icons/create-calendar';
import AndroidLoader from '../loaders/AndroidLoader';
import ChartLineIcon from '../icons/chart-line';
import DownloadIcon from '../icons/download';

export default class DashboardMusicItem extends Component {
    static propTypes = {
        onEditItemClick: PropTypes.func,
        inProgressUploads: PropTypes.array,
        musicItem: PropTypes.object,
        editActive: PropTypes.number,
        onDeleteClick: PropTypes.func,
        onDownloadClick: PropTypes.func,
        onReplaceInputChange: PropTypes.func,
        ampEnabled: PropTypes.bool,
        index: PropTypes.number,
        trackNumberWidth: PropTypes.number,
        displayEditColumn: PropTypes.bool,
        hideDeleteButton: PropTypes.bool,
        onEditAmpCodesClick: PropTypes.func,
        showPlColumn: PropTypes.bool
    };

    static defaultProps = {
        hideDeleteButton: false,
        displayEditColumn: false,
        trackNumberWidth: 40
    };

    constructor(props) {
        super(props);

        this.state = {
            showAlbumTracks: false
        };
    }
    ////////////////////
    // Event handlers //
    ////////////////////

    handleItemClick = (e) => {
        this.props.onEditItemClick(this.props.musicItem, e);
    };

    handleDeleteClick = (e) => {
        this.props.onDeleteClick(this.props.musicItem, e);
    };

    handleShowAlbumTrackClick = () => {
        this.setState({ showAlbumTracks: !this.state.showAlbumTracks });
    };

    handleEditAmpCodesClick = (e) => {
        this.props.onEditAmpCodesClick(this.props.musicItem, e);
    };

    handleDownloadClick = () => {
        this.props.onDownloadClick(this.props.musicItem);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getDisplayStatus(musicItem) {
        // Takedown preceeds all other statuses and supports no action
        if (this.isTakedownSuspended(musicItem.status)) {
            return 'Suspended';
        }

        // Future release
        if (musicItem.released_offset > 0) {
            const status = 'Releasing in ';

            let secondsLeft = musicItem.released_offset;
            const days = parseInt(secondsLeft / 86400, 10);

            secondsLeft = secondsLeft % 86400;
            const hours = parseInt(secondsLeft / 3600, 10);

            secondsLeft = secondsLeft % 3600;
            const minutes = parseInt(secondsLeft / 60, 10);

            const seconds = parseInt(secondsLeft % 60, 10);

            let plural = '';

            if (days > 0) {
                plural = days === 1 ? '' : 's';
                return `${status} ${days} day${plural}`;
            }

            if (hours > 0) {
                plural = hours === 1 ? '' : 's';
                return `${status} ${hours} hour${plural}`;
            }

            if (minutes > 0) {
                plural = minutes === 1 ? '' : 's';
                return `${status} ${minutes} minute${plural}`;
            }

            plural = seconds === 1 ? '' : 's';

            return `${status} ${seconds} second${plural}`;
        }

        if (musicItem.status === 'incomplete') {
            return 'Incomplete';
        }

        if (yesBool(musicItem.private)) {
            return 'Private';
        }

        if (musicItem.status === 'unplayable') {
            return 'Disabled SoundCloud';
        }

        return 'Live';
    }

    isTakedownSuspended(status) {
        return status === 'takedown' || status === 'suspended';
    }

    renderTooltipForItem(musicItem) {
        const buttonsKlass = classnames(
            'sub-menu',
            'tooltip',
            'dashboard__tooltip',
            {
                'tooltip--active': this.props.editActive === musicItem.id
            }
        );

        const manageUrl = `/edit/${musicItem.type}/${musicItem.id}`;
        const statsUrl = `/stats/music/${musicItem.id}`;
        const editButton = (
            <li key="editButton">
                <Link to={manageUrl}>
                    <CogIcon className="sub-menu__icon" />
                    Edit {this.props.musicItem.type}
                </Link>
            </li>
        );
        const viewButton = (
            <li key="viewButton">
                <Link to={getMusicUrl(musicItem)}>
                    <MusicIcon className="sub-menu__icon" />
                    View {this.props.musicItem.type}
                </Link>
            </li>
        );
        const deleteButton = (
            <li key="deleteButton">
                <button onClick={this.handleDeleteClick}>
                    <DeleteIcon className="sub-menu__icon" />
                    Delete {this.props.musicItem.type}
                </button>
            </li>
        );
        const submitHubButton = (
            <li key="submitHubButton">
                <a
                    href="https://www.submithub.com/apply"
                    target="_blank"
                    rel="nofollow noopener"
                >
                    <SubmithubIcon className="sub-menu__icon" />
                    Apply to Submithub
                </a>
            </li>
        );
        let statsButton;
        if (musicItem.type !== 'playlist') {
            statsButton = (
                <li key="statsButton">
                    <Link to={statsUrl}>
                        <ChartLineIcon className="sub-menu__icon" />
                        Advanced Stats
                    </Link>
                </li>
            );
        }
        const replaceButton = (
            <li key="replaceButton">
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
                <div
                    className="button--file"
                    onClick={this.handleReplaceClick}
                    role="button"
                    tabIndex="0"
                >
                    <input
                        id={`replace-song-${musicItem.id}`}
                        type="file"
                        name="songs"
                        data-upload-type={this.props.musicItem.type}
                        data-music-id={this.props.musicItem.id}
                        accept="audio/*"
                        onChange={this.props.onReplaceInputChange}
                    />
                    <MusicIcon className="sub-menu__icon" />
                    Replace {ucfirst(this.props.musicItem.type)} File
                </div>
            </li>
        );
        const downloadButton = (
            <li key="downloadButton">
                <button onClick={this.handleDownloadClick}>
                    <DownloadIcon className="sub-menu__icon" />
                    Download Source File
                    {musicItem && musicItem.type === 'album' ? 's' : ''}
                </button>
            </li>
        );
        let buttons;

        switch (musicItem.status) {
            case 'unplayable':
                buttons = [
                    editButton,
                    this.props.hideDeleteButton ? null : deleteButton,
                    replaceButton
                ];
                break;

            case 'incomplete':
                buttons = [
                    editButton,
                    this.props.hideDeleteButton ? null : deleteButton
                ];
                break;

            default:
                buttons = [
                    editButton,
                    viewButton,
                    this.props.hideDeleteButton ? null : deleteButton,
                    submitHubButton,
                    statsButton,
                    downloadButton
                ];
                break;
        }
        return <ul className={buttonsKlass}>{buttons}</ul>;
    }

    renderAlbumTrackToggle(music, text) {
        if (music.type !== 'album' || !music.tracks.length) {
            return null;
        }

        const arrowClass = classnames('table-sort-arrow', {
            'table-sort-arrow--ascending': this.state.showAlbumTracks
        });

        return (
            <button onClick={this.handleShowAlbumTrackClick}>
                {text} <span className={arrowClass} />
            </button>
        );
    }

    renderErrorList(item) {
        let errors;

        if (item.errors.length > 0) {
            const errorText = item.errors.join('; ');

            errors = (
                <span data-tooltip={ucfirst(errorText)} data-tooltip-dark>
                    <WarningIcon className="u-text-icon u-margin-0 u-text-red" />
                </span>
            );
        }

        return errors;
    }

    renderStatusIcon(status) {
        let icon;

        switch (status.toLowerCase()) {
            case 'live': {
                icon = (
                    <CheckIcon className="u-text-icon u-margin-0 status-icon status-icon--active" />
                );
                break;
            }

            case 'private': {
                icon = (
                    <LockIcon className="u-text-icon u-margin-0 u-text-red" />
                );
                break;
            }

            case 'suspended':
            case 'incomplete':
            case 'disabled soundcloud': {
                icon = (
                    <CloseIcon className="u-text-icon u-margin-0 status-icon status-icon--error" />
                );
                break;
            }

            default: {
                icon = (
                    <WarningIcon className="u-text-icon u-margin-0 u-text-yellow" />
                );

                if (status.includes('releasing in')) {
                    icon = (
                        <CalendarIcon className="u-text-icon u-margin-0 u-text-yellow" />
                    );
                }
                break;
            }
        }

        return (
            <span
                className="dashboard-table__status"
                data-tooltip={ucfirst(status)}
                data-tooltip-dark
            >
                {icon}
            </span>
        );
    }

    renderStatsCells(musicItem) {
        const { showPlColumn } = this.props;

        return [
            {
                column: 'Plays',
                stat: 'plays-raw'
            },
            {
                column: 'Favs',
                stat: 'favorites-raw'
            },
            showPlColumn
                ? {
                      column: 'Playlist Adds',
                      stat: 'playlists-raw'
                  }
                : '',
            {
                column: 'Comments',
                stat: 'comments'
            }
        ].map(({ column, stat }, i) => {
            if (typeof musicItem.stats[stat] === 'undefined') {
                return null;
            }

            let statCount = humanizeNumber(musicItem.stats[stat]);

            if (musicItem.type !== 'song' && stat === 'playlists-raw') {
                statCount = '--';
            }

            return (
                <td data-th={column} className="dashboard-table__cell" key={i}>
                    {statCount}
                </td>
            );
        });
    }

    render() {
        const {
            musicItem,
            inProgressUploads,
            ampEnabled,
            displayEditColumn,
            index,
            trackNumberWidth
        } = this.props;
        const status = this.getDisplayStatus(musicItem);
        const artist = getArtistName(musicItem);

        let editButton;
        let advancedStatsButton;

        const takedown = this.isTakedownSuspended(musicItem.status);

        if (!takedown) {
            editButton = (
                <button
                    onClick={this.handleItemClick}
                    className="dashboard-table__edit button--link"
                >
                    <CogIcon className="u-text-icon u-text-orange" />
                </button>
            );
        }

        if (!takedown && musicItem.type !== 'playlist') {
            advancedStatsButton = (
                <Link
                    to={`/stats/music/${musicItem.id}`}
                    onClick={this.handleItemClick}
                    className="dashboard-table__edit button--link"
                    data-tooltip="Advanced Stats"
                    style={{ marginRight: '0.5em' }}
                    data-tooltip-dark
                >
                    <ChartLineIcon className="u-text-icon u-text-orange" />
                </Link>
            );
        }

        if (displayEditColumn) {
            const isReplacing = inProgressUploads.filter((item) => {
                return item.replaceId && item.replaceId === musicItem.id;
            });

            if (isReplacing.length) {
                editButton = <AndroidLoader size={20} />;
            }
        }

        let albumTrackRow;

        if (musicItem.type === 'album' && this.state.showAlbumTracks) {
            const [image, srcSet] = getDynamicImageProps(
                musicItem.image_base || musicItem.image,
                {
                    size: 240
                }
            );
            const albumTracks = musicItem.tracks.map((track, i) => {
                return (
                    <p className="dashboard-table__album-track" key={i}>
                        <span className="dashboard-table__album-track-number">
                            {i + 1}.
                        </span>{' '}
                        {track.title}
                    </p>
                );
            });
            const collapseButton = (
                <p className="u-text-center dashboard-table__collapse">
                    {this.renderAlbumTrackToggle(musicItem, 'Collapse Album')}
                </p>
            );

            albumTrackRow = (
                <tr>
                    <td
                        data-th="Album Cover"
                        className="flex-3"
                        style={{ maxWidth: 260, padding: '15px 10px' }}
                    >
                        <div className="u-pos-relative">
                            <img src={image} srcSet={srcSet} alt="" />
                            <Link
                                to={`/edit/${musicItem.type}/${musicItem.id}`}
                                className="button button--pill dashboard-table__album-edit-button"
                            >
                                <CogIcon className="u-text-icon" />
                                Edit Album
                            </Link>
                        </div>
                    </td>
                    <td data-th="Album Tracks">
                        {albumTracks}
                        {collapseButton}
                    </td>
                </tr>
            );
        }

        let ampEnabledCell = null;

        if (ampEnabled && musicItem.type !== 'playlist') {
            const klass = classnames(
                'dashboard-table__icon dashboard-table__icon--amp',
                {
                    'u-text-orange': yesBool(musicItem.video_ad)
                }
            );

            ampEnabledCell = (
                <td
                    data-th="AMP"
                    className="dashboard-table__cell dashboard-table__cell--amp flex-half u-text-center"
                >
                    <button
                        className={klass}
                        onClick={this.handleEditAmpCodesClick}
                    >
                        <DollarIcon />
                    </button>
                </td>
            );
        }

        const rowClass = classnames({
            'table-row--dark': this.state.showAlbumTracks
        });

        const bodyClass = classnames({
            'dashboard-table__body--open': this.state.showAlbumTracks
        });

        let isAlbum;

        if (this.props.musicItem.type === 'album') {
            isAlbum = true;
        }

        const titleCellClass = classnames(
            'dashboard-table__cell dashboard-table__cell--title flex-3',
            {
                'dashboard-table__cell--album':
                    this.props.musicItem.type === 'album'
            }
        );

        return (
            <tbody className={bodyClass}>
                <tr className={rowClass}>
                    {typeof index === 'number' && (
                        <td data-th="#" style={{ maxWidth: trackNumberWidth }}>
                            {index + 1}.
                        </td>
                    )}
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-noninteractive-element-interactions */}
                    <td
                        data-th="Title"
                        className={titleCellClass}
                        onClick={
                            isAlbum ? this.handleShowAlbumTrackClick : null
                        }
                    >
                        {this.renderStatusIcon(status)}
                        <Link to={getMusicUrl(musicItem)}>
                            {musicItem.title}
                        </Link>
                        {this.renderAlbumTrackToggle(musicItem)}
                    </td>
                    <td
                        data-th="Artist"
                        className="dashboard-table__cell dashboard-table__cell--artist flex-3"
                    >
                        {artist}
                    </td>
                    {this.renderStatsCells(musicItem)}
                    {ampEnabledCell}
                    {displayEditColumn && (
                        <td
                            data-th="Actions"
                            className="dashboard-table__cell dashboard-table__cell--actions"
                        >
                            {advancedStatsButton}
                            {editButton}
                            {this.renderTooltipForItem(musicItem)}
                        </td>
                    )}
                </tr>

                {albumTrackRow}
            </tbody>
        );
    }
}
