import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { getPercentage } from 'utils/index';

import DashboardModuleTitle from './DashboardModuleTitle';
import ContextButton from '../buttons/ContextButton';

import AndroidLoader from 'components/loaders/AndroidLoader';
import DashboardArtistBlock from './DashboardArtistBlock';

import UserIcon from '../icons/avatar';

export default class StatsFans extends Component {
    static propTypes = {
        dataSet: PropTypes.object,
        onContextButtonClick: PropTypes.func,
        activeContext: PropTypes.oneOf(['fans', 'influencers']),
        onLoadMoreStatsClick: PropTypes.func,
        contextButtonUrlPrefix: PropTypes.string
    };

    renderContextButtons() {
        const {
            onContextButtonClick,
            activeContext,
            contextButtonUrlPrefix
        } = this.props;

        const urlPrefix = contextButtonUrlPrefix || '/dashboard';

        const buttons = [
            {
                text: 'Top Fans',
                context: 'fans'
            },
            {
                text: 'Top Influencers',
                context: 'influencers'
            }
        ].map((button, i) => {
            return (
                <div className="column small-24 medium-12 half-padding" key={i}>
                    <ContextButton
                        href={`${urlPrefix}/${button.context}`}
                        onClick={onContextButtonClick}
                        text={button.text}
                        context={button.context}
                        active={activeContext === button.context}
                    />
                </div>
            );
        });

        return <div className="row half-padding">{buttons}</div>;
    }

    render() {
        const { dataSet, activeContext } = this.props;
        const { loading, page, data, onLastPage } = dataSet;

        const formatted = data.reduce(
            (acc, source) => {
                if (activeContext === 'fans') {
                    acc.total += source.plays;
                } else if (activeContext === 'influencers') {
                    acc.total += source.followers;
                }

                acc.list.push(source);

                return acc;
            },
            {
                total: 0,
                list: []
            }
        );
        const blocks = data.map((artist, i) => {
            let percentage;

            if (activeContext === 'fans') {
                percentage = getPercentage(artist.plays, formatted.total);
            } else if (activeContext === 'influencers') {
                percentage = getPercentage(artist.followers, formatted.total);
            }

            if (!artist.info) {
                return null;
            }

            return (
                <DashboardArtistBlock
                    plays={artist.plays}
                    followers={artist.followers}
                    className={
                        i === formatted.list.length - 1
                            ? ''
                            : 'u-spacing-bottom'
                    }
                    key={i}
                    artist={artist.info}
                    percentage={percentage}
                />
            );
        });

        let loader;
        if (loading) {
            loader = (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        let emptyState;
        if (!data.length && !loading) {
            emptyState = (
                <p className="u-spacing-top u-text-center">
                    Top {activeContext} will be available once you start getting
                    plays.
                </p>
            );
        }

        let loadMoreButton;
        if (!onLastPage && data.length) {
            loadMoreButton = (
                <div className="u-spacing-top u-text-center">
                    <button
                        className="button button--med"
                        data-action={activeContext}
                        data-page={page}
                        onClick={this.props.onLoadMoreStatsClick}
                    >
                        Load more
                    </button>
                </div>
            );
        }

        const titleText =
            activeContext === 'influencers' ? 'Top Influencers' : 'Top Fans';
        const tooltipText =
            activeContext === 'influencers'
                ? 'Your popular listeners, with at least 50 followers'
                : 'Your listeners, based on play count';

        return (
            <Fragment>
                <div className="row u-spacing-top-40">
                    <div className="column small-24">
                        <DashboardModuleTitle
                            text={titleText}
                            icon={<UserIcon />}
                            suffixText="(Last 90 days)"
                            tooltipText={tooltipText}
                        />
                        {this.renderContextButtons()}
                        <div className="u-spacing-top-20">
                            {blocks}
                            {emptyState}
                            {loader}
                        </div>
                        {loadMoreButton}
                    </div>
                </div>
            </Fragment>
        );
    }
}
