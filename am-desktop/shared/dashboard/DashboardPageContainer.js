import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { parse } from 'query-string';

import { eventLabel } from 'utils/analytics';

import { resendEmail } from '../redux/modules/user/index';
import { showModal, MODAL_TYPE_LABEL_SIGNUP } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';

import requireAuth from '../hoc/requireAuth';
import TunecoreAd from '../components/Tunecore';
import ConfirmMessage from '../components/ConfirmMessage';
import LabelSignupModal from '../modal/LabelSignupModal';
import ArtistValidationAlert from '../components/ArtistValidationAlert';

import DashboardBackLink from './DashboardBackLink';
import DashboardMainContainer from './DashboardMainContainer';
import DashboardManageContainer from './DashboardManageContainer';
import LabelManagementPageContainer from './LabelManagementPageContainer';
import CommentManagementPageContainer from './CommentManagementPageContainer';
import CommentRecentPageContainer from './CommentRecentPageContainer';
import CommentUserCommentsPageContainer from './CommentUserCommentsPageContainer';
import StatsGeoContainer from './StatsGeoContainer';
import StatsFansContainer from './StatsFansContainer';
import ArtistServices from './ArtistServices';
import DashboardSourcesContainer from './DashboardSourcesContainer';

class DashboardPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        currentUser: PropTypes.object,
        monetizationSummary: PropTypes.object,
        monetizationPaymentInfo: PropTypes.object,
        match: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            confirmMessageOpen: true
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.maybeShowLabelSignupModal();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleResendEmailClick = () => {
        const { dispatch } = this.props;

        this.setState({
            confirmMessageOpen: false
        });

        dispatch(resendEmail(window.location.pathname));
        dispatch(
            addToast({
                action: 'message',
                message: 'An email confirmation has been sent.'
            })
        );
    };

    handleLabelSignup = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    maybeShowLabelSignupModal() {
        const { dispatch, location, currentUser } = this.props;

        if (
            parse(location.search).enableAmp &&
            currentUser.profile.label_owner &&
            currentUser.profile.label_owner_status === 'invited'
        ) {
            dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
        }
    }

    renderConfirmEmailMessage() {
        const { currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            return null;
        }

        if (currentUser.profile.verified_email) {
            return null;
        }

        return (
            <div className="row">
                <div className="column small-24 u-text-center">
                    <ConfirmMessage
                        className="u-spacing-top"
                        open
                        hideCloseButton
                    >
                        <p>
                            Please confirm your email. Still having issues?{' '}
                            <button
                                className="button button--link"
                                onClick={this.handleResendEmailClick}
                            >
                                Resend confirmation
                            </button>{' '}
                            to {currentUser.profile.email} or{' '}
                            <Link to="/contact-us">contact support</Link>.
                        </p>
                    </ConfirmMessage>
                </div>
            </div>
        );
    }

    renderPaymentMessage() {
        const {
            monetizationPaymentInfo,
            monetizationSummary,
            currentUser
        } = this.props;
        const { error } = monetizationSummary;

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        if (error && error.errorcode === 14004) {
            return (
                <div className="row">
                    <div className="column small-24 u-text-center">
                        <ConfirmMessage
                            open
                            hideCloseButton
                            className="u-spacing-top"
                        >
                            <p>
                                You have been approved for AMP. Please{' '}
                                <button
                                    className="button button--link"
                                    onClick={this.handleLabelSignup}
                                >
                                    click here to agree to our Terms of Service
                                </button>{' '}
                                and start monetizing your content.
                            </p>
                        </ConfirmMessage>
                    </div>
                </div>
            );
        }

        if (
            !monetizationPaymentInfo.info.payable &&
            !monetizationPaymentInfo.loading
        ) {
            return (
                <div className="row">
                    <div className="column small-24 u-text-center">
                        <ConfirmMessage
                            open
                            hideCloseButton
                            className="u-spacing-top"
                        >
                            <p>
                                Please enter your{' '}
                                <Link to="/monetization/info">
                                    payment details
                                </Link>{' '}
                                to receive payment from Audiomack.
                            </p>
                        </ConfirmMessage>
                    </div>
                </div>
            );
        }

        return null;
    }

    renderBackLink() {
        const { match } = this.props;

        if (match.url === '/dashboard') {
            return null;
        }

        return (
            <div style={{ margin: '30px 0 20px' }}>
                <DashboardBackLink
                    href="/dashboard"
                    text="Back to Creator Dashboard"
                />
            </div>
        );
    }

    render() {
        const {
            match: { params }
        } = this.props;

        let content;
        let tunecoreAd = null;
        const excludeTunecoreAd = [
            'comment',
            'comment-recent',
            'comment-user',
            'artist-services',
            'geography',
            'play-sources'
        ].includes(params);

        if (!excludeTunecoreAd) {
            tunecoreAd = (
                <div className="row">
                    <div className="column">
                        <TunecoreAd
                            className="u-text-center u-spacing-top"
                            linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_afpd_su&utm_content=ghoagpwt"
                            eventLabel={eventLabel.dashboard}
                            variant="condensed"
                        />
                    </div>
                </div>
            );
        }

        switch (params.context) {
            case 'manage':
                content = <DashboardManageContainer />;
                break;

            case 'label':
                content = <LabelManagementPageContainer />;
                break;

            case 'comment':
                content = <CommentManagementPageContainer />;
                break;

            case 'comment-recent':
                content = <CommentRecentPageContainer />;
                break;

            case 'comment-user':
                content = (
                    <CommentUserCommentsPageContainer userId={params.userId} />
                );
                break;

            case 'artist-services':
                content = <ArtistServices />;
                break;

            case 'geography':
                content = <StatsGeoContainer />;
                break;

            case 'fans':
            case 'influencers':
                content = <StatsFansContainer />;
                break;

            case 'play-sources':
                content = (
                    <div className="u-spacing-top">
                        <DashboardSourcesContainer />
                    </div>
                );
                break;

            default:
                content = <DashboardMainContainer />;
                break;
        }

        return (
            <Fragment>
                {this.renderConfirmEmailMessage()}
                {this.renderPaymentMessage()}
                {this.renderBackLink()}
                {this.props.currentUser.profile.verified !== 'tastemaker' &&
                    this.props.currentUser.profile.verified !== 'banned' && (
                        <ArtistValidationAlert
                            currentUser={this.props.currentUser}
                            page="dashboard"
                        />
                    )}
                {tunecoreAd}
                {content}
                <LabelSignupModal inviteCode="friends" />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        monetizationSummary: state.monetizationSummary,
        monetizationPaymentInfo: state.monetizationPaymentInfo
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(DashboardPageContainer);
