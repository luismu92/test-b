import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

import SearchIcon from '../icons/search';
import DiscIcon from '../icons/disc';

import AndroidLoader from '../loaders/AndroidLoader';
import Avatar from '../components/Avatar';
import Table from '../components/Table';
import CreateNewArtistModal from '../modal/CreateNewArtistModal';
import SendClaimProfileEmailModal from '../modal/SendClaimProfileEmailModal';

export default class LabelManagementPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        associatedArtists: PropTypes.object,
        onArtistActionClick: PropTypes.func,
        onSendClaimEmailClick: PropTypes.func,
        onFormSubmit: PropTypes.func,
        onCreateNewArtistClick: PropTypes.func,
        onSearchInput: PropTypes.func,
        onGetMoreArtists: PropTypes.func
    };

    getHeaderCells() {
        return [
            {
                value: 'Artist',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.name.localeCompare(x.name);
                    }

                    return x.name.localeCompare(y.name);
                },
                props: {
                    className: 'flex-2'
                }
            },
            '',
            '',
            '',
            ''
        ];
    }

    renderAssociatedArtists() {
        const {
            onSearchInput,
            currentUser,
            associatedArtists,
            onArtistActionClick,
            onSendClaimEmailClick
        } = this.props;

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        let loader;

        if (associatedArtists.loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const headerCells = this.getHeaderCells();
        const rows = associatedArtists.list.map((artist, i) => {
            const sameUser = artist.id === currentUser.profile.id;
            const uploadHref = '/upload';
            const uploadLink = sameUser ? (
                <Link to={uploadHref} className="u-trunc">
                    <span className="u-fw-600">
                        Upload Content{' '}
                        <span className="u-text-orange">&raquo;</span>
                    </span>
                </Link>
            ) : (
                <button
                    className="u-text-left u-trunc"
                    onClick={onArtistActionClick}
                    data-id={artist.id}
                    data-url={uploadHref}
                >
                    <span className="u-fw-600">
                        Upload Content{' '}
                        <span className="u-text-orange">&raquo;</span>
                    </span>
                </button>
            );
            const manageHref = '/dashboard/manage';
            const manageLink = sameUser ? (
                <Link to={manageHref} className="u-trunc">
                    <span className="u-fw-600">
                        Manage Account{' '}
                        <span className="u-text-orange">&raquo;</span>
                    </span>
                </Link>
            ) : (
                <button
                    className="u-text-left u-trunc"
                    onClick={onArtistActionClick}
                    data-id={artist.id}
                    data-url={manageHref}
                >
                    <span className="u-fw-600">
                        Manage Account{' '}
                        <span className="u-text-orange">&raquo;</span>
                    </span>
                </button>
            );
            const sendClaimProfileEmailLink =
                sameUser || artist.status !== 'ghost' ? null : (
                    <button
                        className="u-text-left u-trunc"
                        onClick={onSendClaimEmailClick}
                        data-id={artist.id}
                        data-name={artist.name}
                    >
                        <span className="u-fw-600">
                            "Claim Profile" Email{' '}
                            <span className="u-text-orange">&raquo;</span>
                        </span>
                    </button>
                );

            return {
                model: artist,
                value: (
                    <tr key={i}>
                        <td
                            data-th="Artist"
                            className="flex-2 u-trunc u-fw-700"
                        >
                            <Avatar
                                size={30}
                                type="artist"
                                image={artist.image_base || artist.image}
                                className="u-text-icon"
                            />
                            {artist.name}
                        </td>
                        <td>
                            <Link
                                to={`/artist/${artist.url_slug}`}
                                className="u-trunc"
                            >
                                <span className="u-fw-600">
                                    View Profile{' '}
                                    <span className="u-text-orange">
                                        &raquo;
                                    </span>
                                </span>
                            </Link>
                        </td>
                        <td>{uploadLink}</td>
                        <td>{manageLink}</td>
                        <td>{sendClaimProfileEmailLink}</td>
                    </tr>
                )
            };
        });
        const table = (
            <Table
                className="dashboard-table"
                headerCells={headerCells}
                bodyRows={rows}
            />
        );

        let loadMoreButton;

        if (!associatedArtists.onLastPage) {
            loadMoreButton = (
                <div className="column small-24 u-text-center u-spacing-top">
                    <button
                        onClick={this.props.onGetMoreArtists}
                        className="button button--pill"
                    >
                        Load more
                    </button>
                </div>
            );
        }

        return (
            <Fragment>
                <div className="row u-spacing-top">
                    <div className="column small-24 dashboard-module__top">
                        <h4 className="dashboard-module__title">
                            <span className="u-text-orange u-text-icon u-inline-block">
                                <DiscIcon />
                            </span>
                            Manage Label
                        </h4>
                    </div>
                </div>
                <div className="row">
                    <div className="column small-24">
                        <div className="u-box-shadow u-padded">
                            <div className="row u-spacing-bottom-em">
                                <div className="column small-24 medium-18">
                                    <h4>Associated Accounts</h4>
                                </div>
                                <div className="column small-24 medium-6">
                                    <form
                                        onSubmit={this.props.onFormSubmit}
                                        className="search-form dashboard-search-form u-margin-0"
                                        style={{ display: 'none' }}
                                    >
                                        <button
                                            type="submit"
                                            className="search-form__submit"
                                            value="Search"
                                        >
                                            <SearchIcon className="" />
                                        </button>
                                        <input
                                            type="text"
                                            className="search-form__input"
                                            placeholder="Search associated music..."
                                            onChange={onSearchInput}
                                            value={
                                                associatedArtists.query || ''
                                            }
                                        />
                                    </form>
                                </div>
                            </div>
                            <div className="row">
                                <div className="column small-24 manage-label-list">
                                    {table}
                                    {loader}
                                </div>
                                {loadMoreButton}
                            </div>
                        </div>
                    </div>
                    <div className="column small-24 u-text-center u-spacing-top">
                        <button
                            className="button button--pill button--padded"
                            onClick={this.props.onCreateNewArtistClick}
                        >
                            Create New Account
                        </button>
                    </div>
                </div>
            </Fragment>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Label Management on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                {this.renderAssociatedArtists()}
                <CreateNewArtistModal />
                <SendClaimProfileEmailModal />
            </Fragment>
        );
    }
}
