import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './DashboardEmptyOverlay.module.scss';

export default function DashboardEmptyOverlay({ message, link }) {
    if (!message) {
        return null;
    }

    let uploadLink;
    if (link) {
        uploadLink = <Link to="/upload">Upload Now</Link>;
    }

    return (
        <div className={styles.overlay}>
            <p className={styles.message}>
                {message} {uploadLink}
            </p>
        </div>
    );
}

DashboardEmptyOverlay.propTypes = {
    message: PropTypes.string.isRequired,
    link: PropTypes.bool
};
