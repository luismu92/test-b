import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import InfoTooltip from '../components/InfoTooltip';

import styles from './DashboardModuleTitle.module.scss';

function DashboardModuleTitle({
    text,
    suffixText,
    icon,
    iconClass,
    children,
    style,
    tooltipText,
    spaceBetween,
    popoverText
}) {
    let titleIcon;

    if (icon) {
        const klass = classnames(styles.icon, {
            [styles[iconClass]]: iconClass
        });

        titleIcon = <span className={klass}>{icon}</span>;
    }

    const klass = classnames(styles.container, {
        [styles.containerSpaceBetween]: spaceBetween
    });

    let tooltip;

    if (popoverText || tooltipText) {
        tooltip = (
            <InfoTooltip
                text={popoverText || tooltipText}
                style={{ marginLeft: '0.5em' }}
                clickable={!!popoverText}
            />
        );
    }

    return (
        <div className={klass} style={style}>
            <h2 className={styles.title}>
                {titleIcon}
                {text}
                <span style={{ fontWeight: 400, marginLeft: '0.25em' }}>
                    {suffixText}
                </span>
                {tooltip}
            </h2>
            {children}
        </div>
    );
}

DashboardModuleTitle.propTypes = {
    text: PropTypes.string,
    tooltipText: PropTypes.string,
    suffixText: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    style: PropTypes.object,
    icon: PropTypes.object,
    iconClass: PropTypes.string,
    children: PropTypes.object,
    spaceBetween: PropTypes.bool,
    popoverText: PropTypes.oneOf([PropTypes.string, PropTypes.object])
};

export default DashboardModuleTitle;
