import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import connectDataFetchers from 'lib/connectDataFetchers';
import requireAuth from '../hoc/requireAuth';
import moment from 'moment';

import {
    setCommentContext,
    getComments,
    deleteComment,
    banComment
} from '../redux/modules/comment';
import CommentIcon from 'icons/comment';
import { ucfirst } from 'utils/index';
import { KIND_MANAGEMENT_RECENT } from 'constants/comment';
import { addToast } from '../redux/modules/toastNotification';
import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import TableLarge from '../components/TableLarge';

class CommentRecentPageContainer extends React.PureComponent {
    static propTypes = {
        dispatch: PropTypes.func,
        comment: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            rowIndex: 0
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCommentAction = (e) => {
        e.preventDefault();
        const action = e.currentTarget.getAttribute('data-comment-action');
        const uuid = e.currentTarget.getAttribute('data-comment-uuid');
        const thread = e.currentTarget.getAttribute('data-comment-thread');
        const kind = e.currentTarget.getAttribute('data-comment-kind');
        const id = e.currentTarget.getAttribute('data-comment-id');
        const user = e.currentTarget.getAttribute('data-user');
        const rowIndex = Number(
            e.currentTarget.getAttribute('data-comment-row')
        );

        this.setState({
            rowIndex
        });

        if (action === 'remove') {
            this.doCommentRemove(uuid, thread, kind, id);
            return;
        }

        this.props.dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `${ucfirst(action)} ${
                    action === 'ban' ? 'user' : 'comment'
                }`,
                message: `Are you sure you want to ${action} ${
                    action === 'ban' ? 'this user?' : 'this comment?'
                }`,
                handleConfirm: () => {
                    switch (action) {
                        case 'ban':
                            this.doBanUser(user);
                            break;
                        default:
                            this.props.dispatch(hideModal());
                            break;
                    }
                }
            })
        );
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getColumns() {
        return [
            {
                dataKey: 'created_at',
                label: 'Date',
                width: 100,
                sortable: true,
                render: this.renderDateCell
            },
            {
                dataKey: 'content',
                label: 'Comment',
                width: 500,
                sortable: false,
                render: this.renderCommentCell
            },
            {
                dataKey: 'user_id',
                label: 'User',
                width: 100,
                sortable: true,
                render: this.renderUserCell
            },
            {
                dataKey: 'vote_total',
                label: 'Vote',
                width: 50,
                sortable: true
            },
            {
                dataKey: 'report',
                label: 'Report',
                width: 50,
                sortable: true
            },
            {
                dataKey: 'id',
                label: 'Link',
                width: 50,
                sortable: true,
                render: this.renderItemLinkCell
            },
            {
                dataKey: '',
                label: 'Actions',
                width: 200,
                sortable: false,
                render: this.renderItemActionCell
            }
        ];
    }

    doCommentRemove = (uuid, thread = null, kind, id) => {
        const { dispatch } = this.props;

        dispatch(deleteComment(uuid, thread === '' ? null : thread, kind, id))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Comment deleted.'
                    })
                );
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doBanUser = (user) => {
        const { dispatch } = this.props;

        dispatch(banComment(user))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'User has been banned from commenting.'
                    })
                );
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    renderItemActionCell = ({ rowIndex, rowData }) => {
        const removeButton = (
            <button
                className="music-interaction"
                data-comment-uuid={rowData.uuid}
                data-comment-thread={rowData.thread}
                data-comment-kind={rowData.kind}
                data-comment-id={rowData.id}
                data-comment-action="remove"
                data-comment-row={rowIndex}
                onClick={this.handleCommentAction}
            >
                Remove
            </button>
        );

        const banButton = (
            <button
                className="u-spacing-left-em music-interaction"
                data-user={rowData.user_id}
                data-comment-action="ban"
                data-comment-row={rowIndex}
                onClick={this.handleCommentAction}
            >
                Ban User
            </button>
        );

        return (
            <div>
                {removeButton}
                {banButton}
            </div>
        );
    };

    renderUserCell({ rowData }) {
        const { artist } = rowData;

        if (!artist || !artist.name) {
            return null;
        }

        const tooltip = 'View all comments from this user';

        return (
            <a
                target="_blank"
                title={tooltip}
                aria-label={tooltip}
                data-tooltip={tooltip}
                href={`/dashboard/comment-user/user/${rowData.user_id}`}
            >
                {artist.name}
            </a>
        );
    }

    renderItemLinkCell = ({ rowData }) => {
        const { context, kind } = rowData;

        return (
            <a href={context.url} target="_blank" title={context.name}>
                {kind}
            </a>
        );
    };

    renderDateCell({ cellData, rowData }) {
        const { context } = rowData;

        return (
            <a href={context.url} target="_blank" title={context.name}>
                <span style={{ fontSize: '0.8em', color: 'grey' }}>
                    {moment.unix(cellData).fromNow()}
                </span>
            </a>
        );
    }

    renderCommentCell({ cellData }) {
        const filteredData = cellData.replace(/<br>/gi, ' ');

        return <div title={filteredData}>{filteredData}</div>;
    }

    render() {
        const { comment } = this.props;
        const filteredComments = comment.comments.filter(
            (item) =>
                item.deleted === false &&
                item.artist &&
                item.artist.comment_banned === false
        );

        return (
            <div className="row u-spacing-bottom u-spacing-top-30">
                <div className="column small-24">
                    <div className="row expanded column small-24 u-box-shadow u-spacing-bottom-30">
                        <div className="column small-24">
                            <div className="feed-bar">
                                <h2 className="feed-bar__title u-fs-18 u-ls-n-06 u-lh-11">
                                    <CommentIcon className="feed-bar__title-icon feed-bar__title-icon--comment u-text-icon" />
                                    <span className="u-spacing-left-5">
                                        <strong>Most Recent Comments</strong>
                                    </span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div className="row expanded column small-24 u-box-shadow">
                        <div className="column small-24 u-padding-y-20">
                            <TableLarge
                                ref={(ref) => (this.tableRef = ref)}
                                loading={comment.loading}
                                items={filteredComments}
                                columns={this.getColumns()}
                                initSort="created_at"
                                emptyMessage="No recent comments"
                                scrollToIndex={this.state.rowIndex}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        comment: state.comment
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return currentUser.isLoggedIn && currentUser.profile.is_admin;
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(
    connectDataFetchers(CommentRecentPageContainer, [
        () => setCommentContext(KIND_MANAGEMENT_RECENT, null),
        () => getComments()
    ])
);
