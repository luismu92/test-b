import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { debounce } from 'utils/index';
import analytics from 'utils/analytics';
import connectDataFetchers from 'lib/connectDataFetchers';

import { fileInput, reset as resetUploadList } from '../redux/modules/upload';
import { addToast } from '../redux/modules/toastNotification';
import {
    deleteExternalRssFeedUrl,
    getExternalRssFeedUrl,
    setExternalRssFeedUrl
} from '../redux/modules/user/index';
import {
    getUserUploads,
    nextPage,
    reset as resetUserUploads,
    setQuery,
    removeItem
} from '../redux/modules/user/uploads';
import {
    getAssociatedMusic,
    setAssociatedQuery,
    nextAssociatedPage
} from '../redux/modules/monetization/associatedMusic';

import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_AMP_CODES
} from '../redux/modules/modal';
import { deleteItem } from '../redux/modules/music/index';
import { getMusicSource } from '../redux/modules/admin';

import requireAuth from '../hoc/requireAuth';
import DashboardManage from './DashboardManage';

class DashboardManageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        history: PropTypes.object,
        upload: PropTypes.object,
        currentUser: PropTypes.object,
        associatedMusic: PropTypes.object,
        currentUserUploads: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editActive: null
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        if (
            prevProps.currentUserUploads.query &&
            prevProps.currentUserUploads.query.trim() !==
                this.props.currentUserUploads.query.trim()
        ) {
            this.performUploadSearch(this.props.currentUserUploads.query);
        }

        if (
            prevProps.associatedMusic.query &&
            prevProps.associatedMusic.query.trim() !==
                this.props.associatedMusic.query.trim()
        ) {
            this.performAssociatedMusicSearch(this.props.associatedMusic.query);
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(resetUserUploads());
        dispatch(resetUploadList());

        this._attachedBodyListener = null;
        this._attachedElement = null;

        this.removeBodyListener();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBodyClick = (e) => {
        const target = e.target;

        if (
            this._attachedElement.contains(target) ||
            target === this._attachedElement
        ) {
            return;
        }

        this.setState({
            editActive: null
        });

        this.removeBodyListener();
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
    };

    handleRssSubmit = (e) => {
        e.preventDefault();
        e.persist();

        const { currentUser, dispatch } = this.props;
        const { value } = e.target.rssFeedUrl;

        if (value === currentUser.profile.externalRssFeedUrl) {
            dispatch(deleteExternalRssFeedUrl())
                .then(() => {
                    e.target.rssFeedUrl.value = '';

                    return dispatch(
                        addToast({
                            action: 'message',
                            item: 'Your RSS feed has been deleted.'
                        })
                    );
                })
                .catch((error) => {
                    return dispatch(
                        addToast({
                            action: 'message',
                            item: error.message
                        })
                    );
                });

            return;
        }

        dispatch(setExternalRssFeedUrl(value))
            .then(() => {
                return dispatch(
                    addToast({
                        action: 'message',
                        item:
                            'Your RSS feed has been imported. Please allow 24 hours for content to import.'
                    })
                );
            })
            .catch((error) => {
                let message = error.message;

                // To do: make this generic
                if (error.errorcode === 1008) {
                    if (error.errors.url.noConnection) {
                        message += `: ${error.errors.url.noConnection}`;
                    } else if (error.errors.url.notValid) {
                        message += `: ${error.errors.url.notValid}`;
                    }
                }

                return dispatch(
                    addToast({
                        action: 'message',
                        item: message
                    })
                );
            });
    };

    handleUploadTypeClick = (value) => {
        const { dispatch, history } = this.props;

        history.replace(`/dashboard/manage/${value}`);

        dispatch(
            getUserUploads({
                page: 1,
                show: value,
                incompletes: 'yes'
            })
        );
    };

    handleAssociatedTypeClick = (value) => {
        const { dispatch } = this.props;

        dispatch(
            getAssociatedMusic({
                page: 1,
                show: value,
                incompletes: 'yes'
            })
        );
    };

    handleLoadMoreUploadsClick = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(
            getUserUploads({
                incompletes: 'yes'
            })
        );
    };

    handleLoadMoreAssociatedClick = () => {
        const { dispatch } = this.props;

        dispatch(nextAssociatedPage());
        dispatch(
            getAssociatedMusic({
                incompletes: 'yes'
            })
        );
    };

    handleUploadSearchInput = (e) => {
        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setQuery(val));
    };

    handleAssociatedSearchInput = (e) => {
        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setAssociatedQuery(val));
    };

    handleDeleteClick = (music) => {
        const { dispatch } = this.props;

        let message;

        switch (music.type) {
            case 'song':
                message =
                    "If you need to change some of the information associated with this song, or replace the existing MP3, please cancel and use the 'Edit song' action instead. Deleting a song will break any links the song may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this song, click the 'Delete' button below.";
                break;

            case 'album':
                message =
                    "If you need to change some of the information associated with this album please cancel and use the 'Edit album' action instead. Deleting an album will break any links the album may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this album, click the 'Delete' button below.";
                break;

            default:
                break;
        }

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `Delete ${music.type}: '${music.title}'`,
                message,
                handleConfirm: () =>
                    this.handleDeleteConfirm(music.id, music.type),
                confirmButtonText: `Delete ${music.type}`,
                confirmButtonProps: {
                    className: 'button button--pill button--danger button--wide'
                }
            })
        );
    };

    handleDeleteConfirm = (musicId, type) => {
        const { dispatch } = this.props;

        dispatch(deleteItem(musicId))
            .then(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Your ${type} was deleted`
                    })
                );
                dispatch(removeItem(musicId));
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));

                const ERROR_MUSIC_AMP_FORBIDDEN = 3039;
                let errorMessage = '';
                let messageDuration;

                if (error.errorcode === ERROR_MUSIC_AMP_FORBIDDEN) {
                    errorMessage = `This content is monetized through ${
                        error.errors[0]
                    } and must be deleted by them.  Please contact them or you can reach out to contentops@audiomack.com for additional assistance.`;
                    messageDuration = 20000;
                } else {
                    errorMessage =
                        'There was a problem deleting this music item.';
                }

                dispatch(
                    addToast(
                        {
                            action: 'message',
                            item: errorMessage
                        },
                        messageDuration
                    )
                );
                analytics.error(error);
                console.log(error);
            });
    };

    handleReplaceInputChange = (e) => {
        const { dispatch } = this.props;
        const input = e.currentTarget;
        const id = input.id;
        const uploadType = e.currentTarget.getAttribute('data-upload-type');
        const replaceId = parseInt(
            e.currentTarget.getAttribute('data-music-id'),
            10
        );

        dispatch(fileInput(id, uploadType, replaceId));

        this.setState({
            editActive: null
        });

        this.removeBodyListener();
    };

    handleEditItemClick = (item, e) => {
        const sameItem = item.id === this.state.editActive;

        if (!sameItem) {
            this._attachedElement = e.currentTarget.parentElement;
            this.attachBodyListener();
        }

        this.setState({
            editActive: sameItem ? null : item.id
        });
    };

    handleEditAmpCodesClick = (item) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_AMP_CODES, {
                item,
                contextType: 'uploads'
            })
        );
    };

    handleAmpAssociatedEditCodes = (item) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_AMP_CODES, { item, contextType: 'associated' })
        );
    };

    handleDownloadClick = (item) => {
        const { dispatch } = this.props;

        dispatch(getMusicSource(item.id))
            .then((action) => {
                const json = action.resolved;
                if (item.type === 'album') {
                    for (const musicId in json) {
                        if (json[musicId]) {
                            window.open(json[musicId]);
                        }
                    }
                } else {
                    window.location.href = json.url;
                }
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem to get music ${
                            item.type
                        } url: ${error.message}`
                    })
                );
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;

        document.body.removeEventListener('click', this.handleBodyClick, false);
    }

    performUploadSearch = debounce((query) => {
        const { dispatch } = this.props;

        dispatch(
            getUserUploads({
                page: 1,
                query: query,
                incompletes: 'yes'
            })
        );
    }, 500);

    performAssociatedMusicSearch = debounce((query) => {
        const { dispatch } = this.props;

        dispatch(
            getAssociatedMusic({
                page: 1,
                query: query,
                incompletes: 'yes'
            })
        );
    }, 500);

    render() {
        return (
            <DashboardManage
                upload={this.props.upload}
                currentUser={this.props.currentUser}
                currentUserUploads={this.props.currentUserUploads}
                onUploadTypeClick={this.handleUploadTypeClick}
                onUploadSearchInput={this.handleUploadSearchInput}
                onLoadMoreUploadsClick={this.handleLoadMoreUploadsClick}
                onFormSubmit={this.handleFormSubmit}
                onDeleteClick={this.handleDeleteClick}
                onReplaceInputChange={this.handleReplaceInputChange}
                onRssSubmit={this.handleRssSubmit}
                onEditAmpCodesClick={this.handleEditAmpCodesClick}
                onEditItemClick={this.handleEditItemClick}
                editActive={this.state.editActive}
                onAmpAssociatedEditCodes={this.handleAmpAssociatedEditCodes}
                associatedMusic={this.props.associatedMusic}
                onAssociatedTypeClick={this.handleAssociatedTypeClick}
                onAssociatedSearchInput={this.handleAssociatedSearchInput}
                onLoadMoreAssociatedClick={this.handleLoadMoreAssociatedClick}
                onDownloadClick={this.handleDownloadClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        currentUser: state.currentUser,
        currentUserUploads: state.currentUserUploads,
        associatedMusic: state.monetizationAssociatedMusic
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(DashboardManageContainer, [
        (params) =>
            getUserUploads({ incompletes: 'yes', show: params.subcontext }),
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            return getAssociatedMusic();
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getExternalRssFeedUrl();
        }
    ])
);
