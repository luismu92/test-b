import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';

import { UPLOAD_STATUS, months } from 'constants/index';
import { fauxData, fauxFans, fauxInfluencers } from 'constants/stats';
import {
    yesBool,
    formatCurrency,
    parseRevenueString,
    getPercentage
} from 'utils/index';
import { getCountryName } from 'utils/stats';

import LineChart from '../components/LineChart';
import GeoChart from '../stats/GeoChart';
import StatBlock from '../stats/StatBlock';
import Table from '../components/Table';
import Dropdown from '../components/Dropdown';
import AmpEditCodesModal from '../modal/AmpEditCodesModal';
import AndroidLoader from '../loaders/AndroidLoader';
import DashboardArtistBlock from './DashboardArtistBlock';
import DashboardMusicItem from './DashboardMusicItem';
import ConfirmMessage from '../components/ConfirmMessage';
import PercentageBar from '../components/PercentageBar';

import DashboardModuleTitle from './DashboardModuleTitle';
import DashboardEmptyOverlay from './DashboardEmptyOverlay';
import DashboardEmptyState from './DashboardEmptyState';

import PlayIcon from '../icons/play';
import HeartIcon from '../../../am-shared/icons/heart';
import UserIcon from '../icons/avatar';
import ReupIcon from '../icons/retweet';
import MusicIcon from '../icons/music';
import MarkIcon from '../icons/am-mark';
import PlusIcon from 'icons/plus-alt';
// import CogsIcon from '../icons/cogwheels';
import DollarIcon from '../icons/dollar';
import BarChartIcon from '../icons/bar-chart';
import MapMarker from 'icons/map-marker';
import LinkIcon from 'icons/link-alt';
import StatsSources from '../stats/StatsSources';

export default class DashboardMain extends Component {
    static propTypes = {
        upload: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserUploads: PropTypes.object,
        monetizationSummary: PropTypes.object,
        stats: PropTypes.object,
        statsArtistPlaySource: PropTypes.object,
        statsArtistFans: PropTypes.object,
        statsArtistInfluencers: PropTypes.object,
        statsArtistGeo: PropTypes.object,
        editActive: PropTypes.number,
        onTimespanClick: PropTypes.func,
        onLabelSignup: PropTypes.func,
        onDeleteClick: PropTypes.func,
        onEditItemClick: PropTypes.func,
        selectedVal: PropTypes.string,
        snapshotCategory: PropTypes.string,
        onReplaceInputChange: PropTypes.func,
        onAmpEditCodes: PropTypes.func,
        onSnapshotBlockClick: PropTypes.func,
        onDownloadClick: PropTypes.func
    };

    /////////////////////
    // Helpers methods //
    /////////////////////

    getLoader(loading) {
        if (loading) {
            return (
                <div className="dashboard-panel__loader">
                    <AndroidLoader />
                </div>
            );
        }

        return null;
    }

    // renderRealtimePanel() {
    //     const chartOptions = {
    //         layout: {
    //             padding: {
    //                 top: 50
    //             }
    //         },
    //         scales: {
    //             yAxes: [{
    //                 display: false
    //             }]
    //         }
    //     };

    //     return (
    //         <div className="row dashboard-module u-spacing-bottom u-pos-relative">
    //             <h4 className="dashboard-title column small-24 u-spacing-bottom-em">
    //                 <span className="u-text-orange u-text-icon u-d-inline-block">
    //                     <CogsIcon />
    //                 </span>
    //                 Realtime Stats
    //             </h4>
    //             <div className="column small-24">
    //                 <div className="u-box-shadow u-pos-relative">
    //                     <div className="dashboard-live-plays">
    //                         <p className="dashboard-live-plays__number">129</p>
    //                         <p className="dashboard-live-plays__label">Live plays</p>
    //                     </div>
    //                     <LineChart
    //                         displayPulseDot
    //                         tooltipStyle={2}
    //                         labels={['12PM', '2PM', '4PM', '6PM', '8PM', '10PM', '12AM']}
    //                         datasets={[{
    //                             data: [100, 120, 150, 170, 180, 170, 160]
    //                         }]}
    //                         options={chartOptions}
    //                     />
    //                 </div>
    //             </div>
    //         </div>
    //     );
    // }

    renderSnapshot() {
        const {
            stats,
            onTimespanClick,
            onSnapshotBlockClick,
            snapshotCategory,
            selectedVal
        } = this.props;
        const { snapshot, daily } = stats;

        const {
            listener,
            follow,
            play_song,
            // play_album,
            favorite,
            playlist_song_add,
            repost,
            loading
        } = snapshot;

        const blocks = [
            {
                count: play_song,
                stat: 'play_song',
                text: 'Plays',
                Icon: PlayIcon
            },
            {
                count: favorite,
                stat: 'favorite',
                text: 'Favorites',
                Icon: HeartIcon
            },
            {
                count: playlist_song_add,
                stat: 'playlist_song_add',
                text: 'Playlist Adds',
                Icon: PlusIcon
            },
            {
                count: repost,
                stat: 'repost',
                text: 'Re-Ups',
                Icon: ReupIcon
            },
            {
                count: follow,
                stat: 'follow',
                text: 'Followers',
                Icon: MarkIcon
            },
            {
                count: listener,
                stat: 'listener',
                text: 'Unique Listeners',
                Icon: UserIcon
            }
        ].map(({ count, stat, text, Icon }, i) => {
            return (
                <div
                    className="u-spacing-bottom column small-12 half-padding medium-expand u-text-center"
                    key={`stat-${i}`}
                >
                    <StatBlock
                        text={text}
                        value={count}
                        type="number"
                        loading={loading}
                        icon={<Icon />}
                        action={stat}
                        onClick={onSnapshotBlockClick}
                        iconClass={`${stat}Icon`}
                        active={snapshotCategory === stat}
                    />
                </div>
            );
        });

        const timespanList = [
            {
                value: 'week',
                text: 'This Week'
            },
            {
                value: 'month',
                text: 'This Month'
            },
            {
                value: 'three-months',
                text: '3 Months'
            },
            {
                value: 'six-months',
                text: '6 Months'
            },
            {
                value: 'year',
                text: '12 Months'
            }
        ];

        const dropdown = (
            <Dropdown
                value={selectedVal}
                options={timespanList}
                onChange={onTimespanClick}
                className="c-dropdown--button"
                buttonClassName="button button--pill"
                menuClassName="tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
            />
        );

        const dailyLabels = daily.map((data) => {
            return data.event_date;
        });

        const categoryToKey = {
            play_song: 'play_song',
            follow: 'follow',
            listener: 'listener',
            favorite: 'favorite',
            playlist_song_add: 'playlist_song_add',
            repost: 'repost'
        };

        const dailyData = daily.map((data) => {
            const attributeData = data[categoryToKey[snapshotCategory]];
            return attributeData;
        });

        const categoryToLabel = {
            play_song: 'Plays',
            follow: 'Followers',
            listener: 'Listeners',
            favorite: 'Favorites',
            playlist_song_add: 'Playlist Adds',
            repost: 'Re-Ups'
        };

        const dataLabel = categoryToLabel[snapshotCategory];

        const chartOptions = {
            layout: {
                padding: {
                    top: 25
                }
            }
        };

        let chart;

        if (dailyData.length) {
            chart = (
                <div className="column small-24">
                    <div className="u-box-shadow u-padded half-padding">
                        {this.getLoader(loading)}
                        <LineChart
                            height={265}
                            labels={dailyLabels}
                            datasets={[
                                {
                                    data: dailyData
                                }
                            ]}
                            options={chartOptions}
                            tooltipLabel={dataLabel}
                        />
                    </div>
                </div>
            );
        }

        const popover = (
            <Fragment>
                The Creator Dashboard only reflects stats from uploads on your
                account. For any questions about discrepancies please email{' '}
                <a href="mailto:creators@audiomack.com">
                    creators@audiomack.com
                </a>
            </Fragment>
        );

        return (
            <div className="u-spacing-top u-spacing-bottom-50">
                <div className="row">
                    <div className="column small-24">
                        <DashboardModuleTitle
                            text="Account Snapshot"
                            icon={<UserIcon />}
                            children={dropdown}
                            spaceBetween
                            popoverText={popover}
                        />
                    </div>
                </div>
                <div className="row half-padding">{blocks}</div>
                <div className="row">{chart}</div>
            </div>
        );
    }

    renderMonetizationSummary() {
        const { monetizationSummary, currentUser } = this.props;

        if (!currentUser.profile.label_owner) {
            return null;
        }

        const { error, loading: loadingSummary, summary } = monetizationSummary;
        const { estimated } = summary;
        const ampSummaryYear = new Date().getFullYear();
        const ampSummaryMonth = new Date().getMonth();
        // months are zero index based
        const usingThirtySecondPlays =
            parseInt(`${ampSummaryYear}${ampSummaryMonth + 1}`, 10) >= 20185;
        let acceptTos;
        let firstFieldHeader = 'Plays';

        if (usingThirtySecondPlays) {
            firstFieldHeader = 'Monetized Plays';
        }

        let firstFieldValue = summary.total_plays;
        let FirstIcon = PlayIcon;

        // use old AMP data for anything before August 2017
        if (
            ampSummaryYear < 2017 ||
            (ampSummaryYear === 2017 && ampSummaryMonth < 7)
        ) {
            firstFieldHeader = 'Ad Opportunities';
            firstFieldValue = summary.ad_opportunities;
            FirstIcon = BarChartIcon;
        }

        let blockStyle;
        let moreButton;

        if (error && error.errorcode === 14004) {
            blockStyle = { opacity: 0.2 };
            acceptTos = (
                <div className="u-overlay">
                    <div className="row">
                        <div className="column small-24 u-text-center">
                            <ConfirmMessage
                                open
                                hideCloseButton
                                className="u-spacing-top"
                            >
                                <p>
                                    You have been approved for AMP. Please{' '}
                                    <button
                                        className="button button--link"
                                        onClick={this.props.onLabelSignup}
                                    >
                                        click here to agree to our Terms of
                                        Service
                                    </button>{' '}
                                    and start monetizing your content.
                                </p>
                            </ConfirmMessage>
                        </div>
                    </div>
                </div>
            );
        } else {
            moreButton = (
                <div className="u-spacing-top u-text-center">
                    <Link
                        to="/monetization"
                        className="button button--pill button--med"
                    >
                        See All Monetization
                    </Link>
                </div>
            );
        }

        return (
            <section className="u-spacing-bottom-50">
                <div className="row u-pos-relative u-spacing-bottom-em u-align-center">
                    <h4 className="dashboard-title column small-24 medium-12">
                        <DollarIcon className="u-text-icon u-text-orange" />
                        Monetization Summary{estimated
                            ? ' (Estimated)'
                            : ''} - {months[ampSummaryMonth]} {ampSummaryYear}
                    </h4>
                    <p className="column small-24 medium-12 u-text-right u-fw-700 u-ls-n-025">
                        <Link to="/monetization/faq">Monetization FAQ</Link>
                    </p>
                </div>

                <div className="row u-pos-relative">
                    {acceptTos}
                    <div className="column">
                        <StatBlock
                            style={blockStyle}
                            text={firstFieldHeader}
                            value={firstFieldValue}
                            type="number"
                            loading={loadingSummary}
                            error={error}
                            icon={<FirstIcon />}
                        />
                    </div>
                    <div className="column">
                        <StatBlock
                            style={blockStyle}
                            text="Revenue"
                            type="currency"
                            value={parseRevenueString(summary.total_revenue)}
                            valueFormatter={formatCurrency}
                            loading={loadingSummary}
                            error={error}
                            icon={<DollarIcon />}
                        />
                    </div>
                </div>
                {moreButton}
            </section>
        );
    }

    renderTopContent() {
        const {
            currentUserUploads,
            onEditItemClick,
            onReplaceInputChange,
            onDeleteClick,
            editActive,
            upload,
            currentUser,
            onAmpEditCodes,
            onDownloadClick
        } = this.props;
        const hasNoUploads =
            !currentUserUploads.list.length && currentUserUploads.page === 1;
        const inProgressUploads = upload.list.reduce((acc, item) => {
            if (
                item.status !== UPLOAD_STATUS.FAILED &&
                item.status !== UPLOAD_STATUS.COMPLETED
            ) {
                acc.push(item);
            }

            return acc;
        }, []);

        const ampEnabled = yesBool(currentUser.profile.video_ads);
        const showPlColumn =
            currentUserUploads.show === 'all' ||
            currentUserUploads.show === 'songs';

        const headerCells = [
            {
                value: 'Title',
                props: {
                    className: 'flex-3'
                }
            },
            {
                value: 'Artist',
                props: {
                    className: 'flex-3'
                }
            },
            'Plays',
            'Favs',
            <Fragment key="pls">
                PL's
                <span
                    className="help-prompt dashboard-table__help-prompt"
                    data-tooltip="The number of times your track has been added to a playlist"
                >
                    ?
                </span>
            </Fragment>,
            'Comments',
            ampEnabled
                ? {
                      value: 'AMP',
                      props: {
                          className: 'flex-half u-text-center'
                      }
                  }
                : null,
            {
                value: 'Actions',
                props: {
                    className: 'medium-text-center'
                }
            }
        ].filter(Boolean);

        const rows = currentUserUploads.list.map((item, i) => (
            <DashboardMusicItem
                key={i}
                musicItem={item}
                inProgressUploads={inProgressUploads}
                onReplaceInputChange={onReplaceInputChange}
                onEditItemClick={onEditItemClick}
                onDeleteClick={onDeleteClick}
                editActive={editActive}
                ampEnabled={ampEnabled}
                onEditAmpCodesClick={onAmpEditCodes}
                displayEditColumn
                showPlColumn={showPlColumn}
                onDownloadClick={onDownloadClick}
            />
        ));

        let table;
        let noUploadsMessage;
        let allStatsButton;

        if (hasNoUploads) {
            noUploadsMessage = (
                <div style={{ padding: '65px 0' }}>
                    <DashboardEmptyState
                        message="Rankings will be available when you get your first plays"
                        icon={<MusicIcon />}
                    />
                </div>
            );
        } else {
            table = (
                <Table
                    className="dashboard-table"
                    headerCells={headerCells}
                    bodyRows={rows}
                    ignoreTbody
                />
            );

            allStatsButton = (
                <div className="u-spacing-top u-text-center">
                    <Link
                        to="/dashboard/manage"
                        className="button button--pill"
                    >
                        See All Content
                    </Link>
                </div>
            );
        }

        return (
            <div className="row u-spacing-bottom-50">
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Top Performing Content"
                        icon={<MusicIcon />}
                    />
                </div>
                <div className="column small-24">
                    <div className="u-box-shadow u-padded">
                        {table}
                        {noUploadsMessage}
                        {allStatsButton}
                    </div>
                </div>
            </div>
        );
    }

    renderPlaySources() {
        const { statsArtistPlaySource } = this.props;

        const showFauxData = !statsArtistPlaySource.data.length;
        const sourcesData = showFauxData ? fauxData : statsArtistPlaySource;

        let seeMoreButton;
        if (!statsArtistPlaySource.error) {
            seeMoreButton = (
                <p className="u-text-center">
                    <Link
                        to="/dashboard/play-sources"
                        className="button button--pill button--med"
                    >
                        See All Sources
                    </Link>
                </p>
            );
        }

        let emptyOverlay;
        if (showFauxData) {
            emptyOverlay = (
                <DashboardEmptyOverlay message="Sources will be available when you get your first play" />
            );
        }

        return (
            <section className="column small-24 medium-12 half-padding">
                <DashboardModuleTitle
                    text="Play Sources"
                    suffixText="(Last 7 Days)"
                    icon={<LinkIcon style={{ transform: 'rotate(70deg)' }} />}
                />
                <div
                    className="u-box-shadow u-padded u-pos-relative"
                    style={{ minHeight: 500 }}
                >
                    {this.getLoader(statsArtistPlaySource.loading)}
                    <div className="u-pos-relative">
                        <StatsSources dataSet={sourcesData} limit={6} />
                        {emptyOverlay}
                    </div>
                    {seeMoreButton}
                </div>
            </section>
        );
    }

    renderFans() {
        const { statsArtistFans } = this.props;

        const showFauxData = !statsArtistFans.data.length;
        const fansData = showFauxData ? fauxFans : statsArtistFans;

        const formatted = fansData.data.reduce(
            (acc, source) => {
                acc.total += source.plays;
                acc.list.push(source);

                return acc;
            },
            {
                total: 0,
                list: []
            }
        );
        const blocks = fansData.data.slice(0, 3).map((artist, i) => {
            const percentage = getPercentage(artist.plays, formatted.total);

            if (!artist.info) {
                return null;
            }

            return (
                <DashboardArtistBlock
                    plays={artist.plays}
                    className={
                        i === formatted.list.length - 1
                            ? ''
                            : 'u-spacing-bottom'
                    }
                    key={i}
                    artist={artist.info}
                    percentage={percentage}
                />
            );
        });

        const seeMoreButton = (
            <p className="u-text-center u-spacing-top">
                <Link
                    to="/dashboard/fans"
                    className="button button--pill button--med"
                >
                    See All Fans
                </Link>
            </p>
        );

        let emptyOverlay;
        if (showFauxData) {
            emptyOverlay = (
                <DashboardEmptyOverlay
                    message="Top fans will be available once you start getting plays."
                    link
                />
            );
        }

        return (
            <section className="column small-24 medium-12 half-padding">
                <DashboardModuleTitle
                    text="Top Fans"
                    icon={<UserIcon />}
                    suffixText="(Last 90 days)"
                    tooltipText="Your listeners, based on play count"
                />
                <div className="u-pos-relative">
                    {this.getLoader(statsArtistFans.loading)}
                    <div className="u-pos-relative">
                        {blocks}
                        {emptyOverlay}
                    </div>
                    {seeMoreButton}
                </div>
            </section>
        );
    }

    renderInfluencers() {
        const { statsArtistInfluencers } = this.props;

        const showFauxData = !statsArtistInfluencers.data.length;
        const influencersData = showFauxData
            ? fauxInfluencers
            : statsArtistInfluencers;

        const formatted = influencersData.data.reduce(
            (acc, source) => {
                acc.total += source.followers;
                acc.list.push(source);

                return acc;
            },
            {
                total: 0,
                list: []
            }
        );
        const blocks = influencersData.data.slice(0, 3).map((artist, i) => {
            const percentage = getPercentage(artist.followers, formatted.total);

            if (!artist.info) {
                return null;
            }

            return (
                <DashboardArtistBlock
                    followers={artist.followers}
                    className={
                        i === formatted.list.length - 1
                            ? ''
                            : 'u-spacing-bottom'
                    }
                    key={i}
                    artist={artist.info}
                    percentage={percentage}
                />
            );
        });

        const seeMoreButton = (
            <p className="u-text-center u-spacing-top">
                <Link
                    to="/dashboard/influencers"
                    className="button button--pill button--med"
                >
                    See All Influencers
                </Link>
            </p>
        );

        let emptyOverlay;
        if (showFauxData) {
            emptyOverlay = (
                <DashboardEmptyOverlay
                    message="Top influencers will be available once you start getting plays."
                    link
                />
            );
        }

        return (
            <section className="column small-24 medium-12 half-padding">
                <DashboardModuleTitle
                    text="Top Influencers"
                    icon={<UserIcon />}
                    suffixText="(Last 90 days)"
                    tooltipText="Your popular listeners, with at least 50 followers"
                />
                <div className="u-pos-relative">
                    {this.getLoader(statsArtistInfluencers.loading)}
                    <div className="u-pos-relative">
                        {blocks}
                        {emptyOverlay}
                    </div>
                    {seeMoreButton}
                </div>
            </section>
        );
    }

    renderGeoMap() {
        const { statsArtistGeo } = this.props;

        const showFauxData = !statsArtistGeo.data.length;
        const geoData = showFauxData ? fauxData.data : statsArtistGeo.data;

        const topFour = Array.from(geoData)
            .sort((x, y) => {
                return y.play_song - x.play_song;
            })
            .reduce(
                (acc, stat) => {
                    acc.total += stat.play_song;

                    if (acc.stats.length > 3) {
                        return acc;
                    }

                    acc.stats.push(stat);

                    return acc;
                },
                {
                    stats: [],
                    total: 0
                }
            );

        const seeMoreButton = (
            <p className="u-text-center" style={{ marginTop: 15 }}>
                <Link
                    to="/dashboard/geography/countries"
                    className="button button--pill button--med"
                >
                    See More Geo Stats
                </Link>
            </p>
        );

        const items = topFour.stats.map((stat, i) => {
            const percentage = getPercentage(stat.play_song, topFour.total, {
                decimalsBelowOne: 2
            });

            return (
                <div
                    className="column small-24 medium-12 half-padding"
                    key={i}
                    style={{ marginBottom: 15 }}
                >
                    <PercentageBar
                        label={getCountryName(stat.geo_country)}
                        value={percentage}
                        height={6}
                        small
                    />
                </div>
            );
        });

        const chartData = topFour.stats
            .map((obj) => {
                const countryName = getCountryName(obj.geo_country);
                const percentage = getPercentage(obj.play_song, topFour.total, {
                    decimalsBelowOne: 2
                });
                if (percentage === 0) {
                    return null;
                }

                return [
                    { v: countryName, f: '' },
                    percentage,
                    `<p style="white-space: nowrap; font-family: 'Open Sans'; font-size: 1rem; color: #000; text-align: center;font-weight: bold;    line-height: 1.2;">${percentage}% <span style="color: #666; font-weight: 600;">plays</span></p><p style="white-space: nowrap; font-family: 'Open Sans'; color: #ffa200; font-size: 0.7rem; text-align: center;">${countryName}</p>`
                ];
            })
            .filter(Boolean);
        const chartDataHead = [
            'Country',
            { label: 'Percentage', id: 'percentage', type: 'number' },
            { type: 'string', role: 'tooltip', p: { html: true } }
        ];
        chartData.unshift(chartDataHead);

        const chartOptions = {
            colorAxis: {
                colors: ['#ffa200']
            },
            legend: 'none',
            tooltip: {
                isHtml: true
            }
        };

        let emptyOverlay;
        if (showFauxData) {
            emptyOverlay = (
                <DashboardEmptyOverlay message="Geos will be available when you get your first play" />
            );
        }

        return (
            <div className="column small-24 medium-12 half-padding">
                <DashboardModuleTitle
                    text="Plays by Geo"
                    suffixText="(Last 7 days)"
                    icon={<MapMarker />}
                />
                <div
                    className="u-box-shadow u-pos-relative u-padded"
                    style={{ minHeight: 500 }}
                >
                    <div className="u-pos-relative">
                        <div className="u-pos-relative">
                            {this.getLoader(statsArtistGeo.loading)}
                            <GeoChart data={chartData} options={chartOptions} />
                        </div>
                        <div
                            className="row half-padding"
                            style={{ marginTop: 4 }}
                        >
                            {items}
                        </div>
                        {emptyOverlay}
                    </div>
                    {seeMoreButton}
                </div>
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Dashboard on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                {/* this.renderRealtimePanel() */}
                {this.renderSnapshot()}
                <div className="row u-spacing-bottom-50 half-padding">
                    {this.renderPlaySources()}
                    {this.renderGeoMap()}
                </div>
                <div className="row u-spacing-bottom-50 half-padding">
                    {this.renderFans()}
                    {this.renderInfluencers()}
                </div>
                {this.renderTopContent()}
                {this.renderMonetizationSummary()}
                <AmpEditCodesModal />
            </Fragment>
        );
    }
}
