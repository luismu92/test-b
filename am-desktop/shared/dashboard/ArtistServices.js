import React from 'react';

import SongTrustService from './SongTrustService';
import MBHTService from './MBHTService';

function ArtistServices() {
    return (
        <div
            className="container"
            style={{ maxWidth: 1140, paddingLeft: 20, paddingRight: 20 }}
        >
            <div style={{ marginTop: 40 }}>
                <SongTrustService />
            </div>
            <div style={{ marginTop: 40 }}>
                <MBHTService />
            </div>
        </div>
    );
}

export default ArtistServices;
