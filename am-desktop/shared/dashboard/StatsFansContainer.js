import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import requireAuth from '../hoc/requireAuth';

import {
    getFans,
    clearList as clearFansList
} from '../redux/modules/stats/artist/fans';
import {
    getInfluencers,
    clearList as clearInfluencersList
} from '../redux/modules/stats/artist/influencers';

import { queryMonth, queryToday } from 'utils/stats';

import StatsFans from './StatsFans';

const DEFAULT_CONTEXT = 'fans';
const FETCH_LIMIT = 10;

function makeContextRequest(params, currentUser) {
    const context = params.context || DEFAULT_CONTEXT;
    const artistId = currentUser.profile.id;

    let fn = () => null; //eslint-disable-line

    switch (context) {
        case 'fans':
            fn = () =>
                getFans(artistId, {
                    startDate: queryMonth(),
                    endDate: queryToday(),
                    limit: FETCH_LIMIT
                });
            break;

        case 'influencers':
            fn = () =>
                getInfluencers(artistId, {
                    startDate: queryMonth(),
                    endDate: queryToday(),
                    limit: FETCH_LIMIT
                });
            break;

        default:
            console.warn('No fn implemented for context:', context);
            break;
    }

    return fn();
}
class StatsFansContainer extends Component {
    static propTypes = {
        statsArtistFans: PropTypes.object,
        statsArtistInfluencers: PropTypes.object,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        match: PropTypes.object
    };

    constructor(props) {
        super(props);
        const { match } = this.props;

        this.state = {
            activeContext: match.params.context
        };
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        this.props.dispatch(clearFansList());
        this.props.dispatch(clearInfluencersList());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextButtonClick = (e) => {
        const button = e.currentTarget;
        const context = button.getAttribute('data-context');

        this.setState({
            activeContext: context
        });

        this.fetch(context, 1);
    };

    handleLoadMoreStatsClick = (e) => {
        const button = e.currentTarget;
        const context = button.getAttribute('data-action');

        switch (context) {
            case 'fans': {
                const currentPage = this.props.statsArtistFans.page || 1;
                const nextPage = currentPage + 1;

                this.fetch(context, nextPage);
                break;
            }

            case 'influencers': {
                const currentPage = this.props.statsArtistInfluencers.page || 1;
                const nextPage = currentPage + 1;

                this.fetch(context, nextPage);
                break;
            }

            default:
                break;
        }
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getDataFromContext(context) {
        const { statsArtistFans, statsArtistInfluencers } = this.props;

        if (context === 'influencers') {
            return statsArtistInfluencers;
        }

        return statsArtistFans;
    }

    refetchIfNecessary(currentProps, nextProps) {
        const { currentUser, dispatch } = this.props;
        const { params } = currentProps.match;
        const { params: nextParams } = nextProps.match;

        const changedPage = params.context !== nextParams.context;
        const hasData =
            currentProps.statsArtistFans.data.length &&
            currentProps.statsArtistInfluencers.data.length;

        if (changedPage && !hasData) {
            dispatch(makeContextRequest(nextParams, currentUser));

            this.setState({
                activeContext: nextParams.context
            });
        }
    }

    fetch(context, page) {
        const { dispatch, currentUser } = this.props;

        switch (context) {
            case 'fans': {
                dispatch(
                    getFans(currentUser.profile.id, {
                        limit: FETCH_LIMIT,
                        page: page
                    })
                );
                break;
            }

            case 'influencers': {
                dispatch(
                    getInfluencers(currentUser.profile.id, {
                        limit: FETCH_LIMIT,
                        page: page
                    })
                );
                break;
            }

            default:
                break;
        }
    }

    render() {
        const { match } = this.props;

        return (
            <StatsFans
                dataSet={this.getDataFromContext(match.params.context)}
                onContextButtonClick={this.handleContextButtonClick}
                activeContext={this.state.activeContext}
                onLoadMoreStatsClick={this.handleLoadMoreStatsClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        statsArtistFans: state.statsArtistFans,
        statsArtistInfluencers: state.statsArtistInfluencers
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(StatsFansContainer, [
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return makeContextRequest(params, currentUser);
        }
    ])
);
