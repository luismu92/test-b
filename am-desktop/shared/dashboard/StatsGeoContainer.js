import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import requireAuth from '../hoc/requireAuth';

import {
    getCountryStats,
    nextPage as nextCountriesPage,
    clearList as clearCountriesList
} from '../redux/modules/stats/artist/countries';
import {
    getCityStats,
    nextPage as nextCitiesPage,
    clearList as clearCitiesList
} from '../redux/modules/stats/artist/cities';

import { queryMonth, queryToday } from 'utils/stats';

import StatsGeo from './StatsGeo';

const DEFAULT_CONTEXT = 'countries';

function makeContextRequest(params, currentUser) {
    const context = params.subcontext || DEFAULT_CONTEXT;
    const artistId = currentUser.profile.id;

    let fn = () => null; //eslint-disable-line

    switch (context) {
        case 'countries':
            fn = () =>
                getCountryStats(artistId, {
                    startDate: queryMonth(),
                    endDate: queryToday(),
                    limit: 10
                });
            break;
        case 'cities':
            fn = () =>
                getCityStats(artistId, {
                    startDate: queryMonth(),
                    endDate: queryToday(),
                    limit: 10
                });
            break;
        default:
            console.warn('No fn implemented for context:', context);
            break;
    }

    return fn();
}
class StatsGeoContainer extends Component {
    static propTypes = {
        statsArtistCountries: PropTypes.object,
        statsArtistCities: PropTypes.object,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        match: PropTypes.object
    };

    constructor(props) {
        super(props);
        const { match } = this.props;

        this.state = {
            activeContext: match.params.subcontext
        };
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(clearCountriesList());
        dispatch(clearCitiesList());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextButtonClick = (e) => {
        const button = e.currentTarget;
        const context = button.getAttribute('data-context');

        this.setState({
            activeContext: context
        });
    };

    handleLoadMoreStatsClick = (e) => {
        const { dispatch, currentUser } = this.props;

        const button = e.currentTarget;
        const action = button.getAttribute('data-action');
        const page = parseInt(button.getAttribute('data-page'), 10) || 1;
        const limit = 10;

        switch (action) {
            case 'countries':
                dispatch(
                    getCountryStats(currentUser.profile.id, {
                        startDate: queryMonth(),
                        endDate: queryToday(),
                        limit: limit,
                        page: page + 1
                    })
                );
                dispatch(nextCountriesPage());
                break;

            case 'cities':
                dispatch(
                    getCityStats(currentUser.profile.id, {
                        startDate: queryMonth(),
                        endDate: queryToday(),
                        limit: limit,
                        page: page + 1
                    })
                );
                dispatch(nextCitiesPage());
                break;

            default:
                break;
        }
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getDataFromContext(context) {
        const { statsArtistCities, statsArtistCountries } = this.props;

        if (context === 'cities') {
            return statsArtistCities;
        }

        return statsArtistCountries;
    }

    refetchIfNecessary(currentProps, nextProps) {
        const { currentUser, dispatch } = this.props;
        const { params } = currentProps.match;
        const { params: nextParams } = nextProps.match;

        const changedPage = params.subcontext !== nextParams.subcontext;
        const hasData =
            currentProps.statsArtistCountries.data.length &&
            currentProps.statsArtistCities.data.length;

        if (changedPage && !hasData) {
            dispatch(makeContextRequest(nextParams, currentUser));

            this.setState({
                activeContext: nextParams.subcontext
            });
        }
    }

    render() {
        const { match } = this.props;

        return (
            <StatsGeo
                dataSet={this.getDataFromContext(match.params.subcontext)}
                onContextButtonClick={this.handleContextButtonClick}
                activeContext={this.state.activeContext}
                onLoadMoreStatsClick={this.handleLoadMoreStatsClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        statsArtistGeo: state.statsArtistGeo,
        statsArtistCountries: state.statsArtistCountries,
        statsArtistCities: state.statsArtistCities
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(StatsGeoContainer, [
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return makeContextRequest(params, currentUser);
        }
    ])
);
