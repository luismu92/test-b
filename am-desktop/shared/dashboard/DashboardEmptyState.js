import React from 'react';
import PropTypes from 'prop-types';

import Button from '../buttons/Button';

import styles from './DashboardEmptyState.module.scss';

export default function DashboardEmptyState({ message, icon }) {
    let stateIcon;

    if (icon) {
        stateIcon = <div className={styles.icon}>{icon}</div>;
    }

    const buttonStyle = {
        boxShadow: '0 6px 8px 0 rgba(0, 0, 0, 0.06)'
    };

    return (
        <div className={styles.container}>
            {stateIcon}
            <p className={styles.message}>
                <strong>{message}</strong>
            </p>
            <Button
                width={200}
                height={48}
                href="/upload"
                text="Upload Now"
                style={buttonStyle}
            />
        </div>
    );
}

DashboardEmptyState.propTypes = {
    message: PropTypes.string,
    icon: PropTypes.object
};
