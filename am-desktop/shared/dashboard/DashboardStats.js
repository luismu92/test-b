import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { UPLOAD_STATUS, months } from 'constants/index';
import { humanizeNumber, ucfirst, yesBool } from 'utils/index';

import StatBlock from '../stats/StatBlock';
import LineChart from '../components/LineChart';
import ConfirmMessage from '../components/ConfirmMessage';
import Dropdown from '../components/Dropdown';
import Select from '../components/Select';
import Table from '../components/Table';
// import AmpEditCodesModal from '../modal/AmpEditCodesModal';
import AndroidLoader from '../loaders/AndroidLoader';
import DashboardMusicItem from '../dashboard/DashboardMusicItem';

import PlayIcon from '../icons/play';
import HeartIcon from '../../../am-shared/icons/heart';
import UserIcon from '../icons/avatar';
import ReupIcon from '../icons/retweet';
import MusicIcon from '../icons/music';
import MarkIcon from '../icons/am-mark';
import PlusIcon from '../icons/plus';

export default class DashboardStats extends Component {
    static propTypes = {
        upload: PropTypes.object,
        monetizationSummary: PropTypes.object,
        monetizationPaymentInfo: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserUploads: PropTypes.object,
        stats: PropTypes.object,
        history: PropTypes.object,
        editActive: PropTypes.number,
        onFormSubmit: PropTypes.func,
        onResendEmailClick: PropTypes.func,
        onLoadMoreUploadsClick: PropTypes.func,
        onLoadMoreAssociatedClick: PropTypes.func,
        onUploadTypeClick: PropTypes.func,
        onAssociatedTypeClick: PropTypes.func,
        onUploadSearchInput: PropTypes.func,
        onAssociatedSearchInput: PropTypes.func,
        onTimespanClick: PropTypes.func,
        onDeleteClick: PropTypes.func,
        onAmpSelectChange: PropTypes.func,
        ampSummaryMonth: PropTypes.number,
        ampSummaryYear: PropTypes.number,
        onEditItemClick: PropTypes.func,
        onLabelSignup: PropTypes.func,
        selectedVal: PropTypes.string,
        paymentMessageOpen: PropTypes.bool,
        onDeleteConfirm: PropTypes.func,
        onReplaceInputChange: PropTypes.func,
        onPaymentCloseClick: PropTypes.func,
        onShowClick: PropTypes.func,
        onAmpEditCodes: PropTypes.func,
        onAmpAssociatedEditCodes: PropTypes.func,
        associatedMusic: PropTypes.object
    };

    /////////////////////
    // Helpers methods //
    /////////////////////

    getLoader(loading) {
        if (loading) {
            return (
                <div className="dashboard-panel__loader">
                    <AndroidLoader />
                </div>
            );
        }

        return null;
    }

    getSelectOptions(currentUser, ampYear) {
        const startYear = new Date(
            currentUser.profile.created * 1000
        ).getFullYear();
        const currentYear = new Date().getFullYear();
        const years = [];

        for (let i = startYear; i <= currentYear; i++) {
            years.push(i);
        }

        const yearOptions = years.map((year) => {
            return {
                text: year,
                value: year
            };
        });

        const currentMonth = new Date().getMonth();
        const monthOptions = months
            .map((month, i) => {
                if (currentYear === parseInt(ampYear, 10) && i > currentMonth) {
                    return null;
                }

                return {
                    text: month,
                    value: i
                };
            })
            .filter(Boolean);

        return {
            monthOptions,
            yearOptions
        };
    }

    renderMonetizationSummary() {
        const {
            monetizationSummary,
            onAmpSelectChange,
            ampSummaryMonth,
            ampSummaryYear,
            currentUser,
            monetizationPaymentInfo,
            paymentMessageOpen,
            onPaymentCloseClick
        } = this.props;
        const { error, loading, summary } = monetizationSummary;
        let showMonetizationLink = true;
        let content;
        let acceptTos;
        let selects;

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        if (error) {
            switch (error.errorcode) {
                case 14004: {
                    acceptTos = (
                        <p>
                            You have been approved for monetization. Please{' '}
                            <button
                                className="button button--link"
                                onClick={this.props.onLabelSignup}
                            >
                                enter your account details here
                            </button>{' '}
                            to complete the process.
                        </p>
                    );
                    showMonetizationLink = false;
                    break;
                }

                default: {
                    content = <p>There was an error fetching your summary.</p>;
                    break;
                }
            }
        } else {
            const { monthOptions, yearOptions } = this.getSelectOptions(
                currentUser,
                ampSummaryYear
            );
            let firstFieldHeader = 'Plays';
            let firstFieldValue = humanizeNumber(summary.total_plays);

            // use old AMP data for anything before August 2017
            if (
                ampSummaryYear < 2017 ||
                (ampSummaryYear === 2017 && ampSummaryMonth < 7)
            ) {
                firstFieldHeader = 'Ad Opportunities';
                firstFieldValue = summary.ad_opportunities;
            }

            selects = [
                <Select
                    name="month"
                    key="month"
                    value={`${ampSummaryMonth}`}
                    onChange={onAmpSelectChange}
                    options={monthOptions}
                    selectProps={{
                        'data-key': 'ampSummaryMonth',
                        'data-type': 'summary'
                    }}
                />,
                <Select
                    name="year"
                    key="year"
                    value={`${ampSummaryYear}`}
                    onChange={onAmpSelectChange}
                    options={yearOptions}
                    selectProps={{
                        'data-key': 'ampSummaryYear',
                        'data-type': 'summary'
                    }}
                />
            ];

            content = (
                <table className="dashboard-table">
                    <thead>
                        <tr>
                            <td>{firstFieldHeader}</td>
                            <td>Revenue</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{firstFieldValue}</td>
                            <td>{summary.total_revenue}</td>
                        </tr>
                    </tbody>
                </table>
            );
        }

        let monetizationLink;

        if (showMonetizationLink) {
            monetizationLink = (
                <p className="u-text-center u-spacing-top">
                    <Link
                        to={`/monetization/${ampSummaryYear}/${ampSummaryMonth +
                            1}`}
                    >
                        View Full Report
                    </Link>
                </p>
            );
        }

        let paymentAlert;

        if (
            !monetizationPaymentInfo.info.payable &&
            !monetizationPaymentInfo.loading
        ) {
            paymentAlert = (
                <ConfirmMessage
                    open={paymentMessageOpen}
                    onClose={onPaymentCloseClick}
                    className="dashboard-panel__confirm-message"
                >
                    <p>
                        Please enter your{' '}
                        <Link to="/monetization/info">
                            payment details here
                        </Link>
                        .
                    </p>
                </ConfirmMessage>
            );
        }

        const innerClass = classnames('dashboard-panel__inner', {
            'dashboard-panel__inner--has-alert':
                !monetizationPaymentInfo.info.payable && paymentMessageOpen
        });

        return (
            <div className="dashboard-panel dashboard-panel--charts u-box-shadow">
                {this.getLoader(loading)}
                <div className={innerClass}>
                    {paymentAlert}
                    <div className="dashboard-panel__inner-top">
                        <div>
                            <h2 className="dashboard-panel__title">
                                AMP Monthly Summary
                            </h2>
                        </div>
                        {selects}
                    </div>
                    {acceptTos}
                    {content}
                    {monetizationLink}
                </div>
            </div>
        );
    }

    renderChartPanel() {
        const { overall } = this.props.stats;
        const {
            playCount,
            favoriteCount,
            followerCount,
            reupCount,
            loading
        } = overall;
        const showList = [
            {
                count: playCount,
                show: 'play',
                Icon: PlayIcon,
                text: 'Plays'
            },
            {
                count: favoriteCount,
                show: 'favorite',
                Icon: HeartIcon,
                text: 'Favorites'
            },
            {
                count: followerCount,
                show: 'follower',
                Icon: UserIcon,
                text: 'Followers'
            },
            {
                count: reupCount,
                show: 'reup',
                Icon: ReupIcon,
                text: 'Re-ups'
            }
        ].map(({ count, show, Icon, text }, i) => {
            const klass = classnames(
                'panel-tabs__tab panel-tabs__tab--show u-d-flex',
                {
                    'panel-tabs__tab--active': overall.show === show
                }
            );

            return (
                <li
                    className={klass}
                    key={i}
                    data-count={humanizeNumber(count)}
                >
                    <button onClick={this.props.onShowClick} data-show={show}>
                        <span className="panel-tabs__tab-icon">
                            <Icon />
                        </span>
                        {text}
                    </button>
                </li>
            );
        });
        const timespanList = [
            {
                timespan: 'week',
                text: 'This Week'
            },
            {
                timespan: 'month',
                text: 'This Month'
            },
            {
                timespan: 'three-months',
                text: '3 Months'
            }
        ].map(({ timespan, text }, i) => {
            const klass = classnames('dashboard-panel-context-switcher', {
                'dashboard-panel-context-switcher--active':
                    overall.timespan === timespan
            });

            return (
                <li className={klass} key={i}>
                    <button
                        onClick={this.props.onTimespanClick}
                        data-timespan={timespan}
                    >
                        {text}
                    </button>
                </li>
            );
        });

        const lineChartProps = {
            data: []
        };

        const jsonData = this.props.stats.overall;
        const show = jsonData.show;
        const propertyName = `${show}Stats`;

        if (jsonData[propertyName]) {
            lineChartProps.data.push(['Day', `${ucfirst(show)}s`]);

            Object.keys(jsonData[propertyName]).forEach((property) => {
                // transform date
                const dateSplit = property.split('-');

                lineChartProps.data.push([
                    `${dateSplit[1]}/${dateSplit[2]}`,
                    jsonData[propertyName][property]
                ]);
            });
        }

        return (
            <div className="dashboard-panel dashboard-panel--charts u-box-shadow">
                {this.getLoader(loading)}
                <div className="dashboard-panel__inner dashboard-panel__inner--charts">
                    <div className="dashboard-panel__inner-top">
                        <div>
                            <h2 className="dashboard-panel__title">
                                Overall Stats
                            </h2>
                            <ul className="dashboard-panel-context-switchers u-list-reset u-inline-list">
                                {timespanList}
                            </ul>
                        </div>
                    </div>
                    <div className="dashboard-panel__inner-main">
                        <LineChart {...lineChartProps} />
                    </div>
                </div>
                <ul className="panel-tabs u-reset-list">{showList}</ul>
            </div>
        );
    }

    renderStatBlocks() {
        const { overall } = this.props.stats;
        const {
            playCount,
            favoriteCount,
            followerCount,
            reupCount,
            loading
        } = overall;

        const statBlocks = [
            {
                count: followerCount,
                stat: 'followers',
                text: 'Followers',
                Icon: MarkIcon
            },
            {
                count: playCount,
                stat: 'plays',
                text: 'Plays',
                Icon: PlayIcon
            },
            {
                count: favoriteCount,
                stat: 'favorites',
                text: 'Favorites',
                Icon: HeartIcon
            },
            {
                count: reupCount,
                stat: 'reup',
                text: 'Re-Ups',
                Icon: ReupIcon
            },
            {
                count: '888',
                stat: 'playlist-adds',
                text: 'Playlist Adds',
                Icon: PlusIcon
            }
        ].map(({ count, stat, text, Icon }, i) => {
            const klass = classnames(
                `stat-block stat-block--${stat} column u-text-center`
            );
            const iconClass = `stat-block__icon stat-block__icon--${stat}`;

            return (
                <StatBlock
                    key={i}
                    className={klass}
                    text={text}
                    value={count}
                    type="number"
                    loading={loading}
                    icon={<Icon className={iconClass} />}
                />
            );
        });

        const timespanList = [
            {
                value: 'week',
                text: 'This Week'
            },
            {
                value: 'month',
                text: 'This Month'
            },
            {
                value: 'three-months',
                text: '3 Months'
            }
        ];

        return (
            <div className="dashboard-module dashboard-module--stat-blocks">
                <div className="dashboard-module__top">
                    <h3 className="dashboard-module__title">
                        <span className="u-text-orange u-text-icon u-d-inline-block">
                            <UserIcon />
                        </span>
                        Account Snapshot
                    </h3>
                    <div className="dashboard-module__filter">
                        <Dropdown options={timespanList} />
                    </div>
                </div>
                <div className="dashboard-stat-blocks row small-collapse">
                    <div className="column small-24">
                        <div className="row">{statBlocks}</div>
                    </div>
                </div>
            </div>
        );
    }

    renderTopContent() {
        const {
            currentUserUploads,
            onLoadMoreUploadsClick,
            onEditItemClick,
            onReplaceInputChange,
            onDeleteClick,
            editActive,
            upload,
            currentUser,
            onAmpEditCodes
        } = this.props;
        const hasNoUploads =
            !currentUserUploads.list.length && currentUserUploads.page === 1;
        let loadMoreButton;

        if (!currentUserUploads.onLastPage && currentUserUploads.list.length) {
            loadMoreButton = (
                <div className="u-spacing-top u-text-center">
                    <button
                        className="button button--pill"
                        onClick={onLoadMoreUploadsClick}
                    >
                        Load more Uploads +
                    </button>
                </div>
            );
        }

        const inProgressUploads = upload.list.reduce((acc, item) => {
            if (
                item.status !== UPLOAD_STATUS.FAILED &&
                item.status !== UPLOAD_STATUS.COMPLETED
            ) {
                acc.push(item);
            }

            return acc;
        }, []);

        const ampEnabled = yesBool(currentUser.profile.video_ads);
        const headerCells = [
            {
                content: 'Title',
                props: {
                    className: 'flex-3'
                }
            },
            {
                content: 'Artist',
                props: {
                    className: 'flex-3'
                }
            },
            'Status',
            'Plays',
            'Favs',
            <th key="pls">
                PL's
                <span
                    className="help-prompt dashboard-table__help-prompt"
                    data-tooltip="The number of times your track has been added to a playlist"
                >
                    ?
                </span>
            </th>,
            'Comments',
            ampEnabled
                ? {
                      value: 'AMP',
                      props: {
                          className: 'flex-half u-text-center'
                      }
                  }
                : null,
            {
                content: 'Actions',
                props: {
                    className: 'medium-text-center'
                }
            }
        ].filter(Boolean);

        const rows = currentUserUploads.list.map((item, i) => (
            <DashboardMusicItem
                key={i}
                musicItem={item}
                inProgressUploads={inProgressUploads}
                onReplaceInputChange={onReplaceInputChange}
                onEditItemClick={onEditItemClick}
                onDeleteClick={onDeleteClick}
                editActive={editActive}
                ampEnabled={ampEnabled}
                onEditAmpCodesClick={onAmpEditCodes}
                displayEditColumn
            />
        ));

        let table;
        let noUploadsMessage;

        if (hasNoUploads) {
            if (
                currentUserUploads.query.trim() === '' &&
                currentUserUploads.show === 'all'
            ) {
                noUploadsMessage = <p>You have no uploads yet.</p>;
            } else {
                noUploadsMessage = <p>No uploads matched your search query.</p>;
            }
        } else {
            table = (
                <div className="dashboard-panel__inner-main">
                    <Table
                        className="dashboard-table"
                        headerCells={headerCells}
                        bodyRows={rows}
                        ignoreTbody
                    />
                    {loadMoreButton}
                </div>
            );
        }

        return (
            <div className="dashboard-module dashboard-module--top-content">
                <div className="dashboard-module__top">
                    <h3 className="dashboard-module__title">
                        <span className="u-text-orange u-text-icon u-d-inline-block">
                            <MusicIcon />
                        </span>
                        Top Performing Content
                    </h3>
                </div>
                <div className="dashboard-table-wrap">
                    {table}
                    {noUploadsMessage}
                </div>
            </div>
        );
    }

    renderAssociatedTracksPanel() {
        const {
            onLoadMoreAssociatedClick,
            onAssociatedTypeClick,
            onAssociatedSearchInput,
            onEditItemClick,
            onReplaceInputChange,
            onDeleteClick,
            editActive,
            upload,
            currentUser,
            onAmpAssociatedEditCodes,
            associatedMusic
        } = this.props;

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        // @todo refactor
        const hasNoUploads =
            !associatedMusic.list.length && associatedMusic.page === 1;
        let loadMoreButton;

        if (!associatedMusic.onLastPage && associatedMusic.list.length) {
            loadMoreButton = (
                <div className="u-spacing-top u-text-center">
                    <button
                        className="button button--pill"
                        onClick={onLoadMoreAssociatedClick}
                    >
                        Load more Associated Music +
                    </button>
                </div>
            );
        }

        const inProgressUploads = upload.list.reduce((acc, item) => {
            if (
                item.status !== UPLOAD_STATUS.FAILED &&
                item.status !== UPLOAD_STATUS.COMPLETED
            ) {
                acc.push(item);
            }

            return acc;
        }, []);

        const ampEnabled =
            typeof currentUser.profile.video_ads !== 'undefined' &&
            currentUser.profile.video_ads === 'yes';
        const rows = associatedMusic.list.map((item, i) => (
            <DashboardMusicItem
                key={i}
                musicItem={item}
                inProgressUploads={inProgressUploads}
                onReplaceInputChange={onReplaceInputChange}
                onEditItemClick={onEditItemClick}
                onDeleteClick={onDeleteClick}
                editActive={editActive}
                ampEnabled={ampEnabled}
                onEditAmpCodesClick={onAmpAssociatedEditCodes}
                displayEditColumn
            />
        ));

        const contextButtons = [
            { show: 'all', text: 'All' },
            { show: 'songs', text: 'Songs' },
            { show: 'albums', text: 'Albums' }
        ].map(({ show, text }, i) => {
            const klass = classnames('dashboard-panel-context-switcher', {
                'dashboard-panel-context-switcher--active':
                    show === associatedMusic.show
            });

            return (
                <li className={klass} key={i}>
                    <button onClick={onAssociatedTypeClick} data-show={show}>
                        {text}
                    </button>
                </li>
            );
        });

        const contextSwitcher = (
            <ul className="dashboard-panel-context-switchers u-list-reset u-inline-list">
                {contextButtons}
            </ul>
        );

        let table;
        let noUploadsMessage;
        const searchForm = (
            <div>
                <form onSubmit={this.props.onFormSubmit}>
                    <input
                        type="text"
                        className="dashboard-panel__search"
                        placeholder="Search your associated music..."
                        onChange={onAssociatedSearchInput}
                        value={associatedMusic.query || ''}
                    />
                </form>
            </div>
        );

        if (hasNoUploads) {
            if (
                associatedMusic.query.trim() === '' &&
                associatedMusic.show === 'all'
            ) {
                noUploadsMessage = <p>You have no associated music yet.</p>;
            } else {
                noUploadsMessage = <p>No music matched your search query.</p>;
            }
        } else {
            const ampEnabledCell = ampEnabled ? (
                <td className="amp-cell">AMP</td>
            ) : null;

            table = (
                <div className="dashboard-panel__inner-main">
                    <table className="dashboard-table">
                        <thead>
                            <tr>
                                <td className="artist-cell">Artist</td>
                                <td className="title-cell">Title</td>
                                <td className="status-cell">Status</td>
                                {ampEnabledCell}
                                <td>Plays</td>
                                <td>Favs</td>
                                <td>Downloads</td>
                                <td className="u-text-center">Actions</td>
                            </tr>
                        </thead>
                        {rows}
                    </table>
                    {loadMoreButton}
                </div>
            );
        }

        return (
            <div className="dashboard-panel dashboard-panel--uploads u-box-shadow">
                {this.getLoader(associatedMusic.loading)}
                <div className="dashboard-panel__inner">
                    <div className="dashboard-panel__inner-top">
                        <div>
                            <h2 className="dashboard-panel__title">
                                Your Associated Music
                            </h2>
                            {contextSwitcher}
                        </div>
                        {searchForm}
                    </div>
                    {table}
                    {noUploadsMessage}
                </div>
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                Stats page
                {/* this.renderStatBlocks() */}
                {/* this.renderTopContent() */}
                {/* this.renderChartPanel() */}
                {/* this.renderMonetizationSummary() */}
                {/*
                <div className="dashboard__upload-buttons u-text-center">
                    <Link to="/upload/songs" className="button button--pill dashboard__upload-button">
                        <span className="dashboard__upload-button-icon button__icon dashboard__upload-button-icon--song">
                            <MusicIcon />
                        </span>
                        Upload a Song
                    </Link>
                    <Link to="/upload/albums" className="button button--pill dashboard__upload-button">
                        <span className="dashboard__upload-button-icon button__icon button__icon--album">
                            <AlbumIcon />
                        </span>
                        Upload an Album
                    </Link>
                </div>
                <a name="uploads" />
                {this.renderUploadsPanel()}
                */}
                {/* this.renderAssociatedTracksPanel() */}
                {/* <AmpEditCodesModal /> */}
            </Fragment>
        );
    }
}
