import React, { Fragment } from 'react';

import styles from './SongTrustService.module.scss';

export default function SongTrustService() {
    return (
        <Fragment>
            <div className={styles.main}>
                <div className={styles.mainContent}>
                    <img
                        src="/static/images/desktop/songtrust-logo.png"
                        srcSet="/static/images/desktop/songtrust-logo@2x.png 2x"
                        alt="SongTrust Logo"
                        style={{ marginBottom: 18 }}
                    />
                    <h2 className={styles.heading}>
                        As a global publishing administrator, Songtrust helps
                        you save time and earn more by automatically collecting
                        the royalties you’re owed.{' '}
                    </h2>
                    <p className={styles.copy}>
                        Songwriters, artists, managers, labels, and publishers
                        use our platform to simplify the administration of music
                        publishing assets, performing rights, and digital
                        licensing.
                    </p>
                </div>
                <div className={styles.image}>
                    <img
                        src="/static/images/desktop/songtrust-bg.jpg"
                        srcSet="/static/images/desktop/songtrust-bg@2x.jpg 2x"
                        alt="DJ Mixing"
                    />
                </div>
            </div>
            <div className={styles.secondary}>
                <img
                    className={styles.book}
                    src="/static/images/desktop/songtrust-book.png"
                    srcSet="/static/images/desktop/songtrust-book@2x.png 2x"
                    alt="Modern guide to music publishing icon"
                />
                <h3 className={styles.secondaryHeading}>
                    Download the Modern Guide to Music Publishing, a free primer
                    on music publishing fundamentals.
                </h3>
                <a
                    className={`${
                        styles.button
                    } button button--pill button--med`}
                    href="https://www.songtrust.com/the-modern-guide-to-music-publishing#Audiomack"
                    target="_blank"
                    rel="nofollow noopener"
                >
                    Download
                </a>
                <p>
                    <a
                        href="https://app.songtrust.com/signup/?hsCtaTracking=74eaa466-9b4f-4233-ad7a-1a7c1dfde509%7C5fdd52b4-6327-4c29-9311-eb9ba31efe42"
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        Sign up for Songtrust here{' '}
                    </a>
                    to start collecting what you’re owed, and be sure to use our
                    code <strong>AUDIOMACK20</strong> for a 20% discount off the
                    registration fee – exclusively for Audiomack users.
                </p>
            </div>
        </Fragment>
    );
}
