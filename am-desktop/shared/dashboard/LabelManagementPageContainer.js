import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { debounce } from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';

import requireAuth from '../hoc/requireAuth';
import {
    getAssociatedArtists,
    setAssociatedQuery,
    nextAssociatedPage
} from '../redux/modules/monetization/associatedArtists';
import { loginAsUser } from '../redux/modules/admin/index';
import {
    showModal,
    MODAL_TYPE_CREATE_NEW_ARTIST,
    MODAL_TYPE_SEND_CLAIM_PROFILE_EMAIL
} from '../redux/modules/modal';
import LabelManagementPage from './LabelManagementPage';

class LabelManagementPageContainer extends React.Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        associatedArtists: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        if (
            prevProps.associatedArtists.query &&
            prevProps.associatedArtists.query.trim() !==
                this.props.associatedArtists.query.trim()
        ) {
            this.performAssociatedArtistsSearch(
                this.props.associatedArtists.query
            );
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSearchInput = (e) => {
        const { dispatch } = this.props;
        const val = e.currentTarget.value;

        dispatch(setAssociatedQuery(val));
    };

    handleArtistActionClick = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const artistId = button.getAttribute('data-id');
        const url = button.getAttribute('data-url');

        dispatch(loginAsUser(artistId))
            .then(() => {
                window.location.href = url;
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleSendClaimEmail = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const artistId = parseInt(button.getAttribute('data-id'), 10);
        const artistName = button.getAttribute('data-name');

        dispatch(
            showModal(MODAL_TYPE_SEND_CLAIM_PROFILE_EMAIL, {
                artistId,
                artistName
            })
        );
    };

    handleCreateNewArtistClick = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_CREATE_NEW_ARTIST));
    };

    handleGetMoreArtists = () => {
        const { dispatch } = this.props;

        dispatch(nextAssociatedPage());
        dispatch(getAssociatedArtists());
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    performAssociatedArtistsSearch = debounce((query) => {
        const { dispatch } = this.props;

        dispatch(
            getAssociatedArtists({
                page: 1,
                query: query
            })
        );
    }, 500);

    render() {
        return (
            <LabelManagementPage
                onSearchInput={this.handleSearchInput}
                onFormSubmit={this.handleFormSubmit}
                onArtistActionClick={this.handleArtistActionClick}
                onSendClaimEmailClick={this.handleSendClaimEmail}
                onCreateNewArtistClick={this.handleCreateNewArtistClick}
                currentUser={this.props.currentUser}
                associatedArtists={this.props.associatedArtists}
                onGetMoreArtists={this.handleGetMoreArtists}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        associatedArtists: state.monetizationAssociatedArtists
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return (
                    currentUser.isLoggedIn && currentUser.profile.label_manage
                );
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(
    connectDataFetchers(LabelManagementPageContainer, [
        () => getAssociatedArtists()
    ])
);
