import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import AndroidLoader from '../loaders/AndroidLoader';
import Avatar from '../components/Avatar';

import CommentIcon from 'icons/comment';
import FlagIcon from 'icons/flag';
import classnames from 'classnames';
import SortIcon from 'icons/sort';

import BodyClickListener from '../components/BodyClickListener';

export default class Comments extends Component {
    static propTypes = {
        total: PropTypes.number,
        count: PropTypes.number,
        comment: PropTypes.object,
        onCommentLoadMore: PropTypes.func,
        onCommentAction: PropTypes.func,
        onCommentSortClick: PropTypes.func,
        onCommentSort: PropTypes.func,
        sortTooltipActive: PropTypes.bool,
        activeCommentContext: PropTypes.string
    };

    renderLoader(loading) {
        if (!loading) {
            return null;
        }

        return (
            <div className="dashboard-panel__loader">
                <AndroidLoader />
            </div>
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p>Error: {String(error)}</p>;
    }

    renderLoadMore(nextPage) {
        const { onCommentLoadMore } = this.props;

        if (nextPage === null) {
            return null;
        }

        return (
            <button
                className="button button--med u-fs-14 u-ls-n-05"
                onClick={onCommentLoadMore}
            >
                <CommentIcon className="button__icon" />
                Load more comments
            </button>
        );
    }

    renderCommentItem(item, index) {
        if (typeof item.uuid === 'undefined') {
            return null;
        }

        const { uuid, content, deleted, artist, user_id } = item; // eslint-disable-line

        if (!artist || artist.comment_banned || deleted) {
            return null;
        }

        const postDatetime = moment.unix(item.created_at).fromNow();
        const reportDateTime = item.report_date
            ? `( flagged ${moment.unix(item.report_date).fromNow()})`
            : '';

        const artistLink = (
            <a target="_blank" href={`/dashboard/comment-user/user/${user_id}`}>
                {artist.name}
            </a>
        ); // eslint-disable-line
        const displayContent = content;
        const artistImage = artist.image_base || artist.image;
        const userAndTimeStamp = (
            <span>
                {artistLink} - {postDatetime} {reportDateTime}
            </span>
        );

        if (!content) {
            return null;
        }

        const contentBlock = (
            <div
                className="comment__content u-fw-600 u-ls-n-06"
                dangerouslySetInnerHTML={{ __html: displayContent }}
            />
        ); //eslint-disable-line

        return (
            <div
                className="comment-item comment-item--manage u-d-flex"
                key={index}
                rel={uuid}
            >
                <div className="comment__user u-clearfix">
                    <div className="account-summary__info">
                        {this.renderAvatar(artistImage)}
                    </div>
                </div>
                <div className="comment-item__content">
                    {contentBlock}
                    <div className="comment-item__meta u-fs-12 u-fw-600 u-ls-n-05 u-text-gray7">
                        {userAndTimeStamp}
                    </div>
                    {this.renderAdminActions(item)}
                </div>
            </div>
        );
    }

    renderAvatar(image) {
        return (
            <Avatar
                className="artist-page__avatar media-item__figure"
                type="artist"
                image={image}
                size={50}
                rounded
            />
        );
    }

    renderAdminActions(comment) {
        const removeButton = (
            <button
                className="u-spacing-left-em music-interaction"
                data-comment-uuid={comment.uuid}
                data-comment-thread={comment.thread}
                data-comment-kind={comment.kind}
                data-comment-id={comment.id}
                data-comment-action="remove"
                onClick={this.props.onCommentAction}
            >
                Remove Comment
            </button>
        );

        const banButton = (
            <button
                className="u-spacing-left-em music-interaction"
                data-user={comment.user_id}
                data-comment-action="ban"
                onClick={this.props.onCommentAction}
            >
                Ban User
            </button>
        );

        const unflagButton = (
            <button
                className="u-spacing-left-em music-interaction"
                data-comment-uuid={comment.uuid}
                data-comment-thread={comment.thread}
                data-comment-kind={comment.kind}
                data-comment-id={comment.id}
                data-comment-action="unflag"
                onClick={this.props.onCommentAction}
            >
                Clear This Flag
            </button>
        );

        return (
            <div
                className="comment__votes u-pos-relative u-spacing-top-5"
                style={{ marginBottom: '1.5em' }}
            >
                <span className="u-fw-600 u-ls-n-05">
                    Flag Count:{' '}
                    <span className="u-text-orange">
                        {comment.report_count}
                    </span>
                </span>
                <span className="u-fw-600 u-ls-n-05 u-spacing-left-5">
                    Removed Previously:{' '}
                    <span className="u-text-orange">
                        {comment.removed_count}
                    </span>
                </span>
                {removeButton}
                {banButton}
                {unflagButton}
            </div>
        );
    }

    renderComments(comments = []) {
        if (comments === null || comments.length === 0) {
            return <p>No flagged comments yet...</p>;
        }

        return comments.map((item, i) => {
            return this.renderCommentItem(item, i, true);
        });
    }

    renderSort() {
        const {
            onCommentSort,
            sortTooltipActive,
            activeCommentContext
        } = this.props;

        let orderBy = 'Most Counts';

        if (activeCommentContext === 'recent') {
            orderBy = 'Most Recent';
        }

        const submenuClass = classnames(
            'tooltip sub-menu sub-menu--condensed tooltip--radius tooltip--right-arrow feed-bar__tooltip comment-sort__tooltip',
            {
                'tooltip--active': sortTooltipActive
            }
        );

        const submenuItems = [
            {
                text: 'Most Counts',
                action: 'count',
                className:
                    activeCommentContext === 'count'
                        ? 'menu-button__list-item--active'
                        : ''
            },
            {
                text: 'Most Recent',
                action: 'recent',
                className:
                    activeCommentContext === 'recent'
                        ? 'menu-button__list-item--active'
                        : ''
            }
        ].map((item, i) => {
            return (
                <li
                    key={i}
                    className={`menu-button__list-item ${item.className}`}
                >
                    <button onClick={onCommentSort} data-action={item.action}>
                        {item.text}
                    </button>
                </li>
            );
        });

        const sortSubmenu = (
            <div>
                <ul className={submenuClass}>{submenuItems}</ul>
                <BodyClickListener
                    shouldListen={sortTooltipActive}
                    onClick={this.props.onCommentSortClick}
                    ignoreDomElement={this._tooltipButton}
                />
            </div>
        );

        return (
            <div>
                <button
                    className="comment-sort"
                    onClick={this.props.onCommentSortClick}
                    ref={(e) => {
                        this._tooltipButton = e;
                    }}
                >
                    <SortIcon className="comment-sort__icon" />
                    <span className="comment-sort__label u-spacing-left-5 u-fs-13 u-fw-700 u-ls-n-07">
                        {orderBy}
                    </span>
                </button>
                {sortSubmenu}
            </div>
        );
    }

    render() {
        const {
            comments,
            nextPage,
            loading,
            error,
            total
        } = this.props.comment;

        return (
            <div className="row u-spacing-bottom u-spacing-top-30">
                <div className="column small-24">
                    <div className="row expanded column small-24 u-box-shadow u-spacing-bottom-30">
                        <div className="column small-24">
                            <div className="feed-bar comment-sort-wrap u-padding-y-10 u-d-flex u-d-flex--justify-between">
                                <h2 className="feed-bar__title u-fs-18 u-ls-n-06 u-lh-11">
                                    <FlagIcon className="feed-bar__title-icon feed-bar__title-icon--comment u-text-icon" />
                                    <span className="u-spacing-left-5">
                                        <strong>Flagged Comments</strong>
                                    </span>
                                    <span className="u-spacing-left-5">
                                        ( 1 - {comments.length} of {total} )
                                    </span>
                                </h2>
                                {this.renderSort()}
                            </div>
                        </div>
                    </div>
                    <div className="row expanded column small-24 u-box-shadow">
                        <div className="column small-24 u-padding-y-20">
                            {this.renderLoader(loading && !error)}
                            {this.renderError(error)}
                            <div>{this.renderComments(comments)}</div>
                        </div>
                    </div>
                    <div className="u-spacing-top-30 u-text-center">
                        {this.renderLoadMore(nextPage)}
                    </div>
                </div>
            </div>
        );
    }
}
