import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import analytics from 'utils/analytics';
import connectDataFetchers from 'lib/connectDataFetchers';

import { getOverallStats, getSnapshot, getDaily } from '../redux/modules/stats';
import { getPlaySources } from '../redux/modules/stats/artist/playSource';
import { getCountryPlays } from '../redux/modules/stats/artist/geo';
import { getFans } from '../redux/modules/stats/artist/fans';
import { getInfluencers } from '../redux/modules/stats/artist/influencers';
import { fileInput, reset as resetUploadList } from '../redux/modules/upload';
import { addToast } from '../redux/modules/toastNotification';
import {
    getUserUploads,
    reset as resetUserUploads,
    removeItem
} from '../redux/modules/user/uploads';

import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_LABEL_SIGNUP,
    MODAL_TYPE_AMP_CODES
} from '../redux/modules/modal';
import { getSummary } from '../redux/modules/monetization/summary';
import { getPaymentInfo } from '../redux/modules/monetization/paymentInfo';
import { deleteItem } from '../redux/modules/music/index';
import { getAssociatedMusic } from '../redux/modules/monetization/associatedMusic';
import { getMusicSource } from '../redux/modules/admin';

import { queryToday, queryForTimespan } from 'utils/stats';

import requireAuth from '../hoc/requireAuth';
import DashboardMain from './DashboardMain';

class DashboardMainContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        upload: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserUploads: PropTypes.object,
        monetizationSummary: PropTypes.object,
        stats: PropTypes.object,
        statsArtistPlaySource: PropTypes.object,
        statsArtistFans: PropTypes.object,
        statsArtistInfluencers: PropTypes.object,
        statsArtistGeo: PropTypes.object,
        associatedMusic: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editActive: null,
            selectedVal: 'week',
            snapshotCategory: 'play_song'
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        if (
            prevProps.currentUserUploads.query &&
            prevProps.currentUserUploads.query.trim() !==
                this.props.currentUserUploads.query.trim()
        ) {
            this.performSearch(this.props.currentUserUploads.query);
        }

        if (
            prevProps.associatedMusic.query &&
            prevProps.associatedMusic.query.trim() !==
                this.props.associatedMusic.query.trim()
        ) {
            this.performAssociatedSearch(this.props.associatedMusic.query);
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(resetUserUploads());
        dispatch(resetUploadList());

        this._timer = null;
        this._attachedBodyListener = null;
        this._attachedElement = null;

        this.removeBodyListener();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBodyClick = (e) => {
        const target = e.target;

        if (
            this._attachedElement.contains(target) ||
            target === this._attachedElement
        ) {
            return;
        }

        this.setState({
            editActive: null
        });

        this.removeBodyListener();
    };

    /*
    handleTimespanClick = (text, e) => {
        const { dispatch, stats } = this.props;

        const show = stats.overall.show || undefined;
        const selected = e.currentTarget.getAttribute('data-value') || null;

        this.setState({
            selectedVal: selected
        });

        dispatch(getOverallStats(show, selected));
    };
    */

    handleTimespanClick = (text, e) => {
        const { dispatch } = this.props;
        const { snapshotCategory } = this.state;

        const value = e.currentTarget.getAttribute('data-value');
        const selected = value || null;

        this.setState({
            selectedVal: selected
        });

        let queryStart;

        if (value === 'week') {
            queryStart = queryForTimespan('week');
        }

        if (value === 'month') {
            queryStart = queryForTimespan('month');
        }

        if (value === 'three-months') {
            queryStart = queryForTimespan('three-months');
        }

        if (value === 'six-months') {
            queryStart = queryForTimespan('six-months');
        }

        if (value === 'year') {
            queryStart = queryForTimespan('year');
        }

        dispatch(getSnapshot(queryStart, queryToday()));
        dispatch(getDaily(queryStart, queryToday(), snapshotCategory));
    };

    handleSnapshotBlockClick = (e) => {
        const { dispatch } = this.props;
        const { selectedVal } = this.state;

        const button = e.currentTarget;
        const attribute = button.getAttribute('data-action');

        this.setState({
            snapshotCategory: attribute
        });

        let queryStart;

        if (selectedVal === 'week') {
            queryStart = queryForTimespan('week');
        }

        if (selectedVal === 'month') {
            queryStart = queryForTimespan('month');
        }

        if (selectedVal === 'three-months') {
            queryStart = queryForTimespan('three-months');
        }

        if (selectedVal === 'six-months') {
            queryStart = queryForTimespan('six-months');
        }

        if (selectedVal === 'year') {
            queryStart = queryForTimespan('year');
        }

        dispatch(getDaily(queryStart, queryToday(), attribute));
    };

    handleDeleteClick = (music) => {
        const { dispatch } = this.props;

        let message;

        switch (music.type) {
            case 'song':
                message =
                    "If you need to change some of the information associated with this song, or replace the existing MP3, please cancel and use the 'Edit song' action instead. Deleting a song will break any links the song may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this song, click the 'Delete' button below.";
                break;

            case 'album':
                message =
                    "If you need to change some of the information associated with this album please cancel and use the 'Edit album' action instead. Deleting an album will break any links the album may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this album, click the 'Delete' button below.";
                break;

            default:
                break;
        }

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `Delete ${music.type}: '${music.title}'`,
                message,
                handleConfirm: () =>
                    this.handleDeleteConfirm(music.id, music.type),
                confirmingText: 'Deleting...',
                confirmButtonText: `Delete ${music.type}`,
                confirmButtonProps: {
                    className: 'button button--pill button--danger button--wide'
                }
            })
        );
    };

    handleDeleteConfirm = (musicId, type) => {
        const { dispatch } = this.props;

        dispatch(deleteItem(musicId))
            .then(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Your ${type} was deleted`
                    })
                );
                dispatch(removeItem(musicId));
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));

                const ERROR_MUSIC_AMP_FORBIDDEN = 3039;
                let errorMessage = '';
                let messageDuration;

                if (error.errorcode === ERROR_MUSIC_AMP_FORBIDDEN) {
                    errorMessage = `This content is monetized through ${
                        error.errors[0]
                    } and must be deleted by them.  Please contact them or you can reach out to contentops@audiomack.com for additional assistance.`;
                    messageDuration = 20000;
                } else {
                    errorMessage =
                        'There was a problem deleting this music item.';
                }

                dispatch(
                    addToast(
                        {
                            action: 'message',
                            item: errorMessage
                        },
                        messageDuration
                    )
                );
                analytics.error(error);
                console.log(error);
            });
    };

    handleReplaceInputChange = (e) => {
        const { dispatch } = this.props;
        const input = e.currentTarget;
        const id = input.id;
        const uploadType = e.currentTarget.getAttribute('data-upload-type');
        const replaceId = parseInt(
            e.currentTarget.getAttribute('data-music-id'),
            10
        );

        dispatch(fileInput(id, uploadType, replaceId));

        this.setState({
            editActive: null
        });

        this.removeBodyListener();
    };

    handleEditItemClick = (item, e) => {
        const sameItem = item.id === this.state.editActive;

        if (!sameItem) {
            this._attachedElement = e.currentTarget.parentElement;
            this.attachBodyListener();
        }

        this.setState({
            editActive: sameItem ? null : item.id
        });
    };

    handleLabelSignup = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
    };

    handleEditAmpCodes = (item) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_AMP_CODES, { item, contextType: 'uploads' })
        );
    };

    handleDownloadClick = (item) => {
        const { dispatch } = this.props;

        dispatch(getMusicSource(item.id))
            .then((action) => {
                const json = action.resolved;
                if (item.type === 'album') {
                    for (const musicId in json) {
                        if (json[musicId]) {
                            window.open(json[musicId]);
                        }
                    }
                } else {
                    window.location.href = json.url;
                }
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem to get music ${
                            item.type
                        } url: ${error.message}`
                    })
                );
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;

        document.body.removeEventListener('click', this.handleBodyClick, false);
    }

    performSearch(query) {
        const { dispatch } = this.props;

        clearTimeout(this._timer);
        this._timer = setTimeout(() => {
            dispatch(
                getUserUploads({
                    page: 1,
                    query: query,
                    incompletes: 'yes'
                })
            );
        }, 500);
    }

    performAssociatedSearch(query) {
        const { dispatch } = this.props;

        clearTimeout(this._timer);
        this._timer = setTimeout(() => {
            dispatch(
                getAssociatedMusic({
                    page: 1,
                    query: query,
                    incompletes: 'yes'
                })
            );
        }, 500);
    }

    render() {
        return (
            <DashboardMain
                upload={this.props.upload}
                currentUser={this.props.currentUser}
                monetizationSummary={this.props.monetizationSummary}
                currentUserUploads={this.props.currentUserUploads}
                stats={this.props.stats}
                statsArtistPlaySource={this.props.statsArtistPlaySource}
                statsArtistFans={this.props.statsArtistFans}
                statsArtistInfluencers={this.props.statsArtistInfluencers}
                statsArtistGeo={this.props.statsArtistGeo}
                onTimespanClick={this.handleTimespanClick}
                onLabelSignup={this.handleLabelSignup}
                onDeleteClick={this.handleDeleteClick}
                onReplaceInputChange={this.handleReplaceInputChange}
                onEditItemClick={this.handleEditItemClick}
                onAmpEditCodes={this.handleEditAmpCodes}
                editActive={this.state.editActive}
                snapshotCategory={this.state.snapshotCategory}
                selectedVal={this.state.selectedVal}
                onSnapshotBlockClick={this.handleSnapshotBlockClick}
                onDownloadClick={this.handleDownloadClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        upload: state.upload,
        currentUser: state.currentUser,
        monetizationPaymentInfo: state.monetizationPaymentInfo,
        monetizationSummary: state.monetizationSummary,
        currentUserUploads: state.currentUserUploads,
        email: state.email,
        stats: state.stats,
        statsArtistPlaySource: state.statsArtistPlaySource,
        statsArtistFans: state.statsArtistFans,
        statsArtistInfluencers: state.statsArtistInfluencers,
        statsArtistGeo: state.statsArtistGeo,
        associatedMusic: state.monetizationAssociatedMusic
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(DashboardMainContainer, [
        () => getOverallStats(),
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getCountryPlays(props.currentUser.profile.id, {
                startDate: queryForTimespan('week'),
                endDate: queryToday()
            });
        },
        () => {
            const startDate = queryForTimespan('week');
            return getSnapshot(startDate, queryToday());
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getPlaySources(currentUser.profile.id, {
                startDate: queryForTimespan('week'),
                endDate: queryToday()
            });
        },
        () => {
            const startDate = queryForTimespan('week');
            return getDaily(startDate, queryToday());
        },
        () => getPaymentInfo(),
        () => getUserUploads({ incompletes: 'yes', limit: 5, sort: 'popular' }),
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
                return null;
            }

            return getSummary();
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getFans(currentUser.profile.id, {
                limit: 3
            });
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getInfluencers(currentUser.profile.id, {
                limit: 3
            });
        }
    ])
);
