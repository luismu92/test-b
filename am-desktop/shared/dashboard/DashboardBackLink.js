import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import ArrowPrevIcon from 'icons/arrow-prev-fat';

import styles from './DashboardBackLink.module.scss';

export default function DashboardBackLink({ href, text }) {
    return (
        <Link className={styles.link} to={href}>
            <ArrowPrevIcon /> <span className={styles.text}>{text}</span>
        </Link>
    );
}

DashboardBackLink.propTypes = {
    text: PropTypes.string,
    href: PropTypes.string
};
