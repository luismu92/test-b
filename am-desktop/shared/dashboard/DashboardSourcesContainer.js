import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';

import requireAuth from '../hoc/requireAuth';
import connectDataFetchers from 'lib/connectDataFetchers';

import {
    getPlaySources,
    clearList
} from '../redux/modules/stats/artist/playSource';
import { queryToday, queryForTimespan } from 'utils/stats';

import StatsSources from '../stats/StatsSources';
import DashboardModuleTitle from './DashboardModuleTitle';
import Dropdown from '../components/Dropdown';

import LinkIcon from 'icons/link';

class DashboardSourcesContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        statsArtistPlaySource: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedVal: 'week'
        };
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(clearList());
    }

    handleTimespanClick = (text, e) => {
        const { dispatch, currentUser } = this.props;

        const value = e.currentTarget.getAttribute('data-value');
        const selected = value || null;

        this.setState({
            selectedVal: selected
        });

        const queryStart = queryForTimespan(value);

        dispatch(
            getPlaySources(currentUser.profile.id, {
                startDate: queryStart,
                endDate: queryToday()
            })
        );
    };

    render() {
        const { selectedVal } = this.state;

        const timespanList = [
            {
                value: 'week',
                text: 'This Week'
            },
            {
                value: 'month',
                text: 'This Month'
            }
        ];

        const dropdown = (
            <Dropdown
                value={selectedVal}
                options={timespanList}
                onChange={this.handleTimespanClick}
                className="c-dropdown--button"
                buttonClassName="button button--pill"
                menuClassName="tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
            />
        );

        return (
            <div className="row u-spacing-top">
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Play Sources"
                        children={dropdown}
                        icon={<LinkIcon />}
                        spaceBetween
                    />
                </div>
                <div className="column small-24">
                    <div className="u-box-shadow u-pos-relative u-padded">
                        <StatsSources
                            dataSet={this.props.statsArtistPlaySource}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        statsArtistPlaySource: state.statsArtistPlaySource
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(DashboardSourcesContainer, [
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getPlaySources(currentUser.profile.id, {
                startDate: queryForTimespan('week'),
                endDate: queryToday()
            });
        }
    ])
);
