import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { UPLOAD_STATUS } from 'constants/index';
import { yesBool, getArtistName, getPodcastUrl } from 'utils/index';

import Table from '../components/Table';
import AmpEditCodesModal from '../modal/AmpEditCodesModal';
import DashboardMusicItem from '../dashboard/DashboardMusicItem';
import FeedBar from '../widgets/FeedBar';
import AndroidLoader from '../loaders/AndroidLoader';

import WrenchIcon from '../icons/wrench';
import SearchIcon from '../icons/search';

export default class DashboardManage extends Component {
    static propTypes = {
        upload: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserUploads: PropTypes.object,
        editActive: PropTypes.number,
        onFormSubmit: PropTypes.func,
        onLoadMoreUploadsClick: PropTypes.func,
        onUploadSearchInput: PropTypes.func,
        onDeleteClick: PropTypes.func,
        onEditItemClick: PropTypes.func,
        onReplaceInputChange: PropTypes.func,
        onRssInputChange: PropTypes.func,
        onRssSubmit: PropTypes.func,
        onEditAmpCodesClick: PropTypes.func,
        onUploadTypeClick: PropTypes.func,
        onAmpAssociatedEditCodes: PropTypes.func,
        associatedMusic: PropTypes.object,
        onLoadMoreAssociatedClick: PropTypes.func,
        onAssociatedTypeClick: PropTypes.func,
        onAssociatedSearchInput: PropTypes.func,
        onDownloadClick: PropTypes.func
    };

    /////////////////////
    // Helpers methods //
    /////////////////////

    getHeaderCells(showAmpColumn, showPlColumn) {
        return [
            {
                value: 'Title',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.title.localeCompare(x.title);
                    }

                    return x.title.localeCompare(y.title);
                },
                props: {
                    className: 'flex-3'
                }
            },
            {
                value: 'Artist',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return getArtistName(y).localeCompare(getArtistName(x));
                    }

                    return getArtistName(x).localeCompare(getArtistName(y));
                },
                props: {
                    className: 'flex-3'
                }
            },
            {
                value: 'Plays',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.stats['plays-raw'] - x.stats['plays-raw'];
                    }

                    return x.stats['plays-raw'] - y.stats['plays-raw'];
                }
            },
            {
                value: 'Favs',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return (
                            y.stats['favorites-raw'] - x.stats['favorites-raw']
                        );
                    }

                    return x.stats['favorites-raw'] - y.stats['favorites-raw'];
                }
            },
            showPlColumn
                ? {
                      value: (
                          <Fragment>
                              PL's
                              <span
                                  className="help-prompt dashboard-table__help-prompt"
                                  data-tooltip="The number of times your track has been added to a playlist"
                              >
                                  ?
                              </span>
                          </Fragment>
                      ),
                      comparator(direction, x, y) {
                          if (direction === 'desc') {
                              return (
                                  y.stats['playlists-raw'] -
                                  x.stats['playlists-raw']
                              );
                          }

                          return (
                              x.stats['playlists-raw'] -
                              y.stats['playlists-raw']
                          );
                      }
                  }
                : null,
            {
                value: 'Comments',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.stats.comments - x.stats.comments;
                    }

                    return x.stats.comments - y.stats.comments;
                }
            },
            showAmpColumn
                ? {
                      value: 'AMP',
                      props: {
                          className: 'flex-half u-text-center'
                      }
                  }
                : null,
            {
                value: 'Actions',
                props: {
                    className: 'medium-text-center'
                }
            }
        ].filter(Boolean);
    }

    renderUploadsTable() {
        const {
            currentUserUploads,
            onLoadMoreUploadsClick,
            onEditItemClick,
            onReplaceInputChange,
            onDeleteClick,
            editActive,
            upload,
            currentUser,
            onEditAmpCodesClick,
            onRssSubmit,
            onUploadSearchInput,
            onUploadTypeClick,
            onDownloadClick
        } = this.props;
        const hasNoUploads =
            !currentUserUploads.list.length && currentUserUploads.page === 1;
        let loader;
        let loadMoreButton;
        let podcastRows;

        const showPlColumn =
            currentUserUploads.show === 'all' ||
            currentUserUploads.show === 'songs';

        if (currentUserUploads.loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (!currentUserUploads.onLastPage && currentUserUploads.list.length) {
            loadMoreButton = (
                <button
                    className="button button--pill"
                    onClick={onLoadMoreUploadsClick}
                >
                    Load More Content +
                </button>
            );
        }

        if (currentUserUploads.show === 'podcast') {
            const deleteMessage = currentUser.profile.externalRssFeedUrl ? (
                <div className="row dashboard-table__podcast-rss-delete-message">
                    <span>
                        Deleting the RSS Feed URL will stop new episodes from
                        being imported.
                    </span>
                </div>
            ) : null;

            let rssFeedUrlButtonText = currentUser.profile.externalRssFeedUrl
                ? 'Delete'
                : 'Save';

            if (currentUser.externalRssFeedUrl.loading) {
                rssFeedUrlButtonText = 'Loading...';
            }

            podcastRows = (
                <div className="column small-24">
                    <div className="row dashboard-table__podcast-rss-wrapper">
                        <form
                            className="dashboard-table__podcast-rss-feed-url"
                            onSubmit={onRssSubmit}
                        >
                            <label htmlFor="rssFeedUrl">
                                External RSS Feed URL
                            </label>
                            <div className="input-wrapper">
                                <input
                                    type="url"
                                    defaultValue={
                                        currentUser.profile
                                            .externalRssFeedUrl || ''
                                    }
                                    id="rssFeedUrl"
                                    name="rssFeedUrl"
                                    placeholder="Enter an RSS Feed URL"
                                    disabled={
                                        currentUser.profile.externalRssFeedUrl
                                    }
                                />
                                <button className="button" type="submit">
                                    {rssFeedUrlButtonText}
                                </button>
                            </div>
                        </form>

                        <div className="dashboard-table__podcast-rss-audiomack-url">
                            <label htmlFor="rssAudiomackUrl">
                                Audiomack RSS Feed URL
                            </label>
                            <input
                                type="text"
                                id="rssAudiomackUrl"
                                defaultValue={getPodcastUrl(
                                    currentUser.profile.url_slug
                                )}
                                disabled
                            />
                        </div>
                    </div>

                    {deleteMessage}

                    <span className="row dashboard-table__podcast-episodes-label">
                        Episodes
                    </span>
                </div>
            );
        }

        const inProgressUploads = upload.list.reduce((acc, item) => {
            if (
                item.status !== UPLOAD_STATUS.FAILED &&
                item.status !== UPLOAD_STATUS.COMPLETED
            ) {
                acc.push(item);
            }

            return acc;
        }, []);

        const ampEnabled = yesBool(currentUser.profile.video_ads);
        const headerCells = this.getHeaderCells(
            ampEnabled && currentUserUploads.show !== 'playlists',
            showPlColumn
        );

        const rows = currentUserUploads.list.map((item, i) => {
            return {
                model: item,
                value: (
                    <DashboardMusicItem
                        key={i}
                        musicItem={item}
                        inProgressUploads={inProgressUploads}
                        onReplaceInputChange={onReplaceInputChange}
                        onEditItemClick={onEditItemClick}
                        onDeleteClick={onDeleteClick}
                        editActive={editActive}
                        ampEnabled={ampEnabled}
                        onEditAmpCodesClick={onEditAmpCodesClick}
                        displayEditColumn
                        showPlColumn={showPlColumn}
                        onDownloadClick={onDownloadClick}
                    />
                )
            };
        });

        let table;
        let noUploadsMessage;

        if (hasNoUploads && !currentUserUploads.loading) {
            if (
                currentUserUploads.query.trim() === '' &&
                currentUserUploads.show === 'all'
            ) {
                noUploadsMessage = <p>You have no uploads yet.</p>;
            } else {
                noUploadsMessage = <p>No uploads matched your search query.</p>;
            }
        } else {
            table = (
                <Fragment>
                    <Table
                        className="dashboard-table"
                        headerCells={headerCells}
                        bodyRows={rows}
                        ignoreTbody
                    />
                    <div className="u-spacing-top u-text-center">
                        {loadMoreButton}
                    </div>
                </Fragment>
            );
        }

        const contextItems = [
            { value: 'all', text: 'All Music' },
            { value: 'songs', text: 'Songs' },
            { value: 'albums', text: 'Albums' },
            { value: 'playlists', text: 'Playlists' },
            { value: 'podcast', text: 'Podcast' }
        ].map((item) => {
            return {
                ...item,
                active: item.value === currentUserUploads.show
            };
        });

        return (
            <Fragment>
                <div className="row u-spacing-top">
                    <div className="column small-24 dashboard-module__top">
                        <h4 className="dashboard-module__title">
                            <span className="u-text-orange u-text-icon u-d-inline-block">
                                <WrenchIcon />
                            </span>
                            Manage Content
                        </h4>
                    </div>
                </div>
                <div className="row u-spacing-bottom">
                    <div className="column small-24">
                        <div
                            className="u-box-shadow u-padded"
                            style={{ paddingTop: 0 }}
                        >
                            <div className="row dashboard-table-bar u-spacing-bottom">
                                <div className="small-24 medium-18">
                                    <FeedBar
                                        className="dashboard-feed-bar"
                                        activeMarker={currentUserUploads.show}
                                        items={contextItems}
                                        onContextSwitch={onUploadTypeClick}
                                        showBorderMarker
                                    />
                                </div>
                                <div className="small-24 medium-6">
                                    <form
                                        onSubmit={this.props.onFormSubmit}
                                        className="search-form dashboard-search-form"
                                    >
                                        <button
                                            type="submit"
                                            className="search-form__submit"
                                            value="Search"
                                        >
                                            <SearchIcon className="" />
                                        </button>
                                        <input
                                            type="text"
                                            className="search-form__input"
                                            placeholder="Search your music..."
                                            onChange={onUploadSearchInput}
                                            value={
                                                currentUserUploads.query || ''
                                            }
                                        />
                                    </form>
                                </div>
                            </div>
                            {podcastRows}
                            <div className="row">
                                <div className="column small-24">
                                    {table}
                                    {loader}
                                    {noUploadsMessage}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

    renderAssociatedTracks() {
        const {
            onLoadMoreAssociatedClick,
            onAssociatedTypeClick,
            onAssociatedSearchInput,
            onEditItemClick,
            onReplaceInputChange,
            onDeleteClick,
            editActive,
            upload,
            currentUser,
            onAmpAssociatedEditCodes,
            associatedMusic,
            onDownloadClick
        } = this.props;

        if (!currentUser.isLoggedIn || !currentUser.profile.label_owner) {
            return null;
        }

        // @todo refactor
        const hasNoUploads =
            !associatedMusic.list.length && associatedMusic.page === 1;
        let loader;
        let loadMoreButton;

        if (associatedMusic.loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (!associatedMusic.onLastPage && associatedMusic.list.length) {
            loadMoreButton = (
                <button
                    className="button button--pill"
                    onClick={onLoadMoreAssociatedClick}
                >
                    Load More Music +
                </button>
            );
        }

        const inProgressUploads = upload.list.reduce((acc, item) => {
            if (
                item.status !== UPLOAD_STATUS.FAILED &&
                item.status !== UPLOAD_STATUS.COMPLETED
            ) {
                acc.push(item);
            }

            return acc;
        }, []);

        const ampEnabled = yesBool(currentUser.profile.video_ads);
        const headerCells = this.getHeaderCells(ampEnabled);

        const rows = associatedMusic.list.map((item, i) => {
            return {
                model: item,
                value: (
                    <DashboardMusicItem
                        key={i}
                        musicItem={item}
                        inProgressUploads={inProgressUploads}
                        onReplaceInputChange={onReplaceInputChange}
                        onEditItemClick={onEditItemClick}
                        onDeleteClick={onDeleteClick}
                        editActive={editActive}
                        ampEnabled={ampEnabled}
                        onEditAmpCodesClick={onAmpAssociatedEditCodes}
                        displayEditColumn
                        hideDeleteButton
                        onDownloadClick={onDownloadClick}
                    />
                )
            };
        });

        let table;
        let noUploadsMessage;

        if (hasNoUploads && !associatedMusic.loading) {
            if (
                associatedMusic.query.trim() === '' &&
                associatedMusic.show === 'all'
            ) {
                noUploadsMessage = <p>You have no associated music yet.</p>;
            } else {
                noUploadsMessage = <p>No music matched your search query.</p>;
            }
        } else {
            table = (
                <Fragment>
                    <Table
                        className="dashboard-table"
                        headerCells={headerCells}
                        bodyRows={rows}
                        ignoreTbody
                    />
                    <div className="u-spacing-top u-text-center">
                        {loadMoreButton}
                    </div>
                </Fragment>
            );
        }

        const contextItems = [
            { value: 'all', text: 'All Music' },
            { value: 'songs', text: 'Songs' },
            { value: 'albums', text: 'Albums' }
        ].map((item) => {
            return {
                ...item,
                active: item.value === associatedMusic.show
            };
        });

        return (
            <Fragment>
                <div className="row">
                    <h4 className="column small-24 u-spacing-top-em u-spacing-bottom-em">
                        <span className="u-text-orange u-text-icon u-d-inline-block">
                            <WrenchIcon />
                        </span>
                        Manage Associated Content
                    </h4>
                </div>
                <div className="row">
                    <div className="column small-24">
                        <div
                            className="u-box-shadow u-padded"
                            style={{ paddingTop: 0 }}
                        >
                            <div className="row dashboard-table-bar u-spacing-bottom">
                                <div className="column small-24 medium-18">
                                    <FeedBar
                                        className="dashboard-feed-bar"
                                        activeMarker={associatedMusic.show}
                                        items={contextItems}
                                        onContextSwitch={onAssociatedTypeClick}
                                        showBorderMarker
                                    />
                                </div>
                                <div className="column small-24 medium-6">
                                    <form
                                        onSubmit={this.props.onFormSubmit}
                                        className="search-form dashboard-search-form"
                                    >
                                        <button
                                            type="submit"
                                            className="search-form__submit"
                                            value="Search"
                                        >
                                            <SearchIcon className="" />
                                        </button>
                                        <input
                                            type="text"
                                            className="search-form__input"
                                            placeholder="Search associated music..."
                                            onChange={onAssociatedSearchInput}
                                            value={associatedMusic.query || ''}
                                        />
                                    </form>
                                </div>
                            </div>
                            <div className="row">
                                <div className="column small-24">
                                    {table}
                                    {loader}
                                    {noUploadsMessage}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Dashboard Management on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                {this.renderUploadsTable()}
                {this.renderAssociatedTracks()}
                <AmpEditCodesModal />
            </Fragment>
        );
    }
}
