import React from 'react';

import styles from './MBHTService.module.scss';

export default function MBHTService() {
    return (
        <div className={styles.container}>
            <img
                className={styles.background}
                src="/static/images/desktop/mbhtaccent.jpg"
                srcSet="/static/images/desktop/mbhtaccent@2x.jpg 2x"
                alt=""
                style={{ marginBottom: 18 }}
            />
            <div className={styles.content}>
                <img
                    src="/static/images/desktop/mbhtlogo.png"
                    srcSet="/static/images/desktop/mbhtlogo@2x.png 2x"
                    alt="Music Business How To Logo"
                    style={{ marginBottom: 18 }}
                />
                <h2 className={styles.headline}>
                    Practical music business knowledge taught by industry
                    professionals
                </h2>
                <p className={styles.subline}>
                    Music Business How To provides the tools and techniques that
                    artists can utilize to grow their audience, build their
                    brand, and most importantly - make money along the way.
                </p>
                <div className={styles.buttons}>
                    <a
                        href={`${process.env.STATIC_URL}/mbht.pdf`}
                        className={styles.whiteButton}
                        download="The Artist's Guide to Selling Merch.pdf"
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        Download the artist’s guide to selling merch
                    </a>
                    <a
                        href="https://www.musicbusinesshowto.com/?via=audiomack"
                        className={styles.outlineButton}
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        Sign up now
                    </a>
                </div>
            </div>
        </div>
    );
}
