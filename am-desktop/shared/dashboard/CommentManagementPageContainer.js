import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import connectDataFetchers from 'lib/connectDataFetchers';
import requireAuth from '../hoc/requireAuth';

import {
    setCommentContext,
    getComments,
    deleteComment,
    banComment,
    unflagComment
} from '../redux/modules/comment';

import { ucfirst, hasValidComments } from 'utils/index';
import { KIND_MANAGEMENT_FLAGGED } from 'constants/comment';
import { addToast } from '../redux/modules/toastNotification';
import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import CommentManagementPage from './CommentManagementPage';

class CommentManagementPageContainer extends React.Component {
    static propTypes = {
        dispatch: PropTypes.func,
        comment: PropTypes.object,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            sortTooltipActive: false,
            activeCommentContext: 'count'
        };
    }

    componentDidMount() {
        this.getFlaggedComment();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCommentSortClick = () => {
        this.setState({
            sortTooltipActive: !this.state.sortTooltipActive
        });
    };

    handleCommentSort = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const value = button.getAttribute('data-action');

        dispatch(setCommentContext(KIND_MANAGEMENT_FLAGGED, null, value));
        dispatch(getComments());

        this.setState({
            activeCommentContext: value
        });
    };

    handleCommentGetMore = () => {
        const { comment, dispatch } = this.props;

        if (comment.nextPage === null) {
            return;
        }

        dispatch(getComments())
            .then((json) => {
                if (!hasValidComments(json.resolved)) {
                    this.handleCommentGetMore();
                }
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleCommentAction = (e) => {
        e.preventDefault();
        const action = e.currentTarget.getAttribute('data-comment-action');
        const uuid = e.currentTarget.getAttribute('data-comment-uuid');
        const thread = e.currentTarget.getAttribute('data-comment-thread');
        const kind = e.currentTarget.getAttribute('data-comment-kind');
        const id = e.currentTarget.getAttribute('data-comment-id');
        const user = e.currentTarget.getAttribute('data-user');

        if (action === 'remove') {
            this.doCommentRemove(uuid, thread, kind, id);
            return;
        }

        if (action === 'unflag') {
            this.doCommentUnflag(uuid, thread, kind, id);
            return;
        }

        this.props.dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: `${ucfirst(action)} ${
                    action === 'ban' ? 'user' : 'comment'
                }`,
                message: `Are you sure you want to ${action} ${
                    action === 'ban' ? 'this user?' : 'this comment?'
                }`,
                handleConfirm: () => {
                    switch (action) {
                        case 'ban':
                            this.doBanUser(user);
                            break;
                        default:
                            this.props.dispatch(hideModal());
                            break;
                    }
                }
            })
        );
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getFlaggedComment() {
        const { dispatch } = this.props;

        dispatch(
            setCommentContext(
                KIND_MANAGEMENT_FLAGGED,
                null,
                this.state.activeCommentContext
            )
        );
        dispatch(getComments());
    }

    doCommentRemove = (uuid, thread = null, kind, id) => {
        const { dispatch } = this.props;

        dispatch(deleteComment(uuid, thread === '' ? null : thread, kind, id))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Comment deleted.'
                    })
                );
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doBanUser = (user) => {
        const { dispatch } = this.props;

        dispatch(banComment(user))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'User has been banned from commenting.'
                    })
                );
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    doCommentUnflag = (uuid, thread = null, kind, id) => {
        const { dispatch } = this.props;

        dispatch(unflagComment(uuid, thread === '' ? null : thread, kind, id))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Flag cleared.'
                    })
                );
                return;
            })
            .catch((err) => {
                console.log(err);
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    render() {
        const { currentUser, comment } = this.props;

        return (
            <CommentManagementPage
                currentUser={currentUser}
                comment={comment}
                onCommentLoadMore={this.handleCommentGetMore}
                onCommentAction={this.handleCommentAction}
                sortTooltipActive={this.state.sortTooltipActive}
                onCommentSortClick={this.handleCommentSortClick}
                onCommentSort={this.handleCommentSort}
                activeCommentContext={this.state.activeCommentContext}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        comment: state.comment
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return currentUser.isLoggedIn && currentUser.profile.is_admin;
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/dashboard';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(
    connectDataFetchers(CommentManagementPageContainer, [
        () => {
            setCommentContext(KIND_MANAGEMENT_FLAGGED, null);
            getComments();
        }
    ])
);
