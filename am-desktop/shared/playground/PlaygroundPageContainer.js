import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
// import Cookie from 'js-cookie';

import connectDataFetchers from 'lib/connectDataFetchers';
import {
    // cookies,
    COLLECTION_TYPE_TRENDING,
    GENRE_TYPE_ALL,
    timeMap,
    CHART_TYPE_TOTAL
} from 'constants/index';
import { passiveOption } from 'utils/index';

import {
    // PLAYLIST_TYPE_BROWSE,
    // PLAYLIST_TYPE_MINE,
    // PLAYLIST_TYPE_FAVORITES,

    // setPlaylistContext,
    setContext,
    nextPage,
    fetchSongList,
    setGenre,
    setPage,
    setTimePeriod,
    clearList
} from '../redux/modules/music/browse';
// import { setLastDisplayTime } from '../redux/modules/ad';
import { getFeatured } from '../redux/modules/featured';

import Browse from '../browse/Browse';

const SCROLL_THRESHOLD = 300;

class PlaygroundPageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        route: PropTypes.object,
        ad: PropTypes.object,
        musicBrowse: PropTypes.object,
        player: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object,
        featured: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        // const { dispatch, ad } = this.props;
        const option = passiveOption();

        // When landing on the home page, we allow 12 minutes to go by
        // before displaying an ad
        // if (!ad.lastDisplayed && !Cookie.get(cookies.lastAdDisplayTime)) {
        //     dispatch(setLastDisplayTime(Date.now()));
        // }

        window.addEventListener('scroll', this.handleWindowScroll, option);
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleWindowScroll);
        this._scrollTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTimespanSwitch = (timespan) => {
        const { dispatch, history } = this.props;
        const { activeGenre, activeContext } = this.props.musicBrowse;

        dispatch(setTimePeriod(timespan));

        const prefix = activeGenre ? `/${activeGenre}` : '';
        const suffix = timespan ? `/${timespan}` : '';

        history.push(`${prefix}/${activeContext}${suffix}`);

        dispatch(clearList());
    };

    handleWindowScroll = () => {
        const { musicBrowse } = this.props;
        const { loading, onLastPage } = musicBrowse;

        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (loading || onLastPage) {
                return;
            }

            if (
                document.body.clientHeight -
                    (window.innerHeight + window.pageYOffset) <
                SCROLL_THRESHOLD
            ) {
                this.fetchNextPage();
            }
        }, 200);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    fetchNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(fetchSongList());
    };

    refetchIfNecessary(currentProps, nextProps) {
        const { params } = currentProps.match;
        const { params: nextParams } = nextProps.match;
        const changedGenre = currentProps.route.genre !== nextProps.route.genre;
        const changedContext =
            currentProps.route.context !== nextProps.route.context;
        const changedTime = params.timePeriod !== nextParams.timePeriod;
        const changedPage = params.page !== nextParams.page;

        const { dispatch } = currentProps;
        const options = {};

        if (
            changedPage &&
            !changedGenre &&
            !changedContext &&
            !changedTime &&
            params.page > nextParams.page
        ) {
            options.prependNewData = true;
        }

        if (changedGenre || changedContext || changedTime || changedPage) {
            if (changedGenre) {
                dispatch(setGenre(nextProps.route.genre));
            }

            if (changedContext) {
                dispatch(setContext(nextProps.route.context));
            }

            if (changedTime) {
                dispatch(setTimePeriod(nextParams.timePeriod));
            }

            if (changedPage) {
                dispatch(setPage(nextParams.page));
            }

            dispatch(fetchSongList(options));
        }
    }

    render() {
        const {
            currentUser,
            ad,
            musicBrowse,
            player,
            featured,
            location,
            route,
            match
        } = this.props;

        let controls;

        if (match.params.type.startsWith('freestar')) {
            const bgColor =
                location.pathname === '/playground/freestar'
                    ? '#009900'
                    : '#990000';

            controls = (
                <p
                    style={{
                        textAlign: 'center',
                        marginTop: 20,
                        background: bgColor,
                        padding: 30
                    }}
                >
                    {location.pathname !== '/playground/freestar' && (
                        <Link
                            to="/playground/freestar"
                            className="button button--pill"
                        >
                            Go to page with ads
                        </Link>
                    )}
                    {location.pathname !== '/playground/freestar-noads' && (
                        <Link
                            to="/playground/freestar-noads"
                            className="button button--pill"
                        >
                            Go to page with no ads
                        </Link>
                    )}
                </p>
            );
        }

        return (
            <Fragment>
                {controls}
                <Browse
                    currentUser={currentUser}
                    ad={ad}
                    musicBrowse={musicBrowse}
                    player={player}
                    onTimespanSwitch={this.handleTimespanSwitch}
                    timeMap={timeMap}
                    match={match}
                    featured={featured}
                    location={location}
                    route={route}
                />
                <meta name="robots" content="nofollow,noindex" />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser,
        musicBrowse: state.musicBrowse,
        player: state.player,
        featured: state.featured
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(PlaygroundPageContainer, [
            (params, query, props) => {
                if (!props.route) {
                    return null;
                }

                return setContext(COLLECTION_TYPE_TRENDING);
            },
            (params, query, props) => {
                if (!props.route) {
                    return null;
                }

                return setGenre(GENRE_TYPE_ALL);
            },
            () => setTimePeriod(CHART_TYPE_TOTAL),
            () => fetchSongList(),
            () => getFeatured()
        ])
    )
);
