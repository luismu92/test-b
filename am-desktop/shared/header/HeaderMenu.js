import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    renderCappedDisplayCount,
    buildRedirectUrl,
    buildDynamicImage
} from 'utils/index';

import CogsIcon from '../icons/cogwheels';
import PowerIcon from '../icons/power';
import HelpIcon from '../icons/help';
import EditIcon from '../icons/edit';
import RefreshIcon from '../icons/refresh';
import DefaultAvatar from '../icons/avatar';
import InboxOutIcon from '../icons/inbox-out';
import SpeakerIcon from 'icons/speaker';
import EmbedIcon from '../icons/embed-close';
import Verified from '../../../am-shared/components/Verified';

export default class HeaderMenu extends Component {
    static propTypes = {
        active: PropTypes.bool,
        notificationCount: PropTypes.number,
        currentUser: PropTypes.object,
        location: PropTypes.object.isRequired,
        onMenuItemClick: PropTypes.func,
        onAuthClick: PropTypes.func,
        onMenuClick: PropTypes.func
    };

    renderMenu(open) {
        const {
            onMenuItemClick,
            currentUser,
            currentUser: { profile }
        } = this.props;

        if (!profile) {
            return null;
        }

        const klass = classnames(
            'tooltip tooltip--radius sub-menu main-header__sub-menu',
            {
                'tooltip--active': open
            }
        );
        const items = [
            {
                text: 'Profile',
                href: `/artist/${profile.url_slug}`,
                icon: DefaultAvatar
            },
            {
                text: 'Edit Profile',
                href: '/edit/profile',
                testId: 'editProfile',
                icon: EditIcon
            },
            {
                text: 'Creator Dashboard',
                href: '/dashboard',
                icon: CogsIcon
            },
            {
                text: 'Artist Services',
                href: '/dashboard/artist-services',
                icon: SpeakerIcon
            },
            {
                text: 'Upload',
                href: '/upload',
                icon: InboxOutIcon
            },
            {
                text: 'Support',
                external: 'https://audiomack.zendesk.com/hc/en-us',
                action: 'contact',
                icon: HelpIcon,
                props: {
                    target: '_blank',
                    rel: 'nofollow noopener'
                }
            },
            {
                text: 'Sign out',
                action: 'logout',
                icon: PowerIcon,
                props: {
                    'data-testid': 'logoutButton'
                }
            },
            {
                text: 'Switch Account',
                href: '/switch-account',
                icon: RefreshIcon
            },
            currentUser.isAdmin || currentUser.profile.is_amp_approver
                ? {
                      text: 'Generate Amp Code',
                      href: '/amp-code',
                      icon: EmbedIcon
                  }
                : null
        ]
            .filter(Boolean)
            .map((item, i) => {
                const listItemClass = classnames('sub-menu__item', {
                    [item.className]: item.className
                });

                let tag = 'button';
                if (item.href) {
                    tag = Link;
                }

                if (item.external) {
                    tag = 'a';
                }

                const props = {
                    onClick: onMenuItemClick,
                    'data-testid': item.testId,
                    'data-action': item.action,
                    ...(item.props || {})
                };

                if (item.href) {
                    props.to = item.href;
                }

                if (item.external) {
                    props.href = item.external;
                }

                const element = React.createElement(tag, props, [
                    React.createElement(item.icon, {
                        key: `icon-${i}`,
                        className: 'sub-menu__icon'
                    }),
                    React.createElement('span', { key: `text-${i}` }, item.text)
                ]);

                return (
                    <li key={i} className={listItemClass}>
                        {element}
                    </li>
                );
            });

        return <ul className={klass}>{items}</ul>;
    }

    renderUserBlock(currentUser) {
        const {
            onMenuClick,
            onAuthClick,
            notificationCount,
            location
        } = this.props;

        if (!currentUser.isLoggedIn) {
            return (
                <div className="account-summary main-header__auth">
                    <span className="account-summary__text-links">
                        <a
                            href={buildRedirectUrl('/login', location)}
                            data-modal="login"
                            onClick={onAuthClick}
                            data-testid="signin"
                        >
                            Sign in
                        </a>
                        <span> / </span>
                        <a
                            href={buildRedirectUrl('/join', location)}
                            data-modal="join"
                            onClick={onAuthClick}
                            data-testid="signup"
                        >
                            Sign up
                        </a>
                    </span>
                    <Link
                        to="/login"
                        className="account-summary__avatar-default"
                    >
                        <DefaultAvatar />
                    </Link>
                </div>
            );
        }

        const check = <Verified user={currentUser.profile} small noTooltip />;

        let alert;

        if (notificationCount) {
            alert = (
                <span className="account-summary__user-alert">
                    {renderCappedDisplayCount(notificationCount)}
                </span>
            );
        }

        const imageSize = 30;
        const artwork = buildDynamicImage(
            currentUser.profile.image_base || currentUser.profile.image,
            {
                width: imageSize,
                height: imageSize,
                max: true
            }
        );

        const retinaArtwork = buildDynamicImage(
            currentUser.profile.image_base || currentUser.profile.image,
            {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            }
        );
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        return (
            <button
                className="account-summary account-summary--logged-in main-header__account-summary"
                onClick={onMenuClick}
                data-testid="headerAvatar"
            >
                <div className="account-summary__info">
                    <span>{currentUser.profile.name}</span>
                </div>
                <div className="account-summary__avatar-wrap u-pos-relative">
                    <img
                        className="account-summary__avatar u-right"
                        src={artwork}
                        srcSet={srcSet}
                        alt={currentUser.profile.name}
                    />
                    {check}
                    {alert}
                </div>
            </button>
        );
    }

    render() {
        const { active, currentUser } = this.props;

        return (
            <div className="main-header__block main-header__block--summary">
                {this.renderUserBlock(currentUser)}
                {this.renderMenu(active)}
            </div>
        );
    }
}
