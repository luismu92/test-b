import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import moment from 'moment';

import { renderCappedDisplayCount, getMusicUrl } from 'utils/index';
import { getTotalsFromPlaylists } from 'utils/notifications';

import Avatar from '../components/Avatar';

import NotificationIcon from '../icons/bell';
import HeartIcon from '../../../am-shared/icons/heart';
import RetweetIcon from '../icons/retweet';
import PlusIcon from '../icons/plus';
import MessageIcon from '../icons/message';
import PlaylistIcon from '../icons/playlist';

export default class HeaderNotifications extends Component {
    static propTypes = {
        active: PropTypes.bool,
        number: PropTypes.number,
        onButtonClick: PropTypes.func,
        onViewAllClick: PropTypes.func,
        notifications: PropTypes.array
    };

    static defaultProps = {
        notifications: [],
        onViewAllClick() {}
    };

    renderObjectImageLink(notification) {
        if (!notification.object) {
            return null;
        }

        return (
            <Link to={getMusicUrl(notification.object)}>
                <Avatar
                    type={notification.object.type}
                    size={40}
                    rounded={false}
                    className="notification-list__list-item-right"
                    image={
                        notification.object.image_base ||
                        notification.object.image
                    }
                />
            </Link>
        );
    }

    renderActorAvatarLink(notification) {
        if (!notification.actor) {
            return null;
        }

        const artistUrl = `/artist/${notification.actor.url_slug}`;

        return (
            <Link to={artistUrl}>
                <Avatar
                    type="artist"
                    size={40}
                    className="notification-list__list-item-left"
                    image={
                        notification.actor.image_base ||
                        notification.actor.image
                    }
                />
            </Link>
        );
    }

    renderNotification(notification) {
        const artistUrl = `/artist/${notification.actor.url_slug}`;
        let objectUrl = '';
        let stats = {};

        // Sometiems this comes back null for some reason
        if (notification.object && notification.object.type !== 'artist') {
            objectUrl = getMusicUrl(notification.object);
        }

        switch (notification.verb) {
            case 'reup':
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <RetweetIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--retweet" />
                            <strong>
                                <Link to={artistUrl}>
                                    {notification.actor.name}
                                </Link>
                            </strong>{' '}
                            reupped
                            {notification.object && (
                                <p className="notification-list__track-title">
                                    <strong>
                                        <Link to={objectUrl}>
                                            {notification.object.title}
                                        </Link>
                                    </strong>
                                </p>
                            )}
                        </span>
                        {this.renderObjectImageLink(notification)}
                    </li>
                );

            case 'favorite':
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <HeartIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--star" />
                            <strong>
                                <Link to={artistUrl}>
                                    {notification.actor.name}
                                </Link>
                            </strong>{' '}
                            favorited
                            {notification.object && (
                                <p className="notification-list__track-title">
                                    <strong>
                                        <Link to={objectUrl}>
                                            {notification.object.title}
                                        </Link>
                                    </strong>
                                </p>
                            )}
                        </span>
                        {this.renderObjectImageLink(notification)}
                    </li>
                );

            case 'follow':
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <strong>
                                <Link to={artistUrl}>
                                    {notification.actor.name}
                                </Link>
                            </strong>{' '}
                            is now following you
                            <p className="notification-list__list-item-meta">
                                {moment(notification.created_at).fromNow()}
                            </p>
                        </span>
                    </li>
                );

            case 'comment':
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <MessageIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--star" />
                            <strong>
                                <Link to={artistUrl}>
                                    {notification.actor.name}
                                </Link>
                            </strong>{' '}
                            {notification.extra_context.parent !== null
                                ? 'replied to your comment on'
                                : 'commented on'}
                            {notification.object && (
                                <p className="notification-list__track-title">
                                    <strong>
                                        <Link to={objectUrl}>
                                            {notification.object.title}
                                        </Link>
                                    </strong>
                                </p>
                            )}
                        </span>
                        {this.renderObjectImageLink(notification)}
                    </li>
                );

            case 'playlisted':
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <PlusIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--star" />
                            <strong>
                                <Link to={artistUrl}>
                                    {notification.actor.name}
                                </Link>
                            </strong>{' '}
                            playlisted
                            {notification.object && (
                                <p className="notification-list__track-title">
                                    <strong>
                                        <Link to={objectUrl}>
                                            {notification.object.title}
                                        </Link>
                                    </strong>
                                </p>
                            )}
                        </span>
                        {this.renderObjectImageLink(notification)}
                    </li>
                );

            case 'playlist_updated':
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <PlaylistIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--star" />
                            <strong>
                                <Link to={artistUrl}>
                                    {notification.actor.name}
                                </Link>
                            </strong>{' '}
                            added{' '}
                            {(notification.target || {}).title || 'untitled'} to
                            the{' '}
                            {notification.object && (
                                <p className="notification-list__track-title">
                                    <strong>
                                        <Link to={objectUrl}>
                                            "{notification.object.title}"
                                        </Link>
                                    </strong>{' '}
                                    playlist
                                </p>
                            )}
                        </span>
                        {this.renderObjectImageLink(notification)}
                    </li>
                );

            case 'playlist_updated_bundle':
                stats = getTotalsFromPlaylists(
                    notification.extra_context.playlists
                );
                return (
                    <li
                        key={notification.uid}
                        className="notification-list__list-item"
                    >
                        {this.renderActorAvatarLink(notification)}
                        <span className="notification-list__list-item-content notification-list__list-item-content--header">
                            <PlaylistIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--star" />{' '}
                            {stats.totalSongs} songs were added to{' '}
                            {stats.totalPlaylists} playlists you follow. Listen
                            now!
                        </span>
                    </li>
                );

            default:
                return null;
        }
    }

    renderPopup(open) {
        const klass = classnames(
            'tooltip tooltip--light notifications-tooltip u-box-shadow main-header__tooltip',
            {
                'tooltip--active notifications-tooltip--active': open
            }
        );

        const notifications = this.props.notifications
            .filter((item) => {
                if (!item.actor) {
                    return false;
                }

                return !item.seen;
            })
            .slice(0, 5);

        let items = notifications
            .map((notification) => {
                return this.renderNotification(notification);
            })
            .filter(Boolean);

        if (!items.length) {
            items = (
                <li>
                    <p>You have no new notifications.</p>
                </li>
            );
        }

        return (
            <div className={klass}>
                <p className="widget-title tooltip__title notifications-tooltip__title">
                    Notifications
                </p>
                <ul className="notification-list">{items}</ul>
                <div className="button-wrap notification-list__button-wrap notification-list__content u-text-center">
                    <Link
                        to="/notifications"
                        className="button button--pill"
                        onClick={this.props.onViewAllClick}
                    >
                        View all notifications
                    </Link>
                </div>
            </div>
        );
    }

    render() {
        const { active, onButtonClick, number } = this.props;
        let buttonLabel = 'Notifictions';

        if (number > 0) {
            buttonLabel = `You have ${number} notifications`;
        }

        return (
            <div className="main-header__block main-header__block--notifications">
                <button
                    className="account-notifications"
                    onClick={onButtonClick}
                    data-alert-count={renderCappedDisplayCount(number)}
                    aria-label={buttonLabel}
                >
                    <NotificationIcon className="account-notifications__icon" />
                </button>
                {this.renderPopup(active)}
            </div>
        );
    }
}
