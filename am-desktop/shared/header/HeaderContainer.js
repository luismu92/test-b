import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import classnames from 'classnames';

import connectDataFetchers from 'lib/connectDataFetchers';

import {
    NAV_MENU_NOTIFICATIONS,
    NAV_MENU_ACCOUNT
} from '../redux/modules/global';

import { logOut } from '../redux/modules/user/index';
import { getNotifications } from '../redux/modules/user/notifications';
import { showHeaderMenu, hideHeaderMenu } from '../redux/modules/global';
import { showModal, MODAL_TYPE_AUTH } from '../redux/modules/modal';
import BodyClickListener from '../components/BodyClickListener';

import SearchFormContainer from '../search/SearchFormContainer';
import AmLogo from '../icons/am-logo-new';
import InboxOutIcon from '../icons/inbox-out';
import SearchIcon from '../icons/search';

import AuthModal from '../modal/AuthModal';
import HeaderMenu from '../header/HeaderMenu';
import HeaderNotifications from '../header/HeaderNotifications';

class HeaderContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        match: PropTypes.object,
        location: PropTypes.object,
        history: PropTypes.object,
        currentUser: PropTypes.object,
        bare: PropTypes.bool,
        activeNavMenu: PropTypes.string,
        currentUserNotifications: PropTypes.object
    };

    static defaultProps = {
        bare: false
    };

    constructor(props) {
        super(props);

        this.state = {
            mobileSearch: false,
            clickedNotificationMenu: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        const { dispatch, currentUser } = this.props;

        if (!prevProps.currentUser.isLoggedIn && currentUser.isLoggedIn) {
            dispatch(getNotifications());
        }
    }

    componentWillUnmount() {
        this._attachedBodyListener = null;
        this._attachedElement = null;

        this.removeBodyListener();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSmallScreenClick = (e) => {
        const target = e.target;
        const sidebar = document.getElementById('site-sidebar');
        const menuButton = document.getElementById('menu-button');

        // Ignore menu button clicks
        if (target === menuButton || menuButton.contains(target)) {
            this.removeSmallScreenEvents();
            return;
        }

        // naive classification for a link.
        // To do correctly do this we'd have to make a DOM walk helper something
        // like jQuery's "closest" function. Luckily we don't have to do much
        // dom work in the React workflow so this may be a one-off case
        const isLink =
            target.tagName.toLowerCase() === 'a' ||
            target.parentElement.tagName.toLowerCase() === 'a' ||
            target.parentElement.parentElement.tagName.toLowerCase() === 'a';
        const isButton =
            target.tagName.toLowerCase() === 'button' ||
            target.parentElement.tagName.toLowerCase() === 'button' ||
            target.parentElement.parentElement.tagName.toLowerCase() ===
                'button';

        if (
            target === sidebar ||
            (sidebar.contains(target) && !isLink && !isButton)
        ) {
            return;
        }

        this.removeSmallScreenEvents();
        document.body.classList.remove('has-active-sidebar');
    };

    handleSmallScreenResize = () => {
        document.body.classList.remove('has-active-sidebar');
        this.removeSmallScreenEvents();
    };

    handleSidebarToggle = () => {
        // Lower breakpoints treat the sidebar like a dropdown menu
        // in that it goes away on resize or click of anywhere else
        // This will act funky if the css changes so make sure theyre in sync
        const LARGE_BREAKPOINT = 1200;

        if (window.innerWidth < LARGE_BREAKPOINT) {
            document.body.classList.toggle('has-active-sidebar');

            if (!document.body.classList.contains('has-active-sidebar')) {
                return;
            }

            console.info('Attached click');
            document.body.addEventListener(
                'click',
                this.handleSmallScreenClick,
                false
            );

            console.info('Attached resize');
            window.addEventListener(
                'resize',
                this.handleSmallScreenResize,
                false
            );
            return;
        }

        document.body.classList.toggle('has-hidden-sidebar');

        // Make sure things listening for resize get triggered.
        setTimeout(() => window.dispatchEvent(new Event('resize')), 200);
    };

    handleNotificationMenuClick = (e) => {
        this.setState({
            clickedNotificationMenu: true
        });

        this.menuClick(e, NAV_MENU_NOTIFICATIONS);
    };

    handleUploadLinkClick = (e) => {
        const { currentUser, dispatch } = this.props;

        dispatch(hideHeaderMenu());
        this.removeBodyListener();

        if (currentUser.isLoggedIn) {
            return;
        }

        e.preventDefault();

        const value = 'join';

        dispatch(showModal(MODAL_TYPE_AUTH, { type: value }));
    };

    handleAccountMenuClick = (e) => {
        this.menuClick(e, NAV_MENU_ACCOUNT);
    };

    // Clicking sign in/sign up in the header menu
    handleAuthLinkClick = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const link = e.currentTarget;
        const value = link.getAttribute('data-modal');

        // Using window history to not update the content under the mdoal
        window.history.pushState(null, null, `${link.pathname}${link.search}`);

        dispatch(showModal(MODAL_TYPE_AUTH, { type: value }));
    };

    handleMenuItemClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');
        const { dispatch, history } = this.props;

        switch (action) {
            case 'logout':
                dispatch(logOut());
                history.push('/');
                break;

            default:
                console.warn('No action found');
        }

        this.menuClick(e);
    };

    handleBodyClick = (e) => {
        const target = e.target;
        const { dispatch } = this.props;

        if (
            this._attachedElement.contains(target) ||
            target === this._attachedElement
        ) {
            return;
        }

        dispatch(hideHeaderMenu());
        this.removeBodyListener();
    };

    handleMobileSearch = () => {
        this.setState({
            mobileSearch: !this.state.mobileSearch
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    removeSmallScreenEvents() {
        console.info('Removed events');
        document.body.removeEventListener('click', this.handleSmallScreenClick);
        window.removeEventListener('resize', this.handleSmallScreenResize);
    }

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;

        document.body.removeEventListener('click', this.handleBodyClick, false);
    }

    menuClick(e, menu) {
        const { activeNavMenu, dispatch } = this.props;

        if (menu && activeNavMenu !== menu) {
            this._attachedElement = e.currentTarget.parentElement;

            dispatch(showHeaderMenu(menu));
            this.attachBodyListener();
            return;
        }

        dispatch(hideHeaderMenu());
        this.removeBodyListener();
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    renderNotifications(currentUser, activeNavMenu, notificationCount) {
        if (!currentUser.isLoggedIn) {
            return null;
        }

        const list = this.props.currentUserNotifications.list;

        return (
            <HeaderNotifications
                active={activeNavMenu === NAV_MENU_NOTIFICATIONS}
                onButtonClick={this.handleNotificationMenuClick}
                onViewAllClick={this.handleNotificationMenuClick}
                number={
                    this.state.clickedNotificationMenu ? 0 : notificationCount
                }
                notifications={list}
            />
        );
    }

    render() {
        const {
            activeNavMenu,
            currentUser,
            location,
            match,
            currentUserNotifications,
            bare
        } = this.props;
        const notificationCount = currentUserNotifications.unseen;

        let headerSearch;
        let headerRight;
        let authModal;

        const searchWrapClass = classnames('main-header__search-wrap', {
            'main-header__search-wrap--active': this.state.mobileSearch
        });

        if (bare) {
            if (currentUser.isLoggedIn) {
                headerRight = (
                    <div className="main-header__right">
                        <HeaderMenu
                            active={activeNavMenu === NAV_MENU_ACCOUNT}
                            currentUser={currentUser}
                            location={location}
                            notificationCount={notificationCount}
                            onMenuClick={this.handleAccountMenuClick}
                            onAuthClick={this.handleAuthLinkClick}
                            onMenuItemClick={this.handleMenuItemClick}
                        />
                    </div>
                );
            }
        } else {
            headerSearch = (
                <div className={searchWrapClass}>
                    <div
                        ref={(e) => {
                            this._searchContainer = e;
                        }}
                    >
                        <SearchFormContainer
                            match={match}
                            location={location}
                            id="main-search"
                        />
                    </div>
                    <BodyClickListener
                        shouldListen={this.state.mobileSearch}
                        onClick={this.handleMobileSearch}
                        ignoreDomElement={this._searchContainer}
                    />
                </div>
            );

            headerRight = (
                <div className="main-header__right">
                    <div className="main-header__mobile-search">
                        <button onClick={this.handleMobileSearch}>
                            <SearchIcon />
                        </button>
                    </div>
                    <HeaderMenu
                        active={activeNavMenu === NAV_MENU_ACCOUNT}
                        currentUser={currentUser}
                        location={location}
                        notificationCount={notificationCount}
                        onMenuClick={this.handleAccountMenuClick}
                        onAuthClick={this.handleAuthLinkClick}
                        onMenuItemClick={this.handleMenuItemClick}
                    />

                    {this.renderNotifications(
                        currentUser,
                        activeNavMenu,
                        notificationCount
                    )}

                    <div className="button-wrap main-header__button-wrap main-header__block main-header__block--button-wrap">
                        <Link
                            to="/upload"
                            className="button button--radius button--upload main-header__button"
                            data-testid="uploadButton"
                        >
                            <InboxOutIcon className="button__icon" />
                            <span>Upload</span>
                        </Link>
                    </div>
                </div>
            );

            authModal = <AuthModal />;
        }

        const klass = classnames('main-header main-header--fixed', {
            'main-header--bare': bare
        });

        let subscription;

        if (
            currentUser.isLoggedIn &&
            currentUser.profile.subscription !== null
        ) {
            subscription = (
                <span className="main-header__subscription">
                    <strong>Premium</strong>
                </span>
            );
        }

        return (
            <header id="main-header" className={klass}>
                <div className="main-header__left">
                    <button
                        id="menu-button"
                        className="burger main-header__nav-button"
                        onClick={this.handleSidebarToggle}
                        aria-label="Toggle menu"
                    >
                        <span className="burger__filling" />
                    </button>
                    <Link className="main-header__logo am-logo" to="/">
                        <h6 className="u-pos-relative">
                            <AmLogo />
                            <span
                                className="u-pos-absolute"
                                style={{ left: -999 }}
                            >
                                Audiomack Home
                            </span>
                        </h6>
                    </Link>
                    {subscription}
                </div>
                {headerSearch}
                {headerRight}
                {authModal}
            </header>
        );
    }
}

function mapStateToProps(state) {
    const { activeNavMenu } = state.global;

    return {
        currentUser: state.currentUser,
        currentUserNotifications: state.currentUserNotifications,
        activeNavMenu
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(HeaderContainer, [
            (params, query, props) => {
                if (
                    typeof window !== 'undefined' &&
                    window.location.pathname.indexOf('/notifications') === 0
                ) {
                    return null;
                }

                if (props && props.currentUser.isLoggedIn) {
                    return getNotifications();
                }

                return null;
            }
        ])
    )
);
