import { Component } from 'react';

export default class Shell extends Component {
    render() {
        // Nothing to see here.
        // Just a shell of the page for the service worker.
        return null;
    }
}
