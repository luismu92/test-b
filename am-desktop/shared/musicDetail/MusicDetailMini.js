import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    buildDynamicImage,
    getMusicUrl,
    renderFeaturingLinks
} from 'utils/index';

import PlayIcon from 'icons/play';
import HeartIcon from 'icons/heart';
import RetweetIcon from 'icons/retweet';
import PlusIcon from 'icons/plus-alt';

import styles from './MusicDetailMini.module.scss';

export default function MusicDetailMini({ item }) {
    const imageSize = 80;

    const artwork = buildDynamicImage(item.image_base || item.image, {
        width: imageSize,
        height: imageSize
    });

    const retinaArtwork = buildDynamicImage(item.image_base || item.image, {
        width: imageSize * 2,
        height: imageSize * 2
    });

    let featuring;
    if (item.featuring) {
        featuring = (
            <p className={`u-trunc ${styles.featuring}`}>
                {renderFeaturingLinks(item.featuring)}
            </p>
        );
    }

    const stats = [
        {
            count: item.stats.plays,
            stat: 'plays',
            icon: <PlayIcon />
        },
        {
            count: item.stats.favorites,
            stat: 'fav',
            icon: <HeartIcon />
        },
        {
            count: item.stats.reposts,
            stat: 'reup',
            icon: <RetweetIcon />
        },
        {
            count: item.stats.playlists,
            stat: 'playlist',
            icon: <PlusIcon />
        }
    ];

    const musicStats = stats.map((stat, i) => {
        if (stat.count <= 0) {
            return null;
        }

        return (
            <li key={`stat-${i}`} className={styles.stat}>
                <span className={classnames(styles.icon, styles[stat.stat])}>
                    {stat.icon}
                </span>
                {stat.count}
            </li>
        );
    });

    let musicStatsList;
    if (musicStats.length) {
        musicStatsList = <ul className={styles.stats}>{musicStats}</ul>;
    }

    return (
        <div className={styles.container}>
            <div className={styles.image}>
                <Link to={getMusicUrl(item)}>
                    <img
                        src={artwork}
                        srcSet={retinaArtwork}
                        alt={item.title}
                    />
                </Link>
            </div>
            <div style={{ minWidth: 0 }}>
                <Link to={getMusicUrl(item)}>
                    <h4 className={styles.title}>
                        <span className="u-trunc">{item.uploader.name}</span>
                        <strong className="u-trunc">{item.title}</strong>
                    </h4>
                </Link>
                {featuring}
                {musicStatsList}
            </div>
        </div>
    );
}

MusicDetailMini.propTypes = {
    item: PropTypes.object
};
