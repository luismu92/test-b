import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import analytics from 'utils/analytics';
// import Cookie from 'js-cookie';

import connectDataFetchers from 'lib/connectDataFetchers';
// import { SECTIONS } from 'constants/stats/section';
// import { cookies } from 'constants/index';

// import { setLastDisplayTime } from '../redux/modules/ad';
import hideSidebarForComponent from '../hoc/hideSidebarForComponent';
import { fetchSongList as fetchTrending } from '../redux/modules/home/trending';
import { fetchSongList as fetchSongs } from '../redux/modules/home/song';
import { fetchSongList as fetchAlbums } from '../redux/modules/home/album';
import { fetchTaggedPlaylists } from '../redux/modules/music/browsePlaylist';
// import { setSection } from '../redux/modules/stats';
import {
    showModal,
    MODAL_TYPE_AUTH,
    MODAL_TYPE_TEXT_ME_APP
} from '../redux/modules/modal';

import { getFeaturedPosts } from '../redux/modules/world/featured';
import LoggedOutHomePage from './LoggedOutHomePage';

const HERO_TYPES = ['all', 'artists', 'listeners', 'labels', 'podcasters'];

const HOME_PLAYLIST_SLUG = 'verified-series';

class LoggedOutHomePageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        // ad: PropTypes.object,
        homeTrending: PropTypes.object,
        homeSong: PropTypes.object,
        player: PropTypes.object,
        homeAlbum: PropTypes.object,
        history: PropTypes.object,
        musicBrowsePlaylist: PropTypes.object,
        location: PropTypes.object,
        worldFeatured: PropTypes.object,
        dispatch: PropTypes.func,
        featured: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            activeHero: HERO_TYPES[0]
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        document.body.classList.add(
            'has-hidden-sidebar',
            'no-header-border',
            'no-bottom-padding'
        );

        // this.props.dispatch(setSection(SECTIONS.homePageLoggedOut));
    }

    componentWillUnmount() {
        document.body.classList.remove(
            'has-hidden-sidebar',
            'no-header-border',
            'no-bottom-padding'
        );
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTimespanSwitch = (timespan, id) => {
        const { history } = this.props;

        let url;

        switch (id) {
            case 'songs':
                url = `/songs/${timespan}`;
                break;

            case 'albums':
                url = `/albums/${timespan}`;
                break;

            default:
                console.warn('No id attached to ContextSwitcher');
        }

        if (!url) {
            return;
        }

        history.push(url);
    };

    handleHeroTypeClick = (e) => {
        this.setState({
            activeHero: e.currentTarget.getAttribute('data-type')
        });
    };

    handleSignupClick = () => {
        this.props.dispatch(showModal(MODAL_TYPE_AUTH, { type: 'join' }));
    };

    handleTextModalClick = () => {
        const { dispatch } = this.props;

        this.loadBranchSdk()
            .then((sdk) => {
                dispatch(showModal(MODAL_TYPE_TEXT_ME_APP, { sdk }));
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    loadBranchSdk() {
        return new Promise((resolve, reject) => {
            require.ensure([], (require) => {
                const sdk = require('branch-sdk');

                sdk.init(process.env.BRANCH_KEY, (err, data) => {
                    if (err) {
                        analytics.error(err);
                        reject(err);
                        return;
                    }

                    resolve(sdk);

                    console.log(data);
                });
            });
        });
    }

    render() {
        return (
            <LoggedOutHomePage
                heroTypes={HERO_TYPES}
                homePlaylistName={HOME_PLAYLIST_SLUG}
                activeHero={this.state.activeHero}
                currentUser={this.props.currentUser}
                homeTrending={this.props.homeTrending}
                homeSong={this.props.homeSong}
                worldFeatured={this.props.worldFeatured}
                musicBrowsePlaylist={this.props.musicBrowsePlaylist}
                homeAlbum={this.props.homeAlbum}
                location={this.props.location}
                player={this.props.player}
                onTimespanSwitch={this.handleTimespanSwitch}
                onSignupClick={this.handleSignupClick}
                onHeroTypeClick={this.handleHeroTypeClick}
                onTextModalClick={this.handleTextModalClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        musicBrowsePlaylist: state.musicBrowsePlaylist,
        currentUser: state.currentUser,
        homeTrending: state.homeTrending,
        homeSong: state.homeSong,
        worldFeatured: state.worldFeatured,
        player: state.player,
        homeAlbum: state.homeAlbum
    };
}

export default compose(
    withRouter,
    hideSidebarForComponent,
    connect(mapStateToProps)
)(
    connectDataFetchers(LoggedOutHomePageContainer, [
        () => fetchTrending(),
        () => fetchSongs(),
        () => fetchAlbums(),
        () => fetchTaggedPlaylists(HOME_PLAYLIST_SLUG),
        () => getFeaturedPosts()
    ])
);
