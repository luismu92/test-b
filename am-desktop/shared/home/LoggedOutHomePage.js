import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    allGenresMap,
    timeMap,
    liveGenres,
    liveGenresExcludedFromCharts,
    GENRE_TYPE_ALL,
    GENRE_TYPE_PODCAST,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    CHART_TYPE_DAILY
} from 'constants/index';
import { SECTIONS } from 'constants/stats/section';

import AmMark from '../icons/am-mark';
import FireIcon from '../icons/fire';
import GraphIcon from '../icons/graph';
import AlbumIcon from '../icons/albums';
import FeedBar from '../widgets/FeedBar';
import Carousel from '../components/Carousel';
import MusicCardContainer from '../browse/MusicCardContainer';
import FooterDark from '../components/FooterDark';
import AndroidLoader from '../loaders/AndroidLoader';
import PlaylistCard from '../browse/PlaylistCard';
import AppBadge from '../components/AppBadge';
import BlogCarousel from '../components/BlogCarousel';

export default class LoggedOutHomePage extends Component {
    static propTypes = {
        musicBrowsePlaylist: PropTypes.object,
        homeTrending: PropTypes.object,
        homeSong: PropTypes.object,
        homeAlbum: PropTypes.object,
        player: PropTypes.object,
        worldFeatured: PropTypes.object,
        heroTypes: PropTypes.array,
        activeHero: PropTypes.string,
        homePlaylistName: PropTypes.string,
        onHeroTypeClick: PropTypes.func,
        onTextModalClick: PropTypes.func,
        onTimespanSwitch: PropTypes.func,
        onSignupClick: PropTypes.func
    };

    getContextItems(activeContext = '', activeTimePeriod = '') {
        let context = '';
        let timePeriod = '';

        if (activeContext) {
            context = `/${activeContext}`;
        }

        if (activeTimePeriod && activeContext !== COLLECTION_TYPE_TRENDING) {
            timePeriod = `/${timeMap[activeTimePeriod] || activeTimePeriod}`;
        }

        let genres = liveGenres
            .map((type) => {
                if (
                    activeContext !== COLLECTION_TYPE_TRENDING &&
                    liveGenresExcludedFromCharts.includes(type)
                ) {
                    return null;
                }

                return {
                    text: allGenresMap[type],
                    value: type,
                    href: `/${type}${context}${timePeriod}`,
                    active: false
                };
            })
            .filter(Boolean)
            .filter((type) => {
                // Filter out "All"
                return type.value !== GENRE_TYPE_ALL;
            });

        // Remove podcast if context is albums
        if (activeContext === COLLECTION_TYPE_ALBUM) {
            genres = genres.filter((type) => {
                return type.value !== GENRE_TYPE_PODCAST;
            });
        }

        return [
            {
                text: 'All Genres',
                value: '',
                href:
                    `${
                        activeContext === COLLECTION_TYPE_TRENDING
                            ? '/trending-now'
                            : context + timePeriod
                    }` || '/trending-now',
                active: true
            }
        ].concat(genres);
    }

    getTimespanOptions() {
        return [
            {
                text: 'Today',
                value: 'day',
                active: true
            },
            {
                text: 'This Week',
                value: 'week'
            },
            {
                text: 'This Month',
                value: 'month'
            },
            {
                text: 'This Year',
                value: 'year'
            },
            {
                text: 'All Time',
                value: ''
            }
        ];
    }

    getHeroTitle(active) {
        switch (active) {
            case 'artists':
                return 'Free, limitless hosting, sharing, and stats for creators';

            case 'listeners':
                return 'Know what’s trending, charting, and reduce data use';

            case 'labels':
                return 'Tap into millions of new listeners on Audiomack';

            case 'podcasters':
                return 'Power your podcasts with fast, powerful, and free hosting';

            default:
                return (
                    <span>
                        Audiomack is here to <br />
                        move music forward
                    </span>
                );
        }
    }

    getHeroSubTitle(active) {
        switch (active) {
            case 'artists':
                return (
                    <Fragment>
                        Audiomack gives artists unlimited storage, advanced
                        stats, advanced private links, and more -{' '}
                        <strong>completely free.</strong>
                    </Fragment>
                );

            case 'listeners':
                return (
                    <Fragment>
                        Find the hottest new music first in our trending section
                        - and take any song offline in our iOS and Android apps!
                    </Fragment>
                );

            case 'labels':
                return (
                    <Fragment>
                        Reach millions of new fans, easily monetize your
                        catalog, and leverage our label marketing platform -{' '}
                        <strong>completely free.</strong>
                    </Fragment>
                );

            case 'podcasters':
                return (
                    <Fragment>
                        Audiomack offers free, unlimited storage and RSS feeds
                        to power your podcast -{' '}
                        <strong>no premium account needed.</strong>
                    </Fragment>
                );

            default:
                return (
                    <Fragment>
                        Audiomack is a <strong>FREE</strong>, limitless music
                        sharing and discovery platform for{' '}
                        <strong>artists, tastemakers, labels, and fans</strong>.
                    </Fragment>
                );
        }
    }

    renderHeroTypes() {
        const types = this.props.heroTypes
            .map((type, i) => {
                if (type === 'all') {
                    return null;
                }

                const klass = classnames('hero-types__item', {
                    'hero-types__item--active': type === this.props.activeHero
                });

                return (
                    <li key={i} className={klass}>
                        <button
                            onClick={this.props.onHeroTypeClick}
                            data-type={type}
                            className="hero-types__button u-ls-n-05 u-fs-12"
                        >
                            {type}
                        </button>
                    </li>
                );
            })
            .filter(Boolean);

        return (
            <ul className="hero-types u-text-center u-inline-list">{types}</ul>
        );
    }

    renderHero() {
        let subContent;

        if (this.props.activeHero !== 'all') {
            subContent = (
                <button
                    className="hero__signup u-spacing-top-em button button--pill button--white button--large u-tt-uppercase"
                    onClick={this.props.onSignupClick}
                >
                    Sign up
                </button>
            );
        } else {
            subContent = (
                <ul className="app-badges app-badges--inline u-text-center u-inline-list u-spacing-top-em">
                    <li>
                        <AppBadge
                            type="sms"
                            onClick={this.props.onTextModalClick}
                        />
                    </li>
                    <li>
                        <AppBadge type="ios" style="image" />
                    </li>
                    <li>
                        <AppBadge type="android" style="image" />
                    </li>
                </ul>
            );
        }

        // Render all in the DOM for SEO
        const titlesAndSubTitles = this.props.heroTypes.map((type, i) => {
            const style = {};

            if (this.props.activeHero !== type) {
                style.opacity = '0';
            }

            const wrapClass = classnames('hero__titles', {
                'hero__titles--active': this.props.activeHero === type
            });

            return (
                <Fragment key={i}>
                    <div className={wrapClass} style={style}>
                        <h1 className="hero__title u-text-white u-lh-11">
                            {this.getHeroTitle(type)}
                        </h1>
                        <h4 className="hero__subtitle u-text-white u-fw-500 u-lh-13">
                            {this.getHeroSubTitle(type)}
                        </h4>
                    </div>
                </Fragment>
            );
        });

        return (
            <div className="hero hero--home u-pos-relative u-of-hidden">
                <AmMark className="hero__mark u-text-white" />
                <div className="hero__images-wrap u-pos-absolute">
                    <img
                        src="/static/images/desktop/about/create-macbook.png"
                        srcSet="/static/images/desktop/about/create-macbook@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--laptop"
                    />
                    <img
                        src="/static/images/desktop/about/create-ipad.png"
                        srcSet="/static/images/desktop/about/create-ipad@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--tablet"
                    />
                    <img
                        src="/static/images/desktop/about/create-phone-top.png"
                        srcSet="/static/images/desktop/about/create-phone-top@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--phone-top"
                    />
                    <img
                        src="/static/images/desktop/about/create-phone-bottom.png"
                        srcSet="/static/images/desktop/about/create-phone-bottom@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--phone-bottom"
                    />
                </div>
                <div className="hero__inner u-pos-relative u-text-center">
                    {titlesAndSubTitles}
                    {subContent}
                    <p className="u-tt-uppercase u-spacing-top u-spacing-bottom-10 u-ls-n-025 u-fs-14 u-text-white u-fw-700">
                        Explore features for:
                    </p>
                    {this.renderHeroTypes()}
                </div>
            </div>
        );
    }

    renderListKey(items) {
        return items
            .map((item) => {
                return item.key;
            })
            .join(':');
    }

    renderList(data = {}, context) {
        const { loading } = data;

        const filteredList = data.list.filter((item) => {
            return (
                item.status !== 'suspended' &&
                item.status !== 'takedown' &&
                !item.geo_restricted
            );
        });

        const items = filteredList.map((musicItem, i) => {
            let ranking;

            if (
                context === COLLECTION_TYPE_SONG ||
                context === COLLECTION_TYPE_ALBUM
            ) {
                ranking = i + 1;
            }

            let section = SECTIONS.trending;

            if (context === COLLECTION_TYPE_SONG) {
                section = SECTIONS.topSongs;
            }

            if (context === COLLECTION_TYPE_ALBUM) {
                section = SECTIONS.topAlbums;
            }

            return (
                <MusicCardContainer
                    key={`${musicItem.id}-${i}`}
                    className="column"
                    index={i}
                    item={musicItem}
                    shouldLinkArtwork
                    musicList={filteredList}
                    context={context}
                    genre={data.genre}
                    ranking={ranking}
                    card
                    section={section}
                />
            );
        });

        let loader;

        if (loading) {
            loader = (
                <li className="column small-24" key="loader">
                    <AndroidLoader className="music-feed__loader" />
                </li>
            );
        }

        return (
            <ul className="row u-spacing-top">
                <Carousel
                    className="column small-24"
                    itemWidth={250}
                    items={items}
                    getUniqueKey={this.renderListKey}
                />
                {loader}
            </ul>
        );
    }

    renderTrendingSection() {
        const { homeTrending } = this.props;

        let moreTrendingButton;

        if (homeTrending.list.length) {
            moreTrendingButton = (
                <div className="button-group homepage__section-button-group u-spacing-top u-text-center">
                    <Link
                        className="button button--pill button--med"
                        to="/trending-now"
                    >
                        All Trending Music
                    </Link>
                </div>
            );
        }

        return (
            <section className="homepage__section">
                <div className="row">
                    {/* seo */}
                    <h1 style={{ position: 'absolute', left: '-10000px' }}>
                        Share & Discover Music — Free
                    </h1>
                    <header className="column small-24 homepage__section-head">
                        <div className="music-feed__context-switcher u-box-shadow u-margin-0">
                            <FeedBar
                                title={
                                    <h2 className="feed-bar__title">
                                        <GraphIcon className="feed-bar__title-icon feed-bar__title-icon--graph u-text-icon" />{' '}
                                        <strong>Now Trending</strong>
                                    </h2>
                                }
                                id="trending"
                                items={this.getContextItems(
                                    COLLECTION_TYPE_TRENDING,
                                    ''
                                )}
                                contextDropDown
                                padded
                            />
                        </div>
                    </header>
                </div>
                {this.renderList(homeTrending, COLLECTION_TYPE_TRENDING)}
                {moreTrendingButton}
            </section>
        );
    }

    renderPlaylistSection() {
        const { musicBrowsePlaylist, homePlaylistName } = this.props;
        const item =
            musicBrowsePlaylist.list.find((list) => {
                return list.title === homePlaylistName;
            }) || {};
        const { title = musicBrowsePlaylist.title } = item;
        const sliced = musicBrowsePlaylist.list.slice(0, 4);
        const items = sliced.map((playlistItem, i) => {
            return (
                <div key={i} className="column">
                    <PlaylistCard item={playlistItem} />
                </div>
            );
        });

        const morePlaylistsButton = (
            <div className="button-group homepage__section-button-group u-spacing-top u-text-center">
                <Link
                    className="button button--pill button--med"
                    to="/playlists/browse"
                >
                    More Top Playlists
                </Link>
            </div>
        );

        const feedbarTitle = (
            <h2 className="feed-bar__title">
                <FireIcon className="feed-bar__title-icon feed-bar__title-icon--fire u-text-icon" />
                <strong>{title}</strong> Playlists
            </h2>
        );

        return (
            <section className="homepage__section">
                <div className="row">
                    <div className="column small-24">
                        <FeedBar
                            title={feedbarTitle}
                            className="u-box-shadow"
                            padded
                        />
                    </div>
                </div>
                <div className="card-grid row small-up-1 medium-up-2 large-up-4 u-spacing-top">
                    {items}
                </div>
                {morePlaylistsButton}
            </section>
        );
    }

    renderSongSection() {
        const { homeSong, onTimespanSwitch } = this.props;
        let moreSongsButton;

        if (homeSong.list.length) {
            moreSongsButton = (
                <div className="button-group homepage__section-button-group u-spacing-top u-text-center">
                    <Link
                        className="button button--pill button--med"
                        to="/songs/day"
                    >
                        See Full Top Songs Chart
                    </Link>
                </div>
            );
        }

        return (
            <section className="homepage__section">
                <div className="row">
                    <header className="column small-24 homepage__section-head">
                        <div className="music-feed__context-switcher u-box-shadow u-margin-0">
                            <FeedBar
                                title={
                                    <h2 className="feed-bar__title">
                                        <FireIcon className="feed-bar__title-icon feed-bar__title-icon--fire u-text-icon" />{' '}
                                        <strong>Top Songs</strong> Today
                                    </h2>
                                }
                                id="songs"
                                items={this.getContextItems(
                                    COLLECTION_TYPE_SONG,
                                    CHART_TYPE_DAILY
                                )}
                                options={this.getTimespanOptions(
                                    COLLECTION_TYPE_SONG
                                )}
                                onTimespanSwitch={onTimespanSwitch}
                                contextDropDown
                                padded
                            />
                        </div>
                    </header>
                </div>
                {this.renderList(homeSong, COLLECTION_TYPE_SONG)}
                {moreSongsButton}
            </section>
        );
    }

    renderAlbumSection() {
        const { homeAlbum, onTimespanSwitch } = this.props;

        let moreAlbumsButton;

        if (homeAlbum.list.length) {
            moreAlbumsButton = (
                <div className="button-group homepage__section-button-group u-spacing-top u-text-center">
                    <Link
                        className="button button--pill button--med"
                        to="/albums/day"
                    >
                        See Full Top Albums Chart
                    </Link>
                </div>
            );
        }

        return (
            <section className="homepage__section">
                <div className="row">
                    <header className="column small-24 homepage__section-head">
                        <div className="music-feed__context-switcher u-box-shadow u-margin-0">
                            <FeedBar
                                title={
                                    <h2 className="feed-bar__title">
                                        <AlbumIcon className="feed-bar__title-icon feed-bar__title-icon--album u-text-icon" />{' '}
                                        <strong>Top Albums</strong> Today
                                    </h2>
                                }
                                id="albums"
                                items={this.getContextItems(
                                    COLLECTION_TYPE_ALBUM,
                                    CHART_TYPE_DAILY
                                )}
                                options={this.getTimespanOptions(
                                    COLLECTION_TYPE_ALBUM
                                )}
                                onTimespanSwitch={onTimespanSwitch}
                                contextDropDown
                                padded
                            />
                        </div>
                    </header>
                </div>
                {this.renderList(homeAlbum, COLLECTION_TYPE_ALBUM)}
                {moreAlbumsButton}
            </section>
        );
    }

    renderAdSection() {
        const trapAd = (
            <div className="column small-24 medium-12 u-spacing-bottom">
                <div className="homepage__partner homepage__partner--trap u-text-center u-text-white">
                    <div className="row u-flex-center">
                        <div className="column small-24 medium-20 large-18">
                            <span className="homepage__partner-logo">
                                <img
                                    className="u-margin-center"
                                    src="/static/images/desktop/hp/trap-logo.png"
                                    srcSet="/static/images/desktop/hp/trap-logo@2x.png 2x"
                                    alt="Trap Symphony"
                                />
                            </span>
                            <p className="u-spacing-top-em u-spacing-bottom-em u-ls-n-05 u-lh-14">
                                Audiomack continues moving music forward by
                                fusing your favorite trap hits with a full
                                orchestra. With violins in the cut, trap music
                                never sounded better.
                            </p>
                            <div className="button-group">
                                <a
                                    className="button button--pill button--wide"
                                    href="https://audiomack.world/?id=5bec32a00cae"
                                    target="_blank"
                                    rel="noopener"
                                >
                                    Watch Now
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const worldAd = (
            <div className="column small-24 medium-12 u-spacing-bottom">
                <div className="homepage__partner homepage__partner--world u-text-center u-text-white">
                    <div className="row u-align-center">
                        <div className="column small-24 medium-18 large-14">
                            <span className="homepage__partner-logo">
                                <img
                                    className="u-margin-center"
                                    src="/static/images/desktop/hp/world-logo.png"
                                    srcSet="/static/images/desktop/hp/world-logo@2x.png 2x"
                                    alt="Audiomack World"
                                />
                            </span>
                            <p className="u-spacing-top-em u-spacing-bottom-em u-ls-n-05 u-lh-14">
                                Audiomack goes global to move music forward with
                                video content, playlists, photo essays, and
                                interviews from New York to the universe and
                                back.
                            </p>
                            <div className="button-group">
                                <a
                                    className="button button--pill button--wide"
                                    href="https://audiomack.world"
                                    rel="noopener"
                                    target="_blank"
                                >
                                    Explore the globe
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="homepage__section homepage__section--partners">
                <div className="row">
                    {trapAd}
                    {worldAd}
                </div>
            </div>
        );
    }

    renderAppBanner() {
        return (
            <div className="homepage__app-banner u-of-hidden">
                <div className="row">
                    <div className="column small-24 large-12 homepage__app-banner-devices u-pos-relative">
                        <img
                            src="/static/images/desktop/hp/hp-app-devices.png"
                            srcSet="/static/images/desktop/hp/hp-app-devices@2x.png 2x"
                            alt="Download our iOS and Android apps for Free"
                        />
                    </div>
                    <div className="column small-24 large-12 u-flex-center homepage__app-banner-copy">
                        <div>
                            <h3 className="homepage__app-banner-title u-spacing-bottom u-lh-12 u-fw-700 u-ls-n-2">
                                Download our iOS and Android apps{' '}
                                <span className="u-text-orange">for Free</span>
                            </h3>
                            <p className="u-fs-16">
                                Downloaded over 10 million times in less than
                                six months, our cross-platform app allows you to
                                listen to, discover, favorite, and share music
                                on the go.
                            </p>
                            <ul className="app-badges app-badges--inline u-inline-list u-spacing-top">
                                <li>
                                    <AppBadge type="ios" style="image" />
                                </li>
                                <li>
                                    <AppBadge type="android" style="image" />
                                </li>
                                <li>
                                    <AppBadge
                                        type="sms"
                                        onClick={this.props.onTextModalClick}
                                    />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                {this.renderHero()}
                <div className="homepage__section-wrap">
                    {this.renderTrendingSection()}
                    {this.renderPlaylistSection()}
                    {this.renderAdSection()}
                    {this.renderSongSection()}
                    {this.renderAlbumSection()}
                    <BlogCarousel
                        featured={this.props.worldFeatured}
                        className="homepage__section"
                    />
                </div>
                {this.renderAppBanner()}
                <FooterDark playerActive={!!this.props.player.currentSong} />
            </Fragment>
        );
    }
}
