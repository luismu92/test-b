import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class HomePageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired
    };

    // Usually this happens in connectDataFetchers but since we're doing
    // different things based on the user being logged out/in, this is a
    // special case. This may be the way to fetch data for components in
    // the future depending on how nice this feels
    // eslint-disable-next-line react/sort-comp
    static fetchData(dispatch, params = {}, query = {}, allProps = {}) {
        const { currentUser } = allProps;

        if (currentUser.isLoggedIn) {
            const component = require('./LoggedInHomePageContainer').default;

            return component.fetchData(dispatch, params, query, allProps);
        }

        if (!currentUser.isLoggedIn) {
            const component = require('./LoggedOutHomePageContainer').default;

            return component.fetchData(dispatch, params, query, allProps);
        }

        return null;
    }

    constructor(props) {
        super(props);

        this.state = {
            LoggedOutHomePage: null,
            LoggedInHomePage: null
        };

        const { currentUser } = props;

        if (currentUser.isLoggedIn) {
            const component = require('./LoggedInHomePageContainer').default;

            HomePageContainer.fetchData = component.fetchData;
            this.state.LoggedInHomePage = component;
        }

        if (!currentUser.isLoggedIn) {
            const component = require('./LoggedOutHomePageContainer').default;

            HomePageContainer.fetchData = component.fetchData;
            this.state.LoggedOutHomePage = component;
        }
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const loadAll = true;

        this.loadProperHomePage(loadAll);
    }

    //////////////////////
    // Internal methods //
    //////////////////////

    loadProperHomePage(loadAll = false) {
        const { currentUser } = this.props;

        require.ensure([], (require) => {
            const state = {};

            if (currentUser.isLoggedIn || loadAll) {
                const component = require('./LoggedInHomePageContainer')
                    .default;

                state.LoggedInHomePage = component;
            }

            if (!currentUser.isLoggedIn || loadAll) {
                const component = require('./LoggedOutHomePageContainer')
                    .default;

                state.LoggedOutHomePage = component;
            }

            this.setState(state);
        });
    }

    render() {
        const { LoggedOutHomePage, LoggedInHomePage } = this.state;
        const { currentUser } = this.props;

        if (currentUser.isLoggedIn && LoggedInHomePage) {
            return <LoggedInHomePage />;
        }

        if (!currentUser.isLoggedIn && LoggedOutHomePage) {
            return <LoggedOutHomePage />;
        }

        return null;
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(HomePageContainer);
