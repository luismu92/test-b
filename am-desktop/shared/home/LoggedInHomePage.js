import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
    allGenresMap,
    timeMap,
    liveGenres,
    liveGenresExcludedFromCharts,
    GENRE_TYPE_ALL,
    GENRE_TYPE_PODCAST,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    CHART_TYPE_DAILY
} from 'constants/index';

import { SECTIONS } from 'constants/stats/section';
import { eventLabel } from 'utils/analytics';

import FireIcon from '../icons/fire';
import GraphIcon from '../icons/graph';
import AlbumIcon from '../icons/albums';
import FeedBar from '../widgets/FeedBar';
import MusicDetailContainer from '../browse/MusicDetailContainer';
import BrowseEmptyState from '../browse/BrowseEmptyState';
import MusicFeedBlocks from '../ad/MusicFeedBlocks';
import AndroidLoader from '../loaders/AndroidLoader';
import BlogCarousel from '../components/BlogCarousel';
import Tunecore from '../components/Tunecore';

export default class LoggedInHomePage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        homeTrending: PropTypes.object,
        homeSong: PropTypes.object,
        location: PropTypes.object,
        homeAlbum: PropTypes.object,
        worldFeatured: PropTypes.object,
        onTimespanSwitch: PropTypes.func,
        showUploaderAd: PropTypes.bool,
        onUploaderAdClose: PropTypes.func,
        featured: PropTypes.object
    };

    getContextItems(activeContext = '', activeTimePeriod = '') {
        let context = '';
        let timePeriod = '';

        if (activeContext) {
            context = `/${activeContext}`;
        }

        if (activeTimePeriod && activeContext !== COLLECTION_TYPE_TRENDING) {
            timePeriod = `/${timeMap[activeTimePeriod] || activeTimePeriod}`;
        }

        let genres = liveGenres
            .map((type) => {
                if (
                    activeContext !== COLLECTION_TYPE_TRENDING &&
                    liveGenresExcludedFromCharts.includes(type)
                ) {
                    return null;
                }

                return {
                    text: allGenresMap[type],
                    value: type,
                    href: `/${type}${context}${timePeriod}`,
                    active: false
                };
            })
            .filter(Boolean)
            .filter((type) => {
                // Filter out "All"
                return type.value !== GENRE_TYPE_ALL;
            });

        // Remove podcast if context is albums
        if (activeContext === COLLECTION_TYPE_ALBUM) {
            genres = genres.filter((type) => {
                return type.value !== GENRE_TYPE_PODCAST;
            });
        }

        return [
            {
                text: 'All Genres',
                value: '',
                href:
                    `${
                        activeContext === COLLECTION_TYPE_TRENDING
                            ? '/trending-now'
                            : context + timePeriod
                    }` || '/trending-now',
                active: true
            }
        ].concat(genres);
    }

    getTimespanOptions() {
        return [
            {
                text: 'Today',
                value: 'day',
                active: true
            },
            {
                text: 'This Week',
                value: 'week'
            },
            {
                text: 'This Month',
                value: 'month'
            },
            {
                text: 'This Year',
                value: 'year'
            },
            {
                text: 'All Time',
                value: ''
            }
        ];
    }

    renderList(data = {}, context, howMany = 12) {
        const { featured, location } = this.props;
        const { loading } = data;

        const filteredList = data.list.filter((item) => {
            return (
                item.status !== 'suspended' &&
                item.status !== 'takedown' &&
                !item.geo_restricted
            );
        });

        const items = filteredList.slice(0, howMany).map((musicItem, i) => {
            let ranking;

            if (
                context === COLLECTION_TYPE_SONG ||
                context === COLLECTION_TYPE_ALBUM
            ) {
                ranking = i + 1;
            }

            let section = SECTIONS.trending;

            if (context === COLLECTION_TYPE_SONG) {
                section = SECTIONS.topSongs;
            }

            if (context === COLLECTION_TYPE_ALBUM) {
                section = SECTIONS.topAlbums;
            }

            return (
                <li
                    key={`${musicItem.id}-${i}`}
                    className="row expanded column small-24 u-spacing-bottom-60"
                >
                    <MusicDetailContainer
                        index={i}
                        item={musicItem}
                        feed
                        shouldLinkArtwork
                        musicList={filteredList}
                        context={context}
                        genre={data.genre}
                        ranking={ranking}
                        shouldLazyLoadArtwork={
                            context === COLLECTION_TYPE_TRENDING && i > 2
                        }
                        section={section}
                    />
                </li>
            );
        });

        let loader;

        if (loading) {
            loader = (
                <li key="loader">
                    <AndroidLoader className="music-feed__loader" />
                </li>
            );
        }

        if (!items.length && !loading) {
            return <BrowseEmptyState />;
        }

        if (items.length && featured && context === COLLECTION_TYPE_TRENDING) {
            const index = Math.min(3, data.list.length);
            const featuredBlock = (
                <MusicFeedBlocks
                    currentUser={this.props.currentUser}
                    key="blocks"
                    featuredItems={featured.list}
                    location={location}
                />
            );

            items.splice(index, 0, featuredBlock);
        }

        return (
            <ul className="column small-24">
                {items}
                {loader}
            </ul>
        );
    }

    renderSongSection() {
        const { homeSong, onTimespanSwitch } = this.props;
        let moreSongsButton;

        if (homeSong.list.length) {
            moreSongsButton = (
                <div className="button-group column small-24 homepage__section-button-group u-text-center">
                    <Link
                        className="button button--pill button--med"
                        to="/songs/day"
                    >
                        See Full Top Songs Chart
                    </Link>
                </div>
            );
        }

        return (
            <section className="homepage__section row">
                <header className="homepage__section-head column small-24">
                    <div className="music-feed__context-switcher u-box-shadow">
                        <FeedBar
                            title={
                                <h2 className="feed-bar__title">
                                    <FireIcon className="feed-bar__title-icon feed-bar__title-icon--fire u-text-icon" />{' '}
                                    <strong>Top Songs</strong> Today
                                </h2>
                            }
                            id="songs"
                            items={this.getContextItems(
                                COLLECTION_TYPE_SONG,
                                CHART_TYPE_DAILY
                            )}
                            options={this.getTimespanOptions(
                                COLLECTION_TYPE_SONG
                            )}
                            onTimespanSwitch={onTimespanSwitch}
                            contextDropDown
                            padded
                        />
                    </div>
                </header>
                {this.renderList(homeSong, COLLECTION_TYPE_SONG, 10)}
                {moreSongsButton}
            </section>
        );
    }

    renderAlbumSection() {
        const { homeAlbum, onTimespanSwitch } = this.props;

        let moreAlbumsButton;

        if (homeAlbum.list.length) {
            moreAlbumsButton = (
                <div className="button-group column small-24 homepage__section-button-group u-text-center">
                    <Link
                        className="button button--pill button--med"
                        to="/albums/day"
                    >
                        See Full Top Albums Chart
                    </Link>
                </div>
            );
        }

        return (
            <section className="homepage__section row">
                <header className="homepage__section-head column small-24">
                    <div className="music-feed__context-switcher u-box-shadow">
                        <FeedBar
                            title={
                                <h2 className="feed-bar__title">
                                    <AlbumIcon className="feed-bar__title-icon feed-bar__title-icon--album u-text-icon" />{' '}
                                    <strong>Top Albums</strong> Today
                                </h2>
                            }
                            id="albums"
                            items={this.getContextItems(
                                COLLECTION_TYPE_ALBUM,
                                CHART_TYPE_DAILY
                            )}
                            options={this.getTimespanOptions(
                                COLLECTION_TYPE_ALBUM
                            )}
                            onTimespanSwitch={onTimespanSwitch}
                            contextDropDown
                            padded
                        />
                    </div>
                </header>
                {this.renderList(homeAlbum, COLLECTION_TYPE_ALBUM, 5)}
                {moreAlbumsButton}
            </section>
        );
    }

    render() {
        const { homeTrending, homeSong, worldFeatured } = this.props;
        const shouldShowSongSection = !homeTrending.loading;
        const shouldShowAlbumSection =
            !homeTrending.loading && !homeSong.loading;

        let moreTrendingButton;

        if (homeTrending.list.length) {
            moreTrendingButton = (
                <div className="button-group column small-24 homepage__section-button-group u-text-center">
                    <Link
                        className="button button--pill button--med"
                        to="/trending-now"
                    >
                        All Trending Music
                    </Link>
                </div>
            );
        }

        return (
            <Fragment>
                <div className="row">
                    <div className="column">
                        <Tunecore
                            className="u-text-center u-spacing-top"
                            linkHref="https://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_naap_su&utm_content=gymosamamam"
                            eventLabel={eventLabel.homepageLoggedIn}
                            variant="home"
                        />
                    </div>
                </div>
                <div className="music-feed music-feed--chart homepage">
                    <section className="homepage__section row">
                        {/* seo */}
                        <h1 style={{ position: 'absolute', left: '-10000px' }}>
                            Share & Discover Music — Free
                        </h1>
                        <header className="homepage__section-head column small-24">
                            <div className="music-feed__context-switcher u-box-shadow">
                                <FeedBar
                                    title={
                                        <h2 className="feed-bar__title">
                                            <GraphIcon className="feed-bar__title-icon feed-bar__title-icon--graph u-text-icon" />{' '}
                                            <strong>Trending</strong>
                                        </h2>
                                    }
                                    id="trending"
                                    items={this.getContextItems(
                                        COLLECTION_TYPE_TRENDING,
                                        ''
                                    )}
                                    contextDropDown
                                    padded
                                />
                            </div>
                        </header>
                        {this.renderList(
                            homeTrending,
                            COLLECTION_TYPE_TRENDING,
                            12
                        )}
                        {moreTrendingButton}
                    </section>
                    {shouldShowSongSection ? this.renderSongSection() : null}
                    {shouldShowAlbumSection ? this.renderAlbumSection() : null}
                    <BlogCarousel
                        featured={worldFeatured}
                        className="homepage__section"
                    />
                </div>
            </Fragment>
        );
    }
}
