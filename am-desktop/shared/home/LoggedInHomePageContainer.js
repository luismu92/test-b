import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import Cookie from 'js-cookie';

import connectDataFetchers from 'lib/connectDataFetchers';
// import { SECTIONS } from 'constants/stats/section';
// import { cookies } from 'constants/index';

// import { setLastDisplayTime } from '../redux/modules/ad';
import { fetchSongList as fetchTrending } from '../redux/modules/home/trending';
import {
    fetchSongList as fetchSongs,
    clearList as clearSongs
} from '../redux/modules/home/song';
import {
    fetchSongList as fetchAlbums,
    clearList as clearAlbums
} from '../redux/modules/home/album';
import { getFeatured } from '../redux/modules/featured';
import { getFeaturedPosts } from '../redux/modules/world/featured';
// import { setSection } from '../redux/modules/stats';

import LoggedInHomePage from './LoggedInHomePage';

class LoggedInHomePageContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        // ad: PropTypes.object,
        homeTrending: PropTypes.object,
        homeSong: PropTypes.object,
        homeAlbum: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        worldFeatured: PropTypes.object,
        dispatch: PropTypes.func,
        featured: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            showUploaderAd: true
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        // const { dispatch, ad } = this.props;
        // When landing on the home page, we allow 12 minutes to go by
        // before displaying an ad
        // if (!ad.lastDisplayed && !Cookie.get(cookies.lastAdDisplayTime)) {
        //     dispatch(setLastDisplayTime(Date.now()));
        // }
        // this.props.dispatch(setSection(SECTIONS.homePageLoggedIn));
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(clearSongs());
        dispatch(clearAlbums());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTimespanSwitch = (timespan, id) => {
        const { history } = this.props;

        let url;

        switch (id) {
            case 'songs':
                url = `/songs/${timespan}`;
                break;

            case 'albums':
                url = `/albums/${timespan}`;
                break;

            default:
                console.warn('No id attached to ContextSwitcher');
        }

        if (!url) {
            return;
        }

        history.push(url);
    };

    handleUploaderAdClose = () => {
        this.setState({
            showUploaderAd: false
        });

        document.body.classList.remove('has-uploader-ad');
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const {
            currentUser,
            homeTrending,
            featured,
            homeSong,
            location,
            homeAlbum,
            worldFeatured
        } = this.props;

        return (
            <LoggedInHomePage
                currentUser={currentUser}
                homeTrending={homeTrending}
                homeSong={homeSong}
                worldFeatured={worldFeatured}
                homeAlbum={homeAlbum}
                location={location}
                featured={featured}
                onTimespanSwitch={this.handleTimespanSwitch}
                showUploaderAd={this.state.showUploaderAd}
                onUploaderAdClose={this.handleUploaderAdClose}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser,
        homeTrending: state.homeTrending,
        homeSong: state.homeSong,
        homeAlbum: state.homeAlbum,
        featured: state.featured,
        worldFeatured: state.worldFeatured
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(LoggedInHomePageContainer, [
            () => getFeatured(),
            () => fetchTrending(),
            () => fetchSongs(),
            () => fetchAlbums(),
            () => getFeaturedPosts()
        ])
    )
);
