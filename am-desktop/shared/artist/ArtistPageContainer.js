import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';
import classnames from 'classnames';

import ArtistPageMeta from 'components/ArtistPageMeta';
import connectDataFetchers from 'lib/connectDataFetchers';
import analytics from 'utils/analytics';
import { cookies } from 'constants/index';
import { passiveOption, smoothScroll } from 'utils/index';

import BlockModal from '../modal/BlockModal';
import {
    getPlaylists,
    nextPage as nextUserPlaylistsPage
} from '../redux/modules/user/playlists';
import { showModal, MODAL_TYPE_GHOST_CLAIM } from '../redux/modules/modal';
import {
    getUserFeed,
    nextPage as nextUserFeedPage,
    reset as resetUserFeedPage,
    setUploadsOnlyState as setUserFeedUploadState
} from '../redux/modules/user/feed';
// import { getUserUploads, nextPage as nextUserUploadsPage, reset as resetUserUploadsPage } from '../redux/modules/user/uploads';
import { setLastDisplayTime } from '../redux/modules/ad';
import { getArtist } from '../redux/modules/artist/index';
import {
    getArtistFeed,
    nextPage as nextFeedPage,
    reset as resetFeedPage,
    setUploadsOnlyState as setArtistFeedUploadState
} from '../redux/modules/artist/feed';
import {
    getArtistUploads,
    nextPage as nextUploadsPage,
    reset as resetUploadsPage
} from '../redux/modules/artist/uploads';
import {
    getArtistFavorites,
    nextPage as nextFavoritesPage,
    reset as resetFavoritesPage
} from '../redux/modules/artist/favorites';
import {
    getArtistPlaylists,
    nextPage as nextPlaylistsPage,
    reset as resetPlaylistsPage
} from '../redux/modules/artist/playlists';
import {
    getArtistFollowers,
    nextPage as nextFollowersPage,
    reset as resetFollowersPage
} from '../redux/modules/artist/followers';
import {
    getArtistFollowing,
    nextPage as nextFollowingPage,
    reset as resetFollowingPage
} from '../redux/modules/artist/following';
import { getPinned } from '../redux/modules/artist/pinned';

import ArtistHeader from './ArtistHeader';
import ToggleSwitch from 'components/ToggleSwitch';
import HeartIcon from '../../../am-shared/icons/heart';
import PlaylistIcon from '../icons/playlist';
import UserIcon from '../icons/avatar';
import MusicIcon from '../icons/music';
import PinIcon from '../../../am-shared/icons/pin';
import DownloadIcon from '../icons/download';
import SettingsIcon from '../icons/cog';
import FeedBar from '../widgets/FeedBar';
import ImageBanner from '../widgets/ImageBanner';
import PlaylistDetailContainer from '../browse/PlaylistDetailContainer';
import PlaylistCard from '../browse/PlaylistCard';
import MusicDetailContainer from '../browse/MusicDetailContainer';
import UserCard from '../browse/UserCard';
import AndroidLoader from '../loaders/AndroidLoader';
import FixOnScroll from '../utils/FixOnScroll';
import NotFound from '../NotFound';
import Truncate from 'components/Truncate';

const DEFAULT_CONTEXT = 'uploads';
const SCROLL_THRESHOLD = 300;

function makeContextRequest(params, ownsPage) {
    const artistId = params.artistId;
    // default to uploads since its first in the context switcher list
    const context = params.context || DEFAULT_CONTEXT;
    let fn = () => null; //eslint-disable-line

    switch (context) {
        case 'uploads':
            fn = () => getArtistUploads(artistId);

            /* if (ownsPage) {
                fn = () => getUserUploads();
            } */
            break;

        case 'favorites':
            fn = () => getArtistFavorites(artistId);
            break;

        case 'playlists':
            fn = () => getArtistPlaylists(artistId);

            if (ownsPage) {
                fn = () => getPlaylists();
            }
            break;

        case 'followers':
            fn = () => getArtistFollowers(artistId);
            break;

        case 'following':
            fn = () => getArtistFollowing(artistId);
            break;

        case 'feed':
            fn = () => getArtistFeed(artistId);

            if (ownsPage) {
                fn = () => getUserFeed();
            }
            break;

        default:
            console.warn('No fn implemented for context:', context);
            break;
    }

    return fn();
}

class ArtistPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        currentUserFeed: PropTypes.object,
        currentUserPlaylists: PropTypes.object,
        ad: PropTypes.object,
        match: PropTypes.object,
        artist: PropTypes.object.isRequired,
        artistFeed: PropTypes.object,
        artistUploads: PropTypes.object,
        artistPlaylists: PropTypes.object,
        artistFollowers: PropTypes.object,
        artistFollowing: PropTypes.object,
        artistPinned: PropTypes.object,
        artistFavorites: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            AdminControlsContainer: null,
            GhostClaimModal: null,
            navIsFixed: false
        };
    }

    componentDidMount() {
        const { dispatch, ad, artist, match } = this.props;
        const option = passiveOption();

        // When landing on the account page, we allow 12 minutes to go by
        // before displaying an ad
        if (!ad.lastDisplayed && !Cookie.get(cookies.lastAdDisplayTime)) {
            dispatch(setLastDisplayTime(Date.now()));
        }

        window.addEventListener('scroll', this.handleWindowScroll, option);

        this.loadAdminModules()
            .then(({ AdminControlsContainer }) => {
                this.setState({
                    AdminControlsContainer
                });
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });

        this.loadGhostClaimModal(match.params.code, match.params.ee, artist);
    }

    componentDidUpdate(prevProps) {
        const { params: prevParams } = prevProps.match;
        const {
            currentUser,
            artist,
            match: { params },
            dispatch
        } = this.props;
        const ownsPage =
            currentUser.isLoggedIn &&
            currentUser.profile.url_slug === params.artistId;

        if (!prevProps.artist.profile && this.props.artist.profile) {
            this.loadGhostClaimModal(params.code, params.ee, this.props.artist);
        }

        const scrollToPosition =
            artist.profile && artist.profile.image_banner ? 250 : 155;
        if (params.context === 'about' && prevParams.context !== 'about') {
            smoothScroll(scrollToPosition, { speed: 500 });
        }

        if (
            this.props.artist &&
            currentUser.isAdmin &&
            !this.state.AdminControlsContainer
        ) {
            this.loadAdminModules()
                .then(({ AdminControlsContainer }) => {
                    this.setState({
                        AdminControlsContainer
                    });
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    analytics.error(err);
                });
        }

        if (prevParams.artistId !== params.artistId) {
            this.props.dispatch(getArtist(params.artistId));
            this.props.dispatch(getPinned(params.artistId));
            this.clearArtistLists();
        }

        if (
            prevParams.context !== params.context ||
            prevProps.currentUser.isLoggedIn !== currentUser.isLoggedIn ||
            prevParams.artistId !== params.artistId ||
            prevParams.page !== params.page
        ) {
            const action = makeContextRequest(params, ownsPage);

            if (action) {
                dispatch(action);
            }
        }
    }

    componentWillUnmount() {
        this.clearArtistLists();
        window.removeEventListener('scroll', this.handleWindowScroll, false);
        this._scrollTimer = null;
        this._navOffsetTop = null;
        this._feedBar = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowScroll = () => {
        const { match, currentUser } = this.props;
        const { artistId, context } = match.params;
        const ownsPage =
            currentUser.isLoggedIn && currentUser.profile.url_slug === artistId;
        // default to uploads since its first in the context switcher list
        const paramContext = context || DEFAULT_CONTEXT;
        const contextualProps = {};

        contextualProps.context = paramContext;

        switch (paramContext) {
            case 'uploads': {
                const { artistUploads } = this.props;

                contextualProps.musicList = artistUploads;
                contextualProps.fetchContext = () => getArtistUploads(artistId);
                contextualProps.nextPage = nextUploadsPage;

                /* if (ownsPage) {
                    contextualProps.musicList = currentUserUploads;
                    contextualProps.fetchContext = () => getUserUploads();
                    contextualProps.nextPage = nextUserUploadsPage;
                } */

                break;
            }
            case 'favorites': {
                const { artistFavorites } = this.props;

                contextualProps.musicList = artistFavorites;
                contextualProps.fetchContext = () =>
                    getArtistFavorites(artistId);
                contextualProps.nextPage = nextFavoritesPage;
                break;
            }
            case 'playlists': {
                const { artistPlaylists, currentUserPlaylists } = this.props;

                if (ownsPage) {
                    contextualProps.musicList = currentUserPlaylists;
                    contextualProps.fetchContext = () => getPlaylists();
                    contextualProps.nextPage = nextUserPlaylistsPage;
                } else {
                    contextualProps.musicList = artistPlaylists;
                    contextualProps.fetchContext = () =>
                        getArtistPlaylists(artistId);
                    contextualProps.nextPage = nextPlaylistsPage;
                }

                break;
            }

            case 'followers': {
                const { artistFollowers } = this.props;

                contextualProps.musicList = artistFollowers;
                contextualProps.fetchContext = () =>
                    getArtistFollowers(artistId);
                contextualProps.nextPage = nextFollowersPage;
                break;
            }

            case 'following': {
                const { artistFollowing } = this.props;

                contextualProps.musicList = artistFollowing;
                contextualProps.fetchContext = () =>
                    getArtistFollowing(artistId);
                contextualProps.nextPage = nextFollowingPage;
                break;
            }

            case 'feed': {
                const { artistFeed, currentUserFeed } = this.props;

                if (ownsPage) {
                    contextualProps.musicList = currentUserFeed;
                    contextualProps.fetchContext = () => getUserFeed();
                    contextualProps.nextPage = nextUserFeedPage;
                } else {
                    contextualProps.musicList = artistFeed;
                    contextualProps.fetchContext = () =>
                        getArtistFeed(artistId);
                    contextualProps.nextPage = nextFeedPage;
                }

                break;
            }

            default:
                console.warn('No props implemented for context:', paramContext);
                break;
        }

        if (!contextualProps.musicList) {
            return;
        }

        const { loading, onLastPage } = contextualProps.musicList;

        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (loading || onLastPage) {
                return;
            }

            if (
                document.body.clientHeight -
                    (window.innerHeight + window.pageYOffset) <
                SCROLL_THRESHOLD
            ) {
                this.fetchNextPage(contextualProps);
            }
        }, 200);
    };

    handleTruncate = (truncated) => {
        this.setState({ truncated });
    };

    handleContextSwitch = (value) => {
        console.log(value);
    };

    handleFixedNavChange = (fixed) => {
        this.setState({
            navIsFixed: fixed
        });
    };

    handleExcludeReupClick = (e) => {
        const { currentUser, dispatch, match } = this.props;
        const ownsPage =
            currentUser.isLoggedIn &&
            currentUser.profile.url_slug === match.params.artistId;
        const setUploadState = ownsPage
            ? setUserFeedUploadState
            : setArtistFeedUploadState;
        const checked = e.currentTarget.checked;
        const options = {
            page: 1,
            showOnlyUploads: checked
        };
        const fn = ownsPage
            ? () => getUserFeed(options)
            : () => getArtistFeed(match.params.artistId, options);

        dispatch(setUploadState(checked));
        dispatch(fn()).catch(() => {
            dispatch(setUploadState(!checked));
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getListTypeFromContext(context) {
        if (context === 'playlists') {
            return 'playlist';
        }

        if (context === 'following' || context === 'followers') {
            return 'user';
        }

        return 'song';
    }

    loadAdminModules() {
        const { currentUser } = this.props;

        return new Promise((resolve) => {
            require.ensure([], (require) => {
                let AdminControlsContainer = null;

                if (currentUser.isAdmin) {
                    AdminControlsContainer = require('../admin/ArtistAdminControlsContainer')
                        .default;
                }

                resolve({
                    AdminControlsContainer
                });
            });
        });
    }

    loadGhostClaimModal(code, encodedEmail, artist) {
        if (!artist.profile || !encodedEmail || !code) {
            return;
        }

        if (this.state.GhostClaimModal || artist.profile.status !== 'ghost') {
            return;
        }

        const { dispatch } = this.props;

        require.ensure([], (require) => {
            this.setState(
                {
                    GhostClaimModal: require('../modal/GhostClaimModal').default
                },
                () => {
                    dispatch(
                        showModal(MODAL_TYPE_GHOST_CLAIM, {
                            email: window.atob(
                                decodeURIComponent(encodedEmail)
                            ),
                            code,
                            name: artist.profile.name,
                            id: artist.profile.id,
                            urlSlug: artist.profile.url_slug
                        })
                    );
                }
            );
        });
    }

    clearArtistLists() {
        const { dispatch } = this.props;

        [
            resetUserFeedPage,
            // resetUserUploadsPage,
            resetFeedPage,
            resetUploadsPage,
            resetFavoritesPage,
            resetPlaylistsPage,
            resetFollowersPage,
            resetFollowingPage
        ].forEach((fn) => dispatch(fn()));
    }

    fetchNextPage = (contextualProps) => {
        const { nextPage, fetchContext } = contextualProps;
        const {
            dispatch,
            match: {
                params: { artistId }
            }
        } = this.props;

        dispatch(nextPage());
        dispatch(fetchContext(artistId));
    };

    renderArtistLabel(artist) {
        if (!artist.label) {
            return null;
        }

        return (
            <li className="u-trunc">
                <span className="artist-section__info-label">Label:</span>
                <span className="artist-section__info-value">
                    {artist.label}
                </span>
            </li>
        );
    }

    renderArtistHometown(artist) {
        if (!artist.hometown) {
            return null;
        }

        return (
            <li className="u-trunc">
                <span className="artist-section__info-label">Hometown:</span>
                <span className="artist-section__info-value">
                    {artist.hometown}
                </span>
            </li>
        );
    }

    renderArtistMemberSince(artist) {
        const [, month, , year] = new Date(artist.created * 1000)
            .toDateString()
            .split(' ');
        const yearString = year.substr(-2);

        return (
            <li className="u-trunc">
                <span className="artist-section__info-label">
                    Member Since:
                </span>
                <span className="artist-section__info-value">
                    {month} &apos;{yearString}
                </span>
            </li>
        );
    }

    renderEmptyState(context, ownsPage) {
        let text;
        let icon;
        const thisUser = 'This user';

        switch (context) {
            case 'uploads': {
                const prefix = ownsPage ? "You haven't" : `${thisUser} hasn\'t`;
                const suffix = ownsPage ? (
                    <span>
                        <Link to="/upload/songs">Upload a song</Link> or{' '}
                        <Link to="/upload/albums">upload an album</Link> now.
                    </span>
                ) : (
                    ''
                );

                icon = (
                    <span className="empty-state__icon empty-state__icon--uploads">
                        <MusicIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} uploaded anything yet. {suffix}
                    </p>
                );
                break;
            }

            case 'favorites': {
                const prefix = ownsPage ? 'You have' : `${thisUser} has`;
                const suffix = ownsPage ? (
                    <span>
                        Play a song and hit the heart to add songs here.
                    </span>
                ) : (
                    ''
                );

                icon = (
                    <span className="empty-state__icon empty-state__icon--favs">
                        <HeartIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} no favorites. {suffix}
                    </p>
                );
                break;
            }

            case 'downloads': {
                const prefix = ownsPage ? 'You have' : `${thisUser} has`;

                icon = (
                    <span className="empty-state__icon empty-state__icon--downloads">
                        <DownloadIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} no downloads.
                    </p>
                );
                break;
            }

            case 'followers': {
                const prefix = ownsPage ? 'You have' : `${thisUser} has`;

                icon = (
                    <span className="empty-state__icon empty-state__icon--followers">
                        <UserIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} no followers.
                    </p>
                );
                break;
            }

            case 'following': {
                const prefix = ownsPage ? 'You are' : `${thisUser} is`;

                icon = (
                    <span className="empty-state__icon empty-state__icon--following">
                        <UserIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} not following anyone.
                    </p>
                );
                break;
            }

            case 'playlists': {
                const prefix = ownsPage ? "You haven't" : `${thisUser} hasn't`;

                icon = (
                    <span className="empty-state__icon empty-state__icon--playlists">
                        <PlaylistIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} created any playlists yet.
                    </p>
                );
                break;
            }

            case 'feed': {
                const prefix = ownsPage ? 'You have' : `${thisUser} has`;

                icon = (
                    <span className="empty-state__icon empty-state__icon--feed">
                        <MusicIcon />
                    </span>
                );
                text = (
                    <p className="empty-state__message u-text-center">
                        {prefix} no feed items yet.
                    </p>
                );
                break;
            }

            default:
                return null;
        }

        return (
            <div className="empty-state">
                {icon}
                {text}
            </div>
        );
    }

    renderArtistNav(
        context,
        urlPrefix,
        ownsPage,
        artist = {},
        currentUser = {}
    ) {
        // eslint-disable-line max-params
        const favorites = {
            text: 'Favorites',
            count: artist.favorites_count,
            href: `${urlPrefix}favorites`,
            active: context === 'favorites',
            value: 'favorites',
            buttonProps: {
                rel: 'nofollow'
            }
        };
        const playlists = {
            text: 'Playlists',
            count: ownsPage
                ? (currentUser.playlists || []).length
                : artist.playlists_count,
            href: `${urlPrefix}playlists`,
            active: context === 'playlists',
            value: 'playlists',
            buttonProps: {
                rel: 'nofollow'
            }
        };
        const following = {
            text: 'Following',
            count: artist.following_count,
            href: `${urlPrefix}following`,
            active: context === 'following',
            value: 'following',
            buttonProps: {
                rel: 'nofollow'
            }
        };
        const followers = {
            text: 'Followers',
            count: artist.followers_count,
            href: `${urlPrefix}followers`,
            active: context === 'followers',
            value: 'followers',
            buttonProps: {
                rel: 'nofollow'
            }
        };
        const uploads = {
            text: 'Uploads',
            count: artist.upload_count,
            href: `${urlPrefix}uploads`,
            active: context === 'uploads',
            value: 'uploads',
            buttonProps: {
                rel: 'nofollow'
            }
        };
        const feed = {
            text: 'Feed',
            href: `${urlPrefix}feed`,
            active: context === 'feed',
            value: 'feed',
            buttonProps: {
                rel: 'nofollow'
            }
        };
        const about = {
            text: 'About',
            href: `${urlPrefix}about`,
            active: context === 'about',
            value: 'about',
            buttonProps: {
                rel: 'nofollow'
            }
        };

        let contextItems = [
            uploads,
            favorites,
            playlists,
            following,
            feed,
            about
        ];

        if (currentUser) {
            if (currentUser.id === artist.id || currentUser.is_admin) {
                contextItems = [
                    uploads,
                    favorites,
                    playlists,
                    following,
                    followers,
                    feed,
                    about
                ];
            }
        }

        return (
            <FeedBar
                className="column small-24"
                activeMarker={context}
                items={contextItems}
                onContextSwitch={this.handleContextSwitch}
                showBorderMarker
            />
        );
    }

    renderRightStats(artist) {
        if (!artist) {
            return null;
        }
        // Probably will use moment later
        const [, month, , year] = new Date(artist.created * 1000)
            .toDateString()
            .split(' ');
        const yearString = year.substr(-2);

        return (
            <ul className="music__rankings">
                <li>
                    <strong className="music__ranking-label">Followers</strong>
                    <p className="music__ranking-number">
                        {(artist.followers_count || 0).toLocaleString()}
                    </p>
                </li>
                <li>
                    <strong className="music__ranking-label">Uploads</strong>
                    <p className="music__ranking-number">
                        {(artist.upload_count || 0).toLocaleString()}
                    </p>
                </li>
                <li>
                    <strong className="music__ranking-label">
                        Member since
                    </strong>
                    <p className="music__ranking-number">
                        {month} &apos;{yearString}
                    </p>
                </li>
            </ul>
        );
    }

    renderPinnedMusic(data = []) {
        const { currentUser, match, artistPinned } = this.props;
        const ownsPage =
            currentUser.isLoggedIn &&
            currentUser.profile.url_slug === match.params.artistId;

        const filteredList = artistPinned.list.filter(
            (item) => !item.geo_restricted
        );

        if (!filteredList.length && !ownsPage) {
            return null;
        }

        const showPinMenu = ownsPage;

        let items = filteredList
            .map((musicItem, i) => {
                switch (musicItem.type) {
                    case 'playlist':
                        return (
                            <div className="column pinned-item" key={i}>
                                <PlaylistDetailContainer
                                    index={i}
                                    pinned
                                    playlist={musicItem}
                                    showPlayButton={false}
                                    hideStats
                                    showPinMenu
                                    item={musicItem}
                                />
                            </div>
                        );
                    case 'song':
                    case 'album':
                        return (
                            <div className="column pinned-item" key={i}>
                                <MusicDetailContainer
                                    key={i}
                                    index={i}
                                    profile
                                    pinned
                                    shouldLinkArtwork
                                    item={musicItem}
                                    musicList={data}
                                    showPinMenu={showPinMenu}
                                    showArtworkPlayButton={
                                        filteredList.length >= 3
                                    }
                                />
                            </div>
                        );
                    default:
                        return null;
                }
            })
            .filter(Boolean);

        if (!items.length) {
            items = (
                <div className="column small-24">
                    <div className="pinned-items__empty u-padding-40 u-text-center">
                        <p className="u-fw-600">
                            Highlight your hottest new singles, best albums, and
                            favorite playlists to your Audiomack profile, making
                            sure fans never miss a release!{' '}
                            <Link to="/edit/pins">Try it now</Link>
                        </p>
                    </div>
                </div>
            );
        }

        if (artistPinned.loading) {
            items = (
                <div className="column small-24">
                    <div className="u-padding-40 u-text-center">
                        <AndroidLoader />
                    </div>
                </div>
            );
        }

        const count = items.length;
        const klass = classnames(
            `pinned-items pinned-items--${count} u-padding-x-20`,
            {
                'pinned-items--multiple': count > 1,
                'pinned-items--condensed': count > 2
            }
        );

        let manageLink;

        if (ownsPage) {
            manageLink = (
                <Link
                    to="/edit/pins"
                    data-tooltip="Manage Highlighted Content"
                    className="u-text-gray9"
                >
                    <SettingsIcon className="u-text-icon pinned-title__edit" />
                </Link>
            );
        }

        return (
            <Fragment>
                <div className="u-spacing-top-15">
                    <h3 className="pinned-title column small-24 u-spacing-bottom-25 u-fs-18 u-lh-18 u-ls-n-07">
                        <PinIcon className="u-text-icon u-brand-color pinned-title__icon" />
                        Highlighted Content {manageLink}
                    </h3>
                    <div className={klass}>{items}</div>
                </div>
            </Fragment>
        );
    }

    renderRecentMusic(data = [], pinned = [], loading, loader, emptyState) {
        const filteredData = data.filter((item) => !item.geo_restricted);
        const allData = pinned.concat(filteredData);
        const items = filteredData.map((musicItem, i) => {
            return (
                <div className="column small-24 u-spacing-bottom-20" key={i}>
                    <MusicDetailContainer
                        className="user-profile__feed-item"
                        key={i}
                        index={pinned.length + i}
                        profile
                        shouldLinkArtwork
                        shouldLazyLoadArtwork={i > 2}
                        item={musicItem}
                        musicList={allData}
                        shouldShowReup
                    />
                </div>
            );
        });
        let title;

        if (filteredData.length) {
            title = (
                <h3 className="column small-24 u-padding-0 u-spacing-bottom-25 u-fs-18 u-lh-18 u-ls-n-07">
                    <MusicIcon className="u-text-icon u-brand-color" />
                    Recent Uploads
                </h3>
            );
        }

        return (
            <Fragment>
                {this.renderPinnedMusic(allData)}
                <div className="u-spacing-top-20">
                    <div className="column small-24">{title}</div>
                    {items}
                    <div className="column small-24">
                        {!items.length && !loading ? emptyState : null}
                        {loading ? loader : null}
                    </div>
                </div>
            </Fragment>
        );
    }

    renderFavorites(data = [], loading, loader, emptyState) {
        const filteredData = data.filter((item) => !item.geo_restricted);
        const items = filteredData.map((musicItem, i) => {
            if (musicItem.type === 'playlist') {
                return (
                    <div
                        className="column small-24 u-spacing-bottom-20"
                        key={i}
                    >
                        <PlaylistDetailContainer
                            index={i}
                            playlist={musicItem}
                            showPlayButton={false}
                            shouldLazyLoadArtwork={i > 2}
                        />
                    </div>
                );
            }

            return (
                <div className="column small-24 u-spacing-bottom-20" key={i}>
                    <MusicDetailContainer
                        className="user-profile__feed-item"
                        index={i}
                        profile
                        shouldLinkArtwork
                        shouldLazyLoadArtwork={i > 2}
                        item={musicItem}
                        musicList={filteredData}
                    />
                </div>
            );
        });

        return (
            <Fragment>
                {items}
                <div className="column small-24">
                    {!items.length && !loading ? emptyState : null}
                    {loading ? loader : null}
                </div>
            </Fragment>
        );
    }

    renderPlaylists(data = [], loading, loader, emptyState) {
        const items = data.map((playlist, i) => {
            return (
                <div key={i} className="column">
                    <PlaylistCard
                        item={playlist}
                        shouldLazyLoadArtwork={i > 5}
                    />
                </div>
            );
        });

        const feedClass = classnames(
            'music-feed row card-grid row row--full-width',
            {
                'small-up-1 medium-up-2 large-up-4': items.length
            }
        );

        return (
            <div className={feedClass}>
                {items}
                <div className="column small-24">
                    {!items.length && !loading ? emptyState : null}
                    {loading ? loader : null}
                </div>
            </div>
        );
    }

    renderUserList(data = [], title, loading, loader, emptyState) {
        // eslint-disable-line max-params
        const items = data.map((user, i) => {
            return (
                <div
                    className="column small-24 medium-12 large-6 xlarge-fifth"
                    key={i}
                >
                    <UserCard user={user} shouldLazyLoadArtwork={i > 7} />
                </div>
            );
        });

        return (
            <div className="row">
                {items}
                <div className="column small-24">
                    {!items.length && !loading ? emptyState : null}
                    {loading ? loader : null}
                </div>
            </div>
        );
    }

    renderFeed(feed, loader, emptyState) {
        const { currentUser } = this.props;
        const items = feed.list.map((musicItem, i) => {
            const klass = classnames('column small-24 u-spacing-bottom-20', {
                'u-spacing-top-40': i === 0
            });

            return (
                <div className={klass} key={i}>
                    <MusicDetailContainer
                        className="user-profile__feed-item"
                        profile
                        shouldLinkArtwork
                        index={i}
                        shouldLazyLoadArtwork={i > 2}
                        item={musicItem}
                        musicList={feed.list}
                        shouldShowReup
                    />
                </div>
            );
        });

        let toggleSwitch;

        if (currentUser.isAdmin) {
            toggleSwitch = (
                <ToggleSwitch
                    className="exclude-toggle u-d-inline-block"
                    label="Exclude Re-Ups"
                    onChange={this.handleExcludeReupClick}
                    checked={feed.showOnlyUploads}
                    disabled={feed.loading}
                    showText
                />
            );
        }

        return (
            <Fragment>
                {toggleSwitch}
                {items}
                <div className="column small-24">
                    {!items.length && !feed.loading ? emptyState : null}
                    {feed.loading ? loader : null}
                </div>
            </Fragment>
        );
    }

    renderArtistAbout(artistProfile) {
        const fullBio = true;

        return (
            <ul className="user-profile__about column small-24">
                {this.renderArtistLabel(artistProfile)}
                {this.renderArtistHometown(artistProfile)}
                {this.renderArtistMemberSince(artistProfile)}
                {this.renderArtistBio(artistProfile, fullBio)}
            </ul>
        );
    }

    renderArtistBio(artist, fullBio = false) {
        if (!artist.bio) {
            return null;
        }

        if (fullBio) {
            return (
                <li>
                    <span className="artist-section__info-label">
                        Biography:
                    </span>
                    <span className="artist-section__info-value">
                        {artist.bio}
                    </span>
                </li>
            );
        }

        const ellipsis = (
            <span>
                …{' '}
                <Link
                    to={`/artist/${artist.url_slug}/about`}
                    className="user-profile__bio-more"
                >
                    Read more
                </Link>
            </span>
        );

        return (
            <Truncate
                className="user-profile__bio"
                lines={1}
                ellipsis={ellipsis}
                text={artist.bio}
                cutOffMoreCharacters={20}
            />
        );
    }

    renderContent(context, props, ownsPage) {
        const loader = <AndroidLoader className="music-feed__loader" />;

        switch (context) {
            case 'uploads': {
                const emptyState = this.renderEmptyState(context, ownsPage);
                // const list = ownsPage ? props.currentUserUploads.list : props.artistUploads.list;
                const list = props.artistUploads.list;
                // const loading = ownsPage ? props.currentUserUploads.loading : props.artistUploads.loading;
                const loading = props.artistUploads.loading;
                const pinned = props.artistPinned.list;

                return this.renderRecentMusic(
                    list,
                    pinned,
                    loading,
                    loader,
                    emptyState
                );
            }

            case 'favorites': {
                const emptyState = this.renderEmptyState(context, ownsPage);
                const list = props.artistFavorites.list;
                const loading = props.artistFavorites.loading;

                return this.renderFavorites(list, loading, loader, emptyState);
            }

            case 'playlists': {
                const emptyState = this.renderEmptyState(context, ownsPage);
                const list = ownsPage
                    ? props.currentUserPlaylists.list
                    : props.artistPlaylists.list;
                const loading = ownsPage
                    ? props.currentUserPlaylists.loading
                    : props.artistPlaylists.loading;

                return this.renderPlaylists(list, loading, loader, emptyState);
            }

            case 'followers': {
                const emptyState = this.renderEmptyState(context, ownsPage);

                return this.renderUserList(
                    props.artistFollowers.list,
                    'Followers',
                    props.artistFollowers.loading,
                    loader,
                    emptyState
                );
            }

            case 'following': {
                const emptyState = this.renderEmptyState(context, ownsPage);

                return this.renderUserList(
                    props.artistFollowing.list,
                    'Following',
                    props.artistFollowing.loading,
                    loader,
                    emptyState
                );
            }

            case 'feed': {
                const emptyState = this.renderEmptyState(context, ownsPage);
                const data = ownsPage
                    ? props.currentUserFeed
                    : props.artistFeed;

                return this.renderFeed(data, loader, emptyState);
            }

            case 'about': {
                return this.renderArtistAbout(props.artist.profile);
            }

            default:
                return null;
        }
    }

    renderBanner(user) {
        const { currentUser, artist } = this.props;
        const ownsPage =
            currentUser.isLoggedIn &&
            currentUser.profile.id === artist.profile.id;

        return (
            <ImageBanner
                banner={artist.profile.image_banner}
                defaultBanner={user.image}
                editLink={ownsPage ? '/edit/profile' : null}
            />
        );
    }

    render() {
        const { currentUser, artist, match } = this.props;

        // Wait until the fetch listed at the bottom of the file
        // grabs the user
        if (artist.loading) {
            return <AndroidLoader className="music-feed__loader" />;
        }

        if (!artist.profile) {
            if (artist.errors.length) {
                return <NotFound type="artist" />;
            }

            return null;
        }

        const ownsPage =
            currentUser.profile && currentUser.profile.id === artist.profile.id;
        const { context: paramContext } = match.params;
        const urlPrefix = `/artist/${artist.profile.url_slug}/`;

        if (
            paramContext === 'followers' &&
            (!ownsPage && !currentUser.isAdmin)
        ) {
            return <NotFound type="artist" />;
        }

        const context = paramContext || DEFAULT_CONTEXT;

        let { AdminControlsContainer, GhostClaimModal } = this.state;

        if (AdminControlsContainer) {
            AdminControlsContainer = <AdminControlsContainer artist={artist} />;
        }

        if (GhostClaimModal) {
            GhostClaimModal = <GhostClaimModal />;
        }

        const feedClass = classnames('user-profile__feed music-feed', {
            'user-profile__feed--fixed': this.state.navIsFixed
        });

        const klass = classnames('user-profile image-banner-page', {
            'image-banner-page--no-banner': !artist.profile.image_banner
        });

        return (
            <div className={klass}>
                <ArtistPageMeta
                    defaultContext={DEFAULT_CONTEXT}
                    artistProfile={artist.profile}
                    artistContext={context}
                    page={parseInt(match.params.page, 10) || 1}
                />
                {GhostClaimModal}
                {AdminControlsContainer}
                {this.renderBanner(artist.profile)}
                <div className="row">
                    <div className="column small-24">
                        <div
                            className="user-profile__main"
                            data-testid="userProfile"
                        >
                            <ArtistHeader artist={artist} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="column">
                        <FixOnScroll
                            className="user-profile__nav"
                            fixedClassName="user-profile__nav--fixed"
                            fixToId="main-header"
                            onFixChange={this.handleFixedNavChange}
                        >
                            {this.renderArtistNav(
                                context,
                                urlPrefix,
                                ownsPage,
                                artist.profile,
                                currentUser.profile
                            )}
                        </FixOnScroll>
                    </div>
                </div>
                <div className="row">
                    <div className="column">
                        <div className={feedClass}>
                            {this.renderContent(context, this.props, ownsPage)}
                        </div>
                    </div>
                </div>
                <BlockModal />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        currentUserUploads: state.currentUserUploads,
        currentUserFeed: state.currentUserFeed,
        currentUserPlaylists: state.currentUserPlaylists,
        ad: state.ad,
        modal: state.modal,
        artist: state.artist,
        artistFeed: state.artistFeed,
        artistUploads: state.artistUploads,
        artistFavorites: state.artistFavorites,
        artistPlaylists: state.artistPlaylists,
        artistFollowers: state.artistFollowers,
        artistFollowing: state.artistFollowing,
        artistPinned: state.artistPinned
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(ArtistPageContainer, [
        (params) => getArtist(params.artistId),
        (params) => getPinned(params.artistId),
        (params, query, props) => {
            const { currentUser } = props;

            const ownsPage =
                currentUser.isLoggedIn &&
                currentUser.profile.url_slug === params.artistId;

            return makeContextRequest(params, ownsPage);
        }
    ])
);
