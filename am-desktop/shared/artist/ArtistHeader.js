import React, { Component } from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Avatar from '../components/Avatar';
import FollowButtonContainer from '../components/FollowButtonContainer';
import SocialLinks from '../components/SocialLinks';
import Verified from '../../../am-shared/components/Verified';
import Truncate from 'components/Truncate';
import PlayIcon from '../icons/play';

export default class ArtistHeader extends Component {
    static propTypes = {
        artist: PropTypes.object.isRequired
    };

    renderAvatar(user) {
        return (
            <Avatar
                className="artist-page__avatar media-item__figure"
                image={user.image_base || user.image}
                type="artist"
                size={140}
                zoomable
                rounded
            />
        );
    }

    renderArtistBio(artist, fullBio = false) {
        if (!artist.bio) {
            return null;
        }

        if (fullBio) {
            return (
                <li>
                    <span className="artist-section__info-label">
                        Biography:
                    </span>
                    <span className="artist-section__info-value">
                        {artist.bio}
                    </span>
                </li>
            );
        }

        const ellipsis = (
            <span>
                …{' '}
                <Link
                    to={`/artist/${artist.url_slug}/about`}
                    className="user-profile__bio-more"
                >
                    Read more
                </Link>
            </span>
        );

        return (
            <Truncate
                className="user-profile__bio"
                lines={1}
                ellipsis={ellipsis}
                text={artist.bio}
                cutOffMoreCharacters={20}
            />
        );
    }

    renderArtistSocial(artist) {
        return (
            <SocialLinks
                className="u-inline-list u-list-reset user-profile__social-icons"
                links={artist}
                allowNonVerified
            />
        );
    }

    renderArtistStats(artist) {
        const plays = (artist.stats || {})['plays-raw'] || 0;

        const noSocial =
            [
                artist.url,
                artist.twitter,
                artist.facebook,
                artist.instagram,
                artist.youtube
            ].filter(Boolean).length === 0;

        if (!plays) {
            return null;
        }

        const playsClass = classnames('user-profile__stats u-ls-n-05', {
            'user-profile__stats--no-adjacent u-margin-0 u-padding-0': noSocial
        });

        return (
            <span className={playsClass}>
                <PlayIcon className="u-text-icon u-text-icon--small u-text-orange" />
                <strong>
                    Total Plays:{' '}
                    <span className="u-text-orange u-ls-n-05">
                        {plays.toLocaleString()}
                    </span>
                </strong>
            </span>
        );
    }

    render() {
        const { artist } = this.props;

        const check = <Verified user={artist.profile} size={19} />;

        return (
            <div className="row u-margin-0">
                <div className="column small-24 medium-17 small-text-center medium-text-left user-profile__main-info">
                    {this.renderAvatar(artist.profile)}
                    <div className="u-flex-grow">
                        <h1 className="user-profile__name">
                            {artist.profile.name} {check}
                        </h1>
                        {this.renderArtistBio(artist.profile)}
                        {this.renderArtistSocial(artist.profile)}
                        {this.renderArtistStats(artist.profile)}
                    </div>
                </div>
                <div className="column small-24 medium-7 small-text-center medium-text-right">
                    <FollowButtonContainer
                        className="user-profile__follow follow-button--large u-tt-uppercase"
                        artist={artist.profile}
                        showCount
                    />
                </div>
            </div>
        );
    }
}
