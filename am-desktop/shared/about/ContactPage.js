import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Dropdown from '../components/Dropdown';

export default class ContactPage extends Component {
    static propTypes = {
        onFormSubmit: PropTypes.func.isRequired,
        onContactLinkClick: PropTypes.func,
        onDropdownChange: PropTypes.func,
        inModal: PropTypes.bool,
        email: PropTypes.object,
        urlInputHidden: PropTypes.bool,
        urlInputRequired: PropTypes.bool,
        platformHidden: PropTypes.bool,
        issueInputHidden: PropTypes.bool,
        nameInputHidden: PropTypes.bool,
        phoneInputHidden: PropTypes.bool,
        companyInputHidden: PropTypes.bool,
        submitDisabled: PropTypes.bool,
        platform: PropTypes.string,
        issue: PropTypes.string,
        currentForm: PropTypes.string
    };

    static defaultProps = {
        inModal: false
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderUrlLink(urlInputHidden, urlInputRequired, currentForm) {
        if (urlInputHidden) {
            return null;
        }

        let placeholder = 'Affected URL';
        let pattern = null;
        let title = null;

        if (currentForm === '1') {
            placeholder = `${process.env.AM_URL}/song/artist/music-slug`;
            pattern = `^${process.env.AM_URL}.*`;
            title = `Must be an Audiomack URL i.e. ${placeholder}`;
        }

        return (
            <input
                type="text"
                name="url"
                placeholder={placeholder}
                pattern={pattern}
                title={title}
                required={urlInputRequired}
            />
        );
    }

    renderError(error, key) {
        if (!error) {
            return null;
        }

        if (error.message) {
            return (
                <p className="auth__error" key={key}>
                    {error.message}
                </p>
            );
        }

        return (
            <p className="auth__error" key={key}>
                {error}
            </p>
        );
    }

    render() {
        const { email, inModal, submitDisabled } = this.props;

        const selectOptions = [
            { value: '', text: '-- Concerning --' },
            // { value: 1, text: 'Trending Submission' },
            // { value: 2, text: 'Business Development' },
            { value: 3, text: 'Advertising Placement' },
            // { value: 4, text: 'Enable Follow to Download' }, // f2d Removed for now
            { value: 5, text: '"AMP" Application' }
            // { value: 6, text: 'API Access Request' }
            // { value: 7, text: 'Support' }
        ];

        const issueOptions = [
            { value: '', text: '-- Issue --' },
            { value: 0, text: 'Music not Playing' },
            { value: 1, text: 'Uploading Albums / Songs' },
            { value: 2, text: 'URL Slug Change Request' },
            { value: 3, text: 'Issue with Account' },
            { value: 4, text: 'Search Results' },
            { value: 5, text: 'Other' }
        ];

        const platformOptions = [
            { value: '', text: '--Please Select a Platform--' },
            { value: 0, text: 'iOS App' },
            { value: 1, text: 'Android App' },
            { value: 2, text: 'Mobile Website' },
            { value: 3, text: 'Desktop Website' }
        ];

        let platformDropdown;

        if (!this.props.platformHidden) {
            platformDropdown = (
                <div>
                    <Dropdown
                        options={platformOptions}
                        name="platform"
                        value={this.props.platform}
                        onChange={this.props.onDropdownChange}
                    />
                </div>
            );
        }

        let issueDropdown;

        if (!this.props.issueInputHidden) {
            issueDropdown = (
                <div>
                    <Dropdown
                        options={issueOptions}
                        name="issue"
                        value={this.props.issue}
                        onChange={this.props.onDropdownChange}
                    />
                </div>
            );
        }

        const error = email.errors[0];

        let placeholder = 'Message';

        if (this.props.currentForm === '6') {
            placeholder = 'How will you use this API?';
        }

        let title;

        if (!inModal) {
            title = <h2 className="auth__title">Contact Us</h2>;
        }

        // const talkbot = '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"><title>Audiomack Support</title><style type="text/css">body,html {margin: 0;padding: 0;height: 100%;overflow: hidden;}.tars-fullpage-container-class {position: absolute;left: 0;right: 0;bottom: 0;top: 0;border: 0;}</style></head><body><div id="tars-fullpage-container" class="tars-fullpage-container-class"><iframe id="tars-fullpage" width="100%" height="100%" frameborder="0" src="https://audiomack.hellotars.com/conv/BJ7m3G/"></iframe><script type="text/javascript" src="https://tars-file-upload.s3.amazonaws.com/share/js/fullpage.js"></script></div>'

        let disaledMessage;
        if (submitDisabled) {
            disaledMessage = (
                <p className="u-text-red" style={{ margin: '-10px 0 15px' }}>
                    You must be logged in to apply for "AMP".
                </p>
            );
        }

        return (
            <div className="row">
                <div className="column small-24">
                    {title}
                    <form className="form" onSubmit={this.props.onFormSubmit}>
                        <div>
                            <Dropdown
                                options={selectOptions}
                                name="type"
                                value={this.props.currentForm}
                                onChange={this.props.onDropdownChange}
                            />
                        </div>
                        {disaledMessage}
                        {platformDropdown}
                        {this.renderUrlLink(
                            this.props.urlInputHidden,
                            this.props.urlInputRequired,
                            this.props.currentForm
                        )}
                        <div className="form__input-group u-group">
                            <div className="form__input form__input">
                                <input
                                    required
                                    type="email"
                                    name="email"
                                    placeholder="Your Email"
                                    autoCapitalize="none"
                                />
                            </div>
                        </div>
                        <input
                            hidden={this.props.nameInputHidden}
                            type="text"
                            name="name"
                            placeholder="Full Name"
                        />
                        <input
                            hidden={this.props.companyInputHidden}
                            type="text"
                            name="company"
                            placeholder="Company Name"
                        />
                        <input
                            hidden={this.props.phoneInputHidden}
                            type="text"
                            name="phone"
                            placeholder="Phone Number"
                        />
                        {issueDropdown}
                        <div>
                            <textarea
                                name="message"
                                placeholder={placeholder}
                                required
                            />
                        </div>
                        <div>{this.renderError(error)}</div>
                        <div className="auth__submit-wrap">
                            <input
                                className="button button--pill auth__submit auth__btn"
                                type="submit"
                                disabled={email.loading || submitDisabled}
                                value={email.loading ? 'Sending...' : 'Submit'}
                            />
                        </div>
                    </form>
                </div>
                <div className="modal__footer column small-24 u-text-center">
                    <p>
                        Have a DMCA request?{' '}
                        <Link
                            to="/about/legal"
                            onClick={this.props.onContactLinkClick}
                            data-page="legal"
                        >
                            Submit here
                        </Link>
                    </p>
                </div>
            </div>
        );
    }
}
