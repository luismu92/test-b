import React, { Component } from 'react';

export default class WordpressPageContainer extends Component {
    static propTypes = {};

    render() {
        return (
            <div className="row music-feed">
                <div className="column body-text">
                    <div className="page-content entry-content">
                        <img
                            src="/static/images/desktop/wordpress-plugin/wordpress-logo.gif"
                            alt=""
                        />
                        <p>
                            Get the audiomack plugin for{' '}
                            <strong>Wordpress.org</strong> and easily embed our
                            audio player in your website or blog! With the
                            Audiomack Wordpress plugin you can customize your
                            player colors, search for songs on Audiomack, and
                            easily embed players using wordpress shortcodes
                        </p>
                        <p>
                            If your site is hosted on{' '}
                            <strong>Wordpress.com</strong>, simply enter in the
                            url of the song or album you want to embed and it
                            will appear.
                        </p>
                        <div>
                            <a
                                href="https://wordpress.org/plugins/audiomack/"
                                className="button"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                Get the Audiomack Plugin for Wordpress
                            </a>
                        </div>
                        <div className="testititle">
                            <h3>Video Demo</h3>
                        </div>
                        <iframe
                            title="video demo"
                            width="640"
                            height="400"
                            src="//www.youtube.com/embed/XD0_oLKqGkc"
                            frameBorder="0"
                            allowFullScreen
                        />
                        <div>
                            <a
                                href="https://wordpress.org/plugins/audiomack/"
                                className="button"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                Get the Audiomack Plugin for Wordpress
                            </a>
                        </div>
                        <div className="testititle">
                            <h3>Screenshots</h3>
                        </div>
                        <p>
                            Shortcodes will be converted into embedded players
                            in your post content:
                            <br />
                            <br />
                            <img
                                src="/static/images/desktop/wordpress-plugin/screenshot-2.png"
                                alt=""
                            />
                        </p>
                        <p>
                            Simply click the Audiomack icon to begin!
                            <br />
                            <br />
                            <img
                                src="/static/images/desktop/wordpress-plugin/screenshot-3.png"
                                alt=""
                            />
                        </p>
                        <div>
                            <a
                                href="https://wordpress.org/plugins/audiomack/"
                                className="button"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                Get the Audiomack Plugin for Wordpress
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
