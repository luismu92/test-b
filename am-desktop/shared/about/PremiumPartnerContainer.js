import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Helmet from 'react-helmet';

export default class PremiumPartnerPageContainer extends Component {
    render() {
        const title = 'Premium Partner Agreement';

        return (
            <div className="body-text premium-partner-page">
                <Helmet>
                    <title>{title}</title>
                </Helmet>

                <h2>Audiomack Premium Partner Agreement</h2>

                <p>
                    This AGREEMENT (the “Agreement") is between you (hereby
                    referred to as “Rightsholder” or in some instances “their”)
                    and Audiomack Inc., 648 Broadway, Suite 302, New York, NY
                    10012 (hereby referred to as "Audiomack"). This Agreement
                    contains the general terms and conditions under which
                    Audiomack offers the Services. In agreeing to the terms of
                    this Agreement and freely choosing to become an Audiomack
                    Premium Partner, by submitting sound recordings (and the
                    musical works embodied therein) for monetized streaming on
                    Audiomack (whether by upload to the Audiomack website
                    (www.Audiomack.com) (the "Website") or through the Audiomack
                    App (the “App”)). Rightsholder constitutes their agreement
                    to and acceptance of this document and any applicable
                    Addendum.
                </p>

                <p>
                    Audiomack reserves the right to add, delete change, replace,
                    amend and or otherwise modify any of the terms and
                    conditions contained in this Agreement (specifically
                    excluding Section 3 below; Audiomack may not add, delete
                    change, replace, amend and or otherwise modify any part of
                    Section 3 without specifically amending this Agreement
                    directly with Rightsholder) as Audiomack may deem necessary
                    from time to time in their sole discretion. Notwithstanding
                    the preceding sentence, no such modifications to this
                    Agreement will apply to any dispute between Rightsholder and
                    Audiomack that arose prior to the date of such modification.
                    In the event of any material changes to the terms of this
                    Agreement, Audiomack will notify Rightsholder by email as
                    set forth in Section 9 herein. If any such modification is
                    unacceptable to Rightsholder, their only and sole recourse
                    is to discontinue use of the Services. Rightsholder’s
                    continued use of the Services following posting of a change
                    notice or new agreement on the Site or notice to
                    Rightsholder through e-mail, will constitute their binding
                    acceptance of the changes. For purposes of this Agreement
                    and whenever the context requires the singular number shall
                    include the plural, and vice versa.
                </p>

                <p>
                    RIGHTSHOLDER IS RESPONSIBLE FOR MAINTAINING A VALID E-MAIL
                    ADDRESS ON FILE WITH AUDIOMACK FOR SO LONG AS RIGHTSHOLDER
                    AVAILS THEMSELF OF ANY SERVICES. A FAILURE TO MAINTAIN A
                    VALID E-MAIL ADDRESS SHALL NOT BE A DEFENSE IN THE EVENT
                    RIGHTSHOLDER DOES NOT RECEIVE AN UPDATE OR NOTIFICATION FROM
                    AUDIOMACK FOR ANY AND ALL REASONS.
                </p>

                <p>
                    All capitalized terms used herein shall have the meaning
                    ascribed to such term as described in Section 22 of this
                    Agreement or as otherwise noted in this Agreement.
                </p>

                <ol>
                    <li>
                        <strong>Terms of Service:</strong> This Agreement is
                        incorporated by reference in its entirety into and
                        subject to the Terms of Service solely as they may apply
                        to parties to this agreement, and the Terms of Service
                        are incorporated by reference in their entirety into and
                        subject to this Agreement (
                        <Link to="/about/terms-of-service">
                            https://audiomack.com/about/terms-of-service
                        </Link>
                        ) ("TOS").
                    </li>
                    <li>
                        <strong>Authorization:</strong> Rightsholder hereby
                        appoints Audiomack as their authorized representative
                        for Advertisement(s) in connection with the content
                        Rightsholder uploads or otherwise delivers to Audiomack
                        (“Rightsholders Content”). Accordingly, Rightsholder
                        does hereby grant to Audiomack and to Audiomack’s
                        “Licensees” during the Term (as defined in Section 22
                        herein) of this Agreement the non-exclusive right
                        throughout the “Authorized Territory”, to:
                        <ol>
                            <li>
                                Reproduce and create derivative works of
                                Rightsholder’s Content by converting their
                                Content into Digital Masters, including less
                                than full-length versions of sound recordings
                                (“Clips”) that can be used for promotional
                                purposes as authorized herein.
                            </li>
                            <li>
                                Publicly perform, publicly display, communicate
                                to the public, and otherwise make available
                                Rightsholder’s Content, and portions thereof as
                                embodied in Clips, by means of digital audio
                                transmissions (on an interactive or
                                non-interactive basis) through the Website, a
                                Licensee website, Audiomack’s API or through the
                                App.
                            </li>
                            <li>
                                Place or embed Rightsholder’s Content in any
                                form of media now or hereafter created in
                                connection with the Services and/or Audiomack’s
                                API. Specifically, this does not include other
                                platforms or services, including synchronization
                                or reproduction, licensing in connection with
                                the use of music in advertisements and on
                                television, video and motion picture
                                soundtracks, video and electronic games, and
                                cellular ringtones.
                            </li>
                            <li>
                                Use and distribute Copyright Management
                                Information as embodied in a Digital Master of
                                Rightsholder’s Content.
                            </li>
                            <li>
                                Use Rightsholder’s Content and metadata as may
                                be reasonably necessary or desirable for
                                Audiomack to exercise Audiomack’s rights under
                                this Agreement.
                            </li>
                            <li>
                                Authorize Audiomack’s Licensees to perform any
                                one or more of the activities specified above or
                                in an applicable Addendum.
                            </li>
                            <li>
                                Include a reference to Rightsholder and
                                Rightsholder’s logo on its Website, a Licensee
                                website, or through the App and in marketing
                                literature, labeling and in promotional
                                materials. Each party may also, subject to the
                                other’s written consent, which shall not be
                                unreasonably withheld, issue press releases
                                regarding this Agreement. Either party may
                                re-publish information contained in a previously
                                approved press release, including on a party’s
                                website and in marketing materials.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Compensation:</strong> Conditioned upon
                        Rightsholder’s performance of all of the material terms
                        and conditions hereof and specifically in consideration
                        for Rightsholder’s Authorization (as described above),
                        Audiomack shall pay Rightsholder or a designee of theirs
                        given to Audiomack by Rightsholder the Amp Revenue Share
                        generated in connection with the streaming of
                        Rightsholder’s Monetized Content on the Website, a
                        Licensee website, or through the App (altogether such
                        payment due to Rightsholder shall be considered the
                        “Compensation”). In the case of entities that are
                        legally authorized to enter into this Agreement on
                        behalf of one or more owners of content, such as record
                        labels, while said entity shall have the option whether
                        to maintain a separate Account for each rightsholder on
                        whose behalf this Agreement is entered into,
                        Compensation shall be paid to Rightsholder by Audiomack
                        in the aggregate with respect to all of Rightsholder’s
                        Audiomack accounts.
                        <br />
                        The total Compensation for Monetized Content is
                        calculated as follows:
                        <ol>
                            <li>
                                Audiomack will calculate “Revenue Per Stream” (A
                                “Stream” being content played for Thirty (30)
                                seconds or longer, for the complete definition
                                of “Stream” please see Section 22(k) below) by
                                taking the sum of all monies received by
                                Audiomack from all Advertisements (minus Actual
                                Costs) and all Subscriptions (minus all Actual
                                Costs) and dividing that number by the number of
                                total Stream’s on the Audiomack platform in a
                                calendar month.
                            </li>
                            <li>
                                Rightsholder will be paid based on the number of
                                Streams of solely Rightsholder’s Monetized
                                Content multiplied by the Revenue Per Stream
                                which is then multiplied by the AMP Revenue
                                Share.
                            </li>
                            <li>
                                The “Amp Revenue Share” is Fifty (50%) Percent.
                                <ol>
                                    <li>
                                        For avoidance of doubt, if Audiomack
                                        generates the sum of Ten Thousand
                                        ($10,000.00) Dollars from Advertisements
                                        and Subscriptions in a given calendar
                                        month (including deductions for Actual
                                        Costs) and the total number of Streams
                                        on Audiomack for that given month is Ten
                                        Million (10,000,000) Streams, the
                                        Revenue Per Stream for that given month
                                        shall be one tenth of one cent ($0.001).
                                    </li>
                                    <li>
                                        If in that same given month, the number
                                        of Streams of solely Rightsholder’s
                                        Monetized Content is One Hundred
                                        Thousand (100,000), then to calculate
                                        Rightsholder’s particular Amp Revenue
                                        Share it is necessary to multiply the
                                        Revenue Per Stream (.001) by the number
                                        of streams of Rightsholder’s Monetized
                                        Content (100,000). In the case of this
                                        example, Rightsholder’s Revenue Per
                                        Stream would be One Hundred ($100.00)
                                        Dollars.
                                    </li>
                                    <li>
                                        From that point, Rightsholder’s Amp
                                        Revenue Share would be Fifty (50%)
                                        Percent of One Hundred ($100.00)
                                        Dollars, which would entitle
                                        Rightsholder to the sum of Fifty
                                        ($50.00) Dollars as Rightsholder’s
                                        Compensation for that given month.
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <strong>Other Royalties:</strong> In addition to
                                the Compensation due to Rightsholder as
                                described above, by virtue of Rightsholder’s
                                Content being available through the Services and
                                Rightsholder’s performance of all of the
                                material terms and conditions hereof and
                                specifically in consideration for Rightsholder’s
                                Authorization (as described above), Audiomack
                                shall use its it’s best efforts to enter into
                                agreements with Rightsholder’s affiliated PRO
                                and Mechanical Royalties organizations for the
                                administration and Rightsholder’s part of
                                royalty payments for the public performance of
                                Rightsholder’s sound recordings or musical
                                works. In order to become eligible through
                                Audiomack to pay Rightsholder any royalties
                                through Rightsholder’s PRO, Audiomack requires
                                the following information from Rightsholder for
                                Monetized content:
                                <ol>
                                    <li>
                                        International Standard Recording Code
                                        “ISRC”
                                    </li>
                                    <li>
                                        International Standard Musical Word Code
                                        “ISWC”
                                    </li>
                                    <li>
                                        Universal Product Code “UPC” (If
                                        applicable)
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <strong>Reporting and Payment:</strong> Within
                                One Hundred (100) days of the end of each
                                calendar month, Audiomack shall provide
                                Rightsholder with a report detailing the Amp
                                Revenue Share due from Rightsholder’s Content
                                based on Audiomack’s reasonable calculations and
                                in accordance with the parameters set forth on
                                Audiomack’s Website as may be amended from time
                                to time. Within One Hundred and Thirty (130)
                                days of the end of each calendar month,
                                Audiomack shall transfer the Compensation due to
                                Rightsholder in connection with Rightsholder’s
                                Content to Rightsholder or Rightsholder’s
                                designee in Dollars, less any applicable value
                                added or similar tax appropriately charged to
                                Rightsholder’s share of the Amp Revenue Share,
                                to a designated Bank Account or PayPal account
                                provided by Rightsholder or Rightsholder’s
                                designee.
                                <ol>
                                    <li>
                                        <strong>Payout Threshold:</strong> If
                                        the amount payable to Rightsholder
                                        hereunder for any calendar month does
                                        not equal at least One-Hundred Dollars
                                        ($100.00 USD), then no amounts shall be
                                        payable to Rightsholder until the
                                        aggregate amount due to Rightsholder
                                        after the conclusion of a calendar month
                                        equals at least One-Hundred Dollars
                                        ($100.00 USD) (the “Threshold Streaming
                                        Payout Amount”). If Audiomack is
                                        required by applicable law, Audiomack
                                        shall remit to the proper taxing
                                        authorities any and all taxes withheld
                                        from the payments to Rightsholder. In
                                        the case of entities that are legally
                                        authorized to enter into this Agreement
                                        on behalf of one or more owners of
                                        content, such as record labels, while
                                        said entity shall have the option
                                        whether to maintain a separate Account
                                        for each content owner on whose behalf
                                        this Agreement is entered into, the
                                        Threshold Streaming Payout Amount shall
                                        be accounted for and paid to
                                        Rightsholder by Audiomack in the
                                        aggregate with respect to all of
                                        Rightsholder’s Audiomack accounts.
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <strong>Track Listing:</strong> The track(s)
                                that shall comprise Rightsholder’s Content in
                                this Agreement and be eligible for Compensation
                                are all tracks uploaded on to any of
                                Rightsholder’s Audiomack accounts during the
                                Term.
                            </li>
                            <li>
                                <strong>Licensee Records:</strong> Audiomack
                                may, but need not, audit the books and records
                                of Licensees and may accept any representations
                                made in a Licensee accounting statement
                                delivered to Audiomack as true and complete.
                                Audiomack shall have no liability to
                                Rightsholder for failure to audit or investigate
                                any accountings rendered to it by any Licensees.
                            </li>
                            <li>
                                <strong>Recordkeeping and Audits:</strong>{' '}
                                Audiomack will maintain books and records which
                                report the number of streams or other licensed
                                uses of Rightsholder’s Content. Rightsholder
                                may, but not more than once a year, at their own
                                expense, engage a Certified Public Accountant
                                (“CPA”) to examine those books and records
                                directly related to the streaming royalties or
                                other licensed uses of Rightsholder’s Content,
                                as provided in this Agreement. Rightsholder may
                                have their CPA make those examinations only for
                                the purpose of verifying the accuracy of the
                                statements sent to Rightsholder. All such
                                examinations will be in accordance with
                                generally accepted accounting principles
                                (“GAAP”) procedures and regulations.
                                <br />
                                Rightsholder’s CPA may make such an examination
                                for a particular statement only once, and only
                                within one (1) year after the date Audiomack
                                sends the Rightsholder that statement.
                                Rightsholder’s CPA may make such an examination
                                only during Audiomack’s usual business hours,
                                and only at the place where such books and
                                records are maintained in the ordinary course of
                                business. Rightsholder must provide Audiomack
                                with thirty (30) days written notice prior to
                                commencing an audit and must identify the name,
                                address, telephone number, and email address of
                                the CPA conducting the audit on Rightsholder’s
                                behalf. Rightsholder may not engage the CPA on a
                                contingent fee basis (i.e., Rightsholder’s CPA
                                must be paid on a flat fee or time-based basis).
                                Audiomack may postpone the commencement of
                                Rightsholder’s CPA’s examination by notice given
                                to Rightsholder not later than five (5) days
                                before the commencement date specified in
                                Rightsholder’s notice. In the event of any
                                postponement initiated by Audiomack, the running
                                of the time within which the examination may be
                                made will be suspended during the postponement.
                                If Rightsholder’s CPA’s examination has not been
                                completed within three (3) months from the time
                                commenced, then Audiomack may require
                                Rightsholder to terminate the audit upon seven
                                (7) days’ notice, which notice may be given at
                                any time. Audiomack will not be required to
                                permit the CPA to continue any examination after
                                the end of that seven (7) day period.
                                <br />
                                Rightsholder’s CPA will not be entitled to
                                examine any other records that do not
                                specifically report sales or other licensed uses
                                of Rightsholder’s Content for which Audiomack
                                has actually received payment. Rightsholder’s
                                CPA may act only under an acceptable
                                confidentiality agreement, which provides that
                                any information derived from such audit or
                                examination on Rightsholder’s behalf will not be
                                knowingly released, divulged, published or
                                shared with any other person, firm or
                                corporation, other than to Rightsholder or to a
                                judicial or administrative body in connection
                                with any proceeding relating to this Agreement.
                                Rightsholder’s CPA may not share the results of
                                the examination conducted on Rightsholder’s
                                behalf with any third party without Audiomack’s
                                express written permission.
                            </li>
                            <li>
                                <strong>Objections to Accountings:</strong> If
                                Rightsholder has any objections to an Audiomack
                                accounting statement made available to them,
                                Rightsholder agrees to provide Audiomack
                                specific written notice of that objection,
                                including a copy of Rightsholder’s CPA’s
                                analysis of the accounting statement, and their
                                reasons for it within eighteen (18) months after
                                the date Audiomack send or make that statement
                                available to Rightsholder. Each statement shall
                                become conclusively binding on Rightsholder at
                                the end of that eighteen-month period, and
                                Rightsholder will no longer have any right to
                                make any other objections to it notwithstanding
                                any audit rights Rightsholder may otherwise have
                                under any applicable law or regulation. Any
                                payments determined to be owed Rightsholder
                                following an audit shall be paid within
                                forty-five (45) days of the delivery of
                                Rightsholder’s CPA’s audit report, unless
                                objected to in writing by Audiomack, in which
                                case any payments due shall be postponed pending
                                the resolution of the audit dispute. A late fee
                                of one-half percent (0.5%) shall be due for
                                underpaid streaming royalties. Unless otherwise
                                prohibited by law, Rightsholder will not have
                                the right to sue Audiomack in connection with
                                any statement, or to sue Audiomack for unpaid
                                royalties for the period a statement covers,
                                unless Rightsholder commences the suit within
                                that eighteen (18)-month period. If Rightsholder
                                commences suit on any controversy or claim
                                concerning statements rendered to Rightsholder
                                under this Agreement in a court of competent
                                jurisdiction, the scope of the proceeding will
                                be limited to a determination of the amount of
                                royalties due for the accounting periods
                                concerned, and the court shall have no authority
                                to consider any other issues or award any relief
                                except recovery of any royalties found owing,
                                plus interest at the rate of one-half percent
                                (0.5%) per month. Rightsholder’s recovery of any
                                such royalties plus interest shall be the sole
                                remedy available to Rightsholder by reason of
                                any claim related to Audiomack’s statements.
                            </li>
                            <li>
                                <strong>Tax Information:</strong> Audiomack will
                                use its reasonable efforts to collect sales and
                                other taxes owed on the streaming of
                                Rightsholder’s Content (“Streaming Tax”), and to
                                remit such Streaming Tax on Rightsholder’s
                                behalf to relevant government authorities.
                                Notwithstanding the above, in all events,
                                Rightsholder acknowledges and agrees that they
                                are ultimately responsible for the payment of
                                any Streaming Tax owed in connection with the
                                sale or distribution of Rightsholder’s Content
                                pursuant to this Agreement, and Rightsholder
                                hereby indemnify Audiomack for any Streaming Tax
                                that may be owed in addition to those amounts
                                collected and remitted on Rightsholder’s behalf
                                by Audiomack.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Term:</strong> The term of this Agreement is the
                        date it is accepted (“Effective Date”) by an authorized
                        representative of Rightsholder and, unless terminated in
                        accordance with the termination provisions provided
                        herein, shall continue for a period of twelve (12)
                        months (the "Initial Term"). Following the Initial Term,
                        unless written notice of intent not to renew or
                        terminate is provided by either party at least thirty
                        (30) days prior to the scheduled expiration date of the
                        then current term, this Agreement shall be automatically
                        renewed on an annual basis for successive one (1) year
                        additional terms. This Agreement will continue until
                        terminated by either Rightsholder or Audiomack, upon
                        three (3) business days’ written notice (the “Term”),
                        which notice.
                        <ol>
                            <li>
                                If sent by Audiomack, may be sent to
                                Rightsholder at the last e-mail address
                                Rightsholder provided to Audiomack.
                            </li>
                            <li>
                                If sent by Rightsholder to Audiomack, must be
                                sent only to the following email address:{' '}
                                <a href="mailto:finance@audiomack.com">
                                    finance@audiomack.com
                                </a>{' '}
                                with the following information:
                                <ol>
                                    <li>Rightsholder’s username</li>
                                    <li>
                                        The email address associated with
                                        Rightsholder’s Account
                                    </li>
                                    <li>
                                        All album or song titles for which
                                        Rightsholder is requesting termination
                                    </li>
                                </ol>
                            </li>
                            <li>
                                Any Termination Notice provided by Rightsholder
                                pursuant to this Agreement shall be permanent
                                and irreversible. Notwithstanding anything to
                                the contrary herein, Audiomack may at any time
                                in its sole discretion, with or without notice
                                to Rightsholder:
                                <ol>
                                    <li>
                                        Suspend or limit Rightsholder’s access
                                        to or Rightsholder’s use of the Services
                                        and/or
                                    </li>
                                    <li>
                                        Suspend or limit Rightsholder’s access
                                        to Rightsholder’s Account.
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Rightsholder’s Obligations:</strong>{' '}
                        Rightsholder, or an authorized representative acting on
                        Rightsholder’s behalf will be responsible for obtaining
                        and paying for any and all clearances or licenses
                        required in the Authorized Territory (or any portion
                        thereof) for the use of any musical works embodied in
                        Rightsholder’s Content, as well as:
                        <ol>
                            <li>
                                <strong>Parental Advisory Labeling:</strong>{' '}
                                Rightsholder will be responsible for complying
                                with the Recording Industry Association of
                                America’s (“RIAA”) Parental Advisory Logo
                                (“PAL”) Standards, as applicable, for so long as
                                Rightsholder uses the Services.
                            </li>
                            <li>
                                <strong>Content Sharing:</strong> As part of
                                this Agreement, the Rightsholder agrees to use
                                best efforts to upload all content created or
                                licensed by Rightsholder and uploaded by
                                Rightsholder to other free streaming platforms
                                (For example YouTube, Soundcloud, etc.).
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Right to Withdraw Material;</strong> Termination
                        of Authorizations to Licensees: Rightsholder has the
                        right, at any time during the Term, to withdraw the
                        authorizations set forth in Section 2 above, for any and
                        all uses of all or any portion of Rightsholder’s
                        Content, upon written notice to Audiomack (a “Withdrawal
                        Notice”) or to terminate this Agreement pursuant to
                        Section 4 of this Agreement (a “Termination Notice”).
                        <br />
                        Upon receipt of a Withdrawal Notice with respect to any
                        of Rightsholder’s Content or a Termination Notice with
                        respect to all of Rightsholder’s Content, Audiomack will
                        promptly remove those elements of Rightsholder’s Content
                        covered by such Withdrawal Notice or Termination Notice,
                        as the case may be, from the Website, a Licensee
                        website, or through the App (and in no event more than
                        five (5) business days following receipt of a Withdrawal
                        Notice or Termination Notice). Rightsholder’s submission
                        of a Withdrawal Notice or Termination Notice shall not
                        in any way limit the authorizations granted to Audiomack
                        or any Licensees prior to the implementation of
                        Rightsholder’s Withdrawal Notice or Termination Notice.
                    </li>
                    <li>
                        <strong>Names and Likenesses;</strong> Promotional Use
                        and Opportunities: Rightsholder hereby grants to
                        Audiomack during the Term the right to use and to
                        authorize Audiomack’s Licensees to use the names and
                        approved likenesses of, and biographical material
                        concerning, any of Rightsholder’s , bands, producers
                        and/or songwriters, as well as track and/or album name,
                        and all artwork related to Rightsholder’s Content, in
                        any marketing materials for the promotion, and
                        advertising of Rightsholder’s Content, which is offered
                        to stream or for any other use under the terms of this
                        Agreement (e.g., an artist or band name and likeness may
                        be used in an informational fashion, such as by textual
                        displays or other informational passages, to identify
                        and represent authorship, production credits, and
                        performances of the applicable artist or band in
                        connection with the exploitation of Rightsholder’s
                        Content).
                        <ol>
                            <li>
                                <strong>Listener Information:</strong> Audiomack
                                may, from time to time, provide Rightsholder
                                with information relating to customers that
                                stream Rightsholder’s Content, subject to
                                Audiomack’s privacy policy and the preferences
                                of Audiomack’s customers. Rightsholder may only
                                use, and disclose this information to a third
                                party, for Rightsholder’s internal record
                                keeping purposes. Rightsholder may not disclose
                                any of this information to a third party or use
                                it for any other purpose. In all events,
                                Rightsholder will comply with the terms of
                                Audiomack’s privacy policy{' '}
                                <Link to="/privacy-policy">
                                    http://www.audiomack.com/privacy-policy
                                </Link>{' '}
                                in connection with the customer data provided
                                under this Agreement.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Ownership:</strong> Subject to Audiomack’s
                        rights hereunder or under any prior agreement between
                        Rightsholder and Audiomack, as between Rightsholder and
                        Audiomack, all right, title, and interest in and to (a)
                        Rightsholder’s Content, (b) the Digital Masters, (c) the
                        Clips, (d) the Physical Product (if applicable), (e) all
                        copyrights and equivalent rights embodied therein, and
                        (f) all materials furnished by Rightsholder that were
                        Rightsholder’s prior to this Agreement will remain
                        unchanged by entering into this Agreement and as far as
                        Audiomack is concerned shall remain Rightsholders.
                    </li>
                    <li>
                        <strong>
                            Modification, Termination and Effect of Termination:
                        </strong>
                        <ol>
                            <li>
                                <strong>Modification of Agreement:</strong>{' '}
                                Audiomack reserves the right to change, modify,
                                add to, delete, amend, replace or remove all or
                                any part of this Agreement, as Audiomack may
                                deem necessary in Audiomack’s sole discretion,
                                at any time and from time to time. Notice of any
                                material change will be sent to Rightsholder by
                                electronic mail at least thirty (30) days prior
                                to its effective date. If the e-mail
                                Rightsholder has provided to Audiomack is no
                                longer valid or functioning, then, in addition
                                to any other remedies Audiomack may have with
                                respect to Rightsholder’s Account and use of the
                                Services, Audiomack shall be authorized to
                                communicate with Rightsholder via any other
                                reasonable manner Audiomack may choose in
                                Audiomack’s sole discretion, including through
                                notice on the web page through which
                                Rightsholder accesses their account information
                                or via any accounting statement. The most recent
                                date of this Agreement shall be identified on
                                the first page hereof. In the event that
                                Rightsholder does not consent to any such
                                proposed changes in the Agreement,
                                Rightsholder’s sole recourse shall be to
                                terminate this Agreement by notice to us, and
                                Rightsholder’s failure to submit a Termination
                                Notice within thirty (30) days of the date of
                                Audiomack’s notice to Rightsholder shall
                                constitute their acceptance of such changes to
                                the extent Rightsholder’s Content is still
                                available to stream through the Services. To
                                terminate Rightsholder’s Agreement, Rightsholder
                                must send a Termination Notice to{' '}
                                <a href="mailto:Finance@Audiomack.com">
                                    Finance@Audiomack.com
                                </a>{' '}
                                and include in the subject line of
                                Rightsholder’s e-mail “Termination of Premium
                                Partner Agreement.”
                            </li>
                            <li>
                                <strong>Consequences of Termination.</strong>{' '}
                                The expiration or termination of the Agreement
                                will not relieve either Rightsholder or
                                Audiomack from Audiomack’s respective
                                obligations incurred prior to the effective date
                                of Rightsholder’s termination of the Agreement.
                                In addition, provisions of this Agreement
                                intended to survive the termination of this
                                Agreement shall survive termination.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>
                            Monitoring of Rightsholder’s Content; Removal of
                            Content from Website:
                        </strong>
                        <ol>
                            <li>
                                <strong>Monitoring:</strong> Audiomack does not
                                control Rightsholder’s Content and does not have
                                any obligation to monitor Rightsholder’s Content
                                for any purpose. Audiomack may choose, in its
                                sole discretion, to monitor, review or otherwise
                                access some or all of Rightsholder’s Content,
                                but by doing so Audiomack assumes no
                                responsibility for Rightsholder’s Content, no
                                obligation to modify or remove any inappropriate
                                elements of Rightsholder’s Content, or to
                                monitor, review or otherwise access any other
                                rightsholder’s content or artwork.
                            </li>
                            <li>
                                <strong>Right of Removal.</strong> Audiomack
                                reserves the right, in its sole and absolute
                                discretion, to remove any of Rightsholder’s
                                Content from the Website if such content: (i) is
                                patently offensive, pornographic or defamatory;
                                (ii) is the subject of a dispute between
                                Rightsholder or Audiomack and a third party;
                                (iii) is content to which Rightsholder cannot
                                document their rights therein upon Audiomack’s
                                request; (iv) violates the intellectual property
                                rights or other protected interests of a third
                                party; (v) is the subject of a takedown notice
                                by a party claiming to own the rights therein,
                                or (vi) is the subject of any fraudulent
                                activity, or for any other reason in Audiomack’s
                                sole and absolute judgment is necessary to
                                protect the business interests of Audiomack and
                                any of its business partners or Licensees.
                                Audiomack may also remove Rightsholder’s Content
                                from the Website if Rightsholder is abusive or
                                rude or provide false or intentionally
                                misleading information to any Audiomack
                                employees or agents. Audiomack shall have no
                                liability to Rightsholder for the removal of any
                                of their Content from the Website or any
                                Licensee website or service other than to
                                provide Rightsholder a credit (but not a refund)
                                for any fees previously paid by Rightsholder for
                                making Rightsholder’s Content available through
                                the Website or through Licensees. The removal of
                                any of Rightsholder’s Content shall not relieve
                                Audiomack of the obligation to pay Rightsholder
                                any streaming royalties that may have accrued
                                prior to the removal of Rightsholder’s Content.
                            </li>
                            <li>
                                <strong>No Termination Due to Removal.</strong>{' '}
                                This Agreement shall not be terminated
                                automatically by Audiomack’s removal of
                                Rightsholder’s Content from the Website or
                                Licensee’s websites or services. In order for
                                Rightsholder to terminate this Agreement
                                following the removal of any of Rightsholder’s
                                Content, they must send Audiomack a Termination
                                Notice.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Account Information; Disclosures:</strong>
                        <ol>
                            <li>
                                <strong>
                                    Rightsholder Account Information.
                                </strong>{' '}
                                In order to access some features of the Website
                                or the App, including Rightsholder’s account
                                information and periodic statements,
                                Rightsholder will have to create an online
                                account (“Account”). Rightsholder hereby
                                represent and warrant that the information
                                Rightsholder provides to Audiomack upon
                                registration will be true, accurate, current,
                                and complete. Rightsholder also hereby
                                represents and warrants that Rightsholder will
                                ensure that their account information, including
                                Rightsholder’s e-mail address, is kept accurate
                                and up to-date at all times during the Term of
                                this Agreement. In the case of entities that are
                                legally authorized to enter into this Agreement
                                on behalf of one or more owners of content, such
                                as record labels, said entity shall have the
                                option whether to maintain a separate Account
                                for each rightsholder on whose behalf this
                                Agreement is entered into.
                            </li>
                            <li>
                                <strong>Password.</strong> As a registered user
                                of the Services Rightsholder will have login
                                information, including a username and password.
                                Rightsholder’s Account is personal to
                                Rightsholder, and Rightsholder may not share
                                their Account information with, or allow access
                                to Rightsholder’s Account by, any third party,
                                other than an agent authorized to act on
                                Rightsholder’s behalf. As Rightsholder will be
                                responsible for all activity that occurs under
                                their Account, Rightsholder should take care to
                                preserve the confidentiality of their username
                                and password, and any device that Rightsholder
                                use to access the Website. Rightsholder agrees
                                to notify Audiomack immediately of any breach in
                                secrecy of their login information.
                                <br />
                                If Rightsholder has any reason to believe that
                                their Account information has been compromised
                                or that their Account has been accessed by a
                                third party not authorized by Rightsholder, then
                                Rightsholder agrees to immediately notify
                                Audiomack by e-mail to{' '}
                                <a href="mailto:Finance@Audiomack.com">
                                    Finance@Audiomack.com
                                </a>
                                . Rightsholder will be solely responsible for
                                the losses incurred by Audiomack and others
                                (including other users) due to any unauthorized
                                use of their Account that takes place prior to
                                notifying Audiomack that Rightsholder’s Account
                                has been compromised.
                            </li>
                            <li>
                                <strong>Disclosure of Information.</strong>{' '}
                                Rightsholder acknowledges, consent, and agree
                                that Audiomack may access, preserve, and
                                disclose Rightsholder’s Account information and
                                Rightsholder’s Content if required to do so by
                                law or in a good faith belief that such access,
                                preservation or disclosure is reasonably
                                necessary to:
                                <ol>
                                    <li>Comply with legal process</li>
                                    <li>Enforce this Agreement</li>
                                    <li>
                                        Respond to a claim that any of
                                        Rightsholder’s Content violates the
                                        rights of third parties;
                                    </li>
                                    <li>
                                        Respond to Rightsholder’s requests for
                                        customer servic
                                    </li>
                                    <li>
                                        Protect the rights, business interests,
                                        property or personal safety of Audiomack
                                        and its employees and users, and the
                                        public.
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>
                            Prohibited Use of the Website and Licensee Websites
                            and Services:
                        </strong>{' '}
                        Rightsholder agrees not to use the Website, the App, the
                        Services, and any services provided by Licensees, for
                        any unlawful purpose or in any way that might harm,
                        damage, or disparage Audiomack, its Licensees or any
                        other party. Without limiting the preceding sentence and
                        by way of example and not limitation, Rightsholder
                        agrees that they will not, whether through the Website,
                        the App, Audiomack’s Licensees or Rightsholder’s
                        Content, do or attempt any of the following:
                        <ol>
                            <li>
                                Undertake, cause, permit or authorize the
                                modification, creation of derivative works,
                                translation, reverse engineering, decompiling,
                                disassembling or hacking of any aspect of the
                                Website or any other part thereof, except and
                                solely to the extent permitted by this
                                Agreement, the features of the Website or by
                                law, or otherwise attempt to use or access any
                                portion of the Website other than as intended.
                            </li>
                            <li>
                                Reproduce, duplicate, copy, sell, trade, resell,
                                distribute or exploit, any portion of the
                                Website or App, use of the Website or App,
                                access to the Website or App or content obtained
                                through the Website or App, as a result of
                                Rightsholder’s being granted permission to
                                upload their Content to the Website or App.
                            </li>
                            <li>
                                Remove, circumvent, disable, damage or otherwise
                                interfere with any security-related features of
                                the Website or App, features that prevent or
                                restrict the use or copying of any part of the
                                Website or App or features that enforce
                                limitations on the use of the Website or App.
                            </li>
                            <li>
                                Threaten, harass, abuse, slander, defame or
                                otherwise violate the legal rights (including,
                                without limitation, rights of privacy and
                                publicity) of third parties.
                            </li>
                            <li>
                                Publish, distribute or disseminate any
                                inappropriate, profane, vulgar, defamatory,
                                infringing, obscene, tortious, indecent,
                                unlawful, offensive, immoral or otherwise
                                objectionable material or information.
                            </li>
                            <li>
                                Create a false identity or impersonate another
                                for the purpose of misleading others as to
                                Rightsholder’s identify, including, but not
                                limited to, providing misleading information to
                                any feedback system employed by Audiomack.
                            </li>
                            <li>
                                Transmit or upload any material that contains
                                viruses, Trojan horses, worms, time bombs, bots,
                                cancelbots, or any other type of malware, any
                                other harmful, damaging or deleterious software
                                programs, or any other malicious code that is
                                specifically designed to or would have the
                                effect of damaging, disrupting, stealing or
                                generally inflicting some bad or illegitimate
                                action on Audiomack or its licensee’s data,
                                hosts or networks.
                            </li>
                            <li>
                                Interfere with or disrupt the Website or App,
                                networks or servers connected to the Website or
                                App or violate the regulations, policies or
                                procedures of such networks or servers.
                            </li>
                            <li>
                                Knowingly upload or otherwise transmit any
                                information or content that infringes any
                                patent, trademark, trade secret, copyright or
                                other proprietary rights of any party, including
                                by incorporating any such material in
                                Rightsholder’s Content.
                            </li>
                            <li>
                                Use the Website or App or any Licensee website
                                in any manner whatsoever that could lead to a
                                violation of any federal, state or local laws,
                                rules or regulations.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Availability of Services:</strong> Audiomack may
                        make changes to or discontinue any aspects of the
                        Services and any of the features, media, content,
                        products, software or services available through the
                        Website or App, at any time and without notice and
                        without liability to Rightsholder. The features, media,
                        content, products, software or services available on and
                        through the Website or App may be out of date, and
                        Audiomack makes no commitment to update any aspect of
                        the Website.
                        <br />
                        Audiomack makes no representations and warranties with
                        respect to availability of the Website or App and may
                        discontinue the Service at any time with or without
                        notice. Rightsholder is solely responsible for
                        maintaining back-up copies of any elements of their
                        Content uploaded to the Website or App otherwise
                        delivered to Audiomack as Physical Product.
                    </li>
                    <li>
                        <strong>
                            Additional Representations and Warranties:
                        </strong>
                        <ol>
                            <li>
                                <strong>
                                    Mutual Representations and Warranties:
                                </strong>{' '}
                                Each party represents and warrants to the other
                                that it:
                                <ol>
                                    <li>
                                        Is authorized to enter into this
                                        Agreement on the terms and conditions
                                        set forth herein.
                                    </li>
                                    <li>
                                        Will not act in any manner that
                                        conflicts or interferes with any
                                        existing commitment or obligation of the
                                        other party, and that no agreement
                                        previously entered into by the party
                                        interferes with the performance of its
                                        obligations under this Agreement.
                                    </li>
                                    <li>
                                        Has the capacity to, and will perform
                                        its obligations hereunder in full
                                        compliance with any applicable laws,
                                        rules, and regulations of any
                                        governmental authority having
                                        jurisdiction over such performance.
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <strong>
                                    Representations and Warranties by
                                    Rightsholder:
                                </strong>{' '}
                                Rightsholder represents and warrants to
                                Audiomack that:
                                <ol>
                                    <li>
                                        They have the full right, power, and
                                        authority to act on behalf of any and
                                        all owners of any right, title or
                                        interest in and to Rightsholder’s
                                        Content, including, but not limited to,
                                        all musical works embodied in their
                                        Content, and that they are authorized to
                                        provide Rightsholder’s Content to
                                        Audiomack for the uses specified in this
                                        Agreement. For the avoidance of doubt,
                                        if Rightsholder is acting on behalf of
                                        an artist, band, producer, group or
                                        corporation, Rightsholder hereby
                                        represent and warrant to Audiomack that
                                        Rightsholder is fully authorized to
                                        enter into this Agreement on behalf of
                                        such artist, band, group or corporation
                                        and to grant all of the rights and
                                        assume and fulfill all of the
                                        obligations, covenants, and
                                        representations and warranties set forth
                                        in this Agreement.
                                    </li>
                                    <li>
                                        Rightsholder owns or controls all of the
                                        necessary rights in their Content in
                                        order to make the grant of rights,
                                        licenses, and permissions herein, and
                                        that Rightsholder has permission to use
                                        the name and likeness of each
                                        identifiable individual person whose
                                        name or likeness is contained or used
                                        within Rightsholder’s Content, and to
                                        use such individual's identifying or
                                        personal information (to the extent such
                                        information is used or contained in the
                                        Rightsholder’s Content) as contemplated
                                        by this Agreement.
                                    </li>
                                    <li>
                                        The use or other exploitation of
                                        Rightsholder’s Content, including, but
                                        not limited to, any musical works
                                        embodied in Rightsholder’s sound
                                        recordings, by Audiomack and Audiomack’s
                                        Licensees as contemplated by this
                                        Agreement does not and will not, on
                                        Rightsholder’s part, knowingly and
                                        willfully, infringe or violate the
                                        rights of any third party, including,
                                        without limitation, any privacy rights,
                                        publicity rights, copyrights, contract
                                        rights, or any other intellectual
                                        property or proprietary rights.
                                    </li>
                                    <li>
                                        To the extent Rightsholder is the
                                        songwriter of any or all of the musical
                                        works embodied in Rightsholder’s
                                        Content, whether in whole or in part
                                        (e.g., as a cowriter), Rightsholder has
                                        the full right, power, and authority to
                                        grant the rights set forth in this
                                        Agreement.
                                    </li>
                                    <li>
                                        Rightsholder has not assigned any of the
                                        rights in and to the sound recordings
                                        embodied in Rightsholder’s Content to
                                        any third party (e.g., a record label)
                                        that obtained exclusive rights in and to
                                        such sound recordings.
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>
                            No Representations and Warranties with respect to
                            Streams Distributions:
                        </strong>{' '}
                        Audiomack makes no guarantees regarding the minimum
                        number of streams of Rightsholder’s Content.
                    </li>
                    <li>
                        <strong>Indemnification:</strong> Rightsholder agrees to
                        indemnify and hold Audiomack and its respective,
                        successors, assigns, agents, distributors, licensees,
                        officers, directors, employees, agents and
                        representatives harmless from and against any third
                        party claims (collectively “Claims”), liabilities, costs
                        and expenses (including reasonable attorney's fees and
                        legal costs) in connection with any claim which is
                        inconsistent with any agreement, covenant,
                        representation, or warranty made by Rightsholder herein
                        or any act or omission by Rightsholder, provided that
                        such claim has been settled with Rightsholder’s consent
                        or has resulted in a final judgment against Audiomack or
                        its licensees, including, but not limited to, any Claims
                        made by a PRO or music publisher with respect to any
                        public performances or communications to the public of
                        any musical works embodied in Rightsholder’s Content,
                        any contributor to any sound recording included within
                        Rightsholder’s Content, including claims from any
                        unions, guilds, background musicians or vocalists,
                        engineers, etc., or any other party for any use or
                        misuse of any other forms of intellectual property or
                        proprietary rights in Rightsholder’s Content, including,
                        but not limited to, trademark rights and invasions of
                        the right of privacy or publicity.
                        <br />
                        Rightsholder will reimburse Audiomack upon demand for
                        any payment made by Audiomack at any time after the date
                        hereof (including after the Term of this Agreement
                        terminates) in respect of any claim, liability, damage
                        or expense to which the foregoing indemnity relates.
                        Upon the making or filing of any such claim, action or
                        demand, Audiomack shall be entitled to withhold from any
                        amounts payable under this agreement such amounts as are
                        reasonably related to the potential liability in issue
                        unless and until Rightsholder post a suitable
                        undertaking or bond by a reputable bonding company
                        satisfactory to Audiomack in it’s sole discretion in the
                        sum equal to the amount of Audiomack’s potential
                        liability hereunder (including legal expenses and
                        reasonable counsel fees). Audiomack will have the right
                        to utilize any such sums so withheld to pay for any
                        costs and reasonable attorneys’ fees as incurred by
                        Audiomack during the pendency of the determination of
                        any such claim.
                        <br />
                        If Audiomack has withheld and reserved any monies
                        pursuant to this subparagraph with respect to any claim
                        and if said claim has not been followed by commencement
                        of a legal action or proceeding within one (1) year from
                        the date first made, Audiomack will release such monies
                        to Rightsholder unless the claim is in the process of
                        being settled or Audiomack has a good faith reason to
                        believe an action will be commenced in the future,
                        without prejudice to its rights to again withhold and
                        reserve monies in the future if any legal action or
                        proceeding is later commenced. Rightsholder shall be
                        promptly notified of any such claim, action or demand
                        and shall have the right, at Rightsholder’s own expense,
                        to participate in the defense thereof with counsel of
                        Rightsholder’s own choosing; provided, however, that
                        Audiomack’s decision in connection with the defense of
                        any such claim, action or demand shall be final. In the
                        case of entities that are legally authorized to enter
                        into this Agreement on behalf of one or more owners of
                        content, such as record labels, said entity shall be
                        solely responsible for any acts or omissions by the
                        Rightsholder that result in any liability under any of
                        the terms and conditions of this Agreement.
                        <br />
                        Audiomack shall not be liable for any acts or omissions
                        by the Rightsholder under this Agreement nor does this
                        Agreement create any form of a contractual relationship
                        between Audiomack and the Rightsholder beyond the one
                        stated herein.
                        <ol>
                            <li>
                                Indemnification Request. If Audiomack make an
                                indemnification request to Rightsholder under
                                this Section, Audiomack may permit Rightsholder
                                to control the defense, disposition or
                                settlement of the matter at Rightsholder’s own
                                expenses, provided that Rightsholder shall not,
                                without Audiomack’s prior written consent, enter
                                into any settlement or agree to any disposition
                                that requires any admission of liability by
                                Audiomack or imposes any conditions or
                                obligations on Audiomack other than the payment
                                of monies that are readily measurable for
                                purposes of determining Rightsholder’s monetary
                                indemnification or reimbursement obligations to
                                us.
                                <br />
                                If in Audiomack’s reasonable and good faith
                                judgment conclude that Rightsholder is not
                                capable of defending the Rightsholder’s or
                                Audiomack’s interests against any Claims, then
                                Audiomack shall have the option to control the
                                defense in any matter or litigation through
                                counsel of Audiomack’s own choosing to defend
                                against any such Claim for which Rightsholder
                                owes Audiomack an indemnification, and the costs
                                of such counsel, as well as any court costs,
                                shall be at Rightsholder’s expense.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Disclaimers:</strong>
                        <ol>
                            <li>
                                THE WEBSITE, THE APP AND ANY THIRD-PARTY
                                CONTENT, SOFTWARE, SERVICES OR APPLICATIONS MADE
                                AVAILABLE IN CONJUNCTION WITH OR THROUGH THE
                                WEBSITE OR APP, ARE PROVIDED ON AN “AS IS,” “AS
                                AVAILABLE,” “WITH ALL FAULTS” BASIS WITHOUT
                                REPRESENTATIONS AND WARRANTIES OF ANY KIND,
                                EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT
                                LIMITED TO, IN TERMS OF CORRECTNESS, ACCURACY,
                                RELIABILITY, OR OTHERWISE.
                            </li>
                            <li>
                                TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO
                                APPLICABLE LAW, AUDIOMACK AND ITS AFFILIATES,
                                PARTNERS AND LICENSORS HEREBY DISCLAIM ALL
                                EXPRESS, IMPLIED AND STATUTORY WARRANTIES OF ANY
                                KIND, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                                WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
                                PARTICULAR PURPOSE, AND NON-INFRINGEMENT. NO
                                ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN,
                                OBTAINED BY RIGHTSHOLDER FROM AUDIOMACK OR
                                THROUGH THE WEBSITE OR APP WILL CREATE ANY
                                WARRANTY NOT EXPRESSLY STATED HEREIN. AUDIOMACK
                                AND ITS AFFILIATES, PARTNERS, LICENSORS, AND
                                SUPPLIERS DO NOT WARRANT THAT THE WEBSITE, THE
                                APP OR ANY PART THEREOF, OR ANY SERVICES
                                PROVIDED BY AUDIOMACK, WILL BE UNINTERRUPTED, OR
                                FREE OF ERRORS, VIRUSES OR OTHER HARMFUL
                                COMPONENTS AND DO NOT WARRANT THAT ANY OF THE
                                FOREGOING WILL BE CORRECTED. RIGHTSHOLDER
                                UNDERSTAND AND AGREE THAT THE RIGHTHOLDER USES,
                                ACCESSES, DOWNLOADS, OR OTHERWISE OBTAINS
                                INFORMATION, MATERIALS, OR DATA THROUGH THE
                                WEBSITE, THE APP OR ANY ASSOCIATED SITES OR
                                APPLICATIONS, AND OFFER THE RIGHTHOLDER’S
                                CONTENT THROUGH THE SERVICES, AT THEIR OWN
                                DISCRETION AND RISK, AND THAT THE RIGHTHOLDER
                                WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO
                                RIGHTHOLDER’S PROPERTY (INCLUDING THE
                                RIGHTHOLDER’S COMPUTER SYSTEM, HANDSET, OR ANY
                                OTHER DEVICE OR PERIPHERAL USED IN CONNECTION
                                WITH THE WEBSITE OR THE APP) OR LOSS OF DATA
                                THAT RESULTS FROM THE DOWNLOAD OR USE OF SUCH
                                MATERIAL OR DATA.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>iability:</strong> AUDIOMACK SHALL NOT BE LIABLE
                        TO THE RIGHTHOLDER FOR SPECIAL, INCIDENTAL,
                        CONSEQUENTIAL OR PUNITIVE DAMAGES OF ANY NATURE, FOR ANY
                        REASON, INCLUDING, WITHOUT LIMITATION, THE BREACH OF
                        THIS AGREEMENT OR ANY PROVISION HEREOF OR ANY
                        TERMINATION OF THIS AGREEMENT, WHETHER SUCH LIABILITY IS
                        ASSERTED ON THE BASIS OF CONTRACT, TORT (INCLUDING
                        NEGLIGENCE OR STRICT LIABILITY) OR OTHERWISE, EVEN IF
                        AUDIOMACK HAS BEEN WARNED OF THE POSSIBILITY OF SUCH
                        DAMAGES. AUDIOMACK SHALL ALSO NOT BE LIABLE FOR ANY
                        ROYALTIES, FEES, PAYMENTS OR DAMAGES ARISING OUT OF THE
                        FAILURE OF ANY LICENSEE TO PAY AUDIOMACK OR
                        RIGHTSHOLDERS ANY ROYALTIES THAT ARE DUE FOR ANY USE OR
                        MISUSE OF RIGHTSHOLDER’S CONTENT, WHETHER PURSUANT TO AN
                        EXISTING, EXPIRED OR TERMINATED AGREEMENT WITH AUDIOMACK
                        OR OTHERWISE. AUDIOMACK’S TOTAL LIABILITY TO
                        RIGHTSHOLDER FOR ANY BREACH OF THIS AGREEMENT SHALL IN
                        ALL INSTANCES BE LIMITED TO THE AMOUNT OF MONIES
                        ACTUALLY PAID TO RIGHTSHOLDER BY AUDIOMACK FOR THE RIGHT
                        TO STREAM THE RIGHTHOLDER’S CONTENT DURING THE SIX
                        (6)-MONTH PERIOD IMMEDIATELY PRECEDING THE DATE OF THE
                        RIGHTHOLDER’S CLAIM AGAINST AUDIOMACK.
                        <ol>
                            <li>
                                <strong>Applicable Law:</strong> APPLICABLE LAW
                                MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF
                                LIABILITY OR INCIDENTAL OR CONSEQUENTIAL
                                DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION
                                MAY NOT APPLY TO RIGHTSHOLDER. IN SUCH CASES,
                                RIGHTSHOLDER AGREES THAT BECAUSE SUCH
                                DISCLAIMERS AND LIMITATIONS OF LIABILITY REFLECT
                                A REASONABLE AND FAIR ALLOCATION OF RISK BETWEEN
                                RIGHTSHOLDER AND AUDIOMACK, AND ARE FUNDAMENTAL
                                ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN
                                RIGHTSHOLDER AND AUDIOMACK, AUDIOMACK’S
                                LIABILITY SHALL BE LIMITED TO THE MAXIMUM EXTENT
                                PERMITTED BY LAW. RIGHTSHOLDER UNDERSTANDS AND
                                AGREES THAT AUDIOMACK WOULD NOT BE ABLE TO OFFER
                                ITS PREMIUM PARTNER OPTION TO RIGHTSHOLDER ON AN
                                ECONOMICALLY FEASIBLE BASIS WITHOUT THESE
                                LIMITATIONS AND WOULD NOT OFFER SUCH SERVICES
                                ABSENT A LIMITATION OF LIABILITY.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Force Majeure:</strong> If by reason of failures
                        of telecommunications or internet service providers,
                        labor disputes, riots, inability to obtain labor or
                        materials, earthquake, fire or other action of the
                        elements, accidents, governmental restrictions or other
                        causes beyond Audiomack’s control, Audiomack is unable
                        to perform in whole or in part Audiomack’s obligations
                        as set forth in this Agreement, then Audiomack shall be
                        relieved of those obligations to the extent Audiomack
                        are so unable to perform and such inability to perform
                        shall not make Audiomack liable to the Rightsholder or
                        other third parties.
                    </li>
                    <li>
                        <strong>Dispute Resolution:</strong>
                        <ol>
                            <li>
                                <strong>Mandatory Arbitration:</strong> please
                                read this carefully as it affects Rightsholder’s
                                rights. RIGHTSHOLDER AND AUDIOMACK AND EACH OF
                                AUDIOMACK’S RESPECTIVE SUBSIDIARIES, AFFILIATES,
                                PREDECESSORS IN INTEREST, SUCCESSORS, AND
                                PERMITTED ASSIGNS AGREE TO ARBITRATION (EXCEPT
                                FOR MATTERS THAT MAY BE TAKEN TO SMALL CLAIMS
                                COURT), AS THE EXCLUSIVE FORM OF DISPUTE
                                RESOLUTION EXCEPT AS PROVIDED FOR BELOW, FOR ALL
                                DISPUTES AND CLAIMS ARISING OUT OF OR RELATING
                                TO THIS AGREEMENT OR RIGHTSHOLDER’S USE OF THE
                                SERVICES. Arbitration is more informal than a
                                lawsuit in court. Arbitration uses a neutral
                                arbitrator instead of a judge or jury, allows
                                for more limited discovery than in court, and is
                                subject to very limited review by courts.
                                Arbitrators can award the same damages and
                                relief that a court can award.
                            </li>
                            <li>
                                <strong>Commencing Arbitration:</strong> A party
                                who intends to seek arbitration must first send
                                to the other, by certified mail, a written
                                notice of intent to arbitrate (a “Notice”), or,
                                in the absence of a mailing address provided by
                                RIGHTSHOLDER to AUDIOMACK, to Rightsholder
                                through any other method available to AUDIOMACK,
                                including through e-mail. The Notice to
                                Audiomack should be addressed to: Audiomack
                                Inc., 648 Broadway, Suite 302, New York, NY
                                10012 (the “Arbitration Notice Address”). The
                                Notice must (i) describe the nature and basis of
                                the claim or dispute; and (ii) set forth the
                                specific relief sought (the “Demand”). If
                                Rightsholder and Audiomack do not reach an
                                agreement to resolve the claim within 30 days
                                after the Notice is received, Rightsholder or
                                Audiomack may commence an arbitration proceeding
                                as set forth below or file a claim in small
                                claims court. THE ARBITRATION SHALL BE
                                ADMINISTERED BY THE AMERICAN ARBITRATION
                                ASSOCIATION (“AAA”) IN ACCORDANCE WITH ITS
                                COMMERCIAL ARBITRATION RULES AND THE
                                SUPPLEMENTARY PROCEDURES FOR CONSUMER RELATED
                                DISPUTES (THE “Rules”), AS MODIFIED BY THIS
                                AGREEMENT. The Rules and AAA forms are available
                                online at www.adr.org or by calling the AAA at
                                1-800-778-7879, or by writing to the Notice
                                Address. If Rightsholder is required to pay a
                                filing fee to commence an arbitration against
                                Audiomack, then Audiomack will promptly
                                reimburse Rightsholder for their confirmed
                                payment of the filing fee upon Audiomack receipt
                                of Notice at the Arbitration Notice Address that
                                Rightsholder has commenced arbitration along
                                with a receipt evidencing payment of the filing
                                fee, unless Rightsholder’s Demand is equal to or
                                greater than One Thousand ($1,000.00) Dollars,
                                in which case Rightsholder is solely responsible
                                for the payment of the filing fee.
                            </li>
                            <li>
                                <strong>Arbitration Proceeding:</strong> The
                                arbitration shall be conducted in the English
                                language. A single independent and impartial
                                arbitrator shall be appointed pursuant to the
                                Rules, as modified herein. Rightsholder and
                                Audiomack agree to comply with the following
                                rules, which are intended to streamline the
                                dispute resolution process and reduce the costs
                                and burdens on the parties: (i) the arbitration
                                shall be conducted by telephone, online and/or
                                be solely based on written submissions, the
                                specific manner to be chosen by the party
                                initiating the arbitration; (ii) the arbitration
                                shall not require any personal appearance by the
                                parties or witnesses unless otherwise mutually
                                agreed in writing by the parties; and (iii) any
                                judgment on the award rendered by the arbitrator
                                may be entered in any court of competent
                                jurisdiction.
                            </li>
                            <li>
                                <strong>No Class Actions:</strong> RIGHTSHOLDER
                                AND AUDIOMACK AGREE THAT RIGHTSHOLDER AND
                                AUDIOMACK MAY BRING CLAIMS AGAINST THE OTHER
                                ONLY IN RIGHTSHOLDER’S OR ITS INDIVIDUAL
                                CAPACITY AND NOT AS A PLAINTIFF OR CLASS MEMBER
                                IN ANY PURPORTED CLASS OR REPRESENTATIVE
                                PROCEEDING. FURTHER, RIGHTSHOLDER AGREES THAT
                                THE ARBITRATOR MAY NOT CONSOLIDATE PROCEEDINGS
                                OF MORE THAN ONE PERSON’S CLAIMS, AND MAY NOT
                                OTHERWISE PRESIDE OVER ANY FORM OF A
                                REPRESENTATIVE OR CLASS PROCEEDING, AND THAT IF
                                THIS SPECIFIC PROVISO IS FOUND TO BE
                                UNENFORCEABLE, THEN THE ENTIRETY OF THIS
                                MANDATORY ARBITRATION SECTION SHALL BE NULL AND
                                VOID.
                            </li>
                            <li>
                                <strong>Decision of the Arbitrator:</strong>{' '}
                                Barring extraordinary circumstances, the
                                arbitrator shall issue his or her decision
                                within 120 days from the date the arbitrator is
                                appointed. The arbitrator may extend this time
                                limit for an additional 30 days in the interests
                                of justice. All arbitration proceedings shall be
                                closed to the public and confidential and all
                                records relating thereto shall be permanently
                                sealed, except as necessary to obtain court
                                confirmation of the arbitration award. The award
                                of the arbitrator shall be in writing and shall
                                include a statement setting forth the reasons
                                for the disposition of any claim. The arbitrator
                                shall apply the laws of the State of New York in
                                conducting the arbitration. Rightsholder
                                acknowledge that this Agreement and
                                Rightsholder’s use of the Services and the
                                Website evidences a transaction involving
                                interstate commerce. The United States Federal
                                Arbitration Act shall govern the interpretation,
                                enforcement, and proceedings pursuant to the
                                Mandatory Arbitration clause in this Agreement.
                            </li>
                            <li>
                                <strong>Applicable Law:</strong> This Agreement
                                and Rightsholder’s use of the Services and the
                                Website shall be governed by the substantive
                                laws of the State of New York without reference
                                to its choice or conflicts of law principles.
                            </li>
                            <li>
                                <strong>Equitable Relief:</strong> The foregoing
                                provisions of this Dispute Resolution section do
                                not apply to any claim in which Audiomack seeks
                                equitable relief of any kind. Rightsholder
                                acknowledge that, in the event of a breach of
                                this Agreement by Audiomack or any third party,
                                the damage or harm, if any, caused to
                                Rightsholder will not entitle Rightsholder to
                                seek injunctive or other equitable relief
                                against Audiomack, and Rightsholder’s only
                                remedy shall be for monetary damages, subject to
                                the limitations of liability set forth in this
                                Agreement.
                            </li>
                            <li>
                                <strong>Claims:</strong> Rightsholder and
                                Audiomack agree that, notwithstanding any other
                                rights the party may have under law or equity,
                                any cause of action arising out of or related to
                                this Agreement, the Services or the use of the
                                Website, excluding a claim for indemnification,
                                must commence within one year after the cause of
                                action accrues. Otherwise, such cause of action
                                is permanently barred.
                            </li>
                            <li>
                                <strong>Improperly Filed Claims:</strong> All
                                claims Rightsholder brings against Audiomack
                                must be resolved in accordance with this Dispute
                                Resolution section. All claims filed or brought
                                contrary to this Dispute Resolution section
                                shall be considered improperly filed. Should
                                Rightsholder file a claim contrary to this
                                Dispute Resolution section, Audiomack may
                                recover attorneys’ fees and costs up to Five
                                Thousand ($5,000.00) Dollars, provided that
                                Audiomack has notified Rightsholder in writing
                                of the improperly filed claim, and Rightsholder
                                has failed to promptly withdraw the claim.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>General Provisions:</strong>
                        <ol>
                            <li>
                                <strong>Relationship of the Parties.</strong>{' '}
                                The parties hereto agree and acknowledge that
                                the relationship between them is that of
                                independent contractors. This Agreement shall
                                not be deemed to create an agency, partnership
                                or joint venture between Rightsholder and
                                Audiomack, and Audiomack shall not have a
                                fiduciary obligation to Rightsholder as a result
                                of their entering into this Agreement.
                            </li>
                            <li>
                                <strong>Entire Agreement.</strong> This
                                Agreement together with the Audiomack TOS
                                contains the entire understanding of the parties
                                relating to the subject matter hereof. This
                                Agreement (including all Addenda) supersedes all
                                previous agreements or arrangements between
                                Rightsholder and Audiomack pertaining to the
                                Services, provided that if Rightsholder
                                previously entered into a digital distribution
                                or consignment agreement with Audiomack in the
                                past, and elected any options specifying or
                                limiting the scope of the distribution of
                                Rightsholder’s Content, the limitations and
                                authorizations with respect to the distribution
                                of Rightsholder’s Content shall remain in place
                                under this Agreement and the applicable
                                Addendum. This Agreement cannot be changed or
                                modified except as provided herein.
                            </li>
                            <li>
                                <strong>Waiver; Severability.</strong> A waiver
                                by either party of any term or condition of this
                                Agreement will not be deemed or construed as a
                                waiver of such term or condition, or of any
                                subsequent breach thereof. If any provision of
                                this Agreement is determined by a court of
                                competent jurisdiction to be unenforceable, such
                                determination shall not affect any other
                                provision hereof, and the unenforceable
                                provision shall be replaced by an enforceable
                                provision that most closely meets the commercial
                                intent of the parties.
                            </li>
                            <li>
                                <strong>Binding Effect.</strong> This Agreement
                                will be binding on the assigns, heirs,
                                executors, affiliates, agents, personal
                                representatives, administrators, and successors
                                (whether through merger, operation of law, or
                                otherwise) of each of the parties.
                            </li>
                            <li>
                                <strong>Notice.</strong> Any notice, approval,
                                request, authorization, direction or other
                                communication under this Agreement shall be
                                given in writing and shall be deemed to have
                                been delivered and given for all purposes on the
                                delivery date if sent by electronic mail to the
                                addresses provided to and by Rightsholder upon
                                registration with Audiomack, or as properly
                                updated, or, in the absence of a valid
                                electronic mail address, through any other
                                method Audiomack may elect in its sole
                                discretion, including, but not limited to,
                                through posting on the Website.
                            </li>
                            <li>
                                <strong>Governing Law;</strong> Dispute
                                Resolution. This Agreement will be governed and
                                interpreted in accordance with the laws of the
                                State of New York applicable to agreements
                                entered into and to be wholly performed in New
                                York, without regard to conflict of laws
                                principles. Any and all disputes arising out of
                                this Agreement shall be resolved in the manner
                                set forth in the TOS.
                            </li>
                            <li>
                                <strong>Rights Cumulative.</strong> To the
                                extent permitted by applicable law, the rights
                                and remedies of the parties provided under this
                                Agreement are cumulative and are in addition to
                                any other rights and remedies of the parties at
                                law or equity.
                            </li>
                            <li>
                                <strong>Headings.</strong> The titles and
                                headings used in this Agreement are for
                                convenience only and are not to be considered in
                                construing or interpreting this Agreement.
                            </li>
                            <li>
                                <strong>No Third Party Beneficiaries.</strong>{' '}
                                This Agreement is for the sole benefit of the
                                parties hereto and their authorized successors
                                and permitted assigns. Nothing herein, express
                                or implied, is intended to or shall confer upon
                                any person or entity, other than the parties
                                hereto and their authorized successors and
                                permitted assigns, any legal or equitable right,
                                benefit or remedy of any nature whatsoever under
                                or by reason of this Agreement.
                            </li>
                            <li>
                                <strong>Assignment.</strong> Audiomack may
                                assign its rights and obligations under this
                                Agreement at any time to any party. Rightsholder
                                may not assign their rights and/or obligations
                                under this Agreement without obtaining
                                Audiomack’s prior written consent.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Definitions:</strong> The following capitalized
                        terms shall have the following meanings for purposes of
                        this Agreement:
                        <ol>
                            <li>
                                “Service” or “Services” means the streaming of
                                Rightsholder’s Content on the Website, the App
                                or any Licensee website for which Compensation
                                shall be due to Rightsholder under this
                                Agreement.
                            </li>
                            <li>
                                “Audiomack Premium Partner” means an individual
                                or group of individuals (for example a band)
                                that authorizes Audiomack by way of this
                                Agreement to run Advertisement(s) in connection
                                with the streaming of their Content on Audiomack
                                in exchange for Compensation.
                            </li>
                            <li>
                                “Rightsholder’s Content” means sound recordings,
                                video content (i.e., audiovisual works), and the
                                musical works embodied in such sound recordings
                                and video content, and any album related
                                artwork, photos, liner notes, metadata and other
                                material related to Rightsholder’s sound
                                recordings and video content that Rightsholder
                                has provided to Audiomack, either by digital
                                upload to the Website, the App or by delivery of
                                Physical Product, either directly or through a
                                third party acting on Rightsholder’s behalf. Any
                                such sound recordings and video content (and the
                                musical works embodied therein), artwork,
                                photos, liner notes, metadata, or other material
                                provided by Rightsholder to Audiomack, must be
                                owned or controlled by Rightsholder and/or have
                                been cleared by Rightsholder for all purposes
                                and rights granted and authorized under this
                                Agreement. For the avoidance of doubt,
                                Rightsholder’s Content encompasses each sound
                                recording and the musical work (i.e., the notes
                                and lyrics) embodied in each sound recording.
                            </li>
                            <li>
                                “Licensee” or “Licensees” means any third party
                                that Audiomack may authorize to use the
                                Audiomack embedded player to stream
                                Rightsholder’s Content pursuant to the terms of
                                this Agreement.
                            </li>
                            <li>“Authorized Territory” means the universe.</li>
                            <li>
                                “Digital Master” or “Digital Masters” means a
                                copy or copies of Rightsholder’s Content in
                                digital form, whether created by Rightsholder or
                                Audiomack.
                            </li>
                            <li>
                                “Copyright Management Information” means the
                                digital information conveying information
                                regarding a Digital Master, including, by way of
                                example and not limitation, the title of the
                                applicable album, the name of the song, the ISRC
                                code, the marketing label, and the record
                                company name.
                            </li>
                            <li>
                                “Actual Costs” means any and all costs
                                associated with Advertisement’s or Subscriptions
                                on Audiomack that are actually incurred by
                                Audiomack. For the avoidance of doubt, the
                                following are some of the types of Actual Costs
                                incurred by Audiomack in connection with
                                Advertisements: revenue splits between the
                                advertiser and Audiomack as well as design and
                                development costs for custom campaigns. These
                                are not the only types of Actual Costs that may
                                be considered Actual Costs by Audiomack in
                                connection with Advertisements and Audiomack
                                expressly reserves the right to apply and
                                consider other actual costs that are typically
                                incurred in in connection with Advertisements as
                                Actual Costs. However, hosting costs incurred by
                                Audiomack shall specifically not be included as
                                an Actual Cost under this Agreement.
                                Additionally, the following are some of the
                                types of Actual Costs incurred by Audiomack in
                                connection with Subscriptions: payment
                                processing costs. These are not the only types
                                of Actual Costs that may be considered Actual
                                Costs by Audiomack in connection with
                                Subscriptions and Audiomack expressly reserves
                                the right to apply and consider other actual
                                costs that are typically incurred in in
                                connection with Subscriptions as Actual Costs.
                            </li>
                            <li>
                                “Advertisement” means on the Website, a Licensee
                                website, or through the App, any communication
                                of products or services that appears or is
                                audible to Audiomack users directly in
                                connection with the consumption of Monetized
                                Content. Audiomack reserves the right to
                                consider any form of communication of products
                                or services that is communicated in connection
                                with Rightsholder’s Content on the Website, a
                                Licensee website, or through the App to be an
                                Advertisement. For the avoidance of doubt, the
                                following are potential revenue streams of
                                Audiomack that are not considered
                                Advertisements:
                                <ol>
                                    <li>
                                        Sharing or selling of data to any
                                        reputable third party partner or
                                        service, to enhance the functionality,
                                        reliability, utility, and profitability
                                        of the Services and to provide certain
                                        specialized services for Audiomack’s
                                        administrators and users
                                    </li>
                                    <li>
                                        Advertisement’s in connection with
                                        rightsholder upload pages
                                    </li>
                                    <li>Event sponsorships</li>
                                    <li>
                                        Sponsored emails or any instance of
                                        sponsored email marketing
                                    </li>
                                    <li>
                                        Any other instance of the communication
                                        of products or services on the Website,
                                        a Licensee website, or through the App
                                        that is not in the good faith estimation
                                        of Audiomack directly related to the
                                        consumption of Rightsholder’s Content
                                        and/or Monetized Content.
                                    </li>
                                </ol>
                            </li>
                            <li>
                                “Subscription” means Audiomack’s “Audiomack
                                Gold” and “Audiomack Platinum” subscription
                                services and any forthcoming subscription
                                service implemented by Audiomack in the future.
                            </li>
                            <li>
                                “Stream” means any stream of a sound recording
                                on Audiomack, including, without limitation the
                                Website, a Licensee Website and/or the App that
                                is streamed by an Audiomack user for thirty
                                seconds (30) or longer. All instances where a
                                sound recording is streamed by a user for less
                                than thirty (30) seconds shall not be considered
                                a qualifying Audiomack Stream.
                            </li>
                            <li>
                                “Monetized Content” means sound recordings and
                                video content on Audiomack services, including,
                                without limitation, its app or through embeds
                                from Audiomack, that have been indicated by
                                Rightsholder on either the Website, a Licensee
                                website or the App to be eligible for
                                monetization and that Audiomack, in its sole
                                discretion, has accepted as Monetized Content.
                                For the playing of any sound recording or video
                                content to qualify and be considered Monetized
                                Content it must be streamed by an Audiomack user
                                for thirty seconds (30) or longer. All instances
                                where a sound recording or video content is
                                streamed by a user for less than thirty (30)
                                seconds shall not be considered a stream of
                                Monetized Content and no Compensation shall be
                                due in connection with said content.
                            </li>
                            <li>
                                “Promotional Opportunity” means any
                                communication of Rightsholder’s content that
                                appears or is audible to Audiomack users.
                                Audiomack reserves the right to consider any
                                form of communication of Rightsholder’s content
                                on the Website, a Licensee website, or through
                                the App to be a Promotional Opportunity
                            </li>
                        </ol>
                    </li>
                </ol>
                <p>
                    <strong>
                        THIS AGREEMENT, WHEN ACCEPTED BY YOU AFTER CLICKING
                        "SUBMIT APPLICATION" WILL CREATE A BINDING AND LEGALLY
                        ENFORCEABLE CONTRACT BETWEEN YOU AND US, WHETHER YOU ARE
                        ACTING IN YOUR INDIVIDUAL CAPACITY, OR AS THE AUTHORIZED
                        REPRESENTATIVE FOR AN ARTIST, BAND, GROUP OR
                        CORPORATION, IN WHICH CASE "YOU" SHALL REFER TO THE
                        ARTIST, BAND, GROUP OR CORPORATION ON WHOSE BEHALF YOU
                        ARE ACTING AND AUTHORIZED TO ACT. THEREFORE, PLEASE READ
                        THIS AGREEMENT CAREFULLY AND CONSULT WITH YOUR OWN
                        BUSINESS AND LEGAL ADVISORS BEFORE CLICKING "SUBMIT
                        APPLICATION". THE "EFFECTIVE DATE" OF THIS AGREEMENT IS
                        THE DATE ON WHICH YOU CLICK THE "SUBMIT APPLICATION"
                        BUTTON BELOW AND/OR SIGN AND RETURN THE COVER SHEET.
                    </strong>
                </p>
            </div>
        );
    }
}
