import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import hideSidebarForComponent from '../hoc/hideSidebarForComponent';
import { showModal, MODAL_TYPE_AUTH } from '../redux/modules/modal';

import FooterDark from '../components/FooterDark';
import AmMark from '../icons/am-mark';
import FreeIcon from '../icons/free-icon';
import AdvancedStatsIcon from '../icons/advanced-stats';
import CalendarIcon from '../icons/create-calendar';
import GooglePlayIcon from '../icons/google-play';
import AppStoreIcon from '../icons/app-store';

class AboutPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        player: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        document.body.classList.add('no-bottom-padding', 'no-header-border');
    }

    componentWillUnmount() {
        document.body.classList.remove('no-bottom-padding', 'no-header-border');
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAuthLinkClick = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const link = e.currentTarget;

        const path = `${link.pathname}${link.search}`;

        // Using window.history instead of props.history so the page
        // doesn't change due to react router handling the history
        window.history.replaceState(null, null, path);

        dispatch(showModal(MODAL_TYPE_AUTH, { type: 'join' }));
    };

    renderHero() {
        return (
            <div className="hero hero--about u-pos-relative u-of-hidden">
                <AmMark className="hero__mark u-text-white" />
                <div className="hero__images-wrap u-pos-absolute">
                    <img
                        src="/static/images/desktop/about/create-macbook.png"
                        srcSet="/static/images/desktop/about/create-macbook@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--laptop"
                    />
                    <img
                        src="/static/images/desktop/about/create-ipad.png"
                        srcSet="/static/images/desktop/about/create-ipad@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--tablet"
                    />
                    <img
                        src="/static/images/desktop/about/create-phone-top.png"
                        srcSet="/static/images/desktop/about/create-phone-top@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--phone-top"
                    />
                    <img
                        src="/static/images/desktop/about/create-phone-bottom.png"
                        srcSet="/static/images/desktop/about/create-phone-bottom@2x.png 2x"
                        alt="Moving music forward"
                        className="hero__image u-pos-absolute hero__image--phone-bottom"
                    />
                </div>
                <div className="hero__inner u-pos-relative u-text-center u-margin-center">
                    <h1 className="hero__title u-text-white u-fw-700 u-tt-uppercase u-lh-1">
                        We’re Here to Move Music Forward.
                    </h1>
                    <h2 className="hero__subtitle u-text-white u-fw-500 u-lh-13">
                        Audiomack is a <strong>FREE</strong>, limitless music
                        sharing and discovery platform for artists, tastemakers,
                        labels, and fans.
                    </h2>
                    {!this.props.currentUser.isLoggedIn && (
                        <Link
                            className="button button--pill hero__button u-tt-uppercase u-br-25"
                            to={`/join?redirectTo=${encodeURIComponent(
                                '/upload'
                            )}`}
                            onClick={this.handleAuthLinkClick}
                        >
                            Get Started
                        </Link>
                    )}
                </div>
            </div>
        );
    }

    renderArtistFeatures() {
        const createFeatures = [
            {
                feature: 'free',
                Icon: FreeIcon,
                title: 'No Premium Accounts - Completely Free',
                copy:
                    'Tired of paying for unlimited uploads? Whether you have 1 song or 1,000, Audiomack lets you host it all for free. No premium accounts ever!'
            },
            {
                feature: 'advanced-stats',
                Icon: AdvancedStatsIcon,
                title: 'Advanced Stats - Build Your Fanbase Easily.',
                copy:
                    'Your free artist account includes advanced stats to help you measure engagement and build your fanbase.'
            },
            {
                feature: 'powerful',
                Icon: CalendarIcon,
                title:
                    'Powerful Release Management - Schedule, Share, and More',
                copy:
                    'Audiomack gives you the most advanced release tools anywhere, with next-gen Private Track functionality, beautiful embeds, and advanced scheduling.'
            }
        ].map(({ feature, Icon, title, copy }, i) => {
            const klass = `create-key-feature u-text-center create-key-feature--${feature} column small-24 medium-8`;

            return (
                <div key={i} className={klass}>
                    <div className="create-key-feature__inner">
                        <div className="create-key-feature__icon u-text-white u-br-circle">
                            <Icon />
                        </div>
                        <div className="create-key-feature__content">
                            <h2 className="create-key-feature__title u-lh-13 u-fw-700">
                                {title}
                            </h2>
                            <p>{copy}</p>
                        </div>
                    </div>
                </div>
            );
        });

        return (
            <div className="create-key-features create-key-features--artist">
                <div className="row expanded medium-align-center">
                    {createFeatures}
                </div>
            </div>
        );
    }

    renderMainFeatures() {
        const currentDate = new Date().getFullYear();

        const mainFeatures = [
            {
                feature: 'share',
                imageProps: {
                    src: '/static/images/desktop/about/create-share.png',
                    srcSet:
                        '/static/images/desktop/about/create-share@2x.png 2x',
                    alt: 'Share your music with millions'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        <span className="u-text-orange">
                            Share your music with millions
                        </span>{' '}
                        of highly engaged listeners
                    </h3>
                ),
                copy: (
                    <p>
                        Millions of fans use Audiomack daily to discover the
                        hottest trending music anywhere. Upload your music and
                        submit it for Trending consideration (completely free)
                        to get the chance to be placed in front of a massive new
                        audience.
                    </p>
                )
            },
            {
                feature: 'dashboard',
                imageProps: {
                    src: '/static/images/desktop/about/create-laptop.png',
                    srcSet:
                        '/static/images/desktop/about/create-laptop@2x.png 2x',
                    alt: 'Powerful artist dashboard and stats'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        Powerful{' '}
                        <span className="u-text-orange">Artist Dashboard</span>{' '}
                        and Stats
                    </h3>
                ),
                copy: (
                    <p>
                        Audiomack gives you an advanced Artist Dashboard with
                        in-depth stats on how your content is being consumed,
                        completely free - no premium account needed. See
                        engagement and granular data on how fans are interacting
                        with your releases.
                    </p>
                )
            },
            {
                feature: 'embeds',
                imageProps: {
                    src: '/static/images/desktop/about/create-embed.png',
                    srcSet:
                        '/static/images/desktop/about/create-embed@2x.png 2x',
                    alt: 'Beautiful and functional Embeddable Players'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        Beautiful and functional{' '}
                        <span className="u-text-orange">
                            Embeddable Players
                        </span>
                    </h3>
                ),
                copy: (
                    <p>
                        Every upload on Audiomack creates a free, customizable
                        embed player which can be placed on any 3rd party blog,
                        platform, or site. Use embeds to let your music go wide,
                        then funnel fans to your highest value platforms (merch,
                        ticket sales, etc) using the built-in Buy button.
                    </p>
                )
            },
            // {
            //     feature: 'scheduling',
            //     imageProps: {
            //         src: '/static/images/desktop/about/create-scheduling.png',
            //         alt: 'Next gen release scheduling and sharing'
            //     },
            //     title: <h3 className="create-feature__title u-fw-700 u-lh-12">Next gen <span className="u-text-orange">release scheduling and sharing</span></h3>,
            //     copy: <p>Our innovative Private Tracks feature lets you create multiple custom private URL's for an upload - so you can track listens from blogs, labels, and more, even if the listener doesn't have an Audiomack account, and shut off one private link while leaving the rest live. Plan and schedule upcoming releases easily with our free Release Schedule features.</p>
            // },
            {
                feature: 'reliable',
                imageProps: {
                    src: '/static/images/desktop/about/create-reliable.png',
                    srcSet:
                        '/static/images/desktop/about/create-reliable@2x.png 2x',
                    alt: 'Plan your releases with confidence.'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        Plan your{' '}
                        <span className="u-text-orange">
                            releases with confidence.
                        </span>
                    </h3>
                ),
                copy: (
                    <p>
                        A hosting platform is only useful if it's there when you
                        need it most - on release day. Tired of having your
                        press release cycles ruined by unreliable hosting?
                        Audiomack has served billions of streams with 99.9%
                        uptime over the last 12 months. Plan and release with
                        confidence using Audiomack.
                    </p>
                )
            },
            {
                feature: 'marketing',
                imageProps: {
                    src: '/static/images/desktop/about/create-marketing.png',
                    srcSet:
                        '/static/images/desktop/about/create-marketing@2x.png 2x',
                    alt: 'More than hosting - a free Marketing platform'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        More than hosting -{' '}
                        <span className="u-text-orange">
                            a free Marketing platform
                        </span>
                    </h3>
                ),
                copy: (
                    <Fragment>
                        <p>
                            We firmly believe your music should live everywhere,
                            not just on Audiomack. All Audiomack uploads are
                            compatible with Hype Machine, and creators can embed
                            a YouTube video on every Song and Album page.
                        </p>
                    </Fragment>
                )
            },
            {
                feature: 'monetize',
                imageProps: {
                    src: '/static/images/desktop/about/create-monetize.png',
                    srcSet:
                        '/static/images/desktop/about/create-monetize@2x.png 2x',
                    alt: 'Monetize your audio on Audiomack'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        <span className="u-text-orange">
                            Monetize your audio
                        </span>{' '}
                        on Audiomack
                    </h3>
                ),
                copy: (
                    <Fragment>
                        <p>
                            Our AMP monetization program is currently in use by
                            thousands of creators, earning artists a competitive
                            per-stream rate on their Audiomack uploads.
                        </p>
                        <p>
                            AMP is currently in advanced beta and will roll out
                            to all Audiomack creators in {currentDate + 1}.
                        </p>
                    </Fragment>
                )
            },
            {
                feature: 'unlimited',
                imageProps: {
                    src: '/static/images/desktop/about/create-unlimited.png',
                    srcSet:
                        '/static/images/desktop/about/create-unlimited@2x.png 2x',
                    alt: 'Unlimited everything - for Free'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        Unlimited everything -{' '}
                        <span className="u-text-orange">for Free</span>
                    </h3>
                ),
                copy: (
                    <p>
                        It's {new Date().getFullYear()} - no creator should be
                        paying $15 or more a month for storage space. Audiomack
                        gives you all the advanced features you need to manage
                        your career - including unlimited hosting, advanced
                        stats, powerful scheduling and content sharing tools,
                        and monetization - completely free.
                    </p>
                )
            },
            {
                feature: 'phones',
                imageProps: {
                    src: '/static/images/desktop/about/create-phones.png',
                    srcSet:
                        '/static/images/desktop/about/create-phones@2x.png 2x',
                    alt: 'iOS and Android apps for Free'
                },
                title: (
                    <h3 className="create-feature__title u-fw-700 u-lh-12">
                        iOS and Android apps{' '}
                        <span className="u-text-orange">for Free</span>
                    </h3>
                ),
                copy: (
                    <Fragment>
                        <p>
                            Downloaded over a million times in less than six
                            months since launch, our cross platform apps allow
                            you to discover, favorite, and share music on the
                            go.
                        </p>
                        <div className="create-feature__badges">
                            <a
                                className="create-feature__badge u-d-inline-block create-feature__badge--apple"
                                href="https://itunes.apple.com/us/app/audiomack/id921765888?ls=1&mt=8"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                <AppStoreIcon />
                            </a>
                            <a
                                className="create-feature__badge u-d-inline-block create-feature__badge--google"
                                href="https://play.google.com/store/apps/details?id=com.audiomack"
                                target="_blank"
                                rel="nofollow noopener"
                            >
                                <GooglePlayIcon />
                            </a>
                        </div>
                    </Fragment>
                )
            }
        ].map(({ feature, imageProps, title, copy }, i) => {
            const klass = `create-feature create-feature--${feature}`;
            const rowClass = classnames('row', {
                'flex-dir-row-reverse': i % 2 !== 0
            });

            return (
                <div key={i} className={klass}>
                    <div className={rowClass}>
                        <div className="create-feature__image u-padding-0 column small-24 large-12">
                            {/* eslint-disable-next-line jsx-a11y/alt-text */}
                            <img {...imageProps} />
                        </div>
                        <div className="create-feature__content column small-24 large-12 u-lh-15">
                            {title}
                            {copy}
                        </div>
                    </div>
                </div>
            );
        });

        return <div className="create-features">{mainFeatures}</div>;
    }

    renderCoverage() {
        const coverage = [
            {
                outlet: 'fader',
                link: 'http://www.thefader.com/2015/07/20/audiomack'
            },
            {
                outlet: 'forbes',
                link:
                    'https://www.forbes.com/sites/garysuarez/2015/08/03/chief-keef-bang-3-audiomack/#56305fbc1092'
            },
            {
                outlet: 'vice',
                link:
                    'https://noisey.vice.com/en_us/article/6e4pvp/heres-the-migos-performing-hannah-montana-with-a-symphony'
            },
            {
                outlet: 'mtv',
                link:
                    'http://www.mtv.com/news/2219751/migos-hannah-montana-symphony-orchestra/'
            },
            {
                outlet: 'thinknum',
                link:
                    'https://media.thinknum.com/articles/audiomack-is-quietly-growth-hacking-streaming-music/'
            },
            {
                outlet: 'billboard',
                link:
                    'https://www.billboard.com/articles/business/8493681/audiomack-from-mixtape-site-to-most-influential-streaming-services-around'
            }
        ].map(({ outlet, link }, i) => {
            const klass = `create-coverage u-pos-relative create-coverage--${outlet} column small-12 medium-8 large-4`;
            const imgPath = `/static/images/desktop/about/create-outlet-${outlet}.png`;

            return (
                <div className={klass} key={i}>
                    <a href={link} target="_blank" rel="nofollow noopener">
                        <img src={imgPath} alt={outlet} />
                    </a>
                </div>
            );
        });

        return (
            <div className="create-coverage-wrap row">
                <h3 className="create-feature__title u-fw-700 u-lh-12 create-feature__title u-fw-700 u-lh-12--coverage column u-padding-0 small-24">
                    <span className="u-text-orange">Media</span> Coverage
                </h3>
                <div className="expanded column small-24 u-d-flex u-d-flex--wrap u-padding-0">
                    {coverage}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="create">
                {this.renderHero()}
                <div className="create-main">
                    {this.renderArtistFeatures()}
                    {this.renderMainFeatures()}
                    {this.renderCoverage()}
                </div>
                <FooterDark playerActive={!!this.props.player.currentSong} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player
    };
}

export default compose(
    connect(mapStateToProps),
    hideSidebarForComponent
)(AboutPageContainer);
