import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { addToast } from '../redux/modules/toastNotification';
import { hideModal } from '../redux/modules/modal';

import LegalPage from './LegalPage';
import { sendLegalForm } from '../redux/modules/email';

class LegalPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onContactLinkClick: PropTypes.func,
        location: PropTypes.object,
        hideTitle: PropTypes.bool,
        email: PropTypes.object,
        history: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const data = {};
        const form = e.currentTarget;

        data.name = form.name.value;
        data.emailAddress = form.email.value;
        data.company = form.company.value;
        data.url = form.url.value;
        data.message = form.message.value;

        dispatch(sendLegalForm(data))
            .then(() => {
                const message =
                    'Your message has been sent, and we will respond within one business day.';

                dispatch(
                    addToast({
                        action: 'message',
                        item: message
                    })
                );

                dispatch(hideModal());
                return;
            })
            .catch((err) => console.log(err));
    };

    handleRenderError(error, key) {
        if (!error) {
            return null;
        }

        if (error.description) {
            return (
                <div>
                    <p className="auth__error" key={key}>
                        {error.description}
                    </p>
                </div>
            );
        }

        return (
            <div>
                <p className="auth__error" key={key}>
                    {error}
                </p>
            </div>
        );
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <LegalPage
                onFormSubmit={this.handleFormSubmit}
                onContactLinkClick={this.props.onContactLinkClick}
                onRenderError={this.handleRenderError}
                email={this.props.email}
                hideTitle={this.props.hideTitle}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.currentUser,
        email: state.email
    };
}

export default withRouter(connect(mapStateToProps)(LegalPageContainer));
