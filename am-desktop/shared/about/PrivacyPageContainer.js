import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Helmet from 'react-helmet';

import PrivacyContent from 'components/PrivacyContent';

export default class PrivacyPageContainer extends Component {
    componentDidMount() {
        this.maybeForceScroll();
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.location.hash !== this.props.location.hash &&
            !this._updatingHash
        ) {
            this.maybeForceScroll();
        }
    }

    maybeForceScroll() {
        const hash = window.location.hash;

        // Forces scroll
        if (hash) {
            this._updatingHash = true;

            window.location.hash = '';

            window.requestAnimationFrame(() => {
                window.location.hash = hash;
                this._updatingHash = false;
            });
        }
    }

    render() {
        const title = 'Privacy Policy on Audiomack';

        return (
            <div className="row music-feed" style={{ paddingBottom: 80 }}>
                <div className="column">
                    <div className="body-text">
                        <Helmet>
                            <title>{title}</title>
                        </Helmet>
                        <PrivacyContent />
                    </div>
                </div>
            </div>
        );
    }
}

PrivacyPageContainer.propTypes = {
    location: PropTypes.object
};
