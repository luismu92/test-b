import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

export default class LegalPage extends Component {
    static propTypes = {
        onFormSubmit: PropTypes.func,
        onContactLinkClick: PropTypes.func,
        onRenderError: PropTypes.func,
        inModal: PropTypes.bool,
        email: PropTypes.object
    };

    static defaultProps = {
        inModal: false
    };

    render() {
        const { inModal, email } = this.props;
        const errors = email.errors;

        let messages = {};

        if (errors.length !== 0) {
            if (errors[0].errors) {
                const errorObj =
                    errors && errors[0] && errors[0].errors
                        ? errors[0].errors
                        : {};

                messages = Object.keys(errorObj).reduce((obj, key) => {
                    const keyObj = errors[0].errors[key];
                    const keyObjKey = Object.keys(keyObj)[0];
                    const newObj = obj;

                    newObj[key] = {
                        description: keyObj[keyObjKey]
                    };

                    return newObj;
                }, {});
            } else {
                messages = errors[0];
            }
        }

        let title;

        if (!inModal) {
            title = <h2 className="auth__title">DMCA Notice form</h2>;
        }

        const containerClass = classnames('row', {
            'music-feed': !inModal
        });

        return (
            <div className={containerClass}>
                <div className="column small-24">
                    <div className="body-text">
                        {title}
                        <p>
                            Audiomack respects the rights of copyright holders
                            and will work with said copyright holders to ensure
                            that infringing material is removed from our
                            service. We monitor file uploads to make sure that
                            copyrighted material is not uploaded and proactively
                            ban any users that do not adhere to our terms of
                            service. In cases where you feel a file infringes on
                            your copyright or the copyright of someone you
                            represent, we encourage you to use this page to
                            notify us.
                        </p>

                        <p>
                            Audiomack will respond to any and all takedown
                            requests that comply with the requirements of the
                            Digital Millennium Copyright Act (DMCA), and other
                            applicable intellectual property laws.
                        </p>

                        <p>
                            If you believe that a file that a user has uploaded
                            to Audiomack infringes on your copyright then please
                            use the form below to submit a request. Be sure to
                            include your relationship to the owner of the
                            copyrighted work, your full contact info, and the
                            url of the song/album you are referring to.
                        </p>
                        <form
                            className="form u-spacing-top"
                            onSubmit={this.props.onFormSubmit}
                        >
                            <div className="form__input-group u-group">
                                <div className="form__input form__input--half">
                                    <input
                                        required
                                        type="text"
                                        name="name"
                                        placeholder="Your Name"
                                        autoComplete="name"
                                    />
                                </div>
                                <div className="form__input form__input--half">
                                    <input
                                        name="email"
                                        type="email"
                                        placeholder="Your email"
                                        autoComplete="email"
                                        autoCapitalize="none"
                                    />
                                </div>
                            </div>
                            <div className="form__input-group u-group">
                                <div className="form__input">
                                    <input
                                        required
                                        type="text"
                                        name="company"
                                        placeholder="Company"
                                        autoComplete="organization"
                                    />
                                </div>
                            </div>
                            <div className="form__input-group u-group">
                                <div className="form__input">
                                    <input
                                        required
                                        type="text"
                                        name="url"
                                        placeholder="URL of Infringing material. (Separate mulitple URLs with commas)"
                                    />
                                </div>
                            </div>
                            <div>
                                <textarea
                                    required
                                    name="message"
                                    placeholder="Message"
                                />
                            </div>
                            {this.props.onRenderError(messages.fromemail)}
                            {this.props.onRenderError(messages.infringeurl)}
                            {this.props.onRenderError(messages.message)}
                            {this.props.onRenderError(messages.name)}
                            <div className="auth__submit-wrap">
                                <input
                                    className="button button--pill auth__submit auth__btn"
                                    type="submit"
                                    value="Submit"
                                />
                            </div>
                        </form>
                    </div>
                    <div className="modal__footer column small-24 u-text-center">
                        <p>
                            Having trouble with your account?{' '}
                            <Link
                                to="/contact-us"
                                onClick={this.props.onContactLinkClick}
                                data-page="contact"
                            >
                                Contact us
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}
