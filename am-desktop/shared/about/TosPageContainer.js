import React, { Component } from 'react';

import TosContent from 'components/TosContent';

import Helmet from 'react-helmet';

export default class TosPageContainer extends Component {
    render() {
        const title = 'Terms of Service on Audiomack';

        return (
            <div className="row music-feed" style={{ marginBottom: 80 }}>
                <div className="column">
                    <div className="body-text">
                        <Helmet>
                            <title>{title}</title>
                        </Helmet>
                        <TosContent />
                    </div>
                </div>
            </div>
        );
    }
}
