/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Picture extends Component {
    render() {
        return (
            <svg { ...this.props }  width="19" height="16" viewBox="0 0 19 16"><path d="M14.61 3.478H1.39v10.435l4.22-2.782s2.26.894 3.55 1.714-.4-1.93-1.515-2.404c.71-.77 1.83-2.09 1.83-2.09s.655-.697 1.33-.697c.673 0 1.374.52 1.52.655.147.134 2.284 1.834 2.284 1.834V3.48zM16 2.696V15.39c0 .338-.272.61-.61.61H.61c-.34 0-.61-.272-.61-.61V2.697c0-.337.272-.61.61-.61h14.78c.338 0 .61.273.61.61zM4.174 7.782c.84 0 1.52-.683 1.52-1.52 0-.84-.68-1.522-1.52-1.522s-1.52.683-1.52 1.52c0 .84.68 1.522 1.52 1.522zM18.087.61v12.694c0 .337-.272.61-.61.61h-.78V1.39H2.782V.61c0-.338.272-.61.608-.61h14.09c.337 0 .61.272.61.61z"/></svg>
        );
    }
}
