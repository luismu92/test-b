/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Envelope extends Component {
    render() {
        return (
            <svg { ...this.props }  width="150.07" height="112.52" viewBox="345.35 241.59 150.07 112.52"><path d="M483.86 241.6H356.9c-6.37 0-11.55 5.18-11.55 11.55v89.4c0 6.37 5.18 11.56 11.55 11.56h126.96c6.38 0 11.56-5.18 11.56-11.55v-89.4c0-6.37-5.18-11.56-11.56-11.56zm-1.66 8.54l-61.03 52.16-61.02-52.16H482.2zm4.67 92.4c0 1.67-1.34 3.02-3 3.02H356.9c-1.66 0-3-1.35-3-3v-86.52l64.5 55.17c.04.05.08.1.12.1.04.03.08.06.12.06.08.08.16.12.24.16.04.04.08.04.08.08l.4.2c.03 0 .07.04.07.04l.32.12c.04 0 .08.04.12.04.08.04.2.08.28.08.04 0 .08.04.12.04.07.04.23.04.3.08h.1c.1 0 .27.04.4.04s.26 0 .38-.04h.08c.08 0 .24-.04.32-.08.04 0 .08-.04.12-.04.08-.04.2-.08.27-.08.04 0 .08-.04.12-.04l.32-.12c.04 0 .08-.04.08-.04l.4-.2c.03-.04.07-.04.07-.08.08-.08.16-.08.24-.15.04-.04.08-.08.12-.08.03-.05.07-.1.1-.1l63.17-53.82v85.17z"/></svg>
        );
    }
}
