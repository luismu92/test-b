/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Bump extends Component {
    render() {
        return (
            <svg { ...this.props }  viewBox="0 0 14 16"><path d="M0 0v2h14V0H0zm0 10h4v6h6v-6h4L7 3l-7 7z"/></svg>

        );
    }
}
