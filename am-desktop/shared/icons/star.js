/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Star extends Component {
    render() {
        return (
            <svg { ...this.props }  width="17" height="16" viewBox="246 0 17 16"><path d="M252.536 5.797H246l5.295 3.513-1.983 5.69 5.215-3.566 5.24 3.548-1.985-5.7L263 5.797h-6.442L254.564 0"/></svg>
        );
    }
}
