/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class ThreeDotsAlt extends Component {
    render() {
        return (
            <svg { ...this.props }  viewBox="0 0 81 20.4"><circle cx="10.2" cy="10.2" r="10.2"/><circle cx="40.8" cy="10.2" r="10.2"/><circle cx="70.8" cy="10.2" r="10.2"/></svg>

        );
    }
}
