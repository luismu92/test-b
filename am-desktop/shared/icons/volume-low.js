/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class VolumeLow extends Component {
    render() {
        return (
            <svg { ...this.props }  viewBox="0 0 13.5 16"><path d="M13.5 8c0-1.7-1-3.3-2.5-4v8c1.5-.7 2.5-2.2 2.5-4zM0 5v6h4l5 5V0L4 5H0z"/></svg>

        );
    }
}
