/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class TwitterLogoNew extends Component {
    render() {
        return (
            <svg { ...this.props }  width="18" height="15" viewBox="0 0 18 15"><path d="M17.647 1.776c-.65.3-1.347.504-2.08.596.748-.47 1.322-1.21 1.593-2.095-.7.434-1.475.75-2.3.92C14.2.46 13.26 0 12.218 0c-2 0-3.62 1.695-3.62 3.787 0 .296.03.585.093.863C5.686 4.492 3.018 2.984 1.23.693c-.313.56-.492 1.21-.492 1.904 0 1.314.64 2.473 1.61 3.152-.592-.02-1.15-.19-1.64-.478v.048c0 1.835 1.25 3.366 2.905 3.714-.304.086-.624.133-.954.133-.237 0-.46-.025-.685-.07.46 1.505 1.798 2.6 3.382 2.63-1.24 1.016-2.8 1.622-4.497 1.622-.292 0-.58-.02-.863-.055 1.6 1.073 3.504 1.7 5.55 1.7 6.66 0 10.3-5.77 10.3-10.774 0-.163-.004-.327-.01-.49.707-.533 1.32-1.2 1.806-1.96" fill="#2A2A2A" fillRule="evenodd"/></svg>

        );
    }
}
