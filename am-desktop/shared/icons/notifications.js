/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Notifications extends Component {
    render() {
        return (
            <svg { ...this.props }  viewBox="0 0 197 190"><path d="M36 95h42c1.7 0 3-1.3 3-3s-1.4-3-3-3H36c-1.7 0-3 1.3-3 3s1.3 3 3 3zm81 30H36c-1.7 0-3 1.4-3 3 0 1.7 1.3 3 3 3h81c1.7 0 3-1.3 3-3 0-1.6-1.3-3-3-3zm0-18H36c-1.7 0-3 1.3-3 3s1.3 3 3 3h81c1.7 0 3-1.3 3-3s-1.3-3-3-3z"/><path d="M197 22.5C197 10.1 186.9 0 174.5 0c-10.5 0-19.4 7.2-21.8 17H47.5c-9.7 0-17.7 7.9-17.7 17.7v24.1H17.7C7.9 58.8 0 66.7 0 76.4v63.1c0 9.8 7.9 17.7 17.7 17.7h18.1V187c0 1.2.8 2.3 1.9 2.8.3.1.7.2 1.1.2.8 0 1.6-.3 2.2-1l28.9-31.8h61.5c9.7 0 17.7-7.9 17.7-17.7v-24.1h12.2c9.7 0 17.7-7.9 17.7-17.7V44.5c10.3-2 18-11.1 18-22zm-53.8 117c0 6.5-5.3 11.7-11.7 11.7H68.6c-.8 0-1.6.3-2.2 1l-24.7 27.1v-25.1c0-1.7-1.3-3-3-3h-21C11.2 151.2 6 146 6 139.5V76.4c0-6.5 5.3-11.7 11.7-11.7h113.8c2.4 0 4.7.7 6.5 2 3.1 2.1 5.2 5.7 5.2 9.7v63.1zM173 97.7c0 6.5-5.3 11.7-11.7 11.7h-12.2v-33c0-7.9-5.2-14.7-12.4-16.9-.6-.2-1.1-.3-1.7-.4-1.2-.2-2.3-.4-3.6-.4H35.8v-24c0-6.5 5.3-11.7 11.7-11.7H152c.2 11.7 9.4 21.2 21 22v52.7z"/></svg>

        );
    }
}
