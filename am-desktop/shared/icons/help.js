/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Help extends Component {
    render() {
        return (
            <svg { ...this.props }  width="15" height="14" viewBox="16 140 15 14"><path d="M24.628 146.904c-.552.36-.717.67-.736 1.385 0 .076-.065.14-.143.14h-1.465c-.04 0-.143-.106-.143-.145v-.583c0-.61.33-1.148 1.01-1.644.052-.038.56-.37.56-.83 0-.38-.286-.645-.696-.645-.584 0-.91.293-.94.852-.006.075-.068.134-.144.134h-1.538c-.04 0-.077-.016-.104-.044-.027-.028-.04-.067-.04-.106.08-1.55 1.09-2.44 2.77-2.44 1.272 0 2.644.69 2.644 2.207 0 1.017-.232 1.21-1.036 1.717zM23 140c-3.866 0-7 3.134-7 7s3.134 7 7 7 7-3.134 7-7-3.134-7-7-7zm1.17 11.286c0 .157-.13.285-.286.285h-1.74c-.16 0-.287-.126-.287-.283v-1.715c0-.156.128-.284.286-.284h1.74c.158 0 .287.128.287.285v1.716z"/></svg>
        );
    }
}
