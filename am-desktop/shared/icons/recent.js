/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Recent extends Component {
    render() {
        return (
            <svg { ...this.props }  viewBox="0 0 29.9 29.9"><path d="M0 5.6v22.2c0 1.1.9 2 2 2h22.2v-3.1H5.1c-1.1 0-2-.9-2-2V5.6H0z"/><path d="M27.9 0H8C6.9 0 6 .9 6 2v19.6c0 1.1.9 2 2 2h19.9c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zm-2.4 13.2h-6.1v6.1h-2.9v-6.1h-6.1v-2.9h6.1V4.2h2.9v6.1h6.1v2.9z"/></svg>
        );
    }
}
