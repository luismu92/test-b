/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Flag extends Component {
    render() {
        return (
            <svg { ...this.props }  width="42" height="50" viewBox="0 0 42 50"><path d="M9.743 2.975C8.74 2.51 8 2.895 8 4v22c0 1.104.74 2.51 1.743 2.975C11.63 29.85 15.05 31 20 31c7.517 0 10.62-6.18 20.095-3.608C41.16 27.682 42 27.104 42 26V4c0-1.104-.84-2.318-1.905-2.608C30.62-1.18 27.517 5 20 5c-4.95 0-8.37-1.15-10.257-2.025M6 2.5V47h-.06c.038 0 .06-.172.06 0 0 1.38-1.344 2.25-3 2.25s-3-.744-3-2.125C0 46.953.02 47 .06 47H0V2.5C0 1.12 1.344 0 3 0s3 1.12 3 2.5"/></svg>
        );
    }
}
