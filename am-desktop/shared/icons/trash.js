/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class Trash extends Component {
    render() {
        return (
            <svg { ...this.props }  viewBox="0 0 14 16"><path d="M9 2V0H5v2H0v3.4h.9L2 16h10l1-10.5h1V2H9zM6 .9h2V2H6V.9zm5.2 14.2H2.9l-1-9.6h10.2l-.9 9.6zM13 4.5H1V3h12v1.5z"/><path d="M6.5 13.2h1V7.3h-1v5.9zM10 7.3l-.3 5.9h-.9l.3-5.9zM5.2 13.2h-1L4 7.3h.9l.3 5.9z"/></svg>

        );
    }
}
