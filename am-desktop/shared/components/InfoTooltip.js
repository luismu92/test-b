import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import QuestionIcon from '../icons/question-mark';

import styles from './InfoTooltip.module.scss';

export default function InfoTooltip({ text, className, style, clickable }) {
    const klass = classnames(styles.container, {
        [className]: className
    });

    let popover;
    if (clickable) {
        popover = (
            <span className={`tooltip tooltip--down-arrow ${styles.popover}`}>
                {text}
            </span>
        );
    }

    return (
        <span
            className={klass}
            style={style}
            data-tooltip={clickable ? null : text}
        >
            <QuestionIcon />
            {popover}
        </span>
    );
}

InfoTooltip.propTypes = {
    style: PropTypes.object,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    className: PropTypes.string,
    clickable: PropTypes.bool
};
