import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Wrapper, Button, Menu, MenuItem } from 'react-aria-menubutton';

import styles from './TagListDropdown.module.scss';

export default class TagInput extends Component {
    static propTypes = {
        initialHeight: PropTypes.number,
        inputProps: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }).isRequired,
        nestedOptionDelimiter: PropTypes.string,
        onDropdownChange: PropTypes.func,
        onTagListChange: PropTypes.func,
        onTagListResize: PropTypes.func,
        options: PropTypes.arrayOf(
            PropTypes.shape({
                text: PropTypes.string,
                value: PropTypes.value
            })
        ),
        tagListAdditionalHeight: PropTypes.number,
        tags: PropTypes.array.isRequired
    };

    static defaultProps = {
        initialHeight: 42
    };

    constructor(props) {
        super(props);

        this.state = {
            checked: {},
            showOptions: false,
            tagInput: ''
        };

        this.input = React.createRef();
        this.tagList = React.createRef();
    }

    componentDidMount() {
        this.handleTagListResize();
    }

    componentDidUpdate(prevProps, prevState) {
        const { initialHeight } = this.props;
        const { tagInput } = this.state;
        const tagListAdditionalHeight =
            this.tagList.current.clientHeight - initialHeight;

        if (
            prevProps.tagListAdditionalHeight !== tagListAdditionalHeight ||
            prevState.tagInput !== tagInput
        ) {
            this.handleTagListResize();
        }
    }

    handleDropdownChange = (textValue, e) => {
        const { onDropdownChange } = this.props;
        const text = e.target.getAttribute('data-text');
        // const normalizedValue = value.replace(/[\s\-]/g, '').toLowerCase();

        this.setState((prevState) => ({
            checked: {
                ...prevState.checked,
                [text]: !prevState.checked[text]
            }
        }));

        onDropdownChange(textValue, e);
    };

    handleFocusInput = () => {
        this.input.current.focus();
    };

    handleHideOptions = (e) => {
        if (e.currentTarget.contains(e.relatedTarget)) {
            return;
        }

        this.setState({ showOptions: false });
    };

    handleShowOptions = () => {
        this.setState({ showOptions: true });
    };

    handleTagListResize = () => {
        const { initialHeight, inputProps, onTagListResize } = this.props;

        if (!onTagListResize || !this.tagList) {
            return;
        }

        const { id, name } = inputProps;
        const tagListAdditionalHeight =
            this.tagList.current.clientHeight - initialHeight;

        onTagListResize(id, name, tagListAdditionalHeight);
    };

    handleTagsInputBlur = (e) => {
        const { options } = this.props;

        if (options) {
            return;
        }

        this.handleTagsOnChange(e, 'add');

        this.setState({
            tagInput: ''
        });
    };

    handleTagsInputChange = (e) => {
        this.setState(
            {
                tagInput: e.currentTarget.value
            },
            this.handleTagsOnChange(e)
        );
    };

    handleTagsInputKeyDown = (e) => {
        const { nestedOptionDelimiter, options, tags } = this.props;
        const { checked, tagInput } = this.state;

        if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();

            if (!tagInput) {
                return;
            }

            let tag = tagInput.trim();

            if (options) {
                const hasNestedOptions = nestedOptionDelimiter;

                const flatten = (items) => {
                    const rootItems = new Set(items.map((item) => item.value));

                    return items.reduce((acc, curr) => {
                        if (
                            !checked[curr.value] &&
                            !rootItems.has(curr.value)
                        ) {
                            return acc;
                        }

                        acc.push(curr);

                        if (checked[curr.value] && curr.hasChildren) {
                            acc.push(...flatten(curr.children));
                        }

                        return acc;
                    }, []);
                };

                const allOptions = hasNestedOptions
                    ? flatten(options)
                    : options;

                const closestMatch = allOptions.find((option) => {
                    return (
                        !checked[option.value] &&
                        !tags.includes(option.text) &&
                        !tags.includes(option.value) &&
                        option.text
                            .toLowerCase()
                            .startsWith(tagInput.toLowerCase())
                    );
                });

                if (!closestMatch) {
                    return;
                }

                tag = closestMatch.text;

                if (closestMatch.value.includes(nestedOptionDelimiter)) {
                    tag = closestMatch.value;
                }
            }

            this.handleTagsOnChange(e, 'add', tag);

            this.setState({
                tagInput: ''
            });
        } else if (e.key === 'Backspace' && !tagInput) {
            e.preventDefault();

            if (!tags.length) {
                return;
            }

            const lastTag = tags[tags.length - 1];
            const lastTagArr = lastTag.split(nestedOptionDelimiter);
            const lastTagComponent = lastTagArr[lastTagArr.length - 1];

            this.handleTagsOnChange(e, 'remove', lastTag);

            this.setState({
                tagInput: lastTagComponent || ''
            });
        } else if (e.key === ' ') {
            // disallow consecutive whitespace
            const lastChar = tagInput[tagInput.length - 1];

            if (lastChar === ' ') {
                e.preventDefault();
            }
        }
    };

    handleTagsOnChange = (e, changeType, tag) => {
        const { inputProps, onTagListChange, tags } = this.props;
        const { name } = inputProps;
        let value = [...tags];

        if (tag) {
            switch (changeType) {
                case 'add': {
                    // remove duplicate tags by using set and converting back to array
                    value = [...new Set(value).add(tag)];

                    break;
                }

                case 'remove': {
                    const tagSet = new Set(value);

                    tagSet.delete(tag);
                    value = [...tagSet];

                    break;
                }

                default:
                    break;
            }
        }

        value = value.join();

        // this makes it so that we can use existing input change handlers (for songs, albums, etc.) without modifying them
        const event = {
            ...e,
            currentTarget: {
                ...e.currentTarget,
                type: 'tags',
                name,
                value
            }
        };

        onTagListChange(event);
    };

    renderTagOptions(children) {
        const { showOptions, tagInput } = this.state;
        const { tags } = this.props;
        const { name } = this.props.inputProps;
        const options = children || this.props.options;

        if (!options || !showOptions) {
            return null;
        }

        const tagOptions = options.reduce((acc, curr, i) => {
            const checked =
                this.state.checked[curr.text] ||
                Boolean(tags.filter((tag) => tag.startsWith(curr.text)).length);

            if (
                checked ||
                curr.text.toLowerCase().startsWith(tagInput.toLowerCase())
            ) {
                const element = (
                    <MenuItem
                        data-name={name}
                        data-text={curr.text}
                        data-value={curr.value}
                        data-index={i}
                        key={i}
                        tag="button"
                    >
                        {curr.text}
                    </MenuItem>
                );

                acc.push(element);
            }

            return acc;
        }, []);

        return tagOptions;
    }

    render() {
        const { inputProps, nestedOptionDelimiter, tags } = this.props;
        const { tagInput } = this.state;

        const tagInputProps = {
            ...inputProps,
            onBlur: this.handleTagsInputBlur,
            onChange: this.handleTagsInputChange,
            onFocus: this.handleShowOptions,
            onKeyDown: this.handleTagsInputKeyDown,
            type: 'text',
            value: tagInput
        };

        const tagListItems = tags.map((tag, i) => {
            const nestedTagArr = tag.split(nestedOptionDelimiter);
            const text = nestedTagArr[nestedTagArr.length - 1];

            return (
                <span className={styles.tagListItem} key={`${i}:${tag}`}>
                    {text}
                </span>
            );
        });

        return (
            <Wrapper
                data-name={inputProps.name}
                onSelection={this.handleDropdownChange}
                ref={this.tagList}
            >
                <Button className={styles.tagList}>
                    {tagListItems}
                    <input
                        {...tagInputProps}
                        autoComplete="off"
                        ref={this.input}
                    />
                </Button>
                <Menu className={styles.tagOptions} tag="ul">
                    {this.renderTagOptions()}
                </Menu>
            </Wrapper>
        );
    }
}
