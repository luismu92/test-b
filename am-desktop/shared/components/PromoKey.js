import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import DeleteIcon from '../icons/delete';
import EmbedIcon from '../icons/embed-close';

import styles from './PromoKey.module.scss';

export default class PromoKey extends Component {
    static propTypes = {
        url: PropTypes.string.isRequired,
        promoKey: PropTypes.object.isRequired,
        onDeleteKeyClick: PropTypes.func.isRequired,
        index: PropTypes.number,
        showWpCode: PropTypes.bool,
        onToggleCodeClick: PropTypes.func,
        iframeCode: PropTypes.string,
        wpCode: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            hideEmbed: true
        };
    }

    handleEmbedTextClick = () => {
        this.focusArea();
    };

    handleUrlTextClick = () => {
        this._promoUrlInput.focus();
        this._promoUrlInput.select();
    };

    handleEmbedCodeClick = () => {
        const currentEmbed = this.state.hideEmbed;

        this.setState({ hideEmbed: !currentEmbed });
        return true;
    };

    focusArea() {
        this._embedTextarea.focus();
        this._embedTextarea.select();
    }

    render() {
        const {
            promoKey,
            onDeleteKeyClick,
            url,
            index,
            showWpCode,
            onToggleCodeClick,
            iframeCode,
            wpCode
        } = this.props;

        const toggleText = showWpCode ? 'Iframe Code?' : 'Wordpress Shortcode?';
        const embedCode = showWpCode ? wpCode : iframeCode;

        return (
            <div className={styles.row}>
                <div className={styles.rowInner}>
                    <div className={classnames(styles.item, styles.itemName)}>
                        <a href={url} target="_blank" rel="nofollow noopener">
                            {promoKey.key}
                        </a>
                    </div>
                    <div className={classnames(styles.item, styles.itemLink)}>
                        <input
                            type="text"
                            readOnly
                            value={url}
                            onClick={this.handleUrlTextClick}
                            ref={(input) => {
                                if (input) {
                                    this._promoUrlInput = input;
                                }
                            }}
                        />
                    </div>
                    <div className={classnames(styles.item, styles.itemPlays)}>
                        <span>{promoKey.plays}</span>
                    </div>
                    <div
                        className={classnames(styles.item, styles.itemActions)}
                    >
                        <button
                            className={styles.action}
                            data-tooltip="Delete Link"
                            onClick={onDeleteKeyClick}
                            data-key={promoKey.key}
                            data-index={index}
                        >
                            <DeleteIcon />
                        </button>
                    </div>
                    <div
                        className={classnames(styles.item, styles.itemActions)}
                    >
                        <button
                            className={styles.action}
                            data-tooltip="Embed Code"
                            onClick={this.handleEmbedCodeClick}
                        >
                            <EmbedIcon />
                        </button>
                    </div>
                </div>
                <div className={styles.embed} hidden={this.state.hideEmbed}>
                    <textarea
                        value={embedCode}
                        readOnly
                        onClick={this.handleEmbedTextClick}
                        ref={(input) => {
                            if (input) {
                                this._embedTextarea = input;
                            }
                        }}
                    />
                    <div className="u-text-right u-spacing-bottom-5">
                        <button
                            onClick={onToggleCodeClick}
                            className="u-fs-11 u-fw-700"
                        >
                            {toggleText}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
