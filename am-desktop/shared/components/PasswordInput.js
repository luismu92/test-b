import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PasswordHide from '../icons/eye-hide';
import PasswordView from '../icons/eye-view';

export default class PasswordInput extends Component {
    static propTypes = {
        label: PropTypes.string,
        id: PropTypes.string,
        inputProps: PropTypes.object,
        showLabels: PropTypes.bool
    };

    static defaultProps = {
        label: 'Password',
        id: 'password',
        showLabels: false,
        inputProps: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            showPassword: false
        };
    }

    handleEyeClick = () => {
        this.setState((prevState) => {
            return {
                ...prevState,
                showPassword: !prevState.showPassword
            };
        });
    };

    render() {
        const { label, id, showLabels, inputProps } = this.props;
        const { showPassword } = this.state;

        let inputType = 'password';
        let passwordIcon = <PasswordView />;

        if (showPassword) {
            inputType = 'text';
            passwordIcon = <PasswordHide />;
        }

        const eyeClass = classnames('form__password-toggle', {
            'form__password-toggle--hide': showPassword
        });

        return (
            <div className="u-pos-relative">
                {showLabels && <label htmlFor={id}>{label}</label>}
                <input
                    type={inputType}
                    name="password"
                    id={id}
                    data-testid="password"
                    autoComplete="current-password"
                    placeholder={!showLabels ? label : null}
                    {...inputProps}
                />
                <button
                    className={eyeClass}
                    onClick={this.handleEyeClick}
                    tabIndex="-1"
                    type="button"
                    aria-label={`${showPassword ? 'Hide' : 'Show'} Password`}
                >
                    {passwordIcon}
                </button>
            </div>
        );
    }
}
