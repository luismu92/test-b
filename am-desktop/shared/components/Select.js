import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import DropdownIcon from '../icons/dropdown-arrow';

export default class Select extends Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        options: PropTypes.array.isRequired,
        defaultValue: PropTypes.string,
        value: PropTypes.string,
        className: PropTypes.string,
        selectProps: PropTypes.object,
        onChange: PropTypes.func,
        containerProps: PropTypes.object
    };

    static defaultProps = {
        selectProps: {}
    };

    renderOptions(options = []) {
        return options.map((option, i) => {
            return (
                <option key={i} value={option.value} disabled={option.disabled}>
                    {option.text}
                </option>
            );
        });
    }

    render() {
        const {
            options,
            name,
            value,
            defaultValue,
            selectProps,
            onChange,
            containerProps,
            className
        } = this.props;
        const klass = classnames('select', {
            [className]: className
        });

        return (
            <div className={klass} {...containerProps}>
                {/* eslint-disable-next-line jsx-a11y/no-onchange */}
                <select
                    name={name}
                    onChange={onChange}
                    defaultValue={defaultValue}
                    value={value}
                    {...selectProps}
                >
                    {this.renderOptions(options)}
                </select>
                <DropdownIcon className="select__arrow" />
            </div>
        );
    }
}
