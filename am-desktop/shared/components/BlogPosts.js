import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import AndroidLoader from '../loaders/AndroidLoader';

export default class BlogPosts extends Component {
    static propTypes = {
        blogPost: PropTypes.object
    };

    componentDidMount() {
        this._testEl = document.createElement('div');
    }

    componentWillUnmount() {
        this._testEl = null;
    }

    renderLoader(loading) {
        if (!loading) {
            return null;
        }

        return (
            <div className="dashboard-panel__loader">
                <AndroidLoader />
            </div>
        );
    }

    renderError(error) {
        if (!error) {
            return null;
        }

        return <p>There was an error loading the Audiomack blog posts.</p>;
    }

    renderPosts(list = []) {
        return list.slice(0, 4).map((item, i) => {
            const { content, title, url, image } = item;

            if (!content) {
                return null;
            }

            const excerpt = content.substr(0, 165);
            const imageStyle = {
                backgroundImage: `url(${image})`
            };

            return (
                <a
                    href={url}
                    className="blog-post column small-12 large-6"
                    key={i}
                    rel="noopener"
                    target="_blank"
                >
                    <div
                        className="blog-post__image"
                        style={imageStyle}
                        aria-label={`Image for blog post: "${title}"`}
                    />
                    <h4 className="blog-post__title">{title}</h4>
                    <p className="blog-post__content">{excerpt}...</p>
                </a>
            );
        });
    }

    render() {
        const { list, loading, error } = this.props.blogPost;

        return (
            <section className="blog-posts-container u-spacing-top">
                <div className="blog-posts__header row">
                    <h2 className="blog-posts-container__header column small-12">
                        Audiomack Blog
                    </h2>
                    <Link className="column small-12 u-text-right" to="/world">
                        Read more
                    </Link>
                </div>
                {this.renderLoader(loading && !error)}
                {this.renderError(error)}
                <div className="blog-posts row">{this.renderPosts(list)}</div>
            </section>
        );
    }
}
