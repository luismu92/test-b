import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { AutoSizer, Table, Column, SortDirection } from 'react-virtualized';
import classnames from 'classnames';

export default class TableLarge extends PureComponent {
    static propTypes = {
        items: PropTypes.array,
        columns: PropTypes.array,
        headerHeight: PropTypes.number,
        height: PropTypes.number,
        rowHeight: PropTypes.number,
        loading: PropTypes.bool,
        initSort: PropTypes.string,
        emptyMessage: PropTypes.string,
        scrollToIndex: PropTypes.number
    };

    static defaultProps = {
        items: [],
        columns: [],
        headerHeight: 30,
        height: 600,
        rowHeight: 40,
        loading: true,
        scrollToIndex: 0
    };

    constructor(props) {
        super(props);

        const { initSort } = this.props;

        const sortBy = initSort;
        const sortDirection = SortDirection.DESC;
        const sortedList = this.sortList({ sortBy, sortDirection });

        this.state = {
            sortBy,
            sortDirection,
            sortedList
        };
    }

    componentDidUpdate() {
        // This is very important. Otherwise, the table won't be freshed
        this.sort({
            sortBy: this.state.sortBy,
            sortDirection: this.state.sortDirection
        });
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    ////////////////////
    // Helper methods //
    ////////////////////
    getRowData(sortedList, index) {
        return sortedList[index];
    }

    rowGetter = ({ index }) => {
        const { sortedList } = this.state;

        return this.getRowData(sortedList, index);
    };

    noRowsRenderer = () => {
        const { emptyMessage } = this.props;
        const noRowTest = emptyMessage ? emptyMessage : 'No rows';

        return (
            <div className="u-spacing-top-20 u-padding-x-10">{noRowTest}</div>
        );
    };

    headerRenderer({ dataKey, sortBy, sortDirection, label }) {
        const arrowClass = classnames('table-sort-arrow', {
            'table-sort-arrow--ascending':
                sortBy === dataKey && sortDirection === SortDirection.ASC
        });

        return (
            <div>
                {label}
                {sortBy === dataKey ? <span className={arrowClass} /> : null}
            </div>
        );
    }

    rowClassName({ index }) {
        if (index < 0) {
            return 'headerRow';
        }

        return index % 2 === 0 ? 'evenRow' : 'oddRow';
    }

    sort = ({ sortBy, sortDirection }) => {
        const sortedList = this.sortList({ sortBy, sortDirection });

        this.setState({ sortBy, sortDirection, sortedList });
    };

    sortList = ({ sortBy, sortDirection }) => {
        const { items } = this.props;

        return items.sort(function(a, b) {
            return sortDirection === SortDirection.ASC
                ? a[sortBy] - b[sortBy]
                : b[sortBy] - a[sortBy];
        });
    };

    render() {
        const { loading } = this.props;

        if (loading) {
            return <div>Loading...</div>;
        }

        const {
            columns,
            headerHeight,
            height,
            rowHeight,
            scrollToIndex
        } = this.props;
        const { sortedList, sortBy, sortDirection } = this.state;
        const columnsInTable = columns.map((column, i) => {
            return (
                <Column
                    key={i}
                    dataKey={column.dataKey}
                    label={column.label}
                    headerRenderer={this.headerRenderer}
                    width={column.width || 100}
                    disableSort={!column.sortable}
                    cellRenderer={column.render}
                />
            );
        });

        return (
            <AutoSizer disableHeight>
                {({ width }) => (
                    <Table
                        ref={(ref) => (this.tableRef = ref)}
                        headerHeight={headerHeight}
                        height={height}
                        noRowsRenderer={this.noRowsRenderer}
                        rowClassName={this.rowClassName}
                        rowHeight={rowHeight}
                        rowGetter={this.rowGetter}
                        rowCount={sortedList.length}
                        sort={this.sort}
                        sortBy={sortBy}
                        sortDirection={sortDirection}
                        width={width}
                        scrollToIndex={scrollToIndex}
                    >
                        {columnsInTable}
                    </Table>
                )}
            </AutoSizer>
        );
    }
}
