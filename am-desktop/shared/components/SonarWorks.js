import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CloseIcon from '../icons/close-thin';

import analyics, { eventCategory, eventAction } from 'utils/analytics';

export default class SonarWorks extends Component {
    static propTypes = {
        linkHref: PropTypes.string.isRequired,
        eventLabel: PropTypes.string.isRequired,
        className: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };
    }

    handlePartnerClick = () => {
        const { eventLabel } = this.props;

        analyics.track(eventCategory.ad, {
            eventAction: eventAction.sonar,
            eventLabel: eventLabel
        });
    };

    handleClosePartnerClick = () => {
        this.setState({ visible: false });
    };

    render() {
        const { linkHref, className } = this.props;
        const klass = classnames('partner partner--sonarworks', {
            [className]: className
        });

        if (!this.state.visible) {
            return null;
        }

        const ClosePartner = (
            <CloseIcon
                className="partner__close"
                onClick={this.handleClosePartnerClick}
            />
        );

        const layout = (
            <div className={klass}>
                <span className="partner__sponsor-tag">Advertisement</span>
                {ClosePartner}
                <a
                    onClick={this.handlePartnerClick}
                    href={linkHref}
                    target="_blank"
                    rel="nofollow noopener"
                >
                    <img
                        src="/static/images/desktop/ads/sonarworks-dashboard.jpg"
                        srcSet="/static/images/desktop/ads/sonarworks-dashboard@2x.jpg 2x"
                        alt="Sonarworks"
                    />
                </a>
            </div>
        );

        return layout;
    }
}
