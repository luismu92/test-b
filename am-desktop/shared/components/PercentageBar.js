import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import InfoTooltip from '../components/InfoTooltip';

import styles from './PercentageBar.module.scss';

function PercentageBar({
    label,
    subLabel,
    value = 0,
    height,
    small,
    helper,
    labelLink
}) {
    const borderRadius = Math.round(height / 2);
    const barStyle = {
        width: `${value}%`,
        borderRadius: borderRadius
    };
    const labelStyle = {};
    const trackStyle = {
        height: height,
        borderRadius: borderRadius
    };

    let barLabel;

    if (small) {
        labelStyle.fontSize = 12;
    }

    let barSublabel;

    if (subLabel) {
        barSublabel = <span className={styles.subLabel}>{subLabel}</span>;
    }

    let tooltip;

    if (helper) {
        tooltip = (
            <InfoTooltip
                text={helper}
                style={{ transform: 'translate(5px, -5px)' }}
            />
        );
    }

    let link;
    if (labelLink) {
        link = (
            <Link className={styles.link} to={labelLink}>
                More Details
            </Link>
        );
    }

    if (label) {
        barLabel = (
            <p className={styles.label} style={labelStyle}>
                <span>
                    {label}
                    {link}
                    {tooltip}
                </span>
                <span className={styles.value}>
                    {barSublabel}
                    {value}%
                </span>
            </p>
        );
    }

    return (
        <Fragment>
            {barLabel}
            <div className={styles.track} style={trackStyle}>
                <span className={styles.bar} style={barStyle} />
            </div>
        </Fragment>
    );
}

PercentageBar.propTypes = {
    label: PropTypes.string,
    subLabel: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.number,
    small: PropTypes.bool,
    helper: PropTypes.string,
    labelLink: PropTypes.string
};

PercentageBar.defaultProps = {
    height: 10
};

export default PercentageBar;
