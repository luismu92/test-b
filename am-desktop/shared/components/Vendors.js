import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import analyics, { eventCategory, eventAction } from 'utils/analytics';

export default class Vendors extends Component {
    static propTypes = {
        eventLabel: PropTypes.string.isRequired,
        className: PropTypes.string
    };

    constructor(props) {
        super(props);
    }

    handleLandrClick = () => {
        const { eventLabel } = this.props;

        analyics.track(eventCategory.ad, {
            eventAction: eventAction.landr,
            eventLabel: eventLabel
        });
    };

    handleHiveClick = () => {
        const { eventLabel } = this.props;

        analyics.track(eventCategory.ad, {
            eventAction: eventAction.hive,
            eventLabel: eventLabel
        });
    };

    render() {
        const { className } = this.props;
        const klass = classnames('vendors-wrap', {
            [className]: className
        });

        const landrBlock = (
            <div className="vendor vendor--landr u-box-shadow">
                <a
                    href="http://www.landr.com/join/AUDIOMACK2?utm_source=Audiomack&utm_medium=partner&utm_campaign=SignUp&utm_content=lp_qualityspeed"
                    target="_blank"
                    rel="nofollow noopener"
                    onClick={this.handleLandrClick}
                >
                    <div className="vendor__logo">
                        <img
                            src="/static/images/desktop/landr-logo.png"
                            srcSet="/static/images/desktop/landr-logo@2x.png 2x"
                            alt="Landr"
                        />
                    </div>
                    <div className="vendor__content u-text-center">
                        <p>
                            Sound like a pro - get your next upload mastered
                            instantly at a fraction of the cost of studio
                            Mastering. <br />
                            <strong>
                                Get 2 free high-quality WAV masters here.
                            </strong>
                        </p>
                    </div>
                </a>
            </div>
        );

        const hiveBlock = (
            <div className="vendor vendor--hive u-box-shadow">
                <a
                    href="https://www.hive.co/l/amack"
                    target="_blank"
                    rel="nofollow noopener"
                    onClick={this.handleHiveClick}
                >
                    <div className="vendor__logo">
                        <img
                            src="/static/images/desktop/hive-logo.png"
                            srcSet="/static/images/desktop/hive-logo@2x.png 2x"
                            alt="Hive"
                        />
                    </div>
                    <div className="vendor__content u-text-center">
                        <p>
                            Convert listeners into loyal fans - using email,
                            content giveaways, and advanced segmenting and
                            messaging - <strong>with Hive.</strong>
                        </p>
                    </div>
                </a>
            </div>
        );

        return (
            <div className={klass}>
                {landrBlock}
                {hiveBlock}
            </div>
        );
    }
}
