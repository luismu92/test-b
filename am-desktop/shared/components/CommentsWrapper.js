import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setCommentContext, getComments } from '../redux/modules/comment';

import { ORDER_BY_VOTE } from 'constants/comment';

import CommentsContainer from 'components/CommentsContainer';

class CommentsWrapper extends Component {
    static propTypes = {
        item: PropTypes.object,
        kind: PropTypes.string,
        id: PropTypes.number,
        total: PropTypes.number,
        className: PropTypes.string,
        comment: PropTypes.object,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        singleCommentUuid: PropTypes.string,
        singleCommentThread: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            sortTooltipActive: false,
            expanded: null,
            activeCommentContext: ORDER_BY_VOTE
        };
    }

    handleCommentSortClick = () => {
        this.setState({
            sortTooltipActive: !this.state.sortTooltipActive
        });
    };

    handleCommentSort = (e) => {
        const {
            dispatch,
            kind,
            id,
            singleCommentThread,
            singleCommentUuid
        } = this.props;
        const button = e.currentTarget;
        const value = button.getAttribute('data-action');

        dispatch(setCommentContext(kind, id, value));
        dispatch(getComments(singleCommentUuid, singleCommentThread));

        this.setState({
            expanded: true,
            activeCommentContext: value
        });
    };

    render() {
        return (
            <CommentsContainer
                item={this.props.item}
                kind={this.props.kind}
                id={this.props.id}
                total={this.props.total}
                className={this.props.className}
                comment={this.props.comment}
                currentUser={this.props.currentUser}
                dispatch={this.props.dispatch}
                sortTooltipActive={this.state.sortTooltipActive}
                onCommentSortClick={this.handleCommentSortClick}
                onCommentSort={this.handleCommentSort}
                activeCommentContext={this.state.activeCommentContext}
                singleCommentUuid={this.props.singleCommentUuid}
                singleCommentThread={this.props.singleCommentThread}
                commentsExpanded
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        comment: state.comment,
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(CommentsWrapper);
