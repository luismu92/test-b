import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Accordion extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        content: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
    };

    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    handleButtonClick = () => {
        this.setState({
            open: !this.state.open
        });
    };

    render() {
        const klass = classnames('accordion', {
            'accordion--closed': !this.state.open,
            'accordion--open': this.state.open
        });

        return (
            <div className={klass}>
                <button
                    className="accordion__title u-ls-n-05"
                    onClick={this.handleButtonClick}
                >
                    <h4>{this.props.title}</h4>
                </button>
                <div className="accordion__content u-lh-14 u-ls-n-025">
                    {this.props.content}
                </div>
            </div>
        );
    }
}
