import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import CheckMarkIcon from '../icons/check-mark';

import styles from './ArtistValidationAlert.module.scss';

export default class ArtistValidationAlert extends Component {
    static propTypes = {
        page: PropTypes.string.isRequired,
        currentUser: PropTypes.object
    };

    renderDefaultMessage() {
        return (
            <Fragment>
                <div className={styles.messageText}>
                    <strong>Are you a creator? {'  '}</strong>
                    <span className={styles.messageSubtext}>
                        Authenticate your account now to distinguish yourself as
                        a creator
                        <span>
                            <CheckMarkIcon
                                className={classnames(
                                    styles.iconRoundMiniGray,
                                    'verified u-text-icon'
                                )}
                            />
                        </span>
                        and access exclusive tools and services.
                    </span>
                </div>
                <Link
                    to={`/creators?utm_content=${this.props.page}`}
                    className={styles.btnRightOrange}
                >
                    Apply Now
                </Link>
            </Fragment>
        );
    }

    renderContent(message) {
        let dataMessage;

        switch (message) {
            case 'yes':
            case 'verified-pending':
            case 'verified-declined':
            case 'tastemaker':
                dataMessage = (
                    <Fragment>
                        <span
                            className={classnames(
                                styles.messageText,
                                'u-text-center'
                            )}
                        >
                            <CheckMarkIcon
                                className={classnames(
                                    styles.iconRoundOrange,
                                    'verified u-text-icon'
                                )}
                            />
                            <strong>
                                Congratulations! You are now a Verified Creator
                                on Audiomack
                            </strong>
                        </span>
                    </Fragment>
                );
                break;

            case 'authenticated-pending':
                dataMessage = (
                    <Fragment>
                        <span className={styles.messageText}>
                            <strong>Your application is pending...</strong>
                        </span>
                        <div className={styles.btnRightGray}>
                            <span className={styles.messageTextButton}>
                                Pending...
                            </span>
                        </div>
                    </Fragment>
                );
                break;

            case 'authenticated':
                dataMessage = (
                    <Fragment>
                        <span
                            className={classnames(
                                styles.messageText,
                                'u-text-center'
                            )}
                        >
                            <CheckMarkIcon
                                className={classnames(
                                    styles.iconRoundGray,
                                    'verified u-text-icon'
                                )}
                            />
                            <strong>
                                Congratulations! You are now an Authenticated
                                Creator on Audiomack
                            </strong>
                        </span>
                    </Fragment>
                );
                break;

            case 'authenticated-declined':
                dataMessage = this.renderDefaultMessage();
                break;

            default:
                dataMessage = this.renderDefaultMessage();
                break;
        }

        return dataMessage;
    }

    render() {
        const {
            currentUser: { profile }
        } = this.props;

        const alertStyle = {};

        if (
            profile.verified === 'authenticated-pending' ||
            profile.verified === 'authenticated-declined' ||
            profile.verified === 'no' ||
            profile.verified === ''
        ) {
            alertStyle.justifyContent = 'space-between';
        }

        return (
            <div className="row u-spacing-top u-spacing-bottom-10">
                <div className="column small-24">
                    <div className={styles.container} style={alertStyle}>
                        {this.renderContent(profile.verified)}
                    </div>
                </div>
            </div>
        );
    }
}
