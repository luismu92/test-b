import React from 'react';
import PropTypes from 'prop-types';

import SearchIcon from '../icons/search';
import styles from './RoundedSearchInput.module.scss';

function RoundedSearchInput({ onInputChange, value, placeholder }) {
    return (
        <label className={styles.label}>
            <input
                type="text"
                className={styles.input}
                placeholder={placeholder}
                onChange={onInputChange}
                value={value}
            />
            <button type="button" className={styles.submit} value="Search">
                <SearchIcon />
            </button>
        </label>
    );
}

RoundedSearchInput.propTypes = {
    onFormSubmit: PropTypes.func,
    onInputChange: PropTypes.func,
    value: PropTypes.string,
    placeholder: PropTypes.string
};

export default RoundedSearchInput;
