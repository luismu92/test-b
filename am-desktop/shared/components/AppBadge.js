import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import analyics, {
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';

import GooglePlayIcon from '../icons/google-play';
import AppStoreIcon from '../icons/app-store';

export default class AppBadge extends Component {
    static propTypes = {
        type: PropTypes.oneOf(['android', 'ios', 'sms']),
        style: PropTypes.oneOf(['image', 'svg']),
        className: PropTypes.string,
        onClick: PropTypes.func
    };

    static defaultProps = {
        style: 'image',
        onClick() {}
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLinkClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');

        switch (action) {
            case 'world': {
                analyics.track(eventCategory.ad, {
                    eventAction: eventAction.amworld,
                    eventLabel: eventLabel.sidebarLink
                });
                break;
            }

            case 'ios':
            case 'android': {
                analyics.track(eventCategory.app, {
                    eventAction: eventAction.appLinkClick,
                    eventLabel:
                        action === 'ios'
                            ? eventLabel.iosSidebar
                            : eventLabel.androidSidebar
                });
                break;
            }

            default:
                break;
        }

        this.props.onClick(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderContentForType(type, style) {
        switch (type) {
            case 'ios':
                if (style === 'image') {
                    return (
                        <img
                            src="/static/images/desktop/app-store-badge.png"
                            srcSet="/static/images/desktop/app-store-badge@2x.png 2x"
                            alt=""
                        />
                    );
                }

                return <AppStoreIcon />;

            case 'android':
                if (style === 'image') {
                    return (
                        <img
                            src="/static/images/desktop/google-play-badge.png"
                            srcSet="/static/images/desktop/google-play-badge@2x.png 2x"
                            alt=""
                        />
                    );
                }

                return <GooglePlayIcon />;

            case 'sms':
                return (
                    <img
                        src="/static/images/desktop/textmebadge.png"
                        srcSet="/static/images/desktop/textmebadge@2x.png 2x"
                        alt=""
                    />
                );

            default:
                return null;
        }
    }

    render() {
        const { type, style } = this.props;
        const klass = classnames('app-badge', {
            'app-badge--image': style === 'image',
            'app-badge--apple': type === 'ios',
            'app-badge--google': type === 'android',
            [this.props.className]: this.props.className
        });
        const props = {
            'data-action': type,
            onClick: this.handleLinkClick,
            className: klass
        };

        switch (type) {
            case 'ios':
                props.href =
                    'https://itunes.apple.com/us/app/audiomack/id921765888?ls=1&mt=8';
                props['aria-label'] = 'Download Audiomack on the App Store';
                break;

            case 'android':
                props.href =
                    'https://play.google.com/store/apps/details?id=com.audiomack';
                props['aria-label'] = 'Get Audiomack on Google Play';
                break;

            case 'sms':
                props['aria-label'] = 'Text me the app';
                break;

            default:
                break;
        }

        const tag = type === 'sms' ? 'button' : 'a';

        if (props.href) {
            props.target = '_blank';
            props.rel = 'nofollow noopener';
        }

        return React.createElement(
            tag,
            props,
            this.renderContentForType(type, style)
        );
    }
}
