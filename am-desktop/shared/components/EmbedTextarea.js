import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getIframeString, getWordpressShortCode } from 'utils/index';
import { addToast } from '../redux/modules/toastNotification';

import CloseIcon from '../icons/search-close';

class EmbedTextarea extends Component {
    static propTypes = {
        item: PropTypes.object,
        queryOptions: PropTypes.object,
        title: PropTypes.string,
        dispatch: PropTypes.func,
        onCloseButtonClick: PropTypes.func,
        hideWordpressToggle: PropTypes.bool,
        showCloseButton: PropTypes.bool
    };

    static defaultProps = {
        onCloseButtonClick() {},
        queryOptions: {},
        hideWordpressToggle: false,
        showCloseButton: false
    };

    constructor(props) {
        super(props);

        this.state = {
            wordpress: false
        };
    }

    componentDidMount() {
        this.focusArea();
    }

    componentWillUnmount() {
        this._embedTextarea = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleEmbedTextClick = () => {
        const { dispatch } = this.props;

        this.focusArea();

        try {
            const successful = document.execCommand('copy');

            if (successful) {
                dispatch(
                    addToast({
                        type: 'info',
                        message: 'iFrame source copied to clipboard!'
                    })
                );
            }
        } catch (err) {} // eslint-disable-line
    };

    handleWordpressClick = (e) => {
        this.setState(
            {
                wordpress: e.currentTarget.checked
            },
            () => this.focusArea()
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    focusArea() {
        this._embedTextarea.focus();
        this._embedTextarea.select();
    }

    renderWordpressToggle(shouldHide) {
        if (shouldHide) {
            return null;
        }

        return (
            <label>
                <input
                    type="checkbox"
                    name="wordpress"
                    className="checkbox--circular"
                    checked={this.state.wordpress}
                    onChange={this.handleWordpressClick}
                />
                Wordpress?
            </label>
        );
    }

    renderTitle(title) {
        if (!title) {
            return <span />;
        }

        return (
            <p>
                <strong>{title}</strong>
            </p>
        );
    }

    render() {
        const {
            item,
            hideWordpressToggle,
            showCloseButton,
            onCloseButtonClick,
            queryOptions,
            title
        } = this.props;

        let value = getIframeString(item, { queryOptions });

        if (this.state.wordpress) {
            value = getWordpressShortCode(item, queryOptions);
        }

        let closeButton;

        if (showCloseButton) {
            closeButton = (
                <button
                    className="music-detail__embed-close"
                    onClick={onCloseButtonClick}
                >
                    <CloseIcon />
                </button>
            );
        }

        return (
            <div className="music-detail__embed-wrap">
                <div className="u-left-right">
                    {this.renderTitle(title)}{' '}
                    {this.renderWordpressToggle(hideWordpressToggle)}
                </div>
                <div className="music-detail__embed-inner">
                    <textarea
                        className="music-detail__embed-textarea"
                        value={value}
                        readOnly
                        onClick={this.handleEmbedTextClick}
                        ref={(input) => {
                            if (input) {
                                this._embedTextarea = input;
                            }
                        }}
                    />
                    {closeButton}
                </div>
            </div>
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(EmbedTextarea);
