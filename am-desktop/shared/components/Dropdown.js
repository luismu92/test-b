import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
    Wrapper,
    Button,
    Menu,
    MenuItem,
    closeMenu
} from 'react-aria-menubutton';
import { tagSections } from 'constants/index';
import CloseIcon from '../icons/close-thin';
import DropdownIcon from '../icons/dropdown-arrow';

export default class Dropdown extends Component {
    static propTypes = {
        musicId: PropTypes.string,
        name: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        options: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.arrayOf(PropTypes.string)
        ]).isRequired,
        className: PropTypes.string,
        buttonClassName: PropTypes.string,
        menuClassName: PropTypes.string,
        showDeselectButton: PropTypes.bool,
        type: PropTypes.string,
        multiple: PropTypes.bool,
        sections: PropTypes.bool,
        testId: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.id = `dropdown-${props.name}-${props.musicId || Date.now()}`;
        this.state = {
            isOpen: false,
            visibleSections: {}
        };
    }

    componentDidMount() {
        this.setInitialVisibleSections();
    }

    componentDidUpdate(prevProps) {
        if (
            (!prevProps.value.length && this.props.value.length) ||
            // Sometimes not all sections are initially loaded
            Object.keys(prevProps.options).length !==
                Object.keys(this.props.options).length
        ) {
            this.setInitialVisibleSections();
        }
    }

    handleRemoveTag = (e) => {
        e.stopPropagation();

        const text = e.target.getAttribute('data-text');

        this.props.onChange(text, e);
        closeMenu(this.id);
    };

    handleSelection = (text, e) => {
        const section = e.target.getAttribute('data-section');

        if (section) {
            this.setState((prevState) => ({
                visibleSections: {
                    ...prevState.visibleSections,
                    [section]: !prevState.visibleSections[section]
                }
            }));

            return;
        }

        this.props.onChange(text, e);
        closeMenu(this.id);
    };

    handleMenuOpen = ({ isOpen }) => {
        this.setState({ isOpen });
    };

    setInitialVisibleSections = () => {
        const { options, sections, value } = this.props;

        if (!sections) {
            return;
        }

        const visibleSections = Object.entries(options).reduce(
            (openSections, [section, data]) => {
                if (!Array.isArray(data)) {
                    return openSections;
                }

                const isVisible = Boolean(
                    data.find((option) => value.includes(option.value))
                );

                if (isVisible) {
                    return {
                        ...openSections,
                        [section]: true
                    };
                }

                return openSections;
            },
            {}
        );

        this.setState({
            visibleSections: visibleSections
        });
    };

    renderMenuItems() {
        const {
            name,
            options,
            sections,
            showDeselectButton,
            value
        } = this.props;

        const mapOptions = ({ text, value: textValue }, i) => {
            const isActive =
                textValue === value ||
                (Array.isArray(value) &&
                    (value.includes(textValue) ||
                        value.join('') === textValue));

            const klass = classnames('c-dropdown__item-button u-bare-button', {
                'c-dropdown__item-button--active': isActive
            });

            let deselectButton;

            if (value.length && isActive && showDeselectButton) {
                deselectButton = (
                    <CloseIcon className="c-dropdown__deselect-button" />
                );
            }

            return (
                <li
                    key={`${name}-${textValue}-${i || 'defaultOption'}`}
                    className="c-dropdown__item"
                >
                    <MenuItem
                        className={klass}
                        tag="div"
                        data-name={name}
                        data-testid={`${name}-${textValue}`}
                        data-value={textValue}
                        data-index={`sectionItem-${i || 'defaultOption'}`}
                    >
                        <span>{text}</span>
                        {deselectButton}
                    </MenuItem>
                </li>
            );
        };

        if (sections) {
            return Object.entries(options).reduce(
                (menuItems, [title, data], i) => {
                    if (title === tagSections.defaultOption) {
                        const defaultOption = mapOptions(data);
                        return [...menuItems, defaultOption];
                    }

                    let items;

                    if (this.state.visibleSections[title]) {
                        items = data.map(mapOptions);
                    }

                    const section = (
                        <li key={`${title}-${i}`}>
                            <MenuItem
                                className="c-dropdown__section-header"
                                data-section={title}
                            >
                                <span>{title}</span>
                                <span className="c-dropdown__section-dropdown-button u-text-icon">
                                    <DropdownIcon />
                                </span>
                            </MenuItem>
                            <ul className="c-dropdown__section-items">
                                {items}
                            </ul>
                        </li>
                    );

                    return [...menuItems, section];
                },
                []
            );
        }

        return options.map(mapOptions);
    }

    renderText() {
        const { multiple, options, sections, type, value } = this.props;
        const isTag = value.length && type === 'tag';

        const textClass = classnames({
            'c-dropdown__label-tag': isTag
        });

        if (multiple && sections && type === 'filter') {
            const s = value.length === 1 ? '' : 's';

            let label = `(${value.length}) Tag${s} selected`;

            if (!value.length) {
                label = options[tagSections.defaultOption].text;
            }

            return <span className={textClass}>{label}</span>;
        }

        let removeTagButton;

        const reduceOptions = (opts, opt, i) => {
            if (
                value.includes(opt.value.toString()) ||
                opt.value.toString() === value.join('')
            ) {
                if (isTag) {
                    removeTagButton = (
                        /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
                        <span
                            className="c-dropdown__remove-tag-button"
                            data-text={opt.text}
                            data-value={opt.value}
                            onClick={this.handleRemoveTag}
                        >
                            <CloseIcon className="c-dropdown__deselect-button" />
                        </span>
                    );
                }
                const element = (
                    <span className={textClass} key={`${value}-${i}`}>
                        <span>{opt.text}</span>
                        {removeTagButton}
                    </span>
                );

                opts.push(element);
            }

            return opts;
        };

        if (multiple && sections) {
            return []
                .concat(...Object.values(options))
                .reduce(reduceOptions, []);
        }

        if (multiple) {
            return options.reduce(reduceOptions, []);
        }

        const text = (
            options.find((opt) => opt.value.toString() === value.toString()) ||
            {}
        ).text;

        if (isTag) {
            removeTagButton = (
                /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
                <span
                    className="c-dropdown__remove-tag-button"
                    data-text={text}
                    data-value={value}
                    onClick={this.handleRemoveTag}
                >
                    <CloseIcon className="c-dropdown__deselect-button" />
                </span>
            );
        }

        return (
            <span className={textClass}>
                <span>{text}</span>
                {removeTagButton}
            </span>
        );
    }

    render() {
        const {
            musicId,
            name,
            className,
            buttonClassName,
            menuClassName,
            testId
        } = this.props;
        const klass = classnames('c-dropdown', {
            'c-dropdown--active': this.state.isOpen,
            [className]: className
        });
        const buttonClass = classnames('c-dropdown__label', {
            [buttonClassName]: buttonClassName
        });
        const menuClass = classnames('u-bare-list c-dropdown__list', {
            [menuClassName]: menuClassName
        });

        return (
            <Wrapper
                className={klass}
                closeOnSelection={false}
                id={this.id}
                onSelection={this.handleSelection}
                onMenuToggle={this.handleMenuOpen}
                data-music-id={musicId}
                data-name={name}
                data-testid={testId}
                data-dropdown
            >
                <Button className={buttonClass}>
                    {this.renderText()}
                    <span className="c-dropdown__label-icon u-text-icon">
                        <DropdownIcon />
                    </span>
                </Button>
                <Menu className={menuClass} tag="ul">
                    {this.renderMenuItems()}
                </Menu>
            </Wrapper>
        );
    }
}
