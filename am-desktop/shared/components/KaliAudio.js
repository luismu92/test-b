import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import analyics, { eventCategory, eventAction } from 'utils/analytics';

import CloseIcon from '../icons/close-thin';

export default class KaliAudio extends Component {
    static propTypes = {
        linkHref: PropTypes.string.isRequired,
        eventLabel: PropTypes.string.isRequired,
        className: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };
    }

    handleCloseClick = () => {
        this.setState({
            visible: false
        });
    };

    handleLinkClick = () => {
        const { eventLabel } = this.props;

        analyics.track(eventCategory.ad, {
            eventAction: eventAction.kali,
            eventLabel: eventLabel
        });
    };

    render() {
        const { linkHref, className } = this.props;

        const klass = classnames('kali-audio u-pos-relative', {
            [className]: className
        });

        if (!this.state.visible) {
            return null;
        }

        const closeButton = (
            <button className="partner__close" onClick={this.handleCloseClick}>
                <CloseIcon />
            </button>
        );

        return (
            <div className={klass}>
                <span className="partner__sponsor-tag">Advertisement</span>
                {closeButton}
                <div className="kali-content u-box-shadow">
                    <a
                        href={linkHref}
                        target="_blank"
                        rel="nofollow noopener"
                        onClick={this.handleLinkClick}
                    >
                        <div className="u-d-flex u-d-flex--align-center u-text-left u-padding-right-50">
                            <img
                                className="kali__logo"
                                src="/static/images/desktop/kali-logo.png"
                                srcSet="/static/images/desktop/kali-logo@2x.png 2x"
                                alt="Kali Audio"
                            />
                            <p className="u-fs-15 u-ls-n-03 u-fw-600">
                                Hear every detail in your mix at an unbeatable
                                price point with{' '}
                                <strong>Kali Audio's new LP-6 monitors</strong>,
                                starting at just $300 per pair, and get 2 free
                                months of Splice Sounds.{' '}
                                <span className="u-text-orange">
                                    Learn more
                                </span>
                            </p>
                        </div>
                        <img
                            className="kali__quote"
                            src="/static/images/desktop/kali-speakers.png"
                            srcSet="/static/images/desktop/kali-speakers@2x.png 2x"
                            alt="Kali Audio - DJ Booth"
                        />
                    </a>
                </div>
            </div>
        );
    }
}
