import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import FacebookIcon from '../icons/facebook-letter-logo';
import TwitterIcon from '../icons/twitter-logo-new';
import InstagramIcon from '../icons/instagram';
import YoutubeIcon from '../icons/youtube';
import GlobeIcon from '../icons/globe-wire';
import TwitchIcon from 'icons/twitch';

export default class SocialLinks extends Component {
    static propTypes = {
        links: PropTypes.object.isRequired,
        className: PropTypes.string,
        allowNonVerified: PropTypes.bool
    };

    static defaultProps = {
        allowNonVerified: false
    };

    render() {
        const { links, className, allowNonVerified } = this.props;

        let url;

        let twitter;

        let facebook;

        let instagram;

        let youtube;

        let twitch;

        if (links.url) {
            url = (
                <li>
                    <a
                        href={links.url}
                        target="_blank"
                        className="social-icon social-icon--url"
                        rel="nofollow noopener"
                        aria-label="Go to website"
                    >
                        <GlobeIcon />
                    </a>
                </li>
            );
        }

        if (
            (!allowNonVerified && links.twitter_id) ||
            (allowNonVerified && links.twitter)
        ) {
            twitter = (
                <li>
                    <a
                        target="_blank"
                        href={`https://twitter.com/${links.twitter}`}
                        rel="nofollow noopener"
                        className="social-icon social-icon--twitter"
                        aria-label="Go to Twitter"
                    >
                        <TwitterIcon />
                    </a>
                </li>
            );
        }

        if (
            (!allowNonVerified && links.facebook_id) ||
            (allowNonVerified && links.facebook)
        ) {
            facebook = (
                <li>
                    <a
                        target="_blank"
                        href={links.facebook}
                        rel="nofollow noopener"
                        className="social-icon social-icon--facebook"
                        aria-label="Go to Facebook"
                    >
                        <FacebookIcon />
                    </a>
                </li>
            );
        }

        if (
            (!allowNonVerified && links.instagram_id) ||
            (allowNonVerified && links.instagram)
        ) {
            instagram = (
                <li>
                    <a
                        target="_blank"
                        href={`https://instagram.com/${links.instagram}`}
                        rel="nofollow noopener"
                        className="social-icon social-icon--instagram"
                        aria-label="Go to Instagram"
                    >
                        <InstagramIcon />
                    </a>
                </li>
            );
        }

        if (links.youtube) {
            youtube = (
                <li>
                    <a
                        target="_blank"
                        href={`${links.youtube}`}
                        rel="nofollow noopener"
                        className="social-icon social-icon--youtube"
                        aria-label="Go to YouTube"
                    >
                        <YoutubeIcon />
                    </a>
                </li>
            );
        }

        if (links.twitch) {
            twitch = (
                <li>
                    <a
                        target="_blank"
                        href={`https://twitch.tv/${links.twitch}`}
                        rel="nofollow noopener"
                        className="social-icon social-icon--twitch"
                        aria-label="Go to Twitch"
                    >
                        <TwitchIcon />
                    </a>
                </li>
            );
        }

        const klass = classnames('social-icons', {
            [className]: className
        });

        const allEmpty =
            [url, twitter, facebook, instagram, youtube].filter(Boolean)
                .length === 0;

        if (allEmpty) {
            return null;
        }

        return (
            <ul className={klass}>
                {url} {twitter} {facebook} {instagram} {youtube} {twitch}
            </ul>
        );
    }
}
