import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { renderFeaturingLinks } from 'utils/index';
import AndroidLoader from '../loaders/AndroidLoader';

export default class FeaturedSidebar extends Component {
    static propTypes = {
        featuredList: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired
    };

    renderTopUploads(featured = [], loading) {
        if (loading) {
            return (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        if (!featured.length) {
            return (
                <p>There was a problem retrieving the top Trending Songs.</p>
            );
        }

        const list = featured
            .filter((upload) => {
                return (
                    upload.ref.type === 'song' || upload.ref.type === 'album'
                );
            })
            .slice(0, 5)
            .map((upload, i) => {
                return this.renderUpload(upload.ref, i);
            });

        return (
            <div className="list" role="list">
                {list}
            </div>
        );
    }

    renderUpload(upload, i) {
        let feat;
        let desc;

        const artworkKlass = classnames('list-item__image', {
            'u-album-stack u-album-stack--small':
                upload.type === 'album' || upload.type === 'playlist'
        });

        if (upload.featuring) {
            feat = (
                <p className="list-item__details-meta u-trunc">
                    <strong>{renderFeaturingLinks(upload.featuring)}</strong>
                </p>
            );
        }

        const description = upload.admin_description || upload.description;

        if (description) {
            desc = <p className="list-item__desc">{description}</p>;
        }

        return (
            <div
                className="list-item list-item--featured"
                key={i}
                role="listitem"
            >
                <div className="list-item__inner">
                    <div className={artworkKlass}>
                        <Link
                            to={`/${upload.type}/${upload.uploader.url_slug}/${
                                upload.url_slug
                            }`}
                        >
                            <img src={upload.image} alt={upload.title} />
                        </Link>
                    </div>
                    <div className="list-item__details">
                        <Link
                            className="list-item__details-link"
                            to={`/${upload.type}/${upload.uploader.url_slug}/${
                                upload.url_slug
                            }`}
                        >
                            <p className="list-item__details-artist">
                                {upload.artist}
                            </p>
                            <p className="list-item__details-meta u-trunc">
                                <strong>{upload.title}</strong>
                            </p>
                        </Link>
                        {feat}
                        {desc}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { featuredList, loading } = this.props;

        return (
            <div className="u-padding-x-20">
                <p className="music-page__tracks-header u-spacing-bottom">
                    Featured Music
                </p>
                {this.renderTopUploads(featuredList, loading)}
            </div>
        );
    }
}
