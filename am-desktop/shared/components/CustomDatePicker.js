// https://github.com/Hacker0x01/react-datepicker/issues/730
//
import DatePicker from 'react-datepicker';

export default class CustomDatePicker extends DatePicker {
    constructor(props) {
        super(props);
    }

    deferFocusInput = () => {
        this.cancelFocusInput();
    };
}
