import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { throttle } from 'utils/index';

import ChevronLeftIcon from '../icons/chevron-left';
import ChevronRightIcon from '../icons/chevron-right';

import styles from './Carousel.module.scss';

let xDown = null;
let yDown = null;

export default class Carousel extends Component {
    static propTypes = {
        items: PropTypes.array,
        itemWidth: PropTypes.number,
        itemSpacing: PropTypes.number,
        getUniqueKey: PropTypes.func,
        className: PropTypes.string
    };

    static defaultProps = {
        itemWidth: 200,
        itemSpacing: 20,
        getUniqueKey(items) {
            return items;
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            width: 1180,
            page: 1
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowResize, false);

        this.resize();
    }

    componentDidUpdate(prevProps) {
        if (
            this.props.getUniqueKey(this.props.items) !==
            this.props.getUniqueKey(prevProps.items)
        ) {
            // eslint-disable-next-line
            this.setState({
                page: 1
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize);
        this._container = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowResize = throttle(() => {
        this.resize();
    }, 250);

    handleCarouselButtonClick = (e) => {
        const button = e.currentTarget;
        const pageDelta = parseInt(button.getAttribute('data-delta'), 10);

        this.setState((prevState) => {
            return {
                ...prevState,
                page: prevState.page + pageDelta
            };
        });
    };

    handleSwipe = (direction) => {
        switch (direction) {
            case 'left':
                this.goNext();
                break;

            case 'right':
                this.goPrevious();
                break;

            default:
                break;
        }
    };

    handleTouchStart = (evt) => {
        xDown = (evt.originalEvent || evt).touches[0].clientX;
        yDown = (evt.originalEvent || evt).touches[0].clientY;
    };

    handleTouchMove = (evt) => {
        if (!xDown || !yDown) {
            return;
        }

        const xUp = (evt.originalEvent || evt).touches[0].clientX;
        const yUp = (evt.originalEvent || evt).touches[0].clientY;

        const xDiff = xDown - xUp;
        const yDiff = yDown - yUp;

        if (Math.abs(xDiff) > Math.abs(yDiff)) {
            /* most significant */
            if (xDiff > 0) {
                this.handleSwipe('left');
                /* left swipe */
            } else {
                this.handleSwipe('right');
                /* right swipe */
            }
        } else if (yDiff > 0) {
            this.handleSwipe('up');
            /* up swipe */
        } else {
            this.handleSwipe('down');
            /* down swipe */
        }

        /* reset values */
        xDown = null;
        yDown = null;
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getVisibleItems(containerWidth, itemWidth, itemSpacing, threshold = 0.2) {
        const fullItemWidth = itemWidth + itemSpacing;
        const visibleItems = Math.floor(containerWidth / fullItemWidth);
        const visibleItemWidth = visibleItems * fullItemWidth;
        const spaceLeft = containerWidth - visibleItemWidth;

        // If we have almost a full item, just add 1
        if (spaceLeft / itemWidth + threshold > 1) {
            return visibleItems + 1;
        }

        return visibleItems;
    }

    goNext() {
        if (this.isRightDisabled()) {
            return;
        }

        this.adjustPage(1);
    }

    goPrevious() {
        if (this.isLeftDisabled()) {
            return;
        }

        this.adjustPage(-1);
    }

    adjustPage(pageDelta) {
        this.setState((prevState) => {
            return {
                ...prevState,
                page: prevState.page + pageDelta
            };
        });
    }

    isLeftDisabled() {
        return this.state.page === 1;
    }

    isRightDisabled() {
        const { itemWidth, itemSpacing, items } = this.props;
        const { page, width } = this.state;
        const visibleItems = this.getVisibleItems(
            width,
            itemWidth,
            itemSpacing
        );

        return visibleItems * page >= items.length;
    }

    resize() {
        if (!this._container) {
            return;
        }

        this.setState({
            width: this._container.clientWidth
        });
    }

    renderItems(items = []) {
        return items.map((item, i) => {
            return (
                <div
                    className={styles.item}
                    key={i}
                    style={{
                        width: this.props.itemWidth,
                        marginRight: this.props.itemSpacing
                    }}
                >
                    {item}
                </div>
            );
        });
    }

    render() {
        const { itemWidth, itemSpacing, items, className } = this.props;
        const { page, width } = this.state;
        const fullItemWidth = itemWidth + itemSpacing;
        const visibleItems = this.getVisibleItems(
            width,
            itemWidth,
            itemSpacing
        );
        const transform = visibleItems * fullItemWidth * (page - 1) * -1;
        const itemStyle = {
            transform: `translateX(${transform}px)`
        };
        const leftDisabled = this.isLeftDisabled();
        const rightDisabled = this.isRightDisabled();
        const klass = classnames(styles.carousel, {
            [className]: className
        });

        return (
            <div className={klass}>
                <button
                    className={`${styles.left} u-flex-center`}
                    disabled={leftDisabled}
                    onClick={this.handleCarouselButtonClick}
                    data-delta="-1"
                    aria-label="Previous page"
                >
                    <ChevronLeftIcon />
                </button>
                <div className={styles.inner}>
                    <div
                        className={styles.items}
                        style={itemStyle}
                        ref={(c) => (this._container = c)}
                        onTouchStart={this.handleTouchStart}
                        onTouchMove={this.handleTouchMove}
                    >
                        {this.renderItems(items)}
                    </div>
                </div>
                <button
                    className={`${styles.right} u-flex-center`}
                    disabled={rightDisabled}
                    onClick={this.handleCarouselButtonClick}
                    data-delta="1"
                    aria-label="Next page"
                >
                    <ChevronRightIcon />
                </button>
            </div>
        );
    }
}
