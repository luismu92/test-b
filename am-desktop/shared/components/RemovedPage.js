import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { showModal, MODAL_TYPE_ADMIN } from '../redux/modules/modal';

import PowerIcon from '../icons/power';
import GraphIcon from '../icons/graph';
import MusicDetailContainer from '../browse/MusicDetailContainer';
import AndroidLoader from '../loaders/AndroidLoader';

export default class RemovedPage extends Component {
    static propTypes = {
        trending: PropTypes.object.isRequired,
        music: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        dispatch: PropTypes.func
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    handleAdminClick = () => {
        const { dispatch, music } = this.props;

        dispatch(showModal(MODAL_TYPE_ADMIN, { item: music }));
    };

    renderTrending(trending = []) {
        const filteredTrending = trending.filter((item) => {
            return (
                item.status !== 'suspended' &&
                item.status !== 'takedown' &&
                !item.geo_restricted
            );
        });

        const list = filteredTrending.slice(0, 5).map((result, i) => {
            return (
                <div className="u-spacing-bottom-60" key={i}>
                    <MusicDetailContainer
                        index={i}
                        item={result}
                        musicList={filteredTrending}
                        shouldLinkArtwork
                        // onItemClick={this.props.onItemClick}
                    />
                </div>
            );
        });

        let button;

        if (list.length) {
            button = (
                <div className="u-text-center u-spacing-top">
                    <Link className="button button--pill" to="/trending-now">
                        More Trending Music
                    </Link>
                </div>
            );
        }

        return (
            <div>
                <div className="music-feed__context-switcher u-box-shadow">
                    <div className="feed-bar u-group feed-bar--padded">
                        <h2 className="feed-bar__title">
                            <GraphIcon className="feed-bar__title-icon feed-bar__title-icon--graph u-text-icon" />
                            <span>
                                <strong>Trending now on Audiomack</strong>
                            </span>
                        </h2>
                    </div>
                </div>
                {list}
                {button}
            </div>
        );
    }

    render() {
        const { music, trending, currentUser } = this.props;

        let loader;

        if (trending.loading) {
            loader = (
                <div>
                    <AndroidLoader className="music-feed__loader" />
                </div>
            );
        }

        let adminButton;

        if (currentUser.isLoggedIn && currentUser.isAdmin) {
            adminButton = (
                <button
                    className="music-interaction"
                    data-tooltip="Admin Actions"
                    aria-label="Admin Actions"
                    data-action="admin"
                    onClick={this.handleAdminClick}
                >
                    <span>
                        <PowerIcon className="music-interaction__icon musi-interaction__icon--admin" />
                    </span>
                </button>
            );
        }

        return (
            <div className="music-page u-spacing-top">
                <div className="row">
                    <div className="row expanded column small-24">
                        <Helmet>
                            <title>Not found</title>
                            <meta name="robots" content="noindex, follow" />
                        </Helmet>
                        {adminButton}
                        <div className="music-detail__top u-clearfix">
                            <div className="removed-page-box u-text-center u-box-shadow">
                                <h3>{`This ${
                                    music.type
                                } cannot be found or has been removed.`}</h3>
                            </div>
                        </div>
                        {loader}
                        {this.renderTrending(trending.list)}
                    </div>
                </div>
            </div>
        );
    }
}
