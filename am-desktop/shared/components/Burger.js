import React from 'react';
import PropTypes from 'prop-types';

import styles from './Burger.module.scss';

function Burger({ onBurgerClick }) {
    return (
        <button
            className={styles.burger}
            onClick={onBurgerClick}
            data-action="burger"
        >
            <span className={styles.filling} />
        </button>
    );
}

Burger.propTypes = {
    onBurgerClick: PropTypes.func
};

export default Burger;
