import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getArtistUrl } from 'utils/artist';

import AndroidLoader from '../loaders/AndroidLoader';
import MusicDetailMini from '../musicDetail/MusicDetailMini';

import EmptyIcon from 'icons/empty';

import styles from './RecentTracksAlbums.module.scss';

class RecentTracksAlbums extends Component {
    static propTypes = {
        uploader: PropTypes.object.isRequired,
        uploads: PropTypes.object.isRequired,
        filterIds: PropTypes.array
    };

    renderArtistUploads(uploads = [], loading, uploader) {
        let loader;

        if (loading) {
            loader = (
                <div style={{ textAlign: 'center' }}>
                    <AndroidLoader />
                </div>
            );
        }

        if (!uploads.length && !loading) {
            return (
                <Fragment>
                    <div className={styles.emptyIcon}>
                        <EmptyIcon />
                    </div>
                    <p className={styles.empty}>
                        {uploader.name} has no other uploads.
                    </p>
                </Fragment>
            );
        }

        const list = uploads.map((upload, index) => {
            return <MusicDetailMini key={index} item={upload} />;
        });

        return (
            <Fragment>
                {list}
                {loader}
            </Fragment>
        );
    }

    render() {
        const { uploader, uploads, filterIds } = this.props;

        const filtered = uploads.list.filter((upload) => {
            if (filterIds.indexOf(upload.id) !== -1) {
                return false;
            }

            if (upload.type !== 'song' && upload.type !== 'album') {
                return false;
            }

            if (upload.geo_restricted) {
                return false;
            }

            return true;
        });

        let button;

        if (filtered.length) {
            button = (
                <Link className={styles.more} to={getArtistUrl(uploader)}>
                    View more
                </Link>
            );
        }

        const title = (
            <h2 className={styles.title}>
                More from{' '}
                <Link to={getArtistUrl(uploader)}>{uploader.name}</Link>
            </h2>
        );

        return (
            <Fragment>
                {title}
                {this.renderArtistUploads(filtered, uploads.loading, uploader)}
                {button}
            </Fragment>
        );
    }
}

export default RecentTracksAlbums;
