// - Shared between UploadSongsPage, UploadAlbumsPage, and potentially in the future, AlbumEditPage
// - Note that input elements have global styles as well

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import EditIcon from '../icons/edit';
import QuestionIcon from '../icons/question-mark';

import Dropdown from './Dropdown';
import TagListInput from './TagListInput';
import ToggleSwitch from 'components/ToggleSwitch';

import styles from './LabeledInput.module.scss';

export const inputTypes = {
    checkbox: 'checkbox',
    dropdown: 'dropdown',
    radio: 'radio',
    slug: 'slug',
    tags: 'tags',
    text: 'text',
    textarea: 'textarea',
    toggle: 'toggle',
    url: 'url'
};

export default class LabeledInput extends Component {
    static propTypes = {
        checked: PropTypes.bool,
        'data-testid': PropTypes.string,
        disabled: PropTypes.bool,
        error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
        heading: PropTypes.bool,
        id: PropTypes.string,
        label: PropTypes.node.isRequired,
        maxLength: PropTypes.number,
        name: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        onResize: PropTypes.func,
        options: PropTypes.array,
        prepend: PropTypes.string,
        required: PropTypes.bool,
        subtitle: PropTypes.string,
        tagListAdditionalHeight: PropTypes.number,
        tooltip: PropTypes.string,
        type: PropTypes.string,
        value: PropTypes.string
    };

    static defaultProps = {
        onChange() {},
        error: false,
        heading: false,
        inputTypes,
        required: false,
        type: 'text',
        value: ''
    };

    renderInput() {
        const {
            checked,
            'data-testid': dataTestId,
            disabled,
            error,
            id,
            label,
            maxLength,
            name,
            onChange,
            onResize,
            options,
            prepend,
            required,
            tagListAdditionalHeight,
            type,
            value
        } = this.props;

        const inputClassName = classnames(styles.input, {
            [styles.error]: error,
            [styles[type]]: type
        });

        const inputProps = {
            className: inputClassName,
            disabled,
            id,
            maxLength,
            name,
            onChange,
            required,
            type,
            value
        };

        let input;

        switch (type) {
            case inputTypes.checkbox:
            case inputTypes.radio: {
                const checkedInputProps = {
                    ...inputProps,
                    checked,
                    'data-testid': dataTestId
                };

                input = <input {...checkedInputProps} />;

                break;
            }

            case inputTypes.dropdown: {
                input = (
                    <Dropdown
                        {...inputProps}
                        options={options}
                        showDeselectButton
                        testId={dataTestId}
                        type="tag"
                    />
                );

                break;
            }

            case inputTypes.slug:
                input = (
                    <div className={styles.content}>
                        <span className="input input--prepend">{prepend}</span>
                        <input
                            {...inputProps}
                            className={styles.slugInput}
                            data-testid={dataTestId}
                            pattern="^[a-zA-Z0-9\-]*$"
                            type="text"
                        />
                        <span className={styles.editIcon}>
                            <EditIcon />
                        </span>
                    </div>
                );

                break;

            case inputTypes.tags: {
                // convert value to array because the api and old tag inputs use comma separated values
                const unformattedTags = inputProps.value;
                const tags = unformattedTags
                    ? unformattedTags.split(',').filter(Boolean)
                    : [];

                input = (
                    <TagListInput
                        inputProps={inputProps}
                        onTagListChange={onChange}
                        onTagListResize={onResize}
                        options={options}
                        tagListAdditionalHeight={tagListAdditionalHeight}
                        tags={tags}
                    />
                );

                break;
            }

            case inputTypes.text:
                input = <input {...inputProps} data-testid={dataTestId} />;

                break;

            case inputTypes.textarea: {
                const remaining = maxLength - value.length;
                const s = remaining === 1 ? '' : 's';

                input = (
                    <Fragment>
                        <textarea {...inputProps} data-testid={dataTestId} />
                        <span className={styles.textareaRemaining}>
                            You have {remaining} character{s} left
                        </span>
                    </Fragment>
                );

                break;
            }

            case inputTypes.toggle: {
                let monetization;

                if (name === 'video_ad' && checked) {
                    monetization = (
                        <span className={styles.subtitle}>
                            Monetization partner: <b>{value}</b>
                        </span>
                    );
                }

                input = (
                    <Fragment>
                        <ToggleSwitch
                            {...inputProps}
                            checked={checked}
                            className={classnames(
                                inputClassName,
                                styles.toggle
                            )}
                            label={label}
                        />
                        {monetization}
                    </Fragment>
                );

                break;
            }

            case inputTypes.url:
                input = <input {...inputProps} data-testid={dataTestId} />;

                break;

            default:
                break;
        }

        return input;
    }

    renderLabel() {
        const { id, label, name, required, subtitle, tooltip } = this.props;

        const labelClassName = classnames(styles.label, {
            [styles.required]: required
        });

        const icon = tooltip && (
            <span className={styles.tooltip} data-tooltip={tooltip}>
                <QuestionIcon />
            </span>
        );

        const sub = subtitle && (
            <span className={styles.subtitle}>{subtitle}</span>
        );

        if (name === 'save_as_single') {
            return (
                <label className={labelClassName} htmlFor={id}>
                    {icon}
                    {label}
                    {sub}
                </label>
            );
        }

        return (
            <label className={labelClassName} htmlFor={id}>
                {label}
                {sub}
                {icon}
            </label>
        );
    }

    render() {
        const { heading, label, name, type } = this.props;

        const camelCasedName = name.replace(/[-_][a-z]/g, (group) =>
            group
                .toUpperCase()
                .replace('-', '')
                .replace('_', '')
        );

        const className = classnames(styles.container, styles[camelCasedName], {
            [styles.heading]: heading,
            [styles.songDescription]: label === 'Song Description'
        });

        if (type === 'radio') {
            return (
                <div className={className}>
                    {this.renderInput()}
                    {this.renderLabel()}
                </div>
            );
        }

        return (
            <div className={className}>
                {this.renderLabel()}
                {this.renderInput()}
            </div>
        );
    }
}
