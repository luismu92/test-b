import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import analytics from 'utils/analytics';

import { deleteItem, alterItemPrivacy } from '../redux/modules/music/index';
import { addToast } from '../redux/modules/toastNotification';
import {
    showModal,
    hideModal,
    MODAL_TYPE_PROMO,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';

import OwnerControls from './OwnerControls';

class OwnerControlsContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        music: PropTypes.object.isRequired,
        modal: PropTypes.object,
        history: PropTypes.object,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            editActive: false
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleEditClick = () => {
        this.setState({ editActive: !this.state.editActive });
    };

    handleMakePublicClick = (e) => {
        const { dispatch } = this.props;

        e.preventDefault();

        const id = e.currentTarget.getAttribute('data-id');
        const type = e.currentTarget.getAttribute('data-type');
        const isPrivate = e.currentTarget.getAttribute('data-private');
        const setPrivate = isPrivate === 'yes' ? 'no' : 'yes';

        dispatch(alterItemPrivacy(id, setPrivate))
            .then(() => {
                const responseText =
                    setPrivate === 'yes' ? 'Private' : 'Public';

                dispatch(
                    addToast({
                        type: type,
                        action: 'public',
                        item: `Your ${type} is now ${responseText}.`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            'There was a problem editing your playlist privacy.'
                    })
                );
                analytics.error(error);
                console.log(error);
            });
    };

    handleDeleteClick = (event) => {
        const { dispatch, music } = this.props;

        event.preventDefault();

        let message;

        switch (music.type) {
            case 'song':
                message =
                    "If you need to change some of the information associated with this song, or replace the existing MP3, please cancel and use the 'Edit song' action instead. Deleting a song will break any links the song may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this song, click the 'Delete' button below.";
                break;

            case 'album':
                message =
                    "If you need to change some of the information associated with this album please cancel and use the 'Edit album' action instead. Deleting an album will break any links the album may have pointing to it so it is always better to edit than to delete.\n\nIf you still want to delete this album, click the 'Delete' button below.";
                break;

            default:
                break;
        }

        const title = (
            <span>
                Delete {music.type}: <em>'{music.title}'</em>
            </span>
        );

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: title,
                message,
                handleConfirm: () => this.handleDeleteConfirm(music.id),
                confirmButtonText: `Delete ${music.type}`,
                confirmButtonProps: {
                    className: 'button button--pill button--danger button--wide'
                }
            })
        );
    };

    handleDeleteConfirm = (musicId) => {
        const { dispatch, history, currentUser } = this.props;

        dispatch(deleteItem(musicId))
            .then(() => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                history.push(`/artist/${currentUser.profile.url_slug}`);
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_CONFIRM));
                let errorMsg = 'There was a problem deleting this music item.';

                if (currentUser.isAdmin) {
                    errorMsg = `There was a problem deleting this music item. ${
                        error.message
                    }`;
                }

                dispatch(
                    addToast({
                        action: 'message',
                        item: errorMsg
                    })
                );
                analytics.error(error);
                console.log(error);
            });
    };

    handlePromoLinkClick = (e) => {
        const { dispatch, music } = this.props;

        e.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_PROMO, {
                music: music
            })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { music, modal, currentUser } = this.props;

        return (
            <OwnerControls
                music={music}
                modal={modal}
                currentUser={currentUser}
                editActive={this.state.editActive}
                onEditClick={this.handleEditClick}
                onDeleteClick={this.handleDeleteClick}
                onPublicClick={this.handleMakePublicClick}
                onPromoLinkClick={this.handlePromoLinkClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        modal: state.modal
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(OwnerControlsContainer);
