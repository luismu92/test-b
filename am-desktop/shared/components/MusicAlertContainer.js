import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import analytics from 'utils/analytics';
import { ownsMusic } from 'utils/index';

import { alterItemPrivacy } from '../redux/modules/music/index';
import { addToast } from '../redux/modules/toastNotification';

import {
    showModal,
    hideModal,
    MODAL_TYPE_PROMO,
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_CHANGE_RELEASE
} from '../redux/modules/modal';
import { updateMetadata } from '../redux/modules/music/index';

import MusicAlert from './MusicAlert';
import ChangeReleaseModal from '../modal/ChangeReleaseModal';

export default class MusicAlertContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        item: PropTypes.object.isRequired,
        message: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        icon: PropTypes.element,
        currentUser: PropTypes.object,
        onCountdownFinish: PropTypes.func
    };

    handleActionClick = (e) => {
        e.preventDefault();

        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        switch (action) {
            case 'promo-link':
                this.handlePromoLinkClick();
                break;
            case 'make-public':
                this.handleMakePublicClick(e);
                break;
            case 'change-release':
                this.handleChangeReleaseClick(e);
                break;
            default:
                break;
        }
    };

    handleMakePublicClick = (e) => {
        const { dispatch, item } = this.props;

        const id = e.currentTarget.getAttribute('data-id');
        const type = e.currentTarget.getAttribute('data-type');
        const isPrivate = e.currentTarget.getAttribute('data-private');
        const setPrivate = isPrivate === 'yes' ? 'no' : 'yes';

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure you want to set this public?',
                message: `Once you set this ${
                    item.type
                } public, it will be available for anyone to play. If you want to make it private again later, tap the edit button for this ${
                    item.type
                } and set it to private in the release tab.`,
                handleConfirm: () =>
                    this.makePublic(id, type, isPrivate, setPrivate),
                confirmButtonText: 'Go Public',
                confirmButtonProps: {
                    className: 'button button--pill'
                }
            })
        );
    };

    handlePromoLinkClick = () => {
        const { dispatch, item } = this.props;

        dispatch(
            showModal(MODAL_TYPE_PROMO, {
                music: item
            })
        );
    };

    handleChangeReleaseClick = (e) => {
        const { item, dispatch } = this.props;

        e.preventDefault();

        const button = e.currentTarget;
        const release = button.getAttribute('data-release');

        const currentDate = moment();

        switch (release) {
            case 'now':
                dispatch(
                    showModal(MODAL_TYPE_CONFIRM, {
                        title: `Are you sure you want to release this ${
                            item.type
                        } now?`,
                        message: `Releasing this ${
                            item.type
                        } now will make it available for anyone to play.`,
                        handleConfirm: () =>
                            this.handleDateChange(currentDate.unix()),
                        confirmButtonText: 'Release Now',
                        confirmButtonProps: {
                            className: 'button button--pill'
                        }
                    })
                );
                break;
            case 'new-date':
                dispatch(showModal(MODAL_TYPE_CHANGE_RELEASE));
                break;

            default:
                break;
        }
    };

    handleDateChange = (date) => {
        const { item, dispatch, onCountdownFinish } = this.props;

        const newMusicItem = {
            ...item,
            ['released']: date
        };

        dispatch(hideModal());

        dispatch(updateMetadata(item.id, newMusicItem))
            .catch((err) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There was an error changing the realse date of this ${
                            item.type
                        }`
                    })
                );
                console.log(err);
            })
            .finally(() => {
                onCountdownFinish();
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Release date changed succesfully'
                    })
                );
            });
    };

    makePublic(id, type, isPrivate, setPrivate) {
        const { dispatch } = this.props;

        dispatch(alterItemPrivacy(id, setPrivate))
            .then(() => {
                const responseText =
                    setPrivate === 'yes' ? 'Private' : 'Public';

                dispatch(
                    addToast({
                        type: type,
                        action: 'public',
                        item: `Your ${type} is now ${responseText}.`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            'There was a problem editing your playlist privacy.'
                    })
                );
                analytics.error(error);
                console.log(error);
            });

        dispatch(hideModal());
    }

    render() {
        const { item, currentUser } = this.props;

        if (!ownsMusic(currentUser, item)) {
            return null;
        }

        return (
            <Fragment>
                <MusicAlert
                    item={this.props.item}
                    message={this.props.message}
                    icon={this.props.icon}
                    onActionClick={this.handleActionClick}
                />
                <ChangeReleaseModal
                    music={this.props.item}
                    onDateChange={this.handleDateChange}
                />
            </Fragment>
        );
    }
}
