import React from 'react';

import AmMark from 'icons/am-mark';

import styles from './LandingHeader.module.scss';

function LandingHeader() {
    return (
        <div className={styles.header}>
            <AmMark className={`${styles.mark} u-no-fill`} />
            <div className={styles.body}>
                <h2 className={styles.title}>Discover New Playlists</h2>
                <p className={styles.copy}>
                    Audiomack's <strong>specially curated playlists</strong>{' '}
                    help you cut through the noise of today's saturated music
                    scene, getting you the music you love and helping you
                    discover fresh artists moving music forward.
                </p>
            </div>
        </div>
    );
}

export default LandingHeader;
