import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { clamp } from 'utils/index';

export default class ProgressBar extends Component {
    static propTypes = {
        steps: PropTypes.array,
        className: PropTypes.string,
        activeStep: PropTypes.number
    };

    constructor(props) {
        super(props);

        this.state = {
            fill: 0
        };
    }

    componentDidMount() {
        this.measureShit(() => this.fillProgress());
    }

    componentDidUpdate(prevProps) {
        if (this.props.activeStep !== prevProps.activeStep) {
            this.fillProgress(this.props.activeStep);
        }
    }

    componentWillUnmount() {
        this._fills = null;
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    measureShit(cb, attempts = 1) {
        const containerRect = this._container.getBoundingClientRect();
        const steps = this._container.querySelectorAll('.progress-bar__step');

        this._fills = Array.from(steps).map((step) => {
            const stepRect = step.getBoundingClientRect();
            const percent = Math.round(
                ((stepRect.left - containerRect.left + stepRect.width / 2) /
                    containerRect.width) *
                    100
            );

            return clamp(percent, 0, 100);
        });

        const cssNotKickedIn = this._fills.some((fill, i) => {
            if (i === 0) {
                return false;
            }

            // The css isnt making the dots appear in a line yet
            if (fill <= this._fills[i - 1]) {
                return true;
            }

            return false;
        });

        if (cssNotKickedIn) {
            if (attempts < 4) {
                setTimeout(() => {
                    this.measureShit(cb, attempts + 1);
                }, 1000);
                return;
            }

            console.warn(
                'Could not measure progress bar correctly. Check code'
            );
            return;
        }

        cb();
    }

    fillProgress(step = this.props.activeStep) {
        this.setState({
            fill: this._fills[step - 1]
        });
    }

    renderSteps(steps) {
        return steps.map((step, i) => {
            const klass = classnames('progress-bar__step', {
                'progress-bar__step--filled': i + 1 <= this.props.activeStep,
                'progress-bar__step--active': i + 1 === this.props.activeStep
            });

            return (
                <div className={klass} key={i}>
                    {step}
                </div>
            );
        });
    }

    render() {
        const { className, activeStep, steps } = this.props;
        const klass = classnames('progress-bar', {
            [className]: className
        });

        return (
            <div
                className={klass}
                ref={(c) => {
                    this._container = c;
                }}
            >
                <span
                    className="progress-bar__fill"
                    role="progressbar"
                    style={{ width: `${this.state.fill}%` }}
                    aria-label="played"
                    aria-valuemin="1"
                    aria-valuemax={steps.length}
                    aria-valuenow={activeStep}
                    aria-valuetext={`${this.state.fill} percent complete`}
                />
                <div className="progress-bar__steps">
                    {this.renderSteps(this.props.steps)}
                </div>
            </div>
        );
    }
}
