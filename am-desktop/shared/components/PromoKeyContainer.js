import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import classnames from 'classnames';

import connectDataFetchers from 'lib/connectDataFetchers';
import analytics from 'utils/analytics';

import {
    addPromoKey,
    getPromoKeys,
    deletePromoKey,
    reset
} from '../redux/modules/promoKey';
import { addToast } from '../redux/modules/toastNotification';
import PromoKey from './PromoKey';

import styles from './PromoKey.module.scss';

class PromoKeyContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        music: PropTypes.object.isRequired,
        promoKey: PropTypes.object,
        history: PropTypes.object,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            showWpCode: false
        };
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(reset());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleAddKeyFormSubmit = (e) => {
        const { dispatch, music } = this.props;
        const input = e.currentTarget.keyName;
        const key = input.value;

        input.value = '';

        e.preventDefault();

        dispatch(addPromoKey(music.id, key))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Successfully added "${key}" to your private keys`
                    })
                );
                return;
            })
            .catch((error) => {
                console.error(error);
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There was a problem adding the key "${key}"`
                    })
                );
                analytics.error(error);
                return;
            });

        return;
    };

    handleToggleCodeClick = () => {
        this.setState({
            showWpCode: !this.state.showWpCode
        });
    };

    handleDeletePromoLinkClick = (e) => {
        const { dispatch, music } = this.props;

        const keyName = e.currentTarget.getAttribute('data-key');
        const index = e.currentTarget.getAttribute('data-index');

        e.preventDefault();

        dispatch(deletePromoKey(music.id, keyName, index))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Successfully deleted "${keyName}"`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There was a problem deleting the key "${keyName}"`
                    })
                );
                analytics.error(error);
                return;
            });
        return;
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderPromoKeys(promoKey, music) {
        if (!music.uploader) {
            return null;
        }

        const list = promoKey.list.map((obj, i) => {
            if (obj.isDeleting) {
                return null;
            }

            let embedHeight = '417';

            if (music.type === 'song') {
                embedHeight = '252';
            }

            const url = `${process.env.AM_URL}/${music.type}/${
                music.uploader.url_slug
            }/${music.url_slug}?key=${obj.key}`;
            const embedUrl = `${process.env.AM_URL}/embed/${music.type}/${
                music.uploader.url_slug
            }/${music.url_slug}?key=${obj.key}`;
            const iframeCode = `<iframe src="${embedUrl}" scrolling="no" width="100%" height="${embedHeight}" scrollbars="no" frameborder="0"></iframe>`;
            const wpCode = `[audiomack src="${embedUrl}"]`;

            return (
                <PromoKey
                    key={i}
                    index={i}
                    url={url}
                    promoKey={obj}
                    onDeleteKeyClick={this.handleDeletePromoLinkClick}
                    onToggleCodeClick={this.handleToggleCodeClick}
                    showWpCode={this.state.showWpCode}
                    iframeCode={iframeCode}
                    wpCode={wpCode}
                />
            );
        });

        return list;
    }

    render() {
        const { music, promoKey } = this.props;

        return (
            <div>
                <p>
                    If your upload is set to private, you can allow select
                    people to listen to your music by creating and sending them
                    a promotional link.
                </p>

                <p>
                    You can also use promotional link to track plays and views
                    across different marketing channels or see which recipients
                    (i.e. blogs, labels) have listened to your music.{' '}
                    <a
                        href="https://audiomack.com/world/post/using-audiomack-promo-keys-private-links-5d4235ba03c22706ce2c9b2a"
                        target="_blank"
                        rel="noopener"
                    >
                        Read more here
                    </a>
                    .
                </p>

                <form
                    className={styles.form}
                    onSubmit={this.handleAddKeyFormSubmit}
                >
                    <input
                        type="text"
                        name="keyName"
                        className={styles.formInput}
                        placeholder="Enter a keyword and we will create a promotional link below"
                    />
                    <input
                        type="submit"
                        className={`${styles.formSubmit} button button--pill`}
                        defaultValue="Add Link"
                    />
                </form>
                <div className={styles.container}>
                    <div className={classnames(styles.row, styles.rowHead)}>
                        <div className={styles.item}>Name</div>
                        <div
                            className={classnames(styles.item, styles.itemLink)}
                        >
                            link
                        </div>
                        <div
                            className={classnames(
                                styles.item,
                                styles.itemPlays
                            )}
                        >
                            Plays
                        </div>
                        <div
                            className={classnames(
                                styles.item,
                                styles.itemActions
                            )}
                        >
                            Delete
                        </div>
                        <div
                            className={classnames(
                                styles.item,
                                styles.itemActions
                            )}
                        >
                            Embed
                        </div>
                    </div>
                    <div className={styles.content}>
                        {this.renderPromoKeys(promoKey, music)}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        promoKey: state.promoKey
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(
    connectDataFetchers(PromoKeyContainer, [
        (params, query, props) => getPromoKeys(props.music.id)
    ])
);
