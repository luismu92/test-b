import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CustomDatePicker from './CustomDatePicker';
import TimePickerInput from './TimePickerInput';

import { clamp } from 'utils/index';

import CalendarIcon from '../icons/calendar';

export default class DatePickerInput extends Component {
    static propTypes = {
        className: PropTypes.string,
        // moment date
        value: PropTypes.object,
        onBlur: PropTypes.func,
        onChange: PropTypes.func,
        placement: PropTypes.string
    };

    static defaultProps = {
        placement: 'right-start'
    };

    constructor(props) {
        super(props);

        const [hours, minutes, ampm] = props.value.format('HH,mm,a').split(',');

        this.state = {
            ampm,
            hours: parseInt(hours, 10),
            minutes: parseInt(minutes, 10)
        };
    }

    handleTimeChange = (e) => {
        const { name, value } = e.target;

        if (!['ampm', 'hours', 'minutes'].includes(name)) {
            return;
        }

        if (['hours', 'minutes'].includes(name)) {
            const { max, min } = e.target;
            e.target.value = clamp(value, min, max)
                .toString()
                .padStart(2, '0');
        }

        switch (name) {
            case 'ampm':
                this.setState((prevState) => {
                    let hours = prevState.hours;

                    if (value === 'am' && prevState.hours >= 12) {
                        hours -= 12;
                    }

                    if (value === 'pm' && prevState.hours < 12) {
                        hours += 12;
                    }

                    return {
                        ampm: value,
                        hours
                    };
                });
                break;

            case 'hours':
                this.setState((prevState) => {
                    let hours = parseInt(value, 10);

                    if (prevState.ampm === 'pm' && hours < 12) {
                        hours += 12;
                    }

                    return {
                        hours
                    };
                });
                break;

            case 'minutes':
            default:
                this.setState({ [name]: parseInt(value, 10) });
                break;
        }
    };

    handleDateBlur = () => {
        const { onBlur, value } = this.props;
        const { hours, minutes } = this.state;

        value.hours(hours);
        value.minutes(minutes);

        onBlur(value);
    };

    render() {
        return (
            <div className="date-picker">
                <CustomDatePicker
                    selected={this.props.value}
                    onChange={this.props.onChange}
                    onClickOutside={this.handleDateBlur}
                    onBlur={this.handleDateBlur}
                    shouldCloseOnSelect={false}
                    popperPlacement={this.props.placement}
                    popperModifiers={{
                        offset: {
                            enabled: true,
                            offset: '-20px, 20px'
                        },
                        preventOverflow: {
                            enabled: true,
                            boundariesElement: 'viewport'
                        }
                    }}
                >
                    <p className="date-picker__time-header">Time of Release</p>
                    <TimePickerInput
                        momentDate={this.props.value}
                        onChange={this.handleTimeChange}
                    />
                </CustomDatePicker>
                <CalendarIcon className="date-picker__icon" />
            </div>
        );
    }
}
