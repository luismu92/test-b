import React, { Component } from 'react';
import PropTypes from 'prop-types';
import deepmerge from 'deepmerge';

import { humanizeNumber } from 'utils/index';
import { pluralize } from 'utils/string';

import styles from './LineChart.module.scss';

export default class LineChart extends Component {
    static propTypes = {
        height: PropTypes.number,
        displayPulseDot: PropTypes.bool,
        datasets: PropTypes.array,
        labels: PropTypes.array,
        options: PropTypes.object,
        clip: PropTypes.object,
        tooltipLabel: PropTypes.string
    };

    static defaultProps = {
        tooltipStyle: 1,
        options: {},
        displayPulseDot: false,
        // So the line doesnt get cut off towards the top
        clip: { top: -2, bottom: 0, left: 0, right: 0 },
        height: 200
    };

    componentDidMount() {
        this.loadChartLibrary()
            .then((Chart) => {
                this.initChart(Chart);
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    componentDidUpdate(prevProps) {
        const nextDatasets = this.props.datasets;
        const nextLabels = this.props.labels;
        const nextOptions = this.props.options;
        const { datasets, labels, options } = prevProps;
        const prevData = JSON.stringify({
            datasets: datasets,
            ...labels,
            ...options
        });
        const nextData = JSON.stringify({
            datasets: nextDatasets,
            ...nextLabels,
            ...nextOptions
        });
        const changedData = prevData !== nextData;

        if (changedData) {
            this.update(this.props);
        }
    }

    componentWillUnmount() {
        this._canvas = null;
        this._tooltip = null;
        this._tooltipNumber = null;
        this._pulseDot = null;
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    getTooltipPosition(tooltip, props) {
        const { tooltipStyle } = props;
        const tooltipEl = this._tooltip;
        const tooltipNumberEl = this._tooltipNumber;

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
            tooltipEl.style.opacity = 0;
            tooltipNumberEl.style.opacity = 0;
            return;
        }

        let yValue = '';
        let xValue = '';

        // Set Text
        if (tooltip.dataPoints) {
            yValue = tooltip.dataPoints[0].yLabel;

            if (!isNaN(yValue)) {
                yValue = yValue.toLocaleString();
            }

            xValue = tooltip.dataPoints[0].xLabel;
        }

        const positionX = this._chart.canvas.offsetLeft;

        tooltipEl.style.opacity = 1;

        const left = `${positionX + tooltip.caretX}px`;

        tooltipEl.style.left = left;

        if (tooltipStyle === 2) {
            tooltipEl.textContent = xValue;
            tooltipEl.style.top = '50%';
            tooltipEl.style.transform = 'translate(-50%, -50%)';
            tooltipNumberEl.textContent = pluralize(
                yValue,
                this.props.tooltipLabel
            );
            tooltipNumberEl.style.left = left;
            tooltipNumberEl.style.whiteSpace = 'nowrap';
            tooltipNumberEl.style.transform = 'translate(-50%, 0)';
            tooltipNumberEl.style.opacity = 1;
        } else {
            tooltipEl.textContent = pluralize(yValue, this.props.tooltipLabel);
        }
    }

    getOptions(props = this.props) {
        const { options } = props;
        const ctx = this._canvas.getContext('2d');
        const gradientStroke = ctx.createLinearGradient(0, 0, 0, 100);
        const lightOrange = '#ffa200';
        const darkOrange = '#fdc35e';

        gradientStroke.addColorStop(0, lightOrange);
        gradientStroke.addColorStop(1, darkOrange);

        const orange = '#ffa200';
        const gray8 = '#b2b2b2';

        const chartOptions = {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false,
                intersect: false,
                custom: (tooltip) => this.getTooltipPosition(tooltip, props)
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            padding: 10,
                            fontFamily: 'Open Sans',
                            fontColor: gray8,
                            fontSize: 14,
                            callback(label) {
                                if (!isNaN(label)) {
                                    return humanizeNumber(label);
                                }

                                return label;
                            }
                            // padding: 20
                        },
                        // afterBuildTicks(chart) {
                        // Remove bottom tick
                        // chart.ticks = chart.ticks.filter((x) => x !== 0);
                        // },
                        gridLines: {
                            drawBorder: false,
                            zeroLineColor: 'transparent'
                        }
                    }
                ],
                xAxes: [
                    {
                        ticks: {
                            padding: 0,
                            fontFamily: 'Open Sans',
                            fontSize: 11,
                            fontColor: gray8
                        },
                        gridLines: {
                            drawBorder: false,
                            display: false
                        }
                    }
                ]
            }
        };

        const datasets = props.datasets.map((dataset) => {
            return {
                borderColor: orange,
                pointRadius: 0,
                fill: true,
                backgroundColor: gradientStroke,
                borderWidth: 2,
                ...dataset
            };
        });

        return {
            type: 'LineWithLine',
            data: {
                labels: props.labels,
                datasets: datasets
            },
            options: deepmerge.all([chartOptions, options], {
                arrayMerge: (destination, source) => {
                    // Dont merge arrays
                    return source;
                }
            })
        };
    }

    loadChartLibrary() {
        return new Promise((resolve) => {
            require.ensure([], (require) => {
                const Chart = require('chart.js');

                resolve(Chart);
            });
        });
    }

    initChart(Chart) {
        const pulseDot = this._pulseDot;

        // https://stackoverflow.com/questions/45159895/moving-vertical-line-when-hovering-over-the-chart-using-chart-js
        Chart.defaults.LineWithLine = Chart.defaults.line;
        Chart.controllers.LineWithLine = Chart.controllers.line.extend({
            draw(ease) {
                Chart.controllers.line.prototype.draw.call(this, ease);

                const data = this.chart.getDatasetMeta(0).data;

                if (pulseDot) {
                    const lastPoint = data[data.length - 1];

                    let lastPointX = -999999;
                    let lastPointY = -999999;

                    if (lastPoint) {
                        const center = lastPoint.getCenterPoint();

                        lastPointX = center.x;
                        lastPointY = center.y;
                    }

                    pulseDot.style.opacity = 1;
                    pulseDot.style.left = `${lastPointX}px`;
                    pulseDot.style.top = `${lastPointY}px`;
                }

                if (
                    this.chart.tooltip._active &&
                    this.chart.tooltip._active.length
                ) {
                    const activePoint = this.chart.tooltip._active[0];
                    const ctx = this.chart.ctx;
                    const x = activePoint.tooltipPosition().x;
                    const tooltipValuePadding = 30;
                    const topY =
                        this.chart.scales['y-axis-0'].top +
                        tooltipValuePadding -
                        this.chart.options.layout.padding.top;
                    const bottomGraphPadding = 2;
                    const bottomY =
                        this.chart.scales['y-axis-0'].bottom -
                        bottomGraphPadding;

                    // console.log(x, activePoint, topY, bottomY);

                    ctx.save();

                    // draw triangle on line
                    ctx.beginPath();
                    ctx.moveTo(x - 6, topY);
                    ctx.lineTo(x + 6, topY);
                    ctx.lineTo(x, topY + 6);
                    ctx.fillStyle = '#ffa200';
                    ctx.fill();
                    ctx.closePath();

                    // draw line
                    ctx.beginPath();
                    ctx.moveTo(x, topY);
                    ctx.lineTo(x, bottomY);
                    ctx.lineWidth = 2;
                    ctx.strokeStyle = '#ffa200';
                    ctx.stroke();
                    ctx.restore();
                }
            }
        });

        const clip = {
            ...LineChart.defaultProps.clip,
            ...this.props.clip
        };

        Chart.canvasHelpers.clipArea = function(ctx, clipArea) {
            const x = clipArea.left + clip.left;
            const y = clipArea.top + clip.top;
            const width = clipArea.right - clipArea.left - clip.right;
            const height = clipArea.bottom - clipArea.top - clip.bottom;

            ctx.save();
            ctx.beginPath();

            // Regular rectangle
            // ctx.rect(x, y, width, height);

            // Rounded rectangle
            const radius = 5;

            ctx.moveTo(x + radius, y);
            ctx.lineTo(x + width - radius, y);
            ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
            ctx.lineTo(x + width, y + height - radius);
            ctx.quadraticCurveTo(
                x + width,
                y + height,
                x + width - radius,
                y + height
            );
            ctx.lineTo(x + radius, y + height);
            ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
            ctx.lineTo(x, y + radius);
            ctx.quadraticCurveTo(x, y, x + radius, y);

            // Clip
            ctx.clip();
        };

        const ctx = this._canvas.getContext('2d');

        this._chart = new Chart(ctx, this.getOptions());
    }

    update(props) {
        if (!this._chart) {
            return;
        }

        this._chart.data = this.getOptions(props).data;
        this._chart.update();
    }

    render() {
        const { displayPulseDot } = this.props;

        let pulseDot;

        if (displayPulseDot) {
            pulseDot = (
                <span
                    className={styles.pulseDot}
                    ref={(c) => {
                        this._pulseDot = c;
                    }}
                />
            );
        }

        return (
            <div className={styles.container}>
                <canvas
                    ref={(c) => {
                        this._canvas = c;
                    }}
                    style={{
                        height: this.props.height
                    }}
                />
                <span
                    className={styles.tooltip}
                    ref={(c) => {
                        this._tooltip = c;
                    }}
                />
                <span
                    className={styles.tooltipValue}
                    ref={(c) => {
                        this._tooltipNumber = c;
                    }}
                />
                {pulseDot}
            </div>
        );
    }
}
