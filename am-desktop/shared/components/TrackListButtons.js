import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { provideHttp, ownsMusic, yesBool } from 'utils/index';
import { isAlreadyFavorited, isAlreadyReupped } from 'utils/user';

import PlayIcon from '../icons/play';
import HeartIcon from 'icons/heart';
import HeartOutlineIcon from 'icons/heart-outline';
import RetweetIcon from 'icons/retweet';
import ThreeDotsIcon from '../icons/three-dots-alt';
import PlusIcon from 'icons/plus';
import CogIcon from 'icons/cog';
import CommentIcon from 'icons/comment';

import MusicActionButton from 'buttons/MusicActionButton';

import BodyClickListener from '../components/BodyClickListener';

export default class TrackListButtons extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        currentUser: PropTypes.object,
        currentUserPinned: PropTypes.object,
        className: PropTypes.string,
        containerElement: PropTypes.string,
        isCurrentSong: PropTypes.bool,
        hideQueueLastAction: PropTypes.bool,
        loading: PropTypes.bool,
        disablePlay: PropTypes.bool,
        onActionClick: PropTypes.func,
        onPlayClick: PropTypes.func,
        onShuffleClick: PropTypes.func
    };

    static defaultProps = {
        containerElement: 'div',
        onActionClick() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            tooltipActive: false
        };
    }

    componentWillUnmount() {
        this._tooltipButton = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTooltipClick = () => {
        this.setState({
            tooltipActive: !this.state.tooltipActive
        });
    };

    handleScrollToComments = () => {
        const comments = document.getElementById('comments');

        comments.scrollIntoView({ behavior: 'smooth' });
    };

    renderPinButton() {
        const { item, currentUser, currentUserPinned } = this.props;

        if (!currentUser.isLoggedIn) {
            return null;
        }

        const isPlaylist = item.type === 'playlist';
        const isOwnMusic =
            !isPlaylist &&
            currentUser.isLoggedIn &&
            currentUser.profile.id === item.uploader.id;
        const userPins = currentUserPinned.list;
        const isPinned = userPins.find((pin) => pin.id === item.id);
        const needsReup =
            currentUser.profile.reups &&
            currentUser.profile.reups.indexOf(item.id) === -1;

        let pinAction = 'pin';
        let pinText = 'Highlight';

        if (isPinned) {
            pinAction = 'unpin';
            pinText = 'Highlighted';
        }

        if (!isOwnMusic && !isPinned && needsReup && !isPlaylist) {
            pinAction = 'pin-reup';
        }

        return (
            <li className="sub-menu__item">
                <button
                    className="u-tt-cap"
                    data-action={pinAction}
                    onClick={this.props.onActionClick}
                >
                    {pinText}
                </button>
            </li>
        );
    }

    renderMoreMenu() {
        const { onShuffleClick, item, currentUser } = this.props;
        const ownMusic = ownsMusic(currentUser, item);

        let shuffleButton;

        if (item.type !== 'song') {
            shuffleButton = (
                <li className="sub-menu__item">
                    <button onClick={onShuffleClick} className="u-tt-cap">
                        Shuffle
                    </button>
                </li>
            );
        }

        let buyButton;

        if (item.buy_link) {
            buyButton = (
                <li className="sub-menu__item">
                    <a
                        aria-label="Buy now"
                        href={provideHttp(item.buy_link)}
                        target="_blank"
                        rel="nofollow noopener"
                        className="u-tt-cap"
                    >
                        Buy
                    </a>
                </li>
            );
        }

        let embedButton = (
            <li className="sub-menu__item">
                <button
                    aria-label="Show embed dialog"
                    data-action="embed"
                    onClick={this.props.onActionClick}
                    className="u-tt-cap"
                >
                    Embed
                </button>
            </li>
        );

        if (yesBool(item.private) && !ownMusic) {
            embedButton = null;
        }

        let videoButton;

        if (item.video) {
            videoButton = (
                <li className="sub-menu__item">
                    <button
                        aria-label="Play Video"
                        data-action="video"
                        onClick={this.props.onActionClick}
                        className="u-tt-cap"
                    >
                        Video
                    </button>
                </li>
            );
        }

        let atEnd;
        let nextText = 'Add to Queue';
        if (!this.props.hideQueueLastAction) {
            nextText = 'Queue Next';

            atEnd = (
                <li className="sub-menu__item">
                    <button
                        data-action="add-to-queue-end"
                        onClick={this.props.onActionClick}
                    >
                        Queue End
                    </button>
                </li>
            );
        }

        const queueAddActions = (
            <Fragment>
                <li className="sub-menu__item">
                    <button
                        data-action="add-to-queue-next"
                        onClick={this.props.onActionClick}
                    >
                        {nextText}
                    </button>
                </li>
                {atEnd}
            </Fragment>
        );

        const tooltipClass = classnames(
            'listen__tracklist-submenu tooltip tooltip--down-right-arrow sub-menu sub-menu--condensed',
            {
                'tooltip--active': this.state.tooltipActive
            }
        );

        const menu = (
            <ul className={tooltipClass}>
                {shuffleButton}
                {this.renderPinButton()}
                {embedButton}
                {queueAddActions}
                {videoButton}
                {buyButton}
            </ul>
        );

        return (
            <Fragment>
                <BodyClickListener
                    shouldListen={this.state.tooltipActive}
                    onClick={this.handleTooltipClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <span style={{ position: 'relative' }}>
                    <MusicActionButton
                        onClick={this.handleTooltipClick}
                        buttonProps={{
                            'aria-label': 'Show more actions'
                        }}
                        icon={<ThreeDotsIcon />}
                        type="more"
                        label="More"
                        variant="desktop"
                        isActive={this.state.tooltipActive}
                        style={{
                            margin: 0
                        }}
                    />
                    {menu}
                </span>
            </Fragment>
        );
    }

    render() {
        const {
            loading,
            isCurrentSong,
            onPlayClick,
            className,
            containerElement,
            item,
            currentUser,
            disablePlay
        } = this.props;
        const { profile } = currentUser;
        const isLoading = isCurrentSong && loading;
        const klass = classnames('listen__tracklist-buttons', {
            [className]: className
        });
        const containerProps = {
            className: klass
        };

        let reupButton;

        if (
            item.type !== 'playlist' &&
            (!profile ||
                (profile &&
                    item.uploader.id &&
                    profile.id !== item.uploader.id))
        ) {
            reupButton = (
                <MusicActionButton
                    onClick={this.props.onActionClick}
                    buttonProps={{
                        'data-action': 'reup',
                        'aria-label': `Re-up ${item.type}`
                    }}
                    type="reup"
                    icon={<RetweetIcon />}
                    label={item.stats.reposts}
                    variant="desktop"
                    isActive={isAlreadyReupped(currentUser, item)}
                />
            );
        }

        let playlistButton;

        if (item.type === 'song') {
            playlistButton = (
                <MusicActionButton
                    onClick={this.props.onActionClick}
                    buttonProps={{
                        'data-action': 'playlist',
                        'aria-label': 'Add song to a playlist'
                    }}
                    icon={<PlusIcon />}
                    label={item.stats.playlists}
                    variant="desktop"
                />
            );
        }

        let commentButton;

        if (item.stats.comments > 0) {
            commentButton = (
                <MusicActionButton
                    onClick={this.handleScrollToComments}
                    buttonProps={{
                        'aria-label': 'Comments'
                    }}
                    icon={<CommentIcon />}
                    label={item.stats.comments}
                    variant="desktop"
                />
            );
        }

        let adminButton;
        let adminTagsButton;

        if (currentUser.isAdmin) {
            if (item.type !== 'playlist') {
                adminButton = (
                    <MusicActionButton
                        onClick={this.props.onActionClick}
                        buttonProps={{
                            'data-action': 'admin'
                        }}
                        icon={<CogIcon />}
                        label="Admin"
                        variant="desktop"
                    />
                );
            } else {
                adminTagsButton = (
                    <MusicActionButton
                        onClick={this.props.onActionClick}
                        buttonProps={{
                            'data-action': 'tags'
                        }}
                        icon={<CogIcon />}
                        label="Tags"
                        variant="desktop"
                    />
                );
            }
        }

        let favIcon = <HeartOutlineIcon />;
        if (isAlreadyFavorited(currentUser, item)) {
            favIcon = <HeartIcon />;
        }

        return React.createElement(
            containerElement,
            containerProps,
            <Fragment>
                <MusicActionButton
                    onClick={onPlayClick}
                    buttonProps={{
                        'data-play-start': '',
                        disabled: isLoading || disablePlay,
                        'aria-label': `Play ${item.type}`
                    }}
                    type="play"
                    icon={<PlayIcon />}
                    label={item.stats.plays}
                    variant="desktop"
                />
                <MusicActionButton
                    onClick={this.props.onActionClick}
                    buttonProps={{
                        'data-action': 'favorite',
                        'aria-label': `Add ${item.type} to your favorites"`
                    }}
                    type="favorite"
                    icon={favIcon}
                    label={item.stats.favorites}
                    variant="desktop"
                    isActive={isAlreadyFavorited(currentUser, item)}
                />
                {playlistButton}
                {reupButton}
                {commentButton}
                {adminButton}
                {adminTagsButton}
                {this.renderMoreMenu()}
            </Fragment>
        );
    }
}
