import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { provideHttp, humanizeNumber } from 'utils/index';

import PlayIcon from '../icons/play';
import HeartIcon from '../../../am-shared/icons/heart';
import PlusIcon from '../icons/plus';
import QueueIcon from '../icons/queue';
import QueueNextIcon from '../icons/queue-next';
import QueueEndIcon from '../icons/queue-end';
import PinIcon from '../../../am-shared/icons/pin';
import BumpIcon from '../icons/bump';
import BumpOneIcon from '../icons/bump-one';
import EmbedIcon from '../icons/embed-close';
import RetweetIcon from '../icons/retweet';
import PowerIcon from '../icons/power';
import CartIcon from '../icons/shopping-cart';
import AdminCogIcon from '../icons/admin-cog';
import CommentsIcon from 'icons/comment';
import { COLLECTION_TYPE_TRENDING } from 'constants/index';

class Interactions extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        musicIndex: PropTypes.number,
        currentUser: PropTypes.object,
        currentUserPinned: PropTypes.object,
        onActionClick: PropTypes.func,
        context: PropTypes.string,
        stats: PropTypes.object.isRequired,
        disableKeyboard: PropTypes.bool
    };

    static defaultProps = {
        onActionClick() {},
        disableKeyboard: false
    };

    render() {
        const {
            item,
            stats,
            currentUser,
            currentUserPinned,
            disableKeyboard
        } = this.props;
        const profile = currentUser.profile;
        const isPlaylist = item.type === 'playlist';
        const tabIndex = disableKeyboard ? '-1' : '0';

        const plays = stats['plays-raw'];
        const favorites = stats['favorites-raw'];
        const reposts = stats['reposts-raw'];
        const playlists = stats['playlists-raw'];
        const playsTooltip = 'Play';

        let favoriteTooltip = 'Favorite';
        let favoriteClass = 'music-interaction';
        let reupTooltip = 'Re-Up';
        let reupClass = 'music-interaction';

        if (profile) {
            if (
                (isPlaylist &&
                    profile.favorite_playlists &&
                    profile.favorite_playlists.indexOf(item.id) !== -1) ||
                (!isPlaylist &&
                    profile.favorite_music &&
                    profile.favorite_music.indexOf(item.id) !== -1)
            ) {
                favoriteTooltip = 'Unfavorite';
                favoriteClass += ' music-interaction--active';
            }

            if (profile.reups && profile.reups.indexOf(item.id) !== -1) {
                reupClass += ' music-interaction--active';
                reupTooltip = 'Remove Re-Up';
            }
        }

        let buyButton;

        if (item.buy_link) {
            buyButton = (
                <a
                    className="music-interaction"
                    aria-label="Buy now"
                    href={provideHttp(item.buy_link)}
                    target="_blank"
                    rel="nofollow noopener"
                    tabIndex={tabIndex}
                >
                    <span className="music-interaction__inner">
                        <CartIcon className="music-interaction__icon music-interaction__icon--buy" />
                        <span className="music-interaction__text">Buy</span>
                    </span>
                </a>
            );
        }

        let reupButton;

        if (
            !isPlaylist &&
            (!profile ||
                (profile &&
                    item.uploader.id &&
                    profile.id !== item.uploader.id))
        ) {
            reupButton = (
                <button
                    className={reupClass}
                    title="Re-Up"
                    data-tooltip="Re-Up"
                    aria-label={reupTooltip}
                    data-action="reup"
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                >
                    <span className="music-interaction__inner">
                        <RetweetIcon className="music-interaction__icon" />
                        <span className="music-interaction__count">
                            {humanizeNumber(reposts)}
                        </span>
                    </span>
                </button>
            );
        }

        let playlistButton;

        if (item.type === 'song' && item.private !== 'yes') {
            playlistButton = (
                <button
                    className="music-interaction"
                    title="Add to Playlist"
                    data-tooltip="Add to Playlist"
                    aria-label="Add to Playlist"
                    data-action="playlist"
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                >
                    <span className="music-interaction__inner">
                        <PlusIcon className="music-interaction__icon music-interaction__icon--playlist" />
                        <span className="music-interaction__count">
                            {humanizeNumber(playlists)}
                        </span>
                    </span>
                </button>
            );
        }

        let adminButton;

        if (currentUser.isAdmin && item.type !== 'playlist') {
            adminButton = (
                <button
                    className="music-interaction"
                    aria-label="Admin Actions"
                    data-action="admin"
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                    data-tooltip="Admin"
                >
                    <span className="music-interaction__inner">
                        <AdminCogIcon className="music-interaction__icon music-interaction__icon--admin u-margin-0" />
                    </span>
                </button>
            );
        }

        let playlistTagButton;

        if (currentUser.isAdmin && isPlaylist) {
            playlistTagButton = (
                <button
                    className="music-interaction"
                    onClick={this.props.onActionClick}
                    data-action="tags"
                    data-id={item.id}
                    data-tooltip="Show Playlist Tags"
                    tabIndex={tabIndex}
                >
                    <span className="music-interaction__inner">
                        <PowerIcon className="music-interaction__icon music-interaction__icon--admin u-margin-0" />
                    </span>
                </button>
            );
        }
        let bumpUpTopButton;
        let bumpUpOneButton;

        if (
            currentUser.isAdmin &&
            this.props.context === COLLECTION_TYPE_TRENDING
        ) {
            bumpUpTopButton = (
                <button
                    className="music-interaction"
                    aria-label="Bump to the Top"
                    data-action="bump"
                    data-tooltip="Bump music to top"
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                >
                    <span className="music-interaction__inner">
                        <BumpIcon className="music-interaction__icon music-interaction__icon--bump u-margin-0" />
                    </span>
                </button>
            );
            bumpUpOneButton = (
                <button
                    className="music-interaction"
                    aria-label="Bump music one spot"
                    data-action="bumpMusicOneSpot"
                    data-tooltip="Bump music one spot"
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                >
                    <span className="music-interaction__inner">
                        <BumpOneIcon className="music-interaction__icon music-interaction__icon--bump u-margin-0" />
                    </span>
                </button>
            );
        }

        let favoriteButton;

        if (item.private !== 'yes') {
            favoriteButton = (
                <button
                    className={favoriteClass}
                    title="Favorite"
                    data-tooltip="Favorite"
                    aria-label={favoriteTooltip}
                    data-action="favorite"
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                >
                    <span className="music-interaction__inner">
                        <HeartIcon className="music-interaction__icon music-interaction__icon--fav" />
                        <span className="music-interaction__count">
                            {humanizeNumber(favorites)}
                        </span>
                    </span>
                </button>
            );
        }

        let pinButton;

        if (currentUser.isLoggedIn && profile) {
            const isOwnMusic =
                currentUser.isLoggedIn &&
                currentUser.profile.id === item.uploader.id;
            const needsReup =
                profile.reups && profile.reups.indexOf(item.id) === -1;

            const userPins = currentUserPinned.list;
            const isPinned = userPins.find((pin) => pin.id === item.id);

            let pinTitle = 'Highlight';
            let pinTooltip = pinTitle;
            let pinAction = 'pin';
            let pinClass = 'music-interaction';

            if (isPinned) {
                pinTitle = 'Highlighted';
                pinTooltip = 'Remove Highlight';
                pinAction = 'unpin';
                pinClass += ' music-interaction--active';
            }

            if (!isOwnMusic && !isPinned && needsReup && !isPlaylist) {
                pinAction = 'pin-reup';
            }

            pinButton = (
                <button
                    className={pinClass}
                    data-tooltip={pinTooltip}
                    arial-label={pinTooltip}
                    data-action={pinAction}
                    tabIndex={tabIndex}
                    onClick={this.props.onActionClick}
                >
                    <span className="music-interaction__inner">
                        <PinIcon className="music-interaction__icon music-interaction__icon--pin" />
                        {pinTitle}
                    </span>
                </button>
            );
        }

        return (
            <div className="row u-d-flex--justify-between u-spacing-top-em music-interactions">
                <div className="music-interactions__counts column small-auto">
                    <button
                        className="music-interaction"
                        data-tooltip={playsTooltip}
                        aria-label={playsTooltip}
                        data-action="play"
                        data-music-index={this.props.musicIndex}
                        tabIndex={tabIndex}
                        onClick={this.props.onActionClick}
                    >
                        <span className="music-interaction__inner">
                            <PlayIcon className="music-interaction__icon music-interaction__icon--play" />
                            <span className="music-interaction__count">
                                {humanizeNumber(plays)}
                            </span>
                        </span>
                    </button>
                    {favoriteButton}
                    {playlistButton}
                    {reupButton}
                    <Link
                        to={`/${item.type}/${item.uploader.url_slug}/${
                            item.url_slug
                        }`}
                        className="music-interaction music-interaction--comments"
                        title="Comments"
                        data-tooltip="Comments"
                        tabIndex={tabIndex}
                    >
                        <span className="music-interaction__inner">
                            <CommentsIcon className="music-interaction__icon music-interaction__icon--comment" />
                            <span className="music-interaction__count">
                                {item.stats.comments}
                            </span>
                        </span>
                    </Link>
                </div>
                <div className="music-interactions__words column small-auto">
                    <span className="music-interactions__admin">
                        {bumpUpTopButton}
                        {bumpUpOneButton}
                        {adminButton}
                    </span>
                    {buyButton}
                    {pinButton}
                    <span className="music-interaction music-interaction--queue">
                        <ul className="tooltip tooltip--queue sub-menu tooltip--down-arrow u-text-left">
                            <li className="sub-menu__item">
                                <button
                                    aria-label="Add next up in the queue"
                                    data-action="add-to-queue-next"
                                    tabIndex={tabIndex}
                                    onClick={this.props.onActionClick}
                                >
                                    <QueueNextIcon className="sub-menu__icon" />
                                    Queue Next
                                </button>
                            </li>
                            <li className="sub-menu__item">
                                <button
                                    aria-label="Add to end of queue"
                                    data-action="add-to-queue-end"
                                    tabIndex={tabIndex}
                                    onClick={this.props.onActionClick}
                                >
                                    <QueueEndIcon className="sub-menu__icon sub-menu__icon--queue-end" />
                                    Queue End
                                </button>
                            </li>
                        </ul>
                        <span className="music-interaction__inner">
                            <QueueIcon className="music-interaction__icon" />
                            <span className="music-interaction__text">
                                Add to queue
                            </span>
                        </span>
                    </span>
                    <button
                        className="music-interaction"
                        aria-label="Embed"
                        data-action="embed"
                        tabIndex={tabIndex}
                        onClick={this.props.onActionClick}
                    >
                        <span className="music-interaction__inner">
                            <EmbedIcon className="music-interaction__icon" />
                            <span className="music-interaction__text">
                                Embed
                            </span>
                        </span>
                    </button>
                    {playlistTagButton}
                </div>
            </div>
        );
    }
}

export default Interactions;
