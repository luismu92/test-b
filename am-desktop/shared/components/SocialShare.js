import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { getTwitterShareLink, getFacebookShareLink } from 'utils/index';

import FacebookIcon from '../icons/facebook-letter-logo';
import TwitterIcon from '../icons/twitter-logo-new';

import styles from './SocialShare.module.scss';

export default function SocialShare({
    item,
    iconSize,
    disableKeyboard,
    style
}) {
    const tabIndex = disableKeyboard ? '-1' : '0';

    return (
        <div className={styles.container} style={style || {}}>
            <a
                href={getTwitterShareLink(process.env.AM_URL, item)}
                target="_blank"
                rel="nofollow noopener"
                aria-label="Share on Twitter"
                tabIndex={tabIndex}
                className={classnames(styles.link, styles.twitter)}
                style={{
                    width: iconSize,
                    height: iconSize
                }}
            >
                <TwitterIcon />
            </a>

            <a
                href={getFacebookShareLink(
                    process.env.AM_URL,
                    item,
                    process.env.FACEBOOK_APP_ID
                )}
                target="_blank"
                rel="nofollow noopener"
                aria-label="Share on Facebook"
                tabIndex={tabIndex}
                className={classnames(styles.link, styles.facebook)}
                style={{
                    width: iconSize,
                    height: iconSize
                }}
            >
                <FacebookIcon />
            </a>
        </div>
    );
}

SocialShare.propTypes = {
    item: PropTypes.object.isRequired,
    iconSize: PropTypes.number,
    disableKeyboard: PropTypes.bool,
    style: PropTypes.object
};

SocialShare.defaultProps = {
    disableKeyboard: false
};
