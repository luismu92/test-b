import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import deepmerge from 'deepmerge';

export default class BarChart extends Component {
    static propTypes = {
        dataKey: PropTypes.string.isRequired,
        height: PropTypes.number,
        datasets: PropTypes.array,
        preventUpdate: PropTypes.bool,
        labels: PropTypes.array,
        options: PropTypes.object,
        clip: PropTypes.object
    };

    static defaultProps = {
        preventUpdate: false,
        options: {},
        clip: { top: 0, bottom: 0, left: 0, right: 0 },
        height: 300
    };

    componentDidMount() {
        this.loadChartLibrary()
            .then((Chart) => {
                this.initChart(Chart);
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    shouldComponentUpdate(nextProps) {
        return !nextProps.preventUpdate;
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.dataKey !== this.props.dataKey &&
            !this.props.preventUpdate
        ) {
            this.update();
        }
    }

    componentWillUnmount() {
        this._canvas = null;
        this._tooltip = null;
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    getOptions() {
        const { options } = this.props;
        const ctx = this._canvas.getContext('2d');
        const gradientStroke = ctx.createLinearGradient(0, 0, 0, 100);
        const hoverGradientStroke = ctx.createLinearGradient(0, 0, 0, 100);
        const tooltipEl = this._tooltip;
        const orange = '#ffa200';
        const lightOrange = '#ffbe02';
        const lightGray = '#b8b8b8';
        const darkGray = '#7b7b7b';
        const gray8 = '#666';

        gradientStroke.addColorStop(0, lightOrange);
        gradientStroke.addColorStop(1, orange);

        hoverGradientStroke.addColorStop(0, lightGray);
        hoverGradientStroke.addColorStop(0, darkGray);

        const chartOptions = {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false,
                intersect: false,
                custom(tooltip) {
                    // Hide if no tooltip
                    if (tooltip.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    let xTranslate = 0;

                    // Set Text
                    if (tooltip.dataPoints) {
                        const value = tooltip.dataPoints[0].yLabel;

                        // No need to show tooltip for 0 values
                        if (value === 0) {
                            tooltipEl.style.opacity = 0;
                            return;
                        }

                        tooltipEl.innerHTML = `$<strong>${value}</strong>`;

                        const barWidth = this._chart.getDatasetMeta(0).data[
                            tooltip.dataPoints[0].index
                        ]._model.width;

                        xTranslate = `${-barWidth / 2}px`;
                    }

                    const left = `${tooltip.caretX}px`;
                    const top = `${tooltip.y}px`;

                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.transform = `translate(${xTranslate}, -100%)`;
                    tooltipEl.style.top = top;
                    tooltipEl.style.left = left;
                }
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            fontColor: gray8,
                            fontWeight: 600,
                            fontSize: 14,
                            padding: 20,
                            fontFamily: 'Open Sans',
                            callback(label) {
                                return `$${label}`;
                            }
                        },
                        gridLines: {
                            drawBorder: false
                        }
                    }
                ],
                xAxes: [
                    {
                        gridLines: {
                            drawBorder: false,
                            display: false
                        },
                        ticks: {
                            padding: 10,
                            fontSize: 14,
                            fontColor: gray8,
                            fontWeight: 600,
                            fontFamily: 'Open Sans'
                        }
                    }
                ]
            }
        };

        const datasets = this.props.datasets.map((dataset) => {
            return {
                backgroundColor: gradientStroke,
                hoverBackgroundColor: hoverGradientStroke,
                ...dataset
            };
        });

        return {
            type: 'bar',
            data: {
                labels: this.props.labels,
                datasets: datasets
            },
            options: deepmerge.all([chartOptions, options], {
                arrayMerge: (destination, source) => {
                    // Dont merge arrays
                    return source;
                }
            })
        };
    }

    loadChartLibrary() {
        return new Promise((resolve) => {
            require.ensure([], (require) => {
                const Chart = require('chart.js');

                resolve(Chart);
            });
        });
    }

    initChart(Chart) {
        const clip = {
            ...BarChart.defaultProps.clip,
            ...this.props.clip
        };

        Chart.canvasHelpers.clipArea = function(ctx, clipArea) {
            const x = clipArea.left + clip.left;
            const y = clipArea.top + clip.top;
            const width = clipArea.right - clipArea.left - clip.right;
            const height = clipArea.bottom - clipArea.top - clip.bottom;

            ctx.save();
            ctx.beginPath();

            // Regular rectangle
            // ctx.rect(x, y, width, height);

            // Rounded rectangle
            const radius = 5;

            ctx.moveTo(x + radius, y);
            ctx.lineTo(x + width - radius, y);
            ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
            ctx.lineTo(x + width, y + height - radius);
            ctx.quadraticCurveTo(
                x + width,
                y + height,
                x + width - radius,
                y + height
            );
            ctx.lineTo(x + radius, y + height);
            ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
            ctx.lineTo(x, y + radius);
            ctx.quadraticCurveTo(x, y, x + radius, y);

            // Clip
            ctx.clip();
        };

        const ctx = this._canvas.getContext('2d');

        this._chart = new Chart(ctx, this.getOptions());

        window.chart = this._chart;
        window.getOptions = () => this.getOptions();
    }

    update() {
        if (!this._chart) {
            return;
        }

        this._chart.data = this.getOptions(this.props).data;
        this._chart.update();
    }

    render() {
        return (
            <Fragment>
                <canvas
                    ref={(c) => {
                        this._canvas = c;
                    }}
                    style={{
                        height: this.props.height
                    }}
                />
                <span
                    className="tooltip tooltip--radius tooltip--down-arrow"
                    ref={(c) => {
                        this._tooltip = c;
                    }}
                />
            </Fragment>
        );
    }
}
