import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { passiveOption, getDynamicImageProps } from 'utils/index';
import Verified from 'components/Verified';

import AndroidLoader from '../loaders/AndroidLoader';
import {
    setArtist,
    activatePopover,
    setPosition,
    deactivatePopover
} from '../redux/modules/artist/popover';
import FollowButtonContainer from '../components/FollowButtonContainer';

import styles from './ArtistPopover.module.scss';

class ArtistPopover extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        loading: PropTypes.bool.isRequired,
        active: PropTypes.bool.isRequired,
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired,
        artist: PropTypes.object
    };

    componentDidMount() {
        const option = passiveOption();

        document.body.addEventListener(
            'mouseover',
            this.handleMouseOver,
            option
        );
        document.body.addEventListener('click', this.handleMouseClick, option);
        window.addEventListener('scroll', this.handleWindowScroll, option);
    }

    componentWillUnmount() {
        document.body.removeEventListener('mouseover', this.handleMouseOver);
        document.body.removeEventListener('click', this.handleMouseClick);
        window.removeEventListener('scroll', this.handleWindowScroll);
        this._component = null;
        this._activeTarget = null;
        this._isHoveringOnPopover = null;
        this._popoverTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleMouseOver = ({ target }) => {
        const { dispatch } = this.props;

        clearTimeout(this._popoverTimer);

        if (this._activeTarget) {
            const component = this._component;

            if (
                component &&
                component !== target &&
                !component.contains(target)
            ) {
                this._popoverTimer = setTimeout(() => {
                    this._activeTarget = null;

                    dispatch(deactivatePopover());
                }, 200);
            }

            return;
        }

        this._popoverTimer = setTimeout(() => {
            if (
                target.tagName.toLowerCase() === 'a' &&
                typeof target.getAttribute('data-popover') === 'string'
            ) {
                const slug = target.pathname.match(/^\/artist\/(.*)/);

                if (slug) {
                    const artistSlugParts = slug[1].split('/');
                    const artistSlug = artistSlugParts[0].split('?')[0];

                    if (!artistSlugParts[1]) {
                        this._activeTarget = target;

                        const rect = target.getBoundingClientRect();
                        const x = rect.left + rect.width / 2;
                        const y = rect.top;

                        dispatch(setArtist(artistSlug));
                        dispatch(setPosition(x, y));
                        dispatch(activatePopover());
                    }
                }
            }
        }, 1000);
    };

    handleMouseClick = ({ target }) => {
        const { dispatch } = this.props;

        // If we're clicking on the user's link, lets hide the popover
        // immediately
        if (this._activeTarget && target.tagName.toLowerCase() === 'a') {
            if (this._component && this._component.contains(target)) {
                clearTimeout(this._popoverTimer);
                dispatch(deactivatePopover());
            }
        }
    };

    handleWindowScroll = () => {
        const { dispatch, active } = this.props;

        if (active) {
            clearTimeout(this._popoverTimer);
            dispatch(deactivatePopover());
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderContent(loading, artist) {
        if (loading) {
            return <AndroidLoader />;
        }

        const check = (
            <Verified
                user={artist}
                small
                style={{
                    marginLeft: 10
                }}
            />
        );

        const count = artist.followers_count || 0;
        const [src, srcSet] = getDynamicImageProps(
            artist.image_base || artist.image,
            { size: 90 }
        );
        let text = 'followers';

        if (count === 1) {
            text = 'follower';
        }

        return (
            <Fragment>
                <img
                    src={src}
                    srcSet={srcSet}
                    alt={artist.name}
                    className={styles.avatar}
                />
                <h3 className={styles.name}>
                    <Link to={`/artist/${artist.url_slug}`}>
                        {artist.name}{' '}
                    </Link>
                    {check}
                </h3>
                <p className={styles.followers}>
                    <span>{count.toLocaleString()}</span> {text}
                </p>
                <FollowButtonContainer artist={artist} />
            </Fragment>
        );
    }

    render() {
        const { loading, artist, x, y, active } = this.props;
        const klass = classnames(styles.container, 'u-box-shadow', {
            [styles.containerActive]: active
        });

        const style = {
            position: 'fixed',
            left: `${x}px`,
            top: `${y}px`
        };

        if (!artist && !loading) {
            return null;
        }

        return (
            <div
                className={klass}
                style={style}
                ref={(c) => {
                    this._component = c;
                }}
            >
                <div className={styles.inner}>
                    {this.renderContent(loading, artist || {})}
                </div>
            </div>
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(ArtistPopover);
