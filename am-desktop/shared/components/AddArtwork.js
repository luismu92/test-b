// - Shared between UploadAlbumsPage (new) and AlbumEditPage (future?)
// - Same logic but slightly different presentation due to design changes in new upload flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CameraIcon from '../icons/camera';
import analytics from 'utils/analytics';
import { getDynamicImageProps, previewFile } from 'utils/index';

import styles from './AddArtwork.module.scss';

export default class AddArtwork extends Component {
    static propTypes = {
        image: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        type: PropTypes.string
    };

    static defaultProps = {
        onChange() {}
    };

    handleFileInputChange = (e) => {
        const { name } = e.currentTarget;

        e.persist();

        previewFile(e.target.files[0])
            .then((value) => {
                return this.props.onChange({
                    currentTarget: {
                        name,
                        value
                    },
                    target: e.target
                });
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            });
    };

    render() {
        const { image, type } = this.props;
        const defaultThumb = '/static/images/default-song-image.png';

        const [artwork, srcSet] = getDynamicImageProps(image || defaultThumb, {
            max: true,
            size: 240
        });

        const buttonClassName = classnames(
            styles.button,
            'button',
            'button--pill',
            'button--file'
        );

        const containerClassName = classnames(styles.container, {
            [styles.album]: type === 'album',
            [styles.hasArtwork]: type === 'album' && image,
            [styles.song]: type === 'song'
        });

        return (
            <figure className={containerClassName}>
                <img
                    alt="album artwork"
                    className={styles.image}
                    src={artwork}
                    srcSet={srcSet}
                />

                <div className={buttonClassName} role="button">
                    <input
                        accept="image/*"
                        name="image"
                        onChange={this.handleFileInputChange}
                        type="file"
                    />
                    <span className={styles.icon}>
                        <CameraIcon />
                    </span>
                    Add Artwork
                </div>

                <p className={styles.copy}>
                    Minimum 500x500 size,
                    <br />
                    JPG or PNG
                </p>
            </figure>
        );
    }
}
