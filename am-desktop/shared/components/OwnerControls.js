import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { getUploader } from 'utils/index';
import { MODAL_TYPE_PROMO } from '../redux/modules/modal';

import CogIcon from '../icons/cog';

import Button from '../buttons/Button';
import PromoModal from '../modal/PromoModal';
import BodyClickListener from '../components/BodyClickListener';

export default class OwnerControls extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        music: PropTypes.object,
        modal: PropTypes.object,
        editActive: PropTypes.bool,
        onEditClick: PropTypes.func,
        onDeleteClick: PropTypes.func,
        onPublicClick: PropTypes.func,
        onPromoLinkClick: PropTypes.func
    };

    componentWillUnmount() {
        this._tooltipButton = null;
    }

    render() {
        const {
            music,
            currentUser,
            modal,
            onDeleteClick,
            onPublicClick,
            onPromoLinkClick
        } = this.props;
        const manageUrl = `/edit/${music.type}/${music.id}`;

        if (!currentUser.isLoggedIn) {
            return null;
        }

        const uploader = getUploader(music);

        if (currentUser.profile.id !== uploader.id && !currentUser.isAdmin) {
            return null;
        }

        const privacyText =
            music.private === 'yes' ? 'Make Public' : 'Make Private';

        const promoLinkButton = (
            <button title="Create Promotional Link" onClick={onPromoLinkClick}>
                Make Promo Link
            </button>
        );

        const buttonsKlass = classnames(
            'tooltip',
            'tooltip--right-arrow',
            'sub-menu',
            'sub-menu--condensed',
            'tooltip--below',
            {
                'tooltip--active': this.props.editActive
            }
        );

        const buttons = (
            <ul className={buttonsKlass} style={{ top: 'calc(100% + 8px)' }}>
                <li className="sub-menu__item">
                    <button
                        title={privacyText}
                        data-id={music.id}
                        data-private={music.private}
                        data-type={music.type}
                        onClick={onPublicClick}
                    >
                        {privacyText}
                    </button>
                </li>
                <li className="sub-menu__item">
                    <Link to={manageUrl} title="Edit">
                        Edit {music.type}
                    </Link>
                </li>
                {music.type !== 'playlist' && (
                    <li className="sub-menu__item">
                        <a
                            title="SubmitHub"
                            target="_blank"
                            href="https://www.submithub.com/apply"
                            rel="nofollow noopener"
                        >
                            Submit Hub
                        </a>
                    </li>
                )}
                <li className="sub-menu__item">
                    <button
                        onClick={onDeleteClick}
                        title="Delete"
                        data-id={music.id}
                        data-type={music.type}
                        data-artist={music.artist}
                        data-title={music.title}
                    >
                        Delete {music.type}
                    </button>
                </li>
                {music.type !== 'playlist' && (
                    <li className="sub-menu__item">{promoLinkButton}</li>
                )}
            </ul>
        );

        return (
            <div style={{ display: 'inline-block', position: 'relative' }}>
                <BodyClickListener
                    shouldListen={this.props.editActive}
                    onClick={this.props.onEditClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <Button
                    width={160}
                    height={32}
                    text="Creator Controls"
                    icon={<CogIcon />}
                    onClick={this.props.onEditClick}
                    hasChevron
                    ref={(e) => {
                        this._tooltipButton = e;
                    }}
                    style={{
                        letterSpacing: '-0.5px',
                        marginRight: 10
                    }}
                />
                {buttons}
                <PromoModal
                    onClose={this.handleModalClose}
                    active={modal.type === MODAL_TYPE_PROMO}
                    music={music}
                />
            </div>
        );
    }
}
