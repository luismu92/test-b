import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '../icons/close-thin';

import styles from './TagListInput.module.scss';

export default class TagInput extends Component {
    static propTypes = {
        initialHeight: PropTypes.number,
        inputProps: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }).isRequired,
        onTagListChange: PropTypes.func,
        onTagListResize: PropTypes.func,
        tagListAdditionalHeight: PropTypes.number,
        tags: PropTypes.array.isRequired
    };

    static defaultProps = {
        initialHeight: 42
    };

    constructor(props) {
        super(props);

        this.state = {
            tagInput: ''
        };

        this.input = React.createRef();
        this.tagList = React.createRef();
    }

    componentDidMount() {
        this.handleTagListResize();
    }

    componentDidUpdate(prevProps, prevState) {
        const { initialHeight } = this.props;
        const { tagInput } = this.state;
        const tagListAdditionalHeight =
            this.tagList.current.clientHeight - initialHeight;

        if (
            prevProps.tagListAdditionalHeight !== tagListAdditionalHeight ||
            prevState.tagInput !== tagInput
        ) {
            this.handleTagListResize();
        }
    }

    handleFocusInput = () => {
        this.input.current.focus();
    };

    handleRemoveTagClick = (e) => {
        const tag = e.currentTarget.getAttribute('data-tag');

        this.handleTagsOnChange(e, 'remove', tag);
    };

    handleTagListResize = () => {
        const { initialHeight, inputProps, onTagListResize } = this.props;

        if (!onTagListResize || !this.tagList) {
            return;
        }

        const { id, name } = inputProps;
        const tagListAdditionalHeight =
            this.tagList.current.clientHeight - initialHeight;

        onTagListResize(id, name, tagListAdditionalHeight);
    };

    handleTagsInputBlur = (e) => {
        const tag = this.state.tagInput.trim();

        if (tag) {
            this.handleTagsOnChange(e, 'add', tag);
        }

        this.setState({
            tagInput: ''
        });
    };

    handleTagsInputChange = (e) => {
        this.setState(
            {
                tagInput: e.currentTarget.value
            },
            this.handleTagsOnChange(e)
        );
    };

    handleTagsInputKeyDown = (e) => {
        const { tags } = this.props;
        const { tagInput } = this.state;

        if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();

            if (!tagInput) {
                return;
            }

            const tag = tagInput.trim();

            this.handleTagsOnChange(e, 'add', tag);

            this.setState({
                tagInput: ''
            });
        } else if (e.key === 'Backspace' && !tagInput) {
            e.preventDefault();

            if (!tags.length) {
                return;
            }

            const lastTag = tags[tags.length - 1];

            this.handleTagsOnChange(e, 'remove', lastTag);

            this.setState({
                tagInput: lastTag || ''
            });
        } else if (e.key === ' ') {
            // disallow consecutive whitespace
            const lastChar = tagInput[tagInput.length - 1];

            if (lastChar === ' ') {
                e.preventDefault();
            }
        }
    };

    handleTagsOnChange = (e, changeType, tag) => {
        const { inputProps, onTagListChange, tags } = this.props;
        const { name } = inputProps;
        let value = [...tags];

        if (tag) {
            switch (changeType) {
                case 'add': {
                    // remove duplicate tags by using set and converting back to array
                    value = [...new Set(value).add(tag)];

                    break;
                }

                case 'remove': {
                    const tagSet = new Set(value);

                    tagSet.delete(tag);
                    value = [...tagSet];

                    break;
                }

                default:
                    break;
            }
        }

        value = value.join();

        // this makes it so that we can use existing input change handlers (for songs, albums, etc.) without modifying them
        const event = {
            ...e,
            currentTarget: {
                ...e.currentTarget,
                type: 'tags',
                name,
                value
            }
        };

        onTagListChange(event);
    };

    render() {
        const { inputProps, tags } = this.props;
        const { tagInput } = this.state;

        const tagInputProps = {
            ...inputProps,
            onBlur: this.handleTagsInputBlur,
            onChange: this.handleTagsInputChange,
            onKeyDown: this.handleTagsInputKeyDown,
            type: 'text',
            value: tagInput
        };

        const tagListItems = tags.map((tag, i) => {
            const removeTagButton = (
                // @todo change to button
                /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
                <span
                    className={styles.removeTagButton}
                    onClick={this.handleRemoveTagClick}
                    data-tag={tag}
                >
                    <CloseIcon />
                </span>
            );

            return (
                <span className={styles.tagListItem} key={`${i}:${tag}`}>
                    <span>{tag}</span>
                    {removeTagButton}
                </span>
            );
        });

        return (
            /* eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
            <div
                className={styles.tagList}
                onClick={this.handleFocusInput}
                ref={this.tagList}
            >
                {tagListItems}
                <input {...tagInputProps} autoComplete="off" ref={this.input} />
            </div>
        );
    }
}
