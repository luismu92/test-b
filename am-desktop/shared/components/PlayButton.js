import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';
import PauseIcon from '../icons/pause';
import PlayIcon from '../icons/play';

import AndroidLoader from '../loaders/AndroidLoader';

export default class PlayButton extends Component {
    static propTypes = {
        size: PropTypes.number,
        onlySizeLoader: PropTypes.bool,
        onButtonClick: PropTypes.func,
        buttonProps: PropTypes.object,
        iconClassName: PropTypes.string,
        className: PropTypes.string,
        fillColor: PropTypes.string,
        title: PropTypes.string,
        paused: PropTypes.bool,
        disabled: PropTypes.bool,
        shadow: PropTypes.bool,
        loading: PropTypes.bool
    };

    static defaultProps = {
        buttonProps: {},
        onlySizeLoader: false,
        loading: false,
        disabled: false,
        paused: true,
        shadow: false,
        iconClassName: '',
        onButtonClick: null
    };

    render() {
        const {
            paused,
            onButtonClick,
            loading,
            className,
            size,
            onlySizeLoader,
            fillColor,
            buttonProps,
            shadow,
            iconClassName,
            title,
            disabled
        } = this.props;

        let action = 'pause';

        if (paused) {
            action = 'play';
        }

        const klass = classnames('play-button', {
            [className]: className,
            'play-button--playing': !paused && !loading,
            'play-button--paused': paused && !loading,
            'play-button--loading': loading
        });

        const style = {
            color: fillColor,
            borderColor: fillColor
        };

        if (!onlySizeLoader && size) {
            style.width = `${size}px`;
            style.height = `${size}px`;
        }

        const props = {
            className: klass,
            'data-action': action,
            'data-testid': 'playButton',
            disabled,
            style,
            title,
            ...buttonProps
        };

        if (onButtonClick) {
            props.onClick = onButtonClick;
        }

        const iconClass = classnames('play-button__icon', {
            'play-button__icon--shadow': shadow,
            [iconClassName]: iconClassName
        });

        return React.createElement(onButtonClick ? 'button' : 'div', props, [
            <PlayIcon
                key="play"
                title="Play"
                aria-label="Play"
                disabled={disabled}
                className={`play-icon ${iconClass}`}
                aria-hidden={!paused && !loading}
                style={style}
            />,
            <PauseIcon
                key="pause"
                title="Pause"
                aria-label="Pause"
                disabled={disabled}
                className={`pause-icon ${iconClass}`}
                aria-hidden={paused && !loading}
                style={style}
            />,
            <AndroidLoader
                key="loader"
                disabled={disabled}
                className={`loading-icon loading-icon--flex ${iconClass}`}
                aria-hidden={!loading}
                size={size}
                style={style}
            />
        ]);
    }
}
