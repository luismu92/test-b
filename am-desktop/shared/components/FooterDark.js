import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import AmLogo from '../icons/am-logo-new';
import FacebookIcon from '../icons/facebook-block-icon';
import TwitterIcon from '../icons/twitter-logo-new';
import InstagramIcon from '../icons/instagram';
import TwitchIcon from 'icons/twitch';
import AppBadge from '../components/AppBadge';

export default class FooterDark extends Component {
    static propTypes = {
        playerActive: PropTypes.bool
    };

    renderListItem({ path, title }, i) {
        let link = <Link to={path}>{title}</Link>;

        if (path.indexOf('/') !== 0) {
            link = (
                <a href={path} target="_blank" rel="nofollow noopener">
                    {title}
                </a>
            );
        }

        return <li key={i}>{link}</li>;
    }

    render() {
        const browseMenu = [
            {
                path: '/',
                title: 'Home'
            },
            {
                path: '/trending-now',
                title: 'Trending'
            },
            {
                path: '/songs/week',
                title: 'Top Songs'
            },
            {
                path: '/albums/week',
                title: 'Top Albums'
            },
            {
                path: '/playlists/browse',
                title: 'Playlists'
            },
            {
                path: '/artists/popular',
                title: 'Accounts to Follow'
            },
            {
                path: '/recent',
                title: 'Recently Added'
            }
        ].map(this.renderListItem);

        const aboutMenu = [
            {
                path: '/contact-us',
                title: 'Contact / Support'
            },
            {
                path: '/about',
                title: 'About'
            },
            {
                path: '/about/wordpress',
                title: 'Wordpress'
            },
            {
                path: '/world',
                title: 'Blog'
            }
        ].map(this.renderListItem);

        const termsMenu = [
            {
                path: '/about/legal',
                title: 'Legal & DMCA'
            },
            {
                path: '/about/privacy-policy',
                title: 'Privacy Policy'
            },
            {
                path: '/about/terms-of-service',
                title: 'Terms of Service'
            },
            {
                path: `${process.env.AM_URL}/audiomack-2.0-media-kit.zip`,
                title: 'Press Kit'
            },
            {
                path: 'https://styleguide.audiomack.com/',
                title: 'Style Guide'
            }
        ].map(this.renderListItem);

        const klass = classnames('create-footer', {
            'create-footer--player-active': this.props.playerActive
        });

        return (
            <footer className={klass}>
                <div className="create-footer__main">
                    <div className="row">
                        <div className="create-footer__widget u-spacing-bottom column small-24 medium-12 large-6">
                            <span className="create-footer__logo u-text-white u-d-inline-block">
                                <AmLogo />
                            </span>
                            <p className="create-footer__copyright">
                                Copyright &copy; {new Date().getFullYear()}
                            </p>
                        </div>
                        <div className="create-footer__widget u-spacing-bottom column small-24 medium-12 large-6">
                            <h4 className="create-footer__widget-title u-lh-12 u-text-white u-spacing-bottom-em">
                                Browse
                            </h4>
                            <ul>{browseMenu}</ul>
                        </div>
                        <div className="create-footer__widget u-spacing-bottom column small-24 medium-12 large-6">
                            <h4 className="create-footer__widget-title u-lh-12 u-text-white u-spacing-bottom-em">
                                About
                            </h4>
                            <ul>{aboutMenu}</ul>
                        </div>
                        <div className="create-footer__widget u-spacing-bottom column small-24 medium-12 large-6">
                            <h4 className="create-footer__widget-title u-lh-12 u-text-white u-spacing-bottom-em">
                                Terms of Service
                            </h4>
                            <ul>{termsMenu}</ul>
                        </div>
                    </div>
                </div>
                <div className="create-footer__sub">
                    <div className="row">
                        <div className="create-footer__follow-wrap column small-24 medium-12">
                            <ul className="create-footer__follow u-inline-list">
                                <li className="create-footer__follow-label u-text-white u-fw-700">
                                    Follow Us:
                                </li>
                                <li>
                                    <a
                                        href="https://www.facebook.com/audiomack"
                                        target="_blank"
                                        rel="nofollow noopener"
                                    >
                                        <FacebookIcon />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://twitter.com/audiomack"
                                        target="_blank"
                                        rel="nofollow noopener"
                                    >
                                        <TwitterIcon />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://www.instagram.com/audiomack"
                                        target="_blank"
                                        rel="nofollow noopener"
                                    >
                                        <InstagramIcon />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://twitch.tv/audiomack"
                                        target="_blank"
                                        rel="nofollow noopener"
                                    >
                                        <TwitchIcon />
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="create-footer__apps column small-24 medium-12">
                            <div className="create-footer__badges">
                                <AppBadge
                                    type="ios"
                                    style="svg"
                                    className="create-feature__badge u-d-inline-block"
                                />
                                <AppBadge
                                    type="android"
                                    style="svg"
                                    className="create-feature__badge u-d-inline-block"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
