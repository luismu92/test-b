import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Table extends Component {
    static propTypes = {
        headerCells: PropTypes.array.isRequired,
        bodyRows: PropTypes.array.isRequired,
        ignoreTbody: PropTypes.bool,
        className: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            sortedColumnIndex: null,
            sortComparator: null,
            sortDirection: 'asc'
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleHeaderClick = (e) => {
        const index = parseInt(e.currentTarget.getAttribute('data-index'), 10);

        this.setState({
            sortComparator: this.props.headerCells[index].comparator,
            sortedColumnIndex: index,
            sortDirection: this.state.sortDirection === 'asc' ? 'desc' : 'asc'
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderHeader() {
        const { headerCells } = this.props;
        const cells = headerCells.map((cell, i) => {
            const text = cell.value || cell;
            const props = cell.props || {};
            const isSortable = !!cell.comparator;
            const klass = classnames('table-header', {
                [props.className]: props.className,
                'table-header--sortable': isSortable
            });
            const onClick = isSortable ? this.handleHeaderClick : null;
            const arrowClass = classnames('table-sort-arrow', {
                'table-sort-arrow--ascending':
                    this.state.sortedColumnIndex === i &&
                    this.state.sortDirection === 'asc'
            });

            return (
                <th
                    key={i}
                    data-index={i}
                    onClick={onClick}
                    {...props}
                    className={klass}
                >
                    {text}
                    {isSortable ? <span className={arrowClass} /> : null}
                </th>
            );
        });

        return (
            <thead>
                <tr>{cells}</tr>
            </thead>
        );
    }

    renderBody() {
        const { headerCells, bodyRows, ignoreTbody = false } = this.props;
        let rows = bodyRows;

        if (typeof this.state.sortComparator === 'function') {
            rows = Array.from(rows).sort((x, y) => {
                return this.state.sortComparator(
                    this.state.sortDirection,
                    x.model,
                    y.model
                );
            });
        }

        rows = rows.map((row, i) => {
            if (!row) {
                return null;
            }

            if (React.isValidElement(row)) {
                return row;
            }

            if (React.isValidElement(row.value)) {
                return row.value;
            }

            const cells = row.map((cell, k) => {
                return (
                    <td data-th={headerCells[k]} key={`${i}${k}`}>
                        {cell}
                    </td>
                );
            });

            return <tr key={i}>{cells}</tr>;
        });

        if (ignoreTbody) {
            return rows;
        }

        return <tbody>{rows}</tbody>;
    }

    render() {
        const { className, ignoreTbody } = this.props;
        const klass = classnames('table', {
            'table--singlebody': !ignoreTbody,
            'table--multibody': ignoreTbody,
            [className]: className
        });

        return (
            <table className={klass}>
                {this.renderHeader()}
                {this.renderBody()}
            </table>
        );
    }
}
