import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

export default class Countdown extends Component {
    static propTypes = {
        seconds: PropTypes.number.isRequired,
        type: PropTypes.string.isRequired,
        onFinish: PropTypes.func,
        className: PropTypes.string,
        containerElement: PropTypes.string
    };

    static defaultProps = {
        onFinish() {},
        containerElement: 'p'
    };

    constructor(props) {
        super(props);

        this.state = {
            totalSecondsLeft: this.props.seconds,
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        };
    }

    componentDidMount() {
        const { onFinish } = this.props;

        const refreshCountdown = () => {
            let totalSecondsLeft = this.state.totalSecondsLeft;

            totalSecondsLeft--;

            if (totalSecondsLeft <= 0) {
                onFinish();
                return;
            }

            let secondsLeft = totalSecondsLeft;
            const days = parseInt(secondsLeft / 86400, 10);

            secondsLeft = secondsLeft % 86400;
            const hours = parseInt(secondsLeft / 3600, 10);

            secondsLeft = secondsLeft % 3600;
            const minutes = parseInt(secondsLeft / 60, 10);

            const seconds = parseInt(secondsLeft % 60, 10);

            this.setState({
                totalSecondsLeft: totalSecondsLeft,
                days: days,
                hours: hours,
                minutes: minutes,
                seconds: seconds
            });
            this._refreshTimer = setTimeout(refreshCountdown, 1000);
        };

        refreshCountdown();
    }

    componentWillUnmount() {
        clearTimeout(this._refreshTimer);
        this._refreshTimer = null;
    }

    render() {
        const { className, containerElement } = this.props;
        const { days, hours, minutes, seconds } = this.state;

        const containerProps = {
            className: className
        };

        const inner = (
            <Fragment>
                This {this.props.type} will be released in{' '}
                <strong>
                    {days} days, {hours} hours, {minutes} minutes
                </strong>{' '}
                and <strong>{seconds} seconds</strong>
            </Fragment>
        );

        return React.createElement(containerElement, containerProps, inner);
    }
}
