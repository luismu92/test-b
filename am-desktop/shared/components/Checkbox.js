import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Checkbox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isChecked: props.isChecked
        };
    }

    handleCheckboxChange = () => {
        const { onCheckboxChange, label } = this.props;

        this.setState(
            (prevState) => {
                return { isChecked: !prevState.isChecked };
            },
            () => {
                onCheckboxChange(label, this.state.isChecked);
            }
        );
    };

    render() {
        const { label } = this.props;
        const { isChecked } = this.state;

        return (
            <label>
                <input
                    type="checkbox"
                    value={label}
                    checked={isChecked}
                    onChange={this.handleCheckboxChange}
                />

                {label}
            </label>
        );
    }
}

Checkbox.propTypes = {
    label: PropTypes.string.isRequired,
    onCheckboxChange: PropTypes.func,
    isChecked: PropTypes.bool
};

export default Checkbox;
