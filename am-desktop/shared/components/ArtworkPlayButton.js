import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PlayButton from './PlayButton';

export default class ArtworkPlayButton extends Component {
    static propTypes = {
        image: PropTypes.string.isRequired,
        fullImage: PropTypes.string,
        paused: PropTypes.bool,
        loading: PropTypes.bool,
        iconSize: PropTypes.number,
        buttonProps: PropTypes.object,
        onButtonClick: PropTypes.func,
        className: PropTypes.string,
        playButton: PropTypes.bool,
        ranking: PropTypes.number
    };

    static defaultProps = {
        buttonProps: {},
        onButtonClick() {},
        playButton: true,
        ranking: null
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const {
            image,
            fullImage,
            className,
            buttonProps,
            onButtonClick,
            loading,
            paused,
            ranking,
            iconSize,
            playButton: showPlayButton
        } = this.props;

        const klass = classnames('artwork-play-button', {
            [className]: className
        });

        let rankingNumber;

        if (typeof ranking === 'number') {
            rankingNumber = (
                <span className="artwork-play-button__rank" role="presentation">
                    {ranking}
                </span>
            );
        }

        let playButton;

        if (showPlayButton) {
            playButton = (
                <div className="music-artwork__overlay u-text-center">
                    <PlayButton
                        size={iconSize}
                        loading={loading}
                        paused={paused}
                        shadow
                    />
                </div>
            );
        }

        const imgProps = {};

        if (fullImage) {
            imgProps.srcSet = `${fullImage} 2x`;
        }

        return (
            <button
                className={klass}
                onClick={onButtonClick}
                disabled={!showPlayButton}
                {...buttonProps}
            >
                <img data-album-artwork src={image} alt="" {...imgProps} />
                {playButton}
                {rankingNumber}
            </button>
        );
    }
}
