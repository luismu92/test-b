/* global it, expect */

import React from 'react';
import renderer from 'react-test-renderer';

import AppBadge from '../AppBadge';

it('should render ios image', () => {
    const tree = renderer
        .create(<AppBadge type="ios" style="image" />)
        .toJSON();

    expect(tree).toMatchSnapshot();
});

it('should render ios svg', () => {
    const tree = renderer.create(<AppBadge type="ios" style="svg" />).toJSON();

    expect(tree).toMatchSnapshot();
});

it('should render android image', () => {
    const tree = renderer
        .create(<AppBadge type="android" style="image" />)
        .toJSON();

    expect(tree).toMatchSnapshot();
});

it('should render android svg', () => {
    const tree = renderer
        .create(<AppBadge type="android" style="svg" />)
        .toJSON();

    expect(tree).toMatchSnapshot();
});

it('should render tmta image', () => {
    const tree = renderer.create(<AppBadge type="sms" />).toJSON();

    expect(tree).toMatchSnapshot();
});
