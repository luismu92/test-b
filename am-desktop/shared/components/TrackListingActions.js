import React, { Component, Fragment } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import Transition from 'react-transition-group/Transition';

import { getUploader } from 'utils/index';

import SoundcloudIcon from '../icons/soundcloud-icon';
import PlusIcon from '../icons/plus';
import RetweetIcon from '../icons/retweet';
import QueueIcon from '../icons/queue';
import QueueEndIcon from '../icons/queue-end';
import QueueNextIcon from '../icons/queue-next';
import CloseIcon from '../icons/close-thin';
import HeartIcon from '../../../am-shared/icons/heart';
import ExternalIcon from '../icons/external';
import MusicIcon from 'icons/music';
import CartIcon from '../icons/shopping-cart';
import ThreeDots from '../icons/three-dots-alt';

import BodyClickListener from '../components/BodyClickListener';

export default class TrackListingActions extends Component {
    static propTypes = {
        track: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        index: PropTypes.number.isRequired,
        lightTheme: PropTypes.bool,
        hideQueueAddActions: PropTypes.bool,
        hideQueueLastAction: PropTypes.bool,
        hideQueueRemoveAction: PropTypes.bool,
        musicItem: PropTypes.object,
        onTrackActionClick: PropTypes.func,
        isEmbed: PropTypes.bool
    };

    static defaultProps = {
        lightTheme: false,
        hideQueueRemoveAction: false,
        hideQueueAddActions: false,
        hideQueueLastAction: false,
        onTrackActionClick() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            tooltipActive: false,
            mounted: false
        };
    }

    componentDidMount() {
        // eslint-disable-next-line
        this.setState({
            mounted: true
        });

        this._tooltipContainer = document.getElementById(
            'tracklistTooltipContainer'
        );

        if (!this._tooltipContainer) {
            this._tooltipContainer = document.createElement('div');
            this._tooltipContainer.id = 'tracklistTooltipContainer';
            this._tooltipContainer.className = 'tooltip-container';
            document.body.appendChild(this._tooltipContainer);
        }
    }

    componentWillUnmount() {
        this._tooltipButton = null;
        this._tooltipContainer = null;
    }

    handleTooltipClick = (e) => {
        const button = e.currentTarget;
        const rect = button.getBoundingClientRect();
        const buttonWidth = button.clientWidth;
        const triangleDistance = 26;

        button.blur();

        const newActiveState = !this.state.tooltipActive;
        const newState = {
            tooltipActive: newActiveState
        };

        if (newActiveState) {
            newState.tooltipRight =
                window.innerWidth -
                rect.left -
                buttonWidth / 2 -
                triangleDistance;
            newState.tooltipBottom = window.innerHeight - rect.top;
        }

        this.setState(newState);
    };

    render() {
        const {
            onTrackActionClick,
            track,
            index,
            currentUser,
            musicItem,
            isEmbed
        } = this.props;
        const trackId = track.song_id || track.id;
        const hasFavorite =
            currentUser.isLoggedIn &&
            currentUser.profile.favorite_music &&
            currentUser.profile.favorite_music.indexOf(trackId) !== -1;
        const favText = hasFavorite ? 'Remove Favorite' : 'Favorite';
        const hasReup =
            currentUser.profile &&
            currentUser.profile.reups &&
            currentUser.profile.reups.indexOf(trackId) !== -1;
        const reupText = hasReup ? 'Remove Re-Up' : 'Re-Up';
        const isAlbum = musicItem.type === 'album';
        const isPlaylist = musicItem.type === 'playlist';
        const uploader = isPlaylist
            ? getUploader(track)
            : getUploader(musicItem.parentDetails || musicItem);

        let externalLink;

        if (isAlbum || isPlaylist) {
            const type = track.type || 'song';

            externalLink = (
                <li className="sub-menu__item">
                    <Link
                        title="View Song"
                        aria-label="View song"
                        to={`/${type}/${uploader.url_slug}/${track.url_slug}`}
                    >
                        <MusicIcon className="sub-menu__icon" />
                        View Song
                    </Link>
                </li>
            );
        }

        if ((isEmbed && isAlbum) || isPlaylist) {
            externalLink = (
                <a
                    href={`/song/${uploader.url_slug}/${track.url_slug}`}
                    target="_blank"
                    rel="nofollow noindex"
                >
                    <ExternalIcon className="sub-menu__icon sub-menu__icon--external" />
                    View Song
                </a>
            );
        }

        let reupButton;

        if (
            !currentUser.isLoggedIn ||
            (currentUser.isLoggedIn && currentUser.profile.id !== uploader.id)
        ) {
            reupButton = (
                <li className="sub-menu__item">
                    <button
                        data-track-index={index}
                        data-track-action="reup"
                        onClick={onTrackActionClick}
                    >
                        <RetweetIcon className="sub-menu__icon" />
                        {reupText}
                    </button>
                </li>
            );
        }

        let buyButton;

        if (track.buy_link) {
            buyButton = (
                <li className="sub-menu__item">
                    <a
                        title="Buy now"
                        aria-label="Buy now"
                        href={track.buy_link}
                        target="_blank"
                        rel="nofollow noopener"
                        onClick={onTrackActionClick}
                    >
                        <CartIcon className="sub-menu__icon" />
                        Buy Song
                    </a>
                </li>
            );
        }

        let queueAddActions;

        if (!this.props.hideQueueAddActions) {
            let atEnd;
            let nextText = 'Add to Queue';
            let nextIcon = <QueueIcon className="sub-menu__icon" />;

            if (!this.props.hideQueueLastAction) {
                nextText = 'Queue Next';
                nextIcon = <QueueNextIcon className="sub-menu__icon" />;

                atEnd = (
                    <li className="sub-menu__item">
                        <button
                            data-track-index={index}
                            data-track-action="add-to-queue-end"
                            onClick={onTrackActionClick}
                        >
                            <QueueEndIcon className="sub-menu__icon sub-menu__icon--queue-end" />
                            Queue End
                        </button>
                    </li>
                );
            }

            queueAddActions = (
                <Fragment>
                    <li className="sub-menu__item">
                        <button
                            data-track-index={index}
                            data-track-action="add-to-queue-next"
                            onClick={onTrackActionClick}
                        >
                            {nextIcon}
                            {nextText}
                        </button>
                    </li>
                    {atEnd}
                </Fragment>
            );
        }

        let removeFromQueue;

        if (!this.props.hideQueueRemoveAction) {
            removeFromQueue = (
                <li className="sub-menu__item">
                    <button
                        data-track-index={index}
                        data-track-action="remove-track"
                        onClick={onTrackActionClick}
                    >
                        <CloseIcon className="sub-menu__icon sub-menu__icon--close" />
                        Remove from queue
                    </button>
                </li>
            );
        }

        let soundcloud;

        if (track.is_soundcloud) {
            soundcloud = (
                <li className="sub-menu__item">
                    <a
                        href={track.sourceUrl || track.streaming_url}
                        target="_blank"
                        rel="nofollow noopener"
                        data-track-index={index}
                        data-track-action="soundcloud"
                        aria-label="Listen on Soundcloud"
                        title="Listen on Soundcloud"
                        onClick={onTrackActionClick}
                    >
                        <SoundcloudIcon className="sub-menu__icon" />
                        Soundcloud
                    </a>
                </li>
            );
        }

        const containerClass = classnames(
            'tracklist__track-interactions u-d-flex u-d-flex--align-center'
        );

        return (
            <ul className={containerClass}>
                <BodyClickListener
                    shouldListen={this.state.tooltipActive}
                    onClick={this.handleTooltipClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <li className="u-pos-relative">
                    <button
                        className="tracklist__track-interaction-icon tracklist__track-interaction-icon--dots u-d-block"
                        aria-label="Show more actions"
                        onClick={this.handleTooltipClick}
                        ref={(e) => {
                            this._tooltipButton = e;
                        }}
                    >
                        <ThreeDots />
                    </button>
                    {this.state.mounted &&
                        createPortal(
                            <Transition
                                in={this.state.tooltipActive}
                                timeout={120}
                                unmountOnExit
                            >
                                {(state) => {
                                    const tooltipClass = classnames(
                                        'tooltip tooltip--down-right-arrow tooltip--shadow sub-menu',
                                        {
                                            'tooltip--light': this.props
                                                .lightTheme,
                                            'tooltip--active':
                                                state === 'entered'
                                        }
                                    );

                                    return (
                                        <ul
                                            className={tooltipClass}
                                            style={{
                                                position: 'fixed',
                                                bottom: this.state
                                                    .tooltipBottom,
                                                right: this.state.tooltipRight,
                                                zIndex: 99,
                                                minWidth: 0,
                                                maxWidth: 300
                                            }}
                                        >
                                            <li className="sub-menu__item">
                                                <button
                                                    data-track-index={index}
                                                    data-track-action="favorite"
                                                    onClick={onTrackActionClick}
                                                >
                                                    <HeartIcon className="sub-menu__icon sub-menu__icon--fav" />
                                                    {favText}
                                                </button>
                                            </li>
                                            <li className="sub-menu__item">
                                                <button
                                                    data-track-index={index}
                                                    data-track-action="playlist"
                                                    onClick={onTrackActionClick}
                                                >
                                                    <PlusIcon className="sub-menu__icon sub-menu__icon--plus" />
                                                    Add to Playlist
                                                </button>
                                            </li>
                                            {reupButton}
                                            {queueAddActions}
                                            {removeFromQueue}
                                            {buyButton}
                                            {externalLink}
                                            {soundcloud}
                                        </ul>
                                    );
                                }}
                            </Transition>,
                            this._tooltipContainer
                        )}
                </li>
            </ul>
        );
    }
}
