import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import analytics from 'utils/analytics';

import { follow, unfollow, queueAction } from '../redux/modules/user/index';
import { showModal, hideModal, MODAL_TYPE_AUTH } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';

import FollowButton from 'buttons/FollowButton';

class FollowButtonContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        artist: PropTypes.object.isRequired,
        showCount: PropTypes.bool
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            loading: false
        };
    }

    componentDidUpdate(prevProps) {
        // Follow or unfollow happened
        if (
            prevProps.currentUser.profile &&
            prevProps.currentUser.profile.following &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.following &&
            prevProps.currentUser.profile.following.length !==
                this.props.currentUser.profile.following.length
        ) {
            // eslint-disable-next-line
            this.setState({
                loading: false
            });
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFollowClick = (shouldFollow, artist) => {
        const { dispatch, currentUser } = this.props;

        const queuedAction = () => {
            this.setState({
                loading: true
            });

            dispatch(hideModal());

            if (shouldFollow) {
                dispatch(follow(artist)).catch((error) => {
                    this.setState({
                        loading: false
                    });
                    dispatch(
                        addToast({
                            action: 'message',
                            item: error.message
                        })
                    );
                    analytics.error(error);
                    return;
                });
                return;
            }

            dispatch(unfollow(artist));
        };

        if (!currentUser.profile) {
            dispatch(queueAction(queuedAction));
            this.openLogin();
            return;
        }

        queuedAction();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    openLogin() {
        const { dispatch } = this.props;
        const value = 'login';

        dispatch(showModal(MODAL_TYPE_AUTH, { type: value }));
    }

    render() {
        const { artist, currentUser } = this.props;

        return (
            <FollowButton
                currentUser={currentUser}
                onClick={this.handleFollowClick}
                artist={artist}
                loading={this.state.loading}
                showCount={this.props.showCount}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default withRouter(connect(mapStateToProps)(FollowButtonContainer));
