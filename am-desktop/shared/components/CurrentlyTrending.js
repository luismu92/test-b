import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import AndroidLoader from '../loaders/AndroidLoader';

export default class CurrentlyTrending extends Component {
    static propTypes = {
        trendingList: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired
    };

    renderTopUploads(uploads = [], loading) {
        if (loading) {
            return (
                <div className="u-text-center">
                    <AndroidLoader />
                </div>
            );
        }

        if (!uploads.length) {
            return (
                <p>There was a problem retrieving the top Trending Songs.</p>
            );
        }

        const list = uploads.slice(0, 5).map((upload, i) => {
            if (upload.type === 'song' || upload.type === 'album') {
                return (
                    <div
                        className="list-item list-item--small"
                        key={i}
                        role="listitem"
                    >
                        <div className="list-item__inner">
                            <div className="list-item__image">
                                <Link
                                    to={`/${upload.type}/${
                                        upload.uploader.url_slug
                                    }/${upload.url_slug}`}
                                >
                                    <img
                                        src={upload.image}
                                        alt={upload.title}
                                    />
                                </Link>
                            </div>
                            <div className="list-item__details">
                                <Link
                                    className="list-item__details-link"
                                    to={`/${upload.type}/${
                                        upload.uploader.url_slug
                                    }/${upload.url_slug}`}
                                >
                                    <p className="list-item__details-artist">
                                        <strong>{upload.artist}</strong>
                                    </p>
                                    <p className="list-item__details-meta">
                                        {upload.title}
                                    </p>
                                </Link>
                            </div>
                        </div>
                    </div>
                );
            }

            console.warn(upload.type, 'upload');
            return null;
        });

        return (
            <div className="list" role="list">
                {list}
            </div>
        );
    }

    render() {
        const { trendingList, loading } = this.props;

        return (
            <div>
                <p className="music-page__tracks-header">
                    Top Music Currently Trending
                </p>
                <div className="list">
                    {this.renderTopUploads(trendingList, loading)}
                </div>
                <div className="music-page__tracks-footer">
                    <Link to="/trending-now">More of our trending music</Link>
                </div>
            </div>
        );
    }
}
