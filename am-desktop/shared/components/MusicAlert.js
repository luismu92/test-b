import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { yesBool } from 'utils/index';

import Countdown from './Countdown';

import CalendarIcon from 'icons/calendar';
import PadlockIcon from 'icons/padlock';

import styles from './MusicAlert.module.scss';

export default class MusicAlert extends Component {
    static propTypes = {
        item: PropTypes.object,
        message: PropTypes.string,
        icon: PropTypes.element,
        onActionClick: PropTypes.func
    };

    renderMessageIcon(item) {
        const { icon } = this.props;
        const isScheduled = item.released_offset > 0;

        let messageIcon;

        if (isScheduled) {
            messageIcon = <CalendarIcon />;
        }

        if (yesBool(item.private)) {
            messageIcon = <PadlockIcon />;
        }

        if (icon) {
            messageIcon = icon;
        }

        if (!messageIcon) {
            return null;
        }

        return <span className={styles.icon}>{messageIcon}</span>;
    }

    renderMessage(item) {
        const { message } = this.props;

        let alertMessage = message;

        // Message prop takes precedence if it exists
        if (!message && yesBool(item.private)) {
            alertMessage = <strong>This {item.type} is set to private</strong>;
        }

        if (!message && item.released_offset > 0) {
            alertMessage = (
                <Countdown
                    seconds={item.released_offset}
                    type={item.type}
                    inline
                    containerElement="span"
                />
            );
        }

        if (!alertMessage) {
            return null;
        }

        return (
            <div className={styles.messageWrap}>
                {this.renderMessageIcon(item)}
                <p className={styles.message}>{alertMessage}</p>
            </div>
        );
    }

    renderButtons(item) {
        let buttons;

        if (yesBool(item.private)) {
            buttons = (
                <Fragment>
                    <button
                        className="button button--link u-spacing-right-10"
                        data-action="promo-link"
                        onClick={this.props.onActionClick}
                        style={{
                            fontWeight: 600
                        }}
                    >
                        Create Private Link
                    </button>
                    <button
                        className="button button--pill"
                        data-id={item.id}
                        data-private={item.private}
                        data-type={item.type}
                        data-action="make-public"
                        onClick={this.props.onActionClick}
                    >
                        Make Public
                    </button>
                </Fragment>
            );
        }

        if (item.released_offset > 0) {
            buttons = (
                <Fragment>
                    <button
                        className="button button--link u-spacing-right-10"
                        data-action="change-release"
                        data-release="new-date"
                        onClick={this.props.onActionClick}
                        style={{
                            fontWeight: 600
                        }}
                    >
                        Change release date
                    </button>
                    <button
                        className="button button--pill"
                        data-action="change-release"
                        data-release="now"
                        onClick={this.props.onActionClick}
                    >
                        Release Now
                    </button>
                </Fragment>
            );
        }

        if (!buttons) {
            return null;
        }

        return <div className={styles.buttons}>{buttons}</div>;
    }

    render() {
        const { item } = this.props;

        const shouldShow = yesBool(item.private) || item.released_offset > 0;

        if (!shouldShow) {
            return null;
        }

        return (
            <div className={styles.alert}>
                {this.renderMessage(item)}
                {this.renderButtons(item)}
            </div>
        );
    }
}
