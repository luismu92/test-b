import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classnames from 'classnames';

import styles from './FixedActionBar.module.scss';

class FixedActionBarContainer extends Component {
    static propTypes = {
        player: PropTypes.object,
        children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
        aboveAll: PropTypes.bool
    };

    render() {
        const { player, aboveAll } = this.props;
        const klass = classnames(styles.container, {
            [styles.abovePlayer]: player.currentSong,
            [styles.aboveAll]: aboveAll
        });

        return <footer className={klass}>{this.props.children}</footer>;
    }
}

function mapStateToProps(state) {
    return {
        player: state.player
    };
}

export default compose(connect(mapStateToProps))(FixedActionBarContainer);
