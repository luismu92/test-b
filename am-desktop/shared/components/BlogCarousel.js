import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { buildDynamicImage } from 'utils/index';
import { getBlogPostUrl } from 'utils/blog';

import FeedBar from '../widgets/FeedBar';
import Carousel from '../components/Carousel';

import AmWorld from '../icons/am-world';

import styles from './BlogCarousel.module.scss';

export default class BlogCarousel extends Component {
    static propTypes = {
        featured: PropTypes.object,
        className: PropTypes.string
    };

    render() {
        const { featured, className } = this.props;
        const klass = classnames('blog-carousel', {
            [className]: className
        });
        const feedbarTitle = (
            <h2 className="feed-bar__title">
                <AmWorld className="feed-bar__title-icon feed-bar__title-icon--medium u-text-icon" />
                <strong>Audiomack World</strong>
            </h2>
        );
        const items = featured.list.slice(0, 8).map((item, i) => {
            const { title } = item;
            const excerpt = (item.custom_excerpt || item.excerpt).substr(
                0,
                165
            );
            const imageSize = 250;
            const artwork = buildDynamicImage(item.feature_image, {
                width: imageSize,
                max: true
            });

            const retinaArtwork = buildDynamicImage(item.feature_image, {
                width: imageSize * 2,
                max: true
            });
            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            return (
                <Link
                    to={getBlogPostUrl(item)}
                    className={styles.post}
                    key={i}
                    aria-label={title}
                >
                    <div className={styles.image}>
                        <img src={artwork} srcSet={srcSet} alt="" />
                    </div>
                    <div className={styles.details}>
                        <h4 className={styles.title}>{title}</h4>
                        <p className={styles.content}>{excerpt}...</p>
                    </div>
                </Link>
            );
        });

        return (
            <section className={klass}>
                <div className="row">
                    <div className="column small-24">
                        <FeedBar title={feedbarTitle} padded blog />
                    </div>
                </div>
                <ul className="row u-spacing-top">
                    <Carousel
                        className="column small-24"
                        itemWidth={253}
                        items={items}
                    />
                </ul>
                <div className="row">
                    <div className="column small-24 u-text-center homepage__section-button-group u-spacing-top">
                        <Link
                            className="button button--pill button--med"
                            to="/world"
                        >
                            Read more from the blog
                        </Link>
                    </div>
                </div>
            </section>
        );
    }
}
