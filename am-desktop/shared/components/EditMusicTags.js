import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import fuzzysearch from 'fuzzysearch';

import AndroidLoader from 'components/loaders/AndroidLoader';

import {
    getGlobalMusicTags,
    getMusicTags,
    deleteMusicTags,
    addMusicTags
} from '../redux/modules/admin';

const defaultState = {
    filter: '',
    musicTagsLoading: [],
    musicTags: [],
    musicTagsError: null
};

class EditMusicTags extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        musicItem: PropTypes.object,
        tags: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = defaultState;
    }

    componentDidMount() {
        this.fetchTags();
    }

    componentWillUnmount() {
        this.resetLocalState();
    }

    ////////////////////
    // Event handelrs //
    ////////////////////

    handleTagCheck = (e) => {
        const checkbox = e.currentTarget;
        const checked = checkbox.checked;
        const value = checkbox.value;
        const { musicItem, dispatch } = this.props;

        this.setState((prevState) => {
            return {
                ...prevState,
                musicTagsLoading: prevState.musicTagsLoading.concat(value)
            };
        });

        if (checked) {
            dispatch(addMusicTags(musicItem, value))
                .then((action) => {
                    const success = action.resolved
                        .filter((tag) => {
                            return tag.success;
                        })
                        .map((tag) => {
                            return tag.normalizedKey;
                        });

                    this.setState((prevState) => {
                        return {
                            ...prevState,
                            musicTags: prevState.musicTags.concat(success)
                        };
                    });
                    return;
                })
                .finally(() => {
                    this.setState((prevState) => {
                        return {
                            ...prevState,
                            musicTagsLoading: prevState.musicTagsLoading.filter(
                                (tag) => {
                                    return tag !== value;
                                }
                            )
                        };
                    });
                });
            return;
        }

        dispatch(deleteMusicTags(musicItem, value))
            .then((action) => {
                const success = action.resolved
                    .filter((tag) => {
                        return tag.success;
                    })
                    .map((tag) => {
                        return tag.normalizedKey;
                    });

                this.setState((prevState) => {
                    return {
                        ...prevState,
                        musicTags: prevState.musicTags.filter((tag) => {
                            return !success.includes(tag);
                        })
                    };
                });
                return;
            })
            .finally(() => {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        musicTagsLoading: prevState.musicTagsLoading.filter(
                            (tag) => {
                                return tag !== value;
                            }
                        )
                    };
                });
            });
    };

    handleTagFilterInput = (e) => {
        this.setState({
            filter: e.currentTarget.value
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    fetchTags() {
        const { dispatch, musicItem } = this.props;

        dispatch(getGlobalMusicTags());
        dispatch(getMusicTags(musicItem))
            .then((action) => {
                this.setState({
                    musicTags: action.resolved.map((tag) => tag.normalizedKey)
                });
                return;
            })
            .catch((err) => {
                this.setState({
                    musicTagsError: err
                });
            });
    }

    resetLocalState() {
        this.setState(defaultState);
    }

    renderAvailableTagList(globalTags) {
        if (!globalTags.loading && !globalTags.list.length) {
            return <p>There are currently no tags set.</p>;
        }

        if (this.state.musicTagsError) {
            return (
                <p>
                    There was an error getting the current tags for this item.
                </p>
            );
        }

        const style = {
            maxHeight: 200,
            overflowY: 'scroll'
        };

        const currentTags = this.state.musicTags;
        let filteredTags = globalTags.list;

        if (this.state.filter.trim()) {
            filteredTags = filteredTags.filter((tag) => {
                return fuzzysearch(
                    this.state.filter.trim().toLowerCase(),
                    tag.normalizedKey
                );
            });
        }

        const items = filteredTags.map((tag, i) => {
            const loading = this.state.musicTagsLoading.includes(
                tag.normalizedKey
            );
            const liStyle = {};

            if (loading) {
                liStyle.opacity = 0.5;
            }

            return (
                <li key={`${i}-${tag.normalizedKey}`} style={liStyle}>
                    <label>
                        <input
                            type="checkbox"
                            checked={currentTags.includes(tag.normalizedKey)}
                            onChange={this.handleTagCheck}
                            value={tag.normalizedKey}
                            disabled={loading}
                        />
                        {tag.display}
                    </label>
                </li>
            );
        });

        let loader;

        if (globalTags.loading) {
            loader = <AndroidLoader size={30} />;
        }

        return (
            <ul style={style}>
                {items}
                {loader}
            </ul>
        );
    }

    render() {
        const { tags } = this.props;

        return (
            <div className="row">
                <div className="column small-24 u-spacing-bottom">
                    <h3 className="u-fs-16 u-fw-700 u-ls-n-025 u-spacing-bottom-30">
                        Search Tags
                    </h3>
                    {this.renderAvailableTagList(tags)}
                </div>
                <div className="column small-24">
                    <label>
                        Filter:
                        <input
                            type="text"
                            maxLength="100"
                            className="u-spacing-top-5"
                            onChange={this.handleTagFilterInput}
                        />
                    </label>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        musicItem: state.modal.extraData.musicItem,
        tags: state.admin.tags
    };
}

export default compose(connect(mapStateToProps))(EditMusicTags);
