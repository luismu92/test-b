import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import { getBlogPosts } from '../redux/modules/blogPost';

import BlogPosts from './BlogPosts';

class BlogPostsContainer extends Component {
    static propTypes = {
        blogPost: PropTypes.object,
        dispatch: PropTypes.func
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { blogPost } = this.props;

        return <BlogPosts blogPost={blogPost} />;
    }
}

function mapStateToProps(state) {
    return {
        blogPost: state.blogPost
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(connectDataFetchers(BlogPostsContainer, [() => getBlogPosts()]));
