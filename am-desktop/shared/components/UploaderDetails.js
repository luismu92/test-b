import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Avatar from './Avatar';
import FollowButtonContainer from './FollowButtonContainer';
import SocialLinks from './SocialLinks';
import Verified from '../../../am-shared/components/Verified';

class UploaderDetails extends Component {
    static propTypes = {
        uploader: PropTypes.object.isRequired,
        className: PropTypes.string
    };

    renderArtistStats(artist) {
        if (!artist.created) {
            return null;
        }

        const plays = (artist.stats || {})['plays-raw'] || 0;

        let playStat;

        if (plays) {
            playStat = (
                <li className="music-uploader__stat u-ls-n-05">
                    Plays:{' '}
                    <span className="u-text-orange">
                        {plays.toLocaleString()}
                    </span>
                </li>
            );
        }

        return (
            <ul className="music-uploader__stats u-inline-list u-fw-600">
                <li className="music-uploader__stat u-ls-n-05">
                    Followers:{' '}
                    <span className="u-text-orange">
                        {(artist.followers_count || 0).toLocaleString()}
                    </span>
                </li>
                <li className="music-uploader__stat u-ls-n-05">
                    Uploads:{' '}
                    <span className="u-text-orange u-d-inline-block">
                        <Link to={`/artist/${artist.url_slug}/uploads`}>
                            {(artist.upload_count || 0).toLocaleString()}
                        </Link>
                    </span>
                </li>
                {playStat}
            </ul>
        );
    }

    render() {
        const { uploader, className } = this.props;

        if (uploader.loading || !uploader.profile) {
            return null;
        }

        const check = <Verified user={uploader.profile} small />;

        const klass = classnames(
            'music-uploader u-padding-x-20 u-padding-y-15',
            {
                [className]: className
            }
        );

        return (
            <div className={klass}>
                <Link
                    className="music-uploader__avatar u-br-circle"
                    to={`/artist/${uploader.profile.url_slug}`}
                >
                    <Avatar
                        className="u-circle"
                        type="artist"
                        image={
                            uploader.profile.image_base ||
                            uploader.profile.image
                        }
                    />
                </Link>
                <div className="music-uploader__details">
                    <p className="u-text-orange u-fs-11 u-fw-700 u-tt-uppercase u-ls-n-05 u-lh-17">
                        Uploaded By:
                    </p>
                    <h2 className="music-uploader__name u-fs-18 u-fw-700 u-ls-n-05 u-text-dark u-lh-12">
                        <Link to={`/artist/${uploader.profile.url_slug}`}>
                            {uploader.profile.name} {check}
                        </Link>
                    </h2>
                    <div className="music-uploader__meta u-d-flex u-d-flex--align-center">
                        <SocialLinks
                            links={uploader.profile}
                            className="social-icons--artist"
                            allowNonVerified
                        />
                        {this.renderArtistStats(uploader.profile)}
                    </div>
                </div>
                <div className="music-uploader__follow-wrap">
                    <FollowButtonContainer
                        artist={uploader.profile}
                        className="music-uploader__follow-button follow-button--large"
                        showCount
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(UploaderDetails);
