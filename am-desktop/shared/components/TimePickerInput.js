import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import Dropdown from './Dropdown';
import styles from './TimePickerInput.module.scss';

TimePickerInput.propTypes = {
    momentDate: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};

export default function TimePickerInput({ momentDate, onChange }) {
    const [hours, minutes, ampm] = momentDate.format('hh,mm,a').split(',');

    // <Dropdown /> differs from input elements and doesn't support a defaultValue
    const [displayAmpm, setDisplayAmpm] = useState(ampm);

    const handleTimeChange = useCallback(onChange, [onChange]);
    const handleAmpmChange = useCallback(
        (text, e) => {
            const event = {
                ...e,
                target: {
                    ...e.target,
                    name: e.target.getAttribute('data-name'),
                    value: e.target.getAttribute('data-value')
                }
            };

            setDisplayAmpm(event.target.value);
            onChange(event);
        },
        [onChange]
    );

    return (
        <div className={styles.container}>
            <input
                className={styles.input}
                defaultValue={hours}
                max={12}
                min={1}
                name="hours"
                onChange={handleTimeChange}
                type="number"
            />
            <span className={styles.separator}>:</span>
            <input
                className={styles.input}
                defaultValue={minutes}
                max={59}
                min={0}
                name="minutes"
                onChange={handleTimeChange}
                type="number"
            />
            <Dropdown
                name="ampm"
                onChange={handleAmpmChange}
                options={[
                    { text: 'AM', value: 'am' },
                    { text: 'PM', value: 'pm' }
                ]}
                value={displayAmpm}
            />
        </div>
    );
}
