import { Component } from 'react';
import PropTypes from 'prop-types';

export default class BodyClickListener extends Component {
    static propTypes = {
        shouldListen: PropTypes.bool.isRequired,
        onClick: PropTypes.func.isRequired,
        ignoreDomElement: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        if (this.props.shouldListen) {
            this.attachBodyListener();
        }
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.shouldListen && this.props.shouldListen) {
            this.attachBodyListener();
        } else if (prevProps.shouldListen && !this.props.shouldListen) {
            this.removeBodyListener();
        }
    }

    componentWillUnmount() {
        this.removeBodyListener();
        this._attachedBodyListener = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBodyClick = (e) => {
        const target = e.target;
        const { ignoreDomElement, onClick } = this.props;

        if (
            (ignoreDomElement && ignoreDomElement.contains(target)) ||
            target === ignoreDomElement
        ) {
            return;
        }

        const ret = onClick(e);

        if (ret === false) {
            return;
        }

        this.removeBodyListener();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;
        document.body.removeEventListener('click', this.handleBodyClick, false);
    }

    render() {
        return null;
    }
}
