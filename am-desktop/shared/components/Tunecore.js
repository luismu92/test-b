import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import analyics, { eventCategory, eventAction } from 'utils/analytics';

import CloseIcon from '../icons/close-thin';

import styles from './Tunecore.module.scss';

function Tunecore({
    linkHref,
    eventLabel,
    className,
    variant = '',
    location = '',
    currentUser,
    style
}) {
    const [visible, toggleVisibility] = useState(true);
    const isPremiumUser =
        currentUser.isLoggedIn && currentUser.profile.subscription !== null;

    if (!visible || isPremiumUser) {
        return null;
    }

    const handleTuneCoreClick = () => {
        analyics.track(eventCategory.ad, {
            eventAction: eventAction.tunecore,
            eventLabel: eventLabel
        });
    };

    const klass = classnames(styles.banner, {
        [styles[variant]]: variant,
        [styles[location]]: location,
        [className]: className
    });

    // eslint-disable-next-line
    {
        /*
    let partnerList;

    if (variant !== 'home') {
        partnerList = (
            <ul className={styles.logos}>
                <li className={styles.logo}>
                    <img
                        src="/static/images/tunecore-partners/spotify.png"
                        srcSet="/static/images/tunecore-partners/spotify@2x.png 2x"
                        alt="Spotify"
                    />
                </li>
                <li className={styles.logo}>
                    <img
                        src="/static/images/tunecore-partners/apple-music.png"
                        srcSet="/static/images/tunecore-partners/apple-music@2x.png 2x"
                        alt="Apple Music"
                    />
                </li>
                <li className={`${styles.logo} ${styles.tidal}`}>
                    <img
                        src="/static/images/tunecore-partners/tidal.png"
                        srcSet="/static/images/tunecore-partners/tidal@2x.png 2x"
                        alt="Tidal"
                    />
                </li>
                <li className={styles.logo}>
                    <img
                        src="/static/images/tunecore-partners/youtube.png"
                        srcSet="/static/images/tunecore-partners/youtube@2x.png 2x"
                        alt="YouTube"
                    />
                </li>
                <li className={styles.logo}>
                    <img
                        src="/static/images/tunecore-partners/tiktok-logo.png"
                        srcSet="/static/images/tunecore-partners/tiktok-logo@2x.png 2x"
                        alt="TikTok"
                    />
                </li>
            </ul>
        );
    }

    */
    }

    const TunecoreButton = (
        <div className={styles.buttonWrap}>
            <button
                className={`button button--pill u-tt-uppercase ${
                    styles.button
                }`}
            >
                Get Started
            </button>
        </div>
    );

    const TunecoreTitle = (
        <h3 className={styles.title}>
            Get Heard on Audiomack. Earn More with TuneCore.
        </h3>
    );

    let closeButton;

    if (variant !== 'side' && location !== 'music') {
        closeButton = (
            <CloseIcon
                className={styles.close}
                onClick={() => toggleVisibility(!visible)} // eslint-disable-line
            />
        );
    }

    let AdTag;

    if (variant === 'condensed' && location !== 'music') {
        AdTag = <span className={styles.tag}>Advertisement</span>;
    }

    let adLabel;

    if (variant === 'home') {
        adLabel = <span className={styles.label}>Ad</span>;
    }

    let TunecoreLogo;

    if (variant === 'home') {
        TunecoreLogo = (
            <img
                className={styles.tunecoreLogo}
                src="/static/images/desktop/tc-logo-blue.png"
                srcSet="/static/images/desktop/tc-logo-blue@2x.png 2x"
                alt="TuneCore logo"
            />
        );
    }

    const layout = (
        <div className={klass} style={style || {}}>
            {AdTag}
            {closeButton}
            <a
                onClick={handleTuneCoreClick} //eslint-disable-line
                href={linkHref}
                target="_blank"
                rel="nofollow noopener"
            >
                {adLabel}
                <div className={styles.wrap}>
                    <div className={styles.main}>
                        {TunecoreTitle}
                        <p className={styles.content}>
                            Want to get your music on Spotify, Apple Music, and
                            more? Sell your music worldwide and{' '}
                            <strong>
                                keep 100% of your rights and revenue.
                            </strong>
                        </p>
                    </div>
                    {/* partnerList */}
                    {TunecoreButton}
                </div>
                {TunecoreLogo}
            </a>
        </div>
    );

    return layout;
}

Tunecore.propTypes = {
    linkHref: PropTypes.string.isRequired,
    eventLabel: PropTypes.string.isRequired,
    className: PropTypes.string,
    variant: PropTypes.string,
    location: PropTypes.string,
    currentUser: PropTypes.object,
    style: PropTypes.object
};

// useSelector kept throwing a "is not a function" error
// so using mapStateToProps until I have more time to debug
function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default connect(mapStateToProps)(Tunecore);
