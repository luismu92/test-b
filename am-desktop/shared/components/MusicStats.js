import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { humanizeNumber } from 'utils/index';

import PlayIcon from '../icons/play-thin';
import HeartIcon from '../../../am-shared/icons/heart';
import RetweetIcon from '../icons/retweet';
import PlusIcon from '../icons/plus';

export default class MusicStats extends Component {
    static propTypes = {
        stats: PropTypes.object.isRequired
    };

    render() {
        const { stats } = this.props;
        const plays = stats['plays-raw'];
        const favorites = stats['favorites-raw'];
        const playlists = stats['playlists-raw'];
        const reposts = stats['reposts-raw'];

        // don't show icons if zero counts
        let reups;
        let playlistAdds;

        if (reposts !== 0) {
            reups = (
                <li>
                    <RetweetIcon className="u-text-icon interaction-stat-icon interaction-stat-icon--reup" />
                    {humanizeNumber(reposts)}
                </li>
            );
        }

        if (playlists !== 0) {
            playlistAdds = (
                <li>
                    <PlusIcon className="u-text-icon interaction-stat-icon interaction-stat-icon--playlist" />
                    {humanizeNumber(playlists)}
                </li>
            );
        }

        return (
            <ul className="interaction-stats interaction-stats--inline u-inline-list">
                <li>
                    <PlayIcon className="u-text-icon interaction-stat-icon interaction-stat-icon--play" />
                    {humanizeNumber(plays)}
                </li>

                <li>
                    <HeartIcon className="u-text-icon interaction-stat-icon interaction-stat-icon--fav" />
                    {humanizeNumber(favorites)}
                </li>

                {reups}

                {playlistAdds}
            </ul>
        );
    }
}
