import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import DatePickerInput from '../components/DatePickerInput';
import LabeledInput, {
    inputTypes as labeledInputTypes
} from '../components/LabeledInput';

import moment from 'moment';

import styles from './ReleaseOptions.module.scss';

export default class ReleaseOptions extends Component {
    static propTypes = {
        currentDate: PropTypes.object.isRequired,
        onDateBlur: PropTypes.func.isRequired,
        onDateChange: PropTypes.func.isRequired,
        onReleaseOptionChange: PropTypes.func.isRequired,
        selectedReleaseOption: PropTypes.string.isRequired,
        futureReleaseDate: PropTypes.bool.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            diff: null
        };
    }

    componentDidMount() {
        this.setDiff();

        this._timer = window.setInterval(() => {
            if (this._timer) {
                this.setDiff();
            }
        }, 1000);
    }

    componentWillUnmount() {
        window.clearInterval(this._timer);
        this._timer = null;
    }

    setDiff() {
        const { currentDate, futureReleaseDate } = this.props;

        if (!futureReleaseDate) {
            return;
        }

        const duration = moment.duration(currentDate.diff(moment()));

        const days = Math.floor(duration.asDays());
        duration.subtract(moment.duration(days, 'days'));

        const hours = Math.floor(duration.asHours());
        duration.subtract(moment.duration(hours, 'hours'));

        const minutes = Math.floor(duration.asMinutes());
        duration.subtract(moment.duration(minutes, 'minutes'));

        const s = (n) => (n === 1 ? '' : 's');

        let timeString = '';

        if (days) {
            timeString += `${days} day${s(days)}`;

            if (hours && minutes) {
                timeString += ', ';
            } else if (hours || minutes) {
                timeString += ' and ';
            }
        }

        if (hours) {
            timeString += `${hours} hour${s(hours)}`;

            if (minutes) {
                timeString += ' and ';
            }
        }

        if (minutes) {
            timeString += `${minutes} minute${s(minutes)}`;
        }

        this.setState({ diff: timeString });
    }

    render() {
        let datePicker;
        let futureReleaseDateMessage;

        if (this.props.selectedReleaseOption === '3') {
            datePicker = (
                <DatePickerInput
                    className={styles.datePicker}
                    value={this.props.currentDate}
                    onBlur={this.props.onDateBlur}
                    onChange={this.props.onDateChange}
                    placement="right-start"
                />
            );

            if (this.props.futureReleaseDate && this.state.diff) {
                futureReleaseDateMessage = (
                    <p>
                        This upload will be private until it is released in{' '}
                        {this.state.diff}
                    </p>
                );
            }
        }

        return (
            <Fragment>
                <h2 className={styles.heading}>Release</h2>

                <section className={styles.inputGroup}>
                    <LabeledInput
                        checked={this.props.selectedReleaseOption === '1'}
                        id="release_option_1"
                        label="Public"
                        name="release_option"
                        onChange={this.props.onReleaseOptionChange}
                        tooltip="Your track will be live and visible to all (public) once you click Finish below"
                        type={labeledInputTypes.radio}
                        value="1"
                    />

                    <LabeledInput
                        checked={this.props.selectedReleaseOption === '2'}
                        id="release_option_2"
                        label="Private"
                        name="release_option"
                        onChange={this.props.onReleaseOptionChange}
                        tooltip="Your song will remain private and only visible to you once Finish is clicked below.  You can create private share links once the upload is complete."
                        type={labeledInputTypes.radio}
                        value="2"
                    />

                    <LabeledInput
                        checked={this.props.selectedReleaseOption === '3'}
                        id="release_option_3"
                        label="Release Date"
                        name="release_option"
                        onChange={this.props.onReleaseOptionChange}
                        tooltip="You can either:&#13;&#10;&bull; Select a future date/time when this content will automatically go live and be playable.  A countdown timer will appear on the song page until then.&#13;&#10;&bull; Backdate this release to a previous date/time.  This is useful if you are uploading older content and want to preserve proper chronological order of content on your artist page."
                        type={labeledInputTypes.radio}
                        value="3"
                    />
                    <div className={styles.scheduleFooter}>
                        {datePicker}
                        {futureReleaseDateMessage}
                    </div>
                </section>
            </Fragment>
        );
    }
}
