import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CloseIcon from '../icons/close-thin';

export default class ConfirmMessage extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.object.isRequired,
            PropTypes.array.isRequired
        ]),
        className: PropTypes.string,
        open: PropTypes.bool,
        hideCloseButton: PropTypes.bool,
        onClose: PropTypes.func
    };

    static defaultProps = {
        hideCloseButton: false
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { className, hideCloseButton } = this.props;
        const klass = classnames('c-message', {
            [className]: className,
            'c-message--dismissable': !hideCloseButton
        });

        if (!this.props.open) {
            return null;
        }

        let closeButton;

        if (!hideCloseButton) {
            closeButton = (
                <button
                    className="c-message__icon"
                    onClick={this.props.onClose}
                >
                    <CloseIcon />
                </button>
            );
        }

        return (
            <div className={klass}>
                {this.props.children}
                {closeButton}
            </div>
        );
    }
}
