import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import { COLLECTION_TYPE_TRENDING } from 'constants/index';

import { setContext, fetchSongList } from './redux/modules/music/browse';
import { showTopAd, hideTopAd } from './redux/modules/global';

import AndroidLoader from './loaders/AndroidLoader';
import MusicDetailContainer from './browse/MusicDetailContainer';

import FireIcon from './icons/fire';

class NotFound extends Component {
    static propTypes = {
        type: PropTypes.string,
        dispatch: PropTypes.func,
        errors: PropTypes.array,
        musicBrowse: PropTypes.object
    };

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(hideTopAd());

        document.body.classList.remove('has-hidden-header');
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(showTopAd());
    }

    renderTrending(data = {}) {
        const { list, activeContext, activeGenre } = data;

        const filteredList = list.filter((item) => {
            return (
                item.status !== 'suspended' &&
                item.status !== 'takedown' &&
                !item.geo_restricted
            );
        });

        const items = filteredList.map((musicItem, i) => {
            let ranking = null;

            if (activeContext === 'songs' || activeContext === 'albums') {
                ranking = i + 1;
            }

            return (
                <div
                    className="u-spacing-bottom-60"
                    key={`${musicItem.id}-${i}`}
                >
                    <MusicDetailContainer
                        index={i}
                        item={musicItem}
                        shouldLinkArtwork
                        feed
                        musicList={filteredList}
                        context={activeContext}
                        genre={activeGenre}
                        ranking={ranking}
                    />
                </div>
            );
        });

        return (
            <div data-testid="browseContainer" className="column small-24">
                {items}
            </div>
        );
    }

    render() {
        const { musicBrowse } = this.props;
        const { loading } = musicBrowse;
        let loader;

        if (loading) {
            loader = <AndroidLoader className="music-feed__loader" />;
        }

        return (
            <Fragment>
                <Helmet>
                    <meta name="robots" content="noindex, nofollow" />
                </Helmet>
                <div className="error-404 u-padding-top-50">
                    <div className="error-404__inner u-text-center">
                        <h1 className="error-404__title">404</h1>
                        <h2 className="error-404__subtitle">Page not found</h2>
                        <p className="error-404__content">
                            Discover your next favorite song on our{' '}
                            <a href={`${process.env.AM_URL}/songs/week`}>
                                Top Songs
                            </a>
                            ,{' '}
                            <a href={`${process.env.AM_URL}/albums/week`}>
                                Top Albums
                            </a>
                            , or{' '}
                            <a href={`${process.env.AM_URL}/playlists/browse`}>
                                Top Playlists Charts
                            </a>
                            !
                        </p>
                    </div>
                    <div className="error-404__feed row u-spacing-top-60 u-padding-x-40">
                        <header className="column small-24 u-spacing-bottom-50">
                            <div className="feed-bar feed-bar--padded">
                                <h2 className="feed-bar__title">
                                    <FireIcon className="feed-bar__title-icon feed-bar__title-icon--fire u-text-icon" />
                                    <strong>Trending Now on Audiomack</strong>
                                </h2>
                            </div>
                        </header>
                        <div className="row expanded column small-24">
                            {this.renderTrending(musicBrowse)}
                            {loader}
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        musicBrowse: state.musicBrowse
    };
}

export default compose(
    // Needed because we render this component inside other components
    // The alternative would be to pass match/location as props
    // to this component as connectDataFetchers requires those as props
    withRouter,
    connect(mapStateToProps)
)(
    connectDataFetchers(NotFound, [
        () => setContext(COLLECTION_TYPE_TRENDING),
        () => fetchSongList()
    ])
);
