import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import analytics from 'utils/analytics';
import { queryForTimespan, queryToday } from 'utils/stats';
import connectDataFetchers from 'lib/connectDataFetchers';
import requireAuth from '../hoc/requireAuth';

import { showModal, MODAL_TYPE_AMP_CODES } from '../redux/modules/modal';
import { deletePromoKey } from '../redux/modules/promoKey';
import { addToast } from '../redux/modules/toastNotification';
import { getSummary } from '../redux/modules/stats/music/summary';
import { getCountryPlays } from '../redux/modules/stats/music/geo';
import { getDailyStats } from '../redux/modules/stats/music/daily';
import { getUrls } from '../redux/modules/stats/music/url';
import { getPlaySources } from '../redux/modules/stats/music/playSource';
import { getPlaylistAdds } from '../redux/modules/stats/music/playlistAdds';
import { getPromoLinks } from '../redux/modules/stats/music/promoLink';

import MusicStatsPage from './MusicStatsPage';

const DEFAULT_CATEGORY = 'play';
const DEFAULT_TIMESPAN = 'week';
const COUNTRY_QUERY_LIMIT = 12;
const PLAYLIST_QUERY_LIMIT = 5;

class MusicStatsPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        match: PropTypes.object.isRequired,
        statsMusicGeo: PropTypes.object,
        statsMusicSummary: PropTypes.object,
        statsMusicDaily: PropTypes.object,
        statsMusicUrl: PropTypes.object,
        statsMusicPlaySource: PropTypes.object,
        statsMusicFans: PropTypes.object,
        statsMusicInfluencers: PropTypes.object,
        statsMusicPlaylistAdds: PropTypes.object,
        statsMusicPromoLink: PropTypes.object,
        promoKey: PropTypes.object,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            timespanValue: DEFAULT_TIMESPAN,
            snapshotCategory: DEFAULT_CATEGORY
        };
    }

    componentDidUpdate(prevProps) {
        if (
            this.props.match.params.musicId !== prevProps.match.params.musicId
        ) {
            this.refetchPageData();
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleTimespanChange = (e) => {
        const input = e.currentTarget;
        const { name, value } = input;

        this.setState({
            [name]: value
        });
    };

    handlePromoLinkDeleteClick = (e) => {
        const { dispatch, match } = this.props;

        const keyName = e.currentTarget.getAttribute('data-key');
        const index = e.currentTarget.getAttribute('data-index');

        e.preventDefault();

        dispatch(deletePromoKey(match.params.musicId, keyName, index))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Successfully deleted "${keyName}"`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There was a problem deleting the key "${keyName}"`
                    })
                );
                analytics.error(error);
                return;
            });
        return;
    };

    handleTimespanClick = (text, e) => {
        const { snapshotCategory } = this.state;
        const selected = e.currentTarget.getAttribute('data-value') || null;

        this.setState({
            timespanValue: selected
        });

        this.fetchSummaryData(selected, snapshotCategory);
    };

    handleSnapshotBlockClick = (e) => {
        const { timespanValue } = this.state;

        const button = e.currentTarget;
        const attribute = button.getAttribute('data-action');

        this.setState({
            snapshotCategory: attribute
        });

        this.fetchSummaryData(timespanValue, attribute);
    };

    handleEditAmpCodesClick = (item) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_AMP_CODES, { item, contextType: 'uploads' })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    fetchSummaryData(timespan, category) {
        const {
            dispatch,
            match: { params },
            currentUser
        } = this.props;
        const queryStart = queryForTimespan(timespan);

        dispatch(
            getDailyStats(currentUser.profile.id, params.musicId, {
                startDate: queryStart,
                endDate: queryToday(),
                attribute: category
            })
        );

        dispatch(
            getSummary(currentUser.profile.id, params.musicId, {
                startDate: queryStart,
                endDate: queryToday()
            })
        );
    }

    refetchPageData() {
        const {
            dispatch,
            match: { params },
            currentUser
        } = this.props;
        const {
            timespanValue: timespan,
            snapshotCategory: category
        } = this.state;
        const queryStart = queryForTimespan(timespan);

        dispatch(
            getSummary(currentUser.profile.id, params.musicId, {
                startDate: queryStart,
                endDate: queryToday()
            })
        );
        dispatch(
            getDailyStats(currentUser.profile.id, params.musicId, {
                startDate: queryStart,
                endDate: queryToday(),
                attribute: category
            })
        );

        // @todo lazy load these as a user scrolls
        dispatch(
            getPlaySources(currentUser.profile.id, params.musicId, {
                startDate: queryStart,
                endDate: queryToday()
            })
        );

        dispatch(
            getCountryPlays(currentUser.profile.id, params.musicId, {
                startDate: queryStart,
                endDate: queryToday(),
                limit: COUNTRY_QUERY_LIMIT
            })
        );

        dispatch(
            getPlaylistAdds(currentUser.profile.id, params.musicId, {
                limit: PLAYLIST_QUERY_LIMIT
            })
        );

        dispatch(getUrls(currentUser.profile.id, params.musicId));

        dispatch(getPromoLinks(currentUser.profile.id, params.musicId));
    }

    render() {
        return (
            <MusicStatsPage
                currentUser={this.props.currentUser}
                statsMusicGeo={this.props.statsMusicGeo}
                statsMusicSummary={this.props.statsMusicSummary}
                statsMusicDaily={this.props.statsMusicDaily}
                statsMusicUrl={this.props.statsMusicUrl}
                statsMusicPlaySource={this.props.statsMusicPlaySource}
                statsMusicFans={this.props.statsMusicFans}
                statsMusicInfluencers={this.props.statsMusicInfluencers}
                statsMusicPlaylistAdds={this.props.statsMusicPlaylistAdds}
                statsMusicPromoLink={this.props.statsMusicPromoLink}
                promoKey={this.props.promoKey}
                onTimespanChange={this.handleTimespanChange}
                onEditAmpCodesClick={this.handleEditAmpCodesClick}
                onPromoLinkDeleteClick={this.handlePromoLinkDeleteClick}
                snapshotCategory={this.state.snapshotCategory}
                onTimespanClick={this.handleTimespanClick}
                onSnapshotBlockClick={this.handleSnapshotBlockClick}
                timespanValue={this.state.timespanValue}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        statsMusicDaily: state.statsMusicDaily,
        statsMusicGeo: state.statsMusicGeo,
        statsMusicSummary: state.statsMusicSummary,
        statsMusicUrl: state.statsMusicUrl,
        statsMusicPlaySource: state.statsMusicPlaySource,
        statsMusicFans: state.statsMusicFans,
        statsMusicInfluencers: state.statsMusicInfluencers,
        statsMusicPlaylistAdds: state.statsMusicPlaylistAdds,
        statsMusicPromoLink: state.statsMusicPromoLink,
        promoKey: state.promoKey
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(MusicStatsPageContainer, [
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getSummary(props.currentUser.profile.id, params.musicId, {
                startDate: queryForTimespan(DEFAULT_TIMESPAN),
                endDate: queryToday()
            });
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getDailyStats(props.currentUser.profile.id, params.musicId, {
                startDate: queryForTimespan(DEFAULT_TIMESPAN),
                endDate: queryToday()
            });
        },

        // @todo lazy load these as a user scrolls

        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getPlaySources(
                props.currentUser.profile.id,
                params.musicId,
                {
                    startDate: queryForTimespan(DEFAULT_TIMESPAN),
                    endDate: queryToday()
                }
            );
        },

        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getCountryPlays(
                props.currentUser.profile.id,
                params.musicId,
                {
                    startDate: queryForTimespan(DEFAULT_TIMESPAN),
                    endDate: queryToday(),
                    limit: COUNTRY_QUERY_LIMIT
                }
            );
        },

        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getPlaylistAdds(currentUser.profile.id, params.musicId, {
                limit: PLAYLIST_QUERY_LIMIT
            });
        },

        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getUrls(props.currentUser.profile.id, params.musicId);
        },

        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getPromoLinks(props.currentUser.profile.id, params.musicId);
        }
    ])
);
