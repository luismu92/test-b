import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Table from '../components/Table';

import AndroidLoader from '../loaders/AndroidLoader';

function EmbedStats({ data }) {
    const { data: stats, loading } = data;

    let content;
    let emptyMessage;
    let smallText2;

    if (!stats.length) {
        emptyMessage = (
            <p className="u-text-center u-padded">
                Sorry, there doesn't seem to be anything here. Please try again
                later.
            </p>
        );
    }

    let loader;

    if (loading) {
        loader = (
            <div className="u-overlay-loader">
                <AndroidLoader />
            </div>
        );
    }

    if (stats.length) {
        const headerCells = [
            {
                value: '#',
                props: {
                    style: {
                        flex: '0 0 70px'
                    }
                }
            },
            {
                value: 'Domain',
                props: {
                    className: 'flex-1',
                    style: {
                        paddingRight: 30
                    }
                }
            },
            {
                value: 'Plays',
                props: {
                    style: {
                        flex: '0 0 40px'
                    }
                }
            }
        ];

        const rows = stats.map((site, i) => {
            return (
                <tr key={i}>
                    <td style={{ flex: '0 0 70px' }}>{i + 1}</td>
                    <td
                        className="flex-1 u-text-orange u-trunc"
                        style={{ paddingRight: 30 }}
                    >
                        <a
                            href={site.embedded_url}
                            rel="nofollow noopener"
                            target="_blank"
                        >
                            <strong>{site.embedded_url}</strong>
                        </a>
                    </td>
                    <td style={{ flex: '0 0 40px' }}>{site.play}</td>
                </tr>
            );
        });

        content = (
            <Fragment>
                <Table
                    className="dashboard-table table--block"
                    headerCells={headerCells}
                    bodyRows={rows}
                />
            </Fragment>
        );
        smallText2 = (
            <small className="u-spacing-top-em u-d-block u-fw-700">
                * may not be completely accurate due to various crawlers that
                incorrectly pass referer info and people who have set their
                browsers to not pass referer headers
            </small>
        );
    }

    return (
        <Fragment>
            <div className="u-box-shadow u-padding-x-20 u-pos-relative">
                {loader}
                {content}
                {emptyMessage}
            </div>
            {smallText2}
        </Fragment>
    );
}

EmbedStats.propTypes = {
    data: PropTypes.object
};

export default EmbedStats;
