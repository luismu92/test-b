import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import classnames from 'classnames';

import {
    ucfirst,
    getMusicUrl,
    humanizeNumber,
    getPercentage
} from 'utils/index';
import { eventLabel } from 'utils/analytics';
import { getCountryName } from 'utils/stats';

import AmpEditCodesModal from '../modal/AmpEditCodesModal';
import TunecoreAd from '../components/Tunecore';
import GeoChart from '../stats/GeoChart';
import MusicDetailContainer from '../browse/MusicDetailContainer';
import MusicDetailPlaceholder from '../browse/MusicDetailPlaceholder';
import AndroidLoader from '../loaders/AndroidLoader';
import Dropdown from '../components/Dropdown';
import LineChart from '../components/LineChart';
import Table from '../components/Table';
import PercentageBar from '../components/PercentageBar';
import DashboardModuleTitle from '../dashboard/DashboardModuleTitle';
// import DashboardArtistBlock from '../dashboard/DashboardArtistBlock';
import MusicStatsPlaylists from './MusicStatsPlaylists';

import PlayIcon from '../icons/play';
import HeartIcon from '../../../am-shared/icons/heart';
import MusicIcon from '../icons/music';
import ReupIcon from '../icons/retweet';
import PlusIcon from '../icons/plus';
import ChartLineIcon from '../icons/chart-line';
import EmbedIcon from '../icons/embed-close';
import LinkIcon from '../icons/link';
import TracklistIcon from '../icons/tracklist';
import CheckIcon from '../icons/check-mark';
import WarningIcon from '../icons/warning';
import UserIcon from '../icons/avatar';
import TrashIcon from 'icons/trash';

import StatBlock from './StatBlock';
import StatsSources from './StatsSources';
import EmbedStats from './EmbedStats';
export default class MusicStatsPage extends Component {
    static propTypes = {
        statsMusicDaily: PropTypes.object,
        statsMusicSummary: PropTypes.object,
        statsMusicFans: PropTypes.object,
        statsMusicInfluencers: PropTypes.object,
        statsMusicGeo: PropTypes.object,
        statsMusicPlaySource: PropTypes.object,
        statsMusicPlaylistAdds: PropTypes.object,
        statsMusicUrl: PropTypes.object,
        statsMusicPromoLink: PropTypes.object,
        onSnapshotBlockClick: PropTypes.func,
        onTimespanClick: PropTypes.func,
        timespanValue: PropTypes.string,
        snapshotCategory: PropTypes.string,
        onPromoLinkDeleteClick: PropTypes.func
    };

    static defaultProps = {
        onSnapshotBlockClick() {}
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getLoader(loading) {
        if (loading) {
            return (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        return null;
    }

    renderEmpty(
        text = "Sorry, there doesn't seem to be anything here. Please try again later."
    ) {
        return <p className="u-text-center u-padded">{text}</p>;
    }

    renderMusicDetail() {
        const { statsMusicSummary } = this.props;
        const { info: musicItem, loading } = statsMusicSummary;

        let content = (
            <Fragment>
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Advanced Stats"
                        icon={<MusicIcon />}
                    />
                </div>
                <MusicDetailPlaceholder
                    className="column u-spacing-bottom u-spacing-top"
                    animated
                />
            </Fragment>
        );

        if ((!musicItem || !musicItem.type) && !loading) {
            content = (
                <div className="column small-24">
                    {this.renderEmpty(
                        "Couldn't load music information. Please try again later."
                    )}
                </div>
            );
        } else if (musicItem && musicItem.type) {
            content = (
                <Fragment>
                    <div className="column small-24">
                        <DashboardModuleTitle
                            text="Advanced Stats for"
                            suffixText={
                                <Link to={getMusicUrl(musicItem)}>
                                    {musicItem.title}
                                </Link>
                            }
                            icon={<MusicIcon />}
                        />
                    </div>
                    <div className="row expanded column small-24">
                        <div className="column">
                            <MusicDetailContainer
                                className="u-spacing-top dashboard-music-detail"
                                item={musicItem}
                                musicList={[musicItem]}
                                hideWaveform
                                hideInteractions
                            />
                        </div>
                    </div>
                </Fragment>
            );
        }

        return (
            <section className="row u-pos-relative">
                {this.getLoader(!musicItem && loading)}
                {content}
            </section>
        );
    }

    renderStatBlocks() {
        const {
            timespanValue,
            snapshotCategory,
            onTimespanClick,
            onSnapshotBlockClick,
            statsMusicSummary,
            statsMusicDaily
        } = this.props;

        if (!statsMusicSummary) {
            return null;
        }

        const statBlocks = [
            {
                count: statsMusicSummary.stats.play,
                stat: 'play',
                text: 'Plays',
                Icon: PlayIcon
            },
            {
                count: statsMusicSummary.stats.favorite,
                stat: 'favorite',
                text: 'Favorites',
                Icon: HeartIcon
            },
            {
                count: statsMusicSummary.stats.repost,
                stat: 'repost',
                text: 'Re-Ups',
                Icon: ReupIcon
            },
            statsMusicSummary.stats &&
            statsMusicSummary.stats.content_type === 'song'
                ? {
                      count: statsMusicSummary.stats.playlist_song_add,
                      stat: 'playlist_song_add',
                      text: 'Playlist +',
                      Icon: PlusIcon
                  }
                : null
        ]
            .filter(Boolean)
            .map(({ count, stat, text, Icon }, i, arr) => {
                const klass = classnames(
                    `stat-block--${stat} u-spacing-bottom column small-12 u-text-center`,
                    {
                        'medium-expand': arr.length !== 5,
                        'medium-fifth': arr.length === 5
                    }
                );
                const iconClass = `stat-block__icon stat-block__icon--${stat}`;

                return (
                    <div key={i} className={klass}>
                        <StatBlock
                            text={text}
                            value={count}
                            type="number"
                            loading={
                                statsMusicSummary.loading &&
                                typeof count === 'undefined'
                            }
                            action={stat}
                            iconClass={iconClass}
                            onClick={onSnapshotBlockClick}
                            icon={<Icon />}
                            active={snapshotCategory === stat}
                        />
                    </div>
                );
            });

        const timespanList = [
            {
                value: 'week',
                text: 'This Week'
            },
            {
                value: 'month',
                text: 'This Month'
            },
            {
                value: 'three-months',
                text: '3 Months'
            }

            // When adding custom timeframe and all-time
            // put an "(admin only)" label after them and only
            // make them available to admins initially
        ];

        const categoryToKey = {
            repost: 're-ups',
            playlist_song_add: 'playlist add'
        };

        const chartData = statsMusicDaily.data.reduce(
            (obj, stat) => {
                obj.data.push(stat[snapshotCategory]);
                obj.labels.push(stat.event_date);
                return obj;
            },
            { data: [], labels: [] }
        );
        const chartOptions = {
            layout: {
                padding: {
                    top: 25
                }
            }
        };

        const popover = (
            <Fragment>
                The Creator Dashboard only reflects stats from uploads on your
                account. For any questions about discrepancies please email{' '}
                <a href="mailto:creators@audiomack.com">
                    creators@audiomack.com
                </a>
            </Fragment>
        );

        return (
            <div className="row u-spacing-bottom-50">
                <div className="column small-24 u-left-right">
                    <DashboardModuleTitle
                        text="Stats Summary"
                        icon={<UserIcon />}
                        style={{ width: 'auto' }}
                        popoverText={popover}
                    />
                    <Dropdown
                        value={timespanValue}
                        options={timespanList}
                        onChange={onTimespanClick}
                        className="c-dropdown--button"
                        buttonClassName="button button--pill"
                        menuClassName="tooltip sub-menu sub-menu--condensed tooltip--right-arrow tooltip--active"
                    />
                </div>
                {statBlocks}
                <div className="column small-24">
                    <div className="u-box-shadow u-pos-relative">
                        {this.getLoader(statsMusicDaily.loading)}
                        <div className="u-padded" style={{ paddingTop: 0 }}>
                            <LineChart
                                height={265}
                                labels={chartData.labels}
                                datasets={[
                                    {
                                        data: chartData.data
                                    }
                                ]}
                                options={chartOptions}
                                tooltipLabel={
                                    categoryToKey[snapshotCategory] ||
                                    snapshotCategory
                                }
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderStatusIcon(status) {
        if (status === 'complete') {
            return (
                <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                    <CheckIcon className="u-text-icon u-margin-0 status-icon status-icon--active" />
                </span>
            );
        }

        return (
            <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                <WarningIcon className="u-text-icon u-margin-0 u-text-yellow" />
            </span>
        );
    }

    renderAlbumTrackDetails() {
        const { statsMusicSummary } = this.props;
        const { info: musicItem, tracks } = statsMusicSummary;

        if (
            !musicItem ||
            musicItem.type !== 'album' ||
            !tracks ||
            !tracks.length
        ) {
            return null;
        }

        const headerCells = [
            {
                value: '#',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.trackNo - x.trackNo;
                    }

                    return x.trackNo - y.trackNo;
                }
            },
            {
                value: 'Title',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.title.localeCompare(x.title);
                    }

                    return x.title.localeCompare(y.title);
                },
                props: {
                    className: 'flex-3'
                }
            },
            {
                value: 'Plays',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.play - x.play;
                    }

                    return x.play - y.play;
                }
            },
            {
                value: 'Favs',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.favorite - x.favorite;
                    }

                    return x.favorite - y.favorite;
                }
            },
            {
                value: 'Playlist +',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.playlist_song_add - x.playlist_song_add;
                    }

                    return x.playlist_song_add - y.playlist_song_add;
                }
            },
            {
                value: 'Re-Ups',
                comparator(direction, x, y) {
                    if (direction === 'desc') {
                        return y.playlist_song_add - x.playlist_song_add;
                    }

                    return x.playlist_song_add - y.playlist_song_add;
                }
            },
            'Actions'
        ];

        const rows = statsMusicSummary.tracks.map((track, i) => {
            const model = {
                ...track,
                trackNo: i + 1,
                uploader: musicItem.uploader,
                stats: musicItem.stats
            };

            return {
                model: model,
                value: (
                    <tr key={i}>
                        <td>{i + 1}</td>
                        <td className="flex-3">{track.title}</td>
                        <td data-th="Plays">{humanizeNumber(track.play)}</td>
                        <td data-th="Favs">{humanizeNumber(track.favorite)}</td>
                        <td data-th="Playlist +">
                            {humanizeNumber(track.playlist_song_add)}
                        </td>
                        <td data-th="Re-Ups">{humanizeNumber(track.repost)}</td>
                        <td data-th="Actions">
                            <Link
                                to={`/stats/music/${track.content_id}`}
                                data-tooltip="Advanced Stats"
                                style={{ marginRight: '0.5em' }}
                                data-tooltip-dark
                            >
                                <ChartLineIcon className="u-text-icon u-text-orange" />
                            </Link>
                            {/* Commenting this out since we dont get url_slug
                            from the stats api <button
                                onClick={this.handleItemClick}
                                className="dashboard-table__edit button--link"
                            >
                                <ExternalIcon className="u-text-icon u-text-orange" />
                            </button>*/}
                        </td>
                    </tr>
                )
            };
        });

        return (
            <section className="u-spacing-bottom-50">
                <div className="row">
                    <div className="column small-24">
                        <DashboardModuleTitle
                            text="Album Tracklist"
                            icon={<TracklistIcon />}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="column small-24">
                        <div
                            className="u-box-shadow u-padded"
                            style={{ paddingTop: 0 }}
                        >
                            <div className="row">
                                <div className="column small-24">
                                    <Table
                                        className="dashboard-table stats tracklist"
                                        headerCells={headerCells}
                                        bodyRows={rows}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    // renderFans() {
    //     const { statsMusicFans } = this.props;
    //     const formatted = statsMusicFans.data.reduce(
    //         (acc, source) => {
    //             acc.total += source.plays;
    //             acc.list.push(source);

    //             return acc;
    //         },
    //         {
    //             total: 0,
    //             list: []
    //         }
    //     );
    //     const blocks = statsMusicFans.data.slice(0, 3).map((artist, i) => {
    //         const percentage = getPercentage(artist.plays, formatted.total);

    //         if (!artist.info) {
    //             return null;
    //         }

    //         return (
    //             <DashboardArtistBlock
    //                 plays={artist.plays}
    //                 className={
    //                     i === formatted.list.length - 1
    //                         ? ''
    //                         : 'u-spacing-bottom'
    //                 }
    //                 key={i}
    //                 artist={artist.info}
    //                 percentage={percentage}
    //             />
    //         );
    //     });

    //     let emptyState;
    //     let seeMoreButton = (
    //         <p className="u-text-center u-spacing-top">
    //             <Link
    //                 to="/dashboard/fans"
    //                 className="button button--pill button--med"
    //             >
    //                 See All Fans
    //             </Link>
    //         </p>
    //     );

    //     if (!formatted.list.length) {
    //         let message = 'There is no data to display here.';

    //         if (statsMusicFans.error) {
    //             message =
    //                 'There was an error loading your top fans. Please try again later.';
    //         }

    //         seeMoreButton = null;
    //         emptyState = (
    //             <div className="u-padded u-box-shadow">
    //                 <p>{message}</p>
    //             </div>
    //         );
    //     }

    //     return (
    //         <section className="column small-24 medium-12 half-padding">
    //             <DashboardModuleTitle text="Top Fans" icon={<UserIcon />} />
    //             <div className="u-pos-relative">
    //                 {this.getLoader(statsMusicFans.loading)}
    //                 {blocks}
    //                 {seeMoreButton}
    //                 {emptyState}
    //             </div>
    //         </section>
    //     );
    // }

    // renderInfluencers() {
    //     const { statsMusicInfluencers } = this.props;
    //     const formatted = statsMusicInfluencers.data.reduce(
    //         (acc, source) => {
    //             acc.total += source.followers;
    //             acc.list.push(source);

    //             return acc;
    //         },
    //         {
    //             total: 0,
    //             list: []
    //         }
    //     );
    //     const blocks = statsMusicInfluencers.data
    //         .slice(0, 3)
    //         .map((artist, i) => {
    //             const percentage = getPercentage(
    //                 artist.followers,
    //                 formatted.total
    //             );

    //             if (!artist.info) {
    //                 return null;
    //             }

    //             return (
    //                 <DashboardArtistBlock
    //                     plays={artist.plays}
    //                     className={
    //                         i === formatted.list.length - 1
    //                             ? ''
    //                             : 'u-spacing-bottom'
    //                     }
    //                     key={i}
    //                     artist={artist.info}
    //                     percentage={percentage}
    //                 />
    //             );
    //         });
    //     let emptyState;
    //     let seeMoreButton = (
    //         <p className="u-text-center u-spacing-top">
    //             <Link
    //                 to="/dashboard/influencers"
    //                 className="button button--pill button--med"
    //             >
    //                 See All Influencers
    //             </Link>
    //         </p>
    //     );

    //     if (!formatted.list.length) {
    //         let message = 'There is no data to display here.';

    //         if (statsMusicInfluencers.error) {
    //             message =
    //                 'There was an error loading your top fans. Please try again later.';
    //         }

    //         seeMoreButton = null;
    //         emptyState = (
    //             <div className="u-padded u-box-shadow">
    //                 <p>{message}</p>
    //             </div>
    //         );
    //     }

    //     return (
    //         <section className="column small-24 medium-12 half-padding">
    //             <DashboardModuleTitle
    //                 text="Top Influencers"
    //                 icon={<UserIcon />}
    //             />
    //             <div className="u-pos-relative">
    //                 {this.getLoader(statsMusicInfluencers.loading)}
    //                 {blocks}
    //                 {emptyState}
    //                 {seeMoreButton}
    //             </div>
    //         </section>
    //     );
    // }

    renderPlaylistAdds(musicItem) {
        const { statsMusicPlaylistAdds, statsMusicSummary } = this.props;

        if (!musicItem || musicItem.type !== 'song') {
            return null;
        }

        return (
            <div className="u-spacing-bottom">
                <MusicStatsPlaylists
                    summary={statsMusicSummary}
                    playlists={statsMusicPlaylistAdds}
                />
            </div>
        );
    }

    renderEmbeddedPlayers() {
        const { statsMusicUrl } = this.props;

        return (
            <section className="row u-spacing-bottom-50">
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Embedded Players"
                        icon={<EmbedIcon />}
                    />
                </div>
                <div className="column small-24">
                    <EmbedStats data={statsMusicUrl} />
                </div>
            </section>
        );
    }

    renderStatsSources() {
        const { statsMusicPlaySource, statsMusicSummary } = this.props;
        const { info: musicItem } = statsMusicSummary;

        let seeAllButton = (
            <p className="u-text-center">
                <Link
                    to={`/stats/music/${musicItem.id}/play-sources`}
                    className="button button--pill button--med"
                >
                    See All Sources
                </Link>
            </p>
        );

        if (statsMusicPlaySource.error) {
            seeAllButton = null;
        }

        return (
            <div className="row u-spacing-bottom-50">
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Play Sources"
                        suffixText="(Last 7 Days)"
                        icon={<LinkIcon />}
                    />
                    <div className="u-box-shadow u-padded u-pos-relative">
                        {this.getLoader(statsMusicPlaySource.loading)}
                        <StatsSources
                            dataSet={statsMusicPlaySource}
                            limit={5}
                        />
                        {seeAllButton}
                    </div>
                </div>
            </div>
        );
    }

    renderGeoMap() {
        const { statsMusicGeo, statsMusicSummary } = this.props;
        const { info: musicItem } = statsMusicSummary;

        const topFour = Array.from(statsMusicGeo.data)
            .sort((x, y) => {
                return y.play - x.play;
            })
            .reduce(
                (acc, stat) => {
                    acc.total += stat.play;

                    if (acc.stats.length > 11) {
                        return acc;
                    }

                    acc.stats.push(stat);

                    return acc;
                },
                {
                    stats: [],
                    total: 0
                }
            );

        const items = topFour.stats.map((stat, i) => {
            const percentage = getPercentage(stat.play, topFour.total);

            return (
                <div
                    className="column small-12"
                    key={i}
                    style={{ marginBottom: 15 }}
                >
                    <PercentageBar
                        label={getCountryName(stat.geo_country)}
                        value={percentage}
                        height={6}
                        small
                    />
                </div>
            );
        });

        const chartData = statsMusicGeo.data.map((obj) => {
            const plays = obj.play;
            const countryName = getCountryName(obj.geo_country);

            return [
                { v: countryName, f: '' },
                { v: plays, f: humanizeNumber(plays) },
                `<p style="white-space: nowrap; font-family: 'Open Sans'; font-size: 1rem; color: #000; text-align: center;font-weight: bold;    line-height: 1.2;">${humanizeNumber(
                    plays
                )} <span style="color: #666; font-weight: 600;">plays</span></p><p style="white-space: nowrap; font-family: 'Open Sans'; color: #ffa200; font-size: 0.7rem; text-align: center;">${countryName}</p>`
            ];
        });
        const chartDataHead = [
            'Country',
            { label: 'Play Count', id: 'playCount', type: 'number' },
            { type: 'string', role: 'tooltip', p: { html: true } }
        ];
        chartData.unshift(chartDataHead);

        const chartOptions = {
            colorAxis: {
                colors: ['#ffa200']
            },
            legend: 'none',
            tooltip: {
                isHtml: true
            }
        };

        let seeAllButton = (
            <p className="u-text-center" style={{ marginTop: 15 }}>
                <Link
                    to={`/stats/music/${musicItem.id}/countries`}
                    className="button button--pill button--med"
                >
                    See More Geo Stats
                </Link>
            </p>
        );

        let rightContent = <div className="row">{items}</div>;

        if (statsMusicGeo.error) {
            seeAllButton = null;
            rightContent = (
                <p>
                    There was an error loading your geographical plays. Please
                    try again later.
                </p>
            );
        }

        return (
            <section className="row u-spacing-bottom-50">
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Plays by Geo"
                        suffixText="(Last 7 days)"
                        icon={<MusicIcon />}
                    />
                    <div className="u-box-shadow u-padded u-pos-relative">
                        {this.getLoader(statsMusicGeo.loading)}
                        <div className="row">
                            <div className="column small-24 medium-12 u-spacing-bottom-20">
                                <GeoChart
                                    data={chartData}
                                    options={chartOptions}
                                />
                                {seeAllButton}
                            </div>
                            <div className="column small-24 medium-12">
                                {rightContent}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    renderPromotionalLinks() {
        const { statsMusicPromoLink, statsMusicSummary } = this.props;
        const { data, loading, error } = statsMusicPromoLink;

        let content;
        let emptyMessage;

        if (!data.length) {
            let message;

            if (!data.length && !error) {
                message =
                    "You haven't created any promotional links for this music yet.";
            }

            emptyMessage = this.renderEmpty(message);
        }

        if (data.length && statsMusicSummary.info) {
            const rows = data.map((promo, i) => {
                const relativeLink = `${getMusicUrl(
                    statsMusicSummary.info
                )}?key=${promo.promo_key}`;

                if (promo.isDeleting) {
                    return null;
                }

                return (
                    <tr key={i}>
                        <td>
                            <strong>{promo.promo_key}</strong>
                        </td>
                        <td className="flex-2">
                            <input
                                className="promo-link"
                                type="text"
                                readOnly
                                value={`${process.env.AM_URL}${relativeLink}`}
                            />
                        </td>
                        <td style={{ flex: '0 0 70px' }}>
                            <strong>{promo.play}</strong>
                        </td>
                        <td style={{ flex: '0 0 70px', paddingRight: 30 }}>
                            <button
                                type="button"
                                data-key={promo.key}
                                data-index={i}
                                data-tooltip="Delete Link"
                                onClick={this.props.onPromoLinkDeleteClick}
                            >
                                <TrashIcon width="16px" />
                            </button>
                        </td>
                    </tr>
                );
            });

            const headerCells = [
                'Name',
                {
                    value: 'Link',
                    props: {
                        className: 'flex-2'
                    }
                },
                {
                    value: 'Plays',
                    props: {
                        style: {
                            flex: '0 0 70px'
                        }
                    }
                },
                {
                    value: 'Actions',
                    props: {
                        style: {
                            flex: '0 0 70px',
                            paddingRight: 30
                        }
                    }
                }
            ];

            content = (
                <Table
                    className="dashboard-table table--block"
                    headerCells={headerCells}
                    bodyRows={rows}
                />
            );
        }

        return (
            <section className="row u-spacing-bottom-50">
                <div className="column small-24">
                    <DashboardModuleTitle
                        text="Promotional Links"
                        icon={<LinkIcon />}
                    />
                </div>
                <div className="column small-24">
                    <div className="u-box-shadow u-pos-relative">
                        {this.getLoader(loading)}
                        {content}
                        {emptyMessage}
                    </div>
                </div>
            </section>
        );
    }

    render() {
        const { info: musicItem } = this.props.statsMusicSummary;
        let title = 'Advanced Stats on Audiomack';

        if (musicItem) {
            title = `Advanced Stats for ${musicItem.title} on Audiomack`;
        }

        return (
            <Fragment>
                <Helmet>
                    <title>{title}</title>
                </Helmet>
                <div className="row" style={{ marginTop: 30 }}>
                    <div className="column">
                        <TunecoreAd
                            className="u-text-center u-spacing-top u-spacing-bottom"
                            linkHref="http://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_afpd_su&utm_content=ghoammot"
                            eventLabel={eventLabel.dashboard}
                            variant="condensed"
                        />
                    </div>
                </div>
                {this.renderMusicDetail()}
                {this.renderStatBlocks()}
                {this.renderAlbumTrackDetails()}
                {this.renderStatsSources()}
                {this.renderGeoMap()}
                {this.renderPlaylistAdds(musicItem)}
                {this.renderEmbeddedPlayers()}
                {this.renderPromotionalLinks()}
                <AmpEditCodesModal />
            </Fragment>
        );
    }
}
