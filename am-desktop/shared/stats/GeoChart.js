import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { loadScript } from 'utils/index';
import analytics from 'utils/analytics';

import styles from './GeoChart.module.scss';

export default class GeoChart extends Component {
    static propTypes = {
        data: PropTypes.array,
        options: PropTypes.object
    };

    static defaultProps = {
        // Uses arrayToDataTable for initialization
        // https://developers.google.com/chart/interactive/docs/reference#arraytodatatable
        data: [[]],
        options: {}
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        loadScript('https://www.gstatic.com/charts/loader.js', 'googlegstatic')
            .then(() => {
                // Load the Visualization API and the piechart package.
                window.google.load('visualization', '1.0', {
                    packages: ['geochart'],
                    mapsApiKey: `${process.env.GOOGLE_MAPS_API_KEY}`,
                    callback: () => this.initChart()
                });

                // Set a callback to run when the Google Visualization API is loaded.
                // window.google.setOnLoadCallback(() => {
                //     this.initChart();
                // });
                return;
            })
            .catch((err) => {
                console.log(err);
                analytics.error(err);
            });
    }

    componentDidUpdate(prevProps) {
        const currentData = this.props.data;
        const { data: prevData } = prevProps;
        const changedData =
            this._data &&
            JSON.stringify(prevData) !== JSON.stringify(currentData);

        if (changedData) {
            this._data.removeRows(0, this._data.getNumberOfRows());

            currentData.forEach((row, i) => {
                if (i === 0) {
                    row.forEach((field, k) => {
                        this._data.setColumnLabel(k, field);
                    });
                    return;
                }

                this._data.addRow(row);
            });

            this.draw();
        }
    }

    componentWillUnmount() {
        if (this._chart) {
            this._chart.clearChart();
        }

        this._chart = null;
        this._chartDiv = null;
        this._data = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleChartReady = () => {
        this.addSvgClass();
    };

    handleChartError = (id, message) => {
        console.error(id, message);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    initChart() {
        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        const { data } = this.props;

        this._data = window.google.visualization.arrayToDataTable(data);

        // Instantiate and draw our chart, passing in some options.
        this._chart = new window.google.visualization.GeoChart(this._chartDiv);

        this.draw();
    }

    addSvgClass() {
        const svg = document.querySelector(`.${styles.chart} svg`);
        svg.classList.add('u-no-fill', styles.loaded);
    }

    draw() {
        const { options } = this.props;

        this._chart.draw(this._data, options);

        window.google.visualization.events.addListener(
            this._chart,
            'ready',
            this.handleChartReady
        );
        window.google.visualization.events.addListener(
            this._chart,
            'error',
            this.handleChartError
        );
    }

    render() {
        return (
            <div
                id="chart"
                className={styles.chart}
                ref={(e) => (this._chartDiv = e)}
            />
        );
    }
}
