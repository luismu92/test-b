import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {
    // easeOutSine,
    humanizeNumber,
    formatCurrency,
    round
} from 'utils/index';

import AndroidLoader from '../loaders/AndroidLoader';
import styles from './StatBlock.module.scss';

export default class StatBlock extends Component {
    static propTypes = {
        text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        type: PropTypes.oneOf(['number', 'currency']),
        // totalSteps: PropTypes.number,
        valueFormatter: PropTypes.func,
        style: PropTypes.object,
        icon: PropTypes.element,
        loading: PropTypes.bool,
        action: PropTypes.string,
        onClick: PropTypes.func,
        iconClass: PropTypes.string,
        active: PropTypes.bool
    };

    static defaultProps = {
        value: 0,
        style: {},
        totalSteps: 50,
        valueFormatter(val) {
            return val;
        }
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            currentValue: props.value
        };
    }

    componentDidMount() {
        // this.spinDemNumbas(this.props.value, this.state.currentValue);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value) {
            // this.spinDemNumbas(prevProps.value, this.props.value);
        }
    }

    componentWillUnmount() {
        window.cancelAnimationFrame(this._raf);
        this._raf = null;
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    getFormatter() {
        const { type, valueFormatter } = this.props;

        switch (type) {
            case 'number':
                return this.formatPlaysNumber;

            case 'currency':
                return formatCurrency;

            default:
                return valueFormatter;
        }
    }

    formatPlaysNumber(val) {
        const formatted = round(val);

        return humanizeNumber(formatted);
    }

    /*
    spinDemNumbas(startValue, endValue) {
        window.cancelAnimationFrame(this._raf);

        this.spin(startValue, endValue);
    }

    spin(startValue, endValue, currentValue) {
        const { totalSteps } = this.props;
        const increment = (endValue - startValue) / totalSteps;
        const current =
            typeof currentValue === 'undefined' ? startValue : currentValue;
        const nextValue = current + increment;
        let shouldStop = nextValue >= endValue;
        const valueFormatter = this.getFormatter();

        if (endValue < startValue) {
            shouldStop = nextValue <= endValue;
        }

        if (shouldStop) {
            const finalValue = valueFormatter(endValue);

            this._value.textContent = finalValue;

            this.setState({
                currentValue: endValue
            });
            return;
        }

        this._raf = window.requestAnimationFrame(() => {
            if (!this._value) {
                return;
            }

            let progress = nextValue / endValue;
            let totalValue = endValue;

            if (endValue < startValue) {
                progress = nextValue / startValue;
                totalValue = startValue;
            }

            // This doesnt really give the effect I wanted but ill leave
            // here for now.
            const modifier = easeOutSine(progress);

            this._value.textContent = valueFormatter(totalValue * modifier);

            this.spin(startValue, endValue, nextValue);
        });
    }
    */

    renderIcon(icon, loading) {
        const wrapClass = classnames(styles.iconWrap, 'u-text-center');
        const klass = classnames(styles.icon, styles[this.props.iconClass]);

        return (
            <div className={wrapClass}>
                <span className={klass}>
                    {loading ? <AndroidLoader size={20} center /> : icon}
                </span>
            </div>
        );
    }

    render() {
        const {
            text,
            icon,
            loading,
            action,
            onClick,
            style,
            active
        } = this.props;
        const klass = classnames(styles.block, {
            [styles.active]: active,
            [styles.button]: !!onClick
        });
        const valueFormatter = this.getFormatter();

        let tag = 'div';
        const buttonProps = {
            className: klass,
            'data-action': action,
            style: style
        };

        if (onClick) {
            tag = 'button';
            buttonProps.onClick = onClick;
        }

        let value;

        if (action !== 'listener') {
            value = valueFormatter(this.props.value);
        }

        return React.createElement(
            tag,
            buttonProps,
            <Fragment>
                {this.renderIcon(icon, loading)}
                <div className={styles.count}>
                    <p
                        className={styles.countNumber}
                        ref={(c) => (this._value = c)}
                    >
                        {value}
                    </p>
                    <p className={styles.countText}>{text}</p>
                </div>
            </Fragment>
        );
    }
}
