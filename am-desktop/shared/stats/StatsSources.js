import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { humanizeNumber, getPercentage } from 'utils/index';

import AndroidLoader from '../loaders/AndroidLoader';
import PercentageBar from '../components/PercentageBar';

function StatsSources({ dataSet, limit, musicItem }) {
    const { data: list, loading, error } = dataSet;

    let content;
    let emptyMessage;

    if (!list.length) {
        let message;

        if (!list.length && !error && !loading) {
            message = 'Sources will be available when you get your first play';
        }

        if (error) {
            message =
                'There was an error loading your play sources. Please try again later.';
        }

        emptyMessage = <p className="u-text-center u-padded">{message}</p>;
    }

    let sourceData = list;

    // Slice the data array rather than set a limit in the API request
    // so that the percentages match across preview and full lists
    if (limit) {
        sourceData = list.slice(0, limit);
    }

    if (list.length) {
        const formatted = list.reduce(
            (acc, source) => {
                const playData = source.play_song
                    ? source.play_song
                    : source.play;

                if (playData) {
                    acc.total += playData;
                    acc.list.push(source);
                }

                return acc;
            },
            {
                total: 0,
                list: []
            }
        );

        content = sourceData.map((source, i) => {
            const playData = source.play_song ? source.play_song : source.play;
            const percentage = getPercentage(playData, formatted.total);
            const percentageValue = percentage < 1 ? '< 1' : percentage;

            let subLabel = null;

            if (playData) {
                const suffix = playData === 1 ? 'play' : 'plays';
                subLabel = `${humanizeNumber(playData)} ${suffix}`;
            }

            let drilldownLink;

            if (musicItem && source.section === 'Playlists') {
                drilldownLink = `/stats/music/${musicItem.id}/playlists`;
            }

            if (musicItem && source.section === 'Embed') {
                drilldownLink = `/stats/music/${musicItem.id}/embeds`;
            }

            return (
                <div
                    className="column small-24"
                    key={`${i}-${playData}`}
                    style={{ marginBottom: 32 }}
                >
                    <PercentageBar
                        label={source.section}
                        subLabel={subLabel}
                        value={percentageValue}
                        height={10}
                        labelLink={drilldownLink}
                    />
                </div>
            );
        });
    }

    let loader;

    if (loading) {
        loader = (
            <div className="u-overlay-loader">
                <AndroidLoader />
            </div>
        );
    }

    return (
        <Fragment>
            {loader}
            {content}
            {emptyMessage}
        </Fragment>
    );
}

StatsSources.propTypes = {
    dataSet: PropTypes.object,
    limit: PropTypes.number,
    musicItem: PropTypes.object
};

export default StatsSources;
