import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';
import requireAuth from '../hoc/requireAuth';

import { queryForTimespan, queryToday } from 'utils/stats';

import { getSummary } from '../redux/modules/stats/music/summary';
import {
    getPlaylistAdds,
    clearList as clearPlaylistList,
    nextPage as nextPlaylistsPage
} from '../redux/modules/stats/music/playlistAdds';
import {
    getCountryStats,
    clearList as clearCountriesList,
    nextPage as nextcountriesPage
} from '../redux/modules/stats/music/countries';
import {
    getCityStats,
    clearList as clearCitiesList,
    nextPage as nextCitiesPage
} from '../redux/modules/stats/music/cities';
import {
    getPlaySources,
    clearList as clearSourcesList
} from '../redux/modules/stats/music/playSource';
import { getUrls } from '../redux/modules/stats/music/url';

import MusicStatsPlaylists from './MusicStatsPlaylists';
import StatsGeo from '../dashboard/StatsGeo';
import StatsSources from './StatsSources';
import DashboardModuleTitle from '../dashboard/DashboardModuleTitle';
import DashboardBackLink from '../dashboard/DashboardBackLink';
import EmbedStats from './EmbedStats';

import LinkIcon from 'icons/link';
import EmbedIcon from '../icons/embed-close';

const GEO_FETCH_LIMIT = 10;
const PLAYLIST_FETCH_LIMIT = 10;

function makeContextRequest(params, currentUser) {
    const musicId = params.musicId;
    const context = params.context;

    let fn = () => null; //eslint-disable-line

    switch (context) {
        case 'playlists':
            fn = () =>
                getPlaylistAdds(currentUser.profile.id, musicId, {
                    limit: PLAYLIST_FETCH_LIMIT
                });
            break;

        case 'countries':
            fn = () =>
                getCountryStats(currentUser.profile.id, musicId, {
                    startDate: queryForTimespan('month'),
                    endDate: queryToday(),
                    limit: GEO_FETCH_LIMIT
                });
            break;

        case 'cities':
            fn = () =>
                getCityStats(currentUser.profile.id, musicId, {
                    startDate: queryForTimespan('month'),
                    endDate: queryToday(),
                    limit: GEO_FETCH_LIMIT
                });
            break;

        case 'play-sources':
            fn = () =>
                getPlaySources(currentUser.profile.id, musicId, {
                    startDate: queryForTimespan('week'),
                    endDate: queryToday()
                });
            break;

        case 'embeds':
            fn = () => getUrls(currentUser.profile.id, musicId);
            break;

        default:
            break;
    }

    return fn();
}

class MusicStatsDetailContainer extends Component {
    static propTypes = {
        match: PropTypes.object,
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        statsMusicSummary: PropTypes.object,
        statsMusicCountries: PropTypes.object,
        statsMusicCities: PropTypes.object,
        statsMusicPlaylistAdds: PropTypes.object,
        statsMusicPlaySource: PropTypes.object,
        statsMusicUrl: PropTypes.object
    };

    constructor(props) {
        super(props);

        const { match } = this.props;

        this.state = {
            activeContext: match.params.context
        };
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(clearPlaylistList());
        dispatch(clearCountriesList());
        dispatch(clearCitiesList());
        dispatch(clearSourcesList());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoadMoreClick = (e) => {
        const { dispatch, currentUser, match } = this.props;

        const button = e.currentTarget;
        const action = button.getAttribute('data-action');
        const page = parseInt(button.getAttribute('data-page'), 10) || 1;

        const musicId = match.params.musicId;

        switch (action) {
            case 'playlists':
                dispatch(
                    getPlaylistAdds(currentUser.profile.id, musicId, {
                        limit: PLAYLIST_FETCH_LIMIT,
                        page: page + 1
                    })
                );
                dispatch(nextPlaylistsPage());
                break;

            case 'countries':
                dispatch(
                    getCountryStats(currentUser.profile.id, musicId, {
                        startDate: queryForTimespan('month'),
                        endDate: queryToday(),
                        limit: GEO_FETCH_LIMIT,
                        page: page + 1
                    })
                );
                dispatch(nextcountriesPage());
                break;

            case 'cities':
                dispatch(
                    getCityStats(currentUser.profile.id, musicId, {
                        startDate: queryForTimespan('month'),
                        endDate: queryToday(),
                        limit: GEO_FETCH_LIMIT,
                        page: page + 1
                    })
                );
                dispatch(nextCitiesPage());
                break;
            default:
                break;
        }
    };

    handleContextButtonClick = (e) => {
        const button = e.currentTarget;
        const context = button.getAttribute('data-context');

        this.setState({
            activeContext: context
        });
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getDataFromContext(context) {
        const { statsMusicCities, statsMusicCountries } = this.props;

        if (context === 'cities') {
            return statsMusicCities;
        }

        return statsMusicCountries;
    }

    refetchIfNecessary(currentProps, nextProps) {
        const { currentUser, dispatch } = this.props;
        const { params } = currentProps.match;
        const { params: nextParams } = nextProps.match;

        const changedPage = params.context !== nextParams.context;

        if (changedPage) {
            dispatch(makeContextRequest(nextParams, currentUser));

            this.setState({
                activeContext: nextParams.context
            });
        }
    }

    renderContent(params) {
        const { statsMusicSummary } = this.props;
        const { info: musicItem } = statsMusicSummary;
        const context = params.context;
        let content;

        const dataSet =
            context === 'countries'
                ? this.props.statsMusicCountries
                : this.props.statsMusicCities;
        let backLink;

        if (musicItem && musicItem.type) {
            backLink = (
                <div className="row">
                    <DashboardBackLink
                        href={`/stats/music/${musicItem.id}`}
                        text={`Back to Advanced Stats for ${musicItem.title}`}
                    />
                </div>
            );
        }

        switch (context) {
            case 'playlists':
                content = (
                    <MusicStatsPlaylists
                        summary={this.props.statsMusicSummary}
                        playlists={this.props.statsMusicPlaylistAdds}
                        onLoadMoreClick={this.handleLoadMoreClick}
                    />
                );
                break;

            case 'countries':
            case 'cities': {
                content = (
                    <StatsGeo
                        dataSet={dataSet}
                        onContextButtonClick={this.handleContextButtonClick}
                        activeContext={this.state.activeContext}
                        onLoadMoreStatsClick={this.handleLoadMoreClick}
                        contextButtonUrlPrefix={`/stats/music/${
                            params.musicId
                        }`}
                    />
                );
                break;
            }

            case 'play-sources':
                content = (
                    <div className="row u-spacing-top">
                        <div className="column small-24">
                            <DashboardModuleTitle
                                text="Play Sources"
                                suffixText="(Last 7 Days)"
                                icon={<LinkIcon />}
                            />
                        </div>
                        <div className="column small-24">
                            <div className="u-box-shadow u-pos-relative u-padded">
                                <StatsSources
                                    dataSet={this.props.statsMusicPlaySource}
                                    musicItem={musicItem}
                                />
                            </div>
                        </div>
                    </div>
                );
                break;

            case 'embeds':
                content = (
                    <div className="row u-spacing-top">
                        <div className="column small-24">
                            <DashboardModuleTitle
                                text="Embeds"
                                suffixText="(Last 7 days)"
                                icon={<EmbedIcon />}
                            />
                            <div className="column small-24">
                                <EmbedStats data={this.props.statsMusicUrl} />
                            </div>
                        </div>
                    </div>
                );
                break;

            default:
                break;
        }

        return (
            <Fragment>
                {backLink}
                {content}
            </Fragment>
        );
    }

    render() {
        const { match } = this.props;

        return this.renderContent(match.params);
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        statsMusicSummary: state.statsMusicSummary,
        statsMusicCountries: state.statsMusicCountries,
        statsMusicCities: state.statsMusicCities,
        statsMusicPlaylistAdds: state.statsMusicPlaylistAdds,
        statsMusicUrl: state.statsMusicUrl,
        statsMusicPlaySource: state.statsMusicPlaySource
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(MusicStatsDetailContainer, [
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return getSummary(props.currentUser.profile.id, params.musicId, {
                startDate: queryForTimespan('month'),
                endDate: queryToday()
            });
        },
        (params, query, props) => {
            const { currentUser } = props;

            if (!currentUser.isLoggedIn) {
                return null;
            }

            return makeContextRequest(params, currentUser);
        }
    ])
);
