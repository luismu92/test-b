import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
    getDynamicImageProps,
    humanizeNumber,
    getArtistName,
    getMusicUrl,
    getArtistUrl
} from 'utils/index';

import DashboardModuleTitle from '../dashboard/DashboardModuleTitle';
import Table from '../components/Table';
import InfoTooltip from '../components/InfoTooltip';
import AndroidLoader from 'components/loaders/AndroidLoader';

import ExternalIcon from '../icons/external';
import PlaylistIcon from '../icons/playlist';
import PlayIcon from '../icons/play';

function MusicStatsPlaylists({ summary, playlists, onLoadMoreClick }) {
    const { data, onLastPage } = playlists;
    const { info } = summary;
    const loading = playlists.loading || summary.loading;

    const headerCells = [
        {
            value: 'Playlist Name',
            props: {
                className: 'flex-3'
            }
        },
        {
            value: 'Creator',
            props: {
                className: 'flex-3'
            }
        },
        {
            value: 'Plays',
            props: {
                className: 'u-text-center'
            }
        },
        {
            value: 'Actions',
            props: {
                className: 'flex-half u-text-center'
            }
        }
    ];
    const imageSize = 30;
    const rows = data.map((playlist, i) => {
        let title = 'Unavailable';
        let artist = '–';
        let link = <InfoTooltip text="This playlist does not exist anymore." />;

        const [src, srcSet] = getDynamicImageProps(playlist.image_base, {
            size: imageSize
        });

        const image = (
            <div
                style={{
                    width: imageSize,
                    height: imageSize,
                    overflow: 'hidden',
                    marginRight: 15
                }}
            >
                <img src={src} srcSet={srcSet} alt="" />
            </div>
        );

        title = playlist.title;

        const fauxPlaylist = {
            type: 'playlist',
            artist: {
                name: playlist.artist,
                url_slug: playlist.artist_url_slug
            },
            url_slug: playlist.url_slug
        };

        artist = getArtistName(fauxPlaylist);

        link = (
            <Link to={getMusicUrl(fauxPlaylist)} title="View playlist">
                <ExternalIcon className="u-d-block" style={{ width: 12 }} />
            </Link>
        );

        return (
            <tr key={i}>
                <td className="flex-3 u-fw-600">
                    {image}
                    {title}
                </td>
                <td className="flex-3 u-fw-600">
                    <Link to={getArtistUrl(fauxPlaylist)} title="View artist">
                        {artist}
                    </Link>
                </td>
                <td
                    className="medium-center"
                    style={{ fontWeight: 600 }}
                    data-th="Plays"
                >
                    {humanizeNumber(playlist.play)}
                </td>
                <td className="medium-center flex-half" data-th="Actions">
                    {link}
                </td>
            </tr>
        );
    });

    const suffix = (
        <p style={{ color: '#ffa200', fontWeight: 600 }}>{info.title}</p>
    );

    let totalCount;

    if (info.stats) {
        totalCount = (
            <p className="u-ls-n-05">
                <span
                    className="u-d-inline-block u-spacing-right-5 u-text-orange"
                    style={{ width: 10 }}
                >
                    <PlayIcon />
                </span>
                <strong>
                    Total Plays:{' '}
                    <span className="u-text-orange">
                        {humanizeNumber(info.stats['plays-raw'])}
                    </span>
                </strong>
            </p>
        );
    }

    let button;

    if (!onLoadMoreClick && data.length) {
        button = (
            <Link
                to={`/stats/music/${info.id}/playlists`}
                className="button button--med"
            >
                See All Playlists
            </Link>
        );
    }

    if (onLoadMoreClick && !onLastPage && data.length) {
        button = (
            <button
                onClick={onLoadMoreClick}
                data-action="playlists"
                className="button button--med"
                data-page={playlists.page}
            >
                Load More
            </button>
        );
    }

    let loader;

    if (loading) {
        loader = (
            <div className="u-text-center">
                <AndroidLoader />
            </div>
        );
    }

    const emptyText = (
        <p className="u-text-center u-padded">
            Sorry, there doesn't seem to be anything here. Please try again
            later.
        </p>
    );
    const table = (
        <Table
            className="table table--flex stats-table"
            headerCells={headerCells}
            bodyRows={rows}
        />
    );

    const content = data.length || loading ? table : emptyText;

    return (
        <div className="row u-spacing-top">
            <div className="column small-24">
                <DashboardModuleTitle
                    text="Playlists with"
                    suffixText={suffix}
                    icon={<PlaylistIcon />}
                    children={totalCount}
                    spaceBetween
                />
            </div>
            <div className="column small-24">
                <div className="u-box-shadow" style={{ padding: '10px 20px' }}>
                    {content}
                    {loader}
                </div>
                <div className="u-text-center u-spacing-top-30">{button}</div>
            </div>
        </div>
    );
}

MusicStatsPlaylists.propTypes = {
    summary: PropTypes.object,
    playlists: PropTypes.object,
    onLoadMoreClick: PropTypes.func
};

export default MusicStatsPlaylists;
