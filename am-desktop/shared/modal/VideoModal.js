import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { MODAL_TYPE_VIDEO } from '../redux/modules/modal';

import SongVideo from '../song/SongVideo';

import Modal from './Modal';

import MusicIcon from '../../../am-shared/icons/music';

import styles from './VideoModal.module.scss';

export default class VideoModal extends Component {
    static propTypes = {
        item: PropTypes.object,
        onClose: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            background: true,
            color: false,
            showColorPicker: false,
            colorString: '#ff0000'
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentWillUnmount() {
        this._attachedBodyListener = null;
        this.removeBodyListener();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBodyClick = (e) => {
        const target = e.target;

        if (
            this._swatchContainer.contains(target) ||
            target === this._swatchButton
        ) {
            return;
        }

        this.setState({
            showColorPicker: false
        });

        this.removeBodyListener();
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;

        document.body.removeEventListener('click', this.handleBodyClick);
    }

    render() {
        const { item } = this.props;

        if (!item) {
            return null;
        }

        const icon = (
            <MusicIcon className={classnames(styles.icon, styles.color)} />
        );

        return (
            <Modal
                modalType={MODAL_TYPE_VIDEO}
                onClose={this.props.onClose}
                title={item.title}
                titleIcon={icon}
            >
                <SongVideo song={item} />
            </Modal>
        );
    }
}
