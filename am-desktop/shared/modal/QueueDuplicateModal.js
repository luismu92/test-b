import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_QUEUE_DUPLICATE, hideModal } from '../redux/modules/modal';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './ConfirmModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

class QueueDuplicateModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        item: PropTypes.object,
        onClose: PropTypes.func,
        onConfirmClick: PropTypes.func
    };

    static defaultProps = {
        item: {},
        onConfirmClick() {}
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleConfirmClick = (e) => {
        const { dispatch } = this.props;

        dispatch(hideModal());

        this.props.onConfirmClick(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <Modal
                className={styles.variant.modal}
                modalType={MODAL_TYPE_QUEUE_DUPLICATE}
                onClose={this.props.onClose}
                title="Duplicate queue item"
            >
                <div className="row">
                    <div className="column">
                        <p className={styles.base.message}>
                            You already have “{this.props.item.title}” added to
                            the queue. Are you sure you want to add it again?
                        </p>
                        <div
                            className={classnames(
                                'button-group',
                                'button-group--vert',
                                styles.variant.buttonGroup
                            )}
                        >
                            <div>
                                <button
                                    className="button button--pill button--wide"
                                    onClick={this.handleConfirmClick}
                                    type="button"
                                >
                                    Add to queue anyway
                                </button>
                            </div>
                            <div>
                                <button
                                    className="button button--pill button--transparent button--wide"
                                    onClick={this.handleCancelClick}
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        type: state.modal.extraData.type,
        item: state.modal.extraData.item,
        onConfirmClick: state.modal.extraData.onConfirmClick,
        onReplaceClick: state.modal.extraData.onReplaceClick
    };
}

export default compose(connect(mapStateToProps))(QueueDuplicateModal);
