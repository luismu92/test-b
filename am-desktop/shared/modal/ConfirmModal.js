import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { MODAL_TYPE_CONFIRM, hideModal } from '../redux/modules/modal';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './ConfirmModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

class ConfirmModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        onConfirm: PropTypes.func,
        onCancel: PropTypes.func,
        disableOnConfirm: PropTypes.bool,
        title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        confirmingText: PropTypes.string,
        confirmButtonText: PropTypes.string,
        modalType: PropTypes.string,
        message: PropTypes.string,
        confirmButtonProps: PropTypes.object
    };

    static defaultProps = {
        onConfirm() {},
        disableOnConfirm: true,
        confirmButtonText: 'OK',
        confirmButtonProps: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            disabled: false
        };
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.modalType === MODAL_TYPE_CONFIRM &&
            this.props.modalType !== prevProps.modalType
        ) {
            this.reenableConfirmButton();
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleConfirmClick = (e) => {
        if (this.props.disableOnConfirm) {
            this.setState({
                disabled: true
            });
        }

        this.props.onConfirm(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    reenableConfirmButton() {
        this.setState({
            disabled: false
        });
    }

    render() {
        const {
            title,
            message,
            confirmButtonText,
            confirmButtonProps,
            confirmingText
        } = this.props;

        let buttonText = confirmButtonText;

        if (this.state.disabled && confirmingText) {
            buttonText = confirmingText;
        }

        return (
            <Modal
                className={styles.variant.modal}
                modalType={MODAL_TYPE_CONFIRM}
                onClose={this.props.onClose}
                title={title}
            >
                <div className="row">
                    <div className="column">
                        <p
                            className={styles.base.message}
                            dangerouslySetInnerHTML={{ __html: message }}
                        />
                        <div className="button-group button-group--vert u-text-center">
                            <div>
                                <button
                                    className="button button--pill button--wide"
                                    onClick={this.handleConfirmClick}
                                    type="button"
                                    disabled={this.state.disabled}
                                    {...confirmButtonProps}
                                >
                                    {buttonText}
                                </button>
                            </div>
                            <div>
                                <button
                                    className="button button--pill button--transparent button--wide"
                                    onClick={this.handleCancelClick}
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modalType: state.modal.type,
        title: state.modal.extraData.title,
        message: state.modal.extraData.message,
        onConfirm: state.modal.extraData.handleConfirm,
        confirmingText: state.modal.extraData.confirmingText,
        confirmButtonText: state.modal.extraData.confirmButtonText,
        confirmButtonProps: state.modal.extraData.confirmButtonProps
    };
}

export default compose(connect(mapStateToProps))(ConfirmModal);
