import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { MODAL_TYPE_VALIDATE_ARTIST, hideModal } from '../redux/modules/modal';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './ValidateArtistModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

class ValidateArtistModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        currentUser: PropTypes.object,
        onConfirm: PropTypes.func,
        disableOnConfirm: PropTypes.bool,
        title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        modalType: PropTypes.string
    };

    static defaultProps = {
        onConfirm() {},
        disableOnConfirm: true,
        confirmButtonProps: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            disabled: false
        };
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.modalType === MODAL_TYPE_VALIDATE_ARTIST &&
            this.props.modalType !== prevProps.modalType
        ) {
            this.reenableConfirmButton();
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleConfirmClick = (e) => {
        if (this.props.disableOnConfirm) {
            this.setState({
                disabled: true
            });
        }

        this.props.onConfirm(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    reenableConfirmButton() {
        this.setState({
            disabled: false
        });
    }

    render() {
        const { title, currentUser } = this.props;
        const plays = currentUser.profile.stats['plays-raw'];
        const uploads = currentUser.profile.upload_count_excluding_reups;
        const nameArtist = currentUser.profile.name;
        const urlSlug = currentUser.profile.url_slug;
        const profileImg = currentUser.profile.images.profile.filename;

        return (
            <Modal
                className={styles.variant.modalValidateArtist}
                modalType={MODAL_TYPE_VALIDATE_ARTIST}
                onClose={this.props.onClose}
                title={title}
            >
                <div className="row">
                    <div className="u-text-center">
                        <div className={styles.variant.sorryArtistName}>
                            {`Sorry, ${nameArtist}. You don't meet the requirements to apply for authentication.`}
                        </div>
                        <div>
                            <img src={profileImg} alt="" />
                        </div>
                        <div className={styles.variant.divContentCenter}>
                            <div className={styles.variant.nameArtist}>
                                {nameArtist}
                            </div>
                            <p className={styles.variant.socialNick}>
                                {`@${urlSlug}`}
                            </p>
                        </div>
                        <div className={`${styles.variant.listContainer} row`}>
                            <div className="column">
                                <div
                                    className={`${styles.variant.titleList} ${
                                        styles.variant.titleLeft
                                    }`}
                                >
                                    You must have:
                                </div>
                                <ul className={styles.variant.textList}>
                                    <li>
                                        <span>
                                            {`At least ${parseInt(
                                                process.env
                                                    .ARTIST_VALIDATION_REQUIRED_MUSIC_UPLOADS,
                                                10
                                            )} uploads`}
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            {`${parseInt(
                                                process.env
                                                    .ARTIST_VALIDATION_REQUIRED_TOTAL_PLAYS,
                                                10
                                            )} total plays`}
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div className="column">
                                <div className={styles.variant.titleList}>
                                    You currently have:
                                </div>
                                <ul className={styles.variant.textList}>
                                    <li>
                                        <span>{`${uploads} Uploads`}</span>
                                    </li>
                                    <li>
                                        <span>{`${plays} plays`}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <p className={styles.variant.followBeginner}>
                            {'Follow our '}
                            <a
                                href="https://audiomack.com/world/post/how-to-pitch-your-music-for-trending---playlists"
                                target="_blank"
                            >
                                Beginner's Guide
                            </a>
                            {
                                ' on how to pitch your music for trending and playlists.'
                            }
                        </p>
                        <button
                            className={styles.variant.buttonValidate}
                            onClick={this.handleConfirmClick}
                            type="button"
                            disabled={this.state.disabled}
                        >
                            OK
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modalType: state.modal.type,
        title: state.modal.extraData.title,
        message: state.modal.extraData.message,
        onConfirm: state.modal.extraData.handleConfirm,
        confirmingText: state.modal.extraData.confirmingText,
        confirmButtonText: state.modal.extraData.confirmButtonText,
        confirmButtonProps: state.modal.extraData.confirmButtonProps,
        currentUser: state.currentUser
    };
}

export default compose(connect(mapStateToProps))(ValidateArtistModal);
