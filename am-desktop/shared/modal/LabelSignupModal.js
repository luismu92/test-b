import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { MODAL_TYPE_LABEL_SIGNUP, hideModal } from '../redux/modules/modal';
import LabelSignupContainer from '../monetization/LabelSignupContainer';
import Modal from './Modal';

class LabelSignupModal extends Component {
    static propTypes = {
        inviteCode: PropTypes.string,
        dispatch: PropTypes.func,
        history: PropTypes.object,
        location: PropTypes.object,
        currentUser: PropTypes.object,
        modal: PropTypes.object,
        onFormSubmit: PropTypes.func
    };

    ////////////////////
    // Event handelrs //
    ////////////////////

    handleModalClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { inviteCode } = this.props;

        return (
            <Modal
                modalType={MODAL_TYPE_LABEL_SIGNUP}
                showOverlay
                onClose={this.handleModalClose}
                overlayTransitionLeaveTimeout={300}
                modalTransitionEnterTimeout={300}
                title="Activate Monetization"
            >
                <LabelSignupContainer
                    onFormSubmit={this.handleModalClose}
                    inviteCode={inviteCode}
                    shouldUpdate
                />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(LabelSignupModal);
