import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { getMusicUrl } from 'utils/index';
import { allGenresMap } from 'constants/index';

import { MODAL_TYPE_SAVE_TO_PLAYLIST, hideModal } from '../redux/modules/modal';
import { createPlaylist } from '../redux/modules/playlist';
import { addToast } from '../redux/modules/toastNotification';

import Select from '../components/Select';
import PlaylistIcon from '../icons/playlist';

import Modal from './Modal';
import styles from './NewPlaylistModal.module.scss';

class NewPlaylistModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        songList: PropTypes.array,
        currentUser: PropTypes.object,
        playlist: PropTypes.object
    };

    static defaultProps = {
        songList: []
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, songList } = this.props;
        const form = e.currentTarget;
        const title = form.title.value;
        const genre = form.genre.value;
        const description = form.description.value;
        const isPrivate = form.private.value;
        const options = {
            title,
            genre,
            description,
            private: isPrivate,
            id: songList.map((s) => s.id).join(',')
        };

        dispatch(createPlaylist(options))
            .then((action) => {
                dispatch(hideModal());
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Your playlist was successfully created!',
                        link: getMusicUrl(action.resolved)
                    })
                );
                return;
            })
            .catch((err) => {
                dispatch(
                    addToast({
                        type: 'warning',
                        action: 'message',
                        item: `There was a problem creating your playlist:\n${err}`
                    })
                );
                console.error(err);
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { currentUser, playlist } = this.props;
        const { loading } = playlist;
        const options = Object.keys(allGenresMap).map((key) => {
            return {
                value: key,
                text: allGenresMap[key]
            };
        });

        const icon = <PlaylistIcon className="u-text-icon u-text-orange" />;

        let plural = 'Songs';

        if (this.props.songList.length === 1) {
            plural = 'Song';
        }

        let defaultGenre = '';

        if (currentUser.isLoggedIn && currentUser.profile.genre) {
            defaultGenre = currentUser.profile.genre;
        }

        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_SAVE_TO_PLAYLIST}
                onClose={this.handleClose}
                title="Create a new Playlist"
                titleIcon={icon}
            >
                <form
                    className={`${styles.form} row`}
                    onSubmit={this.handleFormSubmit}
                >
                    <p
                        className={classnames(
                            'column',
                            'small-24',
                            'u-spacing-bottom-em',
                            styles.formHeader
                        )}
                    >
                        You’re about to add{' '}
                        <span className={styles.color}>
                            {this.props.songList.length} {plural}
                        </span>{' '}
                        to a playlist.
                    </p>
                    <div className="column small-14">
                        <label
                            className="label--required"
                            htmlFor="playlist-title"
                        >
                            Name:
                        </label>
                        <input
                            id="playlist-title"
                            type="text"
                            name="title"
                            placeholder="The perfect playlist"
                            defaultValue={new Date().toUTCString()}
                            required
                        />
                    </div>
                    <div className="column small-10">
                        <label
                            className="label--required"
                            htmlFor="playlist-genre"
                        >
                            Genre:
                        </label>
                        <Select
                            name="genre"
                            defaultValue={defaultGenre}
                            id="playlist-genre"
                            options={options}
                            selectProps={{
                                required: true
                            }}
                        />
                    </div>
                    <div className="column small-24 u-spacing-top-20 u-spacing-bottom-10">
                        <label htmlFor="playlist-description">
                            Playlist Description:
                        </label>
                        <textarea
                            id="playlist-description"
                            name="description"
                            placeholder="This playlist is the perfect mix of..."
                        />
                    </div>
                    <div className="column small-24 u-text-center">
                        <label
                            className={classnames(
                                styles.label,
                                'u-spacing-right-30'
                            )}
                        >
                            <input
                                type="radio"
                                className="radio--small u-margin-0"
                                name="private"
                                value="no"
                                defaultChecked
                            />{' '}
                            <span className={styles.labelText}>
                                Public - Anyone can view
                            </span>
                        </label>
                        <label
                            className={classnames(
                                styles.label,
                                'u-spacing-right-em'
                            )}
                        >
                            <input
                                type="radio"
                                className="radio--small u-margin-0"
                                name="private"
                                value="yes"
                            />{' '}
                            <span className={styles.labelText}>
                                Private - Only you can view
                            </span>
                        </label>
                    </div>
                    <p
                        className={classnames(
                            'column',
                            'small-24',
                            styles.formSubmit,
                            'u-spacing-top-30',
                            'u-spacing-bottom-20'
                        )}
                    >
                        <input
                            type="submit"
                            value={
                                loading ? 'Creating...' : 'Create My Playlist'
                            }
                            className="button button--pill button--large u-fs-14"
                            disabled={loading}
                        />
                    </p>
                </form>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        songList: state.modal.extraData.songList,
        currentUser: state.currentUser,
        playlist: state.playlist
    };
}

export default connect(mapStateToProps)(NewPlaylistModal);
