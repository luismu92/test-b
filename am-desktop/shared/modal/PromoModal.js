import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PromoKeyContainer from '../components/PromoKeyContainer';

import Modal from './Modal';
import { MODAL_TYPE_PROMO } from '../redux/modules/modal';
import { compose } from 'redux';
import { connect } from 'react-redux';

class PromoModal extends Component {
    static propTypes = {
        music: PropTypes.object.isRequired,
        onClose: PropTypes.func,
        modal: PropTypes.object
    };

    render() {
        const music = this.props.modal.extraData.music || {};

        return (
            <Modal
                modalType={MODAL_TYPE_PROMO}
                onClose={this.props.onClose}
                title="Create Promotional Link"
            >
                <div className="column small-24">
                    <PromoKeyContainer music={music} />
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(PromoModal);
