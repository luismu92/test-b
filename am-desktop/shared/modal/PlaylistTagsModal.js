import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_TAGS } from '../redux/modules/modal';

import Modal from './Modal';
import EditMusicTags from '../components/EditMusicTags';

import styles from './TrendingModal.module.scss';

class PlaylistTagsModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        tagData: PropTypes.object,
        onSubmit: PropTypes.func
    };

    static defaultProps = {
        tagData: {}
    };

    createCheckbox = (tag, index) => {
        const key = `${index}-${tag}`;

        return (
            <tr key={key}>
                <td>
                    <label>
                        <input
                            type="checkbox"
                            defaultChecked={this.props.tagData.resolved[tag]}
                            value={tag}
                        />
                    </label>
                </td>
                <td>{tag}</td>
            </tr>
        );
    };

    createTrendingCheckboxes = (items) => {
        if (items) {
            return Object.keys(items).map(this.createCheckbox);
        }
        return false;
    };

    render() {
        const { onClose, tagData } = this.props;

        return (
            <Modal modalType={MODAL_TYPE_TAGS} onClose={onClose}>
                <div className="row u-padding-top-30">
                    <div
                        className={`${styles.column} column small-24 medium-12`}
                    >
                        <h3
                            className={classnames(
                                styles.columnHeader,
                                'u-spacing-bottom-30'
                            )}
                        >
                            Category Tags
                        </h3>
                        <div className="button-group">
                            <form
                                className="form"
                                onSubmit={this.props.onSubmit}
                            >
                                <table className={styles.table}>
                                    <tbody className={styles.tableBody}>
                                        {this.createTrendingCheckboxes(
                                            tagData.resolved
                                        )}
                                    </tbody>
                                </table>
                                <input
                                    type="submit"
                                    className={`${
                                        styles.button
                                    } button u-spacing-top-20`}
                                    value="Update Tags"
                                />
                            </form>
                        </div>
                    </div>
                    <div
                        className={`${styles.column} column small-24 medium-12`}
                    >
                        <EditMusicTags />
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: state.modal.extraData.title,
        tagData: state.modal.extraData.tagData,
        onSubmit: state.modal.extraData.handleTagsSubmit
    };
}

export default compose(connect(mapStateToProps))(PlaylistTagsModal);
