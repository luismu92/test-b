import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import {
    MODAL_TYPE_CONFIRM,
    MODAL_TYPE_SEND_CLAIM_PROFILE_EMAIL,
    hideModal,
    showModal
} from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import { sendClaimProfileEmail } from '../redux/modules/monetization/associatedArtists';

import Modal from './Modal';
import styles from './ConfirmModal.module.scss';

class SendClaimProfileEmailModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        artistId: PropTypes.number,
        artistName: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            email: ''
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleInputChange = (e) => {
        this.setState({
            email: e.currentTarget.value
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, artistId, artistName } = this.props;
        const email = this.state.email;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Are you sure?',
                message: `Are you sure to send claim code to ${email} for Artist ${artistName}`,
                artistId,
                artistName,
                handleConfirm: () => {
                    this.sendClaimEmail();
                }
            })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    sendClaimEmail = () => {
        const { dispatch, artistId, artistName } = this.props;

        console.log(artistId, artistName);

        dispatch(sendClaimProfileEmail(artistId, this.state.email))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Email has been sent to ${artistName}`
                    })
                );
                return;
            })
            .catch((err) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item:
                            typeof err.message !== 'undefined'
                                ? err.message
                                : 'Failed to send email'
                    })
                );
                return;
            });
        this.props.dispatch(hideModal(MODAL_TYPE_CONFIRM));
    };

    render() {
        const { artistName } = this.props;

        return (
            <Modal
                modalType={MODAL_TYPE_SEND_CLAIM_PROFILE_EMAIL}
                onClose={this.props.onClose}
                title="Send Claim Profile Email"
            >
                <form className="row" onSubmit={this.handleFormSubmit}>
                    <div className="column small-24">
                        <label htmlFor="send-claim-email">
                            Please enter the email address to send the claim
                            code for artist {artistName}.
                        </label>
                        <input
                            id="send-claim-email"
                            type="text"
                            required
                            value={this.state.email}
                            onChange={this.handleInputChange}
                        />
                        <div
                            className={classnames(
                                'button-group',
                                'button-group--vert',
                                styles.buttonGroup
                            )}
                        >
                            <button
                                className="button button--pill"
                                type="submit"
                                data-testid="deletePasswordSubmit"
                            >
                                Email Claim Code
                            </button>
                        </div>
                    </div>
                </form>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        artistId: state.modal.extraData.artistId,
        artistName: state.modal.extraData.artistName
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(SendClaimProfileEmailModal);
