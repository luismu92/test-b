import React, { Component, Fragment } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';
import Transition, {
    EXITED,
    ENTERING,
    ENTERED,
    EXITING
} from 'react-transition-group/Transition';

import { hideModal } from '../redux/modules/modal';

import CloseThin from '../icons/close-thin';

import styles from './Modal.module.scss';

class Modal extends Component {
    static propTypes = {
        modalType: PropTypes.string.isRequired,
        dispatch: PropTypes.func,
        modal: PropTypes.object,
        onOpen: PropTypes.func,
        onClose: PropTypes.func,
        showCloseButton: PropTypes.bool,
        allowOverlayClose: PropTypes.bool,
        showOverlay: PropTypes.bool,
        title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        titleIcon: PropTypes.element,
        subTitle: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        className: PropTypes.string,
        overlayTransitionEnterTimeout: PropTypes.number,
        overlayTransitionLeaveTimeout: PropTypes.number,
        modalTransitionEnterTimeout: PropTypes.number,
        modalTransitionLeaveTimeout: PropTypes.number,
        children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    };

    static defaultProps = {
        onOpen() {},
        onClose() {},
        allowOverlayClose: true,
        showOverlay: true,
        showCloseButton: true,
        overlayTransitionEnterTimeout: 100,
        overlayTransitionLeaveTimeout: 200,
        modalTransitionEnterTimeout: 300,
        modalTransitionLeaveTimeout: 100
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const { modal, modalType } = this.props;
        const isOpen = modalType === modal.type;

        if (isOpen) {
            window.addEventListener('keydown', this.handleKeyDown);

            this.setOpenClass();
            this.props.onOpen();
        }
    }

    shouldComponentUpdate(nextProps) {
        return (
            nextProps.modal.type === this.props.modalType ||
            this.props.modal.type === this.props.modalType
        );
    }

    componentDidUpdate(prevProps) {
        const { modalType: prevModalType } = prevProps;
        const { modal: currentModal, modalType: currentModalType } = this.props;
        const wasOpen = prevModalType === currentModalType;
        const currentIsOpen = currentModalType === currentModal.type;

        if (currentIsOpen && !this._attachedEvent) {
            this._attachedEvent = true;

            window.addEventListener('keydown', this.handleKeyDown);

            this.setOpenClass();
            this.props.onOpen();
            return;
        }

        if (wasOpen && !currentIsOpen) {
            this._attachedEvent = false;
            window.removeEventListener('keydown', this.handleKeyDown);
        }

        this.setCloseClass();
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyDown);
        this._attachedEvent = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleKeyDown = (e) => {
        const ESC = 27;

        if (e.keyCode === ESC) {
            this.close();
        }
    };

    handleClose = (e) => {
        const { dispatch } = this.props;

        dispatch(hideModal());

        this.close(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getModalRoot() {
        const modalRootId = 'am-modal';
        let modalRoot = document.getElementById(modalRootId);

        if (!modalRoot) {
            modalRoot = document.createElement('div');

            modalRoot.id = modalRootId;
            modalRoot.className = styles.modalRoot;
            document.body.appendChild(modalRoot);
        }

        return modalRoot;
    }

    setOpenClass() {
        document.body.classList.add('modal-open');
    }

    setCloseClass() {
        document.body.classList.remove('modal-open');
    }

    open() {
        this.setOpenClass();
    }

    close(e) {
        this.setCloseClass();
        this.props.onClose(e);
    }

    renderModal = (state) => {
        const { className } = this.props;

        let header = null;
        let closeButton = null;

        const icon = this.props.titleIcon ? this.props.titleIcon : null;

        if (this.props.title) {
            header = (
                <header className={`${styles.header} row`}>
                    <h3 className={`${styles.title} column small-24`}>
                        {icon}
                        {this.props.title}{' '}
                        <span className={styles.subTitle}>
                            {this.props.subTitle}
                        </span>
                    </h3>
                </header>
            );
        }

        if (this.props.showCloseButton) {
            closeButton = (
                <button
                    className={classnames(styles.close, 'close')}
                    onClick={this.handleClose}
                >
                    <CloseThin />
                </button>
            );
        }

        // the 'modal' class is a workaround enabling us to hook legacy styles into child components
        // https://github.com/audiomack/audiomack-js/issues/1012#issuecomment-499309236

        const klass = classnames(styles.modal, 'modal', {
            [className]: className,
            [styles.modalLeaveActive]: state === EXITED,
            [styles.modalEnter]: state === ENTERING,
            [styles.modalEnterActive]: state === ENTERED,
            [styles.modalLeave]: state === EXITING
        });

        return createPortal(
            <div className={klass} key="modal">
                {closeButton}
                {header}
                <div className={classnames(styles.content, 'content')}>
                    {this.props.children}
                </div>
            </div>,
            this.getModalRoot()
        );
    };

    renderModalOverlay = (state) => {
        const klass = classnames(styles.overlay, {
            [styles.overlayLeaveActive]: state === EXITED,
            [styles.overlayEnter]: state === ENTERING,
            [styles.overlayEnterActive]: state === ENTERED,
            [styles.overlayLeave]: state === EXITING
        });
        const handler = this.props.allowOverlayClose ? this.handleClose : null;

        return createPortal(
            /* eslint-disable-next-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
            <div className={klass} onClick={handler} key="o" />,
            this.getModalRoot()
        );
    };

    renderOverlay() {
        if (!this.props.showOverlay) {
            return null;
        }

        const { modalType, modal } = this.props;
        const isActive = modal.type && modalType === modal.type;
        const transitionProps = {
            in: isActive,
            mountOnEnter: true,
            unmountOnExit: true,
            timeout: {
                enter: this.props.overlayTransitionEnterTimeout,
                exit: this.props.overlayTransitionLeaveTimeout
            }
        };

        return (
            <Transition {...transitionProps}>
                {this.renderModalOverlay}
            </Transition>
        );
    }

    render() {
        const { modalType, modal } = this.props;
        const isActive = modal.type && modalType === modal.type;
        const transitionProps = {
            in: isActive,
            mountOnEnter: true,
            unmountOnExit: true,
            timeout: {
                enter: this.props.modalTransitionEnterTimeout,
                exit: this.props.modalTransitionLeaveTimeout
            }
        };

        return (
            <Fragment>
                {this.renderOverlay()}
                <Transition {...transitionProps}>{this.renderModal}</Transition>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(Modal);
