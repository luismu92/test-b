import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';
import classnames from 'classnames';

import { clearErrors } from '../redux/modules/user/index';
import { MODAL_TYPE_AUTH, hideModal } from '../redux/modules/modal';

import AuthPageContainer from '../auth/AuthPageContainer';

import Modal from './Modal';
import styles from './AuthModal.module.scss';

class AuthModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        page: PropTypes.string,
        modal: PropTypes.object
    };

    static defaultProps = {
        onClose() {}
    };

    componentDidUpdate(prevProps) {
        const prevActive = prevProps.modal.type === MODAL_TYPE_AUTH;
        const currentActive = this.props.modal.type === MODAL_TYPE_AUTH;
        const isValidPath = this.isValidAuthPathname(window.location.pathname);

        if (!isValidPath && prevActive && currentActive) {
            this.closeModal();
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handlePasswordResetSubmit = () => {
        this.closeModal();
    };

    handleFormSubmit = () => {
        this.closeModal();
    };

    handleSocialAuth = () => {
        this.closeModal();
    };

    handleModalClose = () => {
        this.closeModal();
    };

    handleModalOpen = () => {
        const { dispatch } = this.props;

        dispatch(clearErrors());
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    isValidAuthPathname(path) {
        return ['/login', '/join', '/forgot-password'].indexOf(path) !== -1;
    }

    closeModal() {
        const { dispatch, onClose } = this.props;
        const redirectTo = parse(window.location.search).redirectTo;

        if (redirectTo) {
            window.history.pushState(
                null,
                null,
                decodeURIComponent(redirectTo)
            );
        }

        dispatch(hideModal());

        onClose();
    }

    renderContent(page) {
        switch (page) {
            case 'login':
            case 'join':
            case 'forgot':
                return (
                    <AuthPageContainer
                        activePage={page}
                        onFormSubmit={this.handleFormSubmit}
                        onSocialAuth={this.handleSocialAuth}
                        inModal
                    />
                );

            default:
                return null;
        }
    }

    render() {
        return (
            <Modal
                className={classnames(styles.modal, 'auth')}
                modalType={MODAL_TYPE_AUTH}
                onClose={this.handleModalClose}
                onOpen={this.handleModalOpen}
            >
                {this.renderContent(this.props.page)}
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        page: state.modal.extraData.type
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(AuthModal);
