import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PromoKeyContainer from '../components/PromoKeyContainer';
import { SketchPicker } from 'react-color';
import classnames from 'classnames';

import { debounce, getEmbedUrl, ownsMusic, yesBool } from 'utils/index';

import { MODAL_TYPE_EMBED, hideModal } from '../redux/modules/modal';

import EmbedTextarea from '../components/EmbedTextarea';

import Modal from './Modal';
import styles from './EmbedModal.module.scss';

export default class EmbedModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        item: PropTypes.object,
        currentUser: PropTypes.object,
        onClose: PropTypes.func
    };

    static defaultProps = {
        onClose() {}
    };

    constructor(props) {
        super(props);

        this.state = {
            background: true,
            color: false,
            showColorPicker: false,
            colorString: '#ff0000',
            showPromoContainer: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentWillUnmount() {
        this._swatchButton = null;
        this._swatchContainer = null;
        this._attachedBodyListener = null;
        this.removeBodyListener();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCheckboxClick = (e) => {
        const input = e.currentTarget;
        const { name, checked } = input;

        this.setState({
            [name]: checked
        });
    };

    handleColorChange = (color) => {
        this.setColor(color.hex);
    };

    handleSwatchClick = (e) => {
        if (e.target !== this._swatchButton) {
            return;
        }

        const shouldShow = this.state.showColorPicker !== true;

        if (shouldShow) {
            this.attachBodyListener();
        } else {
            this.removeBodyListener();
        }

        this.setState({
            showColorPicker: !this.state.showColorPicker
        });
    };

    handleBodyClick = (e) => {
        const target = e.target;

        if (
            this._swatchContainer.contains(target) ||
            target === this._swatchButton
        ) {
            return;
        }

        this.setState({
            showColorPicker: false
        });

        this.removeBodyListener();
    };

    handlePromoLinkClick = () => {
        this.setState({
            showPromoContainer: !this.state.showPromoContainer
        });
    };

    handleCloseModalClick = () => {
        this.closeModal();
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    setColor = debounce((color) => {
        this.setState({
            colorString: color
        });
    }, 500);

    closeModal() {
        const { dispatch, onClose } = this.props;

        this.setState({
            showPromoContainer: false
        });

        dispatch(hideModal());

        onClose();
    }

    attachBodyListener() {
        if (this._attachedBodyListener) {
            return;
        }

        this._attachedBodyListener = true;

        document.body.addEventListener('click', this.handleBodyClick, false);
    }

    removeBodyListener() {
        this._attachedBodyListener = null;

        document.body.removeEventListener('click', this.handleBodyClick);
    }

    renderEmbedPanel(item, queryOptions) {
        return (
            <EmbedTextarea
                title="You can add this player to a website by embedding it."
                item={item}
                queryOptions={queryOptions}
            />
        );
    }

    renderColorPicker(shouldRender) {
        if (!shouldRender) {
            return null;
        }

        let picker;

        if (this.state.showColorPicker) {
            picker = (
                <SketchPicker
                    color={this.state.colorString}
                    className="color-picker__picker"
                    onChangeComplete={this.handleColorChange}
                    triangle="top-right"
                />
            );
        }

        return (
            <div
                className="color-picker"
                ref={(b) => {
                    this._swatchContainer = b;
                }}
            >
                <button
                    ref={(b) => {
                        this._swatchButton = b;
                    }}
                    className="color-picker__button"
                    style={{ backgroundColor: this.state.colorString }}
                    onClick={this.handleSwatchClick}
                />
                {picker}
            </div>
        );
    }

    renderPrivateLabel(item) {
        const { currentUser } = this.props;

        const isPrivate = yesBool(item.private);
        const isOwnMusic = ownsMusic(currentUser, item);

        if (!isPrivate || (isPrivate && !isOwnMusic)) {
            return null;
        }

        return (
            <div
                className={classnames(
                    styles.privateLabel,
                    'u-spacing-bottom-10',
                    'u-padding-3'
                )}
            >
                <p>
                    <button onClick={this.handlePromoLinkClick}>
                        This {item.type} is private, create a private link to
                        embed it.
                    </button>
                </p>
            </div>
        );
    }

    render() {
        const { item } = this.props;
        const { showPromoContainer } = this.state;

        if (!item) {
            return null;
        }

        const type = item.trackType || item.type;

        let modalHeight;

        modalHeight = '417';

        if (type === 'song') {
            modalHeight = '252';
        }

        const queryOptions = {
            background: this.state.background ? 1 : 0,
            color: this.state.color ? this.state.colorString.substr(1) : null
        };

        let title = 'Embed: ';
        let subTitle = item.title;

        let modalContent = (
            <div className="column small-24">
                {this.renderPrivateLabel(item)}
                <iframe
                    title="Embed example"
                    src={getEmbedUrl(item, queryOptions)}
                    scrolling="no"
                    width="100%"
                    height={modalHeight}
                    frameBorder="0"
                />

                <p>
                    <strong>Customize Your Embed Player:</strong>
                </p>

                <div className="u-spacing-top-em">
                    <label
                        className={classnames(
                            styles.label,
                            'u-spacing-right-em'
                        )}
                    >
                        <input
                            type="checkbox"
                            name="background"
                            className="checkbox--circular"
                            checked={this.state.background}
                            onChange={this.handleCheckboxClick}
                        />

                        <p className={styles.text}>Use art as background</p>
                    </label>
                    <label
                        className={classnames(
                            styles.label,
                            'u-spacing-right-em'
                        )}
                    >
                        <input
                            type="checkbox"
                            name="color"
                            className="checkbox--circular"
                            checked={this.state.color}
                            onChange={this.handleCheckboxClick}
                        />
                        <p className={styles.text}>Use custom color</p>
                    </label>
                    {this.renderColorPicker(this.state.color)}
                </div>
                {this.renderEmbedPanel(item, queryOptions)}
            </div>
        );

        if (showPromoContainer) {
            title = 'Create Promotional Link';
            subTitle = null;

            modalContent = <PromoKeyContainer music={item} />;
        }

        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_EMBED}
                onClose={this.handleCloseModalClick}
                title={title}
                subTitle={subTitle}
            >
                {modalContent}
            </Modal>
        );
    }
}
