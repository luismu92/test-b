import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_QUEUE_ALERT, hideModal } from '../redux/modules/modal';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './ConfirmModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

class QueueAlertModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        item: PropTypes.object,
        onClose: PropTypes.func,
        onAppendClick: PropTypes.func,
        onReplaceClick: PropTypes.func
    };

    static defaultProps = {
        item: {},
        onAppendClick() {},
        onReplaceClick() {}
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleAppendClick = (e) => {
        const { dispatch } = this.props;

        dispatch(hideModal());

        this.props.onAppendClick(e);
    };

    handleReplaceClick = (e) => {
        const { dispatch } = this.props;

        dispatch(hideModal());

        this.props.onReplaceClick(e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <Modal
                className={styles.variant.modal}
                modalType={MODAL_TYPE_QUEUE_ALERT}
                onClose={this.props.onClose}
                title="You’re about to overwrite your queue"
            >
                <div className="row">
                    <div className="column">
                        <p className={styles.base.message}>
                            We noticed you’ve done a little curation in removing
                            certain tracks. How should music related to “
                            {this.props.item.title}” update your queue?
                        </p>
                        <div
                            className={classnames(
                                'button-group',
                                'button-group--vert',
                                styles.variant.buttonGroup
                            )}
                        >
                            <div>
                                <button
                                    className="button button--pill button--wide"
                                    onClick={this.handleReplaceClick}
                                    type="button"
                                >
                                    Start new queue
                                </button>
                                <button
                                    className="button button--pill button--wide"
                                    onClick={this.handleAppendClick}
                                    type="button"
                                >
                                    Append new music
                                </button>
                            </div>
                            <div>
                                <button
                                    className="button button--pill button--transparent button--wide"
                                    onClick={this.handleCancelClick}
                                >
                                    Cancel play
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        item: state.modal.extraData.item,
        onAppendClick: state.modal.extraData.onAppendClick,
        onReplaceClick: state.modal.extraData.onReplaceClick
    };
}

export default compose(connect(mapStateToProps))(QueueAlertModal);
