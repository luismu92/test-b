import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_CHOOSE_PREVIOUS_SONGS } from '../redux/modules/modal';

import Modal from './Modal';

import styles from './ChoosePreviousSongsModal.module.scss';

class ChoosePreviousSongsModal extends Component {
    static propTypes = {
        modal: PropTypes.object,
        onChoiceClick: PropTypes.func
    };

    render() {
        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_CHOOSE_PREVIOUS_SONGS}
                onClose={this.props.onChoiceClick}
                showCloseButton={true}
                allowOverlayClose={false}
                modalProps={{ style: { maxWidth: 550 } }}
            >
                <div className={classnames('u-text-center', styles.content)}>
                    <h2>Before you start uploading.</h2>
                    <p className={styles.copy}>
                        Should this album include any music that you've already
                        uploaded?
                    </p>
                    <div className="u-spacing-y-em u-spacing-top">
                        <button
                            className={classnames(
                                'button',
                                'button--pill',
                                'button--med',
                                styles.button
                            )}
                            data-choice="choose"
                            onClick={this.props.onChoiceClick}
                        >
                            Yes, add songs
                        </button>
                        <button
                            className={classnames(
                                'button',
                                'button--pill',
                                'button--med',
                                'button--black',
                                styles.button
                            )}
                            data-choice="close"
                            onClick={this.props.onChoiceClick}
                        >
                            No, start uploading
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(ChoosePreviousSongsModal);
