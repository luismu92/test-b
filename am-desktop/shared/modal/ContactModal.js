import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import { MODAL_TYPE_CONTACT } from '../redux/modules/modal';

import LegalPageContainer from '../about/LegalPageContainer';
import ContactPageContainer from '../about/ContactPageContainer';

import Modal from './Modal';

class ContactModal extends Component {
    static propTypes = {
        page: PropTypes.string,
        modal: PropTypes.object,
        location: PropTypes.object,
        onClose: PropTypes.func,
        onContactLinkClick: PropTypes.func
    };

    static defaultProps = {
        page: 'legal'
    };

    componentDidUpdate(prevProps) {
        const prevActive = prevProps.modal.type === MODAL_TYPE_CONTACT;
        const currentActive = this.props.modal.type === MODAL_TYPE_CONTACT;
        let newPathname;
        let updatePath = () => {}; // eslint-disable-line

        if ((!prevActive && currentActive) || (prevActive && currentActive)) {
            if (!this._locationPathname) {
                this._locationPathname = this.props.location.pathname;
            }

            newPathname = this.getPathnameForPage(this.props.page);

            let historyFn = 'pushState';

            if (prevActive && currentActive) {
                historyFn = 'replaceState';
            }

            updatePath = () =>
                window.history[historyFn](null, null, newPathname);
        } else if (!currentActive && prevActive && this._locationPathname) {
            updatePath = () => {
                window.history.pushState(null, null, this._locationPathname);
                this._locationPathname = null;
            };
        }

        const isValidPath = this.isValidAuthPathname(window.location.pathname);
        // const isNextValidPath = this.isValidAuthPathname(newPathname);

        // console.log(window.location.pathname, this.props.location.pathname, nextProps.location.pathname, this._locationPathname, isValidPath);
        if (!isValidPath && prevActive && currentActive) {
            this.props.onClose();
            return;
        }

        updatePath();
    }

    //////////////////////
    // Internal methods //
    //////////////////////

    getPathnameForPage(page) {
        switch (page) {
            case 'contact':
                return '/contact-us';

            default:
                return '/about/legal';
        }
    }

    getTitle(page) {
        switch (page) {
            case 'contact':
                return 'Contact Us';

            default:
                return 'DMCA Notice form';
        }
    }

    isValidAuthPathname(path) {
        return ['/contact-us', '/about/legal'].indexOf(path) !== -1;
    }

    renderLegal(active) {
        if (!active) {
            return null;
        }

        return (
            <LegalPageContainer
                onContactLinkClick={this.props.onContactLinkClick}
                onClose={this.props.onClose}
                inModal
            />
        );
    }

    renderContact(active) {
        if (!active) {
            return null;
        }

        return (
            <ContactPageContainer
                onContactLinkClick={this.props.onContactLinkClick}
                onClose={this.props.onClose}
                inModal
            />
        );
    }

    render() {
        return (
            <Modal
                modalType={MODAL_TYPE_CONTACT}
                onClose={this.props.onClose}
                title={this.getTitle(this.props.page)}
            >
                {this.renderLegal(this.props.page === 'legal')}
                {this.renderContact(this.props.page === 'contact')}
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        page: state.modal.extraData.page
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(ContactModal);
