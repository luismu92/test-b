import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_ALERT, hideModal } from '../redux/modules/modal';

import Modal from './Modal';
import baseStyles from './Modal.module.scss';
import variantStyles from './AlertModal.module.scss';

const styles = {
    base: baseStyles,
    variant: variantStyles
};

class AlertModal extends Component {
    static propTypes = {
        active: PropTypes.bool,
        onClose: PropTypes.func,
        dispatch: PropTypes.func,
        title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        confirmButtonText: PropTypes.string,
        message: PropTypes.string
    };

    static defaultProps = {
        confirmButtonText: 'OK',
        title: 'Warning'
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleConfirmClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    render() {
        const { title, message, confirmButtonText } = this.props;

        return (
            <Modal
                className={styles.variant.modal}
                modalType={MODAL_TYPE_ALERT}
                onClose={this.props.onClose}
                title={title}
            >
                <div className="row">
                    <p
                        className={classnames(
                            styles.base.message,
                            'column',
                            'small-24'
                        )}
                    >
                        {message}
                    </p>
                    <div className="button-group column small-24">
                        <button
                            className="button button--pill"
                            onClick={this.handleConfirmClick}
                            type="button"
                        >
                            {confirmButtonText}
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: state.modal.extraData.title,
        message: state.modal.extraData.message
    };
}

export default compose(connect(mapStateToProps))(AlertModal);
