import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_ALBUM_FINISH_UPLOAD } from '../redux/modules/modal';

import analytics, {
    eventCategory,
    eventAction,
    eventLabel
} from 'utils/analytics';

import {
    buildDynamicImage,
    getTwitterShareLink,
    getFacebookShareLink
} from 'utils/index';

import Modal from './Modal';
import PromoKeyContainer from '../components/PromoKeyContainer';
import StepProgressBar from '../upload/StepProgressBar';
import Tunecore from '../components/Tunecore';

import ArrowNext from '../icons/arrow-next';
import FacebookIcon from '../icons/facebook-letter-logo';
import TwitterIcon from '../icons/twitter-logo-new';

import styles from './AlbumFinishUploadModal.module.scss';

class AlbumFinishUploadModal extends Component {
    static propTypes = {
        musicEdit: PropTypes.object,
        uploadSteps: PropTypes.array
    };

    constructor(props) {
        super(props);

        this.state = {
            promo: false
        };
    }

    handleClose = (e) => {
        e.preventDefault();

        this.setState({
            promo: false
        });
    };

    handleFinishSocialClick = (e) => {
        e.preventDefault();
        const button = e.currentTarget;
        const socialType = button.getAttribute('data-social-type');

        const label =
            socialType === 'facebook'
                ? eventAction.facebookSocial
                : eventAction.twitterSocial;

        analytics.track(eventCategory.socialShare, {
            eventAction: label,
            eventLabel: eventLabel.uploadFinishSocial
        });
    };

    handlePromoClick = (e) => {
        e.preventDefault();
        this.setState({ promo: true });
    };

    renderPrivateContent() {
        return (
            <div className="upload-complete__promo-link u-spacing-top-15 u-padding-20 u-padding-bottom-30 u-bg-gray11">
                <h3 className="u-fs-16 u-ls-n-05">
                    Share your music privately with a custom link.
                </h3>
                <p className="u-spacing-bottom-10 u-padding-x-60 u-ls-n-03 u-lh-14">
                    Allow select people to listen to any private song by
                    creating a custom private link.
                </p>
                <div className="u-spacing-top-5">
                    <button
                        className="button button--large u-fs-14 upload-complete__promo-button"
                        onClick={this.handlePromoClick}
                    >
                        Create
                        <ArrowNext className="upload-complete__promo-button-icon" />
                    </button>
                </div>
            </div>
        );
    }

    renderShareUpload(uploadItem) {
        const shareOn = ['twitter', 'facebook'].map((item, i) => {
            let shareButton = (
                <a
                    href={getTwitterShareLink(process.env.AM_URL, uploadItem)}
                    className="social-button social-button--twitter upload-complete__social-button"
                    target="_blank"
                    rel="nofollow noopener"
                    data-social-type="twitter"
                    onClick={this.handleFinishSocialClick}
                >
                    <TwitterIcon className="u-text-icon social-button__icon" />{' '}
                    Share on Twitter
                </a>
            );

            if (item === 'facebook') {
                shareButton = (
                    <a
                        href={getFacebookShareLink(
                            process.env.AM_URL,
                            uploadItem,
                            process.env.FACEBOOK_APP_ID
                        )}
                        className="social-button social-button--facebook upload-complete__social-button"
                        target="_blank"
                        rel="nofollow noopener"
                        data-social-type="facebook"
                        onClick={this.handleFinishSocialClick}
                    >
                        <FacebookIcon className="u-text-icon social-button__icon" />{' '}
                        Share on Facebook
                    </a>
                );
            }

            let headingIcon = <TwitterIcon />;

            if (item === 'facebook') {
                headingIcon = <FacebookIcon />;
            }

            const artwork = buildDynamicImage(uploadItem.image, {
                width: 125,
                height: 125,
                max: true
            });

            const retinaArtwork = buildDynamicImage(uploadItem.image, {
                width: 250,
                height: 250,
                max: true
            });

            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            const image = <img src={artwork} srcSet={srcSet} alt="" />;

            return (
                <div key={i} className="upload-share-wrap">
                    <h3 className="upload-share__heading u-fs-18 u-lh-24 u-ls-n-05 u-fw-700 u-spacing-bottom-10">
                        <span
                            className={`upload-share__heading-icon upload-share__heading-icon--${item}`}
                        >
                            {headingIcon}
                        </span>
                        Share on <span className="u-tt-cap">{item}</span>
                    </h3>

                    <div className={`upload-share upload-share--${item}`}>
                        <div className="upload-share__image">{image}</div>
                        <div className="upload-share__details u-padding-x-10">
                            <p className="upload-share__top-am u-fs-13 u-lh-22 u-tt-uppercase">
                                Audiomack.com
                            </p>
                            <h3 className="upload-share__title u-fs-15 u-lh-22 u-ls-n-05 u-fw-600 u-trunc">
                                {uploadItem.artist} - {uploadItem.title}
                            </h3>
                            <p className="upload-share__desc u-fs-13 u-ls-n-025">
                                Listen to {uploadItem.title}, the new{' '}
                                {uploadItem.type} from{' '}
                                {uploadItem.uploader.name}
                            </p>
                            <p className="upload-share__bottom-am u-fs-14 u-fw-600 u-lh-22 u-ls-n-05 u-text-gray8">
                                audiomack.com
                            </p>
                        </div>
                    </div>
                    <div className="u-spacing-top-10">{shareButton}</div>
                </div>
            );
        });

        return (
            <div className={styles.shareUpload}>
                <div className="u-padding-y-20 u-text-left share-upload">
                    {shareOn}
                </div>
                <div className="upload-complete__col upload-complete__col--right u-text-center">
                    <Tunecore
                        className={classnames(
                            styles.tunecore,
                            'u-text-center',
                            'upload-complete__tunecore'
                        )}
                        linkHref="http://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_afpup_su&utm_content=ghoagpwt_a"
                        eventLabel={eventLabel.finishedSongUpload}
                        variant="side"
                    />
                </div>
            </div>
        );
    }

    render() {
        const { musicEdit, uploadSteps } = this.props;
        const { promo } = this.state;

        if (!musicEdit || !uploadSteps) {
            return null;
        }

        const lastStepNumber = uploadSteps.length;
        const privateAlbum = musicEdit.info.private === 'yes';
        const scheduledAlbum =
            Math.floor(Date.now() / 1000) < musicEdit.info.released;
        let body;
        let progress = (
            <StepProgressBar
                backgroundColor="white"
                currentStep={lastStepNumber}
                maxVisitedStep={lastStepNumber}
                stepList={uploadSteps}
            />
        );
        let title;
        let variant;

        if (promo) {
            body = <PromoKeyContainer music={musicEdit.info} />;
            progress = null;
            title = 'Create Promotional Link';
            variant = 'promo';
        } else if (privateAlbum || scheduledAlbum) {
            body = this.renderPrivateContent();
            title = 'Your upload is set to private';
            variant = 'private';
        } else {
            body = this.renderShareUpload(musicEdit.info);
            title = 'Your upload is now live!';
            variant = 'live';
        }

        return (
            <Modal
                className={classnames(styles.modal, styles[variant])}
                modalType={MODAL_TYPE_ALBUM_FINISH_UPLOAD}
                onClose={this.handleClose}
                showCloseButton={true}
                title={title}
            >
                {progress}
                <section className={styles.content}>{body}</section>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        musicEdit: state.modal.extraData.musicEdit,
        uploadSteps: state.modal.extraData.uploadSteps
    };
}

export default compose(connect(mapStateToProps))(AlbumFinishUploadModal);
