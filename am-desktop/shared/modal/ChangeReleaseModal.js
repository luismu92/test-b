import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Modal from './Modal';
import { MODAL_TYPE_CHANGE_RELEASE } from '../redux/modules/modal';

import DatePickerInput from '../components/DatePickerInput';

import styles from './ChangeReleaseModal.module.scss';

export default class ChangeReleaseModal extends Component {
    static propTypes = {
        music: PropTypes.object.isRequired,
        onClose: PropTypes.func,
        onDateChange: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            newDate: moment(this.props.music.released * 1000)
        };
    }

    handleSelectDate = (date) => {
        this.setState({
            newDate: date
        });
    };

    handleChangeDate = () => {
        const { newDate } = this.state;

        this.props.onDateChange(newDate.unix());
    };

    render() {
        const { music } = this.props;
        const { newDate } = this.state;

        const isSameDate = newDate === moment(this.props.music.released * 1000);

        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_CHANGE_RELEASE}
                onClose={this.props.onClose}
                title={`Change the release date of this ${music.type}`}
            >
                <DatePickerInput
                    value={newDate}
                    onChange={this.handleSelectDate}
                />
                <div>
                    <button
                        className="button button--pill"
                        onClick={this.handleChangeDate}
                        disabled={isSameDate}
                    >
                        Confirm Release Date Change
                    </button>
                </div>
            </Modal>
        );
    }
}
