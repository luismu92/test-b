import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import {
    MODAL_TYPE_VALIDATE_AGREEMENT,
    hideModal
} from '../redux/modules/modal';

import CheckIcon from '../icons/check-mark';

import Modal from './Modal';
import styles from './ValidateAgreementModal.module.scss';

const checkboxData = [
    { id: 'checkbox1', label: 'I am a content creator.' },
    {
        id: 'checkbox2',
        label:
            'I own rights to distribute and upload all of the content that currently exists on my profile.'
    },
    {
        id: 'checkbox3',
        label:
            'I will not upload content that I do not own rights to, and understand that in doing so I may be banned from the Creator Authentication Program.'
    }
];

class ValidateAgreementModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        history: PropTypes.object
    };

    static defaultProps = {
        onConfirm() {},
        disableOnConfirm: true,
        confirmButtonProps: {}
    };

    constructor(props) {
        super(props);

        this.state = {};
        checkboxData.forEach(({ id }) => {
            // eslint-disable-next-line react/no-direct-mutation-state
            this.state[id] = false;
        });
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    handleChange = (e) => {
        const name = e.target.name;

        this.setState({
            [name]: !this.state[name]
        });
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        this.props.history.replace('/validate/artist');
    };

    render() {
        const allChecked = checkboxData.every(({ id }) => {
            return this.state[id] === true;
        });
        const checkboxes = checkboxData.map(({ id, label, checked }, i) => {
            return (
                <div className={`${styles.checkboxAgreement}`} key={i}>
                    <input
                        type="checkbox"
                        name={id}
                        id={id}
                        value={label}
                        checked={checked}
                        onChange={this.handleChange}
                        required
                    />
                    <label htmlFor={id}>{label}</label>
                </div>
            );
        });

        return (
            <Modal
                className={styles.modalValidateAgreement}
                modalType={MODAL_TYPE_VALIDATE_AGREEMENT}
                onClose={this.props.onClose}
            >
                <form onSubmit={this.handleFormSubmit}>
                    <div className={`u-text-center ${styles.titleStyle}`}>
                        <div className={`${styles.titleAgreement} row`}>
                            <h3>
                                <CheckIcon className="verified u-text-center" />{' '}
                                Creator Authentication Agreement
                            </h3>
                        </div>
                        <p className={styles.pText}>
                            Creators must agree to following terms to be
                            considered for our Creator Authentication Program.
                            Violation of any of these may result in removal of
                            your authentication status. Check the below fields
                            to confirm.
                        </p>
                    </div>
                    <div className={styles.checkboxContainer}>{checkboxes}</div>
                    <div className="u-text-center">
                        <button
                            className={
                                allChecked
                                    ? styles.buttonValidate
                                    : styles.buttonInvalid
                            }
                            type="submit"
                            disabled={!allChecked}
                        >
                            {' '}
                            I Accept
                        </button>
                    </div>
                </form>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(connect(mapStateToProps))(ValidateAgreementModal);
