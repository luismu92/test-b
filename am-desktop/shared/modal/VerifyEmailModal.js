import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Modal from './Modal';
import { MODAL_TYPE_VERIFY_HASH } from '../redux/modules/modal';

import VerifyEmailPageContainer from '../auth/VerifyEmailPageContainer';

import styles from './ConfirmModal.module.scss';

class VerifyEmailModal extends Component {
    static propTypes = {
        modal: PropTypes.object.isRequired,
        onClose: PropTypes.func
    };

    render() {
        const { modal } = this.props;

        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_VERIFY_HASH}
                onClose={this.props.onClose}
                showCloseButton={false}
            >
                <VerifyEmailPageContainer hash={modal.extraData.hash} />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(VerifyEmailModal);
