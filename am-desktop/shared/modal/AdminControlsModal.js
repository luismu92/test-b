import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { getArtistName } from 'utils/index';

import { MODAL_TYPE_ADMIN } from '../redux/modules/modal';

import AdminControlsContainer from '../admin/AdminControlsContainer';

import Modal from './Modal';

class AdminControlsModal extends Component {
    static propTypes = {
        modal: PropTypes.object
    };

    render() {
        const item = this.props.modal.extraData.item || {};

        return (
            <Modal
                modalType={MODAL_TYPE_ADMIN}
                title={`Admin Controls for ${item.type} ${getArtistName(
                    item
                )} - ${item.title}`}
            >
                <AdminControlsContainer item={item} />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(AdminControlsModal);
