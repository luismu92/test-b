import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_TRENDING } from '../redux/modules/modal';

import Checkbox from '../components/Checkbox';

import Modal from './Modal';
import EditMusicTags from '../components/EditMusicTags';

import styles from './TrendingModal.module.scss';

class TrendingModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        trendData: PropTypes.object
    };

    static defaultProps = {
        trendData: {}
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    createCheckbox = (label, index) => {
        const key = `${index}-${label}`;

        return (
            <tr key={key}>
                <td colSpan="2">
                    <Checkbox
                        label={label}
                        onCheckboxChange={
                            this.props.trendData.handleCheckboxToggle
                        }
                        key={label}
                        isChecked={this.props.trendData.trendGenres[label]}
                    />
                </td>
            </tr>
        );
    };

    createTrendingCheckboxes = (items) => {
        return Object.keys(items).map(this.createCheckbox);
    };

    render() {
        const { trendData } = this.props;

        return (
            <Modal modalType={MODAL_TYPE_TRENDING} onClose={this.props.onClose}>
                <div className="row u-padding-top-30">
                    <div
                        className={`${styles.column} column small-24 medium-12`}
                    >
                        <h3
                            className={classnames(
                                styles.columnHeader,
                                'u-spacing-bottom-30'
                            )}
                        >
                            Trending Controls
                        </h3>
                        <div className="button-group">
                            <table className={styles.table}>
                                <tbody className={styles.tableBody}>
                                    {this.createTrendingCheckboxes(
                                        trendData.trendGenres || {}
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div
                        className={`${styles.column} column small-24 medium-12`}
                    >
                        <EditMusicTags />
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: state.modal.extraData.title,
        trendData: state.modal.extraData.trendData
    };
}

export default compose(connect(mapStateToProps))(TrendingModal);
