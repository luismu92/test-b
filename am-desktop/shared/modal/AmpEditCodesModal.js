import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { MODAL_TYPE_AMP_CODES, hideModal } from '../redux/modules/modal';
import AmpCodesContainer from '../monetization/AmpCodesContainer';
import Modal from './Modal';

class AmpEditCodesModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        modal: PropTypes.object
    };

    ////////////////////
    // Event handelrs //
    ////////////////////

    handleModalClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const item = this.props.modal.extraData.item || {};
        const contextType = this.props.modal.extraData.contextType || '';

        return (
            <Modal
                modalType={MODAL_TYPE_AMP_CODES}
                showOverlay
                onClose={this.handleModalClose}
                overlayTransitionLeaveTimeout={300}
                modalTransitionEnterTimeout={300}
                title={`AMP Codes for ${item.type} ${item.artist} - ${
                    item.title
                }`}
            >
                <AmpCodesContainer
                    onFormSubmit={this.handleModalClose}
                    item={item}
                    contextType={contextType}
                />
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(AmpEditCodesModal);
