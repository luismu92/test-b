import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import Modal from './Modal';
import { MODAL_TYPE_SUGGEST_TAG, hideModal } from '../redux/modules/modal';
import { suggestMusicTag } from '../redux/modules/music/tags';
import { addToast } from '../redux/modules/toastNotification';
import { compose } from 'redux';
import { connect } from 'react-redux';

import MusicIcon from '../icons/music-new';

import styles from './SuggestTagModal.module.scss';

class SuggestTagModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onTagSubmit: PropTypes.func,
        musicItem: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            input: ''
        };
    }

    handleInputChange = (e) => {
        this.setState({
            input: e.currentTarget.value
        });
    };

    handleTagSubmit = (e) => {
        e.preventDefault();

        const { dispatch, onTagSubmit, musicItem } = this.props;
        const { input } = this.state;
        const tag = input.trim();

        if (!tag) {
            return;
        }

        if (onTagSubmit) {
            onTagSubmit(tag);

            dispatch(hideModal());

            this.setState({
                input: ''
            });

            return;
        }

        dispatch(suggestMusicTag(musicItem, input))
            .then(() => {
                return dispatch(
                    addToast({
                        action: 'message',
                        item: "Thanks! We've received your tag suggestion."
                    })
                );
            })
            .catch(() => {
                return dispatch(
                    addToast({
                        action: 'message',
                        item: 'Error: your tag suggestion was not submitted.'
                    })
                );
            });

        dispatch(hideModal());

        this.setState({
            input: ''
        });
    };

    render() {
        const icon = <MusicIcon className="u-text-icon u-text-orange" />;

        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_SUGGEST_TAG}
                showCloseButton={true}
                title="Suggest a new tag"
                titleIcon={icon}
            >
                <form>
                    <input
                        className={styles.input}
                        onChange={this.handleInputChange}
                        placeholder="Enter your suggestion"
                        required
                        type="text"
                        value={this.state.input}
                    />

                    <button
                        className={classnames(
                            'button',
                            'button--pill',
                            'button--med'
                        )}
                        disabled={!this.state.input.trim()}
                        onClick={this.handleTagSubmit}
                        type="submit"
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    };
}

export default compose(connect(mapStateToProps))(SuggestTagModal);
