import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import analytics from 'utils/analytics';
import { ucfirst } from 'utils/index';

import { getArtist } from '../redux/modules/artist/index';
import { addToast } from '../redux/modules/toastNotification';
import { MODAL_TYPE_BLOCK, hideModal } from '../redux/modules/modal';
import { blockArtist } from '../redux/modules/theBackwoods/artist';

import Modal from './Modal';
import styles from './Modal.module.scss';

function fixKey(key) {
    return `data-${key}`;
}

class BlockModal extends Component {
    static propTypes = {
        active: PropTypes.bool,
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        onCancel: PropTypes.func,
        title: PropTypes.string,
        artistId: PropTypes.string,
        artist: PropTypes.string,
        confirmData: PropTypes.object,
        uploadCount: PropTypes.string,
        onConfirm: PropTypes.func,
        fromAdmin: PropTypes.bool
    };

    static defaultProps = {
        onConfirm: () => {
            return;
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            clear: false
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleClearClick = (event) => {
        const target = event.target;
        const value =
            target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    handleBlockClick = (event) => {
        const { dispatch, confirmData, fromAdmin } = this.props;

        const id = event.currentTarget.getAttribute('data-id');
        const clear = event.currentTarget.getAttribute('data-clear') === 'true';
        const operation = confirmData.type === 'block' ? 'banned' : 'allowed';

        dispatch(blockArtist(id, operation, clear))
            .then(({ resolved }) => {
                if (!fromAdmin) {
                    dispatch(getArtist(confirmData.artist.profile.url_slug));
                }
                dispatch(hideModal(MODAL_TYPE_BLOCK));
                dispatch(
                    addToast({
                        action: 'message',
                        message: `Artist ${
                            resolved.can_upload === 'yes'
                                ? 'un-blocked'
                                : 'blocked'
                        }`
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(hideModal(MODAL_TYPE_BLOCK));
                dispatch(
                    addToast({
                        action: 'message',
                        message: `There was a problem ${
                            confirmData.type
                        }ing the artist: ${error.message}`
                    })
                );
                analytics.error(error);
            });
    };

    handleCancel = () => {
        const { dispatch } = this.props;
        dispatch(hideModal(MODAL_TYPE_BLOCK));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const {
            title,
            artist,
            onCancel,
            confirmData,
            uploadCount
        } = this.props;

        const confirmDataAttributes = Object.assign(
            {},
            ...Object.keys(confirmData).map((key) => ({
                [fixKey(key)]: confirmData[key]
            }))
        );

        let takedown = '';

        if (confirmData.type === 'block') {
            takedown = (
                <div>
                    <p className={styles.message}>
                        You can optionally take down all songs and albums
                        associated with this artist.
                    </p>
                    <label className={styles.message}>
                        <input
                            type="checkbox"
                            onClick={this.handleClearClick}
                            name="clear"
                            value="1"
                        />{' '}
                        <strong>Clear {uploadCount} upload(s)</strong>
                    </label>
                </div>
            );
        }

        return (
            <Modal
                modalType={MODAL_TYPE_BLOCK}
                onClose={this.props.onClose}
                title={title}
            >
                <div>
                    <p className={styles.message}>
                        Please confirm that you wish to {confirmData.type}{' '}
                        <strong>{artist}</strong>.
                    </p>
                    {takedown}
                    <div className="button-group">
                        <button
                            className="button button--pill"
                            onClick={this.handleBlockClick}
                            {...confirmDataAttributes}
                            data-clear={this.state.clear}
                        >
                            {ucfirst(confirmData.type || '')}
                        </button>
                        <button
                            className="button button--pill button--black"
                            onClick={onCancel || this.handleCancel}
                        >
                            Cancel
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: state.modal.extraData.title,
        artist: state.modal.extraData.artist,
        artistId: state.modal.extraData.artistId,
        onConfirm: state.modal.extraData.handleConfirm,
        onCancel: state.modal.extraData.handleCancel,
        confirmData: state.modal.extraData.confirmData || {},
        fromAdmin: state.modal.extraData.fromAdmin
    };
}

export default compose(connect(mapStateToProps))(BlockModal);
