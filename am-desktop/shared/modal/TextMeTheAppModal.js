import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_TEXT_ME_APP, hideModal } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';

import Modal from './Modal';
import styles from './TextMeTheAppModal.module.scss';

class TextMeTheAppModal extends Component {
    static propTypes = {
        onClose: PropTypes.func,
        dispatch: PropTypes.func,
        sdk: PropTypes.object
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleFormSubmit = (e) => {
        e.preventDefault();

        const { dispatch, sdk } = this.props;
        const form = e.currentTarget;
        const tel = form.phone.value;
        const linkData = {
            tags: [],
            channel: 'Desktop Web',
            feature: 'Sidebar - TextMeTheApp'
        };
        const options = {};

        sdk.sendSMS(tel, linkData, options, (err) => {
            if (err) {
                dispatch(
                    addToast({
                        type: 'warning',
                        message:
                            'Sorry there was an error sending the SMS. Please try again.'
                    })
                );
                console.error(err);
                return;
            }

            dispatch(
                addToast({
                    type: 'info',
                    message: 'SMS successfully sent!.'
                })
            );
        });

        form.phone.value = '';

        dispatch(hideModal());
    };

    render() {
        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_TEXT_ME_APP}
                onClose={this.props.onClose}
            >
                <form className="media-item" onSubmit={this.handleFormSubmit}>
                    <div className="media-item__figure">
                        <img
                            src="/static/images/desktop/text-me-app.png"
                            srcSet="/static/images/desktop/text-me-app@2x.png 2x"
                            alt=""
                            className={styles.image}
                        />
                    </div>
                    <div
                        className={classnames(styles.body, 'media-item__body')}
                    >
                        <h2>Send me the app</h2>
                        <p
                            className={classnames(
                                'u-spacing-bottom-em',
                                styles.bodyText
                            )}
                        >
                            You will receive a one time SMS to download the app.
                            <em
                                className={classnames(
                                    styles.bodyTextBold,
                                    'u-spacing-top-5'
                                )}
                            >
                                If you are outside the US, make sure to include
                                your country's international dialing code prefix
                            </em>
                        </p>
                        <label>
                            <strong>Enter Your Phone Number</strong>
                            <input
                                className="u-spacing-top-em"
                                type="tel"
                                name="phone"
                                placeholder="555-123-4567"
                                required
                            />
                        </label>
                        <div className={styles.center}>
                            <button
                                className="button button--pill"
                                type="submit"
                            >
                                Text me the app
                            </button>
                        </div>
                    </div>
                </form>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        sdk: state.modal.extraData.sdk
    };
}

export default compose(connect(mapStateToProps))(TextMeTheAppModal);
