import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import classnames from 'classnames';

import { MODAL_TYPE_DELETE_ACCOUNT, hideModal } from '../redux/modules/modal';
import { logOut, deleteAccount } from '../redux/modules/user/index';
import { addToast } from '../redux/modules/toastNotification';

import Modal from './Modal';
import styles from './ConfirmModal.module.scss';

class DeleteAccountModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onClose: PropTypes.func,
        history: PropTypes.object,
        currentUser: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            password: '',
            isSocial:
                props.currentUser.profile.facebook_id ||
                props.currentUser.profile.twitter_id ||
                props.currentUser.profile.google_id ||
                props.currentUser.profile.apple_id
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleInputChange = (e) => {
        this.setState({
            password: e.currentTarget.value
        });
    };

    handleDeleteButtonFormSubmit = (e) => {
        e.preventDefault();
        const { dispatch, history } = this.props;

        dispatch(deleteAccount(this.state.password, this.state.isSocial))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: 'Your account has been deleted.'
                    })
                );

                dispatch(logOut());

                history.push('/');
                dispatch(hideModal());
                return;
            })
            .catch((err) => console.log(err));
    };

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderError(error) {
        if (!error) {
            return null;
        }

        let message;

        switch (error.errorcode) {
            case 1008:
                message = error.message;
                break;

            default:
                message =
                    'There was an error deleting your account. Try again later or contact us at support@audiomack.com.';
                break;
        }

        return <p className="auth__error">{message}</p>;
    }

    render() {
        const { currentUser } = this.props;
        const { loading, error } = currentUser.deleteAccount;

        let deleteAccountText = 'Delete account';

        if (loading) {
            deleteAccountText = 'Deleting…';
        }

        let passwordInput = (
            <Fragment>
                <label className={styles.label} htmlFor="delete-password">
                    This action cannot be undone. If you are sure you want to
                    continue, enter your password to delete your account.
                </label>
                <input
                    id="delete-password"
                    type="password"
                    required
                    value={this.state.password}
                    name="delete_password"
                    data-testid="deletePassword"
                    autoComplete="current-password"
                    autoCapitalize="none"
                    onChange={this.handleInputChange}
                />
                {this.renderError(error)}
            </Fragment>
        );

        if (this.state.isSocial) {
            passwordInput = (
                <label htmlFor="delete-password">
                    This action cannot be undone.
                </label>
            );
        }

        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_DELETE_ACCOUNT}
                onClose={this.props.onClose}
                title="Delete Your Account"
            >
                <form onSubmit={this.handleDeleteButtonFormSubmit}>
                    <div className="small-24">
                        {passwordInput}
                        <div
                            className={classnames(
                                'button-group',
                                'button-group--vert',
                                styles.buttonGroup
                            )}
                        >
                            <button
                                className="button button--pill button--wide button--danger"
                                type="submit"
                                data-testid="deletePasswordSubmit"
                            >
                                {deleteAccountText}
                            </button>
                            <div>
                                <button
                                    className="button button--pill button--transparent button--wide"
                                    onClick={this.handleCancelClick}
                                    data-testid="deletePasswordCancel"
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(DeleteAccountModal);
