import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { MODAL_TYPE_ADD_TO_PLAYLIST } from '../redux/modules/modal';
import { clearAddSong } from '../redux/modules/playlist';

import PlaylistAddPageContainer from '../playlist/PlaylistAddPageContainer';

import Modal from './Modal';
import styles from './AddToPlaylistModal.module.scss';

class AddToPlaylistModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        onSaveButtonClick: PropTypes.func
    };

    handleClose = () => {
        const { dispatch } = this.props;

        dispatch(clearAddSong());
    };

    render() {
        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_ADD_TO_PLAYLIST}
                onClose={this.handleClose}
            >
                <PlaylistAddPageContainer />
            </Modal>
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(AddToPlaylistModal);
