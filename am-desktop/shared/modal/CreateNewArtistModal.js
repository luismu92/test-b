import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Modal from './Modal';
import {
    MODAL_TYPE_CREATE_NEW_ARTIST,
    hideModal
} from '../redux/modules/modal';

import CreateNewArtistPageContainer from '../auth/CreateNewArtistPageContainer';

import styles from './ConfirmModal.module.scss';

class CreateNewArtistModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        associatedArtists: PropTypes.object,
        onClose: PropTypes.func
    };

    handleFormSubmit = () => {
        this.props.dispatch(hideModal());
    };

    render() {
        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_CREATE_NEW_ARTIST}
                onClose={this.props.onClose}
                showCloseButton={false}
                allowOverlayClose={
                    !this.props.associatedArtists.isCreatingAccount
                }
                title="Create New Artist"
            >
                <div className="row">
                    <div className="column small-24">
                        <p>
                            Fill out the fields below to create a new associated
                            artist.
                        </p>
                        <CreateNewArtistPageContainer
                            onFormSubmit={this.handleFormSubmit}
                        />
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        associatedArtists: state.monetizationAssociatedArtists
    };
}

export default compose(connect(mapStateToProps))(CreateNewArtistModal);
