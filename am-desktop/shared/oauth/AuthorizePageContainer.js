import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { parse } from 'query-string';
import classnames from 'classnames';
import { compose } from 'redux';
import Helmet from 'react-helmet';

import GlobalMeta from 'components/GlobalMeta';
import connectDataFetchers from 'lib/connectDataFetchers';
import { clamp } from 'utils/index';

import HeaderContainer from '../header/HeaderContainer';
import hideSidebarForComponent from '../hoc/hideSidebarForComponent';
import AndroidLoader from '../loaders/AndroidLoader';
import { validateToken, logIn, deleteToken } from '../redux/modules/oauth';

class AuthorizePageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        currentUser: PropTypes.object,
        oauth: PropTypes.object
    };

    componentDidMount() {
        if (window.opener) {
            const minHeight = 430;
            const maxHeight = 600;
            const minWidth = 580;
            const maxWidth = 800;
            const windowHeight =
                window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            const windowWidth =
                window.innerWidth ||
                document.documentElement.clientWidth ||
                document.body.clientWidth;
            const width = clamp(windowWidth, minWidth, maxWidth);
            const height = clamp(windowHeight, minHeight, maxHeight);

            if (windowHeight !== height || windowWidth !== width) {
                window.resizeTo(width, height);
            }
        }
    }

    componentWillUnmount() {
        const { dispatch, location } = this.props;
        const query = parse(location.search) || {};
        const token = query.oauth_token;

        dispatch(deleteToken(token));
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoginSubmit = (e) => {
        e.preventDefault();

        const { dispatch } = this.props;
        const form = e.currentTarget;
        const email = form.email.value;
        const password = form.password.value;
        const token = form.token.value;

        dispatch(
            logIn(token, {
                username: email,
                password
            })
        )
            .then((action) => {
                const redirectUrl = action.resolved;

                return this.loadSuccessfulLogIn(redirectUrl);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleAuthorizationClick = () => {
        const { dispatch, oauth, currentUser } = this.props;

        dispatch(
            logIn(oauth.token, {
                accessToken: currentUser.token
            })
        )
            .then((action) => {
                const redirectUrl = action.resolved;

                return this.loadSuccessfulLogIn(redirectUrl);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleCancelClick = () => {
        const { dispatch, location } = this.props;
        const query = parse(location.search) || {};
        const token = query.oauth_token;

        dispatch(deleteToken(token));

        if (window.opener) {
            window.close();
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getAuthErrors(errors) {
        return errors.reduce(
            (acc, error) => {
                if (!error) {
                    return acc;
                }
                // Seems like this error code doesnt change when getting different
                // errors
                if (error.errorcode === 1037) {
                    acc.facebook =
                        'Cannot retrieve necessary details from Facebook. Please make sure you have an email address set in your Facebook account and try again.';
                } else if (
                    error.description &&
                    error.description.toLowerCase().indexOf('email') !== -1
                ) {
                    acc.email = error.description;
                } else if (
                    error.description &&
                    error.description.toLowerCase().indexOf('password') !== -1
                ) {
                    acc.password = error.description;
                }

                return acc;
            },
            {
                email: '',
                password: '',
                facebook: ''
            }
        );
    }

    loadSuccessfulLogIn(redirectUrl) {
        if (window.opener) {
            window.opener.location.href = redirectUrl;
            window.close();
            return;
        }

        window.location.href = redirectUrl;
    }

    renderError(error) {
        if (!error.description) {
            return null;
        }

        return <p className="auth__error">{error.description}</p>;
    }

    renderAuthContent(token) {
        const { oauth, currentUser } = this.props;
        const errors = this.getAuthErrors([oauth.error]);
        const emailClass = classnames({
            'form__input--has-error': !!errors.email
        });
        const passwordClass = classnames({
            'form__input--has-error': !!errors.password
        });

        if (currentUser.isLoggedIn) {
            return (
                <div className="u-spacing-top u-spacing-bottom">
                    <button
                        className="button button--pill"
                        onClick={this.handleAuthorizationClick}
                    >
                        Authorize
                    </button>
                    <a
                        href={process.env.AM_URL}
                        className="button button--pill"
                    >
                        Cancel
                    </a>
                </div>
            );
        }

        return (
            <form
                action="/auth/login"
                method="post"
                onSubmit={this.handleLoginSubmit}
            >
                <input
                    className={emailClass}
                    type="email"
                    name="email"
                    data-testid="email"
                    placeholder="Email"
                    autoComplete="username"
                    autoCapitalize="none"
                    required
                />
                {errors.email &&
                    this.renderError({ description: errors.email })}
                <input
                    className={passwordClass}
                    type="password"
                    name="password"
                    data-testid="password"
                    placeholder="Password"
                    autoComplete="current-password"
                    autoCapitalize="none"
                    required
                />
                {errors.password &&
                    this.renderError({ description: errors.password })}
                <input type="hidden" value={token} name="token" />
                <div className="auth__button-group u-text-center">
                    <input
                        className="button button--pill"
                        type="submit"
                        data-testid="loginSubmit"
                        value={oauth.loading ? 'Authorizing...' : 'Authorize'}
                        disabled={oauth.loading}
                    />

                    <button
                        className="button button--link"
                        type="button"
                        onClick={this.handleCancelClick}
                    >
                        Cancel
                    </button>
                </div>
            </form>
        );
    }

    renderPermissions(accessLevel) {
        const title = <h4>This application will be able to:</h4>;
        const readwrite = (
            <ul>
                <li>Update your profile.</li>
                <li>Favorite and re-up tracks.</li>
                <li>Create, update and delete playlists.</li>
            </ul>
        );

        if (accessLevel === 'readwrite') {
            return (
                <Fragment>
                    {title}
                    {readwrite}
                </Fragment>
            );
        }

        return (
            <Fragment>
                {title}
                <ul>
                    <li>View your uploaded tracks.</li>
                    <li>View your playlists.</li>
                    <li>See your lists of favorites and re-ups.</li>
                </ul>

                <h5>Will not be able to:</h5>
                {readwrite}
            </Fragment>
        );
    }

    renderApplicationDescription(description) {
        if (!description) {
            return null;
        }

        return <p>{description}</p>;
    }

    renderApplicationName(appname) {
        if (!appname) {
            return null;
        }

        return <h4>Application name: {appname}</h4>;
    }

    renderApplicationLogo(logo) {
        if (!logo) {
            return null;
        }

        const logoStyle = {
            margin: 'auto'
        };

        return <img src={logo} alt="App logo" style={logoStyle} height={150} />;
    }

    renderContent() {
        const { location, oauth } = this.props;
        const query = parse(location.search) || {};
        const token = query.oauth_token;

        if (!token || oauth.validationError) {
            return (
                <Fragment>
                    <p>
                        The request token for this page is invalid. It may have
                        already been used, or expired because it is too old.
                        Please go back to the site or application that sent you
                        here and try again; it was probably just a mistake.
                    </p>
                    <p>
                        <a
                            href={process.env.AM_URL}
                            target="_blank"
                            rel="nofollow noopener"
                        >
                            Go to Audiomack
                        </a>
                    </p>
                </Fragment>
            );
        }

        if (oauth.isValidating || !(oauth.consumerName && oauth.permissions)) {
            return (
                <div className="u-vh u-flex-center column">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <Fragment>
                <h3 className="u-spacing-top u-text-center column small-24">
                    Authorize {oauth.consumerName}
                </h3>
                <div className="u-text-center column small-24">
                    {this.renderApplicationName(oauth.appname)}
                    {this.renderApplicationLogo(oauth.logo)}
                    {this.renderApplicationDescription(oauth.description)}
                    {this.renderAuthContent(token)}
                    {this.renderPermissions(oauth.permissions)}
                </div>
            </Fragment>
        );
    }

    render() {
        return (
            <div className="row">
                <GlobalMeta location={this.props.location} />
                <Helmet>
                    <title>Authorize an Application | Audiomack</title>
                </Helmet>
                <HeaderContainer bare />
                {this.renderContent()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        oauth: state.oauth,
        currentUser: state.currentUser
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps),
    hideSidebarForComponent
)(
    connectDataFetchers(AuthorizePageContainer, [
        (params, query) => {
            const token = query.oauth_token;

            return validateToken(token);
        }
    ])
);
