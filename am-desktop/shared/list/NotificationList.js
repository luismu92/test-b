import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';
import { verbDisplayTextMap } from 'constants/notifications/index';
import { getMusicUrl } from 'utils/index';
import { getTotalsFromPlaylists } from 'utils/notifications';

import Avatar from '../components/Avatar';

import HeartIcon from '../../../am-shared/icons/heart';
import ReupIcon from '../icons/retweet';
import PlusIcon from '../icons/plus';
import PlaylistIcon from '../icons/playlist';
import MessageIcon from '../icons/message';

export default class NotificationList extends Component {
    static propTypes = {
        notifications: PropTypes.array
    };

    icon(verb) {
        if (verb === 'favorite') {
            return (
                <HeartIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--heart" />
            );
        }

        if (verb === 'reup') {
            return (
                <ReupIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--retweet" />
            );
        }

        if (verb === 'playlisted') {
            return (
                <PlusIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--playlist" />
            );
        }

        if (verb === 'playlist_updated' || verb === 'playlist_updated_bundle') {
            return (
                <PlaylistIcon className="u-text-icon u-brand-color notification-list__icon" />
            );
        }

        if (verb === 'comment') {
            return (
                <MessageIcon className="u-text-icon u-brand-color notification-list__icon notification-list__icon--playlist" />
            );
        }

        return null;
    }

    activityTime(createdAt) {
        return createdAt;
    }

    renderPlaylists(playlists) {
        if (!playlists.length) {
            return null;
        }

        return (
            <ul>
                {playlists.map((playlist) => {
                    return (
                        <li key={playlist.playlist_id}>
                            {playlist.artist_name} added {playlist.count} songs
                            to{' '}
                            <Link
                                to={`playlist/${playlist.artist_url_slug}/${
                                    playlist.playlist_url_slug
                                }`}
                            >
                                {playlist.playlist_name}
                            </Link>
                        </li>
                    );
                })}
            </ul>
        );
    }

    renderNotificationContent(notification) {
        const {
            actor,
            verb,
            object,
            target,
            created_at: createdAt,
            time
        } = notification;
        const targetTitle = (target || {}).title || 'untitled';
        let objectUrl;
        let objectLink = 'you';
        let avatar;
        let extraMessage = null;
        let verbText = verbDisplayTextMap[verb];
        let stats;

        if (object) {
            objectUrl =
                object.type !== 'artist'
                    ? getMusicUrl(object)
                    : `/artist/${object.url_slug}`;

            if (object.type) {
                objectLink = <Link to={objectUrl}>{object.title}</Link>;
            }

            avatar = (
                <Link to={objectUrl}>
                    <Avatar
                        type={object.type}
                        size={60}
                        className="notification-list__list-item-right"
                        image={object.image_base || object.image}
                        square
                    />
                </Link>
            );
        }

        if (verb === 'playlist_updated') {
            return (
                <div className="notification-list__content">
                    <Link to={`/artist/${actor.url_slug}`}>
                        <Avatar
                            type="artist"
                            size={60}
                            className="notification-list__list-item-left"
                            image={actor.image_base || actor.image}
                        />
                    </Link>
                    <span className="notification-list__list-item-content">
                        {this.icon(verb)}
                        <Link to={`/artist/${actor.url_slug}`}>
                            <strong>{actor.name}</strong>
                        </Link>{' '}
                        added "{targetTitle}" to the{' '}
                        <span className="notification-list__list-item-target">
                            "{objectLink}"
                        </span>{' '}
                        playlist
                        <p className="notification-list__list-item-meta">
                            {moment(
                                this.activityTime(createdAt || time)
                            ).fromNow()}
                        </p>
                    </span>
                    {avatar}
                </div>
            );
        }

        if (verb === 'playlist_updated_bundle') {
            if (typeof notification.extra_context.playlists !== 'undefined') {
                extraMessage = this.renderPlaylists(
                    notification.extra_context.playlists
                );
            }
            stats = getTotalsFromPlaylists(
                notification.extra_context.playlists
            );

            return (
                <div
                    className="notification-list__content"
                    style={{ display: 'block' }}
                >
                    <span className="notification-list__list-item-content">
                        {this.icon(verb)}
                        {stats.totalSongs} songs were added to{' '}
                        {stats.totalPlaylists} playlists you follow:{' '}
                    </span>
                    <div style={{ padding: '0 10px' }}>{extraMessage}</div>
                </div>
            );
        }

        if (typeof notification.extra_context.comment !== 'undefined') {
            extraMessage = (
                <div
                    dangerouslySetInnerHTML={{
                        __html: notification.extra_context.comment
                    }}
                />
            ); //eslint-disable-line

            const truncate = (input, length) =>
                input.length > length
                    ? `${input.substring(0, length)}...`
                    : input;
            const parentComment =
                notification.extra_context.parent !== null
                    ? notification.extra_context.parent.replace(/<br>/gi, ' ')
                    : null;

            if (parentComment !== null) {
                verbText = (
                    <span>
                        replied to your comment (
                        <span title={parentComment}>
                            {truncate(parentComment, 20)}
                        </span>
                        ) on
                    </span>
                );
            }
        }

        return (
            <div className="notification-list__content">
                <Link to={`/artist/${actor.url_slug}`}>
                    <Avatar
                        size={60}
                        type="artist"
                        className="notification-list__list-item-left"
                        image={actor.image_base || actor.image}
                    />
                </Link>
                <span className="notification-list__list-item-content">
                    {this.icon(verb)}
                    <Link to={`/artist/${actor.url_slug}`}>
                        <strong>{actor.name}</strong>
                    </Link>{' '}
                    {verbText}{' '}
                    <span className="notification-list__list-item-target">
                        {objectLink}
                    </span>
                    {extraMessage}
                    <p className="notification-list__list-item-meta">
                        {moment(this.activityTime(createdAt || time)).fromNow()}
                    </p>
                </span>
                {avatar}
            </div>
        );
    }

    renderNotification(notification, i) {
        const { actor, seen } = notification;

        if (!actor) {
            return null;
        }

        const klass = classnames(
            'notification-list__list-item notification-list__list-item--main',
            {
                'notification-list__list-item--unread': !seen
            }
        );

        return (
            <li
                className="column small-24 medium-14 notification-list__item-wrap"
                key={i}
            >
                <div className={klass}>
                    {this.renderNotificationContent(notification)}
                </div>
            </li>
        );
    }

    render() {
        return (
            <ul className="row notification-list notification-list--main">
                {this.props.notifications.map(this.renderNotification, this)}
            </ul>
        );
    }
}
