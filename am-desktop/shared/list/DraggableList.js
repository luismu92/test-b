/**
 * IMPORTANT NOTE ABOUT USING THIS COMPONENT
 *
 * Since this creates a virtualized list of potentially an infinite amount of
 * items, there are a lot of optimizations to make something like that work.
 * Because of this, you must know that passing any sort of `items` to the
 * <DraggableList /> component that contain input elements must be taken care
 * of with special care.
 *
 * Take the album edit/upload page for example. Each album track is an item
 * within <DraggableList /> has multiple input elements. We can not allow the
 * parent component that contains <DraggableList /> to pass input values back
 * like you normally would within React because DraggableList does a lot of caching
 * to keep performant. To update the children of a draggable list you can
 * get a ref to the instance and call `forceGridUpdate`. However this does not
 * work well if you're trying to allow text input changes because calling
 * `forceGridUpdate` re-renders the children, which in turn can make the cursor
 * jump to the end of the input while the user is typing. Because of this, high
 * velocity event changes should be kept within the actual DraggableList items.
 *
 * Related: https://github.com/audiomack/audiomack-js/issues/1074
 *
 * This is not necessarily due to react-virtualized as there may be a way to reflect
 * updates on certain prop changes as mentioned here: https://stackoverflow.com/questions/52769760/react-virtualized-list-item-does-not-re-render-with-changed-props-until-i-scroll
 *
 * It may have more to do with the wrapper of <DraggableList /> around the lib
 * itself. This is something that needs to be taken into account if any major changes
 * happen here. react-virtualized takes care of infinite items along with window
 * scrolling while dragging whether in the context of the window or a fixed height
 * container.
 */
import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import {
    SortableContainer,
    SortableElement,
    SortableHandle
} from 'react-sortable-hoc';
import { List, AutoSizer, WindowScroller } from 'react-virtualized';
import classnames from 'classnames';

const SortableItem = SortableElement(
    ({
        value,
        list,
        style,
        dragHandle: DragHandle,
        multiSelect,
        onItemSelect,
        selectedItems
    }) => {
        let handle;
        let selectHandle;
        let draggingMessage;
        let selectedItemIds;

        if (selectedItems) {
            selectedItemIds = selectedItems.map((item) => item.props.track.id);
        }

        if (DragHandle) {
            handle = <DragHandle />;
        }

        if (multiSelect) {
            const klass = classnames('u-select-handle', {
                'u-select-handle--active':
                    selectedItems &&
                    selectedItemIds.includes(value.props.track.id)
            });

            selectHandle = (
                <button
                    className={klass}
                    data-selected={selectedItems.length}
                    onClick={(e) => onItemSelect(value, list, e)} //eslint-disable-line
                />
            );
        }

        if (selectedItems && selectedItems.length) {
            draggingMessage = (
                <div className="u-dragging-message">
                    <p>
                        You are moving{' '}
                        <span className="u-text-orange u-fw-700">
                            {selectedItems.length}
                        </span>{' '}
                        item{selectedItems.length === 1 ? '' : 's'}
                    </p>
                </div>
            );
        }

        const itemClass = classnames('u-sortable-item', {
            ['u-selectable-item']: multiSelect
        });

        return (
            <div className={itemClass} style={style}>
                {selectHandle}
                {handle}
                {value}
                {draggingMessage}
            </div>
        );
    }
);

class VirtualList extends Component {
    static propTypes = {
        items: PropTypes.array,
        containerProps: PropTypes.object,
        virtualListProps: PropTypes.object,
        useWindow: PropTypes.bool,
        dragHandle: PropTypes.element,
        onRenderDragHandle: PropTypes.func,
        onRowHeight: PropTypes.func,
        onRowRender: PropTypes.func,
        measurementDelay: PropTypes.number,
        multiSelect: PropTypes.bool,
        onItemSelect: PropTypes.func,
        selectedItems: PropTypes.array
    };

    static defaultProps = {
        virtualListProps: {},
        containerProps: {},
        onRowHeight() {},
        measurementDelay: 0
    };

    componentWillUnmount() {
        this._dragHandle = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    ////////////////////
    // Helper methods //
    ////////////////////

    rowHeight = ({ index }) => {
        const { items } = this.props;
        const value = items[index];

        return this.props.onRowHeight(value, index) || 50;
    };

    rowRender = ({ index, style }) => {
        const {
            items,
            dragHandle,
            onRenderDragHandle,
            multiSelect,
            selectedItems
        } = this.props;
        const value = items[index];

        if (dragHandle && !this._dragHandle) {
            this._dragHandle = SortableHandle(() => dragHandle);
        }

        const handle = onRenderDragHandle(this._dragHandle, index);

        return (
            <SortableItem
                key={index}
                index={index}
                list={items}
                value={value}
                style={style}
                dragHandle={handle}
                multiSelect={multiSelect}
                onItemSelect={this.props.onItemSelect}
                selectedItems={selectedItems}
            />
        );
    };

    render() {
        const { items, useWindow } = this.props;

        let content;

        if (useWindow) {
            content = (
                <WindowScroller
                    ref={(instance) => {
                        this.WindowScroller = instance;
                    }}
                >
                    {({ height, isScrolling, onChildScroll, scrollTop }) => (
                        <AutoSizer disableHeight>
                            {({ width }) => (
                                <List
                                    {...this.props.virtualListProps}
                                    autoHeight
                                    ref={(instance) => {
                                        this.List = instance;
                                    }}
                                    height={height}
                                    isScrolling={isScrolling}
                                    onScroll={onChildScroll}
                                    rowHeight={this.rowHeight}
                                    rowRenderer={this.rowRender}
                                    rowCount={items.length}
                                    scrollTop={scrollTop}
                                    width={width}
                                />
                            )}
                        </AutoSizer>
                    )}
                </WindowScroller>
            );
        } else {
            content = (
                <AutoSizer>
                    {({ height, width }) => (
                        <List
                            {...this.props.virtualListProps}
                            ref={(instance) => {
                                this.List = instance;
                            }}
                            rowHeight={this.rowHeight}
                            rowRenderer={this.rowRender}
                            rowCount={items.length}
                            width={width}
                            height={height}
                        />
                    )}
                </AutoSizer>
            );
        }

        return <div {...this.props.containerProps}>{content}</div>;
    }
}

/*
 * Important note:
 * To access the ref of a component that has been wrapped with the SortableContainer HOC,
 * you *must* pass in {withRef: true} as the second param. Refs are opt-in.
 */
const SortableList = SortableContainer(VirtualList, { withRef: true });

// eslint-disable-next-line
export default class DraggableList extends Component {
    static propTypes = {
        items: PropTypes.array.isRequired,
        onRowHeight: PropTypes.func,
        onRenderDragHandle: PropTypes.func,
        dragHandle: PropTypes.element,
        draggingClass: PropTypes.string,
        virtualListProps: PropTypes.object,
        useWindowAsScrollContainer: PropTypes.bool,
        containerProps: PropTypes.object,
        onDragFinish: PropTypes.func,
        multiSelect: PropTypes.bool,
        selectedItems: PropTypes.array,
        onItemSelect: PropTypes.func
    };

    static defaultProps = {
        onDragFinish(fromIndex, toIndex) {
            console.log('from', fromIndex, 'to', toIndex);
        },
        onRenderDragHandle(handle) {
            return handle;
        },
        containerProps: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            draggingItems: null
        };
    }

    componentDidUpdate(prevState) {
        if (prevState.selectedItems !== this.props.selectedItems) {
            this.forceUpdateGrid();
        }
    }

    componentWillUnmount() {
        this._container = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSortStart = () => {
        this.setState({
            draggingItems: true
        });
    };

    handleSortEnd = ({ oldIndex, newIndex }) => {
        if (oldIndex === newIndex) {
            this.setState({
                draggingItems: false
            });

            return;
        }

        this.setState({
            draggingItems: false
        });

        this.props.onDragFinish(oldIndex, newIndex);

        // We need to inform React Virtualized that the items have changed heights
        this.recomputeRowHeights();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getContainer = (comp) => {
        if (!this._container) {
            this._container = findDOMNode(comp);
        }

        return this._container.children[0].children[0];
    };

    updateWindow = () => {
        const instance = this.SortableList.getWrappedInstance().WindowScroller;

        if (!instance) {
            return;
        }

        instance.updatePosition();
    };

    scrollToRow(index) {
        const instance = this.SortableList.getWrappedInstance();

        return instance.List.scrollToRow(index);
    }

    forceUpdateGrid() {
        const instance = this.SortableList.getWrappedInstance();

        instance.List.forceUpdateGrid();
    }

    recomputeRowHeights() {
        const instance = this.SortableList.getWrappedInstance();

        if (instance !== null && instance.List !== null) {
            instance.List.recomputeRowHeights();
            instance.forceUpdate();
        }
    }

    render() {
        const { containerProps } = this.props;

        const helperClass = classnames('u-dragging', {
            [this.props.draggingClass]: this.props.draggingClass
        });

        const containerClass = classnames(containerProps.className, {
            'u-items-dragging': this.state.draggingItems
        });

        return (
            <SortableList
                useWindowAsScrollContainer={
                    this.props.useWindowAsScrollContainer
                }
                useWindow={this.props.useWindowAsScrollContainer}
                helperClass={helperClass}
                onRowHeight={this.props.onRowHeight}
                containerProps={{
                    ...containerProps,
                    className: containerClass
                }}
                ref={(instance) => {
                    this.SortableList = instance;
                }}
                onRenderDragHandle={this.props.onRenderDragHandle}
                dragHandle={this.props.dragHandle}
                useDragHandle={!!this.props.dragHandle}
                getContainer={this.getContainer}
                items={this.props.items}
                virtualListProps={this.props.virtualListProps}
                onSortStart={this.handleSortStart}
                onSortEnd={this.handleSortEnd}
                multiSelect={this.props.multiSelect}
                onItemSelect={this.props.onItemSelect}
                selectedItems={this.props.selectedItems}
            />
        );
    }
}
