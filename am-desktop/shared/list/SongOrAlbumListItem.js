import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { renderFeaturingLinks } from 'utils/index';

import MusicStats from '../components/MusicStats';
import RetweetIcon from '../icons/retweet';

export default class SongOrAlbumListItem extends Component {
    static propTypes = {
        item: PropTypes.object
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getArtistName(item) {
        return item.artist.name || item.artist;
    }

    renderFeaturing(musicItem) {
        if (!musicItem.featuring) {
            return null;
        }

        return (
            <p className="list-item__details-featuring">
                {renderFeaturingLinks(musicItem.featuring)}
            </p>
        );
    }

    renderNumbers(index, showNumbers) {
        if (isNaN(index)) {
            return null;
        }

        if (!showNumbers) {
            return null;
        }

        return <span className="feed-number">{index + 1}</span>;
    }

    renderStats(stats) {
        if (!stats) {
            return null;
        }

        return <MusicStats stats={stats} />;
    }

    render() {
        const { item } = this.props;
        const { uploader, stats } = item;

        let reupText = null;

        if (item.repost) {
            reupText = (
                <p>
                    <RetweetIcon className="u-text-icon" /> Re-up
                </p>
            );
        }

        return (
            <div className="list-item" role="listitem">
                <div className="list-item__inner u-clearfix">
                    <div className="list-item__image">
                        <Link
                            to={`/${item.type}/${uploader.url_slug}/${
                                item.url_slug
                            }`}
                        >
                            <img src={item.image} alt={item.title} />
                        </Link>
                    </div>
                    <div className="list-item__details">
                        {reupText}
                        <Link
                            className="list-item__details-link"
                            to={`/${item.type}/${uploader.url_slug}/${
                                item.url_slug
                            }`}
                        >
                            <p className="list-item__details-artist">
                                {this.getArtistName(item)}
                            </p>
                            <p className="list-item__details-title">
                                {item.title}
                            </p>
                        </Link>
                        {this.renderFeaturing(item)}
                        {this.renderStats(stats)}
                    </div>
                </div>
            </div>
        );
    }
}
