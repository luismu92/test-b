import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import AndroidLoader from 'components/loaders/AndroidLoader';
import MusicIcon from 'icons/music';

import AlbumsIcon from '../icons/albums';

import MusicEditTrackContainer from '../edit/MusicEditTrackContainer';
import styles from './MultiSelectMusicList.module.scss';

class MultiSelectMusicList extends Component {
    static propTypes = {
        selectedItems: PropTypes.array,
        list: PropTypes.array,
        onSelect: PropTypes.func,
        loading: PropTypes.bool,
        hasExpandableTracks: PropTypes.bool,
        shouldSeparateTypes: PropTypes.bool,
        shouldRetainSelected: PropTypes.bool
    };

    static defaultProps = {
        selectedItems: [],
        shouldRetainSelected: false,
        shouldSeparateTypes: false
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedItems: props.selectedItems,
            rangeStart: null
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSelect = (item, e) => {
        const { list, shouldRetainSelected } = this.props;
        const { rangeStart } = this.state;
        const index = list.indexOf(item);

        if (
            e.nativeEvent.shiftKey &&
            typeof rangeStart === 'number' &&
            !shouldRetainSelected
        ) {
            let startIndex = rangeStart;
            let endIndex = index;

            if (index < rangeStart) {
                startIndex = index;
                endIndex = rangeStart;
            }

            this.selectItems(startIndex, endIndex);
            return;
        }

        this.setState({
            rangeStart: index
        });

        this.selectItem(item);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    selectItems(startIndex, endIndex) {
        const { list } = this.props;
        const item = list[endIndex];
        const newList = this.props.list.slice(startIndex, endIndex + 1);

        this.setState(
            {
                selectedItems: newList
            },
            () => {
                this.props.onSelect(item, newList);
            }
        );
    }

    selectItem(item) {
        this.setState(
            ({ selectedItems }) => {
                const selectedItemIds = selectedItems.map((track) => track.id);

                if (selectedItemIds.includes(item.id)) {
                    return {
                        selectedItems: selectedItems.filter(
                            (value) => value.id !== item.id
                        )
                    };
                }

                return {
                    selectedItems: [...selectedItems, item]
                };
            },
            () => {
                this.props.onSelect(item, this.state.selectedItems);
            }
        );
    }

    maybeRenderLoader() {
        if (!this.props.loading) {
            return null;
        }

        return (
            <div className="u-overlay-loader">
                <AndroidLoader />
            </div>
        );
    }

    renderCounts() {
        const { list, loading } = this.props;

        if (loading) {
            return null;
        }

        const items = list.reduce((acc, track) => {
            let typeCount = acc[track.type] || 0;

            typeCount += 1;

            acc[track.type] = typeCount;

            return acc;
        }, {});

        return (
            <span className={styles.countWrapper}>
                Songs <span className={styles.count}>({items.song || 0})</span>
                <span className={styles.middot}>&middot;</span>
                Albums{' '}
                <span className={styles.count}>({items.album || 0})</span>
            </span>
        );
    }

    renderSeparatedNonSelected() {
        const { list, hasExpandableTracks } = this.props;
        const { selectedItems } = this.state;
        const items = list.reduce((acc, track, i) => {
            const typeList = acc[track.type] || [];

            typeList.push(
                <MusicEditTrackContainer
                    key={`${track.id}:${i}`}
                    index={i}
                    track={track}
                    hasExpandableTracks={hasExpandableTracks}
                    onSelect={this.handleSelect}
                    selectedItems={selectedItems}
                    isSelectable
                />
            );

            acc[track.type] = typeList;

            return acc;
        }, {});

        let songTitle;

        if (items.song && items.song.length) {
            songTitle = (
                <h5 className={styles.listHeader}>
                    <MusicIcon />
                    Songs
                </h5>
            );
        }

        let albumTitle;

        if (items.album && items.album.length) {
            albumTitle = (
                <h5 className={styles.listHeader}>
                    <AlbumsIcon />
                    Albums
                </h5>
            );
        }

        return (
            <Fragment>
                {songTitle}
                {items.song}
                {albumTitle}
                {items.album}
            </Fragment>
        );
    }

    renderLinearNonSelected() {
        const { list, hasExpandableTracks } = this.props;
        const { selectedItems } = this.state;
        const items = list.map((track, i) => {
            return (
                <MusicEditTrackContainer
                    key={`${track.id}:${i}`}
                    index={i}
                    track={track}
                    hasExpandableTracks={hasExpandableTracks}
                    onSelect={this.handleSelect}
                    selectedItems={selectedItems}
                    isSelectable
                />
            );
        });

        let title;

        if (selectedItems.length) {
            title = <h5 className={styles.listHeader}>Choose tracks</h5>;
        }

        return (
            <Fragment>
                {title}
                {items}
            </Fragment>
        );
    }

    renderRetainedLists() {
        const { list, hasExpandableTracks, shouldSeparateTypes } = this.props;
        const { selectedItems } = this.state;
        const selected = selectedItems.map((track, i) => {
            return (
                <MusicEditTrackContainer
                    key={`selected:${track.id}:${i}`}
                    index={i}
                    track={track}
                    onSelect={this.handleSelect}
                    selectedItems={selectedItems}
                    hasExpandableTracks={hasExpandableTracks}
                    isSelectable
                />
            );
        });
        const nonSelected = shouldSeparateTypes
            ? this.renderSeparatedNonSelected(list, selected.length)
            : this.renderLinearNonSelected(list, selected.length);

        let selectedTitle;

        if (selected.length) {
            selectedTitle = (
                <h5 className={styles.listHeader}>{`Selected tracks (${
                    selected.length
                })`}</h5>
            );
        }

        return (
            <Fragment>
                {this.renderCounts()}
                {selectedTitle}
                {selected}
                <div className="u-pos-relative">
                    {nonSelected}
                    {this.maybeRenderLoader()}
                </div>
            </Fragment>
        );
    }

    renderNormalList() {
        const { list, hasExpandableTracks } = this.props;
        const { selectedItems } = this.state;
        const items = list.map((track, i) => {
            return (
                <MusicEditTrackContainer
                    key={`${track.id}:${i}`}
                    index={i}
                    track={track}
                    onSelect={this.handleSelect}
                    selectedItems={selectedItems}
                    hasExpandableTracks={hasExpandableTracks}
                    expandableAlbum
                    isSelectable
                />
            );
        });

        return (
            <div className="u-pos-relative">
                {this.renderCounts()}
                {items}
                {this.maybeRenderLoader()}
            </div>
        );
    }

    render() {
        return this.props.shouldRetainSelected
            ? this.renderRetainedLists()
            : this.renderNormalList();
    }
}

export default MultiSelectMusicList;
