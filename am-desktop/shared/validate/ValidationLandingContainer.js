import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import requireAuth from '../hoc/requireAuth';
import hideSidebarForComponent from '../hoc/hideSidebarForComponent';

import ValidationLanding from './ValidationLanding';
import ValidateAgreementModal from '../modal/ValidateAgreementModal';
import ValidateArtistModal from '../modal/ValidateArtistModal';

import {
    showModal,
    MODAL_TYPE_VALIDATE_AGREEMENT,
    MODAL_TYPE_VALIDATE_ARTIST,
    hideModal
} from '../redux/modules/modal';

class ValidationLandingContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        document.body.classList.add('no-bottom-padding', 'no-header-border');
    }

    componentWillUnmount() {
        document.body.classList.remove('no-bottom-padding', 'no-header-border');
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleModals = () => {
        const { currentUser, dispatch } = this.props;
        const plays = currentUser.profile.stats['plays-raw'];
        const uploads = currentUser.profile.upload_count_excluding_reups;
        const playsRequired = parseInt(
            process.env.ARTIST_VALIDATION_REQUIRED_TOTAL_PLAYS,
            10
        );
        const uploadsRequired = parseInt(
            process.env.ARTIST_VALIDATION_REQUIRED_MUSIC_UPLOADS,
            10
        );

        if (plays < playsRequired || uploads < uploadsRequired) {
            dispatch(
                showModal(MODAL_TYPE_VALIDATE_ARTIST, {
                    handleConfirm: () =>
                        dispatch(hideModal(MODAL_TYPE_VALIDATE_ARTIST))
                })
            );
        } else {
            dispatch(showModal(MODAL_TYPE_VALIDATE_AGREEMENT));
        }
    };

    render() {
        return (
            <Fragment>
                <ValidationLanding
                    onShowModal={this.handleModals}
                    currentUser={this.props.currentUser}
                />
                <ValidateAgreementModal history={this.props.history} />
                <ValidateArtistModal />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps),
    hideSidebarForComponent
)(ValidationLandingContainer);
