import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Helmet from 'react-helmet';
import { parse } from 'query-string';
import { previewFile } from 'utils/index';
import { getTwitterOauthVerifier, getInstagramAccessToken } from 'utils/social';
import { canUserJoinCreatorAuthentication } from 'utils/currentUser';
import analytics, { eventCategory, eventAction } from 'utils/analytics';

import { resendEmail, updateSlug } from '../redux/modules/user/index';
import { getTwitterRequestToken } from '../redux/modules/user';
import { showModal, MODAL_TYPE_LABEL_SIGNUP } from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import { validateArtist } from 'redux/modules/user/index';
import ValidateArtist from './ValidateArtist';

import requireAuth from '../hoc/requireAuth';
import LabelSignupModal from '../modal/LabelSignupModal';

class ValidateArtistContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        match: PropTypes.object,
        currentUser: PropTypes.object,
        steps: PropTypes.array,
        history: PropTypes.object
    };

    static defaultProps = {
        steps: [
            {
                label: 'Verify Your Email',
                name: 'Verify Email',
                number: 1
            },
            {
                label: 'Complete Your Profile',
                name: 'Complete Profile',
                number: 2
            },
            {
                label: 'Authenticate Your Socials',
                name: 'Authenticate Socials',
                number: 3
            },
            {
                label: 'Review & Confirm',
                name: 'Confirm',
                number: 4
            }
        ]
    };

    constructor(props) {
        super(props);

        this.state = {
            // Step Progress Bar
            currentStep: props.steps[0],
            maxVisitedStep: props.steps[0].number,
            // Profile Step
            artist: props.currentUser.profile,
            editableFieldValues: {},
            isEdit: false,
            // Social Step
            twitterToken: null,
            twitterSecret: null,
            socials: {}
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.maybeShowLabelSignupModal();
        this.maybeRedirect();
    }

    componentDidUpdate(prevProps, prevState) {
        const { match, steps } = this.props;
        const locationStep = parseInt(match.params.step, 10) || 1;
        const locationStepIndex = locationStep - 1;
        const changedSteps = prevState.currentStep !== steps[locationStepIndex];
        const isGoingBack =
            locationStep < parseInt(prevProps.match.params.step, 10);

        if (isGoingBack && changedSteps) {
            // eslint-disable-next-line
            this.setState({
                currentStep: steps[locationStepIndex]
            });
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLabelSignup = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
    };

    handleNextStep = () => {
        const {
            steps,
            currentUser: { profile }
        } = this.props;
        const { currentStep, maxVisitedStep, socials } = this.state;

        // the zero-based index for the next upload step is equal to the one-based index of the current upload step number
        const nextIndex = currentStep.number;

        switch (currentStep.number) {
            case 2:
                // Check if the artist doesn't have twitter account
                if (!profile.twitter_id && !socials.twitter) {
                    this.setTwitterToken().catch((err) => {
                        console.error(err);
                    });
                }

                this.setState({
                    currentStep: steps[nextIndex]
                });
                break;

            case 1:
            case 3:
                this.setState({
                    currentStep: steps[nextIndex]
                });
                break;

            case 4:
                // Finish step
                this.handleFinish();
                break;
            default:
                break;
        }

        if (steps[nextIndex] && steps[nextIndex].number > maxVisitedStep) {
            this.setState({
                maxVisitedStep: steps[nextIndex].number
            });
        }
    };

    handleStepClick = (e) => {
        e.preventDefault();

        const { steps } = this.props;
        const { currentStep, maxVisitedStep } = this.state;
        const button = e.currentTarget;
        const stepNumber = button.getAttribute('data-step-number');
        const index = parseInt(stepNumber, 10) - 1;

        button.blur();

        if (stepNumber <= maxVisitedStep && stepNumber !== currentStep.number) {
            this.setState({
                currentStep: steps[index]
            });
        }
    };

    handleEditBanner = (imageData) => {
        const { editableFieldValues } = this.state;
        editableFieldValues.image_banner = imageData;

        this.setState({
            editableFieldValues: editableFieldValues
        });
    };

    handleRemoveBanner = () => {
        const { editableFieldValues } = this.state;
        editableFieldValues.image_banner = '';

        this.setState({
            editableFieldValues: editableFieldValues
        });
    };

    handleEditFieldInput = (e) => {
        const { dispatch } = this.props;
        const input = e.currentTarget;
        const { name, type } = input;
        let { value } = input;

        if (name === 'image') {
            previewFile(e.target.files[0])
                .then((result) => {
                    this.setState((prevState) => {
                        const newState = {
                            ...prevState
                        };

                        newState.editableFieldValues[name] = result;
                        return newState;
                    });
                    return;
                })
                .catch((err) => {
                    dispatch(
                        addToast({
                            action: 'message',
                            message: `The image could not be obtained: ${
                                err.message
                            }`
                        })
                    );
                });
            return;
        }

        if (type === 'select-one') {
            value = input.options[input.selectedIndex].value;
        }

        const newState = {
            ...this.state
        };

        newState.editableFieldValues[name] = value;

        this.setState(newState);
    };

    handleEditProfile = (e) => {
        e.preventDefault();

        const { steps } = this.props;
        const button = e.currentTarget;
        const container = button.parentElement;
        const input = container.querySelector('input');
        const textarea = container.querySelector('textarea');
        const select = container.querySelector('select');
        const dataName = button.getAttribute('data-name');
        const name = dataName || (input || textarea || select || {}).name;

        if (name) {
            analytics.track(eventCategory.creatorAuth, {
                eventAction: eventAction.editProfile,
                eventLabel: name
            });
        }

        this.setState({
            isEdit: true,
            currentStep: steps[1]
        });
    };

    handleEditVerifiedEmail = (e) => {
        e.preventDefault();

        const { steps } = this.props;

        this.setState({
            currentStep: steps[0]
        });
    };

    handleUpdateArtist = (e) => {
        e.preventDefault();
        const { dispatch } = this.props;
        const { editableFieldValues, artist } = this.state;
        const urlSlug = editableFieldValues.url_slug
            ? editableFieldValues.url_slug
            : artist.url_slug;

        // Check new UrlSlug
        dispatch(updateSlug(urlSlug))
            .then(() => {
                // Merging artist data with new one
                const newArtistData = {
                    ...artist,
                    ...editableFieldValues
                };

                this.setState({
                    artist: newArtistData,
                    isEdit: false
                });
                return;
            })
            .catch((err) => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: err.message
                    })
                );
            });
    };

    handleSocialLink = (e) => {
        e.preventDefault();

        const { socials } = this.state;
        const {
            currentUser: { profile }
        } = this.props;
        const socialName = e.currentTarget.id;

        switch (socialName) {
            case 'twitter':
                if (!socials.twitter && !profile.twitter_id) {
                    this.doTwitterLogin();
                }
                break;

            case 'instagram':
                if (!socials.instagram && !profile.instagram_id) {
                    this.doInstagramLogin();
                }
                break;

            default:
                break;
        }
    };

    handleResendEmailClick = () => {
        const { dispatch } = this.props;

        this.setState({
            confirmMessageOpen: false
        });

        dispatch(resendEmail(window.location.pathname));
        dispatch(
            addToast({
                action: 'message',
                message: 'An email confirmation has been sent.'
            })
        );
    };

    handleFinish = () => {
        const { dispatch, history, currentUser } = this.props;
        const { artist, socials } = this.state;
        const socialBody = Object.keys(socials).map((i) => socials[i]);

        // Image base64 without 'data:image/jpeg;base64,' part
        if (artist.image.includes('base64,')) {
            artist.image = artist.image.split(',')[1];
        }

        if (artist.image_banner.includes('base64,')) {
            artist.image_banner = artist.image_banner.split(',')[1];
        }

        // Add uploads and total plays to artist object
        const artistData = {
            ...artist,
            uploads: currentUser.profile.upload_count_excluding_reups,
            total_plays: currentUser.profile.stats['plays-raw']
        };

        dispatch(validateArtist(artistData, socialBody))
            .then(() => {
                history.push({
                    pathname: '/dashboard?utm_content=authentication-success'
                });
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message:
                            'Application could not be submitted, please contact support.'
                    })
                );
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    setTwitterToken() {
        return this.props.dispatch(getTwitterRequestToken()).then((result) => {
            this.setState({
                twitterToken: result.resolved.token,
                twitterSecret: result.resolved.secret
            });
            return;
        });
    }

    maybeRedirect() {
        const { history, currentUser } = this.props;
        const verifyStatus = currentUser.profile.verified;
        const plays = currentUser.profile.stats['plays-raw'];
        const uploads = currentUser.profile.upload_count_excluding_reups;
        const playsRequired = parseInt(
            process.env.ARTIST_VALIDATION_REQUIRED_TOTAL_PLAYS,
            10
        );
        const uploadsRequired = parseInt(
            process.env.ARTIST_VALIDATION_REQUIRED_MUSIC_UPLOADS,
            10
        );

        if (
            verifyStatus === 'banned' ||
            verifyStatus === 'authenticated-pending' ||
            verifyStatus === 'authenticated' ||
            verifyStatus === 'yes' ||
            verifyStatus === 'tastemaker' ||
            verifyStatus === 'verified-pending' ||
            verifyStatus === 'verified-declined' ||
            plays < playsRequired ||
            uploads < uploadsRequired
        ) {
            history.replace({
                pathname: '/dashboard'
            });
        }
    }

    maybeShowLabelSignupModal() {
        const { dispatch, location, currentUser } = this.props;

        if (
            parse(location.search).enableAmp &&
            currentUser.profile.label_owner &&
            currentUser.profile.label_owner_status === 'invited'
        ) {
            dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
        }
    }

    doTwitterLogin = () => {
        const { twitterToken, twitterSecret } = this.state;

        analytics.track(eventCategory.creatorAuth, {
            eventAction: eventAction.twitter,
            eventLabel: 'Attempt'
        });

        return getTwitterOauthVerifier(twitterToken).then((oauthVerifier) => {
            if (oauthVerifier) {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        socials: {
                            ...prevState.socials,
                            twitter: {
                                t_token: twitterToken,
                                t_secret: twitterSecret,
                                t_oauth_verifier: oauthVerifier
                            }
                        }
                    };
                });
            } else {
                // Load twitter tokens again
                this.setTwitterToken();
            }

            analytics.track(eventCategory.creatorAuth, {
                eventAction: eventAction.twitter,
                eventLabel: 'Success'
            });

            return;
        });
    };

    doInstagramLogin = () => {
        analytics.track(eventCategory.creatorAuth, {
            eventAction: eventAction.instagram,
            eventLabel: 'Attempt'
        });

        return getInstagramAccessToken().then((code) => {
            // Check code
            if (code) {
                // Remove #_ from code
                const instagramCode = code.split('#')[0];

                this.setState((prevState) => {
                    return {
                        ...prevState,
                        socials: {
                            ...prevState.socials,
                            instagram: {
                                instagram_token: instagramCode
                            }
                        }
                    };
                });

                analytics.track(eventCategory.creatorAuth, {
                    eventAction: eventAction.instagram,
                    eventLabel: 'Success'
                });
            }

            return;
        });
    };

    render() {
        const { steps, currentUser } = this.props;
        const {
            currentStep,
            maxVisitedStep,
            isEdit,
            editableFieldValues,
            artist,
            socials
        } = this.state;

        return (
            <Fragment>
                <Helmet>
                    <title>Creator Authentication on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <ValidateArtist
                    loading={currentUser.loading}
                    steps={steps}
                    currentStep={currentStep}
                    maxVisitedStep={maxVisitedStep}
                    onNextStep={this.handleNextStep}
                    onStepClick={this.handleStepClick}
                    artist={artist}
                    isEdit={isEdit}
                    updateSlug={currentUser.updateSlug}
                    editableFieldValues={editableFieldValues}
                    socials={socials}
                    onEditFieldInput={this.handleEditFieldInput}
                    onBannerEditApply={this.handleEditBanner}
                    onBannerRemove={this.handleRemoveBanner}
                    onEditProfile={this.handleEditProfile}
                    onEditVerifiedEmail={this.handleEditVerifiedEmail}
                    onArtistUpdate={this.handleUpdateArtist}
                    onSocialLink={this.handleSocialLink}
                    onResendEmail={this.handleResendEmailClick}
                />
                <LabelSignupModal />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return canUserJoinCreatorAuthentication(currentUser);
            },
            redirectTo(currentUser, location) {
                // Redirect to creators so they can see the modal tell
                // them they dont make the requirements
                if (currentUser.isLoggedIn) {
                    return '/creators';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(ValidateArtistContainer);
