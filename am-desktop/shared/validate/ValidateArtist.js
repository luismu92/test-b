import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { smoothScroll } from 'utils/index';

import StepProgressBar from '../upload/StepProgressBar';
import ValidateArtistEditProfile from './ValidateArtistEditProfile';

import TwitterLogo from 'icons/twitter-logo-new';
import AndroidLoader from '../loaders/AndroidLoader';
import ArrowNext from '../icons/arrow-next';
import CheckIcon from '../icons/check-mark';
import styles from './ValidateArtist.module.scss';

class ValidateArtist extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        steps: PropTypes.array,
        currentStep: PropTypes.object,
        maxVisitedStep: PropTypes.number,
        artist: PropTypes.object,
        isEdit: PropTypes.bool,
        updateSlug: PropTypes.object,
        editableFieldValues: PropTypes.object,
        socials: PropTypes.object,
        onEditFieldInput: PropTypes.func,
        onBannerEditApply: PropTypes.func,
        onBannerRemove: PropTypes.func,
        onEditProfile: PropTypes.func,
        onEditVerifiedEmail: PropTypes.func,
        onNextStep: PropTypes.func,
        onStepClick: PropTypes.func,
        onArtistUpdate: PropTypes.func,
        onSocialLink: PropTypes.func,
        onResendEmail: PropTypes.func
    };

    componentDidUpdate(prevProps) {
        const { currentStep } = this.props;
        if (prevProps.currentStep !== currentStep) {
            smoothScroll(0, { speed: 500 });
        }
    }

    renderVerifyEmail = () => {
        const { artist, onResendEmail } = this.props;

        return (
            <div className={classnames(styles.verifyEmail, 'u-padded')}>
                <h4>
                    Confirm the email address associated with your Audiomack
                    account.
                </h4>
                {!artist.verified_email && (
                    <p>
                        The email address you listed has not been verified.{' '}
                        <button
                            className={classnames(
                                styles.link,
                                'button button--link'
                            )}
                            onClick={onResendEmail}
                        >
                            Resend Verification
                        </button>
                    </p>
                )}
                <div className={styles.email}>{artist.email}</div>
                <p>
                    Not your email?{' '}
                    <Link
                        className={styles.link}
                        to="/edit/profile/email?utm_content=validate-artist"
                    >
                        {' '}
                        Change it here
                    </Link>
                </p>
                <p>Revisit this page after changing your email.</p>
            </div>
        );
    };

    renderProfile = () => {
        const {
            artist,
            isEdit,
            updateSlug,
            loading,
            editableFieldValues,
            currentStep,
            onEditFieldInput,
            onBannerEditApply,
            onBannerRemove,
            onEditProfile,
            onEditVerifiedEmail,
            onArtistUpdate
        } = this.props;

        return (
            <ValidateArtistEditProfile
                loading={loading}
                artist={artist}
                isEdit={isEdit}
                updateSlug={updateSlug}
                currentStep={currentStep}
                editableFieldValues={editableFieldValues}
                onEditFieldInput={onEditFieldInput}
                onBannerEditApply={onBannerEditApply}
                onBannerRemove={onBannerRemove}
                onEditProfile={onEditProfile}
                onEditVerifiedEmail={onEditVerifiedEmail}
                onArtistUpdate={onArtistUpdate}
            />
        );
    };

    renderSocial = (isReview) => {
        const { artist, onSocialLink, socials } = this.props;
        const linkedSocials = artist.linkedSocials || {};

        let providers = [
            {
                network: 'twitter',
                icon: <TwitterLogo />
            }
        ];

        if (isReview) {
            providers = providers.filter(
                (button) =>
                    linkedSocials[button.network] || socials[button.network]
            );
        }

        const buttons = providers.map((button, i) => {
            const buttonText =
                linkedSocials[button.network] || socials[button.network]
                    ? `YOUR ${button.network.toUpperCase()} ACCOUNT IS LINKED`
                    : `LINK YOUR ${button.network.toUpperCase()} ACCOUNT`;

            const checkStyle =
                linkedSocials[button.network] || socials[button.network]
                    ? styles.check
                    : styles.unCheck;
            return (
                <div key={i} className={styles.socialButton}>
                    <button
                        className={`auth__button--${button.network}`}
                        id={button.network}
                        onClick={onSocialLink}
                    >
                        <span className={styles.icon}>{button.icon}</span>
                        <span className="auth__button-text u-fs-16 u-fw-700">
                            {buttonText}
                        </span>
                    </button>
                    <CheckIcon className={classnames(checkStyle, 'verified')} />
                </div>
            );
        });

        return (
            <div className="u-padded small-24">
                {!isReview && (
                    <div className={styles.topSocial}>
                        <h4>Link your socials accounts.</h4>
                        <p>
                            Connect as many socials account as possible to help
                            authenticate your identity.
                        </p>
                        <p>
                            You must connect at least one. If you do not have
                            any, contact{' '}
                            <span className="u-text-orange">
                                creators@audiomack.com
                            </span>
                        </p>
                    </div>
                )}
                <div className={styles.socialButtons}>{buttons}</div>
            </div>
        );
    };

    renderReview = () => {
        return (
            <Fragment>
                <div className={styles.topReview}>
                    <h4>
                        Review all information is correct and scroll down to
                        confirm and complete authentication.
                    </h4>
                </div>
                {this.renderProfile()}
                {this.renderSocial(true)}
            </Fragment>
        );
    };

    renderHeader() {
        const { currentStep, maxVisitedStep, onStepClick, steps } = this.props;
        const list = steps.map((step) => step.name);
        const title = 'audiomack for artists';

        return (
            <header className={styles.header}>
                <h6 className={styles.title}>{title.toUpperCase()}</h6>
                <h1 className={styles.subtitle}>
                    {`Step ${currentStep.number}: `}
                    <span className={styles.bold}>{currentStep.label}</span>
                </h1>
                <StepProgressBar
                    backgroundColor="gray"
                    currentStep={currentStep.number}
                    maxVisitedStep={maxVisitedStep}
                    onStepClick={onStepClick}
                    stepList={list}
                    className={styles.progressBar}
                    stepClassName={styles.progressBarStep}
                    historyMethod="push"
                    basePath="validate/artist"
                />
            </header>
        );
    }

    renderContent() {
        const { currentStep, isEdit, loading, updateSlug } = this.props;
        let content;
        let loader;

        if (loading || updateSlug.loading) {
            loader = (
                <div className=" u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        switch (currentStep.number) {
            case 1:
                content = this.renderVerifyEmail();
                break;

            case 2:
                content = this.renderProfile();
                break;

            case 3:
                content = this.renderSocial(false);
                break;

            case 4:
                content = this.renderReview();
                break;

            default:
                break;
        }

        return (
            <div className="row u-spacing-bottom">
                <div className={classnames(styles.column, 'small-24')}>
                    {loader}
                    <div className="u-box-shadow u-padding-bottom">
                        {content}
                    </div>
                    {!isEdit && this.renderFooter()}
                </div>
            </div>
        );
    }

    renderFooter() {
        const {
            currentStep,
            onNextStep,
            artist,
            loading,
            socials
        } = this.props;
        const { linkedSocials } = artist;
        let arrowIcon = (
            <ArrowNext
                className={classnames(styles.arrowIcon, 'u-text-icon')}
            />
        );
        let disableNextButton = false;
        let nextButtonText = 'Next Step';
        let socialsConnected = false;
        let profileNameImage = false;

        // Check if there is any social connected
        if (
            linkedSocials.twitter ||
            linkedSocials.instagram ||
            Object.keys(socials).length
        ) {
            socialsConnected = true;
        }

        // check profile name and image
        if (
            artist.name &&
            (!artist.image.includes('default-artist-image') ||
                !artist.image_base.includes('default-artist-image'))
        ) {
            profileNameImage = true;
        }

        if (
            (currentStep.number === 2 && !profileNameImage) ||
            (currentStep.number === 3 && !socialsConnected) ||
            !artist.verified_email
        ) {
            disableNextButton = true;
        }

        if (currentStep.number === 4) {
            nextButtonText = 'Confirm & Finish';
            arrowIcon = null;
        }

        return (
            <footer className={styles.footer}>
                <button
                    className={classnames(
                        styles.nextButton,
                        'button',
                        'button--pill',
                        'button--med'
                    )}
                    disabled={disableNextButton || loading}
                    onClick={onNextStep}
                >
                    <span>{nextButtonText}</span>
                    {arrowIcon}
                </button>
            </footer>
        );
    }

    render() {
        return (
            <Fragment>
                {this.renderHeader()}
                {this.renderContent()}
            </Fragment>
        );
    }
}

export default ValidateArtist;
