import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CheckIcon from '../icons/check-mark';
import BellIcon from 'icons/bell-active';
import StatsIcon from 'icons/trending-up';
import CallBellIcon from 'icons/call-bell';
import styles from './ValidationLanding.module.scss';
import AmLogo from '../icons/am-mark';

import FooterDark from '../components/FooterDark';

class ValidationLanding extends Component {
    static propTypes = {
        onShowModal: PropTypes.func,
        albumArtworks: PropTypes.array
    };

    static defaultProps = {
        albumArtworks: [
            'https://assets.audiomack.com/_default/08e96e868a11f6fab3bb29a288f44d4c8730cfa6d3e4916c2b246e7f50dc10b4.jpg',
            'https://assets.audiomack.com/_default/15f1b9defd0cb5ade1315983b71b802c.jpg',
            'https://assets.audiomack.com/_default/29e5c65985e89c11dd04b1aabf9b9fa85f624fa99e72608e43098e2a09f408f2.jpg',
            'https://assets.audiomack.com/_default/4d7872f4ea4df936b6f29e47a9374d8def1d7925666e8e6e3dc59231630d46d0.jpg',
            'https://assets.audiomack.com/_default/73e814f12c0500d164726b4b610c9d6f637d685333f88855c7d03cb616b4a906.jpg',
            'https://assets.audiomack.com/_default/749bd097104fa5b304f69762aa39a205.jpg',
            'https://assets.audiomack.com/_default/7d3fc5671e0ca67878687eedc6e932de953ec82f2811df31fefe6b2f58d06ceb.jpg',
            'https://assets.audiomack.com/_default/b6476a140a35d9d1335ea10e57e02030e55705c6a8a71d20ce6d867a14b017c2.jpg',
            'https://assets.audiomack.com/_default/d342ebefafcf0073353110818d7016be9c3d6f974d63c016fb9a8a0651f4e01d.jpg',
            'https://assets.audiomack.com/_default/d97b077669dc37fdc013a62b6d7b7779.jpg',
            'https://assets.audiomack.com/_default/f9805120a12dc3dab2dd95da794ea444a1966290142d929810a587ef45a1b2bb.jpg'
        ]
            .sort(() => Math.random() - 0.5)
            .slice(0, 5) // Re organize the array randomly, then get the first 5 elements
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    renderApplyBtn = () => {
        return (
            <button
                type="button"
                className={classnames(styles.applyBtn, 'button button--large')}
                onClick={this.props.onShowModal}
            >
                <strong>APPLY FOR AUTHENTICATION</strong>
            </button>
        );
    };

    render() {
        const { albumArtworks } = this.props;

        return (
            <Fragment>
                <div className={styles.mainCreator}>
                    <div
                        className={`${
                            styles.containerTop
                        } row u-spacing-bottom u-spacing-top`}
                    >
                        <div className="column">
                            <h1 className={styles.titleWelcome}>
                                Welcome to Audiomack for Creators.
                            </h1>
                            {this.renderApplyBtn()}
                            <p className={styles.subtitleWelcome}>
                                Becoming authenticated as a creator on Audiomack
                                allows you to:
                            </p>
                        </div>
                        <div className={`${styles.collage} column`}>
                            <AmLogo className={styles.giantLogoBackground} />
                            <img
                                alt=""
                                className={styles.cell1}
                                src="/static/images/desktop/imglandingartist/cellphone1.png"
                                srcSet="/static/images/desktop/imglandingartist/cellphone1@2x.png 2x"
                            />
                            <img
                                alt=""
                                className={styles.cell2}
                                src="/static/images/desktop/imglandingartist/cellphone2.png"
                                srcSet="/static/images/desktop/imglandingartist/cellphone2@2x.png 2x"
                            />
                            <img
                                alt=""
                                className={`${styles.album} ${styles.album1}`}
                                src={`${albumArtworks[0]}?width=124&height=124`}
                                srcSet={`${
                                    albumArtworks[0]
                                }?width=248&height=248`}
                            />
                            <img
                                alt=""
                                className={`${styles.album} ${styles.album2}`}
                                src={`${albumArtworks[1]}?width=94&height=94`}
                                srcSet={`${
                                    albumArtworks[1]
                                }?width=188&height=188`}
                            />
                            <img
                                alt=""
                                className={`${styles.album} ${styles.album3}`}
                                src={`${albumArtworks[2]}?width=124&height=124`}
                                srcSet={`${
                                    albumArtworks[2]
                                }?width=248&height=248`}
                            />
                            <img
                                alt=""
                                className={`${styles.album} ${styles.album4}`}
                                src={`${albumArtworks[3]}?width=74&height=74`}
                                srcSet={`${
                                    albumArtworks[3]
                                }?width=148&height=148`}
                            />
                            <img
                                alt=""
                                className={`${styles.album} ${styles.album5}`}
                                src={`${albumArtworks[4]}?width=124&height=124`}
                                srcSet={`${
                                    albumArtworks[4]
                                }?width=248&height=248`}
                            />
                            <img
                                alt=""
                                className={styles.headphone}
                                src="/static/images/desktop/imglandingartist/headphone.png"
                                srcSet="/static/images/desktop/imglandingartist/headphone@2x.png 2x"
                            />
                        </div>
                    </div>
                    <div
                        className={classnames(
                            styles.containerCards,
                            'row u-spacing-bottom u-spacing-top'
                        )}
                    >
                        <div className="column small-24 large-12">
                            <div className={styles.itemCards}>
                                <CheckIcon
                                    className={classnames(
                                        styles.iconContainer,
                                        styles.smallPadding,
                                        'verified'
                                    )}
                                />
                                <h1>
                                    <strong>Distinguish yourself</strong> as
                                    creator.
                                </h1>
                                <p>
                                    Authentication allows creators to
                                    distinguish their Audiomack accounts from
                                    listener accounts.
                                </p>
                            </div>
                        </div>
                        <div className="column small-24 large-12">
                            <div className={styles.itemCards}>
                                <BellIcon
                                    className={classnames(
                                        styles.iconContainer,
                                        'verified'
                                    )}
                                />
                                <h1>
                                    Instantly <strong>notify fans</strong> when
                                    you release content.
                                </h1>
                                <p>
                                    Authenticated creators gain access to our
                                    notification tools, allowing you to notify
                                    your followers whenever you have a new
                                    release.
                                </p>
                            </div>
                        </div>
                        <div className="column small-24 large-12">
                            <div className={styles.itemCards}>
                                <StatsIcon
                                    className={classnames(
                                        styles.iconContainer,
                                        styles.smallPadding,
                                        'verified'
                                    )}
                                />
                                <h1>
                                    Submit your content for{' '}
                                    <strong>trending consideration.</strong>
                                </h1>
                                <p>
                                    Audiomack's trending charts are the premier
                                    location for creator discovery.
                                    Authenticated creators gain access to our
                                    new trending submission tools.
                                </p>
                            </div>
                        </div>
                        <div className="column small-24 large-12">
                            <div className={styles.itemCards}>
                                <CallBellIcon
                                    className={classnames(
                                        styles.iconContainer,
                                        'verified'
                                    )}
                                />
                                <h1>
                                    Gain access to our{' '}
                                    <strong>creator services.</strong>
                                </h1>
                                <p>
                                    Audiomack is constantly equipping creators
                                    with the new tools to help their careers.
                                    Authenticated creators gain access to our
                                    growing list of exclusive creator services.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="row u-spacing-bottom u-spacing-top">
                        {this.renderApplyBtn()}
                    </div>
                </div>
                <FooterDark />
            </Fragment>
        );
    }
}

export default ValidationLanding;
