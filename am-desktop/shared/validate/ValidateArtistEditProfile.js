import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Verified from 'components/Verified';
import Avatar from '../components/Avatar';
import EditableImageBanner from '../widgets/EditableImageBanner';
import ImageBanner from '../widgets/ImageBanner';
import {
    allGenresMap,
    podcastCategories,
    podcastLanguages
} from 'constants/index';
import { getPodcastUrl } from 'utils/index';

import EditIcon from 'icons/pencil';
import DropdownIcon from '../icons/dropdown-arrow';
import CloseIcon from '../icons/close-solid';
import CameraIcon from '../icons/camera';
import styles from './ValidateArtistEditProfile.module.scss';

class ValidateArtistEditProfile extends Component {
    static propTypes = {
        artist: PropTypes.object,
        isEdit: PropTypes.bool,
        updateSlug: PropTypes.object,
        editableFieldValues: PropTypes.object,
        currentStep: PropTypes.object,
        onEditFieldInput: PropTypes.func,
        onBannerEditApply: PropTypes.func,
        onBannerRemove: PropTypes.func,
        onEditProfile: PropTypes.func,
        onEditVerifiedEmail: PropTypes.func,
        onArtistUpdate: PropTypes.func
    };

    renderBannerForm(banner) {
        const {
            onBannerEditApply,
            onBannerRemove,
            onEditProfile,
            currentStep,
            isEdit
        } = this.props;
        const defaultImage =
            '/static/images/desktop/profile-banner-placeholder.jpg';
        let bannerView;

        if (isEdit) {
            bannerView = (
                <div className={styles.banner}>
                    <EditableImageBanner
                        banner={banner}
                        onApply={onBannerEditApply}
                        onRemove={onBannerRemove}
                    />
                </div>
            );
        } else {
            bannerView = (
                <Fragment>
                    {!isEdit && currentStep.number !== 4 && (
                        <button
                            className={classnames(
                                styles.bannerEditIcon,
                                'button button--pill'
                            )}
                            onClick={onEditProfile}
                        >
                            <CameraIcon className="button__icon" />
                            Edit Banner
                        </button>
                    )}
                    <div className={styles.banner}>
                        <ImageBanner
                            banner={banner || defaultImage}
                            disableBlurOnScroll={true}
                        />
                    </div>
                </Fragment>
            );
        }

        return bannerView;
    }

    renderProfileInfo() {
        const { artist } = this.props;

        const check = <Verified user={artist} size={19} />;

        return (
            <h2 className="user-profile__name">
                {artist.name} {check}
            </h2>
        );
    }

    renderErrors() {
        const { artist, editableFieldValues, updateSlug } = this.props;
        let errorImage;
        let errorUrlSlug;

        if (
            artist.image.includes('default-artist-image') &&
            artist.image_base.includes('default-artist-image') &&
            !editableFieldValues.image
        ) {
            errorImage = (
                <Fragment>
                    <CloseIcon className={styles.icon} />
                    <p className={styles.light}>
                        {'Profile image is mandatory'}
                    </p>
                </Fragment>
            );
        }

        if (updateSlug.error) {
            errorUrlSlug = (
                <Fragment>
                    <CloseIcon className={styles.icon} />
                    <p className={styles.light}>
                        {'The User Handle is already in use'}
                    </p>
                </Fragment>
            );
        }

        return (
            <div className={classnames(styles.error, 'column small-24')}>
                {errorImage}
                {errorUrlSlug}
            </div>
        );
    }

    renderSelect = (options = [], name, id, selectProps) => {
        const { onEditFieldInput, isEdit, onEditProfile } = this.props;

        const selectClass = classnames(styles.editBox, 'select');

        options = options.map((option, i) => {
            return (
                <option key={i} value={option.value}>
                    {option.text}
                </option>
            );
        });

        return (
            <div className={selectClass}>
                {/* @todo */}
                {/* eslint-disable-next-line jsx-a11y/no-onchange */}
                <select
                    name={name}
                    id={id}
                    onChange={onEditFieldInput}
                    disabled={!isEdit}
                    {...selectProps}
                >
                    {options}
                </select>
                {isEdit ? (
                    <DropdownIcon className="select__arrow" />
                ) : (
                    <button onClick={onEditProfile}>
                        <EditIcon />
                    </button>
                )}
            </div>
        );
    };

    renderBasicForm() {
        const {
            onEditFieldInput,
            editableFieldValues,
            currentStep,
            artist,
            isEdit,
            onEditProfile,
            onEditVerifiedEmail,
            onArtistUpdate
        } = this.props;
        const profileBiographyMaxChars = 800;
        const bio = editableFieldValues.bio || artist.bio || '';

        const genreOptions = Object.keys(allGenresMap).map((genre) => {
            return { value: genre, text: allGenresMap[genre] };
        });
        const genreProps = {
            value:
                typeof editableFieldValues.genre !== 'undefined'
                    ? editableFieldValues.genre
                    : artist.genre || '',
            disabled: !isEdit
        };

        let podcastCategory;
        let hasPodcast = false;

        if (genreProps.value === 'podcast') {
            const podcastCategoryOptions = [
                { value: '', text: 'Choose category' }
            ].concat(
                podcastCategories.map(({ category }) => {
                    return { value: category, text: category };
                })
            );
            const podcastLangOptions = Object.keys(podcastLanguages).map(
                (langCode) => {
                    const text = podcastLanguages[langCode];

                    return { value: langCode, text: text };
                }
            );
            const podcastCategoryProps = {
                value:
                    typeof editableFieldValues.podcast_category !== 'undefined'
                        ? editableFieldValues.podcast_category
                        : artist.podcast_category || '',
                disabled: !isEdit
            };
            const podcastLanguageProps = {
                value:
                    typeof editableFieldValues.podcast_language !== 'undefined'
                        ? editableFieldValues.podcast_language
                        : artist.podcast_language || 'en',
                disabled: !isEdit
            };

            hasPodcast = true;
            podcastCategory = (
                <Fragment>
                    <div className="column small-24 medium-6">
                        <label htmlFor="podcast_category">
                            Podcast Category:
                        </label>
                        {this.renderSelect(
                            podcastCategoryOptions,
                            'podcast_category',
                            'podcast_category',
                            podcastCategoryProps
                        )}
                    </div>
                    <div className="column small-24 medium-6">
                        <label htmlFor="podcast_language">
                            Podcast Language:
                        </label>
                        {this.renderSelect(
                            podcastLangOptions,
                            'podcast_language',
                            'podcast_language',
                            podcastLanguageProps
                        )}
                    </div>
                    <div className="column small-24 medium-6">
                        <label htmlFor="genre">RSS Feed:</label>
                        <input
                            readOnly
                            type="text"
                            defaultValue={getPodcastUrl(artist.url_slug)}
                            onClick={this.handleRssInputClick}
                        />
                    </div>
                </Fragment>
            );
        }

        const genreColumnClass = classnames('column', {
            'small-24 medium-6': hasPodcast
        });

        const btnClass = classnames(
            styles.btnSubmit,
            'button button--radius button--upload column small-24"'
        );

        const formStyle = classnames('user-profile__feed row u-padded', {
            'u-margin-0': isEdit
        });

        return (
            <form onSubmit={onArtistUpdate} className={formStyle}>
                <div className="column small-24 medium-12">
                    <label htmlFor="name" className="required">
                        Name
                        {!isEdit && <span>*</span>}
                    </label>
                    <div className={styles.editBox}>
                        <input
                            type="text"
                            name="name"
                            id="name"
                            className="form-control"
                            defaultValue={artist.name}
                            onChange={onEditFieldInput}
                            required
                            disabled={!isEdit}
                        />
                        {!isEdit && (
                            <button onClick={onEditProfile}>
                                <EditIcon />
                            </button>
                        )}
                    </div>
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="url_slug" className="required">
                        User Handle
                        {!isEdit && <span>*</span>}
                    </label>
                    <div className={styles.editBox}>
                        <input
                            type="text"
                            name="url_slug"
                            id="url_slug"
                            className="form-control"
                            defaultValue={artist.url_slug}
                            onChange={onEditFieldInput}
                            required
                            disabled={!isEdit}
                        />
                        {!isEdit && (
                            <button onClick={onEditProfile}>
                                <EditIcon />
                            </button>
                        )}
                    </div>
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="label">Label</label>
                    <div className={styles.editBox}>
                        <input
                            type="text"
                            name="label"
                            id="label"
                            defaultValue={artist.label}
                            onChange={onEditFieldInput}
                            className="form-control"
                            disabled={!isEdit}
                        />
                        {!isEdit && (
                            <button onClick={onEditProfile}>
                                <EditIcon />
                            </button>
                        )}
                    </div>
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="hometown">Hometown</label>
                    <div className={styles.editBox}>
                        <input
                            type="text"
                            name="hometown"
                            id="hometown"
                            defaultValue={artist.hometown}
                            onChange={onEditFieldInput}
                            className="form-control"
                            disabled={!isEdit}
                        />
                        {!isEdit && (
                            <button onClick={onEditProfile}>
                                <EditIcon />
                            </button>
                        )}
                    </div>
                </div>
                <div
                    className={
                        currentStep.number === 4
                            ? 'column small-24 medium-12'
                            : 'column small-24'
                    }
                >
                    <label htmlFor="url">Website URL</label>
                    <div className={styles.editBox}>
                        <input
                            type="url"
                            name="url"
                            id="url"
                            onChange={onEditFieldInput}
                            defaultValue={artist.url}
                            className="form-control"
                            disabled={!isEdit}
                        />
                        {!isEdit && (
                            <button onClick={onEditProfile}>
                                <EditIcon />
                            </button>
                        )}
                    </div>
                </div>
                {currentStep.number === 4 && (
                    <div className="column small-24 medium-12">
                        <label htmlFor="email">Verified Email</label>
                        <div className={styles.editBox}>
                            <input
                                type="email"
                                name="email"
                                id="email"
                                onChange={onEditFieldInput}
                                defaultValue={artist.email}
                                className="form-control"
                                disabled={!isEdit}
                            />
                            {!isEdit && (
                                <button onClick={onEditVerifiedEmail}>
                                    <EditIcon />
                                </button>
                            )}
                        </div>
                    </div>
                )}
                <div className="column small-24">
                    <label htmlFor="bio">Short Biography</label>
                    <div className={styles.editBox}>
                        <textarea
                            name="bio"
                            id="bio"
                            rows="4"
                            className="form-control"
                            cols="80"
                            onChange={onEditFieldInput}
                            maxLength={profileBiographyMaxChars}
                            defaultValue={bio}
                            disabled={!isEdit}
                        />
                        {!isEdit && (
                            <button onClick={onEditProfile}>
                                <EditIcon />
                            </button>
                        )}
                    </div>
                    <em>
                        <span className="charsleft u-d-inline-block">
                            You have{' '}
                            <span>{profileBiographyMaxChars - bio.length}</span>{' '}
                            character
                            {profileBiographyMaxChars - bio.length === 1
                                ? ''
                                : 's'}{' '}
                            left.
                        </span>
                    </em>
                </div>
                <div className={genreColumnClass}>
                    <label htmlFor="genre">Genre:</label>
                    {this.renderSelect(
                        genreOptions,
                        'genre',
                        'genre',
                        genreProps
                    )}
                </div>
                {podcastCategory}
                {this.renderErrors()}
                {isEdit && (
                    <div className="column small-24">
                        <input
                            className={btnClass}
                            type="submit"
                            value="Save"
                        />
                    </div>
                )}
            </form>
        );
    }

    render() {
        const {
            editableFieldValues,
            artist,
            onEditFieldInput,
            isEdit,
            currentStep,
            onEditProfile
        } = this.props;

        const contentStyle = isEdit
            ? classnames('user-profile user-profile--edit')
            : classnames(styles.content, 'user-profile user-profile--edit');

        return (
            <div id="profile-page" className={contentStyle}>
                {this.renderBannerForm(
                    typeof editableFieldValues.image_banner === 'undefined'
                        ? artist.image_banner
                        : editableFieldValues.image_banner
                )}
                <div className="user-profile__main">
                    <div className="row u-margin-0">
                        <div className="column small-24 medium-16 small-text-center medium-text-left user-profile__main-info">
                            <div className={styles.avatar}>
                                <Avatar
                                    type="artist"
                                    image={
                                        editableFieldValues.image ||
                                        artist.image_base ||
                                        artist.image
                                    }
                                    onInputChange={onEditFieldInput}
                                    dirty={!!editableFieldValues.image}
                                    editable={isEdit}
                                    zoomable={!isEdit}
                                    rounded
                                />
                                {!isEdit && currentStep.number !== 4 && (
                                    <button
                                        className="button button--pill"
                                        onClick={onEditProfile}
                                        data-name="image"
                                    >
                                        <CameraIcon className="button__icon" />
                                        Edit Image
                                        <span>*</span>
                                    </button>
                                )}
                            </div>
                            {this.renderProfileInfo()}
                        </div>
                    </div>
                    {this.renderBasicForm()}
                </div>
            </div>
        );
    }
}

export default ValidateArtistEditProfile;
