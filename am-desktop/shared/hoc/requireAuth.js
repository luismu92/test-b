import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { buildRedirectUrl } from 'utils/index';

export default function requireAuth(
    Component,
    {
        redirectTo = '/login',
        validator = (currentUser) => {
            return currentUser.isLoggedIn;
        }
    } = {}
) {
    class RequireAuthWrapper extends React.Component {
        static displayName = `RequireAuthWrapper[${Component.displayName}]`;

        static propTypes = {
            dispatch: PropTypes.func.isRequired,
            currentUser: PropTypes.object.isRequired,
            history: PropTypes.object.isRequired,
            location: PropTypes.object.isRequired
        };

        constructor(props) {
            super(props);

            this.checkAuth(props);
        }

        componentDidUpdate(prevProps) {
            if (prevProps.location !== this.props.location) {
                this.checkAuth();
            }
        }

        getRedirectUrl() {
            const { location, currentUser } = this.props;

            if (typeof redirectTo === 'function') {
                return redirectTo(currentUser, location);
            }

            return buildRedirectUrl(redirectTo, location);
        }

        checkAuth(props = this.props) {
            const { currentUser, history, location } = props;
            const isAuthenticated = validator(currentUser, history, location);

            if (!isAuthenticated) {
                history.replace({
                    pathname: this.getRedirectUrl()
                });
            }
        }

        render() {
            if (!this.props.currentUser.isLoggedIn) {
                return null;
            }

            return <Component {...this.props} />;
        }
    }

    function mapStateToProps(state) {
        return {
            currentUser: state.currentUser
        };
    }

    return withRouter(connect(mapStateToProps)(RequireAuthWrapper));
}
