import React from 'react';
import hoistStatics from 'hoist-non-react-statics';

const timerKey = '__darkTheme';

export default function activateDarkThemeForComponent(Component) {
    class DarkThemeComponent extends React.Component {
        static displayName = `DarkThemeComponent[${Component.displayName}]`;

        componentDidMount() {
            this._headerWasShowing = !document.body.classList.contains(
                'dark-theme'
            );
            document.body.classList.add('dark-theme');

            clearTimeout(window[timerKey]);
        }

        componentWillUnmount() {
            // Make sure if we're navigating to a component that also needs
            // the header hidden, we don't show then hide again
            window[timerKey] = setTimeout(() => {
                window.requestAnimationFrame(() => {
                    if (this._headerWasShowing) {
                        document.body.classList.remove('dark-theme');
                    }

                    this._headerWasShowing = null;
                });
            }, 100);
        }

        render() {
            return <Component {...this.props} />;
        }
    }

    return hoistStatics(DarkThemeComponent, Component);
}
