import React from 'react';
import hoistStatics from 'hoist-non-react-statics';

const timerKey = '__hiddenSidebar';

export default function hideSidebarForComponent(Component) {
    class HiddenSidebarComponent extends React.Component {
        static displayName = `HiddenSidebarComponent[${Component.displayName}]`;

        componentDidMount() {
            this._sidebarWasShowing = !document.body.classList.contains(
                'has-hidden-sidebar'
            );
            document.body.classList.add('has-hidden-sidebar');

            clearTimeout(window[timerKey]);
        }

        componentWillUnmount() {
            // Make sure if we're navigating to a component that also needs
            // the sidebar hidden, we don't show then hide again
            window[timerKey] = setTimeout(() => {
                window.requestAnimationFrame(() => {
                    if (this._sidebarWasShowing) {
                        document.body.classList.remove('has-hidden-sidebar');
                    }

                    this._sidebarWasShowing = null;
                });
            }, 100);
        }

        render() {
            return <Component {...this.props} />;
        }
    }

    return hoistStatics(HiddenSidebarComponent, Component);
}
