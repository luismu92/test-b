/* eslint-disable react/no-multi-comp */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';

import {
    getArtistProfileImage,
    getUploader,
    getMusicUrl,
    ownsMusic
} from 'utils/index';
import { mapTagOptions } from 'utils/tags';
import { allGenresMap, DEFAULT_DATE_FORMAT } from 'constants/index';

import { eventLabel } from 'utils/analytics';

import OwnerControlsContainer from '../components/OwnerControlsContainer';

import Avatar from '../components/Avatar';
import FollowButtonContainer from '../components/FollowButtonContainer';
import Verified from 'components/Verified';
import Truncate from 'components/Truncate';
import Tunecore from '../components/Tunecore';
import SocialLinks from '../components/SocialLinks';
import MusicRankings from 'components/MusicRankings';
import Button from '../buttons/Button';

import AdvancedStatsIcon from 'icons/advanced-stats';

import styles from './MusicInfo.module.scss';

function MusicInfo({ currentUser, item, artist, tags }) {
    ////////////////////
    // Event handlers //
    ////////////////////

    const [expanded, toggleLines] = useState(false);
    const [truncated, toggleTruncate] = useState(false);

    ////////////////////
    // Helper methods //
    ////////////////////

    const mappedTags = mapTagOptions('normalizedKey', 'display', tags);
    const renderTags = () => {
        let displayTags = [];

        if (item.usertags) {
            const normalizedKeys = item.usertags.split(',');

            if (normalizedKeys.length) {
                displayTags = normalizedKeys.map((normalizedKey, i) => {
                    const display = mappedTags[normalizedKey];

                    if (!display) {
                        return null;
                    }

                    return (
                        <Link
                            key={i}
                            className={styles.tag}
                            to={`/search?q=tag:${encodeURIComponent(display)}`}
                        >
                            #{display}
                        </Link>
                    );
                });
            }
        }

        if (item.genre) {
            const genreTag = (
                <Link
                    key="genre-tag"
                    className={styles.tag}
                    to={`/search?q=genre:${item.genre}`}
                >
                    #{allGenresMap[item.genre]}
                </Link>
            );
            displayTags.unshift(genreTag);
        }

        return <div style={{ margin: '20px 0 0' }}>{displayTags}</div>;
    };

    const renderAlbum = () => {
        if (!item || (!item.album && !item.album_details)) {
            return null;
        }

        const title = (item.album_details || {}).title || item.album;

        let album = item.album;

        if (item.album_details) {
            const fauxItem = {
                ...item.album_details,
                type: 'album',
                title,
                uploader: getUploader(item)
            };

            album = (
                <Link style={{ fontWeight: 600 }} to={getMusicUrl(fauxItem)}>
                    {title}
                </Link>
            );
        }

        return (
            <li className={styles.meta}>
                <strong>Album:</strong> {album}
            </li>
        );
    };

    const renderDescription = () => {
        if (!item.description) {
            return null;
        }

        let buttonToggle;

        if (!expanded && truncated) {
            buttonToggle = (
                <div>
                    <button
                        className="button-link u-d-inline-block"
                        onClick={() => toggleLines(!expanded)} // eslint-disable-line
                    >
                        <span className="music-info__read-more u-d-inline-block u-pos-relative u-fw-700">
                            Read more
                        </span>
                    </button>
                </div>
            );
        } else if (expanded && !truncated) {
            buttonToggle = (
                <div>
                    <button
                        className="button-link u-d-inline-block"
                        onClick={() => toggleLines(!expanded)} // eslint-disable-line
                    >
                        <span className="music-info__read-more music-info__read-more--less u-d-inline-block u-pos-relative u-fw-700">
                            Show less
                        </span>
                    </button>
                </div>
            );
        }

        return (
            <div className={styles.description}>
                <Truncate
                    tagName="span"
                    lines={expanded ? Infinity : 3}
                    ellipsis={null}
                    // Account for "read more" and ellipsis
                    cutOffMoreCharacters={13}
                    onTruncate={() => toggleTruncate(!truncated)} // eslint-disable-line
                    text={item.description}
                    linkify
                />
                {buttonToggle}
            </div>
        );
    };

    const check = <Verified user={artist} size={14} />;

    let contentRight = (
        <div className={styles.follow}>
            <FollowButtonContainer artist={artist} showCount />
        </div>
    );

    if (currentUser.isAdmin || ownsMusic(currentUser, item)) {
        contentRight = (
            <div className={styles.buttons}>
                <OwnerControlsContainer music={item} />
                <Button
                    width={140}
                    height={32}
                    text="Advanced Stats"
                    icon={<AdvancedStatsIcon />}
                    href={`/stats/music/${item.id}`}
                    style={{
                        letterSpacing: '-0.5px'
                    }}
                />
            </div>
        );
    }

    const links = {
        url: artist.url,
        twitter: artist.twitter,
        facebook: artist.facebook,
        instagram: artist.instagram,
        youtube: artist.youtube
    };

    const uploader = (
        <div className={styles.uploader}>
            <Link to={`/artist/${artist.url_slug}`}>
                <Avatar
                    type="artist"
                    size={65}
                    image={getArtistProfileImage(artist)}
                />
            </Link>
            <div className={styles.uploaderContent}>
                <div className={styles.uploaderDetails}>
                    <h3 className={styles.name}>
                        <Link to={`/artist/${artist.url_slug}`}>
                            {artist.name}
                        </Link>
                        {check}
                    </h3>
                    {contentRight}
                </div>
                <SocialLinks
                    links={links}
                    className="social-icons--artist"
                    allowNonVerified
                />
            </div>
        </div>
    );

    let producer;
    if (item.type !== 'playlist' && item.producer) {
        producer = (
            <li className={styles.meta}>
                <strong>Producer:</strong> {item.producer}
            </li>
        );
    }

    const releaseDate = item.type === 'playlist' ? item.updated : item.released;

    let rankings;
    if (item.type !== 'playlist') {
        rankings = (
            <MusicRankings
                type={item.type}
                rankings={item.stats.rankings}
                style={{
                    marginTop: 15
                }}
            />
        );
    }

    let tunecore;
    if (ownsMusic(currentUser, item)) {
        tunecore = (
            <Tunecore
                className="u-text-center"
                linkHref="http://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_afpsp_su&utm_content=ghoagpwt"
                eventLabel={eventLabel.songControl}
                location="music"
                style={{
                    marginTop: 30
                }}
            />
        );
    }

    const music = (
        <div className={styles.music}>
            {renderDescription()}
            <ul>
                {producer}
                {renderAlbum()}
                <li className={styles.meta}>
                    <strong>
                        {item.type === 'playlist'
                            ? 'Updated on:'
                            : 'Release Date:'}
                    </strong>{' '}
                    {moment(releaseDate * 1000).format(DEFAULT_DATE_FORMAT)}
                </li>
            </ul>
            {renderTags()}
            {rankings}
            {tunecore}
        </div>
    );

    return (
        <div style={{ marginBottom: 30 }}>
            {uploader}
            {music}
        </div>
    );
}

MusicInfo.propTypes = {
    currentUser: PropTypes.object,
    item: PropTypes.object,
    artist: PropTypes.object,
    tags: PropTypes.object
};

function mapStateToProps(state) {
    return {
        tags: state.musicTags
    };
}

export default connect(mapStateToProps)(MusicInfo);
