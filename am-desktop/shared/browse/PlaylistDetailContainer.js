import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { deletePinned } from '../redux/modules/user/pinned';
import { addToast } from '../redux/modules/toastNotification';

import PlaylistDetail from './PlaylistDetail';

class PlaylistDetailContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        currentUserPinned: PropTypes.object,
        item: PropTypes.object,
        dispatch: PropTypes.func,
        className: PropTypes.object,
        showPinMenu: PropTypes.bool,
        playlist: PropTypes.object,
        hideStats: PropTypes.bool,
        pinned: PropTypes.bool
    };

    constructor(props) {
        super(props);

        this.state = {
            tooltipActive: false
        };
    }

    handleTooltipClick = () => {
        this.setState({
            tooltipActive: !this.state.tooltipActive
        });
    };

    handleUnpinClick = () => {
        this.unpinItem(this.props.item);
    };

    unpinItem(item) {
        const { dispatch, currentUser } = this.props;
        const user = currentUser.profile;

        if (!user) {
            return;
        }

        dispatch(deletePinned(item));

        dispatch(
            addToast({
                type: item.type,
                action: 'unpin',
                item: `${item.title}`
            })
        );

        // Force waveform to redraw
        // #732
        setTimeout(() => window.dispatchEvent(new Event('resize')), 200);
    }

    render() {
        return (
            <PlaylistDetail
                className={this.props.className}
                currentUser={this.props.currentUser}
                currentUserPinned={this.props.currentUserPinned}
                playlist={this.props.playlist}
                hideStats={this.props.hideStats}
                showPinMenu={this.props.showPinMenu}
                onTooltipClick={this.handleTooltipClick}
                tooltipActive={this.state.tooltipActive}
                onUnpinClick={this.handleUnpinClick}
                pinned={this.props.pinned}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        currentUserPinned: state.currentUserPinned,
        player: state.player
    };
}

export default connect(mapStateToProps)(PlaylistDetailContainer);
