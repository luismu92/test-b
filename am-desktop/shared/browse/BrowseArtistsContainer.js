import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import connectDataFetchers from 'lib/connectDataFetchers';
import { passiveOption } from 'utils/index';

import {
    nextPage,
    fetchArtistList,
    setGenre
} from '../redux/modules/artist/browse';

// import { delayAction, activateAd } from '../redux/modules/ad';

import BrowseArtists from './BrowseArtists';

const SCROLL_THRESHOLD = 300;

class BrowseArtistsContainer extends Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        ad: PropTypes.object,
        artistBrowse: PropTypes.object,
        player: PropTypes.object,
        dispatch: PropTypes.func,
        featured: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const option = passiveOption();

        window.addEventListener('scroll', this.handleWindowScroll, option);
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleWindowScroll);
        this._scrollTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleGenreSwitch = (genre) => {
        const { dispatch, artistBrowse } = this.props;

        if (artistBrowse.activeGenre !== genre) {
            dispatch(setGenre(genre));
        }
    };

    handleWindowScroll = () => {
        const { artistBrowse } = this.props;
        const { loading, onLastPage } = artistBrowse;

        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (loading || onLastPage) {
                return;
            }

            if (
                document.body.clientHeight -
                    (window.innerHeight + window.pageYOffset) <
                SCROLL_THRESHOLD
            ) {
                this.fetchNextPage();
            }
        }, 200);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    fetchNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(fetchArtistList());
    };

    refetchIfNecessary(currentProps, nextProps) {
        const changedGenre = currentProps.route.genre !== nextProps.route.genre;
        const { dispatch } = currentProps;

        if (changedGenre) {
            dispatch(fetchArtistList());
        }
    }

    render() {
        const { currentUser, ad, artistBrowse, player, featured } = this.props;

        return (
            <BrowseArtists
                currentUser={currentUser}
                ad={ad}
                artistBrowse={artistBrowse}
                player={player}
                onGenreSwitch={this.handleGenreSwitch}
                featured={featured.list}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser,
        artistBrowse: state.artistBrowse,
        player: state.player,
        featured: state.featured
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(BrowseArtistsContainer, [() => fetchArtistList()])
    )
);
