import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import {
    convertSecondsToTimecode,
    renderFeaturingLinks,
    getArtistName,
    buildDynamicImage,
    getUploader,
    getMusicUrl
} from 'utils/index';
import WaveformContainer from 'components/WaveformContainer';

import VolumeHighIcon from '../icons/volume-high';
import PlusIcon from '../icons/plus-thin';
import CloseIcon from '../icons/close-thin';
import WarningIcon from '../icons/warning';
import ExternalIcon from '../icons/external';
import HeartIcon from 'icons/heart';
import HeartOutlineIcon from 'icons/heart-outline';

import TrackListingActions from '../components/TrackListingActions';

export default class MusicDetailTrack extends Component {
    static propTypes = {
        track: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        trackIndex: PropTypes.number.isRequired,
        musicIndex: PropTypes.number,
        className: PropTypes.string,
        musicItem: PropTypes.object,
        hideWaveform: PropTypes.bool,
        showArtwork: PropTypes.bool,
        darkTheme: PropTypes.bool,
        draggable: PropTypes.bool,
        showRemoveFromQueue: PropTypes.bool,
        hideQueueAddActions: PropTypes.bool,
        hideQueueLastAction: PropTypes.bool,
        hideQueueRemoveAction: PropTypes.bool,
        elapsedDisplay: PropTypes.number,
        onTrackClick: PropTypes.func,
        onTrackActionClick: PropTypes.func,
        isActive: PropTypes.bool,
        isInactive: PropTypes.bool,
        location: PropTypes.string
    };

    static defaultProps = {
        onActionClick() {},
        onTrackClick() {},
        elapsedDisplay: 0,
        showArtwork: false,
        darkTheme: false,
        draggable: false,
        isActive: true,
        location: 'tracklist'
    };

    renderWaveform({ track, elapsedDisplay, isActive, hideWaveform, type }) {
        if (!isActive || hideWaveform) {
            return null;
        }

        const isPlaylist = type === 'playlist';
        const klass = classnames(
            'listen__waveform-wrap waveform-wrap u-clearfix',
            {
                'listen__waveform-wrap--has-artwork': isPlaylist
            }
        );

        let image;
        const imageSize = 50;
        const artwork = buildDynamicImage(track.image, {
            width: imageSize,
            height: imageSize,
            max: true
        });

        const retinaArtwork = buildDynamicImage(track.image, {
            width: imageSize * 2,
            height: imageSize * 2,
            max: true
        });
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        if (isPlaylist) {
            image = (
                <div className="tracklist__track-artwork-container">
                    <img
                        className="tracklist__track-artwork"
                        src={artwork}
                        srcSet={srcSet}
                        width="50"
                        alt=""
                    />
                </div>
            );
        }

        return (
            <div className={klass}>
                {image}
                <div className="tracklist__waveform">
                    <WaveformContainer musicItem={track} />
                    <span className="waveform__elapsed waveform__time">
                        {convertSecondsToTimecode(elapsedDisplay)}
                    </span>
                    <span className="waveform__duration waveform__time">
                        {convertSecondsToTimecode(track.duration)}
                    </span>
                </div>
            </div>
        );
    }

    renderTrackNumberOrIcon(active, draggable, index) {
        if (draggable) {
            return null;
        }

        if (active) {
            return <VolumeHighIcon className="tracklist__track-icon" />;
        }

        return <span className="tracklist__track-number">{index + 1}</span>;
    }

    renderIcon(active) {
        if (active) {
            return <VolumeHighIcon className="tracklist__track-icon" />;
        }

        return null;
    }

    renderSongLink() {
        const { track, musicItem, location } = this.props;

        if (location === 'player') {
            return null;
        }

        const fauxItem = {
            ...track,
            type: 'song',
            uploader:
                musicItem.type === 'playlist'
                    ? getUploader(track)
                    : getUploader(musicItem)
        };

        return (
            <Link
                className="tracklist__track-link"
                to={getMusicUrl(fauxItem)}
                aria-label="Visit track page"
            >
                <ExternalIcon />
            </Link>
        );
    }

    renderFavButton() {
        const { track, trackIndex, currentUser, location } = this.props;

        if (location === 'player') {
            return null;
        }

        const trackId = track.song_id || track.id;
        const hasFavorite =
            currentUser.isLoggedIn &&
            currentUser.profile.favorite_music &&
            currentUser.profile.favorite_music.indexOf(trackId) !== -1;

        const favClass = classnames('tracklist__track-fav', {
            'tracklist__track-fav--active': hasFavorite
        });

        const buttonLabel = hasFavorite ? 'Remove Favorite for' : 'Favorite';

        let icon = <HeartOutlineIcon />;
        if (hasFavorite) {
            icon = <HeartIcon />;
        }

        return (
            <button
                className={favClass}
                onClick={this.props.onTrackActionClick}
                data-track-index={trackIndex}
                data-track-action="favorite"
                title={`${buttonLabel} ${track.title}`}
                aria-label={`${buttonLabel} ${track.title}`}
            >
                {icon}
            </button>
        );
    }

    render() {
        const {
            track,
            isActive,
            isInactive,
            elapsedDisplay,
            onTrackClick,
            trackIndex,
            musicIndex,
            currentUser,
            hideWaveform,
            musicItem,
            darkTheme,
            draggable,
            showArtwork,
            className
        } = this.props;

        const { geo_restricted: geoRestricted } = track;
        const tabIndex = geoRestricted ? '-1' : '0';

        const klass = classnames('tracklist__track listen__tracklist-track', {
            'tracklist__track--draggable': draggable,
            'tracklist__track--active': isActive,
            'tracklist__track--inactive': isInactive || geoRestricted,
            'tracklist__track--dark': darkTheme,
            'tracklist__track--disabled': geoRestricted,
            [className]: className
        });

        if (track.status === 'takedown') {
            return null;
        }

        let featuring = renderFeaturingLinks(track.featuring, {
            featText: 'feat. ',
            removeLinks: true
        });

        if (featuring) {
            featuring = (
                <span className="tracklist__track-featuring">
                    ({featuring})
                </span>
            );
        }

        let artwork;

        if (showArtwork) {
            const imageSize = 50;
            const artwork1x = buildDynamicImage(track.image, {
                width: imageSize,
                height: imageSize,
                max: true
            });

            const retinaArtwork = buildDynamicImage(track.image, {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            });
            let srcSet;

            if (retinaArtwork) {
                srcSet = `${retinaArtwork} 2x`;
            }

            artwork = (
                <span className="tracklist__track-artwork">
                    <img src={artwork1x} alt="" srcSet={srcSet} />
                </span>
            );
        }

        let removeFromQueueButton;

        if (this.props.showRemoveFromQueue) {
            removeFromQueueButton = (
                <button
                    className="tracklist__track-remove"
                    data-track-index={trackIndex}
                    data-track-action="remove-track"
                    onClick={this.props.onTrackActionClick}
                >
                    <CloseIcon />
                </button>
            );
        }

        let errors;

        if (
            typeof track.errors !== 'undefined' &&
            track.errors.length > 0 &&
            currentUser.isLoggedIn &&
            currentUser.isAdmin
        ) {
            errors = (
                <span className="error-list">
                    <WarningIcon className="u-text-icon u-margin-0 u-text-red" />
                    {track.errors.map((error, index) => (
                        <span className="error-text u-text-red" key={index}>
                            {' '}
                            {error}{' '}
                        </span>
                    ))}
                </span>
            );
        }

        return (
            <div className={klass} role="listitem">
                <div className="tracklist__track-details u-d-flex u-d-flex--align-center u-fs-13 u-pos-relative">
                    {this.renderIcon(isActive)}
                    <button
                        className="tracklist__track-add"
                        data-track-index={trackIndex}
                        data-track-action="playlist"
                        aria-label="Add track to a playlist"
                        tabIndex={tabIndex}
                        onClick={this.props.onTrackActionClick}
                    >
                        <PlusIcon />
                    </button>
                    <button
                        className="tracklist__track-details-left u-d-flex u-d-flex--align-center"
                        data-track-index={trackIndex}
                        data-music-index={musicIndex}
                        tabIndex={tabIndex}
                        onClick={onTrackClick}
                    >
                        {this.renderTrackNumberOrIcon(
                            isActive,
                            draggable,
                            trackIndex
                        )}
                        <span className="tracklist__track-title u-trunc u-ls-n-05 u-d-flex u-d-flex--align-center">
                            {artwork}
                            <span className="tracklist__details-wrap">
                                <span className="u-d-block u-trunc">
                                    {track.title} {featuring} {errors}{' '}
                                </span>
                                <span className="tracklist__track-artist u-trunc">
                                    {getArtistName(track)}
                                </span>
                            </span>
                        </span>
                        <span className="tracklist__track-details-right">
                            <span className="tracklist__track-duration">
                                {convertSecondsToTimecode(track.duration)}
                            </span>
                        </span>
                    </button>
                    {this.renderSongLink()}
                    {this.renderFavButton()}
                    {removeFromQueueButton}
                    {!geoRestricted && (
                        <TrackListingActions
                            index={trackIndex}
                            musicItem={musicItem}
                            track={track}
                            currentUser={currentUser}
                            onTrackActionClick={this.props.onTrackActionClick}
                            lightTheme={darkTheme}
                            showRemoveFromQueue={this.props.showRemoveFromQueue}
                            hideQueueAddActions={this.props.hideQueueAddActions}
                            hideQueueLastAction={this.props.hideQueueLastAction}
                            hideQueueRemoveAction={
                                this.props.hideQueueRemoveAction
                            }
                        />
                    )}
                </div>
                {this.renderWaveform({
                    track,
                    elapsedDisplay,
                    isActive,
                    hideWaveform,
                    type: musicItem.type
                })}
            </div>
        );
    }
}
