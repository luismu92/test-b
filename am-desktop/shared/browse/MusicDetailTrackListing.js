import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import MusicDetailTrack from './MusicDetailTrack';
import GeoNotice from '../components/GeoNotice';

const TRACKS_LIMIT = 10;

export default class MusicDetailTrackListing extends Component {
    static propTypes = {
        musicItem: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        musicIndex: PropTypes.number,
        onTrackClick: PropTypes.func,
        hideWaveform: PropTypes.bool,
        hideLoadMoreTracksButton: PropTypes.bool,
        onLoadMoreTracksButtonClick: PropTypes.func,
        onTrackActionClick: PropTypes.func,
        player: PropTypes.object
    };

    static defaultProps = {
        tracks: []
    };

    shouldShowLoadMoreButton() {
        const { musicItem } = this.props;
        const { tracks } = musicItem;

        return (
            !this.props.hideLoadMoreTracksButton && tracks.length > TRACKS_LIMIT
        );
    }

    renderContent() {
        const {
            player,
            onTrackClick,
            currentUser,
            onTrackActionClick,
            musicItem,
            musicIndex
        } = this.props;
        const { tracks } = musicItem;
        const { currentTime, currentSong } = player;
        const limit = this.shouldShowLoadMoreButton()
            ? TRACKS_LIMIT
            : tracks.length;

        return tracks.slice(0, limit).map((track, i) => {
            const trackId = track.song_id || track.id;
            const isCurrentTrack = !!currentSong && trackId === currentSong.id;

            let elapsedDisplay = 0;

            if (isCurrentTrack) {
                elapsedDisplay = currentTime;
            }

            if (track.geo_restricted) {
                return null;
            }

            return (
                <MusicDetailTrack
                    track={track}
                    trackIndex={i}
                    musicIndex={musicIndex}
                    musicItem={musicItem}
                    showArtwork={musicItem.type === 'playlist'}
                    hideWaveform={this.props.hideWaveform}
                    currentUser={currentUser}
                    key={`${trackId}:${i}`}
                    elapsedDisplay={elapsedDisplay}
                    isActive={isCurrentTrack}
                    onTrackClick={onTrackClick}
                    onTrackActionClick={onTrackActionClick}
                    hideQueueLastAction={
                        player.queueIndex === player.queue.length - 1
                    }
                    hideQueueRemoveAction
                />
            );
        });
    }

    renderLoadMoreButton() {
        if (!this.shouldShowLoadMoreButton()) {
            return null;
        }

        return (
            <div className="button-group u-text-center u-spacing-bottom-20">
                <div className="u-padding-top-20">
                    <button
                        className="button button--pill button--has-arrow"
                        onClick={this.props.onLoadMoreTracksButtonClick}
                    >
                        Load more tracks
                        <span className="button__arrow" />
                    </button>
                </div>
            </div>
        );
    }

    renderGeoNotice() {
        const { musicItem } = this.props;
        const { geo_restricted: geoRestricted } = musicItem;

        if (!geoRestricted) {
            return null;
        }

        return (
            <GeoNotice withBackdrop>
                The rightsholder has not made this content available in your
                country.
            </GeoNotice>
        );
    }

    render() {
        const { musicItem } = this.props;
        const tracklistClass = `tracklist tracklist--${
            musicItem.type
        } listen__tracklist`;

        const wrapClass = classnames(
            'listen__tracklist-wrap u-padding-bottom-10',
            {
                'listen__tracklist-wrap--expanded': this.props
                    .hideLoadMoreTracksButton
            }
        );

        return (
            <div className={wrapClass}>
                <div className={tracklistClass} role="list">
                    {this.renderContent()}
                </div>
                {this.renderLoadMoreButton()}
                {this.renderGeoNotice()}
            </div>
        );
    }
}
