import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { allGenresMap } from 'constants/index';

import Avatar from '../components/Avatar';
import FollowButtonContainer from '../components/FollowButtonContainer';

import Verified from '../../../am-shared/components/Verified';

export default class UserDetail extends Component {
    static propTypes = {
        className: PropTypes.string,
        showVerifiedLabel: PropTypes.bool,
        user: PropTypes.object
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    renderHometown(hometown) {
        if (!hometown) {
            return null;
        }

        return (
            <li className="u-trunc music__meta-featuring">
                <p>
                    <strong>Hometown: </strong>
                    {hometown}
                </p>
            </li>
        );
    }

    renderGenre(genre) {
        if (!genre) {
            return null;
        }

        const artistGenre = allGenresMap[genre] || '';

        return (
            <li className="u-trunc u-tt-cap">
                <p>
                    <strong>Genre: </strong> {artistGenre}
                </p>
            </li>
        );
    }

    renderFollowers(user) {
        return (
            <li className="u-trunc">
                <p>
                    <strong>Followers: </strong>
                    {(user.followers_count || 0).toLocaleString()}
                </p>
            </li>
        );
    }

    renderBio(bio) {
        if (!bio) {
            return null;
        }

        return (
            <li className="music-meta__bio">
                <p className="u-trunc">
                    <strong>Bio: </strong> {bio}
                </p>
            </li>
        );
    }

    render() {
        const { user, className, showVerifiedLabel } = this.props;

        if (!user) {
            // @todo maybe put a skeleton view of the user detail item here?
            return null;
        }

        const check = <Verified user={user} small />;

        const klass = classnames('user-detail row', {
            [className]: className
        });

        let verifiedLabel;
        let imageStyle = 'radius';

        if (showVerifiedLabel) {
            const verified =
                user.verified === 'authenticated'
                    ? 'Authenticated'
                    : 'Verified';

            verifiedLabel = (
                <p className="verified-label">{verified} Account</p>
            );
            imageStyle = 'circle';
        }

        return (
            <div className={klass}>
                <div className="column small-24 medium-20 small-text-center medium-text-left">
                    <Link
                        className="user-detail__link"
                        to={`/artist/${user.url_slug}`}
                    >
                        <Avatar
                            image={user.image_base || user.image}
                            type="artist"
                            className="user-detail__image"
                            radius={imageStyle === 'radius'}
                            square={imageStyle === 'square'}
                        />
                    </Link>
                    <div className="user-detail__content">
                        {verifiedLabel}
                        <Link
                            className="user-detail__link"
                            to={`/artist/${user.url_slug}`}
                        >
                            <h2 className="music__heading music__heading--title u-trunc">
                                {user.name} {check}
                            </h2>
                        </Link>

                        <ul className="music__meta">
                            {this.renderHometown(user.hometown)}
                            {this.renderGenre(user.genre)}
                            {this.renderFollowers(user)}
                            {this.renderBio(!showVerifiedLabel && user.bio)}
                        </ul>
                    </div>
                </div>
                <div className="column small-24 medium-4 small-text-center medium-text-right">
                    <FollowButtonContainer artist={user} />
                </div>
            </div>
        );
    }
}
