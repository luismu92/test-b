import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import { Link } from 'react-router-dom';

import { DEFAULT_DATE_FORMAT } from 'constants/index';
import { buildDynamicImage, yesBool } from 'utils/index';

import BodyClickListener from '../components/BodyClickListener';
import MusicStats from '../components/MusicStats';
import ThreeDotsIcon from '../icons/three-dots-alt';
import Verified from 'components/Verified';
import SocialShare from '../components/SocialShare';

export default class PlaylistDetail extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        currentUserPinned: PropTypes.object,
        playlist: PropTypes.object,
        className: PropTypes.string,
        hideStats: PropTypes.bool,
        showPlayButton: PropTypes.bool,
        showPinMenu: PropTypes.bool,
        onTooltipClick: PropTypes.func,
        tooltipActive: PropTypes.bool,
        onUnpinClick: PropTypes.func,
        pinned: PropTypes.bool
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderStats(stats) {
        if (!stats || this.props.hideStats) {
            return null;
        }

        return <MusicStats stats={stats} />;
    }

    renderPinMenu() {
        const {
            currentUser,
            currentUserPinned,
            showPinMenu,
            tooltipActive,
            onTooltipClick
        } = this.props;

        if (
            !currentUser.isLoggedIn ||
            !showPinMenu ||
            currentUserPinned.list.length < 2
        ) {
            return null;
        }

        const tooltipClass = classnames(
            'tooltip sub-menu pinned-item__sub-menu',
            {
                'tooltip--active': tooltipActive
            }
        );

        return (
            <div className="pinned-item__menu">
                <button
                    className="pinned-item__menu-trigger"
                    onClick={onTooltipClick}
                    ref={(e) => {
                        this._tooltipButton = e;
                    }}
                >
                    <ThreeDotsIcon />
                </button>
                <BodyClickListener
                    shouldListen={tooltipActive}
                    onClick={onTooltipClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <ul className={tooltipClass}>
                    <li className="sub-menu__item">
                        <button
                            title="Remove Highlight"
                            aria-label="Remove Highlight"
                            data-action="unpin"
                            onClick={this.props.onUnpinClick}
                        >
                            Remove Highlight
                        </button>
                    </li>
                </ul>
            </div>
        );
    }

    renderMeta(item) {
        return (
            <ul className="music__meta u-hide-when-pin-condensed">
                <li>
                    <strong>No. of Songs: </strong> {item.track_count}
                </li>
                <li>
                    <strong>Playlist Creator:</strong>{' '}
                    <Link to={`/artist/${item.artist.url_slug}`} data-popover>
                        {item.artist.name}
                    </Link>{' '}
                    <Verified user={item.artist} small />
                </li>
                <li>
                    <strong>Last Updated: </strong>{' '}
                    {moment(item.updated * 1000).format(DEFAULT_DATE_FORMAT)}
                </li>
            </ul>
        );
    }

    renderSocialLinks(item) {
        if (yesBool(item.private)) {
            return null;
        }

        return (
            <SocialShare
                item={item}
                className="music-interactions--above-rank small-text-center medium-text-right"
            />
        );
    }

    render() {
        const { playlist, className, showPinMenu, pinned } = this.props;

        if (!playlist) {
            // @todo maybe put a skeleton view of the music detail item here?
            return null;
        }

        const { title, stats, artist } = playlist;
        let imageSize = 165;

        if (pinned) {
            imageSize = 240;
        }

        const artwork = buildDynamicImage(
            playlist.image_base || playlist.image,
            {
                width: imageSize,
                height: imageSize,
                max: true
            }
        );
        const retinaArtwork = buildDynamicImage(
            playlist.image_base || playlist.image,
            {
                width: imageSize * 2,
                height: imageSize * 2,
                max: true
            }
        );
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        const klass = classnames(
            'music-detail u-clearfix music-detail--playlist',
            {
                [className]: className,
                'music-detail--no-play': !this.props.showPlayButton
            }
        );

        const artworkClass = classnames(
            'music-artwork music-artwork--playlist music-detail__image',
            {
                'u-album-stack': showPinMenu
            }
        );

        return (
            <div className="music-detail-container">
                <div className={klass}>
                    <div className="music-detail__top u-clearfix">
                        <div className={artworkClass}>
                            <Link
                                to={`/playlist/${artist.url_slug}/${
                                    playlist.url_slug
                                }`}
                            >
                                <img src={artwork} srcSet={srcSet} alt="" />
                            </Link>
                        </div>

                        <div className="music-detail__content">
                            <Link
                                className="music-detail__link"
                                to={`/playlist/${artist.url_slug}/${
                                    playlist.url_slug
                                }`}
                            >
                                <h2 className="music__heading">
                                    <span className="music__heading--artist u-trunc u-d-block">
                                        {artist.name}
                                    </span>
                                    <span className="music__heading--title u-trunc u-d-block">
                                        {title}
                                    </span>
                                </h2>
                            </Link>
                            {this.renderMeta(playlist)}
                            {this.renderStats(stats)}
                            {this.renderSocialLinks(playlist)}
                            {this.renderPinMenu()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
