import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PlaylistCard from './PlaylistCard';

export default class BrowsePlaylistSection extends Component {
    static propTypes = {
        item: PropTypes.object,
        className: PropTypes.string
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    render() {
        const { item, className } = this.props;
        const { list } = item;

        if (!list) {
            return null;
        }

        const items = list.map((playlistItem, i) => {
            return (
                <div
                    key={i}
                    className="column small-24 medium-12 large-6"
                    style={{ padding: '0 8px' }}
                >
                    <PlaylistCard item={playlistItem} />
                </div>
            );
        });
        const klass = classnames('grid-wrap', {
            [className]: className
        });

        return (
            <div className={klass}>
                <div className="card-grid row" style={{ margin: '0 -8px' }}>
                    {items}
                </div>
            </div>
        );
    }
}
