import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import AndroidLoader from '../loaders/AndroidLoader';

function BrowsePlaylistsSidebar({ musicBrowsePlaylist, defaultSlug }) {
    const { tags, loading } = musicBrowsePlaylist;

    let items;
    let loader;

    if (!tags.length && loading) {
        loader = (
            <div className="u-text-center">
                <AndroidLoader size={30} className="u-margin-center" />
            </div>
        );
    }

    if (tags) {
        items = tags.map((cat, i) => {
            const href = `/playlists/browse/${cat.url_slug}`;

            return (
                <li key={`cat-${i}`}>
                    <NavLink
                        exact
                        to={href}
                        style={{ padding: '8px 20px' }}
                        className="vertical-menu__link u-trunc"
                        activeClassName="vertical-menu__link--current"
                        // eslint-disable-next-line react/jsx-no-bind
                        isActive={(match, loc) => {
                            return (
                                loc.pathname === href ||
                                (loc.pathname === '/playlists/browse' &&
                                    cat.url_slug === defaultSlug)
                            );
                        }}
                    >
                        {cat.title}
                    </NavLink>
                </li>
            );
        });
    }

    return (
        <div className="widget widget--playlists u-box-shadow u-spacing-bottom-20">
            <h3 className="widget-title">Browse Categories</h3>
            <ul className="vertical-menu" style={{ margin: '0 -20px' }}>
                {items}
                {loader}
            </ul>
        </div>
    );
}

BrowsePlaylistsSidebar.propTypes = {
    musicBrowsePlaylist: PropTypes.object,
    defaultSlug: PropTypes.string
};

export default BrowsePlaylistsSidebar;
