import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import analytics from 'utils/analytics';
import {
    isCurrentMusicItem,
    getArtistName,
    nativeShare,
    getQueueIndexForSong,
    shuffle,
    clamp,
    queueHasMusicItem
} from 'utils/index';
import { mapTagOptions } from 'utils/tags';

import {
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction
} from '../redux/modules/user';
import { addPinned, deletePinned } from '../redux/modules/user/pinned';
import { getPlaylistsWithSongId } from '../redux/modules/user/playlists';
import {
    favoritePlaylist,
    unfavoritePlaylist,
    addSong,
    setPlaylistTags,
    getPlaylistTags
} from '../redux/modules/playlist';
import { setSection } from '../redux/modules/stats';
import {
    showModal,
    MODAL_TYPE_AUTH,
    MODAL_TYPE_ADD_TO_PLAYLIST,
    MODAL_TYPE_ADMIN,
    MODAL_TYPE_EMBED,
    MODAL_TYPE_TAGS,
    MODAL_TYPE_QUEUE_ALERT,
    MODAL_TYPE_QUEUE_DUPLICATE,
    MODAL_TYPE_VIDEO,
    MODAL_TYPE_PROMO
} from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import {
    play,
    pause,
    editQueue,
    addQueueItem,
    timeUpdate
} from '../redux/modules/player';
import { bumpMusic, bumpMusicOneSpot } from '../redux/modules/admin';
import { trackAction } from '../redux/modules/stats';

import MusicDetail from './MusicDetail';

class MusicDetailContainer extends Component {
    static propTypes = {
        musicList: PropTypes.array.isRequired,
        index: PropTypes.number,
        dispatch: PropTypes.func,
        onCountdownFinish: PropTypes.func,
        containerClassName: PropTypes.string,
        className: PropTypes.string,
        artistUploads: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserPinned: PropTypes.object,
        player: PropTypes.object,
        subtext: PropTypes.element,
        item: PropTypes.object,
        hideWaveform: PropTypes.bool,
        large: PropTypes.bool,
        related: PropTypes.bool,
        shouldShowReup: PropTypes.bool,
        feed: PropTypes.bool,
        search: PropTypes.bool,
        profile: PropTypes.bool,
        pinned: PropTypes.bool,
        shouldLazyLoadArtwork: PropTypes.bool,
        hideInteractions: PropTypes.bool,
        removeHeaderLinks: PropTypes.bool,
        allowZoom: PropTypes.bool,
        shouldFixOnScroll: PropTypes.bool,
        shouldLinkArtwork: PropTypes.bool,
        hideLoadMoreTracksButton: PropTypes.bool,
        songKey: PropTypes.string,
        context: PropTypes.string,
        genre: PropTypes.string,
        ranking: PropTypes.number,
        statsToken: PropTypes.string,
        fixOnScrollProps: PropTypes.object,
        showPinMenu: PropTypes.bool,
        isVerified: PropTypes.bool,
        showArtworkPlayButton: PropTypes.bool,
        showFullGeoNotice: PropTypes.bool,
        section: PropTypes.string,
        tags: PropTypes.object
    };

    static defaultProps = {
        hideLoadMoreTracksButton: false,
        showFullGeoNotice: true
    };

    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
            truncated: false,
            nativeShareSupported: false,
            hideLoadMoreTracksButton: props.hideLoadMoreTracksButton,
            isTracklistActivated: isCurrentMusicItem(
                props.player.currentSong,
                props.item
            ),
            isFixed: false,
            tooltipActive: false
        };
    }

    componentDidMount() {
        this.setNativeShareSupport();
    }

    componentDidUpdate(prevProps) {
        const prevWasCurrent = isCurrentMusicItem(
            prevProps.player.currentSong,
            prevProps.item
        );
        const nowIsCurrent = isCurrentMusicItem(
            this.props.player.currentSong,
            this.props.item
        );

        if (
            !prevWasCurrent &&
            nowIsCurrent &&
            !this.state.isTracklistActivated
        ) {
            // eslint-disable-next-line
            this.setState({
                isTracklistActivated: true
            });
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleItemClick = (event) => {
        const { dispatch, player, musicList, section } = this.props;
        const index =
            parseInt(
                event.currentTarget.getAttribute('data-music-index'),
                10
            ) || 0;
        const buttonIndex = parseInt(
            event.currentTarget.getAttribute('data-track-index'),
            10
        );
        const currentSong = player.currentSong;
        const musicItem = musicList[index];
        const clickedOnTrack = !isNaN(buttonIndex);
        const allTracksOfItemQueued = queueHasMusicItem(
            player.queue,
            musicItem,
            player.queueIndex
        );
        const isChildOfMine =
            currentSong &&
            currentSong.parentDetails &&
            currentSong.parentDetails.id === musicItem.id;
        const shouldCountTrack =
            !clickedOnTrack && (allTracksOfItemQueued || isChildOfMine);
        const isAlreadyCurrentSong = isCurrentMusicItem(
            currentSong,
            musicItem,
            shouldCountTrack
        );

        if (!this.state.isTracklistActivated) {
            this.setState({
                isTracklistActivated: true
            });
        }

        if (!isAlreadyCurrentSong && section) {
            dispatch(setSection(section));
        }

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        const isItemWithTracks = !!musicItem.tracks;

        if (isItemWithTracks) {
            if (allTracksOfItemQueued && !clickedOnTrack) {
                const indexToPlay = getQueueIndexForSong(
                    musicItem,
                    player.queue,
                    {
                        currentQueueIndex: player.queueIndex
                    }
                );

                dispatch(
                    play(indexToPlay, {
                        item: musicItem,
                        onConfirmClick() {
                            dispatch(editQueue(musicItem, { append: true }));
                        }
                    })
                );
                return;
            }
        }

        const queueIndex = getQueueIndexForSong(musicItem, player.queue, {
            trackIndex: buttonIndex,
            currentQueueIndex: player.queueIndex
        });

        if (
            queueIndex !== -1 &&
            (!isItemWithTracks || (isItemWithTracks && allTracksOfItemQueued))
        ) {
            dispatch(play(queueIndex));
            return;
        }

        if (clickedOnTrack) {
            const track = musicItem.tracks[buttonIndex];
            const newIndex = getQueueIndexForSong(track, player.queue, {
                trackIndex: buttonIndex,
                currentQueueIndex: player.queueIndex,
                ignoreHistory: false
            });

            if (newIndex !== -1) {
                dispatch(timeUpdate(0));
                dispatch(play(newIndex));
                return;
            }
        }

        if (player.queueHasBeenEdited) {
            dispatch(
                showModal(MODAL_TYPE_QUEUE_ALERT, {
                    item: musicItem,
                    onAppendClick() {
                        const { queue: newQueue } = dispatch(
                            editQueue(musicList, { append: true })
                        );

                        const newIndex = getQueueIndexForSong(
                            musicItem,
                            newQueue,
                            {
                                trackIndex: buttonIndex,
                                currentQueueIndex: player.queueIndex
                            }
                        );

                        dispatch(play(newIndex));
                    },
                    onReplaceClick: () =>
                        this.replaceQueueAndPlay(musicList, musicItem)
                })
            );
            return;
        }

        this.replaceQueueAndPlay(musicList, musicItem, buttonIndex);
    };

    handleActionClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');
        const { item } = this.props;

        this.doAction(action, item, e);
    };

    handleShuffleClick = () => {
        const { dispatch, item } = this.props;
        const shouldShuffle = true;

        dispatch(pause());

        this.playMusic(item, shouldShuffle);
    };

    handleTruncate = (truncated) => {
        this.setState({ truncated });
    };

    handleToggleLines = () => {
        this.setState({ expanded: !this.state.expanded });
    };

    handleTrackActionClick = (e) => {
        const button = e.currentTarget;
        const action = button.getAttribute('data-track-action');
        const index = parseInt(button.getAttribute('data-track-index'), 10);
        const { item } = this.props;
        let track = item.tracks[index];

        if (['add-to-queue-next', 'add-to-queue-end'].includes(action)) {
            track = item;
        }

        this.doAction(action, track, e);
        button.blur();
    };

    handleLoadMoreTracksButtonClick = () => {
        this.setState({
            hideLoadMoreTracksButton: true,
            isTracklistActivated: true
        });
    };

    handleBeforeFixedHeaderChange = (willBeFixed, component) => {
        const parent = component.parentElement;

        if (willBeFixed) {
            this._originalParentHeight = parent.style.height;

            const height = parent.getBoundingClientRect().height;

            parent.style.height = `${height}px`;
        } else {
            parent.style.height = this._originalParentHeight;
        }
    };

    handleFixedHeaderChange = (fixed) => {
        this.setState({
            isFixed: fixed
        });
    };

    handleTagsClick = (item) => {
        const { dispatch } = this.props;

        const id = item.id;

        dispatch(getPlaylistTags(id))
            .then((data) => {
                dispatch(
                    showModal(MODAL_TYPE_TAGS, {
                        title: 'Playlist Tags',
                        musicItem: this.props.item,
                        tagData: data,
                        handleTagsSubmit: this.handleTagsSubmit,
                        handleTagToggle: this.handleTagToggle
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
                return;
            });
        return;
    };

    handleTagsSubmit = (e) => {
        e.preventDefault();

        const { dispatch, item } = this.props;

        if (!item) {
            return null;
        }

        const form = e.currentTarget;

        const tags = Array.from(form.querySelectorAll('input[type="checkbox"]'))
            .filter((input) => input.checked)
            .map((input) => input.value);

        dispatch(setPlaylistTags(item.id, tags))
            .then((data) => {
                if (typeof data.resolved.message !== 'undefined') {
                    dispatch(
                        addToast({
                            type: 'playlist',
                            action: 'public',
                            item: data.resolved.message
                        })
                    );
                }
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `There has been a problem with your fetch operation: ${
                            error.message
                        }`
                    })
                );
                analytics.error(error);
                return;
            });
        return false;
    };

    handleTooltipClick = () => {
        this.setState({
            tooltipActive: !this.state.tooltipActive
        });
    };

    handlePromoLinkClick = (e) => {
        const { dispatch, item } = this.props;

        e.preventDefault();

        dispatch(
            showModal(MODAL_TYPE_PROMO, {
                music: item
            })
        );
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    setNativeShareSupport() {
        if (typeof window.navigator.share !== 'undefined') {
            this.setState({
                nativeShareSupported: true
            });
        }
    }

    replaceQueueAndPlay(queue, musicItem, trackIndex) {
        const { dispatch } = this.props;
        const { queue: newQueue } = dispatch(editQueue(queue));
        // Start with last history item
        const startingIndex = newQueue.findIndex((item) => {
            return !item.startedPlaying;
        });
        const queueIndex = getQueueIndexForSong(musicItem, newQueue, {
            currentQueueIndex: startingIndex === -1 ? 0 : startingIndex - 1,
            trackIndex
        });

        dispatch(play(queueIndex));
    }

    showLoginModal() {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_AUTH, { type: 'login' }));
    }

    showVideoModal(item) {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_VIDEO, {
                item
            })
        );
    }

    doAction(action, item, e) {
        const { currentUser, dispatch, genre, statsToken, player } = this.props;

        switch (action) {
            case 'play': {
                this.handleItemClick(e);
                break;
            }

            case 'favorite': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.favoriteItem(item, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'pin': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.pinItem(item, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'video': {
                this.showVideoModal(item);
                break;
            }

            case 'unpin': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.unpinItem(item, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'reup': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.reupItem(item, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'pin-reup': {
                const actions = [
                    (newlyLoggedInUser) =>
                        this.reupItem(item, newlyLoggedInUser),
                    (newlyLoggedInUser) => this.pinItem(item, newlyLoggedInUser)
                ];

                actions.forEach((queuedAction) => queuedAction());
                break;
            }

            case 'playlist': {
                const queuedAction = () => this.playlistItem(item);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'add-to-queue-next':
            case 'add-to-queue-end': {
                const button = e.currentTarget;
                const trackIndex =
                    button.getAttribute('data-track-index') || undefined;
                const isNext = action === 'add-to-queue-next';
                const parsedTrackIndex = parseInt(trackIndex, 10);
                const runAction = () => {
                    // eslint-disable-line
                    dispatch(
                        addQueueItem(item, {
                            trackIndex,
                            atIndex: isNext ? player.queueIndex + 1 : undefined
                        })
                    );

                    const suffix = isNext ? 'up next' : 'to your queue';
                    let title = item.title;

                    if (!isNaN(parsedTrackIndex)) {
                        title = item.tracks[parsedTrackIndex].title;
                    }

                    button.parentElement.parentElement.blur();

                    dispatch(
                        addToast({
                            action: 'message',
                            item: `${title} was added ${suffix}`
                        })
                    );
                };

                // If we're clicking play on an album/playlist, alert
                // the user if all the tracks of the musicItem is already
                // playing or in the future.
                const isItemWithTracks =
                    !!item.tracks && isNaN(parsedTrackIndex);

                if (isItemWithTracks) {
                    const allTracksOfItemQueued = queueHasMusicItem(
                        player.queue,
                        item,
                        player.queueIndex
                    );

                    if (allTracksOfItemQueued) {
                        dispatch(
                            showModal(MODAL_TYPE_QUEUE_DUPLICATE, {
                                item: item,
                                onConfirmClick() {
                                    runAction();
                                }
                            })
                        );
                        return;
                    }
                }

                runAction();
                break;
            }

            case 'embed': {
                dispatch(
                    showModal(MODAL_TYPE_EMBED, {
                        item
                    })
                );
                break;
            }

            case 'tags': {
                this.handleTagsClick(item);
                break;
            }

            case 'admin': {
                const queuedAction = () => this.adminItem(item);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'bump':
                this.bumpItem(item, genre);
                break;

            case 'bumpMusicOneSpot':
                this.bumpMusicOneSpot(item, genre);
                break;

            case 'download': {
                dispatch(trackAction(statsToken, 'dl', item.id));

                const downloadUrl = e.currentTarget.getAttribute('data-url');

                if (downloadUrl) {
                    window.location.href = downloadUrl;
                }
                break;
            }

            case 'share': {
                nativeShare(item);
                break;
            }

            default:
                break;
        }
    }

    playMusic(musicItem, shouldShuffle = false) {
        const { dispatch, artistUploads } = this.props;

        this.setState({
            hideLoadMoreTracksButton: true
        });

        if (!musicItem) {
            return;
        }

        const uploads = artistUploads.list.filter((item) => {
            return item.id !== musicItem.id;
        });
        let item = musicItem;

        if (shouldShuffle) {
            item = {
                ...musicItem,
                tracks: shuffle(musicItem.tracks || [])
            };
        }

        const queue = [item].concat(uploads);
        const { queue: newQueue } = dispatch(editQueue(queue));
        const indexToPlay = newQueue.findIndex((track) => {
            return !track.startedPlaying;
        });
        dispatch(play(clamp(indexToPlay, 0, Infinity)));
    }

    favoriteItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        const id = item.song_id || item.id;
        let action;

        if (item.type === 'playlist') {
            // Dont allow a newly logged in user to undo on an item
            if (
                newlyLoggedInUser ||
                user.favorite_playlists.indexOf(item.id) === -1
            ) {
                action = 'favorite';
                dispatch(favoritePlaylist(id));
            } else {
                action = 'unfavorite';
                dispatch(unfavoritePlaylist(id));
            }
        }
        // Dont allow a newly logged in user to undo on an item
        else if (newlyLoggedInUser || user.favorite_music.indexOf(id) === -1) {
            action = 'favorite';
            dispatch(favorite(id));
        } else {
            action = 'unfavorite';
            dispatch(unfavorite(id));
        }

        dispatch(
            addToast({
                action: action,
                item: `${getArtistName(item)} - ${item.title}`
            })
        );
    }

    pinItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;

        if (!user) {
            return;
        }

        dispatch(addPinned(item));

        dispatch(
            addToast({
                action: 'pin',
                item: `${getArtistName(item)} - ${item.title}`
            })
        );

        // Force waveform to redraw
        // #732
        setTimeout(() => window.dispatchEvent(new Event('resize')), 200);
    }

    unpinItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;

        if (!user) {
            return;
        }

        dispatch(deletePinned(item));

        dispatch(
            addToast({
                type: item.type,
                action: 'unpin',
                item: `${getArtistName(item)} - ${item.title}`
            })
        );

        // Force waveform to redraw
        // #732
        setTimeout(() => window.dispatchEvent(new Event('resize')), 200);
    }

    reupItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        const id = item.song_id || item.id;
        let action;
        // Dont allow a newly logged in user to undo on an item
        if (newlyLoggedInUser || user.reups.indexOf(id) === -1) {
            action = 'reup';
            dispatch(repost(id));
        } else {
            action = 'unreup';
            dispatch(unrepost(id));
        }

        dispatch(
            addToast({
                type: item.type,
                action: action,
                item: `${getArtistName(item)} - ${item.title}`
            })
        );
    }

    playlistItem(item) {
        const { dispatch } = this.props;
        // Account for album tracks which may not have id
        const id = item.id || item.song_id;

        dispatch(addSong(item));
        dispatch(getPlaylistsWithSongId(id));
        dispatch(showModal(MODAL_TYPE_ADD_TO_PLAYLIST));
    }

    adminItem(item) {
        const { dispatch, currentUser } = this.props;

        if (!currentUser.isAdmin) {
            return;
        }

        dispatch(showModal(MODAL_TYPE_ADMIN, { item: item }));
    }

    bumpItem(item, genre) {
        const { dispatch } = this.props;

        // When the API endpoint is installed this code should dispatch to the API
        //  To attempt to bump the track to the top of the trending chart it's on
        // And upon successful return from the API, physically move it to the top
        // If the top 20 songs are on the page, otherwise remove it from the page?
        const result = dispatch(bumpMusic(item, genre));

        return result;
    }

    bumpMusicOneSpot(item, genre) {
        const { dispatch } = this.props;

        const result = dispatch(bumpMusicOneSpot(item, genre));

        return result;
    }

    render() {
        const fixOnScrollProps = {
            className: 'music-detail-container',
            fixedClassName: 'music-detail-container--fixed',
            fixToId: 'main-header',
            onBeforeFixChange: this.handleBeforeFixedHeaderChange,
            onFixChange: this.handleFixedHeaderChange,
            offset: 305,
            ...(this.props.fixOnScrollProps || {})
        };

        const tags = mapTagOptions('normalizedKey', 'display', this.props.tags);

        return (
            <MusicDetail
                shouldLinkArtwork={this.props.shouldLinkArtwork}
                currentUser={this.props.currentUser}
                currentUserPinned={this.props.currentUserPinned}
                containerClassName={this.props.containerClassName}
                className={this.props.className}
                player={this.props.player}
                shouldFixOnScroll={this.props.shouldFixOnScroll}
                removeHeaderLinks={this.props.removeHeaderLinks}
                fixOnScrollProps={fixOnScrollProps}
                isFixed={this.state.isFixed}
                subtext={this.props.subtext}
                item={this.props.item}
                large={this.props.large}
                related={this.props.related}
                feed={this.props.feed}
                search={this.props.search}
                songKey={this.props.songKey}
                profile={this.props.profile}
                pinned={this.props.pinned}
                allowZoom={this.props.allowZoom}
                shouldShowReup={this.props.shouldShowReup}
                onItemClick={this.handleItemClick}
                onActionClick={this.handleActionClick}
                onShuffleClick={this.handleShuffleClick}
                index={this.props.index}
                context={this.props.context}
                ranking={this.props.ranking}
                shouldLazyLoadArtwork={this.props.shouldLazyLoadArtwork}
                hideWaveform={this.props.hideWaveform}
                hideInteractions={this.props.hideInteractions}
                onTruncate={this.handleTruncate}
                onToggleLines={this.handleToggleLines}
                expanded={this.state.expanded}
                truncated={this.state.truncated}
                nativeShareSupported={this.state.nativeShareSupported}
                hideLoadMoreTracksButton={this.state.hideLoadMoreTracksButton}
                isTracklistActivated={this.state.isTracklistActivated}
                onLoadMoreTracksButtonClick={
                    this.handleLoadMoreTracksButtonClick
                }
                onTrackActionClick={this.handleTrackActionClick}
                onCountdownFinish={this.props.onCountdownFinish}
                tooltipActive={this.state.tooltipActive}
                onTooltipClick={this.handleTooltipClick}
                showPinMenu={this.props.showPinMenu}
                isVerified={this.props.isVerified}
                onPromoLinkClick={this.handlePromoLinkClick}
                showArtworkPlayButton={this.props.showArtworkPlayButton}
                showFullGeoNotice={this.props.showFullGeoNotice}
                tags={tags}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        currentUserPinned: state.currentUserPinned,
        artistUploads: state.artistUploads,
        player: state.player,
        statsToken: state.stats.statsToken,
        tags: state.musicTags
    };
}

export default withRouter(connect(mapStateToProps)(MusicDetailContainer));
