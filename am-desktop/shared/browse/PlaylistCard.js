import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getDynamicImageProps } from 'utils/index';

import styles from './PlaylistCard.module.scss';

function PlaylistDetail({ item, shouldLazyLoadArtwork }) {
    if (!item) {
        return null;
    }

    const {
        type,
        url_slug: urlSlug,
        artist,
        title,
        track_count: trackCount
    } = item;
    const [src, srcSet] = getDynamicImageProps(item.image_base || item.image, {
        size: 245
    });

    let countLabel = 'Songs';

    if (item.genre === 'podcast') {
        countLabel = 'Episodes';
    }

    const artistLink = (
        <Link to={`/artist/${artist.url_slug}`}>{artist.name}</Link>
    );

    return (
        <div className={styles.card}>
            <div className={styles.image}>
                <Link
                    style={{ display: 'block' }}
                    to={`/${type}/${artist.url_slug}/${urlSlug}`}
                >
                    <img
                        src={src}
                        srcSet={srcSet}
                        loading={shouldLazyLoadArtwork ? 'lazy' : 'eager'}
                        alt=""
                    />
                </Link>
            </div>
            <div className={styles.details}>
                <h2 className={styles.title}>
                    <Link to={`/${type}/${artist.url_slug}/${urlSlug}`}>
                        {title}
                    </Link>
                </h2>
                <ul className={styles.meta}>
                    <li className={styles.metaItem}>
                        Total {countLabel}: <span>{trackCount}</span>
                    </li>
                    <li className={styles.metaItem}>
                        by <span>{artistLink}</span>
                    </li>
                </ul>
            </div>
        </div>
    );
}

PlaylistDetail.propTypes = {
    item: PropTypes.object,
    shouldLazyLoadArtwork: PropTypes.bool
};

export default PlaylistDetail;
