import React, { Component } from 'react';
import PropTypes from 'prop-types';

import BrowsePageMeta from 'components/BrowsePageMeta';
import {
    allGenresMap,
    liveGenres,
    liveGenresExcludedFromCharts,
    GENRE_TYPE_ALL,
    GENRE_TYPE_PODCAST,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_PLAYLIST,
    COLLECTION_TYPE_RECENTLY_ADDED,
    CHART_TYPE_DAILY,
    CHART_TYPE_WEEKLY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY,
    CHART_TYPE_TOTAL
} from 'constants/index';

// import InContentAd from '../ad/InContentAd';
import AndroidLoader from '../loaders/AndroidLoader';
import FeedBar from '../widgets/FeedBar';
import MusicFeedBlocks from '../ad/MusicFeedBlocks';

import BrowseEmptyState from './BrowseEmptyState';
import MusicDetailContainer from './MusicDetailContainer';

import FireIcon from '../icons/fire';
import GraphIcon from '../icons/graph';
import AlbumIcon from '../icons/albums';
import RecentIcon from '../icons/recent';

export default class Browse extends Component {
    static propTypes = {
        player: PropTypes.object,
        currentUser: PropTypes.object,
        musicBrowse: PropTypes.object,
        location: PropTypes.object,
        timeMap: PropTypes.object,
        onTimespanSwitch: PropTypes.func,
        featured: PropTypes.object
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getContextItems(
        activeContext = '',
        genre = '',
        activeTimePeriod = '',
        timeMap = {}
    ) {
        let context = '';
        let timePeriod = '';

        if (activeContext) {
            context = `/${activeContext}`;
        }

        const excludeFromTime = [
            COLLECTION_TYPE_TRENDING,
            COLLECTION_TYPE_RECENTLY_ADDED
        ];

        if (activeTimePeriod && !excludeFromTime.includes(activeContext)) {
            timePeriod = `/${timeMap[activeTimePeriod] || activeTimePeriod}`;
        }

        const items = liveGenres
            .map((liveGenre) => {
                const item = {
                    text: allGenresMap[liveGenre],
                    value: liveGenre,
                    href: `/${liveGenre}${context}${timePeriod}`,
                    active: genre === liveGenre
                };

                if (liveGenre === GENRE_TYPE_ALL) {
                    item.text = 'All Genres';
                    item.href =
                        `${
                            activeContext === COLLECTION_TYPE_TRENDING
                                ? '/trending-now'
                                : `${context}${timePeriod}`
                        }` || '/trending-now';
                }

                return item;
            })
            .filter(({ value: genreValue }) => {
                const onlyInTrending = liveGenresExcludedFromCharts.includes(
                    genreValue
                );
                // Remove podcast in case of top albums/songs but keep in recent
                if (
                    activeContext === COLLECTION_TYPE_ALBUM ||
                    activeContext === COLLECTION_TYPE_SONG
                ) {
                    return !onlyInTrending;
                }

                return true;
            });

        return items;
    }

    getTimespanOptions(activeContext, activeTimePeriod) {
        const collectionsThatNeedTime = [
            COLLECTION_TYPE_SONG,
            COLLECTION_TYPE_ALBUM,
            COLLECTION_TYPE_PLAYLIST
        ];

        if (collectionsThatNeedTime.indexOf(activeContext) !== -1) {
            return [
                {
                    text: 'Today',
                    value: 'day',
                    active: CHART_TYPE_DAILY === activeTimePeriod
                },
                {
                    text: 'This Week',
                    value: 'week',
                    active: CHART_TYPE_WEEKLY === activeTimePeriod
                },
                {
                    text: 'This Month',
                    value: 'month',
                    active: CHART_TYPE_MONTHLY === activeTimePeriod
                },
                {
                    text: 'This Year',
                    value: 'year',
                    active: CHART_TYPE_YEARLY === activeTimePeriod
                },
                {
                    text: 'All Time',
                    value: '',
                    active: CHART_TYPE_TOTAL === activeTimePeriod
                }
            ];
        }

        return [];
    }

    renderTitle(activeContext, activeGenre, activeTimePeriod) {
        let title = 'Trending Music';
        let icon = (
            <FireIcon className="feed-bar__title-icon feed-bar__title-icon--fire u-text-icon" />
        );
        let genre = '';
        let timeSpan = '';

        switch (activeTimePeriod) {
            case CHART_TYPE_DAILY:
                timeSpan = ' Today';
                break;

            case CHART_TYPE_WEEKLY:
                timeSpan = ' this Week';
                break;

            case CHART_TYPE_MONTHLY:
                timeSpan = ' this Month';
                break;

            case CHART_TYPE_YEARLY:
                timeSpan = ' this Year';
                break;

            default:
                break;
        }

        liveGenres.some((liveGenre) => {
            if (liveGenre === activeGenre) {
                genre = ` ${allGenresMap[activeGenre]}`;

                if (liveGenre === GENRE_TYPE_ALL) {
                    genre = ' All Genres';
                }
                return true;
            }

            return false;
        });

        switch (activeContext) {
            case COLLECTION_TYPE_TRENDING: {
                title = (
                    <span>
                        <strong>Trending Music</strong> in{genre}
                    </span>
                );
                icon = (
                    <GraphIcon className="feed-bar__title-icon feed-bar__title-icon--graph u-text-icon" />
                );
                if (activeGenre === GENRE_TYPE_ALL) {
                    title = (
                        <span>
                            <strong>Trending</strong>
                        </span>
                    );
                }
                if (activeGenre === GENRE_TYPE_PODCAST) {
                    title = (
                        <span>
                            <strong>Trending</strong>
                            {genre}
                        </span>
                    );
                }
                break;
            }

            case COLLECTION_TYPE_SONG: {
                title = (
                    <span>
                        <strong>Top Songs</strong> in {genre}
                        {timeSpan}
                    </span>
                );
                if (activeGenre === GENRE_TYPE_ALL) {
                    title = (
                        <span>
                            <strong>Top Songs</strong>
                            {timeSpan}
                        </span>
                    );
                }
                if (activeGenre === GENRE_TYPE_PODCAST) {
                    title = (
                        <span>
                            <strong>Top Podcasts</strong> {timeSpan}
                        </span>
                    );
                }
                break;
            }

            case COLLECTION_TYPE_ALBUM: {
                title = (
                    <span>
                        <strong>Top Albums</strong> in{genre}
                        {timeSpan}
                    </span>
                );
                icon = (
                    <AlbumIcon className="feed-bar__title-icon feed-bar__title-icon--album u-text-icon" />
                );
                if (activeGenre === GENRE_TYPE_ALL) {
                    title = (
                        <span>
                            <strong>Top Albums</strong>
                            {timeSpan}
                        </span>
                    );
                }
                if (activeGenre === GENRE_TYPE_PODCAST) {
                    title = (
                        <span>
                            <strong>Top Podcasts</strong>
                            {timeSpan}
                        </span>
                    );
                }
                break;
            }

            case COLLECTION_TYPE_RECENTLY_ADDED: {
                title = (
                    <span>
                        <strong>Recently Added Music</strong> in{genre}
                    </span>
                );
                icon = (
                    <RecentIcon className="feed-bar__title-icon feed-bar__title-icon--recent u-text-icon" />
                );
                if (activeGenre === GENRE_TYPE_ALL) {
                    title = (
                        <span>
                            <strong>Recently Added Music</strong>
                        </span>
                    );
                }
                if (activeGenre === GENRE_TYPE_PODCAST) {
                    title = (
                        <span>
                            <strong>Recently Added</strong> in{genre}
                        </span>
                    );
                }
                break;
            }

            default:
                title = (
                    <span>
                        <strong>Trending Music</strong> in{genre}
                    </span>
                );
        }

        return (
            <h2 className="feed-bar__title">
                {icon} {title}
            </h2>
        );
    }

    renderFeed(data = {}, featuredObject, location, currentUser) {
        const { list, activeContext, activeGenre } = data;

        const filteredList = list.filter((item) => {
            return (
                item.status !== 'suspended' &&
                item.status !== 'takedown' &&
                !item.geo_restricted
            );
        });

        const items = filteredList.map((musicItem, i) => {
            let ranking = null;

            if (activeContext === 'songs' || activeContext === 'albums') {
                ranking = i + 1;
            }

            return (
                <div className="u-spacing-bottom-60" key={i}>
                    <MusicDetailContainer
                        index={i}
                        key={`${musicItem.id}-${i}`}
                        item={musicItem}
                        shouldLazyLoadArtwork={i > 2}
                        shouldLinkArtwork
                        feed
                        musicList={filteredList}
                        context={activeContext}
                        genre={activeGenre}
                        ranking={ranking}
                    />
                </div>
            );
        });

        if (list.length) {
            const index = Math.min(3, list.length);
            const featured = (
                <MusicFeedBlocks
                    currentUser={currentUser}
                    key="blocks"
                    featuredItems={featuredObject.list}
                    location={location}
                />
            );

            items.splice(index, 0, featured);

            /*
            // place incontent ads after 8th music item, and then every 10 music items
            let incontentIndex = 9;

            if (list.length > 8) {
                const incontentAd = (
                    <div
                        key={`incontentAd-${incontentIndex}`}
                        className="u-pos-relative u-spacing-bottom-60 u-clearfix u-flex-center"
                        style={{ padding: 0 }}
                    >
                        <InContentAd
                            location={location}
                            currentUser={currentUser}
                            adCount={incontentIndex}
                        />
                    </div>
                );

                items.splice(incontentIndex, 0, incontentAd);
            }

            incontentIndex += 10;
            while (typeof list[incontentIndex] !== 'undefined') {
                const incontentAd = (
                    <div
                        key={`incontentAd-${incontentIndex}`}
                        className="u-spacing-bottom-60 u-pos-relative u-clearfix u-flex-center"
                        style={{ padding: 0 }}
                    >
                        <InContentAd
                            location={location}
                            adCount={incontentIndex}
                            currentUser={currentUser}
                        />
                    </div>
                );

                items.splice(incontentIndex, 0, incontentAd);
                incontentIndex += 10;
            }
            */
        }

        return (
            <div data-testid="browseContainer" className="column small-24">
                {items}
            </div>
        );
    }

    render() {
        const {
            musicBrowse,
            featured,
            location,
            timeMap,
            currentUser
        } = this.props;
        const {
            activeContext,
            activePlaylistContext,
            activeGenre,
            activeTimePeriod,
            loading,
            page,
            onLastPage
        } = musicBrowse;

        let loader;
        let emptyState;

        if (loading) {
            loader = <AndroidLoader className="music-feed__loader" />;
        }

        if (!loading && !musicBrowse.list.length) {
            emptyState = (
                <BrowseEmptyState
                    activeContext={activeContext}
                    activeGenre={activeGenre}
                    activePlaylistContext={activePlaylistContext}
                />
            );
        }

        return (
            <div className="row music-feed music-feed--chart">
                <BrowsePageMeta
                    page={page}
                    onLastPage={onLastPage}
                    activeGenre={activeGenre}
                    activeContext={activeContext}
                    activeTimePeriod={activeTimePeriod}
                />
                <header className="column small-24">
                    <div className="music-feed__context-switcher u-box-shadow">
                        <FeedBar
                            title={this.renderTitle(
                                activeContext,
                                activeGenre,
                                activeTimePeriod
                            )}
                            items={this.getContextItems(
                                activeContext,
                                activeGenre,
                                activeTimePeriod,
                                timeMap
                            )}
                            options={this.getTimespanOptions(
                                activeContext,
                                activeTimePeriod
                            )}
                            activeMarker={activeGenre}
                            onTimespanSwitch={this.props.onTimespanSwitch}
                            contextDropDown
                            padded
                        />
                    </div>
                </header>
                <div className="row expanded column small-24">
                    {this.renderFeed(
                        musicBrowse,
                        featured,
                        location,
                        currentUser
                    )}
                    {emptyState}
                    {loader}
                </div>
            </div>
        );
    }
}
