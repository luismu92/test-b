import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './MusicDetailPlaceholder.module.scss';

export default function MusicDetailPlaceholder({ className, animated }) {
    const klass = classnames(styles.container, {
        [className]: className
    });
    const bgClass = classnames(styles.background, {
        [styles.animated]: animated
    });
    const avatarClass = classnames(styles.avatar, {
        [styles.animated]: animated
    });

    return (
        <div className={klass}>
            <div className={avatarClass}>
                <span />
            </div>
            <div className={bgClass}>
                <span />
            </div>
        </div>
    );
}

MusicDetailPlaceholder.propTypes = {
    className: PropTypes.string,
    animated: PropTypes.bool
};
