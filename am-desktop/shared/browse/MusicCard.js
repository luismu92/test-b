import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    renderFeaturingLinks,
    isCurrentMusicItem,
    getUploader,
    buildDynamicImage
} from 'utils/index';

import ArtworkPlayButton from '../components/ArtworkPlayButton';
import BodyClickListener from '../components/BodyClickListener';

import HeartIcon from '../../../am-shared/icons/heart';
import RetweetIcon from '../icons/retweet';
import PlusIcon from '../icons/plus';
import EmbedIcon from '../icons/embed-close';
import ThreeDotsIcon from '../icons/three-dots-alt';
import Verified from '../../../am-shared/components/Verified';

export default class MusicCard extends Component {
    static propTypes = {
        containerClassName: PropTypes.string,
        player: PropTypes.object,
        item: PropTypes.object,
        removeHeaderLinks: PropTypes.bool,
        onItemClick: PropTypes.func,
        onActionClick: PropTypes.func,
        index: PropTypes.number,
        context: PropTypes.string,
        ranking: PropTypes.number,
        tooltipActive: PropTypes.bool,
        onTooltipClick: PropTypes.func
    };

    static defaultProps = {
        onActionClick() {},
        onTooltipClick() {},
        tooltipActive: false
    };

    componentWillUnmount() {
        this._tooltipButton = null;
    }

    //////////////////////
    // Internal methods //
    //////////////////////

    renderFeaturing(featuring) {
        if (!featuring) {
            return null;
        }

        return (
            <li className="u-trunc u-ls-n-05 u-lh-13">
                <strong>{renderFeaturingLinks(featuring)}</strong>
            </li>
        );
    }

    renderReleased(item, check) {
        const { released } = item;
        const uploader = getUploader(item);

        if (!released) {
            return null;
        }

        return (
            <li>
                <span className="u-lh-13 u-ls-n-07">
                    by{' '}
                    <Link to={`/artist/${uploader.url_slug}`} data-popover>
                        <strong>{uploader.name}</strong>
                    </Link>{' '}
                    {check}
                </span>
            </li>
        );
    }

    renderHeader(item, removeHeaderLinks) {
        const { title, type } = item;
        const uploader = getUploader(item);
        const artist = item.artist.name || item.artist;
        let artistText;

        if (type !== 'playlist') {
            artistText = (
                <span className="music__heading--artist u-trunc u-d-block">
                    {artist}
                </span>
            );
        }

        let header = (
            <h2 className="music__heading">
                {artistText}
                <span className="music__heading--title u-trunc u-d-block">
                    {title}
                </span>
            </h2>
        );

        if (!removeHeaderLinks) {
            header = (
                <Link
                    className="music-detail__link"
                    to={`/${item.type}/${uploader.url_slug}/${item.url_slug}`}
                >
                    {header}
                </Link>
            );
        }

        return header;
    }

    renderRank(context, rank) {
        if (context === 'songs' || context === 'albums') {
            return rank;
        }

        return null;
    }

    renderInteractionMenu() {
        const { tooltipActive, onTooltipClick, item } = this.props;

        const favLink = (
            <li className="sub-menu__item">
                <button
                    title="Favorite"
                    aria-label="Favorite"
                    data-action="favorite"
                    onClick={this.props.onActionClick}
                >
                    <HeartIcon className="sub-menu__icon sub-menu__icon--fav" />
                    Favorite
                </button>
            </li>
        );

        const reupLink = (
            <li className="sub-menu__item">
                <button
                    title="Re-Up"
                    aria-label="Re-Up"
                    data-action="reup"
                    onClick={this.props.onActionClick}
                >
                    <RetweetIcon className="sub-menu__icon sub-menu__icon--reup" />
                    Re-Up
                </button>
            </li>
        );

        let addLink;

        if (item.type === 'song') {
            addLink = (
                <li className="sub-menu__item">
                    <button
                        title="Add to Playlist"
                        aria-label="Add to Playlist"
                        data-action="playlist"
                        onClick={this.props.onActionClick}
                    >
                        <PlusIcon className="sub-menu__icon sub-menu__icon--plus" />
                        Add to Playlist
                    </button>
                </li>
            );
        }

        const embedLink = (
            <li className="sub-menu__item">
                <button
                    title="Embed"
                    aria-label="Embed"
                    data-action="embed"
                    onClick={this.props.onActionClick}
                >
                    <EmbedIcon className="sub-menu__icon sub-menu__icon--embed" />
                    Embed
                </button>
            </li>
        );

        const klass = classnames('music-card__interactions', {
            'music-card__interactions--active': tooltipActive
        });

        const tooltipClass = classnames(
            'tooltip tooltip--down-right-arrow sub-menu',
            {
                'tooltip--active': tooltipActive
            }
        );

        return (
            <div className={klass}>
                <button
                    className="music-card__interactions-trigger"
                    onClick={onTooltipClick}
                    ref={(e) => {
                        this._tooltipButton = e;
                    }}
                >
                    <ThreeDotsIcon />
                </button>
                <BodyClickListener
                    shouldListen={tooltipActive}
                    onClick={onTooltipClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <ul className={tooltipClass}>
                    {favLink}
                    {addLink}
                    {reupLink}
                    {embedLink}
                </ul>
            </div>
        );
    }

    render() {
        const {
            item,
            player,
            index,
            ranking,
            context,
            removeHeaderLinks,
            containerClassName
        } = this.props;

        if (!item) {
            // @todo maybe put a skeleton view of the music detail item here?
            return null;
        }

        const { featuring } = item;
        const uploader = getUploader(item);
        const { currentSong, paused, loading } = player;
        const isCurrentSong = isCurrentMusicItem(currentSong, item);
        const isPaused = !isCurrentSong || (isCurrentSong && paused);
        const isLoading = isCurrentSong && loading;
        const check = <Verified user={uploader} small />;
        const imageSize = 250;

        const artwork = buildDynamicImage(item.image_base || item.image, {
            width: imageSize,
            height: imageSize,
            max: true
        });
        const retinaArtwork = buildDynamicImage(item.image_base || item.image, {
            width: imageSize * 2,
            height: imageSize * 2,
            max: true
        });

        const containerClass = classnames('music-card-container u-box-shadow', {
            [containerClassName]: containerClassName
        });

        const artworkPlayClass = classnames('music-card__artwork u-d-block', {
            'music-card__artwork--playing': isCurrentSong
        });

        return (
            <div className={containerClass}>
                <div className="music-card u-pos-relative">
                    <ArtworkPlayButton
                        image={artwork}
                        fullImage={retinaArtwork}
                        paused={isPaused}
                        loading={isLoading}
                        className={artworkPlayClass}
                        onButtonClick={this.props.onItemClick}
                        buttonProps={{
                            'data-music-index': index
                        }}
                        ranking={this.renderRank(context, ranking)}
                        playButton
                    />
                    <div className="music-card__content u-padding-em">
                        {this.renderHeader(item, removeHeaderLinks)}
                        {this.renderFeaturing(featuring)}
                        {this.renderReleased(item, check)}
                    </div>
                    {this.renderInteractionMenu()}
                </div>
            </div>
        );
    }
}
