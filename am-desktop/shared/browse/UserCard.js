import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import { buildDynamicImage } from 'utils/index';
import Truncate from 'components/Truncate';
import Verified from 'components/Verified';
import Avatar from 'components/Avatar';

import FollowButtonContainer from '../components/FollowButtonContainer';

export default class UserCard extends Component {
    static propTypes = {
        className: PropTypes.string,
        user: PropTypes.object,
        shouldLazyLoadArtwork: PropTypes.bool
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    renderFollowers(user) {
        return (
            <p className="user-card__followers">
                <span>{(user.followers_count || 0).toLocaleString()}</span>{' '}
                Followers
            </p>
        );
    }

    renderBio(bio) {
        if (!bio) {
            return null;
        }

        return (
            <Truncate
                className="user-card__bio"
                lines={2}
                text={bio}
                ellipsis={'…'}
            />
        );
    }

    render() {
        const { user, className, shouldLazyLoadArtwork } = this.props;

        if (!user) {
            // @todo maybe put a skeleton view of the user detail item here?
            return null;
        }

        const check = <Verified user={user} small />;

        const klass = classnames('user-card', {
            [className]: className
        });

        const imageSize = 90;

        const artwork = buildDynamicImage(user.image_base || user.image, {
            width: imageSize,
            height: imageSize,
            max: true
        });

        const retinaArtwork = buildDynamicImage(user.image_base || user.image, {
            width: imageSize * 2,
            height: imageSize * 2,
            max: true
        });
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }

        return (
            <div className={klass}>
                <div className="user-card__inner u-text-center">
                    <header className="user-card__header">
                        <Link
                            className="user-card__link"
                            to={`/artist/${user.url_slug}`}
                        >
                            <Avatar
                                image={artwork}
                                srcSet={srcSet}
                                type="artist"
                                className="user-card__avatar"
                                lazyLoad={shouldLazyLoadArtwork}
                                size={imageSize}
                                circle
                            />
                        </Link>
                    </header>
                    <div className="user-card__main">
                        <Link
                            className="user-card__link"
                            to={`/artist/${user.url_slug}`}
                        >
                            <h2 className="user-card__title">
                                {user.name} {check}
                            </h2>
                        </Link>
                        {this.renderFollowers(user)}
                        {this.renderBio(user.bio)}
                    </div>
                    <footer className="user-card__footer">
                        <FollowButtonContainer artist={user} />
                    </footer>
                </div>
            </div>
        );
    }
}
