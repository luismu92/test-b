import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import {
    isCurrentMusicItem,
    getArtistName,
    getQueueIndexForSong
} from 'utils/index';

import {
    favorite,
    unfavorite,
    repost,
    unrepost,
    queueAction
} from '../redux/modules/user';
import { getPlaylistsWithSongId } from '../redux/modules/user/playlists';
import {
    favoritePlaylist,
    unfavoritePlaylist,
    addSong
} from '../redux/modules/playlist';
import { setSection } from '../redux/modules/stats';
import {
    showModal,
    MODAL_TYPE_AUTH,
    MODAL_TYPE_ADD_TO_PLAYLIST,
    MODAL_TYPE_ADMIN,
    MODAL_TYPE_EMBED,
    MODAL_TYPE_QUEUE_ALERT
} from '../redux/modules/modal';
import { addToast } from '../redux/modules/toastNotification';
import { play, pause, editQueue, shuffleTracks } from '../redux/modules/player';
import { bumpMusic } from '../redux/modules/admin';
import { trackAction } from '../redux/modules/stats';

import MusicCard from './MusicCard';

class MusicCardContainer extends Component {
    static propTypes = {
        musicList: PropTypes.array.isRequired,
        index: PropTypes.number,
        dispatch: PropTypes.func,
        containerClassName: PropTypes.string,
        player: PropTypes.object,
        item: PropTypes.object,
        currentUser: PropTypes.object,
        artistUploads: PropTypes.object,
        removeHeaderLinks: PropTypes.bool,
        context: PropTypes.string,
        genre: PropTypes.string,
        ranking: PropTypes.number,
        statsToken: PropTypes.string,
        section: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            tooltipActive: false
        };
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleItemClick = (event) => {
        const { dispatch, player, musicList, section } = this.props;
        const index =
            parseInt(
                event.currentTarget.getAttribute('data-music-index'),
                10
            ) || 0;
        const currentSong = player.currentSong;
        const musicItem = musicList[index];
        const isAlreadyCurrentSong = isCurrentMusicItem(currentSong, musicItem);

        if (!isAlreadyCurrentSong && section) {
            dispatch(setSection(section));
        }

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        const queueIndex = getQueueIndexForSong(musicItem, player.queue, {
            currentQueueIndex: player.queueIndex
        });

        if (queueIndex !== -1) {
            dispatch(play(queueIndex));
            return;
        }

        if (player.queueHasBeenEdited) {
            dispatch(
                showModal(MODAL_TYPE_QUEUE_ALERT, {
                    item: musicItem,
                    onAppendClick() {
                        const { queue: newQueue } = dispatch(
                            editQueue(musicList, { append: true })
                        );
                        const newIndex = getQueueIndexForSong(
                            musicItem,
                            newQueue,
                            {
                                currentQueueIndex: player.queueIndex
                            }
                        );

                        dispatch(play(newIndex));
                    },
                    onReplaceClick: () =>
                        this.replaceQueueAndPlay(musicList, musicItem)
                })
            );
            return;
        }

        this.replaceQueueAndPlay(musicList, musicItem);
    };

    handleTooltipClick = () => {
        this.setState({
            tooltipActive: !this.state.tooltipActive
        });
    };

    handleActionClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');
        const { item } = this.props;

        this.doAction(action, item, e);

        this.setState({
            tooltipActive: false
        });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    replaceQueueAndPlay(queue, musicItem) {
        const { dispatch } = this.props;
        const { queue: newQueue } = dispatch(editQueue(queue));
        const queueIndex = getQueueIndexForSong(musicItem, newQueue);

        dispatch(play(queueIndex));
    }

    showLoginModal() {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_AUTH, { type: 'login' }));
    }

    doAction(action, item, e) {
        const { dispatch, genre, statsToken, currentUser } = this.props;

        switch (action) {
            case 'play': {
                this.handleItemClick(e);
                break;
            }

            case 'favorite': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.favoriteItem(item, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'reup': {
                const queuedAction = (newlyLoggedInUser) =>
                    this.reupItem(item, newlyLoggedInUser);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'playlist': {
                const queuedAction = () => this.playlistItem(item);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'embed': {
                dispatch(
                    showModal(MODAL_TYPE_EMBED, {
                        item
                    })
                );
                break;
            }

            case 'tags': {
                this.handleTagsClick(item);
                break;
            }

            case 'admin': {
                const queuedAction = () => this.adminItem(item);

                if (!currentUser.profile) {
                    dispatch(queueAction(queuedAction));
                    this.showLoginModal();
                    return;
                }

                queuedAction();
                break;
            }

            case 'bump':
                this.bumpItem(item, genre);
                break;

            case 'download': {
                dispatch(trackAction(statsToken, 'dl', item.id));

                const downloadUrl = e.currentTarget.getAttribute('data-url');

                if (downloadUrl) {
                    window.location.href = downloadUrl;
                }
                break;
            }

            default:
                break;
        }
    }

    playAlbum(album, trackIndex = 0, shuffle = false) {
        const { dispatch, player, artistUploads } = this.props;

        this.setState({
            hideLoadMoreTracksButton: true
        });

        if (album) {
            if (player.queue.length !== 1 || album !== player.queue[0]) {
                const uploads = artistUploads.list.filter((item) => {
                    return item.id !== album.id;
                });
                const newQueue = [album].concat(uploads);

                dispatch(editQueue(newQueue));
            }

            if (shuffle) {
                dispatch(shuffleTracks());
            }

            dispatch(play(trackIndex));
        }
    }

    favoriteItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        let action;

        if (item.type === 'playlist') {
            // Dont allow a newly logged in user to undo on an item
            if (
                newlyLoggedInUser ||
                user.favorite_playlists.indexOf(item.id) === -1
            ) {
                action = 'favorite';
                dispatch(favoritePlaylist(item.id));
            } else {
                action = 'unfavorite';
                dispatch(unfavoritePlaylist(item.id));
            }
        }
        // Dont allow a newly logged in user to undo on an item
        else if (
            newlyLoggedInUser ||
            user.favorite_music.indexOf(item.id) === -1
        ) {
            action = 'favorite';
            dispatch(favorite(item.id));
        } else {
            action = 'unfavorite';
            dispatch(unfavorite(item.id));
        }

        dispatch(
            addToast({
                type: item.type,
                action: action,
                item: `${getArtistName(item)} - ${item.title}`
            })
        );
    }

    reupItem(item, newlyLoggedInUser) {
        const { dispatch, currentUser } = this.props;
        const user = newlyLoggedInUser || currentUser.profile;
        let action;

        // Make sure we're comparing the same types
        const itemId = item.id;

        // Dont allow a newly logged in user to undo on an item
        if (newlyLoggedInUser || user.reups.indexOf(itemId) === -1) {
            action = 'reup';
            dispatch(repost(itemId));
        } else {
            action = 'unreup';
            dispatch(unrepost(itemId));
        }

        dispatch(
            addToast({
                type: item.type,
                action: action,
                item: `${getArtistName(item)} - ${item.title}`
            })
        );
    }

    playlistItem(item) {
        const { dispatch } = this.props;
        // Account for album tracks which may not have id
        const id = item.id || item.song_id;

        dispatch(addSong(item));
        dispatch(getPlaylistsWithSongId(id));
        dispatch(showModal(MODAL_TYPE_ADD_TO_PLAYLIST));
    }

    adminItem(item) {
        const { dispatch, currentUser } = this.props;

        if (!currentUser.isAdmin) {
            return;
        }

        dispatch(showModal(MODAL_TYPE_ADMIN, { item: item }));
    }

    bumpItem(item, genre) {
        const { dispatch } = this.props;

        // When the API endpoint is installed this code should dispatch to the API
        //  To attempt to bump the track to the top of the trending chart it's on
        // And upon successful return from the API, physically move it to the top
        // If the top 20 songs are on the page, otherwise remove it from the page?
        const result = dispatch(bumpMusic(item, genre));

        return result;
    }

    render() {
        return (
            <MusicCard
                containerClassName={this.props.containerClassName}
                player={this.props.player}
                removeHeaderLinks={this.props.removeHeaderLinks}
                item={this.props.item}
                onItemClick={this.handleItemClick}
                onActionClick={this.handleActionClick}
                index={this.props.index}
                context={this.props.context}
                ranking={this.props.ranking}
                tooltipActive={this.state.tooltipActive}
                onTooltipClick={this.handleTooltipClick}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        artistUploads: state.artistUploads,
        player: state.player,
        statsToken: state.stats.statsToken
    };
}

export default withRouter(connect(mapStateToProps)(MusicCardContainer));
