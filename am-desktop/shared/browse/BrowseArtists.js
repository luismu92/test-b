import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    allGenresMap,
    GENRE_TYPE_ALL,
    GENRE_TYPE_RAP,
    GENRE_TYPE_RNB,
    GENRE_TYPE_ELECTRONIC,
    GENRE_TYPE_DANCEHALL,
    GENRE_TYPE_PODCAST,
    GENRE_TYPE_POP,
    GENRE_TYPE_AFROBEATS,
    GENRE_TYPE_LATIN,
    GENRE_TYPE_INSTRUMENTAL,
    COLLECTION_TYPE_ARTIST
} from 'constants/index';
import BrowseArtistMeta from 'components/BrowseArtistMeta';

import AndroidLoader from '../loaders/AndroidLoader';
import FeedBar from '../widgets/FeedBar';

import BrowseEmptyState from './BrowseEmptyState';
import UserCard from './UserCard';
import UserIcon from '../icons/avatar';

export default class Browse extends Component {
    static propTypes = {
        artistBrowse: PropTypes.object,
        onGenreSwitch: PropTypes.func
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getContextItems(genre = '') {
        const activeContext = COLLECTION_TYPE_ARTIST;

        let context = '';

        if (activeContext) {
            context = `/${activeContext}/popular`;
        }

        const genres = [
            GENRE_TYPE_RAP,
            GENRE_TYPE_RNB,
            GENRE_TYPE_ELECTRONIC,
            GENRE_TYPE_DANCEHALL,
            GENRE_TYPE_POP,
            GENRE_TYPE_AFROBEATS,
            GENRE_TYPE_PODCAST,
            GENRE_TYPE_LATIN,
            GENRE_TYPE_INSTRUMENTAL
        ].map((type) => {
            return {
                text: allGenresMap[type],
                value: type,
                href: `/${type}${context}`,
                active: genre === type
            };
        });

        return [
            {
                text: 'All Genres',
                value: GENRE_TYPE_ALL,
                href: `${context}` || '/',
                active: genre === ''
            }
        ].concat(genres);
    }

    renderFeed(data = []) {
        const items = data.map((accountItem, i) => {
            return (
                <div
                    className="column small-12 large-6 xlarge-fifth"
                    key={`${accountItem.id}-${i}`}
                >
                    <UserCard
                        index={i}
                        user={accountItem}
                        className="user-card--shadow"
                    />
                </div>
            );
        });

        return items;
    }

    render() {
        const { artistBrowse /* , onGenreSwitch */ } = this.props;
        const { activeGenre, loading } = artistBrowse;

        let loader;

        if (loading) {
            loader = <AndroidLoader className="music-feed__loader" />;
        }

        let emptyState;

        if (!loading && !artistBrowse.list.length) {
            emptyState = <BrowseEmptyState activeGenre={activeGenre} />;
        }

        let genreSuffix = '';

        if (artistBrowse.activeGenre) {
            genreSuffix = ` in ${allGenresMap[artistBrowse.activeGenre]}`;
        }

        const title = (
            <h2 className="feed-bar__title">
                <UserIcon className="feed-bar__title-icon feed-bar__title-icon--playlist u-text-icon" />
                <span>
                    <strong>Accounts to Follow</strong>
                    {genreSuffix}
                </span>
            </h2>
        );
        // const genres = Object.keys(allGenresMap).map((genre) => {
        //     return {
        //         text: allGenresMap[genre],
        //         value: genre,
        //         active: genre === artistBrowse.activeGenre
        //     };
        // });
        const list = artistBrowse.list.filter((item) => {
            if (!artistBrowse.activeGenre) {
                return true;
            }

            return item.genre === artistBrowse.activeGenre;
        });

        return (
            <div className="music-feed">
                <BrowseArtistMeta />
                <div className="row">
                    <div className="column small-24">
                        <FeedBar
                            className="feed-bar--artists"
                            title={title}
                            padded
                        />
                        {/* items={genres}
                        onContextSwitch={onGenreSwitch}
                    contextDropDown */}
                    </div>
                    {this.renderFeed(list)}
                    <div className="column small-24 u-text-center">
                        {emptyState}
                        {loader}
                    </div>
                </div>
            </div>
        );
    }
}
