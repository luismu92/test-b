import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    allGenresMap,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_PLAYLIST,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    PLAYLIST_TYPE_BROWSE,
    PLAYLIST_TYPE_MINE,
    PLAYLIST_TYPE_FAVORITES
} from 'constants/index';

export default class BrowseEmptyState extends Component {
    static propTypes = {
        activeContext: PropTypes.string,
        activeGenre: PropTypes.string,
        activePlaylistContext: PropTypes.string
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    renderEmptyMessage() {
        const {
            activeContext,
            activeGenre,
            activePlaylistContext
        } = this.props;
        const music = (activeGenre && allGenresMap[activeGenre]) || 'music';

        switch (activeContext) {
            case COLLECTION_TYPE_TRENDING:
                return `No trending ${music} found.`;

            case COLLECTION_TYPE_SONG:
                return `No ${music} songs found.`;

            case COLLECTION_TYPE_PLAYLIST:
                switch (activePlaylistContext) {
                    case PLAYLIST_TYPE_BROWSE:
                        return 'Something went wrong loading the trending playlists, please try again later.';

                    case PLAYLIST_TYPE_MINE:
                        return "You haven't created any playlists yet.";

                    case PLAYLIST_TYPE_FAVORITES:
                        return 'You have no favorited playlists.';

                    default:
                        // console.warn(`${activePlaylistContext} is unaccounted for with its empty message`);

                        return 'Nothing found here.';
                }

            case COLLECTION_TYPE_ALBUM:
                return `No ${music} albums found.`;

            case COLLECTION_TYPE_RECENTLY_ADDED:
                return `No recent ${music} found.`;

            default:
                // console.warn(`context: "${activeContext}" is unaccounted for with its empty message`);
                return 'Nothing found here.';
        }
    }

    render() {
        return (
            <div className="music-feed__empty">
                <p>{this.renderEmptyMessage()}</p>
            </div>
        );
    }
}
