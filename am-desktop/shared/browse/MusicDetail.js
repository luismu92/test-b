import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';

import { DEFAULT_DATE_FORMAT } from 'constants/index';
import {
    convertSecondsToTimecode,
    renderFeaturingLinks,
    isCurrentMusicItem,
    getUploader,
    getMusicUrl,
    queueHasMusicItem,
    buildDynamicImage,
    getArtistName,
    getFeaturing,
    yesBool,
    ownsMusic
} from 'utils/index';
import WaveformContainer from 'components/WaveformContainer';
import Avatar from '../components/Avatar';

import BodyClickListener from '../components/BodyClickListener';

import TrackListButtons from '../components/TrackListButtons';
import PlayButton from '../components/PlayButton';
import Interactions from '../components/Interactions';
import FixOnScroll from '../utils/FixOnScroll';
import MusicDetailTrackListing from './MusicDetailTrackListing';

import ThreeDotsIcon from '../icons/three-dots-alt';
import RetweetIcon from '../icons/retweet';
import StarIcon from '../icons/star';
import LinkIcon from 'icons/link-alt';
import WarningIcon from '../icons/warning';
import SocialShare from '../components/SocialShare';
import Verified from 'components/Verified';
import Countdown from '../components/Countdown';
import GeoNotice from '../components/GeoNotice';

export default class MusicDetail extends Component {
    static propTypes = {
        containerClassName: PropTypes.string,
        className: PropTypes.string,
        currentUser: PropTypes.object,
        currentUserPinned: PropTypes.object,
        player: PropTypes.object,
        item: PropTypes.object,
        hideWaveform: PropTypes.bool,
        large: PropTypes.bool,
        related: PropTypes.bool,
        feed: PropTypes.bool,
        search: PropTypes.bool,
        profile: PropTypes.bool,
        pinned: PropTypes.bool,
        removeHeaderLinks: PropTypes.bool,
        nativeShareSupported: PropTypes.bool,
        allowZoom: PropTypes.bool,
        onItemClick: PropTypes.func,
        onShuffleClick: PropTypes.func,
        onActionClick: PropTypes.func,
        index: PropTypes.number,
        context: PropTypes.string,
        songKey: PropTypes.string,
        ranking: PropTypes.number,
        expanded: PropTypes.bool,
        shouldLazyLoadArtwork: PropTypes.bool,
        shouldShowReup: PropTypes.bool,
        hideInteractions: PropTypes.bool,
        shouldLinkArtwork: PropTypes.bool,
        shouldShowPlayButton: PropTypes.bool,
        truncated: PropTypes.bool,
        hideLoadMoreTracksButton: PropTypes.bool,
        isTracklistActivated: PropTypes.bool,
        onTrackActionClick: PropTypes.func,
        onLoadMoreTracksButtonClick: PropTypes.func,
        onCountdownFinish: PropTypes.func,
        shouldFixOnScroll: PropTypes.bool,
        isFixed: PropTypes.bool,
        fixOnScrollProps: PropTypes.object,
        tooltipActive: PropTypes.bool,
        onTooltipClick: PropTypes.func,
        showPinMenu: PropTypes.bool,
        isVerified: PropTypes.bool,
        onPromoLinkClick: PropTypes.func,
        showArtworkPlayButton: PropTypes.bool,
        showFullGeoNotice: PropTypes.bool,
        tags: PropTypes.object
    };

    static defaultProps = {
        onActionClick() {},
        shouldShowPlayButton: true,
        shouldLinkArtwork: false,
        hideInteractions: false,
        showFullGeoNotice: true
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    // Edited from https://stackoverflow.com/questions/369147/javascript-regex-to-extract-anchor-text-and-url-from-anchor-tags
    // @todo turn audiomack.com links into <Link> tags
    getAlteredLinks(text) {
        return text.replace(
            /<a href=["|'][^"|']+["|']>[^<]+<\/a>/g,
            (match) => {
                return match.replace(
                    '<a',
                    '<a rel="nofollow noopener" target="_blank"'
                );
            }
        );
    }

    getRuntime(item) {
        if (!item.tracks || !item.tracks.length) {
            return '0 mins, 0 songs';
        }

        let plus = '';
        const seconds = item.tracks.reduce((sum, track) => {
            if (!track.duration) {
                plus = '+';
            }

            return sum + (parseInt(track.duration, 10) || 0);
        }, 0);
        const minutes = Math.round(seconds / 60);

        let mins = 'mins';

        if (minutes === 1) {
            mins = 'min';
        }

        let songs = 'songs';

        if (item.tracks.length === 1) {
            songs = 'song';
        }

        return `${minutes}${plus} ${mins}, ${item.tracks.length} ${songs}`;
    }

    shouldShowPlayButton(item, isLarge) {
        if (item.released_offset > 0) {
            return false;
        }

        if (item.status === 'takedown') {
            return false;
        }

        if (item.status === 'suspended') {
            return false;
        }

        if (item.status === 'unplayable') {
            return false;
        }

        const isTrackItem = item.type === 'album' || item.type === 'playlist';

        if ((item.type === 'song' && isLarge) || (isTrackItem && isLarge)) {
            return false;
        }

        return true;
    }

    shouldShowInteractions(item) {
        if (item.type === 'playlist' && this.props.search) {
            return false;
        }

        if (item.released_offset > 0) {
            return false;
        }

        if (item.status === 'takedown') {
            return false;
        }

        if (item.status === 'suspended') {
            return false;
        }

        if (item.status === 'unplayable') {
            return false;
        }

        return true;
    }

    isNotReleased(item) {
        let hasStreamingUrl = !!item.streaming_url;

        if (item.tracks && item.tracks.length) {
            hasStreamingUrl = item.tracks.some((t) => t.streaming_url);
        }

        return (
            item.released_offset > 0 && !(this.props.songKey && hasStreamingUrl)
        );
    }

    renderFeaturing(featuring) {
        if (!featuring) {
            return null;
        }

        return (
            <li className="u-trunc u-hide-when-fixed music__meta-featuring">
                <strong>{renderFeaturingLinks(featuring)}</strong>
            </li>
        );
    }

    renderAlbum(item) {
        if (!item.album && !item.album_details) {
            return null;
        }

        const title = (item.album_details || {}).title || item.album;

        let album = item.album;

        if (item.album_details) {
            const fauxItem = {
                ...item.album_details,
                type: 'album',
                title,
                uploader: getUploader(item)
            };

            const { geo_restricted: geoRestricted } = item;
            const { showFullGeoNotice } = this.props;
            const tabIndex = geoRestricted && showFullGeoNotice ? '-1' : '0';

            album = (
                <Link
                    style={{ fontWeight: 600 }}
                    to={getMusicUrl(fauxItem)}
                    tabIndex={tabIndex}
                >
                    {title}
                </Link>
            );
        }

        return (
            <li className="music__meta-album u-trunc u-hide-when-fixed u-hide-when-pin-condensed">
                <span className="u-fw-700">Album:</span> {album}
            </li>
        );
    }

    renderReleased(item = {}, check, large) {
        const { released, geo_restricted: geoRestricted } = item;
        const uploader = getUploader(item);

        if (!released || large) {
            return null;
        }

        const { showFullGeoNotice } = this.props;
        const tabIndex = geoRestricted && showFullGeoNotice ? '-1' : '0';
        const date = moment(released * 1000).format(DEFAULT_DATE_FORMAT);

        return (
            <li className="music__meta-released u-hide-when-fixed u-hide-when-pin-condensed">
                <span className="u-fw-700">Release Date: </span> {date} by{' '}
                <Link
                    to={`/artist/${uploader.url_slug}`}
                    tabIndex={tabIndex}
                    data-popover
                >
                    {uploader.name}
                </Link>{' '}
                {check}
            </li>
        );
    }

    renderRuntime(item, large) {
        if (item.type !== 'album' || !large || this.props.search) {
            return null;
        }

        return (
            <li className="music__meta-runtime u-hide-when-fixed u-hide-when-pin-condensed">
                <strong className="u-fw-700">Runtime:</strong>{' '}
                {this.getRuntime(item)}
            </li>
        );
    }

    renderTags(item, large) {
        const { pinned, tags } = this.props;

        if (!item.usertags || large || pinned) {
            return null;
        }

        const normalizedKeys = item.usertags.split(',');

        if (!normalizedKeys.length) {
            return null;
        }

        const displayTags = normalizedKeys.reduce((elements, normalizedKey) => {
            const tag = tags[normalizedKey];

            if (!tag) {
                return elements;
            }

            const element = (
                <Link
                    className="music-detail__tag"
                    key={elements.length}
                    to={`/search?q=tag:${encodeURIComponent(tag)}`}
                >
                    #{tag}
                </Link>
            );

            return [...elements, element];
        }, []);

        if (!displayTags.length) {
            return null;
        }

        return <li style={{ marginTop: 10 }}>{displayTags}</li>;
    }

    renderErrorList(item) {
        const { currentUser } = this.props;

        let errors;

        if (
            typeof item.errors !== 'undefined' &&
            item.errors.length > 0 &&
            currentUser.isLoggedIn &&
            currentUser.isAdmin
        ) {
            errors = (
                <span className="error-list">
                    <WarningIcon className="u-text-icon u-margin-0 u-text-red" />
                    {item.errors.map((error, index) => (
                        <span className="error-text u-text-red" key={index}>
                            {' '}
                            {error}{' '}
                        </span>
                    ))}
                </span>
            );
        }

        return errors;
    }

    renderTrackList(item, player, musicIndex, isLarge = false) {
        const { currentSong: currentMusicItem } = player;
        const isCurrentSong = isCurrentMusicItem(currentMusicItem, item);
        const isPlaylist = item.type === 'playlist';
        const isAlbum = item.type === 'album';
        const isTrackItem = isPlaylist || isAlbum;
        const shouldDisplay =
            ((isLarge && isTrackItem) ||
                isCurrentSong ||
                this.props.isTracklistActivated) &&
            item.tracks &&
            item.tracks.length;
        const hideWaveform = isTrackItem;

        if (!shouldDisplay || this.props.pinned) {
            return null;
        }

        if (this.isNotReleased(item)) {
            return null;
        }

        return (
            <div className="row album-tracklist">
                <div className="column small-24">
                    <MusicDetailTrackListing
                        player={player}
                        currentUser={this.props.currentUser}
                        isActive={isCurrentSong}
                        hideWaveform={hideWaveform}
                        musicItem={item}
                        musicIndex={musicIndex}
                        hideLoadMoreTracksButton={
                            this.props.hideLoadMoreTracksButton
                        }
                        onTrackClick={this.props.onItemClick}
                        onLoadMoreTracksButtonClick={
                            this.props.onLoadMoreTracksButtonClick
                        }
                        onTrackActionClick={this.props.onTrackActionClick}
                    />
                </div>
            </div>
        );
    }

    renderReupBy(repost) {
        if (!repost) {
            return null;
        }

        let repostLink = repost;

        if (typeof repost !== 'string' && repost.url_slug) {
            repostLink = (
                <Link to={`/artist/${repost.url_slug}`} data-popover>
                    {repost.name}
                </Link>
            );
        }

        return (
            <p className="music-detail__reup-label u-spacing-bottom-em">
                <RetweetIcon className="u-text-icon" /> Re-upped by {repostLink}
            </p>
        );
    }

    renderSponsored(isFeatured) {
        if (!isFeatured) {
            return null;
        }

        return (
            <p className="music-detail__reup-label u-spacing-bottom-em">
                <StarIcon className="u-text-icon u-text-icon--star" /> Sponsored
                Track
            </p>
        );
    }

    renderLowerContent(item, currentUser, player, isLarge) {
        const { index, hideWaveform, showFullGeoNotice } = this.props;
        const { currentSong, paused, loading, currentTime } = player;
        const { geo_restricted: geoRestricted } = item;

        const isCurrentSong = isCurrentMusicItem(currentSong, item);

        let isPaused = !isCurrentSong || (isCurrentSong && paused);
        const isLoading = isCurrentSong && loading;
        const elapsedDisplay = isCurrentSong ? currentTime || 0 : 0;
        const stillProcessing = item.streaming_url === '' && !geoRestricted;
        const isTrackItem = item.type === 'album' || item.type === 'playlist';

        if ((isTrackItem && isLarge) || hideWaveform) {
            return null;
        }

        if (this.isNotReleased(item)) {
            return null;
        }

        if (item.status === 'unplayable') {
            return (
                <p className="dmca-notice">
                    Streaming for this song has been Disabled at the source.
                    Discover your next favorite song on our{' '}
                    <Link to="/songs/week">Top Songs</Link>,{' '}
                    <Link to="/albums/week">Top Albums</Link> or{' '}
                    <Link to="/playlists/browse">Top Playlists Charts!</Link>
                </p>
            );
        }

        if (item.status === 'takedown' || item.status === 'suspended') {
            return (
                <p className="dmca-notice">
                    This song has been removed due to a DMCA Complaint. Discover
                    your next favorite song on our{' '}
                    <Link to="/songs/week">Top Songs</Link>,{' '}
                    <Link to="/albums/week">Top Albums</Link> or{' '}
                    <Link to="/playlists/browse">Top Playlists Charts!</Link>
                </p>
            );
        }

        const itemDuration =
            item.duration ||
            (item.tracks && item.tracks.length && item.tracks[0].duration);
        const buttonProps = {
            'data-tooltip': stillProcessing
                ? 'This item is still processing'
                : null,
            'data-tooltip-active': stillProcessing ? true : null
        };

        const iconSize = isLarge ? 75 : 40;

        if (isTrackItem && isCurrentSong) {
            const allTracksOfItemQueued = queueHasMusicItem(player.queue, item);

            if (!allTracksOfItemQueued) {
                isPaused = true;
            }
        }

        return (
            <div className="music-detail__waveform-wrap waveform-wrap waveform-wrap--has-button u-hide-when-fixed">
                <span {...buttonProps}>
                    <PlayButton
                        onlySizeLoader
                        className="waveform-wrap__play-button"
                        loading={isLoading || stillProcessing}
                        size={iconSize}
                        paused={isPaused}
                        disabled={geoRestricted}
                        onButtonClick={this.props.onItemClick}
                        buttonProps={{
                            'data-music-index': index
                        }}
                    />
                </span>
                <WaveformContainer
                    musicItem={item}
                    color="rgba(221, 221, 221, 1)"
                />
                <span className="waveform__elapsed waveform__time">
                    {convertSecondsToTimecode(elapsedDisplay)}
                </span>
                <span className="waveform__duration waveform__time">
                    {convertSecondsToTimecode(itemDuration || player.duration)}
                </span>
                {!showFullGeoNotice && this.renderGeoNotice()}
            </div>
        );
    }

    renderHeader(item, removeHeaderLinks) {
        const { title, type, geo_restricted: geoRestricted } = item;
        const { showFullGeoNotice } = this.props;

        const tabIndex = geoRestricted && showFullGeoNotice ? '-1' : '0';
        const uploader = getUploader(item);
        const artist = getArtistName(item);

        let artistText;

        if (type !== 'playlist') {
            artistText = (
                <span className="music__heading--artist u-trunc u-d-block">
                    {artist}
                </span>
            );
        }

        let playlistRuntime;

        if (item.type === 'playlist' && !this.props.search) {
            playlistRuntime = (
                <span className="music__heading--count">
                    <strong>Runtime:</strong> {this.getRuntime(item)}
                </span>
            );
        }

        let header = (
            <Fragment>
                {artistText}{' '}
                <span className="music__heading--title u-trunc u-d-block">
                    {title}
                </span>
                {playlistRuntime}
            </Fragment>
        );

        if (removeHeaderLinks) {
            // If were here, we're probably on the music
            // page where its uncessary to link to the same page. That means we can use h1 instead of h2
            header = <h1 className="music__heading">{header}</h1>;
        } else {
            header = (
                <Link
                    className="music-detail__link"
                    to={`/${item.type}/${uploader.url_slug}/${item.url_slug}`}
                    tabIndex={tabIndex}
                >
                    <h2 className="music__heading">{header}</h2>
                </Link>
            );
        }

        return header;
    }

    renderSocialLinks(item, large) {
        if (yesBool(item.private)) {
            return null;
        }

        const { showFullGeoNotice } = this.props;
        const { geo_restricted: geoRestricted } = item;

        const style = {};
        if (large) {
            style.top = 25;
            style.right = 25;
        }

        return (
            <SocialShare
                item={item}
                iconSize={large ? 22 : 18}
                disableKeyboard={geoRestricted && showFullGeoNotice}
                style={style}
            />
        );
    }

    renderPillButtons(item, player, isLarge) {
        const { currentUser, currentUserPinned, isFixed } = this.props;
        const { geo_restricted: geoRestricted, stats } = item;
        const isCurrentSong = isCurrentMusicItem(player.currentSong, item);
        const ownMusic = ownsMusic(currentUser, item);

        if (!isLarge || geoRestricted) {
            return null;
        }

        if (ownMusic && yesBool(item.private) && item.type !== 'playlist') {
            return (
                <div className="u-spacing-top-em">
                    <button
                        className="button button--pill button--pill-med"
                        onClick={this.props.onPromoLinkClick}
                    >
                        <LinkIcon className="button__icon button__icon--link" />
                        Create Private Link
                    </button>
                </div>
            );
        }

        return (
            <TrackListButtons
                item={item}
                isCurrentSong={isCurrentSong}
                containerElement="li"
                loading={player.loading}
                onActionClick={this.props.onActionClick}
                onPlayClick={this.props.onItemClick}
                onShuffleClick={this.props.onShuffleClick}
                type={item.type}
                className="u-spacing-top-15 u-d-flex"
                stats={stats}
                currentUser={currentUser}
                currentUserPinned={currentUserPinned}
                hideQueueLastAction={
                    player.queueIndex === player.queue.length - 1
                }
                disablePlay={this.isNotReleased(item)}
                isFixed={isFixed}
                showSocial
            />
        );
    }

    renderMetadata() {
        const { item, large, isVerified } = this.props;
        const featuring = getFeaturing(item);
        const uploader = getUploader(item);
        const check = <Verified user={uploader} small />;

        if (item.type === 'playlist') {
            const playlistArtistUrl = `/artist/${item.artist.url_slug}`;
            const lastUpdated = moment(item.updated * 1000).format(
                DEFAULT_DATE_FORMAT
            );

            let runtime;

            if (!this.props.search) {
                runtime = (
                    <li className="u-hide-when-fixed">
                        <strong>Runtime:</strong> {this.getRuntime(item)}
                    </li>
                );
            }

            let trackCount;

            if (isVerified) {
                trackCount = (
                    <li className="u-hide-when-fixed">
                        <strong>Number of songs:</strong> {item.track_count}
                    </li>
                );
            }

            return (
                <Fragment>
                    {runtime}
                    {trackCount}
                    <li className="u-hide-when-fixed">
                        <strong>Playlist Creator:</strong>{' '}
                        <Link to={playlistArtistUrl} data-popover>
                            {item.artist.name}
                        </Link>{' '}
                        {check}
                    </li>
                    <li className="u-hide-when-fixed">
                        <strong>Last updated:</strong> {lastUpdated}
                    </li>
                </Fragment>
            );
        }

        return (
            <Fragment>
                {this.renderFeaturing(featuring)}
                {this.renderAlbum(item, large)}
                {this.renderReleased(item, check, large)}
                {/* this.renderRuntime(item, large) */}
                {this.renderTags(item, large)}
                {this.renderErrorList(item)}
            </Fragment>
        );
    }

    renderArtworkPlayButton() {
        const { item, index, showArtworkPlayButton, player } = this.props;
        const { currentSong, paused, loading } = player;
        const { geo_restricted: geoRestricted } = item;

        const isCurrentSong = isCurrentMusicItem(currentSong, item);

        let isPaused = !isCurrentSong || (isCurrentSong && paused);
        const isLoading = isCurrentSong && loading;
        const stillProcessing = item.streaming_url === '';
        const isTrackItem = item.type === 'album' || item.type === 'playlist';

        if (isTrackItem && isCurrentSong) {
            const allTracksOfItemQueued = queueHasMusicItem(player.queue, item);

            if (!allTracksOfItemQueued) {
                isPaused = true;
            }
        }

        if (!showArtworkPlayButton || geoRestricted) {
            return null;
        }

        return (
            <PlayButton
                onlySizeLoader
                className="music-artwork__play-button"
                loading={isLoading || stillProcessing}
                size={40}
                paused={isPaused}
                onButtonClick={this.props.onItemClick}
                buttonProps={{
                    'data-music-index': index
                }}
            />
        );
    }

    renderArtwork() {
        const {
            item,
            large,
            ranking,
            shouldLinkArtwork,
            pinned,
            showArtworkPlayButton,
            shouldLazyLoadArtwork
        } = this.props;
        const uploader = getUploader(item);
        let imageSize = 165;

        if (pinned) {
            imageSize = 240;
        }

        if (large) {
            imageSize = 280;
        }

        const artwork = buildDynamicImage(item.image_base || item.image, {
            width: imageSize,
            height: imageSize,
            max: true
        });

        const retinaArtwork = buildDynamicImage(item.image_base || item.image, {
            width: imageSize * 2,
            height: imageSize * 2,
            max: true
        });
        let srcSet;

        if (retinaArtwork) {
            srcSet = `${retinaArtwork} 2x`;
        }
        const artworkKlass = classnames('music-artwork music-detail__image', {
            'u-album-stack music-artwork--album':
                (item.type === 'album' || item.type === 'playlist') && !large,
            'avatar-container--overflow': showArtworkPlayButton
        });

        let rankingNumber;

        if (typeof ranking === 'number') {
            rankingNumber = (
                <span
                    className="artwork-play-button__rank"
                    role="presentation"
                    data-testid="ranking"
                >
                    {ranking}
                </span>
            );
        }

        if (shouldLinkArtwork && !this.props.showArtworkPlayButton) {
            const { geo_restricted: geoRestricted } = item;
            const { showFullGeoNotice } = this.props;
            const tabIndex = geoRestricted && showFullGeoNotice ? '-1' : '0';

            return (
                <Link
                    to={`/${item.type}/${uploader.url_slug}/${item.url_slug}`}
                    className={artworkKlass}
                    aria-label={`Go to ${item.type}`}
                    tabIndex={tabIndex}
                >
                    {rankingNumber}
                    <img
                        src={artwork}
                        srcSet={srcSet}
                        loading={shouldLazyLoadArtwork ? 'lazy' : 'eager'}
                        alt=""
                    />
                </Link>
            );
        }

        return (
            <Avatar
                image={item.image_base || item.image}
                type={item.type}
                imageSize={imageSize}
                zoomable={large && !showArtworkPlayButton}
                zoomImage={
                    showArtworkPlayButton ? null : item.image_base || item.image
                }
                className={artworkKlass}
                rounded={false}
                square={showArtworkPlayButton}
                lazyLoad={shouldLazyLoadArtwork}
                children={this.renderArtworkPlayButton()}
            />
        );
    }

    renderInteractions() {
        const {
            item,
            index,
            currentUser,
            currentUserPinned,
            hideInteractions,
            showFullGeoNotice
        } = this.props;

        const { geo_restricted: geoRestricted, stats } = item;

        if (!hideInteractions && this.shouldShowInteractions(item)) {
            return (
                <Interactions
                    currentUser={currentUser}
                    currentUserPinned={currentUserPinned}
                    className="u-hide-when-fixed"
                    item={item}
                    stats={stats}
                    musicIndex={index}
                    large={this.props.large}
                    context={this.props.context}
                    onActionClick={this.props.onActionClick}
                    nativeShareSupported={this.props.nativeShareSupported}
                    disableKeyboard={geoRestricted && showFullGeoNotice}
                />
            );
        }

        return null;
    }

    renderPinMenu() {
        const {
            tooltipActive,
            onTooltipClick,
            currentUser,
            showPinMenu,
            currentUserPinned
        } = this.props;

        if (
            !currentUser.isLoggedIn ||
            !showPinMenu ||
            currentUserPinned.list.length < 2
        ) {
            return null;
        }

        const tooltipClass = classnames(
            'tooltip sub-menu pinned-item__sub-menu',
            {
                'tooltip--active': tooltipActive
            }
        );

        return (
            <div className="pinned-item__menu">
                <button
                    className="pinned-item__menu-trigger"
                    onClick={onTooltipClick}
                    ref={(e) => {
                        this._tooltipButton = e;
                    }}
                >
                    <ThreeDotsIcon />
                </button>
                <BodyClickListener
                    shouldListen={tooltipActive}
                    onClick={onTooltipClick}
                    ignoreDomElement={this._tooltipButton}
                />
                <ul className={tooltipClass}>
                    <li className="sub-menu__item">
                        <button
                            title="Remove Highlight"
                            aria-label="Remove Highlight"
                            data-action="unpin"
                            onClick={this.props.onActionClick}
                        >
                            Remove Highlight
                        </button>
                    </li>
                </ul>
            </div>
        );
    }

    renderGeoNotice(withBackdrop = false) {
        const { item, large } = this.props;

        if (!item) {
            return null;
        }

        const { geo_restricted: geoRestricted } = item;
        const isPlaylist = item.type === 'playlist';
        const isAlbum = item.type === 'album';
        const isTrackItem = isPlaylist || isAlbum;

        if (!geoRestricted || (isTrackItem && large)) {
            return null;
        }

        return (
            <GeoNotice withBackdrop={withBackdrop}>
                The rightsholder has not made this content available in your
                country.
            </GeoNotice>
        );
    }

    render() {
        const {
            item,
            player,
            index,
            large,
            feed,
            search,
            related,
            profile,
            className,
            currentUser,
            removeHeaderLinks,
            fixOnScrollProps,
            shouldFixOnScroll,
            isFixed,
            containerClassName,
            isVerified,
            pinned,
            shouldShowReup,
            showFullGeoNotice
        } = this.props;

        if (!item) {
            // @todo maybe put a skeleton view of the music detail item here?
            return null;
        }

        const reupText = this.renderReupBy(
            shouldShowReup ? item.repost_artist || item.repost : null
        );
        const klass = classnames(
            `music-detail u-clearfix music-detail--${item.type}`,
            {
                'music-detail--large': large,
                'music-detail--related': related,
                'music-detail--feed': feed,
                'music-detail--search': search,
                'music-detail--profile': profile,
                'music-detail--song-large': large && item.type === 'song',
                'music-detail--album-large': large && item.type === 'album',
                'music-detail--upcoming': item.released_offset > 0,
                'music-detail--fixed': isFixed,
                [className]: className
            }
        );

        let featuredText;

        if (this.renderSponsored(item.isFeatured)) {
            featuredText = this.renderSponsored(item.isFeatured);
        } else if (isVerified) {
            featuredText = (
                <p className="verified-label">Verified {item.type}</p>
            );
        }

        let lowerContent = this.renderLowerContent(
            item,
            currentUser,
            player,
            large
        );

        if (lowerContent) {
            lowerContent = (
                <div className="row">
                    <div className="column small-24">{lowerContent}</div>
                </div>
            );
        }

        let countdown;

        if (this.isNotReleased(item) && !ownsMusic(currentUser, item)) {
            countdown = (
                <div className="row">
                    <div className="column">
                        <Countdown
                            seconds={item.released_offset}
                            type={item.type}
                            onFinish={this.props.onCountdownFinish}
                            containerElement="div"
                            className="countdown countdown--dark u-box-shadow u-spacing-top-em"
                        />
                    </div>
                </div>
            );
        }

        const innerClass = classnames(
            'music-detail__top-left column small-24 medium-flex small-text-center medium-text-left',
            {
                'medium-20': large
            }
        );

        let innerContent = (
            <div className="music-detail__inner">
                <div className="music-detail__top u-clearfix row">
                    <div className={innerClass}>
                        {this.renderArtwork()}
                        <div className="music-detail__content">
                            {reupText}
                            {featuredText}
                            {this.renderHeader(item, removeHeaderLinks)}
                            <ul className="music__meta">
                                {this.renderMetadata()}
                                {this.renderPillButtons(item, player, large)}
                            </ul>
                        </div>
                    </div>
                    <div className="column small-24 medium-4 music-detail__top-right u-hide-when-fixed">
                        {this.renderSocialLinks(item, large)}
                        {this.renderPinMenu()}
                    </div>
                </div>
                {lowerContent}
                {this.renderInteractions()}
            </div>
        );

        if (shouldFixOnScroll) {
            innerContent = (
                <FixOnScroll {...fixOnScrollProps}>{innerContent}</FixOnScroll>
            );
        }

        const containerClass = classnames('music-detail-container', {
            [containerClassName]: containerClassName,
            'u-bg-white': !pinned
        });

        return (
            <div className={containerClass}>
                <div className={klass} data-testid="musicDetail">
                    {innerContent}
                    {countdown}
                </div>
                {this.renderTrackList(item, player, index, large)}
                {showFullGeoNotice && this.renderGeoNotice(true)}
            </div>
        );
    }
}
