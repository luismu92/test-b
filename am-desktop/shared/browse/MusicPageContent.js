import React from 'react';
import PropTypes from 'prop-types';

import RecentTracksAlbums from '../components/RecentTracksAlbums';

import styles from './MusicPageContent.module.scss';

export default function MusicPageContent({
    children,
    style,
    artist,
    artistUploads,
    item
}) {
    return (
        <div className={styles.container} style={style || {}}>
            <div className={styles.inner}>
                <div className={styles.grid}>
                    <div className={styles.content}>{children}</div>
                    <div className={styles.sidebar}>
                        <RecentTracksAlbums
                            uploader={artist}
                            uploads={artistUploads}
                            filterIds={[item.id]}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

MusicPageContent.propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    style: PropTypes.object,
    artist: PropTypes.object,
    artistUploads: PropTypes.object,
    item: PropTypes.object
};
