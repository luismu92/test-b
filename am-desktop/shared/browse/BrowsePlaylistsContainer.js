import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import connectDataFetchers from 'lib/connectDataFetchers';
import { connect } from 'react-redux';

import {
    COLLECTION_TYPE_PLAYLIST,
    PLAYLIST_TYPE_BROWSE
} from 'constants/index';
import BrowsePlaylistMeta from 'components/BrowsePlaylistMeta';

import AndroidLoader from '../loaders/AndroidLoader';
import BrowseEmptyState from './BrowseEmptyState';
import BrowsePlaylistSection from './BrowsePlaylistSection';
import BrowsePlaylistsSidebar from './BrowsePlaylistsSidebar';
import LandingHeader from '../components/LandingHeader';

import FeedBar from '../widgets/FeedBar';
import PlaylistIcon from '../icons/playlist';

import {
    clearList,
    nextPage,
    fetchTaggedPlaylists,
    fetchAllPlaylistTags
} from '../redux/modules/music/browsePlaylist';

// Default to Verified series tag until we have "most popular" endpoint setup
const DEFAULT_PLAYLIST_SLUG = 'verified-series';

class BrowsePlaylistsContainer extends Component {
    static propTypes = {
        musicBrowsePlaylist: PropTypes.object,
        match: PropTypes.object,
        dispatch: PropTypes.func
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(clearList());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoadMoreClick = (e) => {
        const { dispatch, musicBrowsePlaylist } = this.props;
        const button = e.currentTarget;
        const tag = musicBrowsePlaylist.tag;
        const page = parseInt(button.getAttribute('data-page'), 10) || 1;
        const limit = 20;

        dispatch(nextPage());
        dispatch(fetchTaggedPlaylists(tag, limit, page + 1));
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    refetchIfNecessary(currentProps, nextProps) {
        const { dispatch } = this.props;
        const nextTag = nextProps.match.params.urlSlug;
        const changedPage = currentProps.match.params.urlSlug !== nextTag;

        if (changedPage) {
            let newTag = nextTag;

            // If you go from a tag page, back to /playlists/browse it kills the page.
            // Set some fallbacks here to avoid that
            if (!nextTag) {
                newTag = DEFAULT_PLAYLIST_SLUG;
            }

            dispatch(clearList());
            dispatch(fetchTaggedPlaylists(newTag, 20));
        }
    }

    renderFeed(results) {
        const feedbarTitle = (
            <h2 className="feed-bar__title">
                <PlaylistIcon className="feed-bar__title-icon feed-bar__title-icon--playlist u-text-icon" />
                <strong>{results.title}</strong> Playlists
            </h2>
        );

        let loadMoreButton;

        if (!results.onLastPage && results.list.length) {
            loadMoreButton = (
                <div className="u-text-center">
                    <button
                        className="button button--pill button--med"
                        onClick={this.handleLoadMoreClick}
                        data-page={results.page}
                    >
                        More {results.title} Playlists
                    </button>
                </div>
            );
        }

        return (
            <Fragment>
                <div className="row">
                    <div className="column small-24">
                        <FeedBar
                            className="feed-bar--playlist"
                            title={feedbarTitle}
                            padded
                        />
                    </div>
                </div>
                <BrowsePlaylistSection item={results} />
                {loadMoreButton}
            </Fragment>
        );
    }

    render() {
        const { musicBrowsePlaylist } = this.props;
        const { loading } = musicBrowsePlaylist;

        let loader;
        let emptyState;

        if (loading) {
            loader = <AndroidLoader className="music-feed__loader" />;
        }

        const isEmpty =
            !loading &&
            (musicBrowsePlaylist.list && !musicBrowsePlaylist.list.length);

        if (isEmpty) {
            emptyState = (
                <BrowseEmptyState
                    activeContext={COLLECTION_TYPE_PLAYLIST}
                    activePlaylistContext={PLAYLIST_TYPE_BROWSE}
                />
            );
        }

        return (
            <div className="music-feed">
                <BrowsePlaylistMeta
                    urlSlug={
                        this.props.match.params.urlSlug || DEFAULT_PLAYLIST_SLUG
                    }
                    category={musicBrowsePlaylist.title}
                />
                <div className="row">
                    <div className="column small-24 u-padding-0">
                        <LandingHeader type="playlist" />
                    </div>
                </div>
                <div className="row u-spacing-top-n-20">
                    <div className="column small-24 medium-5 u-padding-right-0">
                        <BrowsePlaylistsSidebar
                            musicBrowsePlaylist={musicBrowsePlaylist}
                            defaultSlug={DEFAULT_PLAYLIST_SLUG}
                        />
                    </div>
                    <div className="column small-24 medium-19 u-pos-relative">
                        {this.renderFeed(musicBrowsePlaylist)}
                        {emptyState}
                        {loader}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        ad: state.ad,
        currentUser: state.currentUser,
        musicBrowsePlaylist: state.musicBrowsePlaylist,
        player: state.player
    };
}

export default connect(mapStateToProps)(
    connectDataFetchers(BrowsePlaylistsContainer, [
        (params) =>
            fetchTaggedPlaylists(params.urlSlug || DEFAULT_PLAYLIST_SLUG, 20),
        () => fetchAllPlaylistTags()
    ])
);
