import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import MusicIcon from '../icons/music-new';
import PlaylistIcon from '../icons/playlist';
import WarningIcon from '../icons/warning';

export default class ToastNotification extends Component {
    static propTypes = {
        state: PropTypes.object,
        currentUser: PropTypes.object
    };

    handleToastClick() {
        console.log('test');
    }

    renderToastIconForType(type) {
        let Icon;

        switch (type) {
            case 'song':
                Icon = MusicIcon;
                break;

            case 'playlist':
                Icon = PlaylistIcon;
                break;

            case 'warning':
                Icon = WarningIcon;
                break;

            default:
                return null;
        }

        return <Icon className="u-text-icon" />;
    }

    renderTextForAction(action, item, data = {}) {
        const itemText = (
            <span className="toast-notification__item">{`"${item}"`}</span>
        );

        switch (action) {
            case 'favorite':
                return <span>You favorited {itemText}</span>;

            case 'unfavorite':
                return <span>You removed {itemText} from your favorites</span>;

            case 'pin':
                return (
                    <span>
                        {itemText}{' '}
                        <span className="u-text-gray9 u-fw-500">
                            was highlighted on your profile
                        </span>{' '}
                        <Link
                            to={`/artist/${
                                this.props.currentUser.profile.url_slug
                            }`}
                        >
                            View Profile
                        </Link>
                    </span>
                );

            case 'unpin':
                return <span>You removed {itemText} from your highlights</span>;

            case 'reup':
                return (
                    <span>
                        {itemText} was re-upped and shared with your followers
                    </span>
                );

            case 'unreup':
                return <span>You removed your re-up</span>;

            case 'add-to-playlist':
                return (
                    <span>
                        {itemText} was added to{' '}
                        <span className="u-underline">
                            your playlist{data.plural ? 's' : ''}
                        </span>
                    </span>
                );

            case 'delete-from-playlist':
                return (
                    <span>
                        {itemText} was removed from{' '}
                        <span className="u-underline">
                            your playlist{data.plural ? 's' : ''}
                        </span>
                    </span>
                );

            case 'create-new-playlist':
                return <span>A new playlist was created with {itemText}</span>;

            case 'suspend':
                return <span>You suspended {itemText}</span>;

            case 'un-suspend':
                return <span>You un-suspended {itemText}</span>;

            case 'takedown':
                return <span>You took down {itemText}</span>;

            case 'restored':
                return <span>You restored {itemText}</span>;

            case 'censor':
                return <span>You censored {itemText}</span>;

            case 'delete':
                return <span>You deleted {itemText}</span>;

            case 'public':
                return <span>{itemText}</span>;

            case 'message':
                return item;

            case 'trend':
                return (
                    <span>
                        You trended {itemText} in {data.genre}
                    </span>
                );

            case 'feature':
                return <span>You featured {itemText}</span>;

            case 'clear-stats':
                return <span>You cleared stats for {itemText}</span>;

            case 'exclude-stats':
                return <span>You exluded stats for {itemText}</span>;

            case 'block':
                return <span>You blocked {itemText}</span>;

            case 'unblock':
                return <span>You un-blocked {itemText}</span>;

            case 'followlistadd':
                return <span>You added {itemText} to the follow list</span>;

            case 'followlistremove':
                return <span>You removed {itemText} from the follow list</span>;

            default:
                throw new Error(`action: ${action} not accounted for`);
        }
    }

    renderToastNotifications(notification = {}) {
        // Slice as to not mutate the array
        return notification.toasts.map((toast, i) => {
            const durationSeconds = toast.duration / 1000;

            const style = {
                transform: `translate(-50%, ${i * 60 + 20}px)`,
                animation: `toasted ${durationSeconds}s forwards ease-in`
            };

            let text = toast.message;

            if (toast.action && toast.item) {
                text = this.renderTextForAction(
                    toast.action,
                    toast.item,
                    toast.data
                );
            }

            if (toast.link) {
                text = <Link to={toast.link}>{text}</Link>;
                style.pointerEvents = 'auto';
            }

            return (
                /* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions, jsx-a11y/click-events-have-key-events */
                <span
                    className="toast-notification"
                    key={toast.id}
                    style={style}
                    role="alert"
                    onClick={this.handleToastClick}
                >
                    <span className="toast-notification__icon">
                        {this.renderToastIconForType(toast.type)}
                    </span>{' '}
                    {text}
                </span>
            );
        });
    }

    render() {
        return (
            <Fragment>
                {this.renderToastNotifications(this.props.state)}
            </Fragment>
        );
    }
}
