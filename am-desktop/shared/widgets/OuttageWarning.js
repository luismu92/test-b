import React, { Component } from 'react';

class OuttageWarning extends Component {
    //////////////////////
    // Lifecyle methods //
    //////////////////////

    componentDidMount() {
        document.body.classList.add('is-masquerading');
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        return (
            <div className="masquerade-warning">
                <p>
                    Audiomack is currently offline as we upgrade our server
                    capacity and will be back as soon as possible. Please check
                    back soon and we're sorry for the disruption.
                </p>
            </div>
        );
    }
}

export default OuttageWarning;
