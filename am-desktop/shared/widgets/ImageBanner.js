import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { clamp, passiveOption, getDominantColors } from 'utils/index';

import SettingsIcon from '../icons/cog';

const BANNER_BLUR_THRESHOLD = 200;

export default class ImageBanner extends Component {
    static propTypes = {
        banner: PropTypes.string,
        editLink: PropTypes.string,
        disableBlurOnScroll: PropTypes.bool
    };

    constructor(props) {
        super(props);

        this.state = {
            bannerLoaded: false,
            gradientColors: null
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const { banner } = this.props;
        const option = passiveOption();

        this._mounted = true;

        window.addEventListener('scroll', this.handleWindowScroll, option);

        this.loadBannerGradient(banner);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.banner !== this.props.banner) {
            this.loadBannerGradient(this.props.banner);
        }
    }

    componentWillUnmount() {
        this._mounted = false;
        window.removeEventListener('scroll', this.handleWindowScroll, false);
        this._blurredBg = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowScroll = () => {
        const { disableBlurOnScroll } = this.props;
        if (!this._blurredBg) {
            return;
        }

        if (
            window.pageYOffset <= BANNER_BLUR_THRESHOLD &&
            !disableBlurOnScroll
        ) {
            this._blurredBg.style.opacity = clamp(
                window.pageYOffset / BANNER_BLUR_THRESHOLD,
                0,
                1
            );
        }
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    loadBannerGradient(image) {
        getDominantColors(image)
            .then((colors) => {
                if (!this._mounted) {
                    return;
                }
                this.setState({
                    bannerLoaded: true,
                    gradientColors: colors.length ? colors : null
                });
                return;
            })
            .catch((err) => {
                if (!this._mounted) {
                    return;
                }
                console.log(err);
                this.setState({
                    bannerLoaded: true
                });
            });
    }

    render() {
        const { editLink, banner } = this.props;

        if (!banner) {
            return null;
        }

        const bgImg = `url(${banner})`;
        const bgStyle = {
            backgroundImage: bgImg
        };

        const blurBgStyle = {
            ...bgStyle,
            opacity: 0
        };
        const blurredBg = (
            <div
                className="profile-banner__bg-blur"
                style={blurBgStyle}
                ref={(c) => {
                    this._blurredBg = c;
                }}
            />
        );
        const bannerClass = classnames('profile-banner', {
            'profile-banner--active': this.state.bannerLoaded
        });

        let editButton;

        if (editLink) {
            editButton = (
                <div className="profile-banner__inner">
                    <Link
                        className="button button--pill profile-banner__button"
                        to={editLink}
                    >
                        <SettingsIcon className="u-text-icon" />
                        Edit Image
                    </Link>
                </div>
            );
        }

        return (
            <header className={bannerClass}>
                <div className="profile-banner__bg" style={bgStyle} />
                {blurredBg}
                {editButton}
            </header>
        );
    }
}
