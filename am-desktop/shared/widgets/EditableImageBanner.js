import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cropper from 'cropperjs';
import classnames from 'classnames';

import { previewFile, smoothScroll } from 'utils/index';

import CameraIcon from '../icons/camera';

const BANNER_WIDTH = 1500;
const BANNER_HEIGHT = 500;

export default class EditableImageBanner extends Component {
    static propTypes = {
        banner: PropTypes.string,
        isEditing: PropTypes.bool,
        isSaving: PropTypes.bool,
        onRemove: PropTypes.func,
        onCancel: PropTypes.func,
        onChange: PropTypes.func,
        onApply: PropTypes.func
    };

    static defaultProps = {
        onApply() {},
        onCancel() {},
        onRemove() {},
        onChange() {},
        isEditing: false,
        isSaving: false
    };

    constructor(props) {
        super(props);

        this.state = {
            isEditing: props.isEditing,
            isSaving: props.isSaving,
            bannerZoom: 1
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentWillUnmount() {
        this.resetCropper();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBannerImageZoom = (e) => {
        const scale = parseFloat(e.currentTarget.value);

        this.setState({
            bannerZoom: scale
        });

        this._cropper.scale(scale);
    };

    handleBannerEditCancel = (e) => {
        e.preventDefault();

        this.resetCropper();

        const image = this._bannerImage;

        if (image && this._originalBannerSrc) {
            image.src = this._originalBannerSrc;

            this._originalBannerSrc = null;
        }

        Promise.resolve()
            .then(() => {
                return this.props.onCancel();
            })
            .then(() => {
                this.setState({
                    isEditing: false
                });
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    };

    handleBannerEditApply = (e) => {
        e.preventDefault();

        const canvas = this._cropper.getCroppedCanvas({
            width: BANNER_WIDTH,
            height: BANNER_HEIGHT
        });
        const result = canvas.toDataURL('image/jpeg');

        this.setState({
            isSaving: true
        });

        Promise.resolve()
            .then(() => {
                return this.props.onApply(result);
            })
            .then(() => {
                this.setState({
                    isSaving: false,
                    isEditing: false
                });
                return this.resetCropper();
            })
            .catch((err) => {
                this.setState({
                    isSaving: false
                });
                console.log(err);
            });
    };

    handleBannerImageChange = (e) => {
        previewFile(e.target.files[0])
            .then((result) => {
                this.resetCropper();

                // Scroll to the top so user is seeing all of the banner area
                smoothScroll(0);

                const image = this._bannerImage;

                this._originalBannerSrc = image.src;
                image.src = result;

                this._cropper = new Cropper(image, {
                    viewMode: 1,
                    dragMode: 'move',
                    background: false,
                    aspectRatio: BANNER_WIDTH / BANNER_HEIGHT,
                    autoCropArea: 1,
                    guides: false,
                    highlight: false,
                    cropBoxMovable: false,
                    cropBoxResizable: false,
                    toggleDragModeOnDblclick: false,
                    autoCrop: true
                });

                return this.props.onChange(result);
            })
            .then(() => {
                this.setState({
                    isEditing: true
                });
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    resetCropper() {
        if (this._cropper) {
            this._cropper.destroy();
        }

        this.setState({
            bannerZoom: 0
        });
    }

    render() {
        const { banner, onRemove } = this.props;
        const { isSaving, isEditing } = this.state;

        const image =
            banner || '/static/images/desktop/profile-banner-placeholder.jpg';
        const titleText = banner
            ? 'Edit your banner image'
            : 'Add a banner image';
        const buttonText = banner ? 'Edit banner' : 'Add banner';
        const bannerClass = classnames(
            'profile-banner profile-banner--active profile-banner--edit',
            {
                'profile-banner--isEditing': isEditing
            }
        );
        const saveText = isSaving ? 'Saving…' : 'Save';
        let resetButton;

        if (banner) {
            resetButton = (
                <button className="button button--link" onClick={onRemove}>
                    Remove banner
                </button>
            );
        }

        let controls;

        if (isEditing) {
            controls = (
                <div className="profile-banner__controls">
                    <div className="profile-banner__slider">
                        <span>1x</span>
                        <input
                            type="range"
                            value={this.state.bannerZoom}
                            min="1"
                            max="2"
                            step="0.1"
                            onChange={this.handleBannerImageZoom}
                        />
                        <span>2x</span>
                    </div>
                    <div className="profile-banner__actions">
                        <button
                            className="button button--link"
                            onClick={this.handleBannerEditCancel}
                            type="button"
                        >
                            Cancel
                        </button>
                        <button
                            className="button button--pill"
                            onClick={this.handleBannerEditApply}
                            type="button"
                            disabled={isSaving}
                        >
                            {saveText}
                        </button>
                    </div>
                </div>
            );
        } else {
            controls = (
                <div className="profile-banner__specs">
                    <CameraIcon className="header-image" />
                    <p>
                        <strong>{titleText}</strong>
                    </p>
                    <p>
                        Recommended image size is {BANNER_WIDTH}x{BANNER_HEIGHT}
                        , JPG or PNG
                    </p>
                    <div
                        className="button button--pill button--file"
                        role="button"
                    >
                        <input
                            type="file"
                            name="image_banner"
                            accept="image/*"
                            onChange={this.handleBannerImageChange}
                        />
                        <span className="u-text-icon u-text-icon--camera">
                            <CameraIcon />
                        </span>
                        {buttonText}
                    </div>
                    {resetButton}
                </div>
            );
        }

        return (
            <div className={bannerClass}>
                <img
                    className="profile-banner__default"
                    src={image}
                    alt=""
                    ref={(c) => {
                        this._bannerImage = c;
                    }}
                />
                {controls}
            </div>
        );
    }
}
