import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
    MESSAGE_TYPE_OFFLINE,
    MESSAGE_TYPE_ROUTE_ERROR
} from '../redux/modules/message';

export default class Messages extends Component {
    static propTypes = {
        messages: PropTypes.array
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleRefreshPage = () => {
        window.location.reload();
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    renderMessage({ type, data }) {
        switch (type) {
            case MESSAGE_TYPE_OFFLINE: {
                let link;

                if (data.linkToGoTo) {
                    link = (
                        <span>
                            <Link to={data.linkToGoTo}>Try again</Link>.
                        </span>
                    );
                }

                return <span>Your connection has been lost. {link}</span>;
            }

            case MESSAGE_TYPE_ROUTE_ERROR:
                return (
                    <span>
                        Audiomack has been updated. Please{' '}
                        <button
                            className="button-link"
                            onClick={this.handleRefreshPage}
                        >
                            click here
                        </button>{' '}
                        to refresh the page.
                    </span>
                );

            default:
                throw new Error(`message type: ${type} not accounted for`);
        }
    }

    renderMessages(messages = []) {
        return Array.from(messages)
            .reverse()
            .map((message, i) => {
                const style = {
                    transform: `translate(0, ${i * 60 + 20}px)`
                };

                return (
                    <span
                        className="persistent-message"
                        key={i}
                        style={style}
                        role="alert"
                    >
                        {this.renderMessage(message)}
                    </span>
                );
            });
    }

    render() {
        return <Fragment>{this.renderMessages(this.props.messages)}</Fragment>;
    }
}
