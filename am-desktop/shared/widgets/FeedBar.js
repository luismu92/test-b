import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AriaMenuButton from 'react-aria-menubutton';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

export default class FeedBar extends Component {
    static propTypes = {
        items: PropTypes.array,
        title: PropTypes.element,
        id: PropTypes.string,
        className: PropTypes.string,
        contextLabel: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.element
        ]),
        hideFilter: PropTypes.bool,
        timespanLabel: PropTypes.string,
        onContextSwitch: PropTypes.func,
        onTimespanSwitch: PropTypes.func,
        activeMarker: PropTypes.string,
        options: PropTypes.array,
        padded: PropTypes.bool,
        blog: PropTypes.bool,
        underline: PropTypes.bool,
        showBorderMarker: PropTypes.bool,
        contextDropDown: PropTypes.bool,
        forComments: PropTypes.bool
    };

    static defaultProps = {
        onContextSwitch() {},
        options: [],
        underline: false,
        padded: false,
        showBorderMarker: false,
        contextDropDown: false,
        hideFilter: false,
        contextLabel: 'Genre:',
        timespanLabel: 'Period:'
    };

    constructor(props) {
        super(props);

        this.state = {
            markerX: null,
            markerWidth: null,
            isListOverflowing: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        // Shitty hack. This can be better
        setTimeout(() => {
            window.requestAnimationFrame(() => {
                this.measureList();

                if (typeof this.props.activeMarker !== 'undefined') {
                    window.requestAnimationFrame(() => {
                        this.moveMarker(this.props.activeMarker);
                    });
                }
            });
        }, 1000);

        window.addEventListener('resize', this.handleWindowResize, false);
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.activeMarker !== this.props.activeMarker ||
            (prevProps.items !== this.props.items && this.state.markerWidth)
        ) {
            this.moveMarker(this.props.activeMarker);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleContextSwitch = (e) => {
        const link = e.currentTarget;
        const value = link.getAttribute('data-value');

        this.props.onContextSwitch(value, this.props.id, e);
    };

    handleGenreSwitch = (value, e) => {
        this.props.onContextSwitch(value, this.props.id, e);
    };

    handleTimespanSwitch = (value, e) => {
        this.props.onTimespanSwitch(value, this.props.id, e);
    };

    handleWindowResize = () => {
        this.measureList();

        this.moveMarker(this.props.activeMarker);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    measureList() {
        if (!this._contextList) {
            return;
        }

        const listWidth = this._contextList.clientWidth;
        const childrenWidth = Array.from(this._contextList.children).reduce(
            (width, child) => {
                return width + child.clientWidth;
            },
            0
        );

        this.setState({
            isListOverflowing: childrenWidth > listWidth
        });
    }

    moveMarker(destinationMarker) {
        const { markerWidth, markerX } = this.state;

        if (!this._contextList) {
            return;
        }

        const listItems = Array.from(
            findDOMNode(this._contextList).querySelectorAll('li')
        );
        const found = listItems.some((li) => {
            const activeLink =
                li.children[0].getAttribute('data-value') === destinationMarker;

            if (activeLink) {
                const w = li.clientWidth;
                const tx = li.offsetLeft;

                if (markerWidth !== w || tx !== markerX) {
                    this.setState({
                        markerWidth: w,
                        markerX: tx
                    });
                }

                return true;
            }

            return false;
        });

        if (!found) {
            // Here we center the marker wherever you left off so it grows out from
            // the center if you go back to that page
            this.setState({
                markerWidth: 0,
                markerX: markerX + markerWidth / 2
            });
        }
    }

    renderDropDownLabel(label = '') {
        if (label !== '') {
            return <label className="menu-button__label">{label}</label>;
        }

        return null;
    }

    renderItems(items = []) {
        const lis = items.map((item, i) => {
            const klass = classnames('feed-bar__list-item', {
                'feed-bar__list-item--active': item.active
            });

            let count;

            if (typeof item.count !== 'undefined') {
                count = (
                    <span className="u-brand-color">
                        ({(item.count || 0).toLocaleString()})
                    </span>
                );
            }

            const listItemProps = item.listItemProps || {};
            const buttonProps = {
                onClick: this.handleContextSwitch,
                ...(item.buttonProps || {}),
                'data-value': item.value
            };
            let button;

            if (item.href) {
                button = (
                    <Link to={item.href} {...buttonProps}>
                        {item.text} {count}
                    </Link>
                );
            } else {
                button = (
                    <button type="button" {...buttonProps}>
                        {item.text} {count}
                    </button>
                );
            }

            return (
                <li className={klass} key={i} {...listItemProps}>
                    {button}
                </li>
            );
        });

        if (!lis.length) {
            return null;
        }

        const klass = classnames('feed-bar__list', {
            'feed-bar__list--overflow': this.state.isListOverflowing
        });

        return (
            <ul className={klass} ref={(c) => (this._contextList = c)}>
                {lis}
            </ul>
        );
    }

    renderGenreDropdown(options = [], label, forComments) {
        if (!options.length) {
            return null;
        }

        let count;

        if (typeof options.count !== 'undefined') {
            count = <span className="u-brand-color">({options.count})</span>;
        }

        let selected = null;

        const lis = options.map((item, i) => {
            if (item.active) {
                selected = item.text;
            }

            const klass = classnames('menu-button__list-item', {
                'menu-button__list-item--active': item.active
            });

            let button;

            if (item.href) {
                button = (
                    <Link to={item.href}>
                        {item.text} {count}
                    </Link>
                );
            } else {
                button = (
                    <button type="button">
                        {item.text} {count}
                    </button>
                );
            }

            return (
                <AriaMenuButton.MenuItem
                    data-value={item.value}
                    className={klass}
                    value={item.value}
                    tag="li"
                    key={i}
                >
                    {button}
                </AriaMenuButton.MenuItem>
            );
        });

        const genreMenu = (
            <AriaMenuButton.Menu
                tag="ul"
                className="tooltip sub-menu sub-menu--condensed tooltip--radius tooltip--right-arrow tooltip--active feed-bar__tooltip"
            >
                {lis}
            </AriaMenuButton.Menu>
        );

        const buttonClass = classnames('menu-button__button button', {
            'button--pill': !forComments,
            'button--link menu-button__button--comments': forComments
        });

        return (
            <div className="menu-button">
                {this.renderDropDownLabel(label)}
                <AriaMenuButton.Wrapper
                    className="menu-button__inner u-d-inline-block"
                    onSelection={this.handleGenreSwitch}
                >
                    <AriaMenuButton.Button className={buttonClass} tag="button">
                        <strong>{selected}</strong>
                    </AriaMenuButton.Button>
                    {genreMenu}
                </AriaMenuButton.Wrapper>
            </div>
        );
    }

    renderDropdown(options = [], label) {
        if (!options.length) {
            return null;
        }

        let selected = null;

        const lis = options.map((item, i) => {
            if (item.active) {
                selected = item.text;
            }

            const klass = classnames('menu-button__list-item', {
                'menu-button__list-item--active': item.active
            });

            return (
                <AriaMenuButton.MenuItem
                    className={klass}
                    value={item.value}
                    tag="li"
                    key={i}
                >
                    <button tabIndex="-1">{item.text}</button>
                </AriaMenuButton.MenuItem>
            );
        });

        const timespanMenu = (
            <AriaMenuButton.Menu
                className="tooltip sub-menu sub-menu--condensed tooltip--radius tooltip--right-arrow tooltip--active feed-bar__tooltip"
                tag="ul"
            >
                {lis}
            </AriaMenuButton.Menu>
        );

        return (
            <div className="menu-button">
                {this.renderDropDownLabel(label)}
                <AriaMenuButton.Wrapper
                    className="menu-button__inner u-d-inline-block"
                    onSelection={this.handleTimespanSwitch}
                >
                    <AriaMenuButton.Button
                        className="menu-button__button button button--pill"
                        tag="button"
                    >
                        <strong>{selected}</strong>
                    </AriaMenuButton.Button>
                    {timespanMenu}
                </AriaMenuButton.Wrapper>
            </div>
        );
    }

    render() {
        const {
            items,
            contextDropDown,
            options,
            contextLabel,
            timespanLabel,
            title,
            showBorderMarker,
            padded,
            blog,
            underline,
            className,
            forComments,
            hideFilter
        } = this.props;
        const { markerWidth, markerX } = this.state;

        const borderStyle = {
            width: markerWidth
        };

        if (typeof markerX === 'number') {
            borderStyle.transform = `translateX(${markerX}px)`;
        }

        let borderMarker;
        let showItems;

        if (contextDropDown) {
            showItems = this.renderGenreDropdown(
                items,
                contextLabel,
                forComments
            );
        } else {
            showItems = this.renderItems(items);
        }

        if (showBorderMarker) {
            borderMarker = (
                <span className="feed-bar__marker" style={borderStyle} />
            );
        }
        const hasDropdowns = contextDropDown || options.length;
        const klass = classnames('feed-bar', {
            [className]: className,
            'feed-bar--padded': padded,
            'feed-bar--underline': underline,
            'feed-bar--has-menu': hasDropdowns
        });

        let content;

        if (!hideFilter) {
            content = showItems;

            if (hasDropdowns) {
                content = (
                    <div className="feed-bar__menus">
                        {this.renderDropdown(options, timespanLabel)}
                        {showItems}
                    </div>
                );
            }
        }

        let blogLink;

        if (blog) {
            blogLink = (
                <Link
                    className="u-fw-700 u-fs-13"
                    to="/world"
                    aria-label="View all blog posts"
                >
                    Read More
                </Link>
            );
        }

        return (
            <div className={klass}>
                {title}
                {borderMarker}
                {content}
                {blogLink}
            </div>
        );
    }
}
