import React, { Component, Fragment } from 'react';
import Cookie from 'js-cookie';
import classnames from 'classnames';

import { getConfig } from 'api/index';
import { cookies } from 'constants/index';
import { ucfirst } from 'utils/index';

import Dropdown from '../components/Dropdown';

import styles from './ApiControlPanel.module.css';

const apiOptions = {
    development: {
        baseUrl: 'https://dcf.aws.audiomack.com/v1',
        consumerKey: 'audiomack-js',
        consumerSecret: 'd414055457e991807398b0a61bc15b00'
    },
    production: {
        baseUrl: 'https://api.audiomack.com/v1',
        consumerKey: 'audiomack-js',
        consumerSecret: 'f3ac5b086f3eab260520d8e3049561e6'
    }
};

export default class ApiControlPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    handleFormSubmit = (e) => {
        e.preventDefault();

        const form = e.currentTarget;
        const baseUrl = form.baseUrl.value.trim();
        const consumerKey = form.consumerKey.value.trim();
        const consumerSecret = form.consumerSecret.value.trim();
        const config = {
            baseUrl,
            consumerKey,
            consumerSecret
        };

        this.setConfig(config);

        window.location.reload();
    };

    handleFormToggle = () => {
        this.setState({
            open: !this.state.open
        });
    };

    handleDropdownChange = (text, e) => {
        const value = e.currentTarget.getAttribute('data-value');

        this.setConfig(apiOptions[value]);

        window.location.reload();
    };

    getValueFromConfig({ baseUrl, consumerKey, consumerSecret }) {
        const value = Object.keys(apiOptions).find((key) => {
            const env = apiOptions[key];

            return (
                env.baseUrl === baseUrl &&
                env.consumerKey === consumerKey &&
                env.consumerSecret === consumerSecret
            );
        });

        return value;
    }

    setConfig({ baseUrl, consumerKey, consumerSecret }) {
        const config = JSON.stringify({
            baseUrl,
            consumerKey,
            consumerSecret
        });

        Cookie.set(cookies.apiConfig, config, {
            path: '/',
            secure:
                process.env.NODE_ENV !== 'development' &&
                process.env.AUDIOMACK_DEV !== 'true',
            sameSite: 'strict'
        });
    }

    render() {
        const config = getConfig();
        const { baseUrl, consumerKey, consumerSecret } = config;
        const buttonClass = classnames(styles.button, {
            [styles.buttonActive]: this.state.open
        });
        const formClass = classnames(styles.form, {
            [styles.formActive]: this.state.open
        });
        const value = this.getValueFromConfig(config);
        const dropdownValue = value || 'custom';
        const dropdownOptions = Object.keys(apiOptions).map((key) => {
            return {
                text: ucfirst(key),
                value: key
            };
        });

        if (!value) {
            dropdownOptions.push({ text: 'Custom', value: 'custom' });
        }

        return (
            <Fragment>
                <button className={buttonClass} onClick={this.handleFormToggle}>
                    API
                </button>
                <form className={formClass} onSubmit={this.handleFormSubmit}>
                    <Dropdown
                        options={dropdownOptions}
                        onChange={this.handleDropdownChange}
                        value={dropdownValue}
                        menuClassName={styles.dropdown}
                    />
                    <label>
                        <strong>Base URL:</strong>
                        <input
                            type="text"
                            defaultValue={baseUrl}
                            name="baseUrl"
                        />
                    </label>
                    <label>
                        <strong>Consumer Key:</strong>
                        <input
                            type="text"
                            defaultValue={consumerKey}
                            name="consumerKey"
                        />
                    </label>
                    <label>
                        <strong>Consumer Secret:</strong>
                        <input
                            type="text"
                            defaultValue={consumerSecret}
                            name="consumerSecret"
                        />
                    </label>
                    <div className={styles.buttons}>
                        <input
                            type="submit"
                            className="button button--pill"
                            value="Update and reload"
                        />
                        <input
                            onClick={this.handleFormToggle}
                            type="button"
                            className="button button--pill"
                            value="Cancel"
                        />
                    </div>
                </form>
            </Fragment>
        );
    }
}
