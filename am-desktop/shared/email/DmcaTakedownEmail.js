import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class DmcaTakedownEmail extends Component {
    static propTypes = {
        uploader: PropTypes.string,
        link: PropTypes.string,
        artist: PropTypes.string,
        title: PropTypes.string
    };

    static defaultProps = {
        uploader: 'there'
    };

    static subject(props) {
        return `${props.title} by ${props.artist} was removed`;
    }

    static key() {
        return 'dmca-takedown';
    }

    render() {
        const { uploader, link } = this.props;
        const host = process.env.AM_URL;
        let musicLink = `${host}/404`;

        if (link) {
            musicLink = `${host}${link}`;
        }

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hi {uploader},</Text>

                    <Text>
                        The following sound you uploaded to Audiomack has been
                        removed: {musicLink}. It was removed either due to a
                        DMCA takedown request or because it violated our terms
                        of service. If you would like to file a counter-DMCA
                        claim you can respond to this email.
                    </Text>

                    <Text>
                        Before responding to this email please read below - we
                        will not process any DMCA Counterclaims that ask the
                        questions below.
                    </Text>

                    <h3>DMCA FAQ</h3>

                    <h4>Why did you take down my song?! I bought it!</h4>
                    <Text>
                        We've removed your song or album because an official
                        copyright agent (RIAA, IFPI, Major Label, Publishing
                        Administrator or Artist) has informed us it contains
                        material they own the copyright to. Buying music does
                        not grant you a license to redistribute it online. This
                        is at the sole discretion of the original copyright
                        holder.
                    </Text>

                    <h4>It's a DJ Mix! Why was it taken down?</h4>
                    <Text>
                        Audiomack doesn't have a license to cover DJ mixes and
                        other long form audio content, and we were required to
                        remove this by a rightsholder.
                    </Text>

                    <h4>
                        It's a remix/cover/edit/different than the original!
                    </h4>
                    <Text>
                        The law requires us to take it down if it contains any
                        material whatsoever from someone else's copyrighted
                        work. Changing, remixing, rearranging, or otherwise
                        editing someone else's work does not invalidate their
                        copyright.
                    </Text>

                    <h4>But I don't have it up for download!</h4>
                    <Text>
                        We still have to take it down by law. Rights holders are
                        entitled to payment for streams, not just downloads -
                        meaning they have say as to where and how their music is
                        streamed on sites like Audiomack.
                    </Text>

                    <h4>
                        Still wondering if you should submit a counterclaim?
                    </h4>
                    <Text>
                        Unless you are either a) the 100% sole copyright owner
                        and 100% sole publishing owner of the work in question,
                        or you b) have written, legally binding confirmation
                        from ALL copyright and publishing administrators of the
                        work in question granting you license to use said work,
                        you cannot file a valid counterclaim.
                    </Text>

                    <Text>
                        If you are the sole copyright/publishing owner, reply to
                        this email and we'll evaluate your claim.
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
