import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';
import Button from './elements/Button';
import Link from './elements/Link';
import Image from './elements/Image';
import Table from './elements/Table';
import TableCell from './elements/TableCell';

export default class SubscriptionGracePeriodEmail extends Component {
    static propTypes = {
        link: PropTypes.string
    };

    static defaultProps = {
        link: ''
    };

    static subject() {
        return 'Update your billing information to avoid losing Audiomack Premium';
    }

    static key() {
        return 'subscription-grace-period';
    }

    renderFeatures() {
        const features = [
            {
                title: 'An Ad-Free Experience',
                imgSrc: '/static/images/desktop/no-ads.png',
                imgSrc2: '/static/images/desktop/no-ads@2x.png'
            },
            {
                title: 'Download and Sync Playlists',
                imgSrc: '/static/images/desktop/playlists.png',
                imgSrc2: '/static/images/desktop/playlists@2x.png'
            },
            {
                title: 'Hifi Streaming on Wif',
                imgSrc: '/static/images/desktop/hifi.png',
                imgSrc2: '/static/images/desktop/hifi@2x.png'
            }
        ].map((feature, i) => {
            return (
                <TableCell key={i} style={{ padding: '0 12px' }}>
                    <Image
                        src={feature.imgSrc}
                        src2x={feature.imgSrc2}
                        alt={feature.title}
                    />
                    <Text
                        center
                        style={{ fontSize: 17, lineHeight: 1.4, margin: 0 }}
                    >
                        <strong>{feature.title}</strong>
                    </Text>
                </TableCell>
            );
        });

        return (
            <Table style={{ textAlign: 'center', marginBottom: 35 }}>
                {features}
            </Table>
        );
    }

    render() {
        const { link } = this.props;
        return (
            <EmailTemplate renderSimpleFooter>
                <Box border style={{ marginTop: 16 }}>
                    <Text center style={{ fontSize: 19, lineHeight: '1.4' }}>
                        <strong>
                            We recently tried to charge your payment method but
                            it failed, possibly due to an expired credit card or
                            some other reason.
                        </strong>
                    </Text>
                    <Text center style={{ padding: '0 20px' }}>
                        Please{' '}
                        <Link href={link}>update your payment information</Link>{' '}
                        to avoid losing your Audiomack premium benefits such as:
                    </Text>
                    {this.renderFeatures()}
                    <Text center>
                        <Button
                            href={link}
                            style={{
                                width: 200,
                                paddingTop: 16,
                                paddingBottom: 16
                            }}
                        >
                            UPDATE NOW
                        </Button>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
