import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class NewGhostEmail extends Component {
    static propTypes = {
        ampPartnerName: PropTypes.string,
        ampPartnerUrl: PropTypes.string,
        accountName: PropTypes.string,
        accountUrl: PropTypes.string
    };

    static subject() {
        return 'Notification of a new account created by an AMP Partner';
    }

    static key() {
        return 'new-ghost';
    }

    render() {
        const {
            ampPartnerName,
            ampPartnerUrl,
            accountName,
            accountUrl
        } = this.props;

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>
                        <a href={ampPartnerUrl}>{ampPartnerName}</a> has created
                        a new account named{' '}
                        <a href={accountUrl}>{accountName}</a>.
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
