import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class PasswordChangedEmail extends Component {
    static propTypes = {
        name: PropTypes.string
    };

    static defaultProps = {
        name: 'there'
    };

    static subject() {
        return 'Audiomack Password Changed';
    }

    static key() {
        return 'password-changed';
    }

    render() {
        const { name } = this.props;

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hi {name},</Text>
                    <Text>
                        We want to let you know that your password was
                        successfully changed.
                    </Text>
                    <Text>
                        If you did not authorize this change, please reach out
                        to us at support@audiomack.com.
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
