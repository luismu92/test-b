import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class ApiRequestEmail extends Component {
    static propTypes = {
        name: PropTypes.string,
        company: PropTypes.string,
        request: PropTypes.string,
        replyTo: PropTypes.string
    };

    static subject() {
        return 'API Request Access';
    }

    static key() {
        return 'api-request';
    }

    renderLine(label, data) {
        if (!data) {
            return null;
        }

        return (
            <Text>
                <strong>{label}</strong>: {data}
            </Text>
        );
    }

    render() {
        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>{this.renderLine('Name', this.props.name)}</Text>
                    <Text>
                        {this.renderLine('Company', this.props.company)}
                    </Text>
                    <Text>
                        {this.renderLine(
                            'API request details',
                            this.props.request
                        )}
                    </Text>
                    <Text>
                        {this.renderLine('Reply to email', this.props.replyTo)}
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
