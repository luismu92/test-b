import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import Button from './elements/Button';
import EmailTemplate from './EmailTemplate';

export default class ClaimGhostEmail extends Component {
    static propTypes = {
        name: PropTypes.string,
        url_slug: PropTypes.string,
        code: PropTypes.string,
        // Encoded email
        ee: PropTypes.string
    };

    static defaultProps = {
        name: ''
    };

    static subject(props) {
        const { name } = props;

        return `👋  Invitation to claim ${name}`;
    }

    static key() {
        return 'claim-ghost';
    }

    render() {
        const { name, code, url_slug: urlSlug, ee } = this.props;
        const host = process.env.AM_URL;
        let href = `${host}/404`;

        if (code) {
            href = `${host}/artist/${urlSlug}/claim/${code}/${ee}`;
        }

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hello {name},</Text>
                    <Text>
                        You have been invited to claim your creator account on
                        Audiomack. Please click the button below within 24 hours
                        to claim the account. Once claimed, you will be able to
                        manage your account and edit your profile.
                    </Text>

                    <Text center>
                        <Button href={href}>Claim my account</Button>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
