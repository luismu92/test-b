import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import Button from './elements/Button';
import EmailTemplate from './EmailTemplate';

export default class AmpPartnerApprovalEmail extends Component {
    static propTypes = {
        name: PropTypes.string
    };

    static defaultProps = {
        name: ''
    };

    static subject() {
        return 'AMP Partner Approval';
    }

    static key() {
        return 'amp-partner-approval';
    }

    render() {
        const { name } = this.props;
        const href = `${process.env.AM_URL}/dashboard?enableAmp=true`;

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hello {name},</Text>
                    <Text>
                        You have been approved for monetization on Audiomack
                        through the AMP Program.
                    </Text>
                    <Text>
                        Please click the link below to agree to our terms of
                        service and enable AMP.
                    </Text>

                    <Text center>
                        <Button href={href}>Enable AMP</Button>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
