import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';
import Button from './elements/Button';
import Image from './elements/Image';
import Link from './elements/Link';

export default class VerifiedCreatorEmail extends Component {
    static propTypes = {
        link: PropTypes.string,
        name: PropTypes.string,
        profileImage: PropTypes.string,
        urlSlug: PropTypes.string
    };

    static defaultProps = {
        link: `${process.env.AM_URL}/dashboard`,
        name: 'Audiomack'
    };

    static subject() {
        return 'You are now an Authenticated Creator on Audiomack!';
    }

    static key() {
        return 'authenticated-confirm';
    }

    render() {
        const { link, name, profileImage, urlSlug } = this.props;
        const unsubscribeText = (
            <span
                style={{
                    display: 'block',
                    color: '#ffffff',
                    marginTop: 35,
                    marginBottom: 35,
                    paddingLeft: 54,
                    paddingRight: 54,
                    fontSize: 13,
                    fontWeight: 600,
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.54,
                    letterSpacing: -0.5,
                    textAlign: 'center'
                }}
            >
                You are receiving this email because your account applied for
                authentication on Audiomack. To unsubscribe, change your{' '}
                <Link
                    href={`${process.env.AM_URL}/artist/${urlSlug}`}
                    style={{ textDecoration: 'underline' }}
                >
                    {' '}
                    notification preferences here.
                </Link>
            </span>
        );

        return (
            <EmailTemplate
                darkHeaderProfileImage
                renderSimpleFooter
                unsubscribeText={unsubscribeText}
                profileImage={profileImage}
                style={{
                    background: '#f9f9f9 !important',
                    border: 'solid 1px #eaeaea'
                }}
            >
                <Box
                    style={{
                        padding: '0 !important'
                    }}
                    innerStyle={{
                        padding: '0 30px 16px',
                        background: '#f9f9f9 !important'
                    }}
                >
                    <Text
                        center
                        style={{
                            margin: '6px auto 0 auto !important',
                            fontSize: 18,
                            fontWeight: 'normal',
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.56,
                            letterSpacing: -0.78,
                            textAlign: 'center',
                            color: '#272727'
                        }}
                    >
                        Congratulations{' '}
                        <span style={{ fontWeight: 'bold', color: '#ffa200' }}>
                            {name},
                        </span>
                    </Text>
                    <Text
                        center
                        style={{
                            margin: '0 auto !important',
                            fontSize: 18,
                            fontWeight: 'normal',
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.56,
                            letterSpacing: -0.78,
                            textAlign: 'center',
                            color: '#272727'
                        }}
                    >
                        You are now an Authenticated Creator on Audiomack!
                    </Text>
                    <table
                        border="0"
                        style={{
                            borderCollapse: 'collapse',
                            borderSpacing: 0,
                            background: '#fff !important',
                            marginTop: 30
                        }}
                    >
                        <tbody>
                            <tr>
                                <td
                                    style={{
                                        backgroundImage: `url("${
                                            process.env.AM_URL
                                        }/static/email/icons/gray-cutout.png")`,
                                        backgroundRepeat: 'repeat-x',
                                        backgroundSize: '25px 30px'
                                    }}
                                >
                                    <div style={{ textAlign: 'center' }}>
                                        <div
                                            style={{
                                                borderRadius: '50%',
                                                backgroundColor: '#ffa200',
                                                height: 60,
                                                width: 60,
                                                margin: 'auto'
                                            }}
                                        >
                                            <Image
                                                src={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/check-mark.png`}
                                                src2x={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/check-mark@2x.png`}
                                                style={{
                                                    height: 36,
                                                    width: 36,
                                                    backgroundColor: '#ffa200',
                                                    margin: 12,
                                                    fill: '#fff'
                                                }}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    style={{
                                        textAlign: 'center',
                                        margin: '30px 0 0 0',
                                        backgroundColor: '#ffffff',
                                        padding: '0 30px 31px'
                                    }}
                                >
                                    <Text
                                        center
                                        style={{
                                            padding: '0 19%',
                                            fontSize: 28,
                                            fontWeight: 'bold',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.11,
                                            letterSpacing: -1.4,
                                            textAlign: 'center',
                                            color: '#2a2a2a',
                                            margin: '10px 0 6px'
                                        }}
                                    >
                                        Distinguish yourself as creator.
                                    </Text>
                                    <Text
                                        center
                                        style={{
                                            fontSize: 16,
                                            fontWeight: 'normal',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.63,
                                            letterSpacing: -0.5,
                                            textAlign: 'center',
                                            color: '#686868',
                                            margin: 0
                                        }}
                                    >
                                        Authentication allows creators to
                                        distinguish their Audiomack accounts
                                        from listener accounts.
                                    </Text>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table
                        border="0"
                        style={{
                            borderCollapse: 'collapse',
                            borderSpacing: 0,
                            background: '#fff !important',
                            marginTop: 30
                        }}
                    >
                        <tbody>
                            <tr>
                                <td
                                    style={{
                                        backgroundImage: `url("${
                                            process.env.AM_URL
                                        }/static/email/icons/gray-cutout.png")`,
                                        backgroundRepeat: 'repeat-x',
                                        backgroundSize: '25px 30px'
                                    }}
                                >
                                    <div style={{ textAlign: 'center' }}>
                                        <div
                                            style={{
                                                borderRadius: '50%',
                                                backgroundColor: '#ffa200',
                                                height: 60,
                                                width: 60,
                                                margin: 'auto'
                                            }}
                                        >
                                            <Image
                                                src={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/bell-active.png`}
                                                src2x={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/bell-active@2x.png`}
                                                style={{
                                                    height: 27,
                                                    width: 28,
                                                    backgroundColor: '#ffa200',
                                                    margin:
                                                        '17px 16px 16px 16px',
                                                    fill: '#fff'
                                                }}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    style={{
                                        textAlign: 'center',
                                        margin: '30px 0 0 0',
                                        backgroundColor: '#ffffff',
                                        padding: '0 30px 31px'
                                    }}
                                >
                                    <Text
                                        center
                                        style={{
                                            fontSize: 28,
                                            fontWeight: 'bold',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.11,
                                            letterSpacing: -1.4,
                                            textAlign: 'center',
                                            color: '#2a2a2a',
                                            margin: '10px 0 6px'
                                        }}
                                    >
                                        Instantly notify fans when you release
                                        content.
                                    </Text>
                                    <Text
                                        center
                                        style={{
                                            fontSize: 16,
                                            fontWeight: 'normal',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.63,
                                            letterSpacing: -0.5,
                                            textAlign: 'center',
                                            color: '#686868',
                                            padding: '0 8%',
                                            margin: 0
                                        }}
                                    >
                                        Authenticated creators gain access to
                                        our notification tools, allowing you to
                                        notify your followers whenever you have
                                        a new release.
                                    </Text>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table
                        border="0"
                        style={{
                            borderCollapse: 'collapse',
                            borderSpacing: 0,
                            background: '#fff !important',
                            marginTop: 30
                        }}
                    >
                        <tbody>
                            <tr>
                                <td
                                    style={{
                                        backgroundImage: `url("${
                                            process.env.AM_URL
                                        }/static/email/icons/gray-cutout.png")`,
                                        backgroundRepeat: 'repeat-x',
                                        backgroundSize: '25px 30px'
                                    }}
                                >
                                    <div style={{ textAlign: 'center' }}>
                                        <div
                                            style={{
                                                borderRadius: '50%',
                                                backgroundColor: '#ffa200',
                                                height: 60,
                                                width: 60,
                                                margin: 'auto'
                                            }}
                                        >
                                            <Image
                                                src={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/trending-up.png`}
                                                src2x={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/trending-up@2x.png`}
                                                style={{
                                                    height: 34,
                                                    width: 34,
                                                    backgroundColor: '#ffa200',
                                                    margin: 13,
                                                    fill: '#fff'
                                                }}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    style={{
                                        textAlign: 'center',
                                        margin: '30px 0 0 0',
                                        backgroundColor: '#ffffff',
                                        padding: '0 30px 31px'
                                    }}
                                >
                                    <Text
                                        center
                                        style={{
                                            fontSize: 28,
                                            fontWeight: 'bold',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.11,
                                            letterSpacing: -1.4,
                                            textAlign: 'center',
                                            color: '#2a2a2a',
                                            margin: '10px 0 6px'
                                        }}
                                    >
                                        Submit your content for trending
                                        consideration.
                                    </Text>
                                    <Text
                                        center
                                        style={{
                                            fontSize: 16,
                                            fontWeight: 'normal',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.63,
                                            letterSpacing: -0.5,
                                            textAlign: 'center',
                                            color: '#686868',
                                            padding: '0 8%',
                                            margin: 0
                                        }}
                                    >
                                        Audiomack's trending charts are the
                                        premier location for creator discovery.
                                        Authenticated creators gain access to
                                        our new trending submission tools.
                                    </Text>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table
                        border="0"
                        style={{
                            borderCollapse: 'collapse',
                            borderSpacing: 0,
                            background: '#fff !important',
                            marginTop: 30
                        }}
                    >
                        <tbody>
                            <tr>
                                <td
                                    style={{
                                        backgroundImage: `url("${
                                            process.env.AM_URL
                                        }/static/email/icons/gray-cutout.png")`,
                                        backgroundRepeat: 'repeat-x',
                                        backgroundSize: '25px 30px'
                                    }}
                                >
                                    <div style={{ textAlign: 'center' }}>
                                        <div
                                            style={{
                                                borderRadius: '50%',
                                                backgroundColor: '#ffa200',
                                                height: 60,
                                                width: 60,
                                                margin: 'auto'
                                            }}
                                        >
                                            <Image
                                                src={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/call-bell.png`}
                                                src2x={`${
                                                    process.env.AM_URL
                                                }/static/email/icons/call-bell@2x.png`}
                                                style={{
                                                    height: 28,
                                                    width: 28,
                                                    backgroundColor: '#ffa200',
                                                    margin: 16,
                                                    fill: '#fff'
                                                }}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    style={{
                                        textAlign: 'center',
                                        margin: '30px 0 0 0',
                                        backgroundColor: '#ffffff',
                                        padding: '0 30px 31px'
                                    }}
                                >
                                    <Text
                                        center
                                        style={{
                                            fontSize: 28,
                                            fontWeight: 'bold',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.11,
                                            letterSpacing: -1.4,
                                            textAlign: 'center',
                                            color: '#2a2a2a',
                                            padding: '0 5%',
                                            margin: '10px 0 6px'
                                        }}
                                    >
                                        Gain access to our creator services.
                                    </Text>
                                    <Text
                                        center
                                        style={{
                                            fontSize: 16,
                                            fontWeight: 'normal',
                                            fontStretch: 'normal',
                                            fontStyle: 'normal',
                                            lineHeight: 1.63,
                                            letterSpacing: -0.5,
                                            textAlign: 'center',
                                            color: '#686868',
                                            padding: '0 8%',
                                            margin: 0
                                        }}
                                    >
                                        Audiomack is constantly equipping
                                        creators with new tools to help their
                                        careers. Authenticated creators gain
                                        access to our growing list of exclusive
                                        creator services.
                                    </Text>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Text center>
                        <Button
                            href={link}
                            style={{
                                width: 180,
                                paddingTop: 16,
                                paddingBottom: 16,
                                margin: '24px 0 8px 0'
                            }}
                        >
                            Go to Dashboard
                        </Button>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
