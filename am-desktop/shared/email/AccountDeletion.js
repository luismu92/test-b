import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class AccountDeletion extends Component {
    static propTypes = {
        artistName: PropTypes.string,
        artistUrl: PropTypes.string,
        artistSongUploadCount: PropTypes.string,
        artistAlbumUploadCount: PropTypes.string,
        created: PropTypes.string,
        updated: PropTypes.string,
        deletionReason: PropTypes.string
    };

    static subject(props) {
        const { artistName } = props;

        return `Account Deletion Request from ${artistName}`;
    }

    static key() {
        return 'account-deletion';
    }

    renderLine(label, data) {
        if (!data) {
            return null;
        }

        return (
            <Text>
                <strong>{label}</strong>: {data}
            </Text>
        );
    }

    render() {
        const { artistName, artistUrl } = this.props;
        const artistLink = `${process.env.AM_URL}/artist/${artistUrl}`;

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>
                        User: {artistName} is deleting their account, logging
                        this temporarily through email. This user has not been
                        permanently deleted from the system yet.
                    </Text>
                    <Text>{this.renderLine('Artist URL', artistLink)}</Text>
                    <Text>
                        {this.renderLine(
                            'Song uploads',
                            this.props.artistSongUploadCount
                        )}
                    </Text>
                    <Text>
                        {this.renderLine(
                            'Album uploads',
                            this.props.artistAlbumUploadCount
                        )}
                    </Text>
                    <Text>
                        {this.renderLine(
                            'Account created on',
                            this.props.created
                        )}
                    </Text>
                    <Text>
                        {this.renderLine(
                            'Account last updated',
                            this.props.updated
                        )}
                    </Text>
                    <Text>
                        {this.renderLine(
                            'Reason for deletion',
                            this.props.deletionReason
                        )}
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
