import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';
import Button from './elements/Button';
import List from './elements/List';
import ListItem from './elements/ListItem';
import Link from './elements/Link';

export default class AuthenticatedDeniedEmail extends Component {
    static propTypes = {
        link: PropTypes.string,
        profileImage: PropTypes.string,
        urlSlug: PropTypes.string
    };

    static defaultProps = {
        link: `${process.env.AM_URL}/dashboard`
    };

    static subject() {
        return 'Creator Authentication Declined';
    }

    static key() {
        return 'authenticated-denied';
    }

    render() {
        const { link, profileImage, urlSlug } = this.props;
        const unsubscribeText = (
            <span
                style={{
                    display: 'block',
                    color: '#ffffff',
                    marginTop: 35,
                    marginBottom: 35,
                    paddingLeft: 54,
                    paddingRight: 54,
                    fontSize: 13,
                    fontWeight: 600,
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.54,
                    letterSpacing: -0.5,
                    textAlign: 'center'
                }}
            >
                You are receiving this email because your account applied for
                authentication on Audiomack. To unsubscribe, change your{' '}
                <Link
                    href={`${process.env.AM_URL}/artist/${urlSlug}`}
                    style={{ textDecoration: 'underline' }}
                >
                    {' '}
                    notification preferences here.
                </Link>
            </span>
        );

        return (
            <EmailTemplate
                darkHeaderProfileImage
                renderSimpleFooter
                unsubscribeText={unsubscribeText}
                profileImage={profileImage}
                style={{
                    background: '#fff !important',
                    border: 'solid 1px #eaeaea'
                }}
            >
                <Box
                    style={{
                        padding: '0 !important'
                    }}
                    innerStyle={{
                        padding: '0 40px 16px',
                        background: '#fff !important'
                    }}
                >
                    <Text
                        center
                        style={{
                            margin: '6px 0 20px 0 !important',
                            padding: '0 67px !important',
                            fontSize: 18,
                            fontWeight: 600,
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.56,
                            letterSpacing: -0.78,
                            textAlign: 'center',
                            color: '#272727'
                        }}
                    >
                        Unfortunately your Creator Authentication has been
                        declined at this time.
                    </Text>
                    <Text
                        center
                        style={{
                            padding: '0 43px',
                            margin: '0 0 30px',
                            fontSize: 14,
                            fontWeight: 'normal',
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.64,
                            letterSpacing: -0.6,
                            textAlign: 'center',
                            color: '#272727'
                        }}
                    >
                        If you are indeed a content creator with original
                        content, please look at the potential reasons for being
                        declined and try again.
                    </Text>
                    <Text
                        style={{
                            margin: '0 0 17px',
                            fontSize: 14,
                            fontWeight: 'bold',
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.64,
                            letterSpacing: -0.6,
                            color: '#272727'
                        }}
                    >
                        Reasons to be declined or banned from applying:
                    </Text>
                    <List
                        ul
                        style={{
                            marginBottom: 0,
                            paddingInlineStart: '17px !important',
                            paddingInlineEnd: '30px !important',
                            fontSize: 13,
                            fontWeight: 600,
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.54,
                            letterSpacing: -0.56,
                            color: '#272727'
                        }}
                    >
                        <ListItem style={{ color: '#ffa200' }}>
                            <span style={{ color: '#272727' }}>
                                The social media accounts you authenticated do
                                not prove you are the creator of the uploaded
                                content.
                            </span>
                        </ListItem>
                        <ListItem style={{ color: '#ffa200' }}>
                            <span style={{ color: '#272727' }}>
                                The social media accounts you authenticated do
                                not match the content you are uploading.
                            </span>
                        </ListItem>
                        <ListItem style={{ color: '#ffa200' }}>
                            <span style={{ color: '#272727' }}>
                                The content recognition does not match with the
                                artist you are claiming to be.
                            </span>
                        </ListItem>
                        <ListItem style={{ color: '#ffa200' }}>
                            <span style={{ color: '#272727' }}>
                                There is content on your profile that does not
                                belong to the creator being authenticated.
                            </span>
                        </ListItem>
                        <ListItem style={{ color: '#ffa200' }}>
                            <span style={{ color: '#272727' }}>
                                You are not an original content creator.
                            </span>
                        </ListItem>
                    </List>
                    <Text center>
                        <Button
                            href={link}
                            style={{
                                width: 180,
                                paddingTop: 14,
                                paddingBottom: 16,
                                margin: '24px 0 8px'
                            }}
                        >
                            Go to Dashboard
                        </Button>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
