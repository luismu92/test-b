import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import Button from './elements/Button';
import EmailTemplate from './EmailTemplate';

export default class NewUserEmail extends Component {
    static propTypes = {
        name: PropTypes.string,
        hash: PropTypes.string,
        encodedReturnPath: PropTypes.string,
        isVerifiedAlready: PropTypes.bool
    };

    static defaultProps = {
        name: 'there',
        isVerifiedAlready: false
    };

    static subject(props) {
        if (props.isVerifiedAlready) {
            return `👋  Welcome to ${process.env.AM_NAME}`;
        }

        return '👋  Please verify your email address';
    }

    static key(props) {
        const defaultKey = 'new-user';

        if (!props) {
            return defaultKey;
        }

        if (props.isResendEmail) {
            return 'resent-verification';
        }

        if (props.isVerifiedAlready) {
            return 'new-auto-verified-user';
        }

        return defaultKey;
    }

    render() {
        const { name, hash, encodedReturnPath, isVerifiedAlready } = this.props;
        const host = process.env.AM_URL;
        let confirmLink = `${host}/404`;

        if (hash) {
            confirmLink = host;

            if (encodedReturnPath) {
                confirmLink = `${confirmLink}${decodeURIComponent(
                    encodedReturnPath
                )}`;
            }

            confirmLink = `${confirmLink}?verifyHash=${hash}`;
        }

        let content;

        if (isVerifiedAlready) {
            content = (
                <Text center>
                    <Button href={`${process.env.AM_URL}/trending-now`}>
                        See What’s Trending
                    </Button>
                </Text>
            );
        } else {
            content = (
                <Fragment>
                    <Text>
                        Confirm your email by clicking the button below to get
                        started, and we’re excited to have you on board.
                    </Text>
                    <Text center>
                        <Button href={confirmLink}>
                            Confirm Email Address
                        </Button>
                    </Text>
                </Fragment>
            );
        }

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hey {name}!</Text>
                    <Text>
                        You’re about to be part of the Audiomack community -
                        where millions of listeners and artists come together
                        every day to discover and share new music, completely
                        free.
                    </Text>
                    <Text>
                        We built Audiomack to be the next generation of music
                        discovery and hosting - with no premium accounts for
                        artists, unlimited listening for fans and the best
                        trending music and playlists available anywhere.
                    </Text>
                    {content}
                    <Text>
                        If you have any problems or need technical support,
                        please contact us: support@audiomack.com
                    </Text>

                    <Text>
                        - Dave Macli
                        <br />
                        CEO, Founder
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
