import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { humanizeNumber } from 'utils/index';

import Box from './elements/Box';
import Text from './elements/Text';
import Image from './elements/Image';
import Link from './elements/Link';
import Table from './elements/Table';
import TableCell from './elements/TableCell';
import Button from './elements/Button';
import MusicPreview from './elements/MusicPreview';
import EmailTemplate from './EmailTemplate';

export default class WeeklyReportEmail extends Component {
    static propTypes = {
        stats: PropTypes.object,
        blogPosts: PropTypes.array
    };

    static defaultProps = {
        stats: {},
        blogPosts: []

        // TEST TEST TEST
        // Only un-comment when working on the template
        /*
        stats: {
            artist_id: 11222837,
            play_song: 16926613,
            follow: 4102727,
            favorite: 65231,
            top_songs: [
                {
                    content_id: 7264421,
                    play: 7280636,
                    favorite: 1876,
                    image:
                        'http://assets.dev.audiomack.com/default-song-image.jpg',
                    title: 'Corvette Doors',
                    artist: 'Currensy'
                },
                {
                    content_id: 7151381,
                    play: 1231313,
                    favorite: 489,
                    image:
                        'http://assets.dev.audiomack.com/default-song-image.jpg',
                    title: 'Corvette Doors',
                    artist: 'Currensy'
                },
                {
                    content_id: 7151329,
                    play: 1011140,
                    favorite: 333,
                    image:
                        'http://assets.dev.audiomack.com/default-song-image.jpg',
                    title: 'Corvette Doors',
                    artist: 'Currensy'
                },
                {
                    content_id: 7151341,
                    play: 875047,
                    favorite: 321,
                    image:
                        'http://assets.dev.audiomack.com/default-song-image.jpg',
                    title: 'Corvette Doors',
                    artist: 'Currensy'
                },
                {
                    content_id: 7151374,
                    play: 835737,
                    favorite: 414,
                    image:
                        'http://assets.dev.audiomack.com/default-song-image.jpg',
                    title: 'Corvette Doors',
                    artist: 'Currensy'
                }
            ],
            artist_image:
                'https://assets.dev.audiomack.com/_default/default-artist-image.png',
            artist_url: 'bin-xu',
            artist_name: 'Bin Xu'
        },
        blogPosts: [
            {
                id: '5e178dd3bac2d716becd797c',
                uuid: '4516f6f9-6a24-4277-9585-ec592e0b23cf',
                title: 'Instant Podcast Uploads with Our New RSS Feed Importer',
                slug: 'podcast-rss-feed-importer',
                comment_id: '5e178dd3bac2d716becd797c',
                feature_image:
                    'https://assets.audiomack.com/_am-blog/2020/01/AM-Blog-RSS-1.jpg',
                featured: false,
                page: false,
                meta_title: 'Audiomack Launches RSS Feed Importer for Podcasts',
                meta_description:
                    "Podcast creators can use Audiomack's new RSS feed importer to automatically ingest new content and edits.",
                created_at: '2020-01-09T15:32:19.000-05:00',
                updated_at: '2020-01-10T13:25:58.000-05:00',
                published_at: '2020-01-10T13:04:00.000-05:00',
                custom_excerpt:
                    'Podcasters: spend less time uploading and more time creating. ',
                codeinjection_head: null,
                codeinjection_foot: null,
                og_image: null,
                og_title: null,
                og_description: null,
                twitter_image: null,
                twitter_title: null,
                twitter_description: null,
                custom_template: null,
                canonical_url: null,
                primary_author: null,
                primary_tag: null,
                url: 'https://ghost.audiomack.com/podcast-rss-feed-importer/',
                excerpt:
                    'Podcasters: spend less time uploading and more time creating. '
            },
            {
                id: '5d7febe1fdb5d75b056dca19',
                uuid: 'be8f4b75-5b4b-44b0-9abd-8d87cd3ff16f',
                title: 'Welcome to the New Search',
                slug: 'welcome-to-the-new-search',
                html:
                    "<h3 id='search-history-trending-searches-and-more-make-finding-the-music-you-want-on-audiomack-simpler-than-ever-'><strong>Search history, trending searches, and more make finding the music you want on Audiomack simpler than\u00a0ever.</strong></h3><!--kg-card-begin: image--><figure class='kg-card kg-image-card'><img src='https://assets.audiomack.com/_am-blog/Welcome-to-the-New-Search/1-v1V0ApThGR8_lGXAwLzJ9g.jpeg' class='kg-image'></figure><!--kg-card-end: image--><p>When it comes to finding what music you <em>want</em> to hear, and what music you <em>need</em> to hear, getting there as quickly and easily as possible is crucial.</p><p>Audiomack\u2019s latest release, 4.4.0, includes a holistic overhaul of our search page from beginning to end. These new features and subtle changes along with allow us to better support creators and tastemakers, driving more plays and fans toward the music and artists that Audiomack and our community of listeners cosign.</p><p><strong>Search History:</strong> Searched for an album recently, liked what you heard, and can\u2019t quite remember what it was? Forgot to favorite something and want to get back to it quickly? The Audiomack app now saves your seven most recent searches, making it easy to get right back to what you\u2019ve been listening to lately.</p><p><strong>Trending Searches:</strong> With trending searches, find out what most users are searching for. The top of the search page is now populated with the hottest artists, albums, songs, playlists, and series from across the app.</p><p><strong>Verified Search Priority:</strong> Find what you\u2019re expecting. Whether you\u2019re searching for Future\u2019s official artist account or our own Verified: Hip-Hop playlist, our latest updates make sure you find exactly what you want, not someone trying to game the system.</p><p><strong>Search Replacements:</strong> Looking for a song or artist that might not be on Audiomack yet? Search replacements will surface alternatives that we think you\u2019ll love anyway.</p><p>Taking advantage of our revamped search functionality is easy. Just upgrade your Audiomack app to the latest version (4.4.0) and start searching.\n\n</p>",
                comment_id: '5d364c039227b302258c4cf7',
                feature_image:
                    'https://assets.audiomack.com/_am-blog/Welcome-to-the-New-Search/1-v1V0ApThGR8_lGXAwLzJ9g.jpeg',
                featured: false,
                page: false,
                meta_title: null,
                meta_description: null,
                created_at: '2019-03-25T20:00:00.000-04:00',
                updated_at: '2019-08-15T13:52:42.000-04:00',
                published_at: '2019-03-25T20:00:00.000-04:00',
                custom_excerpt:
                    '\nSearch history, trending searches, and more make finding the music you want on the Audiomack app simpler than ever.\n',
                codeinjection_head: null,
                codeinjection_foot: null,
                og_image: null,
                og_title: null,
                og_description: null,
                twitter_image: null,
                twitter_title: null,
                twitter_description: null,
                custom_template: null,
                canonical_url:
                    'https://medium.com/@brendanvaran/welcome-to-the-new-search-d45307ec41e6',
                primary_author: null,
                primary_tag: null,
                url: 'https://ghost.audiomack.com/welcome-to-the-new-search/',
                excerpt:
                    '\nSearch history, trending searches, and more make finding the music you want on the Audiomack app simpler than ever.\n'
            }
        ]
        */
    };

    static subject() {
        return "Here's how your tracks performed last week.";
    }

    static key() {
        return 'artist-weekly';
    }

    renderStats() {
        const { stats } = this.props;

        const counts = [
            {
                icon: '/static/email/icons/play-icon.png',
                icon2x: '/static/email/icons/play-icon@2x.png',
                title: 'Total Plays',
                value: humanizeNumber(stats.play_song)
            },
            {
                icon: '/static/email/icons/avatar-icon.png',
                icon2x: '/static/email/icons/avatar-icon@2x.png',
                title: 'New Followers',
                value: humanizeNumber(stats.follow)
            },
            {
                icon: '/static/email/icons/heart-icon.png',
                icon2x: '/static/email/icons/heart-icon@2x.png',
                title: 'Total Favorites',
                value: humanizeNumber(stats.favorite)
            }
        ].map((stat, i) => {
            return (
                <Table
                    key={i}
                    style={{
                        display: 'inline-table',
                        maxWidth: 140
                    }}
                >
                    <TableCell>
                        <Image
                            src={stat.icon}
                            src2x={stat.icon2x}
                            style={{
                                width: 'auto',
                                height: 30,
                                display: 'block',
                                marginTop: 0,
                                marginBottom: 0,
                                marginLeft: 'auto',
                                marginRight: 'auto'
                            }}
                        />
                        <Text
                            center
                            style={{
                                fontWeight: 600,
                                fontSize: 14,
                                marginTop: 6,
                                marginBottom: 6
                            }}
                        >
                            <span
                                style={{
                                    color: '#888',
                                    fontSize: 13,
                                    letterSpacing: '-0.5px'
                                }}
                            >
                                {stat.title}
                            </span>
                            <p
                                style={{
                                    display: 'block',
                                    fontSize: 26,
                                    lineHeight: 1,
                                    marginTop: 0
                                }}
                            >
                                <strong>{stat.value}</strong>
                            </p>
                        </Text>
                    </TableCell>
                </Table>
            );
        });

        return (
            <Box
                style={{
                    textAlign: 'center',
                    marginTop: 20,
                    paddingLeft: 0,
                    paddingRight: 0
                }}
                innerStyle={{
                    background: 'none'
                }}
            >
                {counts}
            </Box>
        );
    }

    renderTopSongs() {
        const {
            stats: { top_songs }
        } = this.props;

        const headerCells = [
            {
                title: 'Song'
            },
            {
                title: 'Plays',
                style: {
                    width: '50px',
                    textAlign: 'right'
                }
            },
            {
                title: 'Favs',
                style: {
                    width: '50px',
                    textAlign: 'right'
                }
            }
        ].map((cell, i) => {
            return (
                <TableCell
                    key={`head-${i}`}
                    style={{
                        fontWeight: 400,
                        fontSize: 12,
                        color: '#777777',
                        textTransform: 'uppercase',
                        borderBottom: '#e8e8e8 1px solid',
                        paddingBottom: 10,
                        ...cell.style
                    }}
                >
                    {cell.title}
                </TableCell>
            );
        });

        const tableHead = (
            <thead style={{ textAlign: 'left' }}>{headerCells}</thead>
        );

        const items = (top_songs || []).map((item, i) => {
            return (
                <tr
                    key={`item-${i}`}
                    style={{
                        borderBottom: '#e8e8e8 1px solid'
                    }}
                >
                    <MusicPreview musicItem={item} index={i} />
                </tr>
            );
        });

        const table = (
            <Table
                tableHead={tableHead}
                style={{
                    marginBottom: 25,
                    borderCollapse: 'collapse'
                }}
            >
                {items}
            </Table>
        );

        let tableContent = (
            <Fragment>
                <Text
                    style={{
                        fontSize: 15,
                        letterSpacing: '-0.4px',
                        marginTop: 0
                    }}
                >
                    <strong>Your top-performing songs:</strong>
                </Text>
                {table}
            </Fragment>
        );

        if (!items.length) {
            tableContent = null;
        }

        return (
            <Fragment>
                {tableContent}
                <Text
                    center
                    style={{
                        fontSize: 16,
                        letterSpacing: '-0.5px',
                        marginBottom: 15,
                        marginTop: 40
                    }}
                >
                    <strong>See more info on your Creator Dashboard.</strong>
                </Text>
                <Button
                    href="https://audiomack.com/dashboard"
                    style={{
                        width: 200,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                        marginBottom: 35,
                        paddingTop: 16,
                        paddingBottom: 16,
                        textAlign: 'center',
                        display: 'block'
                    }}
                >
                    View More
                </Button>
            </Fragment>
        );
    }

    renderWorldPosts() {
        const { blogPosts } = this.props;

        if (!blogPosts || !blogPosts.length) {
            return null;
        }

        const posts = blogPosts.map((post, i) => {
            return (
                <TableCell
                    key={`post-${i}`}
                    style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingBottom: 30,
                        width: '50%',
                        verticalAlign: 'initial'
                    }}
                >
                    <a href={post.url} target="_blank" rel="nofollow noopener">
                        <Image src={post.feature_image} />
                    </a>
                    <Text
                        style={{
                            marginTop: 5,
                            marginBottom: 10,
                            fontSize: 16,
                            letterSpacing: '-0.45px',
                            lineHeight: 1.2
                        }}
                    >
                        <strong>{post.title}</strong>
                    </Text>
                    <Text
                        style={{
                            color: '#777',
                            fontSize: 13,
                            lineHeight: 1.3,
                            margin: 0
                        }}
                    >
                        {post.custom_excerpt}
                    </Text>
                </TableCell>
            );
        });

        return (
            <Fragment>
                <Text
                    style={{
                        padding: '0 10px'
                    }}
                >
                    <strong>Audiomack World</strong>
                </Text>
                <Table>
                    <tr
                        style={{
                            verticalAlign: 'top'
                        }}
                    >
                        {posts}
                    </tr>
                </Table>
                <Button
                    href="https://ghost.audiomack.com"
                    style={{
                        width: 200,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                        marginTop: 35,
                        marginBottom: 35,
                        paddingTop: 16,
                        paddingBottom: 16,
                        textAlign: 'center',
                        display: 'block'
                    }}
                >
                    Read More
                </Button>
            </Fragment>
        );
    }

    render() {
        const { stats } = this.props;
        const { artist_image, artist_name } = stats;
        const unsubscribeText = (
            <span
                style={{
                    display: 'block',
                    color: '#ffffff',
                    marginTop: 35,
                    marginBottom: 35,
                    paddingLeft: 54,
                    paddingRight: 54,
                    fontSize: 13,
                    fontWeight: 600,
                    fontStretch: 'normal',
                    fontStyle: 'normal',
                    lineHeight: 1.54,
                    letterSpacing: -0.5,
                    textAlign: 'center'
                }}
            >
                You are receiving this email because your account applied for
                authentication on Audiomack. To unsubscribe, change your{' '}
                <Link
                    href={`${process.env.AM_URL}/edit/profile/notifications}`}
                    style={{ textDecoration: 'underline' }}
                >
                    {' '}
                    notification preferences here.
                </Link>
            </span>
        );

        return (
            <EmailTemplate
                darkHeaderProfileImage
                renderSimpleFooter
                unsubscribeText={unsubscribeText}
                profileImage={artist_image}
                style={{
                    background: '#f9f9f9 !important',
                    border: 'solid 1px #eaeaea'
                }}
            >
                <Box
                    style={{
                        padding: '0 !important'
                    }}
                    innerStyle={{
                        padding: '0 30px 16px',
                        background: '#f9f9f9 !important'
                    }}
                >
                    <Text
                        center
                        style={{
                            margin: '6px auto 0 auto !important',
                            fontSize: 18,
                            fontWeight: 'normal',
                            fontStretch: 'normal',
                            fontStyle: 'normal',
                            lineHeight: 1.56,
                            letterSpacing: -0.78,
                            textAlign: 'center',
                            color: '#272727',
                            maxWidth: 380
                        }}
                    >
                        Hey{' '}
                        <span style={{ fontWeight: 'bold', color: '#ffa200' }}>
                            {artist_name},
                        </span>{' '}
                        here's how your tracks are doing on Audiomack:
                    </Text>
                    {this.renderStats()}
                    {this.renderTopSongs()}
                    {this.renderWorldPosts()}
                </Box>
            </EmailTemplate>
        );
    }
}
