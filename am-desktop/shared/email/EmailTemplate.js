import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Image from './elements/Image';
import Text from './elements/Text';
import Link from './elements/Link';

export default class EmailTemplate extends Component {
    static propTypes = {
        preHeaderText: PropTypes.string,
        darkHeader: PropTypes.bool,
        renderSimpleFooter: PropTypes.bool,
        unsubscribeHash: PropTypes.string,
        unsubscribeType: PropTypes.string,
        children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
        unsubscribeText: PropTypes.object,
        darkHeaderProfileImage: PropTypes.bool,
        profileImage: PropTypes.string,
        style: PropTypes.object
    };

    static defaultProps = {
        renderSimpleFooter: false,
        style: {}
    };

    // In some email clients, like gmail, an email looks like this
    //
    // {fromName}       {subject} - {bodyText}
    //
    // The preHeader text is a way to precede the content of the body with some
    // text that we may want to entice the user with.

    renderPreHeader(text) {
        if (!text) {
            return null;
        }

        const style = {
            display: 'none !important',
            visibility: 'hidden !important',
            maxHeight: '0px !important'
        };

        return (
            <span className="preheader" style={style}>
                {text}
            </span>
        );
    }

    renderLogoImage() {
        if (this.props.darkHeader) {
            return (
                <tr>
                    <td
                        align="center"
                        style={{ paddingLeft: 10, paddingRight: 10 }}
                    >
                        <Text
                            center
                            style={{
                                margin: 0,
                                background: '#000000',
                                paddingTop: 35,
                                paddingBottom: 45
                            }}
                        >
                            <Link
                                href={process.env.AM_URL}
                                style={{
                                    display: 'block'
                                }}
                            >
                                <Image
                                    src="/static/email/logo-white.png"
                                    style={{
                                        width: '150px'
                                    }}
                                />
                            </Link>
                        </Text>
                    </td>
                </tr>
            );
        }

        if (this.props.darkHeaderProfileImage) {
            return (
                <Fragment>
                    <tr>
                        <td align="center">
                            <Text
                                center
                                style={{
                                    margin: 0,
                                    background: '#000000',
                                    paddingTop: 34,
                                    paddingBottom: 20
                                }}
                            >
                                <Link
                                    href={process.env.AM_URL}
                                    style={{
                                        display: 'block'
                                    }}
                                >
                                    <Image
                                        src="/static/email/logo-white.png"
                                        style={{
                                            width: '150px'
                                        }}
                                    />
                                </Link>
                            </Text>
                        </td>
                    </tr>
                    <tr>
                        <td
                            style={{
                                backgroundImage: `url("${
                                    process.env.AM_URL
                                }/static/email/icons/black-cutout.png")`,
                                backgroundRepeat: 'repeat-x',
                                backgroundSize: '21px 21px'
                            }}
                        >
                            <div style={{ textAlign: 'center' }}>
                                <Image
                                    style={{
                                        borderRadius: '50%',
                                        height: '100px',
                                        width: '100px',
                                        display: 'block',
                                        marginLeft: 'auto',
                                        marginRight: 'auto',
                                        border: '#f5f5f5 1px solid'
                                    }}
                                    src={
                                        this.props.profileImage
                                            ? this.props.profileImage
                                            : `${
                                                  process.env.ASSETS_URL
                                              }/_default/default-artist-image.png`
                                    }
                                    alt="Profile pic"
                                />
                            </div>
                        </td>
                    </tr>
                </Fragment>
            );
        }

        return (
            <tr>
                <td align="center">
                    <Text center>
                        <Link
                            href={process.env.AM_URL}
                            style={{
                                margin: '16px 0',
                                display: 'block'
                            }}
                        >
                            <Image
                                src="/static/email/logo.png"
                                style={{
                                    width: '150px'
                                }}
                            />
                        </Link>
                    </Text>
                </td>
            </tr>
        );
    }

    renderFooter() {
        const inheritLinkStyle = {
            color: 'inherit'
        };
        const legalLinkStyle = {
            ...inheritLinkStyle,
            margin: '0 0.5em',
            fontWeight: 'bold'
        };
        const appLinkStyle = {
            width: '100px',
            margin: '0.5em'
        };

        let unsubscribeLink = null;
        if (this.props.unsubscribeHash) {
            unsubscribeLink = (
                <tr>
                    <td>
                        <Text
                            small
                            center
                            style={{
                                color: '#a7a7a7',
                                margin: 0
                            }}
                        >
                            <Link href="https://audiomack.com/edit/profile/notifications">
                                Unsubscribe from all{' '}
                                {this.props.unsubscribeType}s
                            </Link>
                        </Text>
                    </td>
                </tr>
            );
        }

        if (this.props.renderSimpleFooter) {
            return (
                <tfoot>
                    <tr>
                        <td>
                            <Text
                                center
                                style={{
                                    padding: '9px 0 35px',
                                    backgroundColor: '#000',
                                    margin: this.props.darkHeaderProfileImage
                                        ? 0
                                        : '0 10px'
                                }}
                            >
                                {this.props.unsubscribeText
                                    ? this.props.unsubscribeText
                                    : null}
                                <Link
                                    href="https://itunes.apple.com/us/app/audiomack/id921765888?ls=1&mt=8"
                                    style={{ display: 'inline-block' }}
                                >
                                    <Image
                                        src="/static/images/desktop/app-store-badge.png"
                                        src2x="/static/images/desktop/app-store-badge@2x.png"
                                        alt="Download the iOS app"
                                        style={{
                                            ...appLinkStyle,
                                            border: '1px solid #333333',
                                            borderRadius: 5,
                                            width: 120
                                        }}
                                    />
                                </Link>
                                <Link
                                    href="https://play.google.com/store/apps/details?id=com.audiomack"
                                    style={{ display: 'inline-block' }}
                                >
                                    <Image
                                        src="/static/images/desktop/google-play-badge.png"
                                        src2x="/static/images/desktop/google-play-badge@2x.png"
                                        alt="or the Android app"
                                        style={{
                                            ...appLinkStyle,
                                            border: '1px solid #333333',
                                            borderRadius: 5,
                                            width: 120
                                        }}
                                    />
                                </Link>
                            </Text>
                        </td>
                    </tr>
                    {unsubscribeLink}
                </tfoot>
            );
        }

        return (
            <tfoot>
                <tr>
                    <td>
                        <Text
                            small
                            center
                            style={{
                                color: '#a7a7a7',
                                margin: '16px 0 0',
                                fontWeight: 'bold'
                            }}
                        >
                            <span style={{ display: 'block' }}>
                                Get the Audiomack app to keep your music with
                                you on the go.
                            </span>
                            <Link href="https://itunes.apple.com/us/app/audiomack/id921765888?ls=1&mt=8">
                                <Image
                                    src="/static/images/desktop/app-store-badge.png"
                                    src2x="/static/images/desktop/app-store-badge@2x.png"
                                    alt="Download the iOS app"
                                    style={appLinkStyle}
                                />
                            </Link>
                            <Link href="https://play.google.com/store/apps/details?id=com.audiomack">
                                <Image
                                    src="/static/images/desktop/google-play-badge.png"
                                    src2x="/static/images/desktop/google-play-badge@2x.png"
                                    alt="or the Android app"
                                    style={appLinkStyle}
                                />
                            </Link>
                        </Text>
                        <Text
                            small
                            center
                            style={{
                                color: '#a7a7a7',
                                fontWeight: 'bold',
                                margin: 0
                            }}
                        >
                            <Link
                                style={legalLinkStyle}
                                href={`${
                                    process.env.AM_URL
                                }/about/privacy-policy`}
                            >
                                Privacy Policy
                            </Link>{' '}
                            •{' '}
                            <Link
                                style={legalLinkStyle}
                                href={`${
                                    process.env.AM_URL
                                }/about/terms-of-service`}
                            >
                                Terms of Service
                            </Link>
                        </Text>
                        <Text
                            small
                            center
                            style={{
                                color: '#a7a7a7',
                                lineHeight: 1.2,
                                margin: '10px 0'
                            }}
                        >
                            © {new Date().getFullYear()} Audiomack, Inc
                            <br />
                            <Link
                                style={inheritLinkStyle}
                                href="https://www.google.com/maps/place/Audiomack+Studios/@40.726562,-73.99533,15z/data=!4m5!3m4!1s0x0:0xea70e3c7ea3e669c!8m2!3d40.726562!4d-73.99533?sa=X&sqi=2&ved=0ahUKEwi46qGbm43aAhWHVt8KHQ_HB_UQ_BII0gEwDw"
                            >
                                648 Broadway
                                <br />
                                New York, NY 10012
                            </Link>{' '}
                            |{' '}
                            <Link
                                style={inheritLinkStyle}
                                href={process.env.AM_URL}
                            >
                                audiomack.com
                            </Link>
                        </Text>
                    </td>
                </tr>
                {unsubscribeLink}
            </tfoot>
        );
    }

    render() {
        const { preHeaderText, style } = this.props;

        return (
            <Fragment>
                {this.renderPreHeader(preHeaderText)}
                <link
                    rel="stylesheet"
                    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800"
                />
                <center>
                    <table
                        align="center"
                        border="0"
                        cellPadding="0"
                        cellSpacing="0"
                        height="100%"
                        width="100%"
                        bgcolor="#f4f4f4"
                        style={{
                            fontFamily:
                                "'Open Sans', Helvetica, Arial, sans-serif !important"
                        }}
                    >
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table
                                        width="100%"
                                        border="0"
                                        cellPadding="0"
                                        cellSpacing="0"
                                        valign="top"
                                        style={{
                                            maxWidth: '580px',
                                            ...style
                                        }}
                                    >
                                        <tbody>
                                            {this.renderLogoImage()}
                                            <tr style={{ marginTop: '16px' }}>
                                                <td>{this.props.children}</td>
                                            </tr>
                                        </tbody>
                                        {this.renderFooter()}
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </Fragment>
        );
    }
}
