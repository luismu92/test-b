import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class DmcaNotice extends Component {
    static propTypes = {
        name: PropTypes.string,
        company: PropTypes.string,
        url: PropTypes.string,
        message: PropTypes.string
    };

    static subject(props) {
        const { name } = props;

        return `DMCA Notice - ${name}`;
    }

    static key() {
        return 'dmca-notice';
    }

    renderLine(label, data) {
        if (!data) {
            return null;
        }

        return (
            <Text>
                <strong>{label}</strong>: {data}
            </Text>
        );
    }

    render() {
        return (
            <EmailTemplate>
                <Box rounded>
                    {this.renderLine('Name', this.props.name)}
                    {this.renderLine('Company', this.props.company)}
                    {this.renderLine('URL', this.props.url)}
                    {this.renderLine('Message', this.props.message)}
                </Box>
            </EmailTemplate>
        );
    }
}
