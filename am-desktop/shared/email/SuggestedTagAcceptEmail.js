import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Link from './elements/Link';
import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class SuggestedTagAcceptEmail extends Component {
    static propTypes = {
        tag: PropTypes.string,
        musictype: PropTypes.string,
        title: PropTypes.string,
        url: PropTypes.string
    };

    static defaultProps = {
        tag: ''
    };

    static subject(props) {
        return `${props.tag} was accepted`;
    }

    static key() {
        return 'suggested-tag-accept';
    }

    render() {
        const { tag, musictype, title, url } = this.props;
        const host = process.env.AM_URL;
        let musicLink = `${host}/404`;
        let objtype = 'song';

        if (url) {
            musicLink = url;
        }

        if (musictype) {
            objtype = musictype;
        }

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>
                        The music tag, <strong>{tag}</strong>, that you
                        suggested to Audiomack for the {objtype}{' '}
                        <Link href={musicLink}>{title}</Link> has been accepted!
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
