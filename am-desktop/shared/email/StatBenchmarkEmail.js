import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { tidyNumber } from 'utils/index';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';
import Image from './elements/Image';
import Button from './elements/Button';

export default class StatBenchmarkEmail extends Component {
    static propTypes = {
        number: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        stat: PropTypes.string,
        name: PropTypes.string,
        title: PropTypes.string,
        musicImage: PropTypes.string,
        unsubscribeHash: PropTypes.string,
        unsubscribeType: PropTypes.string
    };

    static defaultProps = {
        number: '',
        stat: '',
        name: '',
        title: '',
        musicImage: 'https://assets.audiomack.com/default-album-image.png'
    };

    static subject(props) {
        return `${props.number} ${props.stat} for ${
            props.title
        } on Audiomack 📈`;
    }

    static key(props) {
        const suffix = props.stat ? `-${props.stat}` : '';

        return `stat-benchmark${suffix}`;
    }

    render() {
        const {
            number,
            stat,
            name,
            title,
            musicImage,
            unsubscribeHash,
            unsubscribeType
        } = this.props;

        return (
            <EmailTemplate
                renderSimpleFooter
                unsubscribeHash={unsubscribeHash}
                unsubscribeType={unsubscribeType}
            >
                <Box border style={{ marginTop: 16, letterSpacing: -0.5 }}>
                    <Image
                        src={musicImage}
                        style={{
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            marginBottom: '24px',
                            marginTop: '-32px',
                            display: 'block',
                            height: 351,
                            width: 280
                        }}
                    />

                    <Text center style={{ fontSize: 20, fontWeight: 600 }}>
                        <strong>Congratulations</strong>, {name}.<br />
                        You have {tidyNumber(number)} {stat.toLowerCase()} for{' '}
                        {title} on Audiomack!
                    </Text>

                    <Box
                        style={{ background: '#f3f3f3', padding: 20 }}
                        innerStyle={{ background: '#f3f3f3', padding: 0 }}
                    >
                        <Text
                            center
                            style={{
                                maxWidth: 410,
                                margin: '0 auto',
                                fontWeight: 600
                            }}
                        >
                            Share this image on your socials to let your fans
                            know where to listen.
                        </Text>
                        <Text style={{ marginBottom: 0 }} center>
                            <Button
                                href={musicImage}
                                style={{
                                    width: 150,
                                    paddingTop: 16,
                                    paddingBottom: 16
                                }}
                            >
                                Save Image
                            </Button>
                        </Text>
                    </Box>
                </Box>
            </EmailTemplate>
        );
    }
}
