import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import Link from './elements/Link';
import Button from './elements/Button';
import EmailTemplate from './EmailTemplate';

export default class OptInEmail extends Component {
    static propTypes = {
        confirmUrl: PropTypes.string
    };

    static subject() {
        return 'Action Required: Important Audiomack Updates';
    }

    static key() {
        return 'opt-in-verification';
    }

    render() {
        const { confirmUrl } = this.props;

        return (
            <EmailTemplate preHeaderText="Artists and listeners are about to get closer than ever before.">
                <Box rounded>
                    <Text>Hey there,</Text>
                    <Text>
                        Audiomack was founded to draw the shortest line between
                        artists and listeners. We’re about to roll out new,
                        one-of-a-kind methods to deliver the music you want,
                        including instant music alerts from your favorite
                        artists when they drop new music. To activate occasional
                        updates, confirm your email address by tapping the
                        button below:
                    </Text>

                    <Text center>
                        <Button href={confirmUrl}>Confirm your email</Button>
                    </Text>

                    <Text>
                        We renewed our commitment to artists last year with the{' '}
                        <Link href="https://audiomack.com/world/post/the-audiomack-artist-bill-of-rights-5d4235ba03c22706ce2c99e3">
                            Artist’s Bill of Rights
                        </Link>
                        . We’re renewing our commitment to listeners, too, with
                        changes in our Privacy Policy designed to maximize
                        transparency as delineated in the EU General Data
                        Protection Regulation, commonly referred to as GDPR.
                    </Text>

                    <Text>
                        Our{' '}
                        <Link href="https://audiomack.com/about/privacy-policy">
                            Privacy Policy
                        </Link>{' '}
                        was written to be simple, straightforward, and to give
                        users power over their personal data. It outlines the
                        information we collect about you, how we use and share
                        it, how you can access and update it, and more. We’re
                        here for you. Feel free to email us at
                        support@audiomack.com with questions.
                    </Text>

                    <Text>
                        We’re moving music forward, one listener at a time.
                    </Text>

                    <Text>Thank you,</Text>

                    <Text>Samuel A. Benjamin, Esq.</Text>

                    <Text>General Counsel at Audiomack</Text>
                </Box>
            </EmailTemplate>
        );
    }
}
