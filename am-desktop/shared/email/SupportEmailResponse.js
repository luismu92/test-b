import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class SupportEmailResponse extends Component {
    static propTypes = {
        message: PropTypes.string
    };

    static subject() {
        return 'Audiomack Support Request';
    }

    static key() {
        return 'support-ticket-thank-you';
    }

    render() {
        let originalMessage;

        if (this.props.message) {
            originalMessage = (
                <Text
                    center
                    style={{
                        background: '#f0f0f0',
                        fontStyle: 'italic',
                        fontSize: 13,
                        padding: 10
                    }}
                >
                    "{this.props.message}"
                </Text>
            );
        }
        return (
            <EmailTemplate>
                <Box rounded>
                    <Text center>
                        Thank you for your inquiry. Someone will be in touch
                        soon.
                    </Text>
                    {originalMessage}
                </Box>
            </EmailTemplate>
        );
    }
}
