import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Link from './elements/Link';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class SoundcloudCsvEmail extends Component {
    static propTypes = {
        name: PropTypes.string
    };

    static subject() {
        return 'SoundCloud CSV';
    }

    static key() {
        return 'soundcloud-csv';
    }

    render() {
        const soundcloudUrl = `https://soundcloud.com/${this.props.name}`;
        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Sup,</Text>
                    <Text>
                        Attached are the user statistics for{' '}
                        <Link href={soundcloudUrl}>{this.props.name}</Link>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
