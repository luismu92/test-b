import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class UploadSuspendedAdminEmail extends Component {
    static propTypes = {
        uploader: PropTypes.string,
        link: PropTypes.string,
        artist: PropTypes.string,
        artistUrl: PropTypes.string,
        title: PropTypes.string
    };

    static defaultProps = {
        uploader: 'an unknown user'
    };

    static subject(props) {
        return `${props.title} by ${props.artist} was suspended`;
    }

    static key() {
        return 'upload-suspended-admin';
    }

    render() {
        const { uploader, link, artistUrl } = this.props;
        const host = process.env.AM_URL;
        let musicLink = `${host}/404`;
        let artistLink = `${host}/404`;

        if (link) {
            musicLink = `${host}${link}`;
        }

        if (artistUrl) {
            artistLink = `${host}${artistUrl}`;
        }
        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>
                        The following music uploaded by {uploader} has been
                        suspended: {musicLink}
                    </Text>
                    <Text>
                        To view more uploads on this account please go here:{' '}
                        {artistLink}
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
