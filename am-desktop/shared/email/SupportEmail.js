import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class SupportEmail extends Component {
    static propTypes = {
        uploader: PropTypes.string,
        type: PropTypes.string,
        subject: PropTypes.string,
        problem: PropTypes.string,
        browserInfo: PropTypes.string,
        platform: PropTypes.string,
        name: PropTypes.string,
        email: PropTypes.string,
        url: PropTypes.string,
        description: PropTypes.string,
        company: PropTypes.string,
        localtime: PropTypes.string,
        phone: PropTypes.string,
        accountUrl: PropTypes.string,
        replyTo: PropTypes.string,
        accountEmail: PropTypes.string
    };

    static defaultProps = {
        uploader: 'an unknown user'
    };

    static subject(props) {
        return props.subject;
    }

    static key() {
        return 'support-ticket';
    }

    renderLine(label, data) {
        if (!data) {
            return null;
        }

        return (
            <Text>
                <strong>{label}</strong>: {data}
            </Text>
        );
    }

    render() {
        return (
            <EmailTemplate>
                <Box rounded>
                    {this.renderLine('Name', this.props.name)}
                    {this.renderLine('Account', this.props.accountEmail)}
                    {this.renderLine('Artist Url', this.props.accountUrl)}
                    {this.renderLine('Company', this.props.company)}
                    {this.renderLine('Phone Number', this.props.phone)}
                    {this.renderLine('Url', this.props.url)}
                    {this.renderLine('Message', this.props.description)}

                    <Text>
                        <hr />
                    </Text>

                    <Text>
                        {this.renderLine('Platform', this.props.platform)}
                    </Text>
                    <Text>
                        {this.renderLine('Reply To Email', this.props.replyTo)}
                    </Text>
                    <Text>
                        {this.renderLine(
                            'Browser Information',
                            this.props.browserInfo
                        )}
                    </Text>
                    <Text>
                        {this.renderLine('Local Time', this.props.localtime)}
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
