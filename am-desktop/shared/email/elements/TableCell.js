import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TableCell extends Component {
    static propTypes = {
        style: PropTypes.object,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ])
    };

    render() {
        const { style } = this.props;

        return <td style={style}>{this.props.children}</td>;
    }
}
