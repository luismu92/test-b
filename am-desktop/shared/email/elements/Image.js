import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { absoluteUrl } from 'utils/index';

export default class Image extends Component {
    static propTypes = {
        src: PropTypes.string.isRequired,
        src2x: PropTypes.string,
        alt: PropTypes.string,
        style: PropTypes.object
    };

    render() {
        const { src, src2x, alt, style = {} } = this.props;
        const props = {
            src: absoluteUrl(src),
            alt: alt,
            style: {
                maxWidth: '100%',
                border: 0,
                ...style
            }
        };

        if (src2x) {
            props.srcSet = `${props.src} 1x, ${absoluteUrl(src2x)} 2x`;
        }

        // eslint-disable-next-line jsx-a11y/alt-text
        return <img {...props} />;
    }
}
