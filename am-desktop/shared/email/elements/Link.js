import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { absoluteUrl } from 'utils/index';

export default class Link extends Component {
    static propTypes = {
        href: PropTypes.string.isRequired,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ]),
        style: PropTypes.object
    };

    static defaultProps = {
        style: {}
    };

    render() {
        const style = {
            fontSize: 'inherit',
            color: '#ffa200',
            textDecoration: 'none',
            ...this.props.style
        };

        return (
            <a
                style={style}
                href={absoluteUrl(this.props.href)}
                target="_blank"
                rel="noopener nofollow"
            >
                {this.props.children}
            </a>
        );
    }
}
