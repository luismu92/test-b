import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Text extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ]),
        small: PropTypes.bool,
        center: PropTypes.bool,
        logoCenter: PropTypes.bool,
        style: PropTypes.object
    };

    static defaultProps = {
        style: {}
    };

    render() {
        const style = {
            fontFamily: "'Open Sans', Helvetica, Arial, sans-serif !important",
            fontSize: 14,
            color: '#222222',
            lineHeight: 1.6,
            margin: '16px 0',
            ...this.props.style
        };

        if (this.props.small) {
            style.fontSize = 10;
        }

        if (this.props.center) {
            style.textAlign = 'center';
        }

        if (this.props.logoCenter) {
            style.margin = '0 auto';
        }

        return <p style={style}>{this.props.children}</p>;
    }
}
