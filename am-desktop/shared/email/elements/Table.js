import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Table extends Component {
    static propTypes = {
        style: PropTypes.object,
        tableHead: PropTypes.object,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ])
    };

    render() {
        const style = {
            width: '100%',
            ...this.props.style
        };

        return (
            <table style={style}>
                {this.props.tableHead}
                <tbody>
                    <tr>{this.props.children}</tr>
                </tbody>
            </table>
        );
    }
}
