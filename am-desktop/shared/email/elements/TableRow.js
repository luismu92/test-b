import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TableRow extends Component {
    static propTypes = {
        style: PropTypes.object,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ])
    };

    render() {
        const { style } = this.props;

        return (
            <tr style={style}>
                <td>{this.props.children}</td>
            </tr>
        );
    }
}
