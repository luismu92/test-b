import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { humanizeNumber } from 'utils/index';

import Text from './Text';
import Image from './Image';
import Table from './Table';
import TableCell from './TableCell';

function MusicPreview({ musicItem, index }) {
    if (!musicItem) {
        return null;
    }

    const image = musicItem.image;

    const statStyle = {
        fontSize: 12,
        fontWeight: 600,
        width: '50px',
        textAlign: 'right'
    };

    return (
        <Fragment>
            <TableCell style={{ fontSize: 12, width: '75%' }}>
                <Table>
                    <TableCell style={{ width: 20 }}>
                        <Text
                            style={{
                                fontSize: 13,
                                color: '#777777',
                                marginTop: 8,
                                marginBottom: 8
                            }}
                        >
                            #{index + 1}
                        </Text>
                    </TableCell>
                    <TableCell>
                        <Image
                            src={image}
                            style={{
                                width: 28,
                                height: 28,
                                marginRight: 10
                            }}
                        />
                        <Text
                            style={{
                                display: 'inline-block',
                                maxWidth: '80%',
                                marginTop: 10,
                                marginBottom: 3
                            }}
                        >
                            <span
                                style={{
                                    fontSize: 12,
                                    display: 'block',
                                    fontWeight: 600,
                                    lineHeight: 1.2,
                                    width: '100%',
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                    whiteSpace: 'nowrap'
                                }}
                            >
                                {musicItem.title}
                            </span>
                            <span
                                style={{
                                    fontSize: 12,
                                    display: 'block',
                                    color: '#777777',
                                    lineHeight: 1.2,
                                    width: '100%',
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                    whiteSpace: 'nowrap'
                                }}
                            >
                                {musicItem.artist}
                            </span>
                        </Text>
                    </TableCell>
                </Table>
            </TableCell>
            <TableCell style={statStyle}>
                {humanizeNumber(musicItem.play)}
            </TableCell>
            <TableCell style={statStyle}>
                {humanizeNumber(musicItem.favorite)}
            </TableCell>
        </Fragment>
    );
}

MusicPreview.propTypes = {
    musicItem: PropTypes.object,
    index: PropTypes.number
};

export default MusicPreview;
