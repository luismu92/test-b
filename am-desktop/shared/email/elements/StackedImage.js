import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Image from './Image';

export default class StackedImage extends Component {
    static propTypes = {
        src: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        colorLayers: PropTypes.array,
        alt: PropTypes.string,
        widthOffset: PropTypes.number,
        heightOffset: PropTypes.number,
        style: PropTypes.object,
        imageStyle: PropTypes.object
    };

    static defaultProps = {
        colorLayers: ['#d8d8d8', '#e7e7e7'],
        widthOffset: 16,
        heightOffset: 5
    };

    render() {
        const {
            src,
            alt,
            imageStyle = {},
            width,
            height,
            style,
            colorLayers,
            widthOffset,
            heightOffset
        } = this.props;
        const stackedStyle = {
            position: 'absolute',
            top: 0,
            left: 0
        };
        const layers = colorLayers.map((color, i) => {
            const css = {
                ...stackedStyle,
                zIndex: colorLayers.length - i,
                height,
                width: width - widthOffset * (i + 1),
                top: heightOffset * (i + 1),
                backgroundColor: color,
                left: (widthOffset / 2) * (i + 1)
            };

            return <div key={i} style={css} />;
        });

        return (
            <div style={{ position: 'relative', ...style, width, height }}>
                <Image
                    src={src}
                    alt={alt}
                    style={{
                        ...imageStyle,
                        ...stackedStyle,
                        zIndex: 3,
                        width,
                        height
                    }}
                />
                {layers}
            </div>
        );
    }
}
