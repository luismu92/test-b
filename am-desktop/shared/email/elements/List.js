import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class List extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ]),
        small: PropTypes.bool,
        ul: PropTypes.bool,
        style: PropTypes.object
    };

    static defaultProps = {
        style: {}
    };

    typeList() {
        const style = {
            fontFamily: "'Open Sans', Helvetica, Arial, sans-serif !important",
            fontSize: 16,
            color: '#222222',
            lineHeight: 1.6,
            margin: '16px 0',
            ...this.props.style
        };

        if (this.props.small) {
            style.fontSize = 10;
        }

        if (this.props.ul) {
            return <ul style={style}>{this.props.children}</ul>;
        }

        return <ol style={style}>{this.props.children}</ol>;
    }

    render() {
        return this.typeList();
    }
}
