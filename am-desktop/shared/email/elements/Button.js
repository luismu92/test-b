import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { absoluteUrl } from 'utils/index';

export default class Button extends Component {
    static propTypes = {
        href: PropTypes.string.isRequired,
        style: PropTypes.object,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ])
    };

    static defaultProps = {
        href: '',
        style: {}
    };

    render() {
        const style = {
            border: 0,
            borderRadius: 25,
            background: '#ffa200',
            display: 'inline-block',
            color: '#fff',
            fontWeight: 700,
            padding: '12px 18px',
            lineHeight: 1,
            position: 'relative',
            cursor: 'pointer',
            letterSpacing: '-0.5px',
            fontSize: 14,
            textDecoration: 'none',
            textTransform: 'capitalize',
            ...this.props.style
        };

        return (
            <a style={style} href={absoluteUrl(this.props.href)}>
                {this.props.children}
            </a>
        );
    }
}
