import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Box extends Component {
    static propTypes = {
        border: PropTypes.bool,
        rounded: PropTypes.bool,
        style: PropTypes.object,
        innerStyle: PropTypes.object,
        children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    };

    static defaultProps = {
        style: {},
        innerStyle: {}
    };

    render() {
        const style = {
            width: '100%',
            padding: '0 10px',
            boxSizing: 'border-box'
        };

        const innerStyle = {
            ...style,
            background: '#fff',
            padding: '16px 32px'
        };

        if (this.props.rounded) {
            innerStyle.borderRadius = '4px';
        }

        if (this.props.border) {
            innerStyle.border = '1px solid #eaeaea';
        }

        return (
            <div style={{ ...style, ...this.props.style }}>
                <div style={{ ...innerStyle, ...this.props.innerStyle }}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
