import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ListItem extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
            PropTypes.array
        ]),
        style: PropTypes.object
    };

    static defaultProps = {
        style: {}
    };

    typeListItem() {
        const style = {
            margin: '16px 0',
            ...this.props.style
        };

        return <li style={style}>{this.props.children}</li>;
    }

    render() {
        return this.typeListItem();
    }
}
