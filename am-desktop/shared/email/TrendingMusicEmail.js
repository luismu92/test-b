import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';
import Link from './elements/Link';
import Image from './elements/Image';
import Button from './elements/Button';

export default class TrendingMusicEmail extends Component {
    static propTypes = {
        name: PropTypes.string,
        musicTitle: PropTypes.string,
        musicImage: PropTypes.string,
        musicType: PropTypes.string,
        musicUrl: PropTypes.string,
        genreName: PropTypes.string,
        genreUrl: PropTypes.string
    };

    static defaultProps = {
        name: '',
        musicTitle: '',
        musicImage: 'https://assets.audiomack.com/default-album-image.png',
        musicType: 'song',
        musicUrl: '',
        genreName: '',
        genreUrl: ''
    };

    static subject() {
        return "Congrats, You're Trending on Audiomack 📈";
    }

    static key() {
        return 'trending-music';
    }

    render() {
        const {
            name,
            musicTitle,
            musicUrl,
            genreName,
            genreUrl,
            musicImage
        } = this.props;

        // genreName is empty when its globally trended
        const genreText = genreName ? (
            <Fragment>
                in{' '}
                <Link href={`${process.env.AM_URL}${genreUrl}`}>
                    {genreName.toLowerCase()}{' '}
                </Link>
            </Fragment>
        ) : (
            ''
        );
        const buttonLabel = (
            <Fragment>
                See it trending {genreText}and check your stats to see your
                numbers going up!
            </Fragment>
        );

        return (
            <EmailTemplate renderSimpleFooter>
                <Box border style={{ marginTop: 16, letterSpacing: -0.5 }}>
                    <Image
                        src={musicImage}
                        style={{
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            marginBottom: '24px',
                            marginTop: '-32px',
                            display: 'block',
                            height: 280,
                            width: 280
                        }}
                    />

                    <Text
                        center
                        style={{
                            fontSize: 20,
                            fontWeight: 600,
                            lineHeight: 1.4
                        }}
                    >
                        <strong>Congratulations</strong>, {name}.<br />
                        <Link href={`${process.env.AM_URL}${musicUrl}`}>
                            {musicTitle}
                        </Link>{' '}
                        is now <strong>TRENDING</strong> on Audiomack!
                    </Text>
                    <Text center>
                        We've heard your music and want the world to hear it
                        too.
                    </Text>
                    <Box
                        style={{ background: '#f3f3f3', padding: 20 }}
                        innerStyle={{ background: '#f3f3f3', padding: 0 }}
                    >
                        <Text
                            center
                            style={{
                                maxWidth: 410,
                                margin: '0 auto',
                                fontWeight: 600
                            }}
                        >
                            Share this image on your socials to let your fans
                            know where to listen.
                        </Text>
                        <Text style={{ marginBottom: 0 }} center>
                            <Button
                                href={musicImage}
                                style={{
                                    width: 150,
                                    paddingTop: 16,
                                    paddingBottom: 16
                                }}
                            >
                                Save Image
                            </Button>
                        </Text>
                    </Box>

                    <Text
                        center
                        style={{
                            fontSize: 16,
                            margin: '2em auto 0',
                            maxWidth: 380,
                            lineHeight: 1.3
                        }}
                    >
                        <strong>{buttonLabel}</strong>
                    </Text>
                    <Text center>
                        <Link href={`${process.env.AM_URL}${musicUrl}`}>
                            <strong>View Stats</strong>
                        </Link>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
