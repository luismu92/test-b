import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import Button from './elements/Button';
import EmailTemplate from './EmailTemplate';

export default class ForgotPasswordEmail extends Component {
    static propTypes = {
        name: PropTypes.string,
        token: PropTypes.string
    };

    static defaultProps = {
        name: 'there'
    };

    static subject() {
        return 'Reset your Audiomack password';
    }

    static key() {
        return 'forgot-password';
    }

    render() {
        const { name, token } = this.props;
        const host = process.env.AM_URL;
        let confirmLink = `${host}/404`;

        if (token) {
            confirmLink = host;

            confirmLink = `${confirmLink}?verifyPasswordToken=${encodeURIComponent(
                token
            )}`;
        }

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hey {name},</Text>
                    <Text>
                        We all know how frustrating it can be to forget your
                        password.
                    </Text>
                    <Text>
                        There's no need to worry though. We've generated a
                        temporary link below for you to get started with a new
                        password.
                    </Text>

                    <Text center>
                        <Button href={confirmLink}>
                            Choose a new password
                        </Button>
                    </Text>

                    <Text>
                        You can also copy and paste the link below into your
                        browser:
                        <br />
                        {confirmLink}
                    </Text>

                    <Text>
                        If you didn't mean to reset your password, then you can
                        just ignore this email and your password will remain
                        unchanged.
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
