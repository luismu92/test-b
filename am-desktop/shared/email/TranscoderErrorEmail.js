import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import Link from './elements/Link';
import EmailTemplate from './EmailTemplate';

export default class TranscoderErrorEmail extends Component {
    static propTypes = {
        artistName: PropTypes.string,
        musicTitle: PropTypes.string,
        musicType: PropTypes.string,
        musicSlug: PropTypes.string,
        uploaderSlug: PropTypes.string
    };

    static defaultProps = {
        uploader: 'there'
    };

    static subject(props) {
        return `Audiomack - ${props.artistName} - ${
            props.musicTitle
        } failed to process`;
    }

    static key() {
        return 'transcoder-error';
    }

    render() {
        const { musicType, uploaderSlug, musicSlug } = this.props;
        const host = process.env.AM_URL;
        const musicLink = `${host}/${musicType}/${uploaderSlug}/${musicSlug}`;

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>
                        Unfortunately, we could not process the audio file you
                        uploaded for track "
                        <Link href={musicLink}>{this.props.musicTitle}</Link>."
                        Please try uploading an alternative audio file.
                    </Text>
                    <Text>
                        If this problem persists, please contact us at
                        support@audiomack.com and we will do our best to help
                        resolve the issue.
                    </Text>
                    <Text>Thank you for using Audiomack,</Text>
                    <Text>- Audiomack Support</Text>
                </Box>
            </EmailTemplate>
        );
    }
}
