import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';

export default class UploadSuspendedEmail extends Component {
    static propTypes = {
        uploader: PropTypes.string,
        link: PropTypes.string,
        artist: PropTypes.string,
        title: PropTypes.string
    };

    static defaultProps = {
        uploader: 'there'
    };

    static subject(props) {
        return `${props.title} by ${props.artist} was suspended`;
    }

    static key() {
        return 'upload-suspended';
    }

    render() {
        const { uploader, link } = this.props;
        const host = process.env.AM_URL;
        let musicLink = `${host}/404`;

        if (link) {
            musicLink = `${host}${link}`;
        }

        return (
            <EmailTemplate>
                <Box rounded>
                    <Text>Hi {uploader},</Text>

                    <Text>
                        The following sound you uploaded to Audiomack has been
                        suspended: {musicLink}. It was suspended due to
                        suspected copyrighted infringement or because it
                        violated our terms of service. It will be reviewed by
                        our compliance team within the next 48 hours at which
                        time it will either be reinstated or removed from
                        Audiomack.
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
