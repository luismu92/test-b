import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Box from './elements/Box';
import Text from './elements/Text';
import EmailTemplate from './EmailTemplate';
import Image from './elements/Image';
import Button from './elements/Button';
import Link from './elements/Link';

export default class AddToVerifiedPlaylist extends Component {
    static propTypes = {
        name: PropTypes.string,
        title: PropTypes.string,
        image: PropTypes.string,
        link: PropTypes.string,
        songName: PropTypes.string,
        musicUrl: PropTypes.string
    };

    static defaultProps = {
        name: '',
        title: '',
        image: 'https://assets.audiomack.com/default-playlist-image.png',
        link: '',
        songName: '',
        musicUrl: ''
    };

    static subject() {
        return 'You Have Been Added to a Verified Playlist 📈';
    }

    static key() {
        return 'add-to-verified-playlist';
    }

    render() {
        const { name, title, link, image, songName, musicUrl } = this.props;

        return (
            <EmailTemplate renderSimpleFooter>
                <Box border style={{ marginTop: 16 }}>
                    <Image
                        src={image}
                        style={{
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            marginBottom: '10px',
                            display: 'block',
                            height: 240,
                            width: 240
                        }}
                    />

                    <Text center style={{ fontSize: 18 }}>
                        Congratulations, {name}!<br />
                        <Link href={`${process.env.AM_URL}${musicUrl}`}>
                            {songName}
                        </Link>{' '}
                        has been added to the verified playlist {title}.
                    </Text>

                    <Text center style={{ maxWidth: 410, margin: '0 auto' }}>
                        Our verified tastemakers and artists select the best
                        breaking music on the platform and your music has now
                        joined its ranks.
                        <br />
                        Check out your song live on the playlist now.
                    </Text>
                    <Text center>
                        <Button
                            href={`${process.env.AM_URL}${link}`}
                            style={{
                                width: 200,
                                paddingTop: 16,
                                paddingBottom: 16
                            }}
                        >
                            View Playlist
                        </Button>
                    </Text>
                </Box>
            </EmailTemplate>
        );
    }
}
