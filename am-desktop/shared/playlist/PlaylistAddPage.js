import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fuzzysearch from 'fuzzysearch';

import { yesBool } from 'utils/index';
import { allGenresMap as genreMap } from 'constants/index';

import FeedBar from '../widgets/FeedBar';
import AndroidLoader from '../loaders/AndroidLoader';
import DotsLoader from '../loaders/DotsLoader';
import PlaylistIcon from '../icons/playlist';
import PlusIcon from '../icons/plus';
import SearchIcon from '../icons/search';
import Select from '../components/Select';

export default class PlaylistAddPage extends Component {
    static propTypes = {
        createFormOpen: PropTypes.bool,
        editFieldValues: PropTypes.object,
        playlist: PropTypes.object,
        currentUser: PropTypes.object,
        musicBrowsePlaylist: PropTypes.object,
        onPlaylistSelect: PropTypes.func,
        onCreateNewClick: PropTypes.func,
        onEditFieldInput: PropTypes.func,
        onListScroll: PropTypes.func,
        onTabSwitch: PropTypes.func,
        search: PropTypes.string,
        tab: PropTypes.string,
        onSearchInput: PropTypes.func,
        onLoadMoreClick: PropTypes.func,
        onCreateFormToggle: PropTypes.func,
        currentUserPlaylists: PropTypes.object
    };

    static defaultProps = {
        onPlaylistSelect() {},
        onListScroll() {},
        onEditFieldInput() {},
        onCreateNewClick() {}
    };

    /////////////////////
    // Helper methods  //
    /////////////////////

    getActiveList(tab) {
        switch (tab) {
            case 'browse':
                return {
                    list: this.props.musicBrowsePlaylist.list.reduce(
                        (acc, listObj) => {
                            (listObj.list || []).forEach((item) => {
                                if (!acc.find((n) => n.id === item.id)) {
                                    acc.push(item);
                                }
                            });

                            return acc;
                        },
                        []
                    ),
                    onLastPage: true,
                    loading: this.props.musicBrowsePlaylist.loading
                };

            case 'recent':
                return {
                    list: this.props.playlist.mixedList,
                    loading: this.props.playlist.mixedLoading,
                    onLastPage: true
                };

            default:
                return this.props.currentUserPlaylists;
        }
    }

    listPlaylists(listObj, songIdToPlaylistIds, playlistModule, search) {
        const songToAdd = playlistModule.songToAdd || {};
        const selectedPlaylistIds = songIdToPlaylistIds[songToAdd.id] || [];

        let filteredList = listObj.list;

        if (search) {
            filteredList = filteredList.filter((playlist) => {
                return fuzzysearch(
                    search.toLowerCase(),
                    playlist.title.toLowerCase()
                );
            });
        }

        const listItems = filteredList.map((playlist, i) => {
            const checked = selectedPlaylistIds.indexOf(playlist.id) !== -1;

            return (
                <li className="playlist-add-item" key={i}>
                    <label className="media-item media-item--center">
                        <div className="media-item__figure">
                            <img src={playlist.image} alt="" />
                        </div>
                        <div className="media-item__body playlist-add-item__body">
                            <div className="">
                                <strong className="u-d-block playlist-add-item__title">
                                    {playlist.title}
                                </strong>
                                <span className="playlist-add-item__count">
                                    ({playlist.track_count}{' '}
                                    {playlist.track_count === 1
                                        ? 'song'
                                        : 'songs'}
                                    )
                                </span>
                            </div>
                            <input
                                checked={checked}
                                className="plus plus--minimal"
                                type="checkbox"
                                onChange={this.props.onPlaylistSelect}
                                data-id={playlist.id}
                            />
                        </div>
                    </label>
                </li>
            );
        });

        let loader;

        if (listObj.loading) {
            loader = (
                <div className="u-text-center" key="loader">
                    <AndroidLoader size={34} className="list__loader" />
                </div>
            );
        }

        let loadMore;

        if (search && !listObj.onLastPage && !listObj.loading) {
            loadMore = (
                <p className="u-text-center" key="load-more">
                    <button
                        className="button button--pill"
                        onClick={this.props.onLoadMoreClick}
                    >
                        Load more playlists
                    </button>
                </p>
            );
        }

        return (
            <ul
                className="playlist-add-list u-scrollable u-scrollable--fade-in"
                onScroll={this.props.onListScroll}
            >
                {listItems}
                {loader}
                {loadMore}
            </ul>
        );
    }

    renderCreateForm(visible) {
        if (!visible) {
            return null;
        }
        const { playlist, editFieldValues } = this.props;
        const { loading, songToAdd } = playlist;

        const options = Object.keys(genreMap).map((key) => {
            return {
                value: key,
                text: genreMap[key]
            };
        });

        let saveButton;

        if (songToAdd) {
            saveButton = (
                <button
                    type="submit"
                    className="button button--lg button--pill playlist-add__button"
                    disabled={
                        loading ||
                        !(editFieldValues.title && editFieldValues.genre)
                    }
                >
                    {loading ? <DotsLoader active={loading} /> : 'Create'}
                </button>
            );
        }

        return (
            <form
                className="playlist-add-form"
                onSubmit={this.props.onCreateNewClick}
            >
                <div className="playlist-add-form__header">
                    <div className="playlist-add-form__name">
                        <input
                            type="text"
                            name="title"
                            id="title"
                            placeholder="Enter playlist name"
                            defaultValue={editFieldValues.title}
                            onChange={this.props.onEditFieldInput}
                        />
                    </div>
                    <div className="playlist-add-form__genre">
                        <Select
                            name="genre"
                            onChange={this.props.onEditFieldInput}
                            value={editFieldValues.genre}
                            id="genre"
                            options={options}
                        />
                    </div>
                </div>
                <div className="playlist-add-form__footer">
                    <div className="playlist-add-form__privacy">
                        <span className="playlist-add-form__privacy-title">
                            Playlist will be:
                        </span>
                        <label className="playlist-add-form__privacy-label">
                            <input
                                type="radio"
                                className="radio--small"
                                name="private"
                                value="yes"
                                checked={yesBool(editFieldValues.private)}
                                onChange={this.props.onEditFieldInput}
                            />
                            <span>Private</span>
                        </label>
                        <label className="playlist-add-form__privacy-label">
                            <input
                                type="radio"
                                className="radio--small"
                                name="private"
                                value="no"
                                checked={!yesBool(editFieldValues.private)}
                                onChange={this.props.onEditFieldInput}
                            />
                            <span>Public</span>
                        </label>
                    </div>
                    {saveButton}
                </div>
            </form>
        );
    }

    render() {
        const {
            currentUserPlaylists,
            playlist,
            createFormOpen,
            search,
            onSearchInput,
            currentUser,
            tab
        } = this.props;
        const songToAdd = playlist.songToAdd || {};
        const { list: activeList, loading, onLastPage } = this.getActiveList(
            tab
        );
        let loader;

        if (loading && !activeList.length) {
            loader = <AndroidLoader size={34} className="list__loader" />;
        }

        let list;

        if (activeList.length) {
            list = this.listPlaylists(
                { list: activeList, loading, onLastPage },
                currentUserPlaylists.songIdToPlaylistIds,
                playlist,
                search
            );
        }

        let artist = songToAdd.artist;

        if (artist && typeof artist !== 'string') {
            artist = songToAdd.artist.name;
        }

        let searchForm;

        if (activeList.length > 5) {
            searchForm = (
                <form
                    className="search-form search-form--playlist"
                    autoComplete="off"
                    noValidate
                >
                    <span className="search-form__submit">
                        <SearchIcon />
                    </span>
                    <input
                        className="search-form__input"
                        placeholder="Search your playlists…"
                        value={search}
                        onChange={onSearchInput}
                    />
                </form>
            );
        }

        let contextSwitcher;

        if (currentUser.isAdmin) {
            contextSwitcher = (
                <FeedBar
                    className="u-spacing-bottom-em"
                    activeMarker={tab}
                    items={[
                        {
                            text: 'Mine',
                            active: tab === 'mine',
                            value: 'mine'
                        },
                        {
                            text: 'Browse',
                            active: tab === 'browse',
                            value: 'browse'
                        },
                        {
                            text: 'Recent',
                            active: tab === 'recent',
                            value: 'recent'
                        }
                    ]}
                    onContextSwitch={this.props.onTabSwitch}
                    showBorderMarker
                />
            );
        }

        return (
            <div className="playlist-add">
                <h2 className="playlist-add__title u-text-center">
                    <PlaylistIcon className="u-text-icon u-text-orange playlist-add__title-icon" />
                    Add To Playlists
                </h2>

                <div className="playlist-add__create">
                    <button
                        className="playlist-add-button media-item media-item--center"
                        onClick={this.props.onCreateFormToggle}
                    >
                        <div className="media-item__figure">
                            <div className="playlist-new-thumb">
                                <div className="playlist-new-thumb__inner">
                                    <PlusIcon />
                                </div>
                            </div>
                        </div>
                        <div className="media-item__body">
                            <strong className="u-d-block playlist-add__create-title">
                                Create new playlist
                            </strong>
                            <small className="u-d-block playlist-add__create-small">
                                Click here to create a new playlist
                            </small>
                        </div>
                    </button>
                    {this.renderCreateForm(createFormOpen)}
                </div>
                {contextSwitcher}
                {searchForm}
                {loader}
                {list}
            </div>
        );
    }
}
