import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import storage, { STORAGE_RECENT_PLAYLIST_ADD } from 'utils/storage';
import analytics, { eventCategory, eventAction } from 'utils/analytics';
import connectDataFetchers from 'lib/connectDataFetchers';
import { previewFile } from 'utils/index';

import { fetchPlaylistList } from '../redux/modules/music/browsePlaylist';
import {
    createPlaylist,
    addSongToPlaylist,
    deleteSong,
    getMixedPlaylists
} from '../redux/modules/playlist';
import { getPlaylists, nextPage } from '../redux/modules/user/playlists';
import { addToast } from '../redux/modules/toastNotification';

import requireAuth from '../hoc/requireAuth';
import PlaylistAddPage from './PlaylistAddPage';

const PLAYLIST_QUERY_LIMIT = 50;

class PlaylistAddPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        playlist: PropTypes.object,
        musicBrowsePlaylist: PropTypes.object,
        currentUser: PropTypes.object,
        currentUserPlaylists: PropTypes.object,
        onCreateNewClick: PropTypes.func
    };

    //////////////////////
    // Lifeycle methods //
    //////////////////////

    constructor(props) {
        super(props);

        this.state = this.getDefaultState(props);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleListScroll = (e) => {
        const { currentUserPlaylists, dispatch } = this.props;

        if (
            currentUserPlaylists.onLastPage ||
            this.state.search ||
            this.state.tab !== 'mine'
        ) {
            return;
        }

        const scrollHeight = e.target.scrollHeight;
        const height = e.target.clientHeight;
        const scrollY = e.target.scrollTop;
        const threshold = 70;

        if (
            !currentUserPlaylists.loading &&
            scrollHeight - height - scrollY < threshold
        ) {
            dispatch(nextPage());
            dispatch(
                getPlaylists({
                    limit: PLAYLIST_QUERY_LIMIT
                })
            );
        }
    };

    handleLoadMoreClick = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(
            getPlaylists({
                limit: PLAYLIST_QUERY_LIMIT
            })
        );
    };

    handlePlaylistSelect = (e) => {
        const { dispatch, playlist, currentUserPlaylists } = this.props;
        const input = e.currentTarget;
        const justChecked = input.checked;
        const playlistId = parseInt(input.getAttribute('data-id'), 10);
        const songId = playlist.songToAdd.id;
        let artist = playlist.songToAdd.artist;

        if (typeof artist !== 'string') {
            artist = playlist.songToAdd.artist.name;
        }
        const item = `${artist} - ${playlist.songToAdd.title}`;
        const obj = {
            type: playlist.songToAdd.type,
            action: justChecked ? 'add-to-playlist' : 'delete-from-playlist',
            item
        };
        const pl = currentUserPlaylists.list.filter((p) => {
            return p.id === playlistId;
        })[0];

        if (pl) {
            obj.link = `/playlist/${pl.artist.url_slug}/${pl.url_slug}`;
        }

        if (justChecked) {
            dispatch(addSongToPlaylist(playlistId, playlist.songToAdd))
                .then(() => {
                    dispatch(addToast(obj));
                    return;
                })
                .catch(() => {
                    dispatch(
                        addToast({
                            action: 'message',
                            message: `There was an error adding "${item}" to your playlist`
                        })
                    );
                });
            return;
        }

        dispatch(deleteSong(playlistId, songId))
            .then(() => {
                dispatch(addToast(obj));
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: `There was an error removing "${item}" from your playlist`
                    })
                );
            });
    };

    handleCreateButtonClick = (e) => {
        e.preventDefault();

        const { dispatch, playlist } = this.props;
        const data = {
            ...this.state.editFieldValues
        };

        if (playlist.songToAdd) {
            data.id = playlist.songToAdd.id;
        }

        let artist = playlist.songToAdd.artist;

        if (typeof artist !== 'string') {
            artist = playlist.songToAdd.artist.name;
        }

        if (data.title && data.genre) {
            dispatch(createPlaylist(data))
                .then((action) => {
                    const pl = action.resolved;

                    dispatch(
                        addToast({
                            type: playlist.songToAdd.type,
                            action: 'create-new-playlist',
                            item: `${artist} - ${playlist.songToAdd.title}`,
                            link: `/playlist/${pl.artist.url_slug}/${
                                pl.url_slug
                            }`
                        })
                    );

                    analytics.track(
                        eventCategory.playlist,
                        {
                            eventAction: eventAction.createPlaylist
                        },
                        ['ga', 'fb']
                    );

                    return;
                })
                .catch((err) => console.error(err));
        }

        this.setState(this.getDefaultState(this.props));

        if (typeof this.props.onCreateNewClick === 'function') {
            this.props.onCreateNewClick(e);
        }
    };

    handleEditFieldInput = (e) => {
        const { name, value } = e.currentTarget;

        if (name === 'image') {
            previewFile(e.target.files[0])
                .then((result) => {
                    this.setState((prevState) => {
                        const newState = Object.assign({}, prevState);

                        newState.editFieldValues[name] = result;
                        return newState;
                    });
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    analytics.error(err);
                });
            return;
        }

        this.setState((prevState) => {
            const newState = Object.assign({}, prevState);

            newState.editFieldValues[name] = value;
            return newState;
        });
    };

    handleCreateFormToggle = () => {
        this.setState({
            createFormOpen: !this.state.createFormOpen
        });
    };

    handleSearchInput = (e) => {
        this.setState({
            search: e.target.value.trim()
        });
    };

    handleTabSwitch = (value) => {
        const { dispatch } = this.props;

        this.setState({
            tab: value
        });

        switch (value) {
            case 'browse':
                dispatch(fetchPlaylistList());
                break;

            case 'recent': {
                const parse = true;
                const ids = storage.get(STORAGE_RECENT_PLAYLIST_ADD, parse);

                if (ids && ids.length) {
                    dispatch(getMixedPlaylists(ids));
                }
                break;
            }

            default:
                break;
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    getDefaultState(props) {
        return {
            editFieldValues: {
                genre: (props.playlist.songToAdd || {}).genre
            },
            search: '',
            tab: 'mine',
            createFormOpen: false
        };
    }

    render() {
        return (
            <PlaylistAddPage
                currentUser={this.props.currentUser}
                currentUserPlaylists={this.props.currentUserPlaylists}
                musicBrowsePlaylist={this.props.musicBrowsePlaylist}
                playlist={this.props.playlist}
                editFieldValues={this.state.editFieldValues}
                createFormOpen={this.state.createFormOpen}
                search={this.state.search}
                tab={this.state.tab}
                onEditFieldInput={this.handleEditFieldInput}
                onCreateNewClick={this.handleCreateButtonClick}
                onPlaylistSelect={this.handlePlaylistSelect}
                onCreateFormToggle={this.handleCreateFormToggle}
                onLoadMoreClick={this.handleLoadMoreClick}
                onListScroll={this.handleListScroll}
                onSearchInput={this.handleSearchInput}
                onTabSwitch={this.handleTabSwitch}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        playlist: state.playlist,
        currentUserPlaylists: state.currentUserPlaylists,
        musicBrowsePlaylist: state.musicBrowsePlaylist,
        currentUser: state.currentUser
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(
    connectDataFetchers(PlaylistAddPageContainer, [
        () =>
            getPlaylists({
                limit: PLAYLIST_QUERY_LIMIT
            })
    ])
);
