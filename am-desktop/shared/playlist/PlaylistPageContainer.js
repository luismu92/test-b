import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { parse } from 'query-string';
import classnames from 'classnames';

import {
    convertTimeStringToSeconds,
    yesBool,
    clamp,
    getQueueIndexForSong
} from 'utils/index';
import MusicPageMeta from 'components/MusicPageMeta';
import connectDataFetchers from 'lib/connectDataFetchers';

import { getInfoWithSlugs, reset } from '../redux/modules/playlist';
import { getFeatured } from '../redux/modules/featured';
import { getArtist } from '../redux/modules/artist/index';
import {
    getArtistUploads,
    setPage,
    nextPage
} from '../redux/modules/artist/uploads';
import { getFeaturedPosts } from '../redux/modules/world/featured';
import { editQueue, play, seek } from '../redux/modules/player';

import AndroidLoader from '../loaders/AndroidLoader';

import ImageBanner from '../widgets/ImageBanner';
import MusicDetailContainer from '../browse/MusicDetailContainer';
import NotFound from '../NotFound';
import BlogCarousel from '../components/BlogCarousel';
import CommentsWrapper from '../components/CommentsWrapper';
import { KIND_PLAYLIST } from 'constants/comment';

import MusicPageContent from '../browse/MusicPageContent';
import MusicInfo from '../browse/MusicInfo';
import MusicAlertContainer from '../components/MusicAlertContainer';

class PlaylistPageContainer extends Component {
    static propTypes = {
        playlist: PropTypes.object,
        player: PropTypes.object,
        currentUser: PropTypes.object,
        worldFeatured: PropTypes.object,
        match: PropTypes.object,
        location: PropTypes.object,
        artistUploads: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);

        const query = parse(props.location.search);

        this._query = {
            autoplay: yesBool(query.autoplay),
            seekTo: convertTimeStringToSeconds(query.t),
            trackIndex: clamp(parseInt(query.track, 10) || 1, 1, Infinity) - 1
        };
        this._autoplayStarted = false;
    }

    componentDidUpdate(prevProps) {
        const { dispatch, match } = prevProps;
        const { params } = match;
        const { params: nextParams } = this.props.match;

        const changedArtistSlug =
            params.artistId !== nextParams.artistId && nextParams.artistId;
        const changedPlaylistSlug =
            params.playlistSlug !== nextParams.playlistSlug &&
            nextParams.playlistSlug;
        const changedUserToken =
            this.props.currentUser.token &&
            prevProps.currentUser.token !== this.props.currentUser.token;
        const prevPlaylistInfo = prevProps.playlist.data || {};
        const currentPlaylistInfo = this.props.playlist.data || {};
        const changedPlaylistId =
            prevPlaylistInfo.id !== currentPlaylistInfo.id;

        if (changedArtistSlug || changedPlaylistSlug || changedUserToken) {
            dispatch(
                getInfoWithSlugs(nextParams.artistId, nextParams.playlistSlug)
            );

            if (changedArtistSlug) {
                dispatch(getArtistUploads(nextParams.artistId, { limit: 5 }));
            }
        }

        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;

        if (
            changedPlaylistId ||
            ((autoplay || seekToTime) &&
                !this._autoplayStarted &&
                this.props.playlist.data &&
                this.props.player.queue.length)
        ) {
            this.playFromQueryString(this.props.playlist.data);
        }
    }

    componentWillUnmount() {
        const { dispatch } = this.props;

        dispatch(reset());
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoadMoreTracksClick = () => {
        const { dispatch, playlist } = this.props;

        dispatch(nextPage());
        dispatch(getArtistUploads(playlist.data.artist.url_slug, { limit: 5 }))
            .then((action) => {
                const newItems = action.resolved.results;

                if (newItems.length) {
                    const filtered = newItems.filter((upload) => {
                        return (
                            upload.type === 'song' || upload.type === 'album'
                        );
                    });

                    dispatch(editQueue(filtered, { append: true }));
                }
                return;
            })
            .catch((err) => console.error(err));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    playFromQueryString(musicItem) {
        const { dispatch, player, artistUploads } = this.props;

        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;
        const trackIndex = this._query.trackIndex;

        if ((autoplay || seekToTime) && !this._autoplayStarted) {
            this._autoplayStarted = true;

            const tracks = musicItem.tracks || [];
            const clampedTrackIndex = clamp(trackIndex, 0, tracks.length - 1);
            let queueIndex = getQueueIndexForSong(musicItem, player.queue, {
                trackIndex: clampedTrackIndex,
                currentQueueIndex: player.queueIndex
            });

            if (queueIndex === -1) {
                const uploads = artistUploads.list.filter((item) => {
                    return item.id !== musicItem.id;
                });
                const queue = [musicItem].concat(uploads);
                const { queue: newQueue } = dispatch(editQueue(queue));

                queueIndex = getQueueIndexForSong(musicItem, newQueue, {
                    trackIndex: clampedTrackIndex,
                    currentQueueIndex: player.queueIndex
                });
            }

            dispatch(play(queueIndex))
                .then(() => {
                    if (seekToTime) {
                        dispatch(seek(seekToTime));
                    }
                    return;
                })
                .catch((err) => {
                    console.error(err);
                });
        }
    }

    render() {
        const {
            playlist,
            artistUploads,
            worldFeatured,
            currentUser
        } = this.props;
        const loading = playlist.loading;

        if (loading) {
            return <AndroidLoader className="music-feed__loader" />;
        }

        if (!playlist.data) {
            // @todo maybe put a skeleton view of the music detail item here?
            if (playlist.errors.length) {
                return <NotFound errors={playlist.errors} type="playlist" />;
            }

            return null;
        }

        const ownsPage =
            currentUser.isLoggedIn &&
            currentUser.profile.id === playlist.data.artist.id;
        const banner = playlist.data.image_banner;
        const klass = classnames('music-page image-banner-page', {
            'image-banner-page--no-banner': !banner
        });

        const filteredItem = {
            ...playlist.data,
            tracks: playlist.data.tracks.filter(
                (track) => !track.geo_restricted
            )
        };

        return (
            <div className={klass}>
                <MusicPageMeta
                    musicItem={playlist.data}
                    currentUser={currentUser}
                    location={this.props.location}
                />
                <ImageBanner
                    banner={banner}
                    editLink={
                        ownsPage ? `/edit/playlist/${playlist.data.id}` : null
                    }
                />
                <div className="row u-spacing-bottom-30">
                    <div className="column small-24">
                        <MusicAlertContainer
                            dispatch={this.props.dispatch}
                            item={playlist.data}
                            currentUser={currentUser}
                        />
                    </div>
                </div>
                <div className="row">
                    <div
                        className="column small-24"
                        style={{ padding: '0 var(--contentPadding)' }}
                    >
                        <div className="row expanded column small-24">
                            <MusicDetailContainer
                                rankings={playlist.data.stats.rankings}
                                item={filteredItem}
                                musicList={[filteredItem].concat(
                                    artistUploads.list
                                )}
                                shouldFixOnScroll
                                large
                                allowZoom
                                removeHeaderLinks
                                hideInteractions
                            />
                        </div>
                    </div>
                </div>
                <MusicPageContent
                    artist={playlist.data.artist}
                    artistUploads={artistUploads}
                    item={playlist.data}
                >
                    <MusicInfo
                        currentUser={currentUser}
                        item={playlist.data}
                        artist={playlist.data.artist}
                    />
                    <CommentsWrapper
                        item={playlist.data}
                        kind={KIND_PLAYLIST}
                        id={playlist.data.id}
                        total={playlist.data.stats.comments}
                        singleCommentUuid={
                            parse(this.props.location.search).comment
                        }
                        singleCommentThread={
                            parse(this.props.location.search).thread
                        }
                    />
                </MusicPageContent>
                <BlogCarousel
                    featured={worldFeatured}
                    className="blog-carousel u-spacing-top-60"
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        playlist: state.playlist,
        artist: state.artist,
        featured: state.featured,
        artistUploads: state.artistUploads,
        currentUser: state.currentUser,
        player: state.player,
        worldFeatured: state.worldFeatured
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(PlaylistPageContainer, [
            (params) => getInfoWithSlugs(params.artistId, params.playlistSlug),
            (params) => getArtist(params.artistId),
            () => setPage(1),
            (params) => getArtistUploads(params.artistId, { limit: 5 }),
            () => getFeatured(),
            () => getFeaturedPosts()
        ])
    )
);
