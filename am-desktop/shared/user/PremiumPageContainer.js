import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import requireAuth from '../hoc/requireAuth';

import {
    getCheckoutSession,
    getSubscription,
    getCheckoutUpdateSession,
    cancelSubscription,
    reactivateSubscription
} from 'redux/modules/user/premium';
import { loadScript } from 'utils/index';
import {
    showModal,
    hideModal,
    MODAL_TYPE_CONFIRM
} from '../redux/modules/modal';
import { setPremium } from 'redux/modules/user/index';

import AndroidLoader from '../loaders/AndroidLoader';
import PremiumUserPage from './PremiumUserPage';
import PremiumPageSignup from './PremiumPageSignup';

class PremiumPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        currentUser: PropTypes.object,
        subscription: PropTypes.object
    };

    componentDidMount() {
        const { dispatch } = this.props;
        if (!window.__UA__.isHeadless) {
            loadScript('https://js.stripe.com/v3/', 'stripe')
                .then(() => {
                    return;
                })
                .catch((err) => {
                    console.log(err);
                });
        }

        // Get subscription
        dispatch(getSubscription())
            .then((result) => {
                if (
                    result.resolved !== null &&
                    (result.resolved.status === 'active' ||
                        result.resolved.status === 'trialing')
                ) {
                    dispatch(setPremium());
                }
                return;
            })
            .catch((err) => {
                console.error(err);
            });
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSignupClick = () => {
        const { dispatch } = this.props;

        dispatch(getCheckoutSession())
            .then((result) => {
                // eslint-disable-next-line no-undef
                const stripe = Stripe(process.env.STRIPE_KEY);

                stripe
                    .redirectToCheckout({
                        sessionId: result.resolved.id
                    })
                    .then(function(stripeResult) {
                        console.log('Failed to redirect', stripeResult);
                        return stripeResult;
                    })
                    .catch((err) => {
                        console.log(err);
                    });

                return result;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleUpdateClick = () => {
        const { dispatch } = this.props;

        dispatch(getCheckoutUpdateSession())
            .then((result) => {
                // eslint-disable-next-line no-undef
                const stripe = Stripe(process.env.STRIPE_KEY);

                stripe
                    .redirectToCheckout({
                        sessionId: result.resolved.id
                    })
                    .then(function(stripeResult) {
                        console.log('Failed to redirect', stripeResult);
                        return stripeResult;
                    })
                    .catch((err) => {
                        console.log(err);
                    });

                return result;
            })
            .catch((err) => {
                console.log(err);
            });
    };

    handleCancelClick = () => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Cancel Subscription',
                message:
                    'Are you sure you want to cancel your subscription? ' +
                    'Once you do that, you will lose all the premium features after the end of this billing period.',
                handleConfirm: () => {
                    this.props.dispatch(hideModal());
                    this.doCancelSubscription();
                }
            })
        );
    };

    handleReactivateClick = () => {
        const { dispatch } = this.props;
        dispatch(reactivateSubscription());
    };

    handleButtonClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');

        switch (action) {
            case 'update':
                this.handleUpdateClick();
                break;

            case 'cancel':
                this.handleCancelClick();
                break;

            case 'reactivate':
                this.handleReactivateClick();
                break;

            default:
                break;
        }
    };

    doCancelSubscription() {
        const { dispatch } = this.props;

        dispatch(cancelSubscription());
    }

    render() {
        const { currentUser } = this.props;
        const { loading, error, subscription } = this.props.subscription;

        // Still loading. Don't show anything
        let loader;
        if (loading) {
            loader = (
                <div className="u-spacing-top u-text-center">
                    <AndroidLoader />
                </div>
            );

            return <div className="u-text-center">{loader}</div>;
        }

        let errorMessage;
        if (error !== null) {
            errorMessage = error.message;
        }

        // Show existing subscription (Web and non-web)
        if (subscription) {
            return (
                <PremiumUserPage
                    currentUser={currentUser}
                    subscription={subscription}
                    onButtonClick={this.handleButtonClick}
                    errorMessage={errorMessage}
                />
            );
        }

        // Show sign up
        return (
            <PremiumPageSignup
                errorMessage={errorMessage}
                onSignupClick={this.handleSignupClick}
                trial={this.props.subscription.trial}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        subscription: state.currentUserPremium
    };
}

export default compose(
    withRouter,
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return currentUser.isLoggedIn;
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/premium';
                }
                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(PremiumPageContainer);
