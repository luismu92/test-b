import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { DEFAULT_DATE_FORMAT, currencySymbols } from 'constants/index';

import Avatar from 'components/Avatar';
import Button from 'buttons/Button';
import LinkButton from 'buttons/LinkButton';

import styles from './PremiumUserPage.module.scss';

export default function PremiumUserPage({
    currentUser,
    subscription,
    onButtonClick,
    errorMessage
}) {
    if (!subscription) {
        return null;
    }

    const cancelled = subscription.cancel_at_period_end;
    const nonWebSub =
        subscription &&
        typeof subscription.store !== 'undefined' &&
        subscription.store !== 'STRIPE';
    const store =
        subscription.store === 'PLAY_STORE'
            ? 'Apple Play Store'
            : 'Google Play';

    let subscriptionDetails = (
        <Fragment>
            <p>Your subscription is managed by {store}.</p>
            <p>Please go to your app to manage your subscription.</p>
        </Fragment>
    );

    if (!nonWebSub) {
        const since = moment(subscription.start_date * 1000).format(
            DEFAULT_DATE_FORMAT
        );
        const periodEnd = moment(subscription.current_period_end * 1000).format(
            DEFAULT_DATE_FORMAT
        );

        let activeStatus = 'Active';
        if (subscription.status === 'trialing') {
            activeStatus = 'In Trial';
        }

        let statusSub = `next billing on ${periodEnd}`;
        if (cancelled) {
            statusSub = `on ${periodEnd}`;
        }

        const currency = subscription.plan.currency.toUpperCase();

        subscriptionDetails = (
            <ul className={styles.subscription}>
                <li>
                    <strong>Subscribed since:</strong> {since}
                </li>
                <li>
                    <strong>Status:</strong>{' '}
                    <span className="u-text-orange">
                        <strong>
                            {cancelled ? 'Cancelled' : activeStatus}
                        </strong>
                    </span>{' '}
                    <span style={{ color: '#666666' }}>({statusSub})</span>
                </li>
                <li>
                    <strong>Monthly payment:</strong>{' '}
                    {currencySymbols[currency]}
                    {subscription.plan.amount / 100} {currency}
                </li>
            </ul>
        );
    }

    let buttons;

    if (!nonWebSub) {
        buttons = (
            <Fragment>
                <Button
                    width={210}
                    height={48}
                    text="Update Payment Method"
                    props={{
                        onClick: onButtonClick,
                        'data-action': 'update'
                    }}
                    style={{
                        boxShadow: '0 4px 5px rgba(0, 0, 0, 0.1)'
                    }}
                />
                <LinkButton
                    text="Cancel My Subscription"
                    props={{
                        onClick: onButtonClick,
                        'data-action': 'cancel'
                    }}
                />
            </Fragment>
        );
    }

    if (!nonWebSub && subscription.cancel_at_period_end) {
        buttons = (
            <Button
                width={210}
                height={48}
                text="Re-activate My Account"
                props={{
                    onClick: onButtonClick,
                    'data-action': 'reactivate'
                }}
                style={{
                    boxShadow: '0 4px 5px rgba(0, 0, 0, 0.1)'
                }}
            />
        );
    }

    let title = (
        <Fragment>
            <span className="u-text-orange">{currentUser.profile.name}</span>,
            you are a premium user!
        </Fragment>
    );

    if (cancelled) {
        title = 'You are no longer a premium user.';
    }

    return (
        <div className={styles.container}>
            <div className={styles.inner}>
                <div className={styles.image}>
                    <Avatar
                        image={
                            currentUser.profile.image_base ||
                            currentUser.profile.image
                        }
                        size={140}
                        rounded
                        style={{
                            marginTop: '-50px',
                            boxShadow: '0 2px 4px rgba(0, 0, 0, 0.5)'
                        }}
                    />
                </div>
                <h1 className={styles.title}>{title}</h1>
                {errorMessage}
                {subscriptionDetails}
                <div className={styles.buttons}>{buttons}</div>
                {errorMessage && (
                    <p
                        className="u-text-red u-text-center"
                        style={{ marginTop: 30 }}
                    >
                        <strong>{errorMessage}</strong>
                    </p>
                )}
            </div>
        </div>
    );
}

PremiumUserPage.propTypes = {
    currentUser: PropTypes.object,
    subscription: PropTypes.object,
    onButtonClick: PropTypes.func,
    errorMessage: PropTypes.string
};
