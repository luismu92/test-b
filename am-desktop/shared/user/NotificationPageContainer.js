import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import {
    getNotifications,
    markNotificationsAsSeen
} from '../redux/modules/user/notifications';

import MessageIcon from '../icons/message';
import NotificationIcon from '../icons/notifications';
import NotificationList from '../list/NotificationList';
import AndroidLoader from '../loaders/AndroidLoader';

import requireAuth from '../hoc/requireAuth';

class NotificationPageContainer extends Component {
    static propTypes = {
        notifications: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired,
        pagingToken: PropTypes.string,
        onLastPage: PropTypes.bool,
        dispatch: PropTypes.func
    };

    //////////////////////
    // Lifecyle methods //
    //////////////////////

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getNotifications({ pagingToken: null }));
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleNotificationLoadMoreClick = () => {
        const { dispatch, pagingToken } = this.props;

        dispatch(getNotifications({ pagingToken }));
    };

    handleMarkAllAsReadClick = () => {
        const { dispatch } = this.props;

        dispatch(markNotificationsAsSeen({ all: true }));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    render() {
        const { notifications, loading, onLastPage } = this.props;

        let loadMoreButton;

        if (!loading && !onLastPage && notifications.length) {
            loadMoreButton = (
                <button
                    className="load-more button button--pill"
                    onClick={this.handleNotificationLoadMoreClick}
                >
                    + Load More
                </button>
            );
        }

        let content;

        if (!loading && !notifications.length) {
            content = (
                <div className="notifications-content notifications-content--empty">
                    <div className="notifications-empty">
                        <div className="notifications-empty__icon">
                            <NotificationIcon />
                        </div>
                        <div className="u-text-center">
                            <p className="notifications-empty__message">
                                Notifications will appear here when Audiomack
                                users follow you and when they favorite, re-up
                                or playlist your music!
                            </p>
                        </div>
                    </div>
                </div>
            );
        } else {
            let loader;

            if (loading) {
                loader = (
                    <div className="u-spacing-top u-text-center">
                        <AndroidLoader />
                    </div>
                );
            }

            content = (
                <Fragment>
                    <NotificationList notifications={notifications} />
                    <div className="u-text-center">{loader}</div>
                </Fragment>
            );
        }

        const hasUnseenNotifications = notifications.some((notification) => {
            return !notification.seen;
        });

        return (
            <Fragment>
                <Helmet>
                    <title>Notifications on Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <div className="row">
                    <div className="column small-24 medium-14 medium-offset-5 u-spacing-top u-spacing-bottom-em u-left-right">
                        <h4>
                            <MessageIcon className="u-text-icon u-text-orange" />
                            Notifications
                        </h4>

                        {hasUnseenNotifications && (
                            <button
                                className="button--link"
                                onClick={this.handleMarkAllAsReadClick}
                            >
                                <strong>Mark all as read</strong>
                            </button>
                        )}
                    </div>
                </div>
                {content}
                <div className="u-spacing-top u-text-center">
                    {loadMoreButton}
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        notifications: state.currentUserNotifications.list,
        loading: state.currentUserNotifications.loading,
        pagingToken: state.currentUserNotifications.pagingToken,
        onLastPage: state.currentUserNotifications.onLastPage,
        currentUser: state.currentUser
    };
}

export default requireAuth(connect(mapStateToProps)(NotificationPageContainer));
