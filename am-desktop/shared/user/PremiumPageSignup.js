import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Button from '../buttons/Button';

import NoAdsIcon from 'icons/no-ads';
import PlaylistIcon from 'icons/playlist';
import HifiIcon from 'icons/hifi';
import AirplayIcon from 'icons/airplay';
import EqualizerIcon from 'icons/equalizer';
import TrialIcon from 'icons/trial';
import PhoneIcon from 'icons/phone';

import styles from './PremiumPageSignup.module.scss';

export default function PremiumPageSignup({
    errorMessage,
    onSignupClick,
    trial
}) {
    const ctaButton = (
        <Button
            onClick={onSignupClick}
            text={trial ? 'Start your free trial' : 'Sign Up'}
            width={280}
            height={64}
            style={{
                borderRadius: 100,
                fontSize: 16,
                letterSpacing: '-0.7px',
                textTransform: 'uppercase'
            }}
        />
    );

    const features = [
        {
            type: 'ads',
            icon: <NoAdsIcon />,
            title: 'An ad-free experience',
            copy: 'Unlimited playback with no ad interruptions of banners'
        },
        {
            type: 'playlists',
            icon: <PlaylistIcon />,
            title: 'Download full playlists',
            copy:
                'Save all the songs in any playlist to your device in one tap',
            appOnly: true
        },
        {
            type: 'hifi',
            icon: <HifiIcon />,
            title: 'Higher-Quality Streaming',
            copy:
                'Make sure you hear every song at the highest quality available'
        },
        {
            type: 'airplay',
            icon: <AirplayIcon />,
            title: 'Airplay to music devices',
            copy:
                'Want to play on Airplay-enabled devices? Cast right from the player'
        },
        {
            type: 'equalizer',
            icon: <EqualizerIcon />,
            title: 'Equalizer Controls',
            copy:
                'Make your music sound how you like with bass and treble customization',
            appOnly: true
        },
        {
            type: 'trial',
            icon: <TrialIcon />,
            title: 'Free 7-day Trial',
            copy:
                'Try before you buy – Get all of the above free for a week after you start.'
        }
    ].map((feature, i) => {
        const klass = classnames(styles.block, {
            [styles[feature.type]]: feature.type
        });

        return (
            <div key={`feature=${i}`} className={klass}>
                <span className={styles.icon}>{feature.icon}</span>
                <h3 className={styles.blockTitle}>{feature.title}</h3>
                <p className={styles.blockCopy}>{feature.copy}</p>
                {feature.appOnly && (
                    <p className={styles.appOnly}>
                        <span className={styles.appIcon}>
                            <PhoneIcon />
                        </span>
                        App Only
                    </p>
                )}
            </div>
        );
    });

    return (
        <div className={styles.wrap}>
            <div className={styles.container}>
                <div className={styles.hero}>
                    <div className={styles.heroContent}>
                        <h1 className={styles.title}>
                            Welcome to Audiomack Premium
                        </h1>
                        <h2 className={styles.subtitle}>
                            Remove ads, unlock special app features, and bring
                            your support of artists to the next level.
                        </h2>
                        {ctaButton}
                    </div>
                </div>
                <div className={styles.features}>{features}</div>
                <div className={styles.endButton}>
                    <Button
                        onClick={onSignupClick}
                        text={trial ? 'Start your free trial' : 'Sign Up'}
                        width={280}
                        height={64}
                        style={{
                            borderRadius: 100,
                            fontSize: 16,
                            letterSpacing: '-0.7px',
                            textTransform: 'uppercase'
                        }}
                    />
                </div>
                {errorMessage && (
                    <p
                        className="u-text-red u-text-center"
                        style={{ marginTop: 30 }}
                    >
                        <strong>{errorMessage}</strong>
                    </p>
                )}
            </div>
        </div>
    );
}

PremiumPageSignup.propTypes = {
    errorMessage: PropTypes.string,
    onSignupClick: PropTypes.func,
    trial: PropTypes.bool
};

PremiumPageSignup.defaultProps = {
    onSignupClick() {},
    errorMessage: null,
    trial: true
};
