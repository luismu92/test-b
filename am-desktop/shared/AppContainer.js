import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';
import classnames from 'classnames';
import { parse } from 'query-string';

import GlobalMeta from 'components/GlobalMeta';
import connectDataFetchers from 'lib/connectDataFetchers';
import analytics from 'utils/analytics';
import { uuid, isEmbedUrl, smoothScroll, passiveOption } from 'utils/index';
import { cookies } from 'constants/index';
import { ENVIRONMENT_DESKTOP } from 'constants/stats/environment';
import storage, { STORAGE_AUTH_USER } from 'utils/storage';

import { getUserFromToken } from './redux/modules/user';
import {
    MODAL_TYPE_CONTACT,
    MODAL_TYPE_VERIFY_HASH,
    MODAL_TYPE_VERIFY_PASSWORD_TOKEN,
    MODAL_TYPE_TEXT_ME_APP,
    MODAL_TYPE_ALERT,
    showModal,
    hideModal
} from './redux/modules/modal';
import { setLastDisplayTime } from './redux/modules/ad';
import { getPinned } from './redux/modules/user/pinned';
import { logOut } from './redux/modules/user/index';
import { setMasqueradeValue } from './redux/modules/admin/index';
import {
    showMessage,
    hideMessage,
    MESSAGE_TYPE_OFFLINE
} from './redux/modules/message';
import { addToast } from './redux/modules/toastNotification';
import { setOnlineStatus } from './redux/modules/global';
import {
    setEnvironment,
    getStatsToken,
    setReferer
} from './redux/modules/stats';
import { getGlobalMusicTags } from './redux/modules/music/tags';

import ChevronIcon from './icons/chevron';

import AlbumFinishUploadModal from './modal/AlbumFinishUploadModal';
import AlertModal from './modal/AlertModal';
import ContactModal from './modal/ContactModal';
import AddToPlaylistModal from './modal/AddToPlaylistModal';
import ConfirmModal from './modal/ConfirmModal';
import TextMeTheAppModal from './modal/TextMeTheAppModal';
import TrendingModal from './modal/TrendingModal';
import EditMusicTagsModal from './modal/EditMusicTagsModal';
import ConfirmMessage from './components/ConfirmMessage';

import PlaylistTagsModal from './modal/PlaylistTagsModal';
import ToastNotification from './widgets/ToastNotification';
import Messages from './widgets/Messages';
import HeaderContainer from './header/HeaderContainer';
import Sidebar from './sidebar/Sidebar';
import PlayerContainer from './player/PlayerContainer';
import UploaderContainer from './upload/UploaderContainer';
import ArtistPopover from './components/ArtistPopover';
import FixedAd from './ad/FixedAd';
import AndroidLoader from './loaders/AndroidLoader';
import MasqueradeWarning from './admin/MasqueradeWarning';
import OuttageWarning from './widgets/OuttageWarning';
import VideoAdContainer from './ad/VideoAdContainer';

// import './AppContainer.scss';

const SCROLL_ARROW_THRESHOLD = 1000;

class AppContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        artistPopover: PropTypes.object,
        message: PropTypes.object,
        children: PropTypes.object,
        currentUser: PropTypes.object,
        history: PropTypes.object,
        musicBrowse: PropTypes.object,
        global: PropTypes.object,
        location: PropTypes.object,
        admin: PropTypes.object,
        toastNotification: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            ApiControlPanel: null,
            AdminControlsModal: null,
            VerifyEmailModal: null,
            VerifyPasswordTokenModal: null,
            scrollArrowActive: false,
            showCookieAlert: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        const { dispatch, location, currentUser } = this.props;

        // Ignore all this setup as embeds have their own mounting setup
        if (isEmbedUrl(location.pathname)) {
            return;
        }

        const option = passiveOption();
        const query = parse(location.search);

        this.handleOnlineChange({
            type: navigator.onLine ? 'online' : 'offline'
        });
        window.addEventListener('online', this.handleOnlineChange);
        window.addEventListener('offline', this.handleOnlineChange);
        window.addEventListener('scroll', this.handleWindowScroll, option);
        window.addEventListener('storage', this.handleStorageChange, false);

        dispatch(getStatsToken(uuid()));
        dispatch(setEnvironment(ENVIRONMENT_DESKTOP));
        dispatch(getGlobalMusicTags());

        this.maybeLoadVerifyEmailModal(query.verifyHash);
        this.maybeLoadVerifyPasswordTokenModal(query.verifyPasswordToken);
        this.loadAdminModules();
        this.maybeShowCookieAlert();
        this.toggleUserClass(currentUser);

        if (query.referer) {
            const hourExpiration = 1000 * 60 * 60;
            dispatch(
                setReferer(decodeURIComponent(query.referer), hourExpiration)
            );
        }

        const isAdminOnDev =
            process.env.AUDIOMACK_DEV === 'true' && currentUser.isAdmin;

        if (process.env.NODE_ENV === 'development' || isAdminOnDev) {
            import('./widgets/ApiControlPanel')
                .then(({ default: ApiControlPanel }) => {
                    this.setState({
                        ApiControlPanel
                    });
                    return;
                })
                .catch((err) => {
                    console.error(err);
                });
        }
    }

    componentDidUpdate(prevProps) {
        const { currentUser } = this.props;
        const prevPathname = prevProps.location.pathname;
        const currentPathname = this.props.location.pathname;

        if (prevPathname !== currentPathname) {
            this.handleLocationChange(this.props);
        }

        this.checkActivePlayerClass(this.props);
        this.checkCookieClass();

        if (!prevProps.currentUser.isLoggedIn && currentUser.isLoggedIn) {
            this.loadAdminModules();
            this.toggleUserClass(currentUser);
        }

        if (prevProps.currentUser.isLoggedIn && !currentUser.isLoggedIn) {
            this.toggleUserClass(currentUser);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('online', this.handleOnlineChange);
        window.removeEventListener('offline', this.handleOnlineChange);
        window.removeEventListener('scroll', this.handleWindowScroll);
        window.removeEventListener('storage', this.handleStorageChange);
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleScrollToTopClick = () => {
        smoothScroll();
    };

    handleWindowScroll = () => {
        const { scrollArrowActive } = this.state;
        const aboveThreshold = window.pageYOffset >= SCROLL_ARROW_THRESHOLD;

        if (scrollArrowActive && !aboveThreshold) {
            this.setState({
                scrollArrowActive: false
            });
        } else if (!scrollArrowActive && aboveThreshold) {
            this.setState({
                scrollArrowActive: true
            });
        }
    };

    handleContactModalLaunch = (e) => {
        e.preventDefault();
        const page = e.currentTarget.getAttribute('data-page');
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_CONTACT, { page: page }));
    };

    handleOnlineChange = (e) => {
        const isOnline = e.type === 'online';
        const { dispatch, message, history } = this.props;

        dispatch(setOnlineStatus(isOnline));

        if (isOnline) {
            const offlineMessage = message.list.filter((obj) => {
                return obj.type === MESSAGE_TYPE_OFFLINE;
            })[0];

            if (offlineMessage && offlineMessage.data.linkToGoTo) {
                history.push(offlineMessage.data.linkToGoTo);
            }

            dispatch(hideMessage(MESSAGE_TYPE_OFFLINE));
        } else {
            dispatch(showMessage(MESSAGE_TYPE_OFFLINE));
        }
    };

    handleLocationChange = (nextProps) => {
        const nextLocation = nextProps.location;
        const { global, dispatch, location } = this.props;
        const { isOnline } = global;
        const { pathname, search } = nextLocation;
        const linkToGoTo = `${pathname}${search}`;
        const artistRegex = /^\/artist/;
        const isArtistPage = location.pathname.match(artistRegex);
        const nextPageIsArtist = nextLocation.pathname.match(artistRegex);

        if (!isOnline) {
            dispatch(
                showMessage(MESSAGE_TYPE_OFFLINE, {
                    linkToGoTo
                })
            );
        }

        if (nextProps.history.action !== 'POP') {
            if (isArtistPage && nextPageIsArtist) {
                return;
            }

            window.scrollTo(0, 0);
        }
    };

    handleTextModalClick = () => {
        const { dispatch } = this.props;

        this.loadBranchSdk()
            .then((sdk) => {
                dispatch(showModal(MODAL_TYPE_TEXT_ME_APP, { sdk }));
                return;
            })
            .catch((err) => {
                dispatch(
                    showModal(MODAL_TYPE_ALERT, {
                        title: 'Error',
                        message:
                            'Please make sure your popup blocker is turned off.'
                    })
                );
                console.error(err);
            });
    };

    handleModalClose = () => {
        const { dispatch } = this.props;

        dispatch(hideModal());
    };

    handleLogoutClick = () => {
        const { dispatch, history } = this.props;

        dispatch(logOut());
        history.push('/');
    };

    handleCookieAlertClose = () => {
        this.setState({
            showCookieAlert: false
        });

        document.body.classList.remove('has-cookie-alert');

        Cookie.set(cookies.cookieAlert, '1', {
            path: '/',
            secure:
                process.env.NODE_ENV !== 'development' &&
                process.env.AUDIOMACK_DEV !== 'true',
            sameSite: 'strict',
            expires: 365
        });
    };

    handleStorageChange = (e) => {
        if (e.key === STORAGE_AUTH_USER) {
            try {
                const parseJson = true;
                const { token, secret, expiration } = storage.get(
                    STORAGE_AUTH_USER,
                    parseJson
                );

                this.props.dispatch(
                    getUserFromToken(token, secret, expiration)
                );
            } catch (err) {
                console.warn(err);
                this.props.dispatch(logOut());
            }
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    loadSupport() {
        if (window.zE) {
            window.zE(function() {
                window.zE.activate();
            });
        }
    }

    loadAdminModules() {
        const { currentUser } = this.props;

        if (!currentUser.isLoggedIn) {
            return;
        }

        if (!currentUser.isAdmin) {
            return;
        }

        if (this.state.AdminControlsModal) {
            return;
        }

        new Promise((resolve) => {
            require.ensure([], (require) => {
                const AdminControlsModal = require('./modal/AdminControlsModal')
                    .default;

                resolve({
                    AdminControlsModal
                });
            });
        })
            .then(({ AdminControlsModal }) => {
                this.setState({
                    AdminControlsModal
                });
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    loadBranchSdk() {
        return new Promise((resolve, reject) => {
            require.ensure([], (require) => {
                const sdk = require('branch-sdk');

                sdk.init(process.env.BRANCH_KEY, (err, data) => {
                    if (err) {
                        analytics.error(err);
                        reject(err);
                        return;
                    }

                    resolve(sdk);

                    console.log(data);
                });
            });
        });
    }

    toggleUserClass(currentUser) {
        if (currentUser.isAdmin) {
            document.body.classList.add('is-admin-user');
        }

        if (currentUser.isLoggedIn) {
            document.body.classList.add('is-logged-in');
        } else {
            document.body.classList.remove('is-logged-in', 'is-admin-user');
        }
    }

    checkActivePlayerClass({ currentUser, location, player }) {
        if (location.pathname === '/' && !currentUser.isLoggedIn) {
            document.body.classList.remove('has-active-player');
            return;
        }

        if (player.currentSong) {
            document.body.classList.add('has-active-player');
            return;
        }

        document.body.classList.remove('has-active-player');
    }

    checkCookieClass() {
        if (this.state.showCookieAlert) {
            document.body.classList.add('has-cookie-alert');
            return;
        }
    }

    maybeLoadVerifyEmailModal(hash) {
        const { currentUser, dispatch } = this.props;

        if (window.location.hash === '#verifyHashSuccess') {
            dispatch(
                addToast({
                    action: 'message',
                    message: 'Thanks for verifying your email!'
                })
            );

            window.location.hash = '';
        }

        if (
            !hash ||
            (currentUser.isLoggedIn && currentUser.profile.verified_email)
        ) {
            return;
        }

        new Promise((resolve) => {
            require.ensure([], (require) => {
                const VerifyEmailModal = require('./modal/VerifyEmailModal')
                    .default;

                resolve({
                    VerifyEmailModal
                });
            });
        })
            .then(({ VerifyEmailModal }) => {
                this.setState(
                    {
                        VerifyEmailModal
                    },
                    () => {
                        dispatch(showModal(MODAL_TYPE_VERIFY_HASH, { hash }));
                    }
                );
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    maybeLoadVerifyPasswordTokenModal(token) {
        const { dispatch, currentUser } = this.props;

        if (currentUser.isLoggedIn && !token) {
            return;
        }

        if (window.location.hash === '#recoverAccountSuccess') {
            dispatch(
                addToast({
                    action: 'message',
                    message:
                        'Password updated! Please login using your new password.'
                })
            );

            window.location.hash = '';
        }

        if (!token) {
            return;
        }

        new Promise((resolve) => {
            require.ensure([], (require) => {
                const VerifyPasswordTokenModal = require('./modal/VerifyPasswordTokenModal')
                    .default;

                resolve({
                    VerifyPasswordTokenModal
                });
            });
        })
            .then(({ VerifyPasswordTokenModal }) => {
                this.setState(
                    {
                        VerifyPasswordTokenModal
                    },
                    () => {
                        dispatch(
                            showModal(MODAL_TYPE_VERIFY_PASSWORD_TOKEN, {
                                token
                            })
                        );
                    }
                );
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    maybeShowCookieAlert() {
        if (!Cookie.get(cookies.cookieAlert)) {
            this.setState({
                showCookieAlert: true
            });
        }
    }

    render() {
        const {
            musicBrowse,
            toastNotification,
            location,
            currentUser,
            message,
            admin,
            artistPopover
        } = this.props;
        const { activeContext, activeGenre, activeTimePeriod } = musicBrowse;

        if (
            isEmbedUrl(location.pathname) ||
            location.pathname === '/oauth/authenticate'
        ) {
            return this.props.children;
        }

        let {
            AdminControlsModal,
            VerifyEmailModal,
            VerifyPasswordTokenModal,
            ApiControlPanel
        } = this.state;

        if (AdminControlsModal) {
            AdminControlsModal = <AdminControlsModal />;
        }

        if (VerifyEmailModal) {
            VerifyEmailModal = <VerifyEmailModal />;
        }

        if (VerifyPasswordTokenModal) {
            VerifyPasswordTokenModal = <VerifyPasswordTokenModal />;
        }

        if (ApiControlPanel) {
            ApiControlPanel = <ApiControlPanel />;
        }

        let masqueradeWarning;

        if (admin.isMasquerading) {
            masqueradeWarning = <MasqueradeWarning />;
        }

        const showOuttageWarning = false;

        let outtageWarning;

        if (showOuttageWarning) {
            outtageWarning = <OuttageWarning />;
        }

        const scrollArrowClass = classnames('c-scroll-up', {
            'c-scroll-up--active': this.state.scrollArrowActive
        });

        const videoAd = <VideoAdContainer location={location} />;

        return (
            <Fragment>
                <GlobalMeta location={location} />
                {masqueradeWarning}
                {outtageWarning}
                <HeaderContainer />
                <ToastNotification
                    state={toastNotification}
                    currentUser={currentUser}
                />
                <Messages messages={message.list} />
                <Sidebar
                    currentUser={currentUser}
                    onLogoutClick={this.handleLogoutClick}
                    onModalClick={this.handleContactModalLaunch}
                    activeContext={activeContext}
                    activeGenre={activeGenre}
                    activeTimePeriod={activeTimePeriod}
                    onTextModalClick={this.handleTextModalClick}
                    location={location}
                />
                <div className="body-loader">
                    <AndroidLoader />
                </div>
                <div className="l-container">{this.props.children}</div>
                {ApiControlPanel}
                <button
                    className={scrollArrowClass}
                    aria-hidden={!this.state.scrollArrowActive}
                    aria-label="Go to top of page"
                    onClick={this.handleScrollToTopClick}
                >
                    <ChevronIcon className="u-chevron-icon u-chevron-icon--up" />
                </button>
                {videoAd}
                <ConfirmMessage
                    open={this.state.showCookieAlert}
                    onClose={this.handleCookieAlertClose}
                    className="c-message--cookie-alert"
                >
                    <p>
                        This site uses cookies. By using this site, you agree to
                        our{' '}
                        <Link to="/about/privacy-policy">Privacy Policy</Link>{' '}
                        and{' '}
                        <Link to="/about/terms-of-service">
                            Terms of Service
                        </Link>
                        .
                    </p>
                </ConfirmMessage>
                <FixedAd currentUser={currentUser} location={location} />
                <PlayerContainer location={location} />

                <UploaderContainer location={location} />
                <ContactModal
                    onContactLinkClick={this.handleContactModalLaunch}
                />
                <AddToPlaylistModal />
                {AdminControlsModal}
                {VerifyEmailModal}
                {VerifyPasswordTokenModal}
                <TextMeTheAppModal />
                <ConfirmModal />
                <AlertModal />
                <TrendingModal />
                <EditMusicTagsModal />
                <ArtistPopover
                    artist={artistPopover.data[artistPopover.artistSlug]}
                    loading={artistPopover.loading}
                    active={artistPopover.active}
                    x={artistPopover.x}
                    y={artistPopover.y}
                />
                <PlaylistTagsModal />
                <AlbumFinishUploadModal />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        musicBrowse: state.musicBrowse,
        playlist: state.playlist,
        admin: state.admin,
        toastNotification: state.toastNotification,
        search: state.search,
        message: state.message,
        userSearch: state.userSearch,
        player: state.player,
        currentUser: state.currentUser,
        global: state.global,
        artistPopover: state.artistPopover
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(AppContainer, [
            () => {
                if (!process.env.BROWSER) {
                    return null;
                }

                const lastAdDisplayTime = Cookie.get(cookies.lastAdDisplayTime);

                if (!lastAdDisplayTime) {
                    return null;
                }

                return setLastDisplayTime(lastAdDisplayTime);
            },
            () => {
                if (!process.env.BROWSER) {
                    return null;
                }

                const token = Cookie.get('otoken');
                const secret = Cookie.get('osecret');
                const adminToken = Cookie.get('adminosecret');
                const adminSecret = Cookie.get('adminosecret');

                if (token && secret && adminToken && adminSecret) {
                    return setMasqueradeValue(true);
                }

                return null;
            },
            (params, query, props) => {
                if (
                    props.currentUser.isLoggedIn &&
                    !query.verifyPasswordToken
                ) {
                    return getPinned();
                }

                return null;
            }
        ])
    )
);
