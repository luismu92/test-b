import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { getYoutubeIdFromLink, getVimeoIdFromLink } from 'utils/index';

export default class SongVideo extends Component {
    static propTypes = {
        song: PropTypes.object.isRequired
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    //////////////////////
    // Internal methods //
    //////////////////////
    renderYouTube(videoLink) {
        const youtubeId = getYoutubeIdFromLink(videoLink);

        if (!youtubeId) {
            return null;
        }

        const src = `//www.youtube.com/embed/${youtubeId}?feature=oembed`;

        return (
            <iframe
                title={this.props.song.title}
                width="100%"
                height="235"
                src={src}
                frameBorder="0"
                allowFullScreen
            />
        );
    }

    renderVimeo(videoLink) {
        const vimeoId = getVimeoIdFromLink(videoLink);

        if (!vimeoId) {
            return null;
        }

        const src = `//player.vimeo.com/video/${vimeoId}`;

        return (
            <iframe
                title={this.props.song.title}
                width="100%"
                height="235"
                src={src}
                frameBorder="0"
                webkitAllowFullscreen
                mozAllowFullScreen
                allowFullScreen
            />
        );
    }

    render() {
        const { song } = this.props;

        if (!song || (!!song && !song.video)) {
            return null;
        }

        const youtube = this.renderYouTube(song.video);
        let vimeo;

        if (!youtube) {
            vimeo = this.renderVimeo(song.video);
        }

        if (!vimeo && !youtube) {
            return null;
        }

        return (
            <div className="video-container">
                {youtube}
                {vimeo}
            </div>
        );
    }
}
