import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import { convertTimeStringToSeconds, yesBool } from 'utils/index';
import connectDataFetchers from 'lib/connectDataFetchers';

import { getSongInfo, reset } from '../redux/modules/music/song';
import { getArtist } from '../redux/modules/artist/index';
import {
    getArtistUploads,
    setPage,
    nextPage
} from '../redux/modules/artist/uploads';
import { fetchSongList as fetchTrending } from '../redux/modules/home/trending';
import { getFeatured } from '../redux/modules/featured';
import { editQueue, play, seek } from '../redux/modules/player';
import { getFeaturedPosts } from '../redux/modules/world/featured';

import SongPage from './SongPage';

class SongPageContainer extends Component {
    static propTypes = {
        song: PropTypes.object,
        artist: PropTypes.object,
        worldFeatured: PropTypes.object,
        artistUploads: PropTypes.object,
        currentUser: PropTypes.object,
        featured: PropTypes.object,
        homeTrending: PropTypes.object,
        match: PropTypes.object,
        player: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);

        const query = parse(props.location.search);

        this._query = {
            key: query.key,
            autoplay: yesBool(query.autoplay),
            seekTo: convertTimeStringToSeconds(query.t)
        };
        this._autoplayStarted = false;

        this.state = {
            sortTooltipActive: false
        };
    }

    componentDidMount() {
        const { song } = this.props;

        this.setSongForPage(song);
    }

    componentDidUpdate(prevProps) {
        const { dispatch, match } = prevProps;
        const { params: prevParams } = match;
        const { params: currentParams } = this.props.match;
        const prevSongInfo = prevProps.song.info || {};
        const currentSongInfo = this.props.song.info || {};

        const changedArtist =
            prevParams.artistId !== currentParams.artistId &&
            currentParams.artistId;
        const changedSong =
            prevParams.songSlug !== currentParams.songSlug &&
            currentParams.songSlug;
        const changedSongId = prevSongInfo.id !== currentSongInfo.id;
        const changedUserToken =
            this.props.currentUser.token &&
            prevProps.currentUser.token !== this.props.currentUser.token;

        if (changedArtist || changedSong || changedUserToken) {
            const { token, secret } = this.props.currentUser;

            dispatch(
                getSongInfo(currentParams.artistId, currentParams.songSlug, {
                    secret: secret,
                    token: token
                })
            );

            if (changedSong) {
                this.stopPoll();
            }

            if (changedArtist) {
                dispatch(getArtist(currentParams.artistId));
                dispatch(setPage(1));
                dispatch(
                    getArtistUploads(currentParams.artistId, { limit: 5 })
                );
            }
        }

        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;

        if (
            changedSongId ||
            ((autoplay || seekToTime) &&
                !this._autoplayStarted &&
                this.props.song.info)
        ) {
            this.setSongForPage(this.props.song);
        }
    }

    componentWillUnmount() {
        this.props.dispatch(reset());
        this.stopPoll();
        this._autoplayStarted = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLoadMoreTracksClick = () => {
        const { dispatch, artist } = this.props;

        dispatch(nextPage());
        dispatch(getArtistUploads(artist.profile.url_slug, { limit: 5 }))
            .then((action) => {
                const newItems = action.resolved.results;

                if (newItems.length) {
                    const filtered = newItems.filter((upload) => {
                        return (
                            upload.type === 'song' || upload.type === 'album'
                        );
                    });

                    dispatch(editQueue(filtered, { append: true }));
                }
                return;
            })
            .catch((err) => console.error(err));
    };

    handleCountdownFinish = () => {
        const {
            currentUser: { token, secret },
            dispatch,
            match: { params }
        } = this.props;
        const options = {};

        options.key = this._query.key;
        options.token = token;
        options.secret = secret;

        dispatch(getSongInfo(params.artistId, params.songSlug, options));
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    /**
     * Assigns the page's song to the player if not dmca'd and one doesn't
     * exist already
     * @param {object} song
     */
    setSongForPage(song) {
        const { dispatch, player, artistUploads } = this.props;

        const currentSong = player.currentSong;
        const songInfo = song.info;
        const isSuspended =
            songInfo &&
            (songInfo.status === 'suspended' || songInfo.status === 'takedown');
        const isGeoRestricted = songInfo && songInfo.geo_restricted;
        const stillProcessing =
            songInfo &&
            !isSuspended &&
            !isGeoRestricted &&
            songInfo.streaming_url === '';

        // This can happen if we just refresh a song page

        if (stillProcessing) {
            this.pollForStreamingUrl();
        } else if (
            songInfo &&
            (!currentSong || currentSong.id === songInfo.id) &&
            !isSuspended
        ) {
            const uploads = artistUploads.list.filter((item) => {
                return item.id !== songInfo.id;
            });
            const queue = [songInfo].concat(uploads);

            dispatch(
                editQueue(queue, {
                    append: !!currentSong && currentSong.id === songInfo.id
                })
            );

            this.playFromQueryString();
        }
    }

    playFromQueryString() {
        const { dispatch } = this.props;

        const autoplay = this._query.autoplay;
        const seekToTime = this._query.seekTo;

        if ((autoplay || seekToTime) && !this._autoplayStarted) {
            this._autoplayStarted = true;

            dispatch(play())
                .then(() => {
                    if (seekToTime) {
                        dispatch(seek(seekToTime));
                    }
                    return;
                })
                .catch((err) => {
                    console.error(err);
                });
        }
    }

    pollForStreamingUrl() {
        clearTimeout(this._pollTimer);
        this._pollTimer = setTimeout(() => {
            const options = {};
            const {
                currentUser: { token, secret },
                location,
                match,
                dispatch
            } = this.props;
            const query = parse(location.search) || {};

            options.key = query.key;
            options.token = token;
            options.secret = secret;

            dispatch(
                getSongInfo(
                    match.params.artistId,
                    match.params.songSlug,
                    options
                )
            )
                .then((action) => {
                    if (
                        action.resolved.results.streaming_url === '' &&
                        !action.resolved.results.geo_restricted &&
                        this._pollTimer !== null
                    ) {
                        this.pollForStreamingUrl();
                    }
                    return;
                })
                .catch((err) => {
                    console.log(err);
                });
        }, 1000);
    }

    stopPoll() {
        clearTimeout(this._pollTimer);
        this._pollTimer = null;
    }

    goToArtistPage() {
        const { history, currentUser } = this.props;

        history.push(`/artist/${currentUser.profile.url_slug}`);
    }

    render() {
        const {
            song,
            player,
            artist,
            artistUploads,
            currentUser,
            homeTrending,
            location,
            dispatch,
            featured,
            worldFeatured
        } = this.props;

        return (
            <SongPage
                song={song}
                trending={homeTrending}
                artist={artist}
                location={location}
                artistUploads={artistUploads}
                player={player}
                currentUser={currentUser}
                dispatch={dispatch}
                featured={featured}
                worldFeatured={worldFeatured}
                onLoadMoreTracksClick={this.handleLoadMoreTracksClick}
                onCountdownFinish={this.handleCountdownFinish}
                songKey={this._query.key}
                singleCommentUuid={parse(this.props.location.search).comment}
                singleCommentThread={parse(this.props.location.search).thread}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        song: state.musicSong,
        artist: state.artist,
        artistUploads: state.artistUploads,
        homeTrending: state.homeTrending,
        featured: state.featured,
        player: state.player,
        currentUser: state.currentUser,
        worldFeatured: state.worldFeatured
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(SongPageContainer, [
            (params, query, props) => {
                const options = {};
                const {
                    currentUser: { token, secret }
                } = props;

                options.key = query.key;
                options.token = token;
                options.secret = secret;

                return getSongInfo(params.artistId, params.songSlug, options);
            },
            (params) => getArtist(params.artistId),
            () => setPage(1),
            (params) => getArtistUploads(params.artistId, { limit: 5 }),
            () => getFeatured(),
            () => fetchTrending(),
            () => getFeaturedPosts()
        ])
    )
);
