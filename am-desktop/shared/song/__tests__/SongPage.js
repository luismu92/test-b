/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { StaticRouter } from 'react-router';
import Enzyme, { render } from 'enzyme';
import { Provider } from 'react-redux';

import SongPage from '../SongPage';

Enzyme.configure({ adapter: new Adapter() });

test('should render 404 page correctly', () => {
    const song = {
        loading: false,
        info: null,
        errors: ['Error']
    };
    const store = {
        dispatch() {},
        subscribe() {},
        getState() {
            return {
                musicBrowse: {
                    loading: true,
                    list: []
                },
                song
            };
        }
    };

    const component = (
        <Provider store={store}>
            <StaticRouter context={{}}>
                <SongPage song={song} />
            </StaticRouter>
        </Provider>
    );
    const wrapper = render(component);

    expect(wrapper.text()).toEqual(expect.stringMatching(/^404/));
});
