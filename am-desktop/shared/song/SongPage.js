import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import MusicPageMeta from 'components/MusicPageMeta';

import analytics, { eventLabel } from 'utils/analytics';

import MusicDetailContainer from '../browse/MusicDetailContainer';
import BlogCarousel from '../components/BlogCarousel';
import RemovedPage from '../components/RemovedPage';
import MusicAlertContainer from '../components/MusicAlertContainer';
import Tunecore from '../components/Tunecore';

import AndroidLoader from '../loaders/AndroidLoader';
import NotFound from '../NotFound';

import CommentsWrapper from '../components/CommentsWrapper';
import { KIND_SONG } from 'constants/comment';

import MusicPageContent from '../browse/MusicPageContent';
import MusicInfo from '../browse/MusicInfo';

export default class SongPage extends Component {
    static propTypes = {
        currentUser: PropTypes.object,
        song: PropTypes.object,
        worldFeatured: PropTypes.object,
        artistUploads: PropTypes.object,
        trending: PropTypes.object,
        location: PropTypes.object,
        songKey: PropTypes.string,
        onCountdownFinish: PropTypes.func,
        dispatch: PropTypes.func,
        singleCommentUuid: PropTypes.string,
        singleCommentThread: PropTypes.string
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    constructor(props) {
        super(props);

        this.state = {
            CopyrightControls: null,
            OwnerControlsContainer: null
        };
    }

    componentDidMount() {
        this.loadAdminModules()
            .then(({ CopyrightControls, OwnerControlsContainer }) => {
                this.setState({
                    CopyrightControls,
                    OwnerControlsContainer
                });
                return;
            })
            .catch((err) => {
                console.error(err);
                analytics.error(err);
            });
    }

    componentDidUpdate(prevProps) {
        const prevSong = prevProps.song.info || {};
        const currentSong = this.props.song.info || {};
        const changedSong = prevSong.id !== currentSong.id;

        if (
            changedSong &&
            currentSong.id &&
            !this.state.OwnerControlsContainer
        ) {
            this.loadAdminModules()
                .then(({ OwnerControlsContainer }) => {
                    this.setState({
                        OwnerControlsContainer
                    });
                    return;
                })
                .catch((err) => {
                    console.error(err);
                    analytics.error(err);
                });
        }
    }

    ////////////////////
    // Helper methods //
    ////////////////////

    getSubtext(description) {
        return <p>{description}</p>;
    }

    loadAdminModules() {
        const { song, currentUser } = this.props;

        return new Promise((resolve) => {
            require.ensure([], (require) => {
                let OwnerControlsContainer = null;

                if (song.info && (this.isOwner() || currentUser.isAdmin)) {
                    OwnerControlsContainer = require('../components/OwnerControlsContainer')
                        .default;
                }

                let CopyrightControls = null;

                if (currentUser.isLoggedIn && currentUser.isSuspension) {
                    CopyrightControls = require('../admin/CopyrightControls')
                        .default;
                }

                resolve({
                    OwnerControlsContainer,
                    CopyrightControls
                });
            });
        });
    }

    isOwner() {
        const { currentUser, song } = this.props;

        return (
            currentUser.isLoggedIn &&
            song.info &&
            currentUser.profile.id === song.info.uploader.id
        );
    }

    renderArtistUrl(artist) {
        if (!artist.url) {
            return null;
        }

        return (
            <div className="artist-section__info-block u-trunc">
                <span className="artist-section__info-label">URL:</span>
                <span className="artist-section__info-value">
                    <a
                        href={artist.url}
                        target="_blank"
                        rel="nofollow noopener"
                    >
                        {artist.url}
                    </a>
                </span>
            </div>
        );
    }

    renderArtistLabel(artist) {
        if (!artist.label) {
            return null;
        }

        return (
            <div className="artist-section__info-block u-trunc">
                <span className="artist-section__info-label">Label:</span>
                <span className="artist-section__info-value">
                    {artist.label}
                </span>
            </div>
        );
    }

    renderArtistBio(artist) {
        if (!artist.bio) {
            return null;
        }

        return (
            <div className="artist-section__info-block">
                <span className="artist-section__info-label">Bio:</span>
                <span className="artist-section__info-value">{artist.bio}</span>
            </div>
        );
    }

    renderArtistStats(artist) {
        if (!artist.created) {
            return null;
        }

        // Probably will use moment later
        const [, month, , year] = new Date(artist.created * 1000)
            .toDateString()
            .split(' ');
        const yearString = year.substr(-2);

        return (
            <div className="artist-section__info-block artist-section__info-block--footer">
                <div className="u-trunc">
                    <span className="artist-section__info-label">
                        Followers:
                    </span>
                    <span className="artist-section__info-value u-brand-color">
                        {(artist.followers_count || 0).toLocaleString()}
                    </span>
                </div>
                <div className="u-trunc">
                    <span className="artist-section__info-label">Uploads:</span>
                    <span className="artist-section__info-value u-brand-color">
                        <Link to={`/artist/${artist.url_slug}/uploads`}>
                            {(artist.upload_count || 0).toLocaleString()}
                        </Link>
                    </span>
                </div>
                <div className="u-trunc">
                    <span className="artist-section__info-label">
                        Member since:
                    </span>
                    <span className="artist-section__info-value u-brand-color">
                        {month} &apos;{yearString}
                    </span>
                </div>
            </div>
        );
    }

    renderTunecore(currentUser, musicItem) {
        if (this.isOwner(currentUser, musicItem)) {
            return (
                <Tunecore
                    className="u-text-center"
                    linkHref="http://www.tunecore.com/?utm_medium=d&utm_source=audiomack&utm_campaign=all_afpsp_su&utm_content=ghoagpwt"
                    eventLabel={eventLabel.songControl}
                    location="music"
                />
            );
        }

        return null;
    }

    render() {
        const {
            song,
            artistUploads,
            trending,
            worldFeatured,
            currentUser,
            singleCommentThread,
            singleCommentUuid
        } = this.props;
        const subtext = this.getSubtext(song.description);
        let { CopyrightControls, OwnerControlsContainer } = this.state;
        const stillProcessing = song.info && song.info.streaming_url === '';

        if (song.loading && (!song.info || !stillProcessing)) {
            return (
                <div>
                    <AndroidLoader className="music-feed__loader" />
                </div>
            );
        }

        if (!song.info) {
            if (song.errors.length) {
                return <NotFound errors={song.errors} type="song" />;
            }

            return null;
        }

        if (
            song.info.status === 'suspended' ||
            song.info.status === 'takedown' ||
            song.info.status === 'unplayable'
        ) {
            return (
                <RemovedPage
                    musicType="song"
                    trending={trending}
                    music={song.info}
                    dispatch={this.props.dispatch}
                    currentUser={currentUser}
                />
            );
        }

        if (CopyrightControls) {
            CopyrightControls = <CopyrightControls music={song.info} />;
        }

        if (OwnerControlsContainer) {
            OwnerControlsContainer = (
                <OwnerControlsContainer music={song.info} />
            );
        }

        const filteredArtistUploads = artistUploads.list.filter((item) => {
            return item.id !== song.info.id;
        });
        const musicList = [song.info].concat(filteredArtistUploads);

        return (
            <div className="music-page u-spacing-top-30">
                <MusicPageMeta
                    musicItem={song.info}
                    location={this.props.location}
                    currentUser={currentUser}
                />
                <div className="row u-spacing-bottom-30">
                    <div className="column small-24">
                        <MusicAlertContainer
                            dispatch={this.props.dispatch}
                            item={song.info}
                            currentUser={currentUser}
                            onCountdownFinish={this.props.onCountdownFinish}
                        />
                    </div>
                </div>
                <div className="row">
                    <div
                        className="column small-24"
                        style={{ padding: '0 var(--contentPadding)' }}
                    >
                        <div className="row expanded column small-24">
                            <MusicDetailContainer
                                item={song.info}
                                subtext={subtext}
                                musicList={musicList}
                                onCountdownFinish={this.props.onCountdownFinish}
                                songKey={this.props.songKey}
                                className="u-padding-bottom-em"
                                large
                                allowZoom
                                removeHeaderLinks
                                hideInteractions
                                shouldFixOnScroll
                                showFullGeoNotice={false}
                            />
                        </div>
                    </div>
                    <MusicPageContent
                        artist={song.info.uploader}
                        artistUploads={artistUploads}
                        item={song.info}
                    >
                        <MusicInfo
                            currentUser={currentUser}
                            item={song.info}
                            artist={song.info.uploader}
                        />
                        <CommentsWrapper
                            item={song.info}
                            kind={KIND_SONG}
                            id={song.info.id}
                            total={song.info.stats.comments}
                            singleCommentUuid={singleCommentUuid}
                            singleCommentThread={singleCommentThread}
                        />
                    </MusicPageContent>
                </div>
                {CopyrightControls}
                <BlogCarousel
                    featured={worldFeatured}
                    className="u-spacing-top-60"
                />
            </div>
        );
    }
}
