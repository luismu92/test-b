import React, { Fragment, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { randomNumberBetween } from 'utils/index';

import styles from './Starfield.module.scss';

function Starfield({ shouldShow, onStarfieldReady }) {
    const klass = classnames(styles.starfield, {
        [styles.active]: shouldShow
    });
    const containerRef = useRef(null);
    const shootingStar = useRef(null);

    useEffect(() => {
        import('utils/ShootingStar')
            .then((shootingStarModule) => {
                if (!containerRef.current) {
                    return;
                }

                onStarfieldReady();
                shootingStar.current = new shootingStarModule.default(
                    containerRef.current,
                    {
                        radiusDelta() {
                            return randomNumberBetween(0.01, 0.03, 2);
                        },
                        vy() {
                            let mutliplier = 1;

                            if (Math.random() >= 0.5) {
                                mutliplier = -1;
                            }

                            return (
                                randomNumberBetween(0.02, 0.9, 2) * mutliplier
                            );
                        },
                        vx() {
                            let mutliplier = 1;

                            if (Math.random() >= 0.5) {
                                mutliplier = -1;
                            }

                            return randomNumberBetween(2, 5) * mutliplier;
                        },
                        interval() {
                            return randomNumberBetween(2000, 6000);
                        }
                    }
                );
                return;
            })
            .catch((err) => {
                console.error(err);
            });

        return () => {
            if (shootingStar.current) {
                shootingStar.current.destroy();
            }
        };
    }, []);

    return (
        <Fragment>
            <div className={klass}>
                <div className={styles.bg} />
                <div className={styles.gas} />
            </div>
            <div className={styles.shootingStarContainer} ref={containerRef} />
        </Fragment>
    );
}

Starfield.propTypes = {
    shouldShow: PropTypes.bool,
    onStarfieldReady: PropTypes.func
};

export default Starfield;
