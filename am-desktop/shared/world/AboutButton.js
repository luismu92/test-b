import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import InfoIcon from 'icons/info2';

import styles from './AboutButton.module.scss';

function AboutButton({ hidden, onClick }) {
    const klass = classnames(styles.button, {
        [styles.buttonHidden]: hidden
    });

    return (
        <button
            className={klass}
            onClick={onClick}
            aria-hidden={hidden || null}
            tabIndex={hidden ? -1 : null}
            aria-label="Show about dialog"
        >
            <InfoIcon />
        </button>
    );
}

AboutButton.propTypes = {
    onClick: PropTypes.func,
    hidden: PropTypes.bool
};

export default AboutButton;
