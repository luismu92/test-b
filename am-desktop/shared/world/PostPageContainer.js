import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
// import classnames from 'classnames';

import connectDataFetchers from 'lib/connectDataFetchers';

import { addToast } from '../redux/modules/toastNotification';

import { getBySlug } from '../redux/modules/world/post';

import AboutModal from './AboutModal';
import PostPage from './PostPage';

class PostPageContainer extends Component {
    static propTypes = {
        worldPost: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            aboutModalActive: false
        };
    }

    handleAddedToQueue = (e) => {
        const { dispatch } = this.props;
        const button = e.currentTarget;
        const title = button.getAttribute('data-title');

        dispatch(
            addToast({
                action: 'message',
                item: `${title} was added to your queue`
            })
        );
    };

    render() {
        return (
            <Fragment>
                <PostPage
                    worldPost={this.props.worldPost}
                    history={this.props.history}
                    onAddedToQueue={this.handleAddedToQueue}
                />
                <AboutModal active={this.state.aboutModalActive} />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        worldPost: state.worldPost
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(
    connectDataFetchers(PostPageContainer, [
        (params) =>
            getBySlug(params.slug, {
                include: 'authors,tags',
                formats: ['html', 'mobiledoc']
            })
    ])
);
