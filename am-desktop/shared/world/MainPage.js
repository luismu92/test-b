import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import GlobeHeader from './GlobeHeader';
import Nav from './Nav';
import PostScrollerContainer from './PostScrollerContainer';
import BlogPostLandingMeta from 'components/BlogPostLandingMeta';

export default class MainPage extends Component {
    static propTypes = {
        worldPage: PropTypes.object,
        worldFeatured: PropTypes.object,
        worldSettings: PropTypes.object,
        match: PropTypes.object
    };

    render() {
        return (
            <Fragment>
                <BlogPostLandingMeta
                    settings={this.props.worldSettings.info}
                    match={this.props.match}
                />
                <GlobeHeader
                    match={this.props.match}
                    worldPage={this.props.worldPage}
                    worldFeatured={this.props.worldFeatured}
                />
                <div
                    className="row"
                    style={{
                        marginTop: -95,
                        maxWidth: 1090,
                        position: 'relative'
                    }}
                >
                    <div className="column">
                        <Nav worldPage={this.props.worldPage} />
                        <PostScrollerContainer match={this.props.match} />
                    </div>
                </div>
            </Fragment>
        );
    }
}
