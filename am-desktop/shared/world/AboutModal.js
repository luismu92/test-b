import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { MODAL_TYPE_ABOUT_WORLD } from '../redux/modules/modal';

import FacebookIcon from '../icons/facebook-letter-logo';
import InstagramIcon from '../icons/instagram';
import TwitterIcon from '../icons/twitter-logo';
import TwitchIcon from 'icons/twitch';
import Modal from '../modal/Modal';

import styles from './AboutModal.module.scss';

class AboutModal extends Component {
    static propTypes = {
        dispatch: PropTypes.func
    };

    render() {
        return (
            <Modal
                className={styles.modal}
                modalType={MODAL_TYPE_ABOUT_WORLD}
                onClose={this.handleClose}
            >
                <h3 className={styles.title}>About</h3>
                <p className={styles.subtitle}>
                    Since 2011, Audiomack’s mission has been{' '}
                    <strong>Moving Music Forward</strong> while fostering a
                    connection between online music discovery and the culture
                    itself. Audiomack World is an interactive experience that
                    showcases original content from around the globe. From
                    spotlighting influencers who push the boundaries of sound,
                    to pairing your favorite artists wisth a full classical
                    orchestra, Audiomack will continue driving the culture
                    forward from your city to the universe.
                </p>

                <p className={styles.socialContainer}>
                    <a
                        href="https://twitter.com/audiomack"
                        target="_blank"
                        rel="nofollow noopener"
                        className={`${styles.social} ${styles.twitter}`}
                        aria-label="Audiomack on Twitter"
                    >
                        <TwitterIcon />
                    </a>
                    <a
                        href="https://www.facebook.com/audiomack"
                        target="_blank"
                        rel="nofollow noopener"
                        className={styles.social}
                        aria-label="Audiomack on Facebook"
                    >
                        <FacebookIcon />
                    </a>
                    <a
                        href="https://www.instagram.com/audiomack"
                        target="_blank"
                        rel="nofollow noopener"
                        className={styles.social}
                        aria-label="Audiomack on Instagram"
                    >
                        <InstagramIcon />
                    </a>
                    <a
                        href="https://twitch.tv/audiomack"
                        target="_blank"
                        rel="nofollow noopener"
                        className={styles.social}
                        aria-label="Audiomack on Twich"
                    >
                        <TwitchIcon />
                    </a>
                </p>
            </Modal>
        );
    }
}

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(AboutModal);
