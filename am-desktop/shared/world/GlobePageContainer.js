import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { clamp } from 'utils/index';

import { showModal, MODAL_TYPE_ABOUT_WORLD } from '../redux/modules/modal';
import { getWorldLocations } from '../redux/modules/world/location';
import { getAllPosts } from '../redux/modules/world/globePosts';

import hideSidebarForComponent from '../hoc/hideSidebarForComponent';
import hideHeaderForComponent from 'hoc/hideHeaderForComponent';
import activateDarkThemeForComponent from '../hoc/activateDarkThemeForComponent';

import GlobePage from './GlobePage';

const MIN_ZOOM = 1;
const MAX_ZOOM = 5;
const DEFAULT_ROTATION = [100, -25];

class GlobePageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        worldPost: PropTypes.object,
        modal: PropTypes.object,
        worldGlobePosts: PropTypes.object,
        worldPostArtists: PropTypes.object,
        history: PropTypes.object,
        player: PropTypes.object,
        worldLocation: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            hasError: false,
            isPanelActive: false,
            panelCity: '',
            loadingData: true,
            globeZoom: MIN_ZOOM,
            blogDetailActive: false
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;

        // this.addOverflowClass();

        dispatch(getWorldLocations());

        window.addEventListener('keyup', this.handleKeyUp);
        document.body.classList.add('u-window-height');
    }

    componentWillUnmount() {
        document.body.classList.remove('overflow');
        window.removeEventListener('keyup', this.handleKeyUp);
        this._globeWasPlaying = null;
        this._articleTimer = null;
        this._container = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleKeyUp = (e) => {
        const esc = e.keyCode === 27;

        if (esc) {
            if (this.state.blogDetailActive) {
                this.setState({
                    blogDetailActive: false
                });
            }
        }
    };

    handleGlobeCityClick = (city /* , id */) => {
        const { dispatch, worldGlobePosts } = this.props;

        this.setState({
            isPanelActive: true,
            panelCity: city.split(',')[0]
        });

        // console.log(city, id);
        // const value = city.split(',')[0].toLowerCase().trim();

        // this.props.dispatch(updateSetting('cityFilter', value));

        // if (this._feedFilter) {
        //     const el = findDOMNode(this._feedFilter);

        //     if (el) {
        //         const top = el.getBoundingClientRect().top;

        //         smoothScroll(top, 1000);
        //     }
        // }
        if (!worldGlobePosts.loading) {
            dispatch(getAllPosts(city));
        }
    };

    handlePanelCloseClick = () => {
        this.setState({
            isPanelActive: false,
            panelCity: ''
        });
    };

    handleGlobeZoom = (e) => {
        const input = e.currentTarget;
        const value = input.getAttribute('data-value');
        let scale = parseInt(input.value, 10) || this.state.globeZoom;

        if (value === 'in') {
            scale += 1;
            input.blur();
        } else if (value === 'out') {
            scale -= 1;
            input.blur();
        }

        const zoom = clamp(scale, MIN_ZOOM, MAX_ZOOM);

        this.setState({
            globeZoom: zoom
        });
    };

    handleAboutModalOpen = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_ABOUT_WORLD));
    };

    ////////////////////
    // helper methods //
    ////////////////////

    addOverflowClass() {
        if (window.innerWidth >= 768) {
            document.body.classList.add('overflow');
        }
    }

    render() {
        const aboutModalActive =
            this.props.modal.type === MODAL_TYPE_ABOUT_WORLD;

        return (
            <GlobePage
                aboutModalActive={aboutModalActive}
                onAboutInfoClick={this.handleAboutModalOpen}
                history={this.props.history}
                worldPost={this.props.worldPost}
                worldGlobePosts={this.props.worldGlobePosts}
                worldPostArtists={this.props.worldPostArtists}
                worldLocation={this.props.worldLocation}
                player={this.props.player}
                isPanelActive={this.state.isPanelActive}
                onPanelCloseClick={this.handlePanelCloseClick}
                panelCity={this.state.panelCity}
                currentZoom={this.state.globeZoom}
                minZoom={MIN_ZOOM}
                maxZoom={MAX_ZOOM}
                onGlobeCityClick={this.handleGlobeCityClick}
                onGlobeZoom={this.handleGlobeZoom}
                isGlobeSpinning={!aboutModalActive && !this.state.isPanelActive}
                loading={this.state.globeLoading}
                hasError={this.state.globeError}
                defaultGlobeRotation={DEFAULT_ROTATION}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        modal: state.modal,
        worldPost: state.worldPost,
        worldGlobePosts: state.worldGlobePosts,
        worldPostArtists: state.worldPostArtists,
        worldLocation: state.worldLocation,
        player: state.player
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps),
    hideHeaderForComponent,
    hideSidebarForComponent,
    activateDarkThemeForComponent
)(GlobePageContainer);
