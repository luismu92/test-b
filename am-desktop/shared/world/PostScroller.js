import React from 'react';
import PropTypes from 'prop-types';
import PostBlock from './PostBlock';

function PostScroller({ worldFeatured, worldPostArtists }) {
    return worldFeatured.list.map((post, i) => {
        const isLarge = i % 6 === 0;
        const isMedium = i % 6 === 4 || i % 6 === 5;
        const isSmall = !isLarge && !isMedium;

        return (
            <PostBlock
                className="column small-24"
                key={post.id}
                worldPostArtists={worldPostArtists}
                post={post}
                isSmall={isSmall}
                isMedium={isMedium}
                isLarge={isLarge}
            />
        );
    });
}

PostScroller.propTypes = {
    worldPostArtists: PropTypes.object,
    worldFeatured: PropTypes.object
};

export default PostScroller;
