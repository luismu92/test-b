import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import { getPages } from '../redux/modules/world/page';
import {
    getFeaturedPosts,
    setIncludes,
    setFilter
} from '../redux/modules/world/featured';
import { getWorldSettings } from '../redux/modules/world/settings';

import MainPage from './MainPage';

class MainPageContainer extends Component {
    static propTypes = {
        worldPage: PropTypes.object,
        dispatch: PropTypes.func,
        worldFeatured: PropTypes.object,
        worldSettings: PropTypes.object,
        match: PropTypes.object
    };

    componentDidUpdate(prevProps) {
        const { dispatch } = this.props;

        if (prevProps.match.params.slug !== this.props.match.params.slug) {
            dispatch(setFilter('tag', this.props.match.params.slug));
            dispatch(
                getFeaturedPosts({
                    page: 1
                })
            );
        }
    }

    render() {
        return (
            <MainPage
                worldSettings={this.props.worldSettings}
                worldPage={this.props.worldPage}
                worldFeatured={this.props.worldFeatured}
                match={this.props.match}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        worldFeatured: state.worldFeatured,
        worldSettings: state.worldSettings,
        worldPage: state.worldPage
    };
}

export default compose(connect(mapStateToProps))(
    connectDataFetchers(MainPageContainer, [
        () => getWorldSettings(),
        () => setIncludes(['authors', 'tags']),
        (params, query, props, dispatch) => {
            let promise = dispatch(getPages());

            // for /world/:page
            if (params.page) {
                promise = promise.then((action) => {
                    const foundPage = action.resolved.pages.find((page) => {
                        return page.slug === params.page;
                    });

                    let slug;

                    if (foundPage) {
                        slug = foundPage.tags[0].slug;
                    }

                    dispatch(setFilter('tag', slug));

                    return;
                });
            }
            // for /world/tag/:slug
            else if (params.slug) {
                dispatch(setFilter('tag', params.slug));
            } else {
                dispatch(setFilter('tag', null));
            }

            return promise.then(() => {
                return dispatch(
                    getFeaturedPosts({
                        page: 1
                    })
                );
            });
        }
    ])
);
