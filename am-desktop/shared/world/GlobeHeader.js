import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getDynamicImageProps } from 'utils/index';

import WorldLogoColor from 'components/WorldLogoColor';
import Button from '../buttons/Button';

import styles from './GlobeHeader.module.scss';

function GlobeHeader({ match, worldPage, worldFeatured }) {
    const page = worldPage.list.find((obj) => {
        return match.params.page === obj.slug;
    });
    let foundTag;

    // For tag page
    if (match.params.slug) {
        worldFeatured.list.some((post) => {
            return post.tags.some((tag) => {
                const matched = tag.slug === match.params.slug;

                if (matched) {
                    foundTag = tag;
                }

                return matched;
            });
        });
    }

    function renderTitle() {
        if (page) {
            return page.title;
        }

        // For tag page
        if (foundTag) {
            return foundTag.name;
        }

        return 'Audiomack World';
    }

    function renderExcerpt() {
        if (page) {
            return page.excerpt;
        }

        // For tag page
        if (foundTag) {
            return foundTag.description;
        }

        return 'An interactive experience that showcases original content from around the globe.';
    }

    // eslint-disable-next-line
    function renderExploreButton() {
        if (page || foundTag) {
            return null;
        }

        return (
            <div className={styles.button}>
                <Button
                    href="/world/globe"
                    height={48}
                    text="Explore the Globe"
                />
            </div>
        );
    }

    // eslint-disable-next-line
    function renderGlobeBackground() {
        const url = 'https://assets.audiomack.com/_am-blog/hero-globe-full.jpg';

        const [artwork, srcSet] = getDynamicImageProps(url, {
            width: 1000,
            max: true
        });

        return <img src={artwork} srcSet={srcSet} alt="" />;
    }

    return (
        <header className={styles.container}>
            {renderGlobeBackground()}
            <div className={styles.content}>
                <div className={styles.titleWrap}>
                    <Link to="/world" className={styles.logo}>
                        <WorldLogoColor className="u-no-fill" />
                    </Link>
                    <h1 className={styles.title}>{renderTitle()}</h1>
                </div>
                <h3 className={styles.subtitle}>{renderExcerpt()}</h3>
                {renderExploreButton()}
            </div>
        </header>
    );
}

GlobeHeader.propTypes = {
    match: PropTypes.object,
    worldFeatured: PropTypes.object,
    worldPage: PropTypes.object
};

export default GlobeHeader;
