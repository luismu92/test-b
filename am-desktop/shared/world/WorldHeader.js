import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import WorldLogoColor from 'components/WorldLogoColor';
import Arrow from 'icons/arrow-prev';
import Burger from '../components/Burger';

import InfoIcon from 'icons/info2';

import styles from './WorldHeader.module.scss';

export default class WorldHeader extends Component {
    static propTypes = {
        pages: PropTypes.object,
        logo: PropTypes.bool,
        transparent: PropTypes.bool,
        tooltipActive: PropTypes.bool,
        onBackClick: PropTypes.func,
        onActionClick: PropTypes.func,
        globePage: PropTypes.bool
    };

    renderSubMenu() {
        const { pages, tooltipActive } = this.props;
        const { list } = pages;

        if (!pages || !list.length) {
            return null;
        }

        const klass = classnames(
            `${styles.tooltip} tooltip tooltip--right-arrow sub-menu`,
            {
                'tooltip--active': tooltipActive
            }
        );

        const items = list.map((item, i) => {
            return (
                <li key={i} className="sub-menu__item">
                    <Link to={`/world/${item.slug}`}>{item.title}</Link>
                </li>
            );
        });

        return <ul className={klass}>{items}</ul>;
    }

    renderActionButton() {
        if (this.props.globePage) {
            return (
                <button
                    onClick={this.props.onActionClick}
                    className={styles.info}
                    data-action="about"
                    aria-label="Show about dialog"
                >
                    <InfoIcon />
                </button>
            );
        }

        return (
            <div className={styles.burger}>
                <Burger onBurgerClick={this.props.onActionClick} />
                {this.renderSubMenu()}
            </div>
        );
    }

    render() {
        const { logo, transparent } = this.props;

        let worldLogo;

        if (logo) {
            worldLogo = (
                <Link to="/world" className={styles.logo}>
                    <WorldLogoColor className="u-no-fill" />
                </Link>
            );
        }

        const klass = classnames(styles.header, {
            [styles.transparent]: transparent
        });

        return (
            <div className={klass}>
                <button
                    onClick={this.props.onBackClick}
                    className={styles.back}
                    aria-label="Go back"
                >
                    <Arrow />
                </button>
                {worldLogo}
                {this.renderActionButton()}
            </div>
        );
    }
}
