import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { clamp, request } from 'utils/index';
import AndroidLoader from 'components/loaders/AndroidLoader';

import ZoomScale from './ZoomScale';

import styles from './Globe.module.scss';

export default class Globe extends Component {
    static propTypes = {
        shouldShow: PropTypes.bool,
        isSpinning: PropTypes.bool,
        minZoom: PropTypes.number,
        currentZoom: PropTypes.number,
        maxZoom: PropTypes.number,
        onGlobeCityClick: PropTypes.func,
        onGlobeReady: PropTypes.func,
        onGlobeZoom: PropTypes.func,
        defaultRotation: PropTypes.array,
        cityData: PropTypes.array
    };

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            hasError: false
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        Promise.all([
            import('utils/GlobeD3Canvas'),
            request('/static/globe/world-110m.json')
        ])
            .then(([GlobeModule, worldData]) => {
                const globe = new GlobeModule.default(
                    this._container,
                    worldData,
                    {
                        classes: {
                            cityText: styles.city,
                            countryText: styles.country,
                            point: styles.point,
                            pointHover: styles.pointHover,
                            pointInner: styles.pointInner,
                            pointAnimate: styles.pointAnimate,
                            pointInactive: styles.pointInactive,
                            tooltip: styles.tooltip,
                            tooltipActive: styles.tooltipActive
                        },
                        sidePadding: (width) => {
                            const minPadding = 10;
                            const maxPadding = 200;
                            const paddingWidthRatio = minPadding / 350;
                            const padding = paddingWidthRatio * width;

                            return clamp(padding, minPadding, maxPadding);
                        },
                        onLoad: () => {
                            setTimeout(() => {
                                globe.resize(() => {
                                    globe.rotate(...this.props.defaultRotation);

                                    if (this.props.cityData.length) {
                                        console.warn(
                                            'Setting data onload',
                                            this.props.cityData
                                        );
                                        globe.setTooltipData(
                                            this.props.cityData
                                        );
                                        globe.enableTooltips();
                                    }

                                    this.setState({
                                        loading: false
                                    });

                                    this.props.onGlobeReady();
                                });
                            }, 200);
                        },

                        onResize: () => {
                            if (window.innerWidth >= 768) {
                                globe.enableControls();
                            } else {
                                globe.disableControls();
                            }
                        },
                        onPointClick: this.props.onGlobeCityClick,
                        onError: (err) => {
                            console.error(err);

                            this.setState({
                                loading: false,
                                hasError: true
                            });
                        }
                    }
                );

                this._globe = globe;
                return;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    componentDidUpdate(prevProps) {
        if (this._globe) {
            if (this.props.currentZoom !== prevProps.currentZoom) {
                this._globe.zoom(this.props.currentZoom);
            }

            if (this.props.cityData.length !== prevProps.cityData.length) {
                this._globe.setTooltipData(this.props.cityData);
            }

            if (this.props.isSpinning !== prevProps.isSpinning) {
                if (this.props.isSpinning) {
                    this._globe.start();
                    this._globe.enableControls();
                } else {
                    this._globe.stop();
                    this._globe.disableControls();
                }
            }
        }
    }

    componentWillUnmount() {
        if (this._globe) {
            this._globe.destroy();
        }
    }

    render() {
        const { shouldShow } = this.props;
        const { loading, hasError } = this.state;
        const klass = classnames(styles.container, {
            [styles.ready]: shouldShow
        });

        let zoomScale;

        if (!loading && !hasError) {
            zoomScale = (
                <ZoomScale
                    onGlobeZoom={this.props.onGlobeZoom}
                    currentZoom={this.props.currentZoom}
                    minZoom={this.props.minZoom}
                    maxZoom={this.props.maxZoom}
                />
            );
        }

        let loader;

        if (loading) {
            loader = (
                <div className={styles.loader}>
                    <AndroidLoader />
                    <p>Loading world data…</p>
                </div>
            );
        }

        let error;

        if (hasError) {
            error = (
                <p className={styles.loader}>
                    Sorry, there's an error in the world. Try again later.
                </p>
            );
        }

        return (
            <Fragment>
                {loader}
                {error}
                <div
                    className={klass}
                    ref={(c) => {
                        this._container = c;
                    }}
                />
                {zoomScale}
            </Fragment>
        );
    }
}
