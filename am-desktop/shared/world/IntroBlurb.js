import React from 'react';
// import PropTypes from 'prop-types';

import AmWorldLogo from '../icons/am-world';

import styles from './IntroBlurb.module.scss';

function IntroBlurb() {
    return (
        <div className={styles.container}>
            <AmWorldLogo className={styles.logo} />
            <h1 className={styles.title}>Audiomack World</h1>
            <h2 className={styles.subtitle}>
                An interactive experience that showcases original content from
                around the globe.
            </h2>
        </div>
    );
}

IntroBlurb.propTypes = {};

export default IntroBlurb;
