import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getBlogPageUrl } from 'utils/blog';

import styles from './Nav.module.scss';

function Nav({ worldPage }) {
    const pages = [
        <NavLink
            to="/world"
            key="all"
            activeClassName={styles.active}
            title="View all posts"
            exact
        >
            Home
        </NavLink>
    ].concat(
        worldPage.list.map((page) => {
            return (
                <NavLink
                    to={getBlogPageUrl(page)}
                    key={page.id}
                    title={page.excerpt}
                    activeClassName={styles.active}
                    exact
                >
                    {page.title}
                </NavLink>
            );
        })
    );

    return (
        <div className={styles.wrap}>
            <nav className={styles.container}>{pages}</nav>
        </div>
    );
}

Nav.propTypes = {
    worldPage: PropTypes.object
};

export default Nav;
