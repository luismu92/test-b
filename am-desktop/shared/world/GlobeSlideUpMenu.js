import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

// import { truncate } from 'utils/string';
import AndroidLoader from 'components/loaders/AndroidLoader';

import Carousel from '../components/Carousel';
import CloseIcon from 'icons/close-thin';

import PostBlock from './PostBlock';
import styles from './GlobeSlideUpMenu.module.scss';

function GlobeSlideUpMenu({
    isActive,
    posts,
    activeCity,
    onClose,
    worldPostArtists,
    loading,
    error
}) {
    const postWidth = 324;
    const panelClass = classnames(styles.panel, {
        [styles.panelActive]: isActive
    });
    const items = posts.map((post) => {
        return (
            <PostBlock
                key={post.id}
                worldPostArtists={worldPostArtists}
                post={post}
                maxTitleLength={65}
                maxExcerptLength={155}
            />
        );
    });

    let loader;
    let errorMessage;
    let carousel = (
        <Carousel
            items={items}
            itemSpacing={40}
            className="column small-24"
            itemWidth={postWidth}
        />
    );

    if (loading) {
        loader = (
            <div className={styles.loader}>
                <AndroidLoader />
            </div>
        );
    } else if (error) {
        carousel = null;
        errorMessage = (
            <div className={styles.loader}>
                <p className="u-text-red">
                    There was an error getting posts for {activeCity}.
                </p>
            </div>
        );
    }

    return (
        <aside className={panelClass}>
            <button
                className={styles.close}
                onClick={onClose}
                aria-label="Close panel"
            >
                <CloseIcon />
            </button>
            <span className={styles.city} aria-hidden="true">
                {activeCity}
            </span>
            {loader}
            {errorMessage}
            <div className={styles.carouselContainer}>{carousel}</div>
        </aside>
    );
}

GlobeSlideUpMenu.propTypes = {
    worldPostArtists: PropTypes.object,
    loading: PropTypes.bool,
    activeCity: PropTypes.string,
    onClose: PropTypes.func,
    error: PropTypes.object,
    isActive: PropTypes.bool,
    posts: PropTypes.array
};

GlobeSlideUpMenu.defaultProps = {
    posts: []
};

export default GlobeSlideUpMenu;
