import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import AboutModal from './AboutModal';
import WorldHeaderContainer from './WorldHeaderContainer';
import Starfield from './Starfield';
import Globe from './Globe';
import GlobeSlideUpMenu from './GlobeSlideUpMenu';
import Button from '../buttons/Button';

import styles from './GlobePage.module.scss';

function GlobePage({
    // worldPost,
    worldGlobePosts,
    worldPostArtists,
    worldLocation,
    aboutModalActive,
    onAboutInfoClick,
    minZoom,
    currentZoom,
    player,
    maxZoom,
    onGlobeZoom,
    onGlobeCityClick,
    onPanelCloseClick,
    defaultGlobeRotation,
    isPanelActive,
    panelCity,
    isGlobeSpinning,
    history
}) {
    const [showGlobe, setGlobeReady] = useState(false);
    const [showStarfield, setStarfieldReady] = useState(false);

    function handleGlobeReady() {
        setGlobeReady(true);
    }

    function handleStarfieldReady() {
        setStarfieldReady(true);
    }

    const pageIsReady = showGlobe && showStarfield;
    const buttonClass = classnames(styles.buttonWrap, {
        [styles.playerActive]: !!player.currentSong
    });

    return (
        <Fragment>
            <WorldHeaderContainer
                transparent
                logo={false}
                onAboutModalClick={onAboutInfoClick}
                history={history}
                globePage
            />
            <Starfield
                shouldShow={showStarfield}
                onStarfieldReady={handleStarfieldReady}
            />
            <Globe
                shouldShow={pageIsReady}
                defaultRotation={defaultGlobeRotation}
                onGlobeZoom={onGlobeZoom}
                onGlobeReady={handleGlobeReady}
                onGlobeCityClick={onGlobeCityClick}
                minZoom={minZoom}
                currentZoom={currentZoom}
                maxZoom={maxZoom}
                cityData={worldLocation.list}
                isSpinning={isGlobeSpinning}
            />
            <AboutModal active={aboutModalActive} />
            <div className={buttonClass}>
                <Button href="/world" height={48} text="Collapse Globe" />
            </div>
            <GlobeSlideUpMenu
                worldPostArtists={worldPostArtists}
                activeCity={panelCity}
                isActive={isPanelActive}
                posts={worldGlobePosts.filteredList}
                loading={worldGlobePosts.loading}
                error={worldGlobePosts.error}
                onClose={onPanelCloseClick}
            />
        </Fragment>
    );
}

GlobePage.propTypes = {
    worldPost: PropTypes.object,
    worldGlobePosts: PropTypes.object,
    worldPostArtists: PropTypes.object,
    panelCity: PropTypes.string,
    worldLocation: PropTypes.object,
    history: PropTypes.object,
    player: PropTypes.object,
    isPanelActive: PropTypes.bool,
    isGlobeSpinning: PropTypes.bool,
    aboutModalActive: PropTypes.bool,
    loading: PropTypes.bool,
    hasError: PropTypes.bool,
    globeReady: PropTypes.bool,
    onPanelCloseClick: PropTypes.func,
    minZoom: PropTypes.number,
    currentZoom: PropTypes.number,
    maxZoom: PropTypes.number,
    defaultGlobeRotation: PropTypes.array,
    onAboutInfoClick: PropTypes.func,
    onGlobeCityClick: PropTypes.func,
    onGlobeZoom: PropTypes.func
};

export default GlobePage;
