import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { getDynamicImageProps } from 'utils/index';
import { truncate } from 'utils/string';
import { getBlogPostUrl, getFeaturedImage } from 'utils/blog';

import Tag from 'components/blog/Tag';
import PostAuthor from 'world/PostAuthor';

import styles from './PostBlock.module.scss';

function getFeatureImage(image, small, medium, large) {
    let width = 324;
    let height = 188;

    if (medium) {
        width = 505;
        height = 295;
    }

    if (large) {
        width = 700;
        height = 400;
    }

    return getDynamicImageProps(image, {
        width: width,
        height: height
    });
}

function PostBlock({
    worldPostArtists,
    post,
    isMedium,
    isSmall,
    isLarge,
    className,
    maxTitleLength,
    maxExcerptLength
}) {
    const postClass = classnames(styles.post, className, {
        [styles.large]: isLarge,
        [styles.medium]: isMedium,
        'medium-12': isMedium,
        'medium-8': isSmall
    });
    const imageClass = classnames(styles.image, {
        'column small-24 medium-16': isLarge
    });
    const contentClass = classnames(styles.content, {
        'column small-24 medium-8': isLarge
    });
    const postLink = getBlogPostUrl(post);

    let readMore;

    if (isLarge) {
        readMore = (
            <Link to={postLink} className={styles.readMore}>
                Read more
            </Link>
        );
    }

    const [featureImage, featureImageSrcSet] = getFeatureImage(
        getFeaturedImage(post),
        isSmall,
        isMedium,
        isLarge
    );

    let title = post.title;
    let titleTitle = null;

    if (maxTitleLength) {
        title = truncate(post.title, maxTitleLength);

        if (title !== post.title) {
            titleTitle = post.title;
        }
    }

    let excerpt = post.custom_excerpt || post.excerpt;
    let excerptTitle = null;

    if (maxExcerptLength) {
        const newExcerpt = truncate(excerpt, maxExcerptLength);

        if (newExcerpt !== excerpt) {
            excerptTitle = excerpt;
        }

        excerpt = newExcerpt;
    }

    const tag = (post.tags || []).filter((obj) => {
        return obj.visibility !== 'internal';
    })[0];

    if (!tag) {
        console.log(post);
    }

    return (
        <article className={postClass} key={post.id}>
            <div className="row collapse">
                <Link
                    aria-label={`Image for blog post: "${post.title}"`}
                    className={imageClass}
                    to={postLink}
                >
                    <img
                        className={styles.featureImage}
                        src={featureImage}
                        srcSet={featureImageSrcSet}
                        alt=""
                    />
                </Link>

                <div className={contentClass}>
                    <Tag tag={tag} />
                    <h2 className={styles.title}>
                        <Link to={postLink} aria-label={titleTitle}>
                            {title}
                        </Link>
                    </h2>
                    <div className={styles.excerpt}>
                        <p aria-label={excerptTitle}>{excerpt}</p>
                        {readMore}
                    </div>
                    <PostAuthor
                        post={post}
                        worldPostArtists={worldPostArtists}
                        style={{
                            marginTop: 20
                        }}
                    />
                </div>
            </div>
        </article>
    );
}

PostBlock.propTypes = {
    worldPostArtists: PropTypes.object,
    className: PropTypes.string,
    maxTitleLength: PropTypes.number,
    maxExcerptLength: PropTypes.number,
    post: PropTypes.object,
    isSmall: PropTypes.bool,
    isMedium: PropTypes.bool,
    isLarge: PropTypes.bool
};

export default PostBlock;
