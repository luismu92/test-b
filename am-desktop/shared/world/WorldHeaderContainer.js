import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';

import connectDataFetchers from 'lib/connectDataFetchers';

import { showModal, MODAL_TYPE_ABOUT_WORLD } from '../redux/modules/modal';
import { getPages } from '../redux/modules/world/page';

import WorldHeader from './WorldHeader';

class WorldHeaderContainer extends Component {
    static propTypes = {
        worldPage: PropTypes.object,
        history: PropTypes.object,
        logo: PropTypes.bool,
        transparent: PropTypes.bool,
        dispatch: PropTypes.func,
        globePage: PropTypes.bool
    };

    static defaultProps = {
        logo: true
    };

    constructor(props) {
        super(props);

        this.state = {
            tooltipActive: false
        };
    }

    handleBackClick = () => {
        const { history } = this.props;

        // Going to /world all the time instead of "back" because
        // sometimes if you do a fresh load on a post page, there is no back
        // page
        history.push('/world');
    };

    handleBurgerClick = () => {
        this.setState({ tooltipActive: !this.state.tooltipActive });
    };

    handleAboutModalOpen = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_ABOUT_WORLD));
    };

    handleActionClick = (e) => {
        const button = e.currentTarget;
        const action = button.getAttribute('data-action');

        switch (action) {
            case 'burger': {
                this.handleBurgerClick();
                return;
            }

            case 'about': {
                this.handleAboutModalOpen();
                return;
            }

            default:
                break;
        }
    };

    render() {
        return (
            <WorldHeader
                pages={this.props.worldPage}
                logo={this.props.logo}
                transparent={this.props.transparent}
                tooltipActive={this.state.tooltipActive}
                onBackClick={this.handleBackClick}
                onBurgerClick={this.handleBurgerClick}
                onActionClick={this.handleActionClick}
                globePage={this.props.globePage}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        worldPage: state.worldPage
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps)
)(connectDataFetchers(WorldHeaderContainer, [() => getPages()]));
