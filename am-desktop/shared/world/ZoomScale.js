import React from 'react';
import PropTypes from 'prop-types';

import PlusIcon from 'icons/plus-skinny';
import MinusIcon from 'icons/minus-skinny';

import styles from './ZoomScale.module.scss';

function ZoomScale({ onGlobeZoom, minZoom, currentZoom, maxZoom }) {
    return (
        <div className={styles.scale}>
            <button
                className={styles.in}
                onClick={onGlobeZoom}
                data-value="in"
                aria-label="Zoom in"
            >
                <PlusIcon />
            </button>
            <input
                type="range"
                value={currentZoom}
                min={minZoom}
                max={maxZoom}
                step="1"
                onChange={onGlobeZoom}
            />
            <button
                className={styles.out}
                onClick={onGlobeZoom}
                data-value="out"
                aria-label="Zoom out"
            >
                <MinusIcon />
            </button>
        </div>
    );
}

ZoomScale.propTypes = {
    onGlobeZoom: PropTypes.func,
    minZoom: PropTypes.number,
    currentZoom: PropTypes.number,
    maxZoom: PropTypes.number
};

export default ZoomScale;
