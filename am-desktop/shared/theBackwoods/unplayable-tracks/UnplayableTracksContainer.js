import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import UnplayableTracks from './UnplayableTracks';
import { showModal, MODAL_TYPE_ADMIN } from '../../redux/modules/modal';

import {
    getUnplayableTracks,
    removeItem,
    previousUnplayableTracks,
    nextUnplayableTracks
} from '../../redux/modules/theBackwoods/unplayableTracks';

class UnplayableTracksContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminUnplayableTracks: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getUnplayableTracks());
    }

    handleAfterUnplayable = (next) => {
        const { dispatch } = this.props;
        dispatch(nextUnplayableTracks(next));
    };

    handleBeforeUnplayable = (previous) => {
        const { dispatch } = this.props;
        dispatch(previousUnplayableTracks(previous));
    };

    handleRemoveUnplayable = (id) => {
        const { dispatch } = this.props;
        dispatch(removeItem(id));
    };

    handleShowModalAdmin = (item) => {
        const { dispatch } = this.props;
        dispatch(showModal(MODAL_TYPE_ADMIN, { item: item }));
    };

    render() {
        const {
            adminUnplayableTracks: {
                loading,
                errorTop,
                errorContent,
                unplayableTracks,
                next,
                previous
            }
        } = this.props;
        const error = { errorTop, errorContent };

        return (
            <UnplayableTracks
                error={error}
                loading={loading}
                unplayableTracks={unplayableTracks}
                onAfterUnplayable={this.handleAfterUnplayable}
                onBeforeUnplayable={this.handleBeforeUnplayable}
                onRemoveUnplayable={this.handleRemoveUnplayable}
                onShowModalAdmin={this.handleShowModalAdmin}
                next={next}
                previous={previous}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminUnplayableTracks: state.adminUnplayableTracks
    };
}

export default compose(
    // requireAuth,
    connect(mapStateToProps)
)(UnplayableTracksContainer);
