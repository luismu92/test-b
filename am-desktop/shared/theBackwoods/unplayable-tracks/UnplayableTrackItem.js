import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './UnplayableTracks.module.scss';
import AdminCogIcon from '../../icons/admin-cog';

export default class UnplayableTrackItem extends Component {
    static propTypes = {
        musicItem: PropTypes.object,
        onRemove: PropTypes.func,
        onShowModalAdmin: PropTypes.func,
        index: PropTypes.number
    };

    constructor(props) {
        super(props);
    }

    handleAdminItem = () => {
        const { musicItem, onShowModalAdmin } = this.props;

        onShowModalAdmin(musicItem);
    };

    handleRemove = () => {
        const { musicItem, onRemove } = this.props;

        onRemove(musicItem.id);
    };

    render() {
        const klass = classnames(styles.inputBtn, 'music-interaction');
        return (
            <div
                className={
                    'music-detail u-clearfix music-detail--album music-detail--feed'
                }
            >
                <div className={styles.containerInteractions}>
                    <button className={klass} onClick={this.handleAdminItem}>
                        <span className="music-interaction__inner">
                            <AdminCogIcon className="music-interaction__icon music-interaction__icon--admin u-margin-0" />
                        </span>
                    </button>
                    <button
                        value="Remove"
                        className={klass}
                        onClick={this.handleRemove}
                    >
                        Reviewed
                    </button>
                </div>
            </div>
        );
    }
}
