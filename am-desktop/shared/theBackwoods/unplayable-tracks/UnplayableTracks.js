import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import FeedBar from '../../widgets/FeedBar';
import AndroidLoader from '../../loaders/AndroidLoader';
import styles from './UnplayableTracks.module.scss';
import MusicDetailContainer from '../../browse/MusicDetailContainer';
import UnplayableTrackItem from './UnplayableTrackItem';
import AlbumIcon from '../../icons/albums';

export default class UnplayableTracks extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        error: PropTypes.object,
        unplayableTracks: PropTypes.array,
        onBeforeUnplayable: PropTypes.func,
        onAfterUnplayable: PropTypes.func,
        onRemoveUnplayable: PropTypes.func,
        onShowModalAdmin: PropTypes.func,
        next: PropTypes.string,
        previous: PropTypes.string
    };

    handleBefore = () => {
        const { onBeforeUnplayable, previous } = this.props;
        onBeforeUnplayable(previous);
    };

    handleAfter = () => {
        const { onAfterUnplayable, next } = this.props;
        onAfterUnplayable(next);
    };

    renderMusic() {
        const { unplayableTracks, next, previous } = this.props;
        const items = unplayableTracks.map((musicItem, i) => {
            return (
                <div className="u-spacing-bottom-60" key={i}>
                    <MusicDetailContainer
                        index={i}
                        key={`${musicItem.id}-${i}`}
                        item={musicItem}
                        musicList={unplayableTracks}
                        hideInteractions
                        feed
                    />
                    <UnplayableTrackItem
                        musicItem={musicItem}
                        index={i}
                        key={i}
                        onShowModalAdmin={this.props.onShowModalAdmin}
                        onRemove={this.props.onRemoveUnplayable}
                    />
                </div>
            );
        });

        const pagination = (
            <div className={styles.pages}>
                <button onClick={this.handleBefore} disabled={previous === ''}>
                    &laquo; Previous
                </button>
                <button onClick={this.handleAfter} disabled={next === ''}>
                    Next &raquo;
                </button>
            </div>
        );

        return (
            <div data-testid="browseContainer" className="column small-24">
                {items}
                {pagination}
            </div>
        );
    }

    renderTitle() {
        const title = (
            <span>
                <strong>Unplayable Tracks</strong>
            </span>
        );
        const icon = (
            <AlbumIcon className="feed-bar__title-icon feed-bar__title-icon--graph u-text-icon" />
        );
        return (
            <div className="feed-bar feed-bar--padded feed-bar--has-menu">
                <h2 className="feed-bar__title">
                    {icon} {title}
                </h2>
            </div>
        );
    }

    renderTable() {
        const { loading, error } = this.props;
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        return (
            <div className="row music-feed music-feed--chart">
                <header className="column small-24">
                    <div className="music-feed__context-switcher u-box-shadow">
                        <FeedBar
                            title={this.renderTitle()}
                            contextDropDown
                            padded
                        />
                    </div>
                </header>
                <div className="row expanded column small-24">
                    {loader}
                    {error.errorContent ? (
                        <p>{error.errorContent}</p>
                    ) : (
                        this.renderMusic()
                    )}
                </div>
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                {this.renderTable()}
            </Fragment>
        );
    }
}
