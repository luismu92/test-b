import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { parse } from 'query-string';

import { showModal, MODAL_TYPE_LABEL_SIGNUP } from '../redux/modules/modal';

import requireAuth from '../hoc/requireAuth';
import LabelSignupModal from '../modal/LabelSignupModal';

import ReservedEmailContainer from './reserved-email/ReservedEmailContainer';
import FollowListContainer from './follow-list/FollowListContainer';
import StatsSwapContainer from './stats-swap/StatsSwapContainer';
import TrendingOrganizerContainer from './trending-organizer/TrendingOrganizerContainer';
import RecentContainer from './recent/RecentContainer';
import ValidateArtistContainer from './validate-artist/ValidateArtistContainer';
import ImageResizeContainer from './image-resize/ImageResizeContainer';
import ArtistContainer from './artist/ArtistContainer';
import UnplayableTracksContainer from './unplayable-tracks/UnplayableTracksContainer';
import MusicAdminContainer from './music-admin/MusicAdminContainer';

class AdminPageContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        location: PropTypes.object,
        currentUser: PropTypes.object,
        match: PropTypes.object,
        history: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            confirmMessageOpen: true
        };
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        this.maybeShowLabelSignupModal();
        this.maybeRedirect();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleLabelSignup = () => {
        const { dispatch } = this.props;

        dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    maybeRedirect() {
        const {
            history,
            match: { params }
        } = this.props;

        if (typeof params.context === 'undefined') {
            history.replace({
                pathname: '/the-backwoods/reserved-email'
            });
        }
    }

    maybeShowLabelSignupModal() {
        const { dispatch, location, currentUser } = this.props;

        if (
            parse(location.search).enableAmp &&
            currentUser.profile.label_owner &&
            currentUser.profile.label_owner_status === 'invited'
        ) {
            dispatch(showModal(MODAL_TYPE_LABEL_SIGNUP));
        }
    }

    render() {
        const {
            match: { params, history }
        } = this.props;
        let content;

        switch (params.context) {
            case 'follow-list':
                content = <FollowListContainer />;
                break;

            case 'song-stats-migration':
                content = <StatsSwapContainer />;
                break;

            case 'accounts':
                content = (
                    <ArtistContainer detail={params.detail} history={history} />
                );
                break;

            case 'music':
                content = <MusicAdminContainer />;
                break;

            case 'trending':
                content = <TrendingOrganizerContainer />;
                break;

            case 'recent':
                content = <RecentContainer genreUrl={params.detail} />;
                break;

            case 'authenticate':
                content = <ValidateArtistContainer />;
                break;

            case 'image-resize':
                content = <ImageResizeContainer />;
                break;

            case 'unplayable-tracks':
                content = <UnplayableTracksContainer />;
                break;

            case 'reserved-email':
            default:
                content = <ReservedEmailContainer />;
                break;
        }

        return (
            <Fragment>
                {content}
                <LabelSignupModal />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

export default compose(
    (comp) =>
        requireAuth(comp, {
            validator(currentUser) {
                return currentUser.isLoggedIn && currentUser.isAdmin;
            },
            redirectTo(currentUser, location) {
                if (currentUser.isLoggedIn) {
                    return '/';
                }

                const pathname = `${location.pathname}${location.search}`;

                return `/login?redirectTo=${encodeURIComponent(pathname)}`;
            }
        }),
    connect(mapStateToProps)
)(AdminPageContainer);
