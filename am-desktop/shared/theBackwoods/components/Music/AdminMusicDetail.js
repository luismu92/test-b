import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import {
    convertSecondsToTimecode,
    isCurrentMusicItem,
    getUploader,
    getDynamicImageProps
} from 'utils/index';
import WaveformContainer from 'components/WaveformContainer';
import Avatar from 'components/Avatar';
import PlayButton from '../../../components/PlayButton';

import styles from './AdminMusicDetail.module.scss';

export default class AdminMusicDetail extends Component {
    static propTypes = {
        className: PropTypes.string,
        player: PropTypes.object,
        item: PropTypes.object,
        onItemClick: PropTypes.func,
        onActionClick: PropTypes.func,
        index: PropTypes.number,
        songKey: PropTypes.string,
        shouldLazyLoadArtwork: PropTypes.bool,
        shouldLinkArtwork: PropTypes.bool,
        isVerified: PropTypes.bool
    };

    static defaultProps = {
        onActionClick() {},
        shouldLinkArtwork: false
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    isNotReleased(item) {
        let hasStreamingUrl = !!item.streaming_url;

        if (item.tracks && item.tracks.length) {
            hasStreamingUrl = item.tracks.some((t) => t.streaming_url);
        }

        return (
            item.released_offset > 0 && !(this.props.songKey && hasStreamingUrl)
        );
    }

    renderLowerContent(item, player) {
        const { currentSong, currentTime } = player;
        const isCurrentSong = isCurrentMusicItem(currentSong, item);
        const elapsedDisplay = isCurrentSong ? currentTime || 0 : 0;

        if (this.isNotReleased(item)) {
            return null;
        }

        const itemDuration = item.duration;

        return (
            <div
                className={classnames(
                    styles.wave,
                    'music-detail__waveform-wrap waveform-wrap waveform-wrap--has-button u-hide-when-fixed'
                )}
            >
                <WaveformContainer
                    className={classnames(styles.waveformContainer)}
                    musicItem={item}
                />
                <span className="waveform__elapsed waveform__time">
                    {convertSecondsToTimecode(elapsedDisplay)}
                </span>
                <span className="waveform__duration waveform__time">
                    {convertSecondsToTimecode(itemDuration || player.duration)}
                </span>
            </div>
        );
    }

    renderHeader(item, player) {
        const { artist, title } = item;
        const { index } = this.props;
        const { currentSong, paused, loading } = player;
        const isCurrentSong = isCurrentMusicItem(currentSong, item);

        const isPaused = !isCurrentSong || (isCurrentSong && paused);
        const isLoading = isCurrentSong && loading;
        const stillProcessing = item.streaming_url === '';

        const iconSize = 40;

        const buttonProps = {
            'data-tooltip': stillProcessing
                ? 'This item is still processing'
                : null,
            'data-tooltip-active': stillProcessing ? true : null
        };

        return (
            <div className={classnames(styles.info, 'row small-24')}>
                <div className="column small-21">
                    <h3 className={classnames(styles.artistName, 'u-trunc')}>
                        {artist}
                    </h3>
                    <h3 className={classnames(styles.songName, 'u-trunc')}>
                        {title}
                    </h3>
                </div>
                <div className="row">
                    <span {...buttonProps}>
                        <PlayButton
                            onlySizeLoader
                            className="waveform-wrap__play-button"
                            loading={isLoading || stillProcessing}
                            size={iconSize}
                            paused={isPaused}
                            onButtonClick={this.props.onItemClick}
                            buttonProps={{
                                'data-music-index': index
                            }}
                        />
                    </span>
                </div>
            </div>
        );
    }

    renderArtwork() {
        const { item, shouldLinkArtwork, shouldLazyLoadArtwork } = this.props;
        const uploader = getUploader(item);
        const imageSize = 50;

        const [artwork, srcSet] = getDynamicImageProps(
            item.image_base || item.image,
            {
                size: imageSize
            }
        );

        if (shouldLinkArtwork) {
            return (
                <Link
                    to={`/${item.type}/${uploader.url_slug}/${item.url_slug}`}
                    className={styles.artwork}
                    aria-label={`Go to ${item.type}`}
                >
                    <img
                        src={artwork}
                        srcSet={srcSet}
                        loading={shouldLazyLoadArtwork ? 'lazy' : 'eager'}
                        alt=""
                    />
                </Link>
            );
        }

        return (
            <Avatar
                image={item.image_base || item.image}
                type={item.type}
                imageSize={imageSize}
                zoomImage={item.image_base || item.image}
                className={styles.artwork}
                rounded={false}
                lazyLoad={shouldLazyLoadArtwork}
            />
        );
    }

    renderMusicData = () => {
        const { item } = this.props;
        const { title, artist, isrc, upc, label, album } = item.recognition;

        return (
            <ul className={styles.data}>
                <li>
                    <span>Label:</span> {label}
                </li>
                <li>
                    <span>Artist:</span> {artist}
                </li>
                <li>
                    <span>Title:</span> {title}
                </li>
                <li>
                    <span>ISRC:</span> {isrc}
                </li>
                <li>
                    <span>Album:</span> {album}
                </li>
                <li>
                    <span>UPC:</span> {upc}
                </li>
            </ul>
        );
    };

    render() {
        const { item, player } = this.props;

        if (!item) {
            return null;
        }

        const lowerContent = this.renderLowerContent(item, player);

        const innerContent = (
            <div>
                <div className={styles.top}>
                    {this.renderArtwork()}
                    {this.renderHeader(item, player)}
                </div>
                {lowerContent}
                {this.renderMusicData()}
            </div>
        );

        const containerClass = classnames(styles.music, 'u-bg-white');

        return (
            <div className={containerClass}>
                <div className="u-clearfix" data-testid="musicDetail">
                    {innerContent}
                </div>
            </div>
        );
    }
}
