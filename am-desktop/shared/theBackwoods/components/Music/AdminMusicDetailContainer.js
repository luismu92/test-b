import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import {
    isCurrentMusicItem,
    getQueueIndexForSong,
    queueHasMusicItem
} from 'utils/index';

import {
    showModal,
    MODAL_TYPE_QUEUE_ALERT
} from '../../../redux/modules/modal';
import { play, pause, editQueue } from '../../../redux/modules/player';

import MusicDetail from './AdminMusicDetail';

class AdminMusicDetailContainer extends Component {
    static propTypes = {
        musicList: PropTypes.array.isRequired,
        index: PropTypes.number,
        dispatch: PropTypes.func,
        className: PropTypes.string,
        currentUser: PropTypes.object,
        player: PropTypes.object,
        item: PropTypes.object,
        shouldLazyLoadArtwork: PropTypes.bool,
        shouldLinkArtwork: PropTypes.bool,
        songKey: PropTypes.string,
        isVerified: PropTypes.bool
    };

    ////////////////////
    // Event handlers //
    ////////////////////

    handleItemClick = (event) => {
        const { dispatch, player, musicList } = this.props;
        const index =
            parseInt(
                event.currentTarget.getAttribute('data-music-index'),
                10
            ) || 0;
        const buttonIndex = parseInt(
            event.currentTarget.getAttribute('data-track-index'),
            10
        );
        const currentSong = player.currentSong;
        const musicItem = musicList[index];
        const clickedOnTrack = !isNaN(buttonIndex);
        const allTracksOfItemQueued = queueHasMusicItem(
            player.queue,
            musicItem,
            player.queueIndex
        );
        const shouldCountTrack = !clickedOnTrack && allTracksOfItemQueued;
        const isAlreadyCurrentSong = isCurrentMusicItem(
            currentSong,
            musicItem,
            shouldCountTrack
        );

        if (isAlreadyCurrentSong) {
            if (player.paused) {
                dispatch(play());
            } else {
                dispatch(pause());
            }
            return;
        }

        const isItemWithTracks = !!musicItem.tracks;

        if (isItemWithTracks) {
            if (allTracksOfItemQueued && !clickedOnTrack) {
                const indexToPlay = getQueueIndexForSong(
                    musicItem,
                    player.queue,
                    {
                        currentQueueIndex: player.queueIndex
                    }
                );

                dispatch(
                    play(indexToPlay, {
                        item: musicItem,
                        onConfirmClick() {
                            dispatch(editQueue(musicItem, { append: true }));
                        }
                    })
                );
                return;
            }
        }

        const queueIndex = getQueueIndexForSong(musicItem, player.queue, {
            trackIndex: buttonIndex,
            currentQueueIndex: player.queueIndex
        });

        if (
            queueIndex !== -1 &&
            (!isItemWithTracks || (isItemWithTracks && allTracksOfItemQueued))
        ) {
            dispatch(play(queueIndex));
            return;
        }

        if (clickedOnTrack) {
            const track = musicItem.tracks[buttonIndex];
            const newIndex = getQueueIndexForSong(track, player.queue, {
                trackIndex: buttonIndex,
                currentQueueIndex: player.queueIndex,
                ignoreHistory: false
            });

            if (newIndex !== -1) {
                dispatch(play(newIndex));
                return;
            }
        }

        if (player.queueHasBeenEdited) {
            dispatch(
                showModal(MODAL_TYPE_QUEUE_ALERT, {
                    item: musicItem,
                    onAppendClick() {
                        const { queue: newQueue } = dispatch(
                            editQueue(musicList, { append: true })
                        );

                        const newIndex = getQueueIndexForSong(
                            musicItem,
                            newQueue,
                            {
                                trackIndex: buttonIndex,
                                currentQueueIndex: player.queueIndex
                            }
                        );

                        dispatch(play(newIndex));
                    },
                    onReplaceClick: () =>
                        this.replaceQueueAndPlay(musicList, musicItem)
                })
            );
            return;
        }

        this.replaceQueueAndPlay(musicList, musicItem, buttonIndex);
    };

    handleActionClick = (e) => {
        const action = e.currentTarget.getAttribute('data-action');

        this.doPlay(action, e);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    replaceQueueAndPlay(queue, musicItem, trackIndex) {
        const { dispatch } = this.props;
        const { queue: newQueue } = dispatch(editQueue(queue));
        // Start with last history item
        const startingIndex = newQueue.findIndex((item) => {
            return !item.startedPlaying;
        });
        const queueIndex = getQueueIndexForSong(musicItem, newQueue, {
            currentQueueIndex: startingIndex === -1 ? 0 : startingIndex - 1,
            trackIndex
        });

        dispatch(play(queueIndex));
    }

    doPlay(action, e) {
        if (action === 'play') {
            this.handleItemClick(e);
        }
    }

    render() {
        return (
            <MusicDetail
                shouldLinkArtwork={this.props.shouldLinkArtwork}
                currentUser={this.props.currentUser}
                className={this.props.className}
                player={this.props.player}
                item={this.props.item}
                songKey={this.props.songKey}
                onItemClick={this.handleItemClick}
                onActionClick={this.handleActionClick}
                index={this.props.index}
                shouldLazyLoadArtwork={this.props.shouldLazyLoadArtwork}
                isVerified={this.props.isVerified}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        player: state.player
    };
}

export default withRouter(connect(mapStateToProps)(AdminMusicDetailContainer));
