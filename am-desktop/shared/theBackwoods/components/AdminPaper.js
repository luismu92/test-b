import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import CogIcon from '../../icons/cog';
import styles from './AdminPaper.module.scss';

export default class AdminPaper extends Component {
    static propTypes = {
        title: PropTypes.string,
        topPaper: PropTypes.object,
        contentPaper: PropTypes.object
    };

    render() {
        const { title, topPaper, contentPaper } = this.props;
        const klass = classnames(styles.adminPaper, 'row u-spacing-bottom');

        return (
            <Fragment>
                <div className="row middle-xs u-spacing-top">
                    <div className="column small-24 dashboard-module__top">
                        <h1 className="dashboard-module__title">
                            <span className="u-text-orange u-text-icon u-d-inline-block">
                                <CogIcon />
                            </span>
                            {title}
                        </h1>
                    </div>
                </div>
                <div className={klass}>
                    <div className="column small-24">
                        <div className="u-box-shadow u-padded">
                            <div className={styles.adminTop}>{topPaper}</div>
                            <div className={styles.adminContent}>
                                {contentPaper}
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
