import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import CloseIcon from '../../icons/close-solid';
import styles from './FollowList.module.scss';

export default class FollowerItem extends Component {
    static propTypes = {
        artist: PropTypes.object,
        onRemove: PropTypes.func,
        index: PropTypes.number
    };

    constructor(props) {
        super(props);
    }

    handleRemove = () => {
        const { artist, onRemove } = this.props;

        onRemove(artist.id);
    };

    render() {
        const { artist } = this.props;

        return (
            <div className={styles.item}>
                <Link to={`/artist/${artist.urlSlug}`} className="u-fw-700">
                    {artist.name}
                </Link>
                <button onClick={this.handleRemove}>
                    <CloseIcon />
                </button>
            </div>
        );
    }
}
