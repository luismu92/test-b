import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import DraggableList from '../../list/DraggableList';
import FollowerItem from './FollowerItem';
import PaperTable from '../components/AdminPaper';

import Add from '../../icons/plus';
import AndroidLoader from '../../loaders/AndroidLoader';
import DragHandle from '../../../../am-shared/icons/drag-handle';
import styles from './FollowList.module.scss';

class FollowList extends Component {
    static propTypes = {
        inputValue: PropTypes.string,
        artists: PropTypes.array,
        loading: PropTypes.bool,
        onAddArtist: PropTypes.func,
        onRemoveArtist: PropTypes.func,
        onChangeInput: PropTypes.func,
        onDragEnd: PropTypes.func,
        error: PropTypes.object
    };

    componentDidUpdate(prevProps) {
        const currentList = this.props.artists;
        const lastList = prevProps.artists;

        const changedIds =
            currentList.length !== lastList.length ||
            currentList.some((item, i) => item.id !== lastList[i].id);

        if (changedIds && this.DraggableList) {
            this.DraggableList.forceUpdateGrid();
        }
    }

    renderTop() {
        const {
            inputValue,
            error: { errorTop }
        } = this.props;

        return (
            <div className={styles.followListTop}>
                <h5>Add or remove the artists you follow</h5>
                <p>
                    Add a new artist to the follow list by entering their full
                    URL in the search box. To change artist order just drag
                    them.
                </p>
                <div className={styles.searchBox}>
                    <input
                        type="url"
                        placeholder="https://example.com"
                        value={inputValue}
                        onChange={this.props.onChangeInput}
                    />
                    <button
                        disabled={!inputValue}
                        onClick={this.props.onAddArtist}
                    >
                        <Add />
                    </button>
                </div>
                <p className={styles.errorMessage}>{errorTop}</p>
            </div>
        );
    }

    renderContent() {
        const {
            artists,
            loading,
            error: { errorContent }
        } = this.props;
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const artistItems = artists.map((artist, index) => {
            return (
                <FollowerItem
                    key={`${index}${artist.id}`}
                    artist={artist}
                    onRemove={this.props.onRemoveArtist}
                    loading={loading}
                    index={index}
                />
            );
        });

        const klass = classnames(styles.adminDrag, 'u-drag-handle');
        const listFollowers = (
            <Fragment>
                {loader}
                {errorContent ? (
                    <p>{errorContent}</p>
                ) : (
                    <DraggableList
                        containerProps={{
                            className: 'column small-24'
                        }}
                        dragHandle={
                            <span className={klass}>
                                <DragHandle />
                            </span>
                        }
                        draggingClass=""
                        onDragFinish={this.props.onDragEnd}
                        items={artistItems}
                        onRowHeight={this.handleRowHeight}
                        ref={(instance) => {
                            this.DraggableList = instance;
                        }}
                        useWindowAsScrollContainer
                    />
                )}
            </Fragment>
        );

        return listFollowers;
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Follow List'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default FollowList;
