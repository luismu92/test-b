import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import FollowList from './FollowList';
import {
    addUrl,
    getFollowList,
    removeItem,
    orderItem
} from '../../redux/modules/theBackwoods/followList';

class FollowListContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminFollowList: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            inputValue: ''
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getFollowList());
    }

    handleAddArtist = () => {
        const { dispatch } = this.props;

        const { inputValue } = this.state;

        dispatch(addUrl(inputValue));
        this.setState({
            inputValue: ''
        });
    };

    handleRemoveArtist = (id) => {
        const { dispatch } = this.props;

        dispatch(removeItem(id));
    };

    handleInput = (e) => {
        this.setState({
            inputValue: e.target.value
        });
    };

    handleDragEnd = (fromIndex, toIndex) => {
        const {
            dispatch,
            adminFollowList: { artists }
        } = this.props;
        const draggedItem = artists[fromIndex];
        const draggedOverItem = artists[toIndex];

        if (draggedItem.id === draggedOverItem.id) {
            return;
        }

        // filter out the currently dragged item
        const items = artists.filter((artist) => artist.id !== draggedItem.id);

        // add the dragged item after the dragged over item
        items.splice(toIndex, 0, draggedItem);

        dispatch(orderItem(items));
    };

    render() {
        const {
            adminFollowList: { artists, loading, errorTop, errorContent }
        } = this.props;
        const { inputValue } = this.state;
        const error = { errorTop, errorContent };

        return (
            <FollowList
                inputValue={inputValue}
                error={error}
                artists={artists}
                loading={loading}
                onAddArtist={this.handleAddArtist}
                onRemoveArtist={this.handleRemoveArtist}
                onChangeInput={this.handleInput}
                onDragEnd={this.handleDragEnd}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminFollowList: state.adminFollowList
    };
}

export default compose(
    // requireAuth,
    connect(mapStateToProps)
)(FollowListContainer);
