import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import classnames from 'classnames';

import AndroidLoader from '../../loaders/AndroidLoader';
import PaperTable from '../components/AdminPaper';
import ItemEmail from './ItemEmail';
import styles from './ReservedEmail.module.scss';

export default class ReservedEmail extends Component {
    static propTypes = {
        emails: PropTypes.array,
        loading: PropTypes.bool,
        error: PropTypes.string,
        disabledRemove: PropTypes.number,
        onAddEmail: PropTypes.func,
        onCancel: PropTypes.func,
        onRemove: PropTypes.func,
        onSave: PropTypes.func,
        onRepeat: PropTypes.func,
        onReload: PropTypes.func,
        onDisableRemove: PropTypes.func
    };

    renderEmails(emails) {
        const listEmails = emails.map((email, index) => {
            return (
                <ItemEmail
                    key={`${email.email}${index}`}
                    index={index}
                    emailObj={email}
                    disabledRemove={this.props.disabledRemove}
                    onCancel={this.props.onCancel}
                    onRemove={this.props.onRemove}
                    onSave={this.props.onSave}
                    onRepeat={this.props.onRepeat}
                    onDisableRemove={this.props.onDisableRemove}
                />
            );
        });

        return listEmails;
    }

    renderTable() {
        const { loading, emails, error } = this.props;
        let disabledAddBtn = false;
        let loader;

        if (emails.length > 0 && emails[emails.length - 1].email === '') {
            disabledAddBtn = true;
        }

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const klass = classnames(
            styles.newBtn,
            'button button--radius button--upload'
        );
        const topPaper = (
            <div className={styles.adminTop}>
                <div>
                    <h5>Add or remove reserved email</h5>
                    <p>
                        The list of emails below are reserved by the system and
                        can't be used for account sign up.
                    </p>
                </div>
                {error ? (
                    <button className={klass} onClick={this.props.onReload}>
                        Reload
                    </button>
                ) : (
                    <button
                        className={klass}
                        disabled={disabledAddBtn}
                        onClick={this.props.onAddEmail}
                    >
                        New Email
                    </button>
                )}
            </div>
        );

        const contentPaper = (
            <Fragment>
                {loader}
                {error ? <p>{error}</p> : this.renderEmails(emails)}
            </Fragment>
        );

        return (
            <PaperTable
                title={'Reserved Email'}
                topPaper={topPaper}
                contentPaper={contentPaper}
            />
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                {this.renderTable()}
            </Fragment>
        );
    }
}
