import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import requireAuth from '../../hoc/requireAuth';
import {
    getReservedEmails,
    newEmail,
    cancelNew,
    removeEmail,
    saveEmail
} from '../../redux/modules/theBackwoods/reservedEmail';

import ReservedEmail from './ReservedEmail';

class ReservedEmailContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminReservedEmail: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            disabledRemove: 0
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getReservedEmails());
    }

    handleReload = () => {
        const { dispatch } = this.props;

        dispatch(getReservedEmails());
    };

    handleAddEmail = () => {
        const { dispatch } = this.props;

        dispatch(newEmail());
    };

    handleCancel = (emailValue) => {
        const { dispatch } = this.props;

        if (emailValue === '') {
            dispatch(cancelNew());
        }
    };

    handleRemove = (emailIndex) => {
        const { dispatch } = this.props;

        dispatch(removeEmail(emailIndex));
    };

    handleSave = (emailIndex, newEmailValue) => {
        const { dispatch } = this.props;
        const { disabledRemove } = this.state;

        this.setState(
            {
                disabledRemove: disabledRemove - 1
            },
            () => dispatch(saveEmail(emailIndex, newEmailValue))
        );
    };

    handleDisableRemove = (number) => {
        const { disabledRemove } = this.state;

        this.setState({
            disabledRemove: disabledRemove + number
        });
    };

    handleRepeatValue = (emailValue) => {
        const {
            adminReservedEmail: { emails }
        } = this.props;
        const isRepeat = emails.some(
            (email) => email.email === emailValue && email.email !== ''
        );

        return isRepeat;
    };

    render() {
        const { disabledRemove } = this.state;
        const {
            adminReservedEmail: { emails, loading, error }
        } = this.props;

        return (
            <ReservedEmail
                onAddEmail={this.handleAddEmail}
                onCancel={this.handleCancel}
                onRemove={this.handleRemove}
                onSave={this.handleSave}
                onRepeat={this.handleRepeatValue}
                onReload={this.handleReload}
                onDisableRemove={this.handleDisableRemove}
                emails={emails}
                loading={loading}
                error={error}
                disabledRemove={disabledRemove}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminReservedEmail: state.adminReservedEmail
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(ReservedEmailContainer);
