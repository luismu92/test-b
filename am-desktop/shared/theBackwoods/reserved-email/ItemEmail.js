import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './ReservedEmail.module.scss';

export default class ItemEmail extends Component {
    static propTypes = {
        emailObj: PropTypes.object,
        index: PropTypes.number,
        disabledRemove: PropTypes.number,
        onCancel: PropTypes.func,
        onRemove: PropTypes.func,
        onSave: PropTypes.func,
        onRepeat: PropTypes.func,
        onDisableRemove: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            editedEmail: '',
            edited: false,
            errorMessage: ''
        };
    }

    handleChange = (e) => {
        const { onDisableRemove } = this.props;
        const { edited } = this.state;

        if (edited === false) {
            onDisableRemove(1);
        }

        this.setState({
            editedEmail: e.target.value,
            edited: true
        });
    };

    handleCancel = (e) => {
        e.preventDefault();
        const {
            emailObj: { email },
            onCancel,
            onDisableRemove
        } = this.props;
        const { edited } = this.state;

        if (edited === true) {
            onDisableRemove(-1);
        }

        if (email.length === 0) {
            onCancel(email);
        } else {
            this.setState({
                editedEmail: '',
                edited: false
            });
        }
    };

    handleRemove = (e) => {
        e.preventDefault();
        const { index, onRemove } = this.props;

        onRemove(index);
    };

    handleSave = (e) => {
        e.preventDefault();
        const { editedEmail } = this.state;
        const { index, onSave, onRepeat } = this.props;

        if (onRepeat(editedEmail)) {
            this.setState({
                errorMessage: 'This email is already a reserved email'
            });
        } else {
            onSave(index, editedEmail);

            this.setState({
                editedEmail: '',
                edited: false,
                errorMessage: ''
            });
        }
    };

    renderInputButtons() {
        const { edited } = this.state;
        const { emailObj, disabledRemove } = this.props;
        let btn;

        const klass = classnames(styles.inputBtn, 'music-interaction');

        if (!emailObj.default) {
            if (edited || emailObj.email.length === 0) {
                btn = (
                    <Fragment>
                        <input type="submit" value="Save" className={klass} />
                        <button
                            value="Cancel"
                            className={klass}
                            onClick={this.handleCancel}
                        >
                            Cancel
                        </button>
                    </Fragment>
                );
            } else {
                btn = (
                    <button
                        value="Remove"
                        className={klass}
                        onClick={this.handleRemove}
                        disabled={disabledRemove > 0}
                    >
                        Remove
                    </button>
                );
            }
        }

        return btn;
    }

    render() {
        const { editedEmail, errorMessage, edited } = this.state;
        const { emailObj } = this.props;
        const { email } = emailObj;
        const emailToShow = edited ? editedEmail : email;

        const error = <p className={styles.error}>{errorMessage}</p>;

        return (
            <div className="column small-24">
                <form
                    className={styles.adminReservedItem}
                    onSubmit={this.handleSave}
                >
                    <div className={styles.inputBox}>
                        <input
                            type="email"
                            name="email"
                            value={emailToShow}
                            disabled={emailObj.default}
                            placeholder={'example@audiomack.com'}
                            onChange={this.handleChange}
                            className={styles.input}
                            required
                        />
                        {errorMessage.length > 0 ? error : null}
                    </div>
                    {this.renderInputButtons()}
                </form>
            </div>
        );
    }
}
