import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    allGenresMap,
    GENRE_TYPE_ALL,
    GENRE_TYPE_RAP,
    GENRE_TYPE_RNB,
    GENRE_TYPE_ELECTRONIC,
    GENRE_TYPE_DANCEHALL,
    GENRE_TYPE_PODCAST,
    GENRE_TYPE_POP,
    GENRE_TYPE_AFROBEATS,
    GENRE_TYPE_LATIN,
    GENRE_TYPE_INSTRUMENTAL,
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_RECENTLY_ADDED,
    CHART_TYPE_DAILY,
    CHART_TYPE_WEEKLY,
    CHART_TYPE_MONTHLY,
    CHART_TYPE_YEARLY
} from 'constants/index';

// import InContentAd from '../ad/InContentAd';
import AndroidLoader from '../../loaders/AndroidLoader';
import FeedBar from '../../widgets/FeedBar';

import BrowseEmptyState from '../../browse/BrowseEmptyState';
import MusicDetailContainer from '../../browse/MusicDetailContainer';

import FireIcon from '../../icons/fire';
import GraphIcon from '../../icons/graph';
import AlbumIcon from '../../icons/albums';
import RecentIcon from '../../icons/recent';

export default class Recent extends Component {
    static propTypes = {
        player: PropTypes.object,
        currentUser: PropTypes.object,
        adminRecent: PropTypes.object,
        location: PropTypes.object,
        timeMap: PropTypes.object,
        onTimespanSwitch: PropTypes.func,
        featured: PropTypes.object
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    getContextItems(activeContext = '', genre = '') {
        let context = '';

        if (activeContext) {
            context = `/${activeContext}`;
        }

        const items = [
            {
                text: 'All Genres',
                value: GENRE_TYPE_ALL,
                href: `/the-backwoods${context}`,
                active: genre === GENRE_TYPE_ALL
            },
            {
                text: allGenresMap[GENRE_TYPE_RAP],
                value: GENRE_TYPE_RAP,
                href: `/the-backwoods${context}/rap`,
                active: genre === GENRE_TYPE_RAP
            },
            {
                text: allGenresMap[GENRE_TYPE_RNB],
                value: GENRE_TYPE_RNB,
                href: `/the-backwoods${context}/rnb`,
                active: genre === GENRE_TYPE_RNB
            },
            {
                text: allGenresMap[GENRE_TYPE_ELECTRONIC],
                value: GENRE_TYPE_ELECTRONIC,
                href: `/the-backwoods${context}/electronic`,
                active: genre === GENRE_TYPE_ELECTRONIC
            },
            {
                text: allGenresMap[GENRE_TYPE_DANCEHALL],
                value: GENRE_TYPE_DANCEHALL,
                href: `/the-backwoods${context}/dancehall`,
                active: genre === GENRE_TYPE_DANCEHALL
            },
            {
                text: allGenresMap[GENRE_TYPE_POP],
                value: GENRE_TYPE_POP,
                href: `/the-backwoods${context}/pop`,
                active: genre === GENRE_TYPE_POP
            },
            {
                text: allGenresMap[GENRE_TYPE_AFROBEATS],
                value: GENRE_TYPE_AFROBEATS,
                href: `/the-backwoods${context}/afrobeats`,
                active: genre === GENRE_TYPE_AFROBEATS
            },
            {
                text: allGenresMap[GENRE_TYPE_PODCAST],
                value: GENRE_TYPE_PODCAST,
                href: `/the-backwoods${context}/podcast`,
                active: genre === GENRE_TYPE_PODCAST
            },
            {
                text: allGenresMap[GENRE_TYPE_LATIN],
                value: GENRE_TYPE_LATIN,
                href: `/the-backwoods${context}/latin`,
                active: genre === GENRE_TYPE_LATIN
            },
            {
                text: allGenresMap[GENRE_TYPE_INSTRUMENTAL],
                value: GENRE_TYPE_INSTRUMENTAL,
                href: `/the-backwoods${context}/instrumental`,
                active: genre === GENRE_TYPE_INSTRUMENTAL
            }
        ];

        // Remove podcast if context is albums
        if (activeContext === COLLECTION_TYPE_ALBUM) {
            items.splice(7, 1);
        }

        return items;
    }

    renderTitle(activeContext, activeGenre, activeTimePeriod) {
        let title = 'Trending Music';
        let icon = (
            <FireIcon className="feed-bar__title-icon feed-bar__title-icon--fire u-text-icon" />
        );
        let genre = '';
        let timeSpan = '';

        switch (activeTimePeriod) {
            case CHART_TYPE_DAILY:
                timeSpan = ' Today';
                break;

            case CHART_TYPE_WEEKLY:
                timeSpan = ' this Week';
                break;

            case CHART_TYPE_MONTHLY:
                timeSpan = ' this Month';
                break;

            case CHART_TYPE_YEARLY:
                timeSpan = ' this Year';
                break;

            default:
                break;
        }

        switch (activeGenre) {
            case GENRE_TYPE_RAP:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_RNB:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_ELECTRONIC:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_DANCEHALL:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_PODCAST:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_POP:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_AFROBEATS:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_LATIN:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            case GENRE_TYPE_INSTRUMENTAL:
                genre = ` ${allGenresMap[activeGenre]}`;
                break;

            default:
                genre = ' All Genres';
                break;
        }

        switch (activeContext) {
            case COLLECTION_TYPE_TRENDING: {
                title = (
                    <span>
                        <strong>Trending Music</strong> in{genre}
                    </span>
                );
                icon = (
                    <GraphIcon className="feed-bar__title-icon feed-bar__title-icon--graph u-text-icon" />
                );
                if (activeGenre === '') {
                    title = (
                        <span>
                            <strong>Trending</strong>
                        </span>
                    );
                }
                if (activeGenre === 'podcast') {
                    title = (
                        <span>
                            <strong>Trending</strong>
                            {genre}
                        </span>
                    );
                }
                break;
            }

            case COLLECTION_TYPE_SONG: {
                title = (
                    <span>
                        <strong>Top Songs</strong> in {genre}
                        {timeSpan}
                    </span>
                );
                if (activeGenre === '') {
                    title = (
                        <span>
                            <strong>Top Songs</strong>
                            {timeSpan}
                        </span>
                    );
                }
                if (activeGenre === 'podcast') {
                    title = (
                        <span>
                            <strong>Top Podcasts</strong> {timeSpan}
                        </span>
                    );
                }
                break;
            }

            case COLLECTION_TYPE_ALBUM: {
                title = (
                    <span>
                        <strong>Top Albums</strong> in{genre}
                        {timeSpan}
                    </span>
                );
                icon = (
                    <AlbumIcon className="feed-bar__title-icon feed-bar__title-icon--album u-text-icon" />
                );
                if (activeGenre === '') {
                    title = (
                        <span>
                            <strong>Top Albums</strong>
                            {timeSpan}
                        </span>
                    );
                }
                if (activeGenre === 'podcast') {
                    title = (
                        <span>
                            <strong>Top Podcasts</strong>
                            {timeSpan}
                        </span>
                    );
                }
                break;
            }

            case COLLECTION_TYPE_RECENTLY_ADDED: {
                title = (
                    <span>
                        <strong>Recently Added Music</strong> in{genre}
                    </span>
                );
                icon = (
                    <RecentIcon className="feed-bar__title-icon feed-bar__title-icon--recent u-text-icon" />
                );
                if (activeGenre === '') {
                    title = (
                        <span>
                            <strong>Recently Added Music</strong>
                        </span>
                    );
                }
                if (activeGenre === 'podcast') {
                    title = (
                        <span>
                            <strong>Recently Added</strong> in{genre}
                        </span>
                    );
                }
                break;
            }

            default:
                title = (
                    <span>
                        <strong>Trending Music</strong> in{genre}
                    </span>
                );
        }

        return (
            <h2 className="feed-bar__title">
                {icon} {title}
            </h2>
        );
    }

    renderFeed(data = {}) {
        const { list, activeContext, activeGenre } = data;

        const filteredList = list.filter((item) => {
            return (
                item.status !== 'suspended' &&
                item.status !== 'takedown' &&
                !item.geo_restricted
            );
        });

        const items = filteredList.map((musicItem, i) => {
            let ranking = null;

            if (activeContext === 'songs' || activeContext === 'albums') {
                ranking = i + 1;
            }

            return (
                <div className="u-spacing-bottom-60" key={i}>
                    <MusicDetailContainer
                        index={i}
                        key={`${musicItem.id}-${i}`}
                        item={musicItem}
                        shouldLazyLoadArtwork={i > 2}
                        shouldLinkArtwork
                        feed
                        musicList={filteredList}
                        context={activeContext}
                        genre={activeGenre}
                        ranking={ranking}
                    />
                </div>
            );
        });

        return (
            <div data-testid="browseContainer" className="column small-24">
                {items}
            </div>
        );
    }

    render() {
        const { adminRecent, featured, location, currentUser } = this.props;
        const {
            activeContext,
            activeGenre,
            activeTimePeriod,
            loading
        } = adminRecent;

        let loader;
        let emptyState;

        if (loading) {
            loader = <AndroidLoader className="music-feed__loader" />;
        }

        if (!loading && !adminRecent.list.length) {
            emptyState = (
                <BrowseEmptyState
                    activeContext={activeContext}
                    activeGenre={activeGenre}
                />
            );
        }

        return (
            <div className="row music-feed music-feed--chart">
                <header className="column small-24">
                    <div className="music-feed__context-switcher u-box-shadow">
                        <FeedBar
                            title={this.renderTitle(
                                activeContext,
                                activeGenre,
                                activeTimePeriod
                            )}
                            items={this.getContextItems(
                                activeContext,
                                activeGenre
                            )}
                            activeMarker={activeGenre}
                            onTimespanSwitch={this.props.onTimespanSwitch}
                            contextDropDown
                            padded
                        />
                    </div>
                </header>
                <div className="row expanded column small-24">
                    {this.renderFeed(
                        adminRecent,
                        featured,
                        location,
                        currentUser
                    )}
                    {emptyState}
                    {loader}
                </div>
            </div>
        );
    }
}
