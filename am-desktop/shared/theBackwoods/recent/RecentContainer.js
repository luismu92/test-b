import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import connectDataFetchers from 'lib/connectDataFetchers';

import { timeMap } from 'constants/index';
import { passiveOption } from 'utils/index';

import {
    nextPage,
    fetchSongs,
    setGenre,
    setPage
} from '../../redux/modules/theBackwoods/recent';

import Recent from './Recent';

const SCROLL_THRESHOLD = 300;

class RecentContainer extends Component {
    static propTypes = {
        genreUrl: PropTypes.string,
        currentUser: PropTypes.object.isRequired,
        adminRecent: PropTypes.object,
        player: PropTypes.object,
        location: PropTypes.object,
        dispatch: PropTypes.func,
        featured: PropTypes.object
    };

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidMount() {
        // const { dispatch, ad } = this.props;
        const option = passiveOption();

        // When landing on the home page, we allow 12 minutes to go by
        // before displaying an ad
        // if (!ad.lastDisplayed && !Cookie.get(cookies.lastAdDisplayTime)) {
        //     dispatch(setLastDisplayTime(Date.now()));
        // }

        window.addEventListener('scroll', this.handleWindowScroll, option);
    }

    componentDidUpdate(prevProps) {
        this.refetchIfNecessary(prevProps, this.props);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleWindowScroll);
        this._scrollTimer = null;
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleWindowScroll = () => {
        const { adminRecent } = this.props;
        const { loading, onLastPage } = adminRecent;

        clearTimeout(this._scrollTimer);
        this._scrollTimer = setTimeout(() => {
            if (loading || onLastPage) {
                return;
            }

            if (
                document.body.clientHeight -
                    (window.innerHeight + window.pageYOffset) <
                SCROLL_THRESHOLD
            ) {
                this.fetchNextPage();
            }
        }, 200);
    };

    //////////////////////
    // Internal methods //
    //////////////////////

    fetchNextPage = () => {
        const { dispatch } = this.props;

        dispatch(nextPage());
        dispatch(fetchSongs());
    };

    refetchIfNecessary(currentProps, nextProps) {
        const { params } = currentProps.match;
        const { params: nextParams } = nextProps.match;
        const changedGenre = currentProps.genreUrl !== nextProps.genreUrl;
        const changedPage = params.page !== nextParams.page;

        const { dispatch } = currentProps;
        const options = {};

        if (changedPage && !changedGenre && params.page > nextParams.page) {
            options.prependNewData = true;
        }

        if (changedGenre || changedPage) {
            if (changedGenre) {
                dispatch(setGenre(nextProps.genreUrl));
            }

            if (changedPage) {
                dispatch(setPage(nextParams.page));
            }

            dispatch(fetchSongs(options));
        }
    }

    render() {
        const {
            currentUser,
            adminRecent,
            player,
            featured,
            location
        } = this.props;

        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <Recent
                    currentUser={currentUser}
                    adminRecent={adminRecent}
                    player={player}
                    timeMap={timeMap}
                    featured={featured}
                    location={location}
                />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser,
        adminRecent: state.adminRecent,
        player: state.player,
        featured: state.featured
    };
}

export default withRouter(
    connect(mapStateToProps)(
        connectDataFetchers(RecentContainer, [
            (params, query, props) => setGenre(props.genreUrl),
            () => fetchSongs()
        ])
    )
);
