import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';

import AndroidLoader from '../../loaders/AndroidLoader';
import PaperTable from '../components/AdminPaper';
import MusicDetailContainer from '../../browse/MusicDetailContainer';

class VerifiedFeed extends Component {
    static propTypes = {
        verifiedUsers: PropTypes.object,
        loading: PropTypes.bool,
        error: PropTypes.object
    };

    renderTop() {
        const { error } = this.props;

        return (
            <Fragment>
                <h5>List of verified artists</h5>
                {error ? <p>{error}</p> : ''}
            </Fragment>
        );
    }

    renderArtistsVerified(artists = []) {
        const list = artists.map((result, i) => {
            return (
                <div className="u-spacing-bottom-60" key={i}>
                    <MusicDetailContainer
                        index={i}
                        item={result}
                        musicList={artists}
                        shouldLinkArtwork
                        hideLoadMoreTracksButton
                        large
                        allowZoom
                        removeHeaderLinks
                    />
                </div>
            );
        });

        const nullVerified = <p>There aren't verified Artists </p>;

        return <div>{list.length === 0 ? nullVerified : list}</div>;
    }

    renderContent() {
        const { loading, error, verifiedUsers } = this.props;
        const verifiedUsersArr = verifiedUsers.results;
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const listVerifiedUsers = (
            <Fragment>
                {loader}
                {error ? (
                    <p>{error}</p>
                ) : (
                    this.renderArtistsVerified(verifiedUsersArr)
                )}
            </Fragment>
        );

        return listVerifiedUsers;
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Verified Artists'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default VerifiedFeed;
