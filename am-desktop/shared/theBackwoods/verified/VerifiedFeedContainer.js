import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { getVerifiedFeed } from '../../redux/modules/theBackwoods/verified';

import VerifiedFeed from './VerifiedFeed';

class VerifiedFeedContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminVerified: PropTypes.object
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getVerifiedFeed());
    }

    render() {
        const {
            adminVerified: { verifiedUsers, loading, error }
        } = this.props;

        return (
            <VerifiedFeed
                verifiedUsers={verifiedUsers}
                loading={loading}
                error={error}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminVerified: state.adminVerified
    };
}

export default compose(connect(mapStateToProps))(VerifiedFeedContainer);
