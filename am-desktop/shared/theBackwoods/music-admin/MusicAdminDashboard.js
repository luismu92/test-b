import React, { Component, Fragment } from 'react';

import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import PaperTable from '../components/AdminPaper';
import FeedBar from '../../widgets/FeedBar';
import SearchIcon from '../../icons/search';
import AndroidLoader from '../../loaders/AndroidLoader';
import MusicDetailContainer from '../../browse/MusicDetailContainer';
import classnames from 'classnames';
import MicIcon from '../../icons/mic';

import styles from './MusicAdmin.module.scss';

class MusicAdminDashboard extends Component {
    static propTypes = {
        onSearch: PropTypes.func,
        musics: PropTypes.array,
        loading: PropTypes.bool,
        errorContent: PropTypes.string,
        context: PropTypes.string
    };

    handleSearch = (e) => {
        e.preventDefault();
        const { onSearch } = this.props;
        const form = e.currentTarget;

        onSearch(form.searchValue.value);
    };

    empty() {
        const { musics, errorContent } = this.props;
        let value = null;
        // import styles
        const emptyIcon = classnames(styles.bigIcon, 'empty-state__icon');
        const emptyMessage = classnames(styles.bigMessage, 'u-text-center');

        if (errorContent) {
            value = (
                <Fragment>
                    <p className={emptyMessage}>{'No Musics to show'}.</p>
                </Fragment>
            );
        }

        if (musics.length === 0) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <MicIcon />
                    </span>
                    <p className={emptyMessage}>{'No Musics to show'}.</p>
                </Fragment>
            );
        }

        return value;
    }

    renderTop() {
        return (
            <Fragment>
                <div className={styles.adminTop}>
                    <div>
                        <h5>Music Admin</h5>
                        <p>Manage music by admin.</p>
                    </div>
                </div>
                <form onSubmit={this.handleSearch}>
                    <div className={styles.searchBox}>
                        <input
                            type="text"
                            name="searchValue"
                            placeholder="Search a Music!"
                        />
                        <button type="submit">
                            <SearchIcon />
                        </button>
                    </div>
                </form>
            </Fragment>
        );
    }

    renderContent() {
        const { loading, context, musics } = this.props;

        let loader;
        let content;
        const emptyOrError = this.empty();

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const contextItems = [
            { value: 'song', text: 'Musics' },
            { value: 'all', text: 'All' }
        ].map((item) => {
            return {
                ...item,
                active: item.value === context
            };
        });

        switch (context) {
            case 'song':
                content = musics.map((musicItem, i) => {
                    return (
                        <div
                            className="u-spacing-top-60"
                            key={`${musicItem.id}-${i}`}
                        >
                            <MusicDetailContainer
                                search
                                index={i}
                                item={musicItem}
                                musicList={musics}
                                shouldLinkArtwork
                                feed
                            />
                        </div>
                    );
                });
                break;
            default:
                break;
        }

        const view = (
            <Fragment>
                <FeedBar
                    className="dashboard-feed-bar"
                    activeMarker={context}
                    items={contextItems}
                    showBorderMarker
                />
                {loader}
                {content ? content : emptyOrError}
            </Fragment>
        );

        return view;
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Music'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default MusicAdminDashboard;
