import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { searchMusic } from '../../redux/modules/theBackwoods/music';

import MusicAdminDashboard from './MusicAdminDashboard';

class MusicAdminContainer extends Component {
    static propTypes = {
        musicSong: PropTypes.object,
        adminMusics: PropTypes.object,
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            context: 'song'
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(searchMusic(''));
    }

    handleGetLists = (value) => {
        const { dispatch } = this.props;

        this.setState({
            context: value
        });

        switch (value) {
            case 'song':
                dispatch(searchMusic());
                break;
            default:
                break;
        }
    };

    handleSearch = (search) => {
        const { dispatch } = this.props;

        dispatch(searchMusic(search));
    };

    render() {
        const {
            adminMusics: { musics, loading, errorContent }
        } = this.props;

        const { context } = this.state;

        return (
            <MusicAdminDashboard
                onSearch={this.handleSearch}
                musics={musics}
                context={context}
                loading={loading}
                errorContent={errorContent}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        musicSong: state.musicSong,
        adminMusics: state.adminMusics
    };
}

export default connect(mapStateToProps)(MusicAdminContainer);
