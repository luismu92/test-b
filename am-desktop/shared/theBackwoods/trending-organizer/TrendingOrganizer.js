import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import classnames from 'classnames';

import AndroidLoader from '../../loaders/AndroidLoader';
import PaperTable from '../components/AdminPaper';

import DragHandle from 'icons/drag-handle';

import FixedActionBarContainer from '../../components/FixedActionBarContainer';
import DraggableList from '../../list/DraggableList';
import MusicEditTrackContainer from '../../edit/MusicEditTrackContainer';
import FeedBar from '../../widgets/FeedBar';

import styles from './TrendingOrganizer.module.scss';

import CloseIcon from 'icons/close-thin';
import TrashIcon from 'icons/trash-solid';
import PlusIcon from '../../icons/plus';
import QueueNextIcon from '../../icons/queue-next';
import QueueEndIcon from '../../icons/queue-end';

import {
    allGenresMap,
    GENRE_TYPE_RAP,
    GENRE_TYPE_RNB,
    GENRE_TYPE_ELECTRONIC,
    GENRE_TYPE_DANCEHALL,
    GENRE_TYPE_PODCAST,
    GENRE_TYPE_POP,
    GENRE_TYPE_AFROBEATS,
    GENRE_TYPE_LATIN,
    GENRE_TYPE_INSTRUMENTAL
} from 'constants/index';

export default class TrendingOrganizer extends Component {
    static propTypes = {
        context: PropTypes.string,
        genre: PropTypes.string,
        updateButtonText: PropTypes.string,
        updatingButtonText: PropTypes.string,
        trendingList: PropTypes.array,
        loading: PropTypes.bool,
        error: PropTypes.string,
        dirty: PropTypes.bool,
        isUpdating: PropTypes.bool,
        onTrackRemoveClick: PropTypes.func,
        onTrackReorder: PropTypes.func,
        selectedItems: PropTypes.array,
        onItemSelect: PropTypes.func,
        onMoveToTopClick: PropTypes.func,
        onMoveToBottomClick: PropTypes.func,
        onClearSelectedItemsClick: PropTypes.func,
        onContextSwitch: PropTypes.func,
        onGenreChange: PropTypes.func,
        addMusicInput: PropTypes.string,
        addMusicError: PropTypes.string,
        onAddMusicInputChange: PropTypes.func,
        onAddMusic: PropTypes.func,
        onSave: PropTypes.func
    };

    static defaultProps = {
        updateButtonText: 'Confirm & Save',
        updatingButtonText: 'Updating Trending...'
    };

    componentDidUpdate(prevProps) {
        const currentList = this.props.trendingList;
        const lastList = prevProps.trendingList;

        const changedIds =
            currentList.length !== lastList.length ||
            currentList.some((item, i) => item.id !== lastList[i].id);

        if (changedIds && this.DraggableList) {
            this.DraggableList.forceUpdateGrid();
        }
    }

    handleRowHeight = () => {
        return 56;
    };

    renderContextNav(context) {
        const organizer = {
            text: 'Organizer (live)',
            active: context === 'organizer',
            value: 'organizer'
        };
        const scheduler = {
            text: 'Scheduler (tomorrow)',
            active: context === 'scheduler',
            value: 'scheduler'
        };

        return (
            <FeedBar
                className="column small-24 medium-18"
                activeMarker={context}
                items={[organizer, scheduler]}
                onContextSwitch={this.props.onContextSwitch}
                showBorderMarker
            />
        );
    }

    renderGenreNav(genre) {
        const items = [
            {
                text: 'All Genres',
                value: 'global'
            },
            {
                text: allGenresMap[GENRE_TYPE_RAP],
                value: GENRE_TYPE_RAP
            },
            {
                text: allGenresMap[GENRE_TYPE_RNB],
                value: GENRE_TYPE_RNB
            },
            {
                text: allGenresMap[GENRE_TYPE_ELECTRONIC],
                value: GENRE_TYPE_ELECTRONIC
            },
            {
                text: allGenresMap[GENRE_TYPE_DANCEHALL],
                value: GENRE_TYPE_DANCEHALL
            },
            {
                text: allGenresMap[GENRE_TYPE_POP],
                value: GENRE_TYPE_POP
            },
            {
                text: allGenresMap[GENRE_TYPE_AFROBEATS],
                value: GENRE_TYPE_AFROBEATS
            },
            {
                text: allGenresMap[GENRE_TYPE_PODCAST],
                value: GENRE_TYPE_PODCAST
            },
            {
                text: allGenresMap[GENRE_TYPE_LATIN],
                value: GENRE_TYPE_LATIN
            },
            {
                text: allGenresMap[GENRE_TYPE_INSTRUMENTAL],
                value: GENRE_TYPE_INSTRUMENTAL
            }
        ].map((item) => {
            return {
                ...item,
                active: item.value === genre
            };
        });

        return (
            <FeedBar
                className="column small-24 medium-6"
                activeMarker={genre}
                items={items}
                onContextSwitch={this.props.onGenreChange}
                contextDropDown
            />
        );
    }

    renderActionButtons() {
        const {
            isUpdating,
            dirty,
            updateButtonText,
            updatingButtonText,
            onSave
        } = this.props;

        const updateText = isUpdating ? updatingButtonText : updateButtonText;
        const disabledUpdate = isUpdating || !dirty;

        return (
            <FixedActionBarContainer>
                <button
                    onClick={onSave}
                    disabled={disabledUpdate}
                    type="button"
                    className="button button--pill button--large u-tt-cap"
                >
                    {updateText}
                </button>
            </FixedActionBarContainer>
        );
    }

    renderSortToolbar() {
        const { selectedItems, onClearSelectedItemsClick } = this.props;

        const label = selectedItems.length > 1 ? 'Items' : 'Item';

        const klass = classnames('sort-toolbar u-padding-x-30', {
            'sort-toolbar--active': selectedItems.length
        });

        return (
            <div className={klass}>
                <div className="sort-toolbar__inner">
                    <div className="sort-toolbar__count">
                        <p className="u-fw-600 u-ls-n-05">
                            <span className="u-text-white u-bg-orange u-lh-17">
                                {selectedItems.length}
                            </span>{' '}
                            {label} Selected
                        </p>
                    </div>
                    <div className="sort-toolbar__buttons u-text-center">
                        <button
                            className="sort-toolbar__button sort-toolbar__button--top"
                            title="Move to top"
                            data-tooltip="Move to top"
                            onClick={this.props.onMoveToTopClick}
                        >
                            <QueueNextIcon />
                        </button>
                        <button
                            className="sort-toolbar__button sort-toolbar__button--delete"
                            title="Delete"
                            data-tooltip="Delete"
                            onClick={this.props.onTrackRemoveClick}
                        >
                            <TrashIcon />
                        </button>
                        <button
                            className="sort-toolbar__button sort-toolbar__button--bottom"
                            title="Move to bottom"
                            data-tooltip="Move to bottom"
                            onClick={this.props.onMoveToBottomClick}
                        >
                            <QueueEndIcon />
                        </button>
                    </div>
                    <div className="sort-toolbar__clear-wrap u-text-right">
                        <button
                            className="sort-toolbar__button sort-toolbar__button--clear"
                            onClick={onClearSelectedItemsClick}
                            data-tooltip="Clear selected items"
                            data-tooltip-bottom-right
                            title="Clear selected items"
                        >
                            <CloseIcon />
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    renderList() {
        const { trendingList } = this.props;
        const trackList = (trendingList || [])
            .filter((track) => {
                return !track.isDeleting;
            })
            .map((track, i) => {
                return (
                    <MusicEditTrackContainer
                        key={`${track.id}:${i}`}
                        index={i}
                        track={track}
                        showAlbumIndicator={true}
                        titleLink={true}
                        onRemoveClick={this.props.onTrackRemoveClick}
                        musicItem={track}
                    />
                );
            });

        const draggingClass = classnames(
            'playlist-edit-track--dragging u-dragging'
        );
        return (
            <div className={styles.editContainer}>
                <div className="row">
                    <div className="column small-24">
                        {this.renderAdd()}
                        {this.renderActionButtons()}
                        <DraggableList
                            containerProps={{
                                className: 'small-24 u-bg-white u-padding-x-20'
                            }}
                            dragHandle={
                                <span className="u-drag-handle u-drag-handle--playlist u-text-gray9">
                                    <DragHandle />
                                </span>
                            }
                            draggingClass={draggingClass}
                            onDragFinish={this.props.onTrackReorder}
                            items={trackList}
                            onRowHeight={this.handleRowHeight}
                            useWindowAsScrollContainer
                            multiSelect
                            selectedItems={this.props.selectedItems}
                            onItemSelect={this.props.onItemSelect}
                            ref={(instance) => {
                                this.DraggableList = instance;
                            }}
                        />
                    </div>
                </div>
                {this.renderSortToolbar()}
            </div>
        );
    }

    renderAdd() {
        const {
            addMusicInput,
            addMusicError,
            onAddMusicInputChange,
            onAddMusic
        } = this.props;
        return (
            <div className="column small-24 u-spacing-bottom">
                <p>
                    Add a new song or album by entering their URL in the search
                    box.
                </p>
                <div className={styles.searchBox}>
                    <input
                        type="url"
                        placeholder="https://audiomack.com/album/author-name/song-name"
                        value={addMusicInput}
                        onChange={onAddMusicInputChange}
                    />
                    <button disabled={!addMusicInput} onClick={onAddMusic}>
                        <PlusIcon />
                    </button>
                </div>
                <p className={styles.errorMessage}>{addMusicError}</p>
            </div>
        );
    }

    renderTable() {
        const { loading, error, context, genre } = this.props;
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const topPaper = (
            <div className="row">
                {this.renderContextNav(context)}
                {this.renderGenreNav(genre)}
            </div>
        );

        const contentPaper = (
            <Fragment>
                {loader}
                {error ? <p>{error}</p> : this.renderList()}
            </Fragment>
        );

        return (
            <PaperTable
                title={'Trending Management'}
                topPaper={topPaper}
                contentPaper={contentPaper}
            />
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                {this.renderTable()}
            </Fragment>
        );
    }
}
