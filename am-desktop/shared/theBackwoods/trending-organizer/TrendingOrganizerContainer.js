import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import requireAuth from '../../hoc/requireAuth';
import {
    setList,
    getList,
    setContext,
    getMusic,
    saveList
} from '../../redux/modules/theBackwoods/trendingOrganizer';
import { addToast } from '../../redux/modules/toastNotification';

import TrendingOrganizer from './TrendingOrganizer';

class TrendingOrganizerContainer extends Component {
    static propTypes = {
        context: PropTypes.string,
        genre: PropTypes.string,
        dispatch: PropTypes.func,
        trendingOrganizer: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            isUpdating: false,
            dirty: false,
            errors: {},
            trendingListLocal: props.trendingOrganizer.trendingList || [],
            selectedItems: [],
            rangeStart: null,
            addMusicInput: ''
        };
    }

    componentDidMount() {
        this.getList();
    }

    componentDidUpdate(prevProps) {
        if (this.props.trendingOrganizer !== prevProps.trendingOrganizer) {
            // eslint-disable-next-line
            this.setState({
                trendingListLocal: this.props.trendingOrganizer.trendingList,
                selectedItems: [],
                dirty: false
            });
        }

        if (
            this.props.context !== prevProps.context ||
            this.props.genre !== prevProps.genre
        ) {
            this.getList();
        }
    }

    handleTrackRemoveClick = (e) => {
        const { selectedItems } = this.state;

        let musicId;

        if (selectedItems.length) {
            const selectedItemIds = selectedItems.map(
                (item) => item.props.track.id
            );

            musicId = selectedItemIds;
        } else {
            musicId = e.currentTarget.getAttribute('data-music-id');
        }

        this.deleteItem(musicId);
    };

    handleTrackReorder = (fromIndex, toIndex) => {
        const { trendingListLocal, selectedItems } = this.state;
        let newTracks = Array.from(trendingListLocal);

        if (selectedItems.length) {
            const selectedItemObj = selectedItems.map(
                (item) => item.props.track
            );
            const selectedItemIds = selectedItems.map(
                (item) => item.props.track.id
            );

            const items = this.state.trendingListLocal.filter(
                (value) => !selectedItemIds.includes(value.id)
            );

            newTracks = [
                ...items.slice(0, toIndex),
                ...selectedItemObj,
                ...items.slice(toIndex, items.length)
            ];
        } else {
            const fromTrack = newTracks.splice(fromIndex, 1)[0];
            newTracks.splice(toIndex, 0, fromTrack);
        }

        this.setState({
            dirty: true,
            trendingListLocal: newTracks
        });
    };

    handleMoveToTopClick = () => {
        this.handleTrackReorder(null, 0);
    };

    handleMoveToBottomClick = () => {
        const { selectedItems } = this.state;

        const selectedItemIds = selectedItems.map((item) => item.id);

        const items = this.state.trendingListLocal.filter(
            (value) => !selectedItemIds.includes(value.id)
        );

        this.handleTrackReorder(null, items.length);
    };

    handleItemSelect = (item, list, e) => {
        const { rangeStart } = this.state;

        if (e.nativeEvent.shiftKey && rangeStart) {
            if (item.props.index < rangeStart) {
                this.selectRange(
                    this.setRange(item.props.index, rangeStart),
                    list
                );
            } else {
                this.selectRange(
                    this.setRange(rangeStart, item.props.index),
                    list
                );
            }
        }
        // No range start is defined
        else {
            this.setState({
                rangeStart: item.props.index
            });

            this.selectItems(item);
        }
    };

    handleClearSelectedItemsClick = () => {
        this.setState({
            selectedItems: []
        });
    };

    handleContextSwitch = (context) => {
        const { dispatch } = this.props;
        dispatch(setContext(context, this.props.genre));
    };

    handleGenreChange = (genre) => {
        const { dispatch } = this.props;
        dispatch(setContext(this.props.context, genre));
    };

    handleAddMusicInputChange = (e) => {
        this.setState({
            addMusicInput: e.target.value
        });
    };

    handleAddMusic = () => {
        const { dispatch } = this.props;

        dispatch(getMusic(this.state.addMusicInput))
            .then((result) => {
                const newItem = result.resolved;
                const { trendingListLocal } = this.state;

                let newList = Array.from(trendingListLocal);
                newList = newList.filter((music) => music.id !== newItem.id);
                newList.unshift(newItem);

                this.setState({
                    trendingListLocal: newList,
                    selectedItems: [],
                    addMusicInput: '',
                    dirty: true
                });

                return;
            })
            .catch((err) => {
                this.setState({
                    addMusicError: err.message
                });
            });
    };

    handleSave = () => {
        this.setState({
            isUpdating: true
        });

        const { dispatch, context, genre } = this.props;
        const { trendingListLocal } = this.state;
        const listIds = trendingListLocal.map((item) => item.id);

        // Save the list to state
        dispatch(setList(trendingListLocal));

        // Save to backend
        dispatch(saveList(context, genre, listIds))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        item: `Trending ${context} for ${genre} saved successfully!`
                    })
                );

                this.setState({
                    isUpdating: false
                });
                return;
            })
            .catch((error) => {
                this.setState({
                    isUpdating: false
                });
                console.log(error);
            });
    };

    getList() {
        const { dispatch, context, genre } = this.props;

        // The list should return based on context and date
        dispatch(getList(context, genre));
    }

    // Set a range of integers based on rangeStart and rangeEnd
    setRange(start, end) {
        return Array(end - start + 1)
            .fill()
            .map((_, idx) => start + idx);
    }

    deleteItem(musicIds) {
        const { selectedItems } = this.state;

        this.setState((prevState) => {
            const newTrendingListLocal = prevState.trendingListLocal.filter(
                (track) => {
                    if (selectedItems.length) {
                        return musicIds.indexOf(track.id) === -1;
                    }

                    return track.id !== parseInt(musicIds, 10);
                }
            );

            return {
                ...prevState,
                trendingListLocal: newTrendingListLocal,
                selectedItems: [],
                dirty: true
            };
        });
    }

    // Original select items function
    selectItems(item) {
        this.setState(({ selectedItems }) => {
            const selectedItemIds = selectedItems.map(
                (track) => track.props.track.id
            );

            if (selectedItemIds.includes(item.props.track.id)) {
                return {
                    selectedItems: selectedItems.filter(
                        (value) => value.props.track.id !== item.props.track.id
                    )
                };
            }

            return {
                selectedItems: [...selectedItems, item]
            };
        });
    }

    render() {
        const {
            trendingOrganizer: { loading, error }
        } = this.props;

        return (
            <Fragment>
                <TrendingOrganizer
                    trendingList={this.state.trendingListLocal}
                    loading={loading}
                    isUpdating={this.state.isUpdating}
                    error={error}
                    dirty={this.state.dirty}
                    onTrackReorder={this.handleTrackReorder}
                    onTrackRemoveClick={this.handleTrackRemoveClick}
                    selectedItems={this.state.selectedItems}
                    onItemSelect={this.handleItemSelect}
                    onMoveToTopClick={this.handleMoveToTopClick}
                    onMoveToBottomClick={this.handleMoveToBottomClick}
                    onClearSelectedItemsClick={
                        this.handleClearSelectedItemsClick
                    }
                    context={this.props.context}
                    genre={this.props.genre}
                    onContextSwitch={this.handleContextSwitch}
                    onGenreChange={this.handleGenreChange}
                    addMusicInput={this.state.addMusicInput}
                    addMusicError={this.state.addMusicError}
                    onAddMusicInputChange={this.handleAddMusicInputChange}
                    onAddMusic={this.handleAddMusic}
                    onSave={this.handleSave}
                />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        context: state.adminTrendingOrganizer.context,
        genre: state.adminTrendingOrganizer.genre,
        trendingOrganizer: state.adminTrendingOrganizer
    };
}

export default compose(
    requireAuth,
    connect(mapStateToProps)
)(TrendingOrganizerContainer);
