import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TaskTable from './StatsSwapTaskTable';
import SubTable from './StatsSwapSubTable';
import MigrateUrl from './StatsSwapMigrateUrl';
import MigrateId from './StatsSwapMigrateId';
import {
    getTasks,
    getSubTasks,
    migrateByUrl,
    getSearchData,
    migrateById,
    nextPagination,
    previousPagination
} from '../../redux/modules/theBackwoods/statsSwap';
import { addToast } from '../../redux/modules/toastNotification';

class StatsSwapContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminStatsSwap: PropTypes.object,
        showValuesMap: PropTypes.object
    };

    static defaultProps = {
        showValuesMap: {
            task: 'taskTable',
            subtask: 'subTable',
            migrateUrl: 'migrateUrl',
            migrateId: 'migrateId'
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            show: this.props.showValuesMap.task,
            searchKey: 0
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;

        dispatch(getTasks());
    }

    handleNextPagination = (next) => {
        const { dispatch } = this.props;
        dispatch(nextPagination(next));
    };

    handlePreviousPagination = (previous) => {
        const { dispatch } = this.props;
        dispatch(previousPagination(previous));
    };

    handleShowSub = (taskId) => {
        const { dispatch, showValuesMap } = this.props;

        dispatch(getSubTasks(taskId))
            .then(() => {
                this.setState({
                    show: showValuesMap.subtask
                });
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'There was an error trying to get task details'
                    })
                );
            });
    };

    handleShowMigrateUrl = () => {
        this.setState({
            show: this.props.showValuesMap.migrateUrl
        });
    };

    handleShowMigrateId = () => {
        this.setState({
            show: this.props.showValuesMap.migrateId
        });
    };

    handleShowTask = () => {
        const { dispatch, showValuesMap } = this.props;
        this.setState(
            {
                show: showValuesMap.task
            },
            () => {
                dispatch(getTasks());
            }
        );
    };

    handleMigrateByUrl = (targetUrl, sourceUrls, reUp) => {
        const { dispatch } = this.props;
        dispatch(migrateByUrl(targetUrl, sourceUrls, reUp))
            .then(() => {
                this.handleShowTask();
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message:
                            'There was an error trying to migrate song data'
                    })
                );
            });
    };

    handleMigrateById = (targetId, sourceIds, reUp) => {
        const { dispatch } = this.props;
        dispatch(migrateById(targetId, sourceIds, reUp))
            .then(() => {
                this.handleShowTask();
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message:
                            'There was an error trying to migrate song data'
                    })
                );
            });
    };

    handleSearch = (value) => {
        const { dispatch } = this.props;
        dispatch(getSearchData(value))
            .then(() => {
                this.setState({
                    searchKey: this.state.searchKey + 1
                });
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'There was an error searching the music'
                    })
                );
            });
    };

    render() {
        const {
            adminStatsSwap: {
                tasks,
                subTasks,
                loading,
                errorContent,
                migrateSongs,
                next,
                previous
            },
            showValuesMap
        } = this.props;
        const { show, searchKey } = this.state;
        let content;

        switch (show) {
            case showValuesMap.task:
                content = (
                    <TaskTable
                        tasks={tasks}
                        loading={loading}
                        errorContent={errorContent}
                        onShowSubs={this.handleShowSub}
                        onMigrateUrl={this.handleShowMigrateUrl}
                        onMigrateId={this.handleShowMigrateId}
                        onPreviousPagination={this.handlePreviousPagination}
                        onNextPagination={this.handleNextPagination}
                        previous={previous}
                        next={next}
                    />
                );
                break;

            case showValuesMap.subtask:
                content = (
                    <SubTable
                        tasks={tasks}
                        subTasks={subTasks}
                        loading={loading}
                        errorContent={errorContent}
                        onShowTasks={this.handleShowTask}
                    />
                );
                break;

            case showValuesMap.migrateUrl:
                content = (
                    <MigrateUrl
                        loading={loading}
                        errorContent={errorContent}
                        onShowTasks={this.handleShowTask}
                        onMigrateUrl={this.handleMigrateByUrl}
                    />
                );
                break;

            case showValuesMap.migrateId:
                content = (
                    <MigrateId
                        loading={loading}
                        migrateSongs={migrateSongs}
                        errorContent={errorContent}
                        onShowTasks={this.handleShowTask}
                        onSearch={this.handleSearch}
                        onMigrateId={this.handleMigrateById}
                        key={`search:${searchKey}`}
                    />
                );
                break;

            default:
                content = (
                    <TaskTable
                        tasks={tasks}
                        loading={loading}
                        errorContent={errorContent}
                    />
                );
                break;
        }

        return content;
    }
}

function mapStateToProps(state) {
    return {
        adminStatsSwap: state.adminStatsSwap
    };
}

export default connect(mapStateToProps)(StatsSwapContainer);
