import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './StatsSwapMigrateId.module.scss';
import MusicDetailContainer from '../../browse/MusicDetailContainer';

export default class MigrateIdItem extends Component {
    static propTypes = {
        song: PropTypes.object,
        allSongs: PropTypes.array,
        position: PropTypes.number,
        targetSelected: PropTypes.bool,
        onSelect: PropTypes.func
    };

    handleCheck = (e) => {
        const { onSelect, position } = this.props;
        onSelect(e, position);
    };

    render() {
        const { song, targetSelected, position, allSongs } = this.props;
        const { source, target } = song;

        return (
            <tbody>
                <tr>
                    <td data-th="Song" className={styles.song}>
                        <MusicDetailContainer
                            index={position}
                            item={song}
                            musicList={allSongs}
                            shouldLinkArtwork
                            hideLoadMoreTracksButton
                            removeHeaderLinks
                        />
                    </td>
                    <td data-th="Target" className={styles.target}>
                        <input
                            title="Target"
                            name="target"
                            type="checkbox"
                            checked={target}
                            disabled={targetSelected && !target}
                            onChange={this.handleCheck}
                        />
                    </td>
                    <td data-th="Source" className={styles.source}>
                        <input
                            title="Source"
                            name="source"
                            type="checkbox"
                            checked={source}
                            onChange={this.handleCheck}
                        />
                    </td>
                </tr>
            </tbody>
        );
    }
}
