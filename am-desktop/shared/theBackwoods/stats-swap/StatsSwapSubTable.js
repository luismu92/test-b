import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { ucfirst } from 'utils/index';
import moment from 'moment';

import PaperTable from '../components/AdminPaper';
import Table from '../../components/Table';

import AndroidLoader from '../../loaders/AndroidLoader';
import CloseIcon from '../../icons/close-solid';
import CheckIcon from '../../icons/check-mark';
import WarningIcon from '../../icons/warning';
import TaskIcon from '../../icons/feed';
import styles from './StatsSwapSub.module.scss';

class StatsSwapSubTable extends Component {
    static propTypes = {
        tasks: PropTypes.array,
        subTasks: PropTypes.array,
        loading: PropTypes.bool,
        onShowTasks: PropTypes.func
    };

    getHeaderCells() {
        return [
            {
                value: 'Sub Task ID',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Date',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Update',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Status',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Plays',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Artist',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Source Song',
                props: {
                    className: 'flex-2'
                }
            }
        ];
    }

    statusIcon(status) {
        let icon;

        switch (status.toLowerCase()) {
            case 'complete': {
                icon = (
                    <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                        <CheckIcon className="u-text-icon u-margin-0 status-icon status-icon--active" />
                    </span>
                );
                break;
            }

            case 'failure': {
                icon = (
                    <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                        <CloseIcon className="u-text-icon u-margin-0 u-text-red" />
                    </span>
                );
                break;
            }

            default: {
                icon = (
                    <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                        <WarningIcon className="u-text-icon u-margin-0 u-text-yellow" />
                    </span>
                );
                break;
            }
        }

        return <span className={styles.status}>{icon}</span>;
    }

    formatDate(timestamp) {
        return moment(timestamp).format('MM-DD-YYYY');
    }

    subItems(subTask, key) {
        const {
            sub_task_id,
            created,
            updated,
            sub_task_status,
            play,
            source_song_name,
            source_artist
        } = subTask;

        return (
            <tbody key={`sub${key}`}>
                <tr className={styles.rows}>
                    <td data-th="Task ID" className="flex-1">
                        {sub_task_id}
                    </td>
                    <td data-th="Date" className="flex-1">
                        {this.formatDate(created)}
                    </td>
                    <td data-th="Update" className="flex-1">
                        {this.formatDate(updated)}
                    </td>
                    <td data-th="Status" className="flex-1">
                        {this.statusIcon(sub_task_status)}
                    </td>
                    <td data-th="Play" className="flex-1">
                        {play}
                    </td>
                    <td data-th="Artist" className="flex-1">
                        {source_artist}
                    </td>
                    <td data-th="Source Song" className="flex-2">
                        {source_song_name}
                    </td>
                </tr>
            </tbody>
        );
    }

    emptyOrError() {
        const { subTasks } = this.props;
        let value = null;

        const emptyIcon = classnames(styles.bigIcon, 'empty-state__icon');
        const emptyMessage = classnames(styles.bigMessage, 'u-text-center');

        if (subTasks.length === 0) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <TaskIcon />
                    </span>
                    <p className={emptyMessage}>
                        {'No migrate Sub Tasks to show'}.
                    </p>
                </Fragment>
            );
        }

        return value;
    }

    taskInfo() {
        const { tasks, subTasks } = this.props;
        const parent_task = subTasks[0].parent_task_id;
        const task = tasks.filter((item) => {
            return item.task_id === parent_task;
        });

        return (
            <div className={styles.info}>
                <h4>Task</h4>
                <div className={styles.desc}>
                    <p>
                        <span>Task ID:</span>
                        {task[0].task_id}
                    </p>
                    <p>
                        <span>Created:</span>
                        {this.formatDate(task[0].created)}
                    </p>
                    <p>
                        <span>Update:</span>
                        {this.formatDate(task[0].updated)}
                    </p>
                </div>
            </div>
        );
    }

    targetInfo() {
        const { subTasks } = this.props;
        const { target_song_name, target_artist, re_up } = subTasks[0];
        return (
            <div className={styles.info}>
                <h4 className={styles.subTitle}>Target</h4>
                <div className={styles.desc}>
                    <p>
                        <span>Song Name:</span>
                        {target_song_name}
                    </p>
                    <p>
                        <span>Artist:</span>
                        {target_artist}
                    </p>
                    <p>
                        <span>Re Up:</span>
                        {ucfirst(re_up)}
                    </p>
                </div>
            </div>
        );
    }

    table() {
        const { subTasks } = this.props;
        const rows = subTasks.map((item, i) => {
            return {
                model: item,
                value: this.subItems(item, i)
            };
        });

        const table = (
            <Table
                className={styles.table}
                headerCells={this.getHeaderCells()}
                bodyRows={rows}
                ignoreTbody
            />
        );

        return (
            <div>
                <h4 className={styles.subTitle}>Sub Tasks</h4>
                {table}
            </div>
        );
    }

    renderTop() {
        return (
            <div>
                <h5>Task Detail Info</h5>
                <p>
                    {`All stats migration is divided into mini tasks,a "Sub Task"
                    for each song, here you can check all the sub tasks related
                    to a specific "Task" among other useful data. To return just `}
                    <button
                        className="u-text-orange"
                        onClick={this.props.onShowTasks}
                    >
                        Go Back
                    </button>
                </p>
            </div>
        );
    }

    renderContent() {
        const { loading } = this.props;
        let content = this.emptyOrError();
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (content === null) {
            content = (
                <Fragment>
                    {this.taskInfo()}
                    {this.targetInfo()}
                    {this.table()}
                </Fragment>
            );
        }

        return (
            <div>
                {loader}
                {content}
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Song Migration'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default StatsSwapSubTable;
