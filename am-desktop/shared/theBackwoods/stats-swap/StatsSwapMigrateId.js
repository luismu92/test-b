import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PaperTable from '../components/AdminPaper';
import Table from '../../components/Table';
import SongItem from './StatsSwapMigrateIdItem';
import { smoothScroll } from 'utils/index';

import AndroidLoader from '../../loaders/AndroidLoader';
import CloseIcon from '../../icons/close-solid';
import SearchIcon from '../../icons/search';
import styles from './StatsSwapMigrateId.module.scss';

class StatsSwapMigrateId extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        migrateSongs: PropTypes.array,
        errorContent: PropTypes.string,
        onShowTasks: PropTypes.func,
        onSearch: PropTypes.func,
        onMigrateId: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            songs: Array.from(this.props.migrateSongs),
            songsPerPage: 20,
            page: 1,
            reUp: false,
            errorSong: null
        };
    }

    componentDidUpdate(prevProps, prevState) {
        const { errorSong } = this.state;
        const { errorContent } = this.props;
        if (
            prevState.errorSong !== errorSong ||
            prevProps.errorContent !== errorContent
        ) {
            smoothScroll(0, { speed: 500 });
        }
    }

    handleChange = (e) => {
        const name = e.target.name;
        let value = e.target.value;

        if (name === 'reUp') {
            value = e.target.checked;
        }

        this.setState({
            [name]: value
        });
    };

    handleSearch = (e) => {
        e.preventDefault();
        const { onSearch } = this.props;
        const form = e.currentTarget;
        const searchValue = form.searchValue.value;

        onSearch(searchValue);
    };

    handleSelect = (e, index) => {
        const { songs } = this.state;
        const checkBox = e.target.name;
        let allSourceSongs = [];

        if (checkBox === 'target') {
            songs[index] = {
                ...songs[index],
                source: !songs[index].target ? false : songs[index].source,
                target: !songs[index].target
            };
        }

        if (checkBox === 'source') {
            songs[index] = {
                ...songs[index],
                source: !songs[index].source,
                target: !songs[index].source ? false : songs[index].target
            };
        }

        if (checkBox === 'all') {
            const sourceAll = songs.some(
                (song) => song.source === false && song.target === false
            );
            allSourceSongs = songs.map((song) => {
                return {
                    ...song,
                    source: song.target ? false : sourceAll
                };
            });
        }

        this.setState({
            songs: allSourceSongs.length > 0 ? allSourceSongs : songs
        });
    };

    handleSubmit = () => {
        const { onMigrateId } = this.props;
        const { reUp, songs } = this.state;
        const valid = this.checkSubmit();

        if (valid) {
            const target = songs
                .filter((song) => song.target === true)
                .map((song) => song.id);
            const targetId = target[0];
            const sourceIds = songs
                .filter((song) => song.source === true)
                .map((song) => song.id);
            onMigrateId(targetId, sourceIds, reUp);
        }
    };

    handleNextPage = () => {
        const { page } = this.state;

        this.setState({
            page: page + 1
        });
    };

    handlePreviousPage = () => {
        const { page } = this.state;

        this.setState({
            page: page - 1
        });
    };

    getHeaderCells() {
        const { songs } = this.state;
        const sourceAll = songs.some(
            (song) => song.source === false && song.target === false
        );
        return [
            {
                value: 'Song'
            },
            {
                value: 'Target',
                props: {
                    className: 'flex-1 medium-text-center'
                }
            },
            {
                value: (
                    <Fragment>
                        Source
                        <input
                            type="checkbox"
                            name="all"
                            className={styles.selectAll}
                            checked={!sourceAll}
                            onChange={this.handleSelect}
                        />
                    </Fragment>
                ),
                props: {
                    className: 'flex-1 medium-text-center'
                }
            }
        ];
    }

    checkSubmit = () => {
        const { songs } = this.state;

        // Check if one target is selected
        const target = songs.some((song) => song.target === true);
        if (!target) {
            this.setState({
                errorSong: 'You need to select one target.'
            });
            return false;
        }

        // Check if on source is selected
        const source = songs.some((song) => song.source === true);
        if (!source) {
            this.setState({
                errorSong: 'You need to select at least one source song.'
            });
            return false;
        }

        // Check if there is less than 100 sources.
        const sourceRows = songs.filter((song) => song.source === true);
        if (sourceRows.length > 100) {
            this.setState({
                error: `Invalid number of source songs, actual: ${sourceRows} songs, please remove some.`
            });
            return false;
        }

        return true;
    };

    renderTop() {
        return (
            <div className={styles.top}>
                <h5>Migrate by Song Name</h5>
                <p>
                    {`To migrate stats, playlist and fav data from one song to another,
                    please search the song name and select the destination and the source songs.
                    To return just `}
                    <button
                        className="u-text-orange"
                        onClick={this.props.onShowTasks}
                    >
                        Go Back
                    </button>
                </p>
                <form onSubmit={this.handleSearch} className={styles.searchBox}>
                    <input
                        type="text"
                        name="searchValue"
                        placeholder="Search for an Song!"
                    />
                    <button type="submit">
                        <SearchIcon />
                    </button>
                </form>
            </div>
        );
    }

    renderContent() {
        const { loading, errorContent } = this.props;
        const { reUp, errorSong, songs, songsPerPage, page } = this.state;
        let loader;
        let error;
        let empty;
        let pagination;

        if (loading) {
            loader = (
                <div className=" u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (errorSong || errorContent) {
            const errorValue = errorContent ? errorContent : errorSong;
            error = (
                <div className={styles.error}>
                    <CloseIcon className={styles.icon} />
                    <p className={styles.light}>
                        <span>Error: </span>
                        {errorValue}
                    </p>
                </div>
            );
        }

        const emptyIcon = classnames(styles.bigIcon, 'empty-state__icon');
        const emptyMessage = classnames(styles.bigMessage, 'u-text-center');

        if (songs.length === 0) {
            empty = (
                <Fragment>
                    <span className={emptyIcon}>
                        <SearchIcon />
                    </span>
                    <p className={emptyMessage}>
                        {'Search a song to show the list'}.
                    </p>
                </Fragment>
            );
        }

        const targetSelected = songs.some((song) => song.target === true);

        const songsToShow = songs.slice(
            (page - 1) * songsPerPage,
            page * songsPerPage
        );

        const rows = songsToShow.map((item, i) => {
            return {
                model: item,
                value: (
                    <SongItem
                        key={`${i}:${item.id}`}
                        position={i + songsPerPage * (page - 1)}
                        song={item}
                        allSongs={songs}
                        targetSelected={targetSelected}
                        onSelect={this.handleSelect}
                    />
                )
            };
        });

        if (songs.length > songsPerPage) {
            pagination = (
                <div className={styles.pages}>
                    <button
                        onClick={this.handlePreviousPage}
                        disabled={page === 1}
                    >
                        &laquo; Previous
                    </button>
                    {page}
                    <button
                        onClick={this.handleNextPage}
                        disabled={page >= songs.length / songsPerPage}
                    >
                        Next &raquo;
                    </button>
                </div>
            );
        }

        const btnClass = classnames(
            styles.btnSubmit,
            'button button--radius button--upload'
        );

        const content = (
            <Fragment>
                {error}
                <h3 className={styles.subTitle}>
                    Target & Source Songs:{' '}
                    <span>Select one target song and up to 100 sources</span>
                </h3>
                <Table
                    className={styles.table}
                    headerCells={this.getHeaderCells()}
                    bodyRows={rows}
                    ignoreTbody
                />
                {pagination}
                <div className={styles.subTitle}>
                    <p>
                        <span>Re-Up:</span>Make Source url a re-up of
                        destination url after moving stats
                    </p>
                </div>
                <label>
                    <input
                        type="checkbox"
                        name="reUp"
                        checked={reUp}
                        onChange={this.handleChange}
                    />
                    Re-Up
                </label>
                <button className={btnClass} onClick={this.handleSubmit}>
                    Migrate Song Data
                </button>
            </Fragment>
        );

        return (
            <Fragment>
                {loader}
                {empty ? empty : content}
            </Fragment>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Song Migration'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default StatsSwapMigrateId;
