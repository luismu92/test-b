import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ucfirst } from 'utils/index';
import moment from 'moment';

import CloseIcon from '../../icons/close-solid';
import CheckIcon from '../../icons/check-mark';
import InfoIcon from '../../icons/info';
import WarningIcon from '../../icons/warning';
import styles from './StatsSwapTask.module.scss';

export default class TaskItem extends Component {
    static propTypes = {
        task: PropTypes.object,
        onShowSubs: PropTypes.func
    };

    handleShowDetails = () => {
        const { onShowSubs, task } = this.props;

        onShowSubs(task.task_id);
    };

    renderStatusIcon(status) {
        let icon;

        switch (status.toLowerCase()) {
            case 'complete': {
                icon = (
                    <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                        <CheckIcon className="u-text-icon u-margin-0 status-icon status-icon--active" />
                    </span>
                );
                break;
            }

            case 'failure': {
                icon = (
                    <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                        <CloseIcon className="u-text-icon u-margin-0 u-text-red" />
                    </span>
                );
                break;
            }

            default: {
                icon = (
                    <span data-tooltip={ucfirst(status)} data-tooltip-dark>
                        <WarningIcon className="u-text-icon u-margin-0 u-text-yellow" />
                    </span>
                );
                break;
            }
        }

        return <span className={styles.status}>{icon}</span>;
    }

    render() {
        const {
            task: {
                task_id,
                created,
                task_status,
                play,
                total,
                failure,
                running,
                complete
            }
        } = this.props;
        const date = moment(created).format('MM-DD-YYYY');

        return (
            <tbody>
                <tr className={styles.rows}>
                    <td data-th="Task ID" className="flex-2">
                        {task_id}
                    </td>
                    <td data-th="Date" className="flex-2">
                        {date}
                    </td>
                    <td data-th="Status" className="flex-3">
                        {this.renderStatusIcon(task_status)}
                    </td>
                    <td data-th="Play" className="flex-1 medium-text-center">
                        {play}
                    </td>
                    <td data-th="Running" className="flex-1 medium-text-center">
                        {running}
                    </td>
                    <td data-th="Failure" className="flex-1 medium-text-center">
                        {failure}
                    </td>
                    <td
                        data-th="Complete"
                        className="flex-1 medium-text-center"
                    >
                        {complete}
                    </td>
                    <td data-th="Total" className="flex-1 medium-text-center">
                        {total}
                    </td>
                    <td data-th="Detail" className={styles.edit}>
                        <button onClick={this.handleShowDetails}>
                            <InfoIcon />
                        </button>
                    </td>
                </tr>
            </tbody>
        );
    }
}
