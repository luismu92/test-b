import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PaperTable from '../components/AdminPaper';
import Table from '../../components/Table';
import TaskItem from './StatsSwapTaskItem';

import AndroidLoader from '../../loaders/AndroidLoader';
import TaskIcon from '../../icons/feed';
import Refresh from '../../icons/refresh';
import styles from './StatsSwapTask.module.scss';

class StatsSwapTaskTable extends Component {
    static propTypes = {
        tasks: PropTypes.array,
        loading: PropTypes.bool,
        errorContent: PropTypes.string,
        onShowSubs: PropTypes.func,
        onMigrateUrl: PropTypes.func,
        onMigrateId: PropTypes.func,
        onNextPagination: PropTypes.func,
        onPreviousPagination: PropTypes.func,
        next: PropTypes.string,
        previous: PropTypes.string
    };

    handleBefore = () => {
        const { onPreviousPagination, previous } = this.props;
        onPreviousPagination(previous);
    };

    handleAfter = () => {
        const { onNextPagination, next } = this.props;
        onNextPagination(next);
    };

    getHeaderCells() {
        return [
            {
                value: 'Task ID',
                props: {
                    className: 'flex-2'
                }
            },
            {
                value: 'Date',
                props: {
                    className: 'flex-2'
                }
            },
            {
                value: 'Status',
                props: {
                    className: 'flex-3'
                }
            },
            {
                value: 'Plays',
                props: {
                    className: 'flex-1 medium-text-center'
                }
            },
            {
                value: 'Running',
                props: {
                    className: 'flex-1 medium-text-center'
                }
            },
            {
                value: 'Failure',
                props: {
                    className: 'flex-1 medium-text-center'
                }
            },
            {
                value: 'Complete',
                props: {
                    className: 'flex-1 medium-text-center'
                }
            },
            {
                value: 'Total',
                props: {
                    className: 'flex-1 medium-text-center'
                }
            },
            {
                value: 'Details',
                props: {
                    className: 'medium-text-center'
                }
            }
        ];
    }

    emptyOrError() {
        const { tasks, errorContent } = this.props;
        let value = null;

        const emptyIcon = classnames(styles.bigIcon, 'empty-state__icon');
        const emptyMessage = classnames(styles.bigMessage, 'u-text-center');

        if (tasks.length === 0) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <TaskIcon />
                    </span>
                    <p className={emptyMessage}>
                        {'No migrate Tasks to show'}.
                    </p>
                </Fragment>
            );
        }

        if (errorContent !== null) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <Refresh />
                    </span>
                    <p className={emptyMessage}>
                        {`${errorContent}, please reload.`}
                    </p>
                </Fragment>
            );
        }

        return value;
    }

    renderTop() {
        return (
            <div>
                <h5>Stats Migration</h5>
                <p>
                    Check all migrations that have been done, if you want to
                    make a new song migration you can do it by using the name of
                    the song or its url.
                </p>
                <div className={styles.btn}>
                    <button
                        className="button button--radius button--upload"
                        onClick={this.props.onMigrateId}
                    >
                        Migrate by Name
                    </button>
                    <button
                        className="button button--radius button--upload"
                        onClick={this.props.onMigrateUrl}
                    >
                        Migrate by URL
                    </button>
                </div>
            </div>
        );
    }

    renderContent() {
        const { tasks, loading, next, previous } = this.props;
        const emptyOrError = this.emptyOrError();
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const rows = tasks.map((item, i) => {
            return {
                model: item,
                value: (
                    <TaskItem
                        key={`${i}:${item.task_id}`}
                        task={item}
                        onShowSubs={this.props.onShowSubs}
                    />
                )
            };
        });

        const pagination = (
            <div className={styles.pages}>
                <button onClick={this.handleBefore} disabled={previous === ''}>
                    &laquo; Previous
                </button>
                <button onClick={this.handleAfter} disabled={next === ''}>
                    Next &raquo;
                </button>
            </div>
        );

        const table = (
            <Table
                className={styles.table}
                headerCells={this.getHeaderCells()}
                bodyRows={rows}
                ignoreTbody
            />
        );

        return (
            <div>
                {loader}
                {emptyOrError ? emptyOrError : table}
                {pagination}
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Song Migration'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default StatsSwapTaskTable;
