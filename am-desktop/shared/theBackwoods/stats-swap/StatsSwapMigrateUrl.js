import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PaperTable from '../components/AdminPaper';
import { smoothScroll } from 'utils/index';

import AndroidLoader from '../../loaders/AndroidLoader';
import CloseIcon from '../../icons/close-solid';
import styles from './StatsSwapMigrateUrl.module.scss';

class StatsSwapMigrateUrl extends Component {
    static propTypes = {
        loading: PropTypes.bool,
        errorContent: PropTypes.string,
        onShowTasks: PropTypes.func,
        onMigrateUrl: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            sourceUrls: '',
            targetUrl: null,
            reUp: false,
            errorUrl: ''
        };
    }

    componentDidUpdate(prevProps, prevState) {
        const { errorUrl } = this.state;
        const { errorContent } = this.props;
        if (
            prevState.errorUrl !== errorUrl ||
            prevProps.errorContent !== errorContent
        ) {
            smoothScroll(0, { speed: 500 });
        }
    }

    handleChange = (e) => {
        const name = e.target.name;
        let value = e.target.value;

        if (name === 'reUp') {
            value = e.target.checked;
        }

        this.setState({
            [name]: value
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { onMigrateUrl } = this.props;
        const { sourceUrls, targetUrl, reUp } = this.state;
        const sourceArray = sourceUrls.split('\n');
        const rows = sourceArray.length;

        if (rows > 100) {
            this.setState({
                errorUrl: `Invalid number of urls, actual: ${rows} url's, please remove some.`
            });
        }

        const invalidUrl = sourceArray.some((url) => {
            const valid = this.validateUrl(url);

            if (!valid) {
                this.setState({
                    errorUrl: `Invalid url on the list: ${url}, please fix.`
                });
            }
            return !valid;
        });

        if (!invalidUrl && rows <= 100) {
            onMigrateUrl(targetUrl, sourceArray, reUp);
        }
    };

    validateUrl(url) {
        const pattern = new RegExp(
            '^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$',
            'i'
        ); // fragment locator
        return !!pattern.test(url);
    }

    renderTop() {
        return (
            <div>
                <h5>Migrate by Url</h5>
                <p>
                    {`To migrate stats, playlist and fav data from one song to another,
                    please enter the source and destination song url's. To return just `}
                    <button
                        className="u-text-orange"
                        onClick={this.props.onShowTasks}
                    >
                        Go Back
                    </button>
                </p>
            </div>
        );
    }

    renderContent() {
        const { loading, errorContent } = this.props;
        const { errorUrl } = this.state;
        const btnClass = classnames(
            styles.btn,
            'button button--radius button--upload'
        );
        let error;
        let loader;

        if (loading) {
            loader = (
                <div className=" u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        if (errorUrl || errorContent) {
            const errorValue = errorUrl ? errorUrl : errorContent;
            error = (
                <div className={styles.error}>
                    <CloseIcon className={styles.icon} />
                    <p className={styles.light}>
                        <span>Error: </span>
                        {errorValue}
                    </p>
                </div>
            );
        }

        return (
            <Fragment>
                {loader}
                {error}
                <form onSubmit={this.handleSubmit} className={styles.form}>
                    <p>
                        <span>Source Url's:</span>Enter up to 100 url's, one on
                        each line
                    </p>
                    <textarea
                        name="sourceUrls"
                        onChange={this.handleChange}
                        required
                    />
                    <p className={styles.spcTop}>
                        <span>Destination Url</span>
                    </p>
                    <input
                        type="url"
                        name="targetUrl"
                        onChange={this.handleChange}
                        required
                    />
                    <p className={styles.spcTop}>
                        <span>Re-Up:</span>Make Source url a re-up of
                        destination url after moving stats
                    </p>
                    <div>
                        <input
                            type="checkbox"
                            name="reUp"
                            onChange={this.handleChange}
                        />
                        <span>Re-Up</span>
                    </div>
                    <input
                        className={btnClass}
                        type="submit"
                        value="Migrate Song Data"
                    />
                </form>
            </Fragment>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Song Migration'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default StatsSwapMigrateUrl;
