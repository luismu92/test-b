import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Verified from '../../../../am-shared/components/Verified';
import Avatar from '../../components/Avatar';
import { ucfirst } from 'utils/index';
import { getArtistUrl } from 'utils/artist';
import AdminMusicDetailContainer from '../components/Music/AdminMusicDetailContainer';

import TwitterLogo from 'icons/twitter-logo-new';
import InstagramLogo from '../../icons/instagram';
import CheckIcon from '../../icons/check-mark';
import styles from './ValidateArtist.module.scss';

class ValidateArtistItem extends Component {
    static propTypes = {
        artist: PropTypes.object,
        songs: PropTypes.array,
        linkedSocials: PropTypes.object,
        onNewValidateStatus: PropTypes.func,
        onCheckSocial: PropTypes.func
    };

    handleApprove = () => {
        const { onNewValidateStatus, artist } = this.props;

        onNewValidateStatus(artist.id, 'approved');
    };

    handleDecline = () => {
        const { onNewValidateStatus, artist } = this.props;

        onNewValidateStatus(artist.id, 'declined');
    };

    handleBan = () => {
        const { onNewValidateStatus, artist } = this.props;

        onNewValidateStatus(artist.id, 'banned');
    };

    handleCheckSocial = (e) => {
        e.preventDefault();
        const { onCheckSocial, artist } = this.props;
        const social = e.currentTarget.name;

        if (social) {
            onCheckSocial(artist.id, social);
        }
    };

    renderProfile = () => {
        const { artist } = this.props;
        const check = <Verified user={artist} size={18} />;
        const plays = artist.stats['plays-raw'];

        return (
            <div className={styles.profile}>
                <Avatar
                    className={classnames(
                        styles.avatar,
                        'artist-page__avatar media-item__figure'
                    )}
                    image={artist.image_base || artist.image}
                    type="artist"
                    size={70}
                    rounded
                />
                <div className={styles.info}>
                    <a
                        href={getArtistUrl(artist, {
                            host: process.env.AM_URL
                        })}
                        target="_blank"
                        className={styles.artistName}
                    >
                        {ucfirst(artist.name)} {check}
                    </a>
                    <h3>
                        Total Plays:{' '}
                        <span className="u-text-orange">
                            {plays.toLocaleString()}
                        </span>
                    </h3>
                </div>
            </div>
        );
    };

    renderButtons = () => {
        return (
            <div className={styles.buttons}>
                <button
                    className="button button--radius button--upload"
                    onClick={this.handleApprove}
                >
                    Approve
                </button>
                <button
                    className={classnames(
                        styles.decline,
                        'button button--radius button--upload'
                    )}
                    onClick={this.handleDecline}
                >
                    Decline
                </button>
                <button
                    className={classnames(
                        styles.ban,
                        'button button--radius button--upload'
                    )}
                    onClick={this.handleBan}
                >
                    Ban User
                </button>
            </div>
        );
    };

    renderSocials = () => {
        const { artist, linkedSocials } = this.props;
        const providers = [
            {
                network: 'twitter',
                icon: <TwitterLogo className={styles.icon} />
            },
            {
                network: 'instagram',
                icon: <InstagramLogo className={styles.icon} />
            }
        ];

        const socials = providers.filter(
            (social) => linkedSocials[social.network]
        );

        const buttons = socials.map((button, i) => {
            return (
                <button
                    className={`auth__button--${button.network}`}
                    key={i}
                    name={button.network}
                    onClick={this.handleCheckSocial}
                >
                    {button.icon}
                    <span className="auth__button-text u-fs-16 u-fw-700">
                        {`${button.network === 'twitter' ? '@' : ''}${
                            artist[button.network]
                        }`}
                    </span>
                    <CheckIcon
                        className={classnames(styles.check, 'verified')}
                    />
                </button>
            );
        });

        return <div className={styles.socials}>{buttons}</div>;
    };

    renderSongs = () => {
        const { songs } = this.props;

        const music = songs.map((song, i) => {
            return (
                <AdminMusicDetailContainer
                    key={`${i}:${song.id}`}
                    index={i}
                    item={song}
                    musicList={songs}
                    shouldLinkArtwork
                    removeHeaderLinks
                />
            );
        });

        return <div className={styles.songs}>{music}</div>;
    };

    render() {
        return (
            <div className="row u-spacing-bottom">
                <div className="column small-24">
                    <div className={classnames(styles.padding, 'u-box-shadow')}>
                        <div className={styles.top}>
                            {this.renderProfile()}
                            {this.renderButtons()}
                        </div>
                        {this.renderSocials()}
                        {this.renderSongs()}
                    </div>
                </div>
            </div>
        );
    }
}

export default ValidateArtistItem;
