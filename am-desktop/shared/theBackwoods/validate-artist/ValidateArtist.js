import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import ValidateArtistItem from './ValidateArtistItem';
import BlockModal from '../../modal/BlockModal';

import AndroidLoader from '../../loaders/AndroidLoader';
import TaskIcon from '../../icons/feed';
import Refresh from '../../icons/refresh';
import styles from './ValidateArtist.module.scss';

class ValidateArtist extends Component {
    static propTypes = {
        artists: PropTypes.array,
        next: PropTypes.number,
        previous: PropTypes.number,
        loading: PropTypes.bool,
        errorContent: PropTypes.string,
        onNextPage: PropTypes.func,
        onPreviousPage: PropTypes.func,
        onNewValidateStatus: PropTypes.func,
        onCheckSocial: PropTypes.func
    };

    emptyOrError() {
        const { artists, errorContent } = this.props;
        let value = null;

        const emptyIcon = classnames(styles.bigIcon, 'empty-state__icon');
        const emptyMessage = classnames(styles.bigMessage, 'u-text-center');

        if (artists.length === 0) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <TaskIcon />
                    </span>
                    <p className={emptyMessage}>{'No artists to show'}.</p>
                </Fragment>
            );
        }

        if (errorContent !== null) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <Refresh />
                    </span>
                    <p className={emptyMessage}>
                        {`${errorContent}, please reload.`}
                    </p>
                </Fragment>
            );
        }

        return value;
    }

    renderContent() {
        const {
            loading,
            artists,
            next,
            previous,
            onPreviousPage,
            onNextPage
        } = this.props;
        const emptyOrError = this.emptyOrError();
        let loader;
        let pagination;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const items = artists.map((artist, i) => {
            return (
                <ValidateArtistItem
                    key={i}
                    artist={artist.artist}
                    songs={artist.songs}
                    linkedSocials={artist.linkedSocials || {}}
                    onNewValidateStatus={this.props.onNewValidateStatus}
                    onCheckSocial={this.props.onCheckSocial}
                />
            );
        });

        if (next || previous) {
            pagination = (
                <div className={styles.pages}>
                    <button onClick={onPreviousPage} disabled={!previous}>
                        &laquo; Previous
                    </button>
                    {Number(previous) + 1}
                    <button onClick={onNextPage} disabled={!next}>
                        Next &raquo;
                    </button>
                </div>
            );
        }

        return (
            <div>
                {loader}
                {emptyOrError ? emptyOrError : items}
                {pagination}
                <BlockModal />
            </div>
        );
    }

    render() {
        return this.renderContent();
    }
}

export default ValidateArtist;
