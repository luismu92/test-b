import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ValidateArtist from './ValidateArtist';
import {
    getArtists,
    updateArtistValidate,
    checkArtistTwitter
} from '../../redux/modules/theBackwoods/validateArtist';
import { addToast } from '../../redux/modules/toastNotification';

class ValidateArtistContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminValidateArtist: PropTypes.object
    };

    componentDidMount() {
        const { dispatch } = this.props;
        const page = 1;

        dispatch(getArtists(page));
    }

    handleNextPage = () => {
        const {
            adminValidateArtist: { nextPage },
            dispatch
        } = this.props;

        dispatch(getArtists(nextPage));
    };

    handlePreviousPage = () => {
        const {
            adminValidateArtist: { previousPage },
            dispatch
        } = this.props;

        dispatch(getArtists(previousPage));
    };

    handleNewValidateStatus = (id, newStatus) => {
        const { dispatch } = this.props;

        dispatch(updateArtistValidate(id, newStatus))
            .then(({ resolved }) => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: `Artist Verified status: ${resolved.verified}`
                    })
                );
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'There was an error changing artists status'
                    })
                );
            });
    };

    handleCheckSocial = (artistId, social) => {
        switch (social) {
            case 'twitter':
                this.handleTwitter(artistId);
                break;
            default:
                break;
        }
    };

    handleTwitter = (artistId) => {
        const { dispatch } = this.props;

        dispatch(checkArtistTwitter(artistId))
            .then((action) => {
                window.open(
                    `https://twitter.com/${action.resolved.twitter}`,
                    '_blank'
                );
                return;
            })
            .catch((err) => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: err.message
                    })
                );
            });
    };

    render() {
        const {
            adminValidateArtist: {
                artists,
                nextPage,
                previousPage,
                errorContent,
                loading
            }
        } = this.props;
        return (
            <ValidateArtist
                artists={artists}
                next={nextPage}
                previous={previousPage}
                loading={loading}
                errorContent={errorContent}
                onNextPage={this.handleNextPage}
                onPreviousPage={this.handlePreviousPage}
                onNewValidateStatus={this.handleNewValidateStatus}
                onCheckSocial={this.handleCheckSocial}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminValidateArtist: state.adminValidateArtist
    };
}

export default connect(mapStateToProps)(ValidateArtistContainer);
