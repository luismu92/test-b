import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ucfirst } from 'utils/index';
import classnames from 'classnames';

import CheckMarkIcon from '../../icons/check-mark';
import RemoveIcon from '../../icons/remove-circle';
import ArtistIcon from '../../icons/avatar';
import TrashIcon from '../../../../am-shared/icons/trash-solid';
import SuspensionAdminIcon from '../../../../am-shared/icons/remove';
import MessageIcon from '../../icons/message';
import DollarIcon from '../../icons/dollar';
import SuspensionIcon from '../../icons/recent';
import styles from './Artist.module.scss';

export default class ArtistDashboardItem extends Component {
    static propTypes = {
        artist: PropTypes.object,
        onVerifyArtist: PropTypes.func,
        onSuspensionWhitelist: PropTypes.func,
        onSuspensionAdmin: PropTypes.func,
        onShowBlockModal: PropTypes.func,
        onShowDeleteModal: PropTypes.func,
        onShowAmpModal: PropTypes.func
    };

    handleVerify = () => {
        const {
            onVerifyArtist,
            artist: { id }
        } = this.props;

        onVerifyArtist(id);
    };

    handleSuspensionWhitelist = () => {
        const {
            onSuspensionWhitelist,
            artist: { id }
        } = this.props;

        onSuspensionWhitelist(id);
    };

    handleSuspensionAdmin = () => {
        const {
            onSuspensionAdmin,
            artist: { id }
        } = this.props;

        onSuspensionAdmin(id);
    };

    handleShowBlockModal = () => {
        const { onShowBlockModal, artist } = this.props;

        onShowBlockModal(artist);
    };

    handleDeleteModal = () => {
        const { onShowDeleteModal, artist } = this.props;

        onShowDeleteModal(artist);
    };

    handleAmpModal = () => {
        const { onShowAmpModal, artist } = this.props;

        onShowAmpModal(artist);
    };

    renderButtons() {
        const {
            artist: {
                id,
                email,
                verified,
                suspension_whitelist,
                admin_suspension,
                can_upload,
                label_owner,
                status
            }
        } = this.props;
        const hasEmail =
            typeof email !== 'boolean' ||
            email.length !== 0 ||
            typeof email !== 'undefined';
        const isWhiteList =
            typeof suspension_whitelist !== 'undefined' &&
            !(
                suspension_whitelist.length === 0 ||
                suspension_whitelist === 'no'
            );
        const isVerified = verified === 'yes' || verified === true;
        const isAdminSuspended =
            typeof admin_suspension !== 'undefined' &&
            !(admin_suspension.length === 0 || admin_suspension === 'no');

        return (
            <Fragment>
                <Link to={`/the-backwoods/accounts/${id}`}>
                    <button data-tooltip="Edit Artist">
                        <ArtistIcon />
                    </button>
                </Link>
                <button
                    data-tooltip={isVerified ? 'Unverify' : 'Verify'}
                    className={isVerified ? styles.btnOn : null}
                    onClick={this.handleVerify}
                >
                    <CheckMarkIcon />
                </button>
                <button
                    data-tooltip={
                        isWhiteList
                            ? 'Remove from Suspension Whitelist'
                            : 'Add to Suspension Whitelist'
                    }
                    className={isWhiteList ? styles.btnOn : null}
                    onClick={this.handleSuspensionWhitelist}
                >
                    <SuspensionIcon />
                </button>
                {!hasEmail && status.toLowerCase() === 'ghost' && (
                    <button data-tooltip="Email claim code">
                        <MessageIcon />
                    </button>
                )}
                {hasEmail && (
                    <Fragment>
                        <button
                            data-tooltip={can_upload ? 'Block' : 'Un-block'}
                            className={can_upload ? null : styles.btnOn}
                            onClick={this.handleShowBlockModal}
                        >
                            <RemoveIcon />
                        </button>
                        <button
                            data-tooltip={
                                isAdminSuspended
                                    ? 'Revoke Suspension Admin'
                                    : 'Grant Suspension Admin'
                            }
                            className={isAdminSuspended ? styles.btnOn : null}
                            onClick={this.handleSuspensionAdmin}
                        >
                            <SuspensionAdminIcon />
                        </button>
                        <button
                            data-tooltip="Create Amp Account"
                            onClick={this.handleAmpModal}
                            className={label_owner ? styles.btnOn : null}
                        >
                            <DollarIcon />
                        </button>
                        <button
                            data-tooltip="Delete"
                            onClick={this.handleDeleteModal}
                        >
                            <TrashIcon />
                        </button>
                    </Fragment>
                )}
            </Fragment>
        );
    }

    render() {
        const {
            artist: { name, status, email, url_slug }
        } = this.props;

        return (
            <tbody>
                <tr className={styles.rows}>
                    <td data-th="Name" className="flex-1">
                        <Link to={`/artist/${url_slug}`}>{name}</Link>
                    </td>
                    <td data-th="Email" className="flex-1">
                        {typeof email !== 'undefined' || email
                            ? email
                            : 'No email'}
                    </td>
                    <td data-th="Status" className="flex-1">
                        {status ? ucfirst(status) : ''}
                    </td>
                    <td
                        data-th="Actions"
                        className={classnames(styles.actions, 'flex-3')}
                    >
                        {this.renderButtons()}
                    </td>
                </tr>
            </tbody>
        );
    }
}
