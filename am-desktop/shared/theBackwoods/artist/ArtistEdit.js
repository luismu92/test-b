import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import classnames from 'classnames';

import PaperTable from '../components/AdminPaper';
import Select from '../../components/Select';
import Truncate from 'components/Truncate';
import Verified from 'components/Verified';
import Avatar from '../../components/Avatar';
import EditableImageBanner from '../../widgets/EditableImageBanner';
import {
    allGenresMap,
    podcastCategories,
    podcastLanguages
} from 'constants/index';
import { getPodcastUrl } from 'utils/index';

import AndroidLoader from '../../loaders/AndroidLoader';
import styles from './ArtistEdit.module.scss';

class ArtistAdminEdit extends Component {
    static propTypes = {
        artist: PropTypes.object,
        loading: PropTypes.bool,
        isEdit: PropTypes.bool,
        editableFieldValues: PropTypes.object,
        onEditFieldInput: PropTypes.func,
        onBannerEditApply: PropTypes.func,
        onBannerRemove: PropTypes.func,
        onUpdateArtist: PropTypes.func,
        onNewArtist: PropTypes.func
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {
            isEdit,
            onUpdateArtist,
            onNewArtist,
            editableFieldValues
        } = this.props;

        if (isEdit) {
            onUpdateArtist(editableFieldValues);
        } else {
            onNewArtist(editableFieldValues);
        }
    };

    renderBannerForm(banner) {
        const { onBannerEditApply, onBannerRemove } = this.props;

        return (
            <EditableImageBanner
                banner={banner}
                onApply={onBannerEditApply}
                onRemove={onBannerRemove}
            />
        );
    }

    renderProfileInfo() {
        const { artist } = this.props;

        const check = <Verified user={artist} size={19} />;

        return (
            <div>
                <h2 className="user-profile__name">
                    {artist.name} {check}
                </h2>
                <Truncate
                    className="user-profile__bio"
                    lines={1}
                    ellipsis={'…'}
                    text={artist.bio}
                />
            </div>
        );
    }

    renderBasicForm() {
        const {
            onEditFieldInput,
            editableFieldValues,
            artist,
            isEdit
        } = this.props;
        const profileBiographyMaxChars = 800;
        const bio = editableFieldValues.bio || artist.bio || '';

        const genreOptions = Object.keys(allGenresMap).map((genre) => {
            return { value: genre, text: allGenresMap[genre] };
        });
        const genreProps = {
            value:
                typeof editableFieldValues.genre !== 'undefined'
                    ? editableFieldValues.genre
                    : artist.genre || ''
        };

        let podcastCategory;
        let hasPodcast = false;

        if (genreProps.value === 'podcast') {
            const podcastCategoryOptions = [
                { value: '', text: 'Choose category' }
            ].concat(
                podcastCategories.map(({ category }) => {
                    return { value: category, text: category };
                })
            );
            const podcastLangOptions = Object.keys(podcastLanguages).map(
                (langCode) => {
                    const text = podcastLanguages[langCode];

                    return { value: langCode, text: text };
                }
            );
            const podcastCategoryProps = {
                value:
                    typeof editableFieldValues.podcast_category !== 'undefined'
                        ? editableFieldValues.podcast_category
                        : artist.podcast_category || ''
            };
            const podcastLanguageProps = {
                value:
                    typeof editableFieldValues.podcast_language !== 'undefined'
                        ? editableFieldValues.podcast_language
                        : artist.podcast_language || 'en'
            };

            hasPodcast = true;
            podcastCategory = (
                <Fragment>
                    <div className="column small-24 medium-6">
                        <label htmlFor="podcast_category">
                            Podcast Category:
                        </label>
                        <Select
                            onChange={onEditFieldInput}
                            options={podcastCategoryOptions}
                            name="podcast_category"
                            id="podcast_category"
                            selectProps={podcastCategoryProps}
                        />
                    </div>
                    <div className="column small-24 medium-6">
                        <label htmlFor="podcast_language">
                            Podcast Language:
                        </label>
                        <Select
                            onChange={onEditFieldInput}
                            options={podcastLangOptions}
                            name="podcast_language"
                            id="podcast_language"
                            selectProps={podcastLanguageProps}
                        />
                    </div>
                    <div className="column small-24 medium-6">
                        <label htmlFor="genre">RSS Feed:</label>
                        <input
                            readOnly
                            type="text"
                            defaultValue={getPodcastUrl(artist.url_slug)}
                            onClick={this.handleRssInputClick}
                        />
                    </div>
                </Fragment>
            );
        }

        const genreColumnClass = classnames('column', {
            'small-24 medium-6': hasPodcast
        });
        const btnClass = classnames(
            styles.btnSubmit,
            'button button--radius button--upload column small-24"'
        );

        return (
            <form
                onSubmit={this.handleSubmit}
                className="user-profile__feed row u-margin-0"
            >
                <div className="column small-24 medium-12">
                    <label htmlFor="name" className="required">
                        Name
                    </label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        defaultValue={artist.name}
                        onChange={onEditFieldInput}
                        required
                    />
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="label">Label</label>
                    <input
                        type="text"
                        name="label"
                        id="label"
                        defaultValue={artist.label}
                        onChange={onEditFieldInput}
                        className="form-control"
                    />
                </div>
                <div className="column small-24 medium-12">
                    <label htmlFor="hometown">Hometown</label>
                    <input
                        type="text"
                        name="hometown"
                        id="hometown"
                        defaultValue={artist.hometown}
                        onChange={onEditFieldInput}
                        className="form-control"
                    />
                </div>
                <div className="column small-24 medium-12 u-pos-relative">
                    <label htmlFor="url">Website URL</label>
                    <input
                        type="url"
                        name="url"
                        id="url"
                        onChange={onEditFieldInput}
                        defaultValue={artist.url}
                        className="form-control"
                    />
                </div>
                <div className="column small-24">
                    <label htmlFor="bio">Short Biography</label>
                    <textarea
                        name="bio"
                        id="bio"
                        rows="4"
                        className={classnames(styles.bio, 'form-control')}
                        cols="80"
                        onChange={onEditFieldInput}
                        maxLength={profileBiographyMaxChars}
                        defaultValue={bio}
                    />
                    <em>
                        <span className="charsleft u-d-inline-block">
                            You have{' '}
                            <span>{profileBiographyMaxChars - bio.length}</span>{' '}
                            character
                            {profileBiographyMaxChars - bio.length === 1
                                ? ''
                                : 's'}{' '}
                            left.
                        </span>
                    </em>
                </div>
                <div className="column small-24 medium-6">
                    <label htmlFor="twitter">Twitter Handle</label>
                    <input
                        type="text"
                        name="twitter"
                        id="twitter"
                        defaultValue={artist.twitter}
                        onChange={onEditFieldInput}
                        className="form-control"
                    />
                </div>
                <div className="column small-24 medium-6">
                    <label htmlFor="facebook">Facebook URL</label>
                    <input
                        type="url"
                        name="facebook"
                        id="facebook"
                        defaultValue={artist.facebook}
                        onChange={onEditFieldInput}
                        className="form-control"
                    />
                </div>
                <div className="column small-24 medium-6">
                    <label htmlFor="instagram">Instagram Username</label>
                    <input
                        type="text"
                        name="instagram"
                        id="instagram"
                        defaultValue={artist.instagram}
                        onChange={onEditFieldInput}
                        className="form-control"
                    />
                </div>
                <div className="column small-24 medium-6">
                    <label htmlFor="youtube">YouTube URL</label>
                    <input
                        type="url"
                        name="youtube"
                        id="youtube"
                        defaultValue={artist.youtube}
                        onChange={onEditFieldInput}
                        className="form-control"
                    />
                </div>
                <div className={genreColumnClass}>
                    <label htmlFor="genre">Genre:</label>

                    <Select
                        onChange={onEditFieldInput}
                        options={genreOptions}
                        name="genre"
                        id="genre"
                        selectProps={genreProps}
                    />
                </div>
                {podcastCategory}
                <div className="column small-24">
                    <input
                        className={btnClass}
                        type="submit"
                        value={isEdit ? 'Update Artist' : 'Save Artist'}
                    />
                </div>
            </form>
        );
    }

    renderTop() {
        const { isEdit } = this.props;
        const newArtistBtn = classnames(
            styles.newArtist,
            'button button--radius button--upload'
        );
        return (
            <Fragment>
                <div className={styles.adminTop}>
                    <div>
                        <h5>{`${isEdit ? 'Edit' : 'New'} Artist`}</h5>
                        <p>
                            {isEdit
                                ? 'Edit artist values and images.'
                                : 'Create a new artist from scratch, created artist is going to be added as a Ghost Artist.'}
                        </p>
                    </div>
                    <Link to={'/the-backwoods/accounts/dashboard'}>
                        <button className={newArtistBtn}>
                            Artist dashboard
                        </button>
                    </Link>
                </div>
            </Fragment>
        );
    }

    renderContent() {
        const {
            loading,
            editableFieldValues,
            artist,
            onEditFieldInput,
            isEdit
        } = this.props;
        let loader;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const content = classnames(
            styles.content,
            'user-profile user-profile--edit'
        );

        return (
            <div>
                {loader}
                <div id="profile-page" className={content}>
                    <div className={styles.banner}>
                        {this.renderBannerForm(
                            typeof editableFieldValues.image_banner ===
                                'undefined'
                                ? artist.image_banner
                                : editableFieldValues.image_banner
                        )}
                    </div>
                    <div className="user-profile__main">
                        <div className="row u-margin-0">
                            <div className="column small-24 medium-16 small-text-center medium-text-left user-profile__main-info">
                                <Avatar
                                    type="artist"
                                    image={
                                        editableFieldValues.image ||
                                        artist.image_base ||
                                        artist.image
                                    }
                                    onInputChange={onEditFieldInput}
                                    dirty={!!editableFieldValues.image}
                                    editable
                                    rounded
                                />
                                {isEdit && this.renderProfileInfo()}
                            </div>
                        </div>
                        {this.renderBasicForm()}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Artists'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default ArtistAdminEdit;
