import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ArtistDashboard from './ArtistDashboard';
import {
    getArtists,
    searchArtists,
    verifyArtist,
    suspensionWhiteList,
    suspensionAdmin,
    deleteArtist,
    ampAccount,
    verifiedArtists,
    ghostArtists,
    getAllArtists,
    tastemakerArtists
} from '../../redux/modules/theBackwoods/artist';
import {
    MODAL_TYPE_BLOCK,
    MODAL_TYPE_CONFIRM,
    showModal,
    hideModal
} from '../../redux/modules/modal';
import { addToast } from '../../redux/modules/toastNotification';

class ArtistsAdminDashboardContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        adminArtists: PropTypes.object,
        adminValidateArtist: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            offset: 0,
            limit: 50,
            context: 'all'
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getAllArtists());
    }

    handleGetLists = (value) => {
        const { dispatch } = this.props;

        this.setState({
            context: value
        });

        switch (value) {
            case 'verified':
                dispatch(verifiedArtists());
                break;
            case 'all':
                dispatch(getAllArtists());
                break;
            case 'ghost-accounts':
                dispatch(ghostArtists());
                break;
            case 'tastemaker':
                dispatch(tastemakerArtists());
                break;
            default:
                break;
        }
    };

    handleShowMainPage = () => {
        const { dispatch } = this.props;
        const { limit } = this.state;

        dispatch(getArtists(limit, 0));
    };

    handleNextPage = () => {
        const { dispatch } = this.props;
        const { offset, limit } = this.state;
        const nextOffset = offset + limit;

        dispatch(getArtists(limit, nextOffset))
            .then(() => {
                this.setState({
                    offset: nextOffset
                });
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'No more artists to show'
                    })
                );
            });
    };

    handlePreviousPage = () => {
        const { dispatch } = this.props;
        const { offset, limit } = this.state;
        const previousOffset = offset - limit;

        dispatch(getArtists(limit, previousOffset));

        this.setState({
            offset: previousOffset
        });
    };

    handleSearch = (search, genre, verified, ghost, tastemaker) => {
        const { dispatch } = this.props;

        dispatch(searchArtists(search, genre, verified, ghost, tastemaker));
    };

    handleVerifyArtist = (id) => {
        const { dispatch } = this.props;

        dispatch(verifyArtist(id))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'Verify status updated'
                    })
                );
                return;
            })
            .catch(() => {
                const {
                    adminArtists: { errorItemOption }
                } = this.props;
                dispatch(
                    addToast({
                        action: 'message',
                        message: errorItemOption
                    })
                );
            });
    };

    handleSuspensionWhiteList = (id) => {
        const { dispatch } = this.props;

        dispatch(suspensionWhiteList(id))
            .then(({ resolved }) => {
                const status =
                    resolved.suspension_whitelist === 'yes'
                        ? 'added to'
                        : 'removed from';

                dispatch(
                    addToast({
                        action: 'message',
                        message: `Artist ${status} Suspension Whitelist`
                    })
                );
                return;
            })
            .catch(() => {
                const {
                    adminArtists: { errorItemOption }
                } = this.props;
                dispatch(
                    addToast({
                        action: 'message',
                        message: errorItemOption
                    })
                );
            });
    };

    handleSuspensionAdmin = (id) => {
        const { dispatch } = this.props;

        dispatch(suspensionAdmin(id))
            .then(({ resolved }) => {
                const status =
                    resolved.admin_suspension === 'yes'
                        ? ' Artist suspended'
                        : 'Artist suspension revoke';

                dispatch(
                    addToast({
                        action: 'message',
                        message: status
                    })
                );
                return;
            })
            .catch(() => {
                const {
                    adminArtists: { errorItemOption }
                } = this.props;
                dispatch(
                    addToast({
                        action: 'message',
                        message: errorItemOption
                    })
                );
            });
    };

    handleBlockModal = (artist) => {
        const { dispatch } = this.props;
        const type = artist.can_upload ? 'block' : 'un-block';
        const id = artist.id;

        dispatch(
            showModal(MODAL_TYPE_BLOCK, {
                title: `Are you sure you want to ${type} this artist?`,
                artistId: id,
                artist: artist.name,
                confirmData: {
                    id,
                    type
                },
                fromAdmin: true
            })
        );
    };

    handleDeleteConfirm = (id) => {
        const { dispatch } = this.props;

        dispatch(hideModal(MODAL_TYPE_CONFIRM));
        dispatch(deleteArtist(id))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'Artist successfully removed'
                    })
                );
                return;
            })
            .catch(() => {
                const {
                    adminArtists: { errorItemOption }
                } = this.props;
                dispatch(
                    addToast({
                        action: 'message',
                        message: errorItemOption
                    })
                );
            });
    };

    handleDeleteModal = (artist) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'Delete Artist',
                message: `Are you sure you wish to delete the artist ${
                    artist.name
                }?`,
                confirmButtonText: 'Delete',
                handleConfirm: () => this.handleDeleteConfirm(artist.id)
            })
        );
    };

    handleAmpAccountConfirm = (id) => {
        const { dispatch } = this.props;

        dispatch(hideModal(MODAL_TYPE_CONFIRM));
        dispatch(ampAccount(id))
            .then(({ resolved }) => {
                const exception = resolved.exception || '';
                const message =
                    exception.length !== 0 ? exception : 'AMP account created';

                dispatch(
                    addToast({
                        action: 'message',
                        message: message
                    })
                );
                return;
            })
            .catch((error) => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: error.message
                    })
                );
            });
    };

    handleAmpModal = (artist) => {
        const { dispatch } = this.props;

        dispatch(
            showModal(MODAL_TYPE_CONFIRM, {
                title: 'AMP Account',
                message: `Are you sure you want to AMP the account ${
                    artist.name
                }?`,
                confirmButtonText: 'Confirm',
                handleConfirm: () => this.handleAmpAccountConfirm(artist.id)
            })
        );
    };

    render() {
        const {
            adminArtists: { artists, loading, errorContent, search }
        } = this.props;
        const {
            adminValidateArtist: { totalPending }
        } = this.props;
        const { offset, limit, context } = this.state;

        return (
            <ArtistDashboard
                artists={artists}
                totalPending={totalPending}
                context={context}
                loading={loading}
                errorContent={errorContent}
                search={search}
                offset={offset}
                limit={limit}
                onNextPage={this.handleNextPage}
                onPreviousPage={this.handlePreviousPage}
                onSearch={this.handleSearch}
                onShowMainPage={this.handleShowMainPage}
                onVerifyArtist={this.handleVerifyArtist}
                onSuspensionWhitelist={this.handleSuspensionWhiteList}
                onSuspensionAdmin={this.handleSuspensionAdmin}
                onShowBlockModal={this.handleBlockModal}
                onShowDeleteModal={this.handleDeleteModal}
                onShowAmpModal={this.handleAmpModal}
                onGetLists={this.handleGetLists}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminArtists: state.adminArtists,
        adminValidateArtist: state.adminValidateArtist
    };
}

export default connect(mapStateToProps)(ArtistsAdminDashboardContainer);
