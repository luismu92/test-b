import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import classnames from 'classnames';

import PaperTable from '../components/AdminPaper';
import Table from '../../components/Table';
import BlockModal from '../../modal/BlockModal';
import FeedBar from '../../widgets/FeedBar';

import AuthenticationPending from '../validate-artist/ValidateArtistContainer';
import ArtistItem from './ArtistDashboardItem';
import AndroidLoader from '../../loaders/AndroidLoader';
import SearchIcon from '../../icons/search';
import MicIcon from '../../icons/mic';
import styles from './Artist.module.scss';

class ArtistAdminDashboard extends Component {
    static propTypes = {
        artists: PropTypes.array,
        totalPending: PropTypes.number,
        loading: PropTypes.bool,
        search: PropTypes.bool,
        offset: PropTypes.number,
        limit: PropTypes.number,
        errorContent: PropTypes.string,
        onNextPage: PropTypes.func,
        onPreviousPage: PropTypes.func,
        onSearch: PropTypes.func,
        onVerifyArtist: PropTypes.func,
        onSuspensionWhitelist: PropTypes.func,
        onSuspensionAdmin: PropTypes.func,
        onShowBlockModal: PropTypes.func,
        onShowDeleteModal: PropTypes.func,
        onShowAmpModal: PropTypes.func,
        context: PropTypes.string,
        onGetLists: PropTypes.func
    };

    handleSearch = (e) => {
        e.preventDefault();
        const { onSearch } = this.props;
        const form = e.currentTarget;

        onSearch(
            form.searchValue.value,
            form.genre.checked,
            form.verified.checked,
            form.ghost.checked,
            form.tastemaker.checked
        );
    };

    getHeaderCells() {
        return [
            {
                value: 'Name',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Email',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Status',
                props: {
                    className: 'flex-1'
                }
            },
            {
                value: 'Actions',
                props: {
                    className: 'flex-3'
                }
            }
        ];
    }

    totalPending() {
        const { totalPending } = this.props;
        return totalPending > 0 ? `(${totalPending})` : '';
    }

    empty() {
        const { artists } = this.props;
        let value = null;

        const emptyIcon = classnames(styles.bigIcon, 'empty-state__icon');
        const emptyMessage = classnames(styles.bigMessage, 'u-text-center');

        if (artists.length === 0) {
            value = (
                <Fragment>
                    <span className={emptyIcon}>
                        <MicIcon />
                    </span>
                    <p className={emptyMessage}>{'No Artists to show'}.</p>
                </Fragment>
            );
        }

        return value;
    }

    renderTop() {
        const newArtistBtn = classnames(
            styles.newArtist,
            'button button--radius button--upload'
        );

        return (
            <Fragment>
                <div className={styles.adminTop}>
                    <div>
                        <h5>Recent Artists</h5>
                        <p>
                            Create a new artist or manage its profile values,
                            use the artists name to search for a specific one.
                        </p>
                    </div>
                    <Link to={'/the-backwoods/accounts/new'}>
                        <button className={newArtistBtn}>Add new artist</button>
                    </Link>
                </div>
                <form onSubmit={this.handleSearch}>
                    <div className={styles.searchBox}>
                        <input
                            type="text"
                            name="searchValue"
                            placeholder="Search an Artist!"
                        />
                        <button type="submit">
                            <SearchIcon />
                        </button>
                    </div>
                    <div className={styles.filters}>
                        <h5>Filters:</h5>
                        <label>
                            <input
                                type="checkbox"
                                name="genre"
                                onChange={this.handleChange}
                            />
                            Genre
                        </label>
                        <label>
                            <input
                                type="checkbox"
                                name="verified"
                                onChange={this.handleChange}
                            />
                            Verified
                        </label>
                        <label>
                            <input
                                type="checkbox"
                                name="ghost"
                                onChange={this.handleChange}
                            />
                            Ghost
                        </label>
                        <label>
                            <input
                                type="checkbox"
                                name="tastemaker"
                                onChange={this.handleChange}
                            />
                            Tastemaker
                        </label>
                    </div>
                </form>
            </Fragment>
        );
    }

    renderContent() {
        const {
            loading,
            errorContent,
            offset,
            limit,
            search,
            context,
            artists
        } = this.props;

        const emptyOrError = this.empty();
        let loader;
        let rows;
        let content;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        const contextItems = [
            { value: 'all', text: 'All' },
            { value: 'verified', text: 'Verified' },
            { value: 'ghost-accounts', text: 'Ghost Accounts' },
            {
                value: 'authentication-application',
                text: `Authentication Applications ${this.totalPending()}`
            },
            { value: 'tastemaker', text: 'Tastemaker' }
        ].map((item) => {
            return {
                ...item,
                active: item.value === context
            };
        });

        switch (context) {
            case 'all':
                rows = artists.map((item, i) => {
                    return {
                        model: item,
                        value: (
                            <ArtistItem
                                key={`${i}:${item.id}`}
                                artist={item}
                                onVerifyArtist={this.props.onVerifyArtist}
                                onSuspensionWhitelist={
                                    this.props.onSuspensionWhitelist
                                }
                                onSuspensionAdmin={this.props.onSuspensionAdmin}
                                onShowBlockModal={this.props.onShowBlockModal}
                                onShowDeleteModal={this.props.onShowDeleteModal}
                                onShowAmpModal={this.props.onShowAmpModal}
                            />
                        )
                    };
                });

                content = this.renderTable(rows);
                break;
            case 'verified':
                rows = artists.map((item, i) => {
                    return {
                        model: item,
                        value: (
                            <ArtistItem
                                key={`${i}:${item.id}`}
                                artist={item}
                                onVerifyArtist={this.props.onVerifyArtist}
                                onSuspensionWhitelist={
                                    this.props.onSuspensionWhitelist
                                }
                                onSuspensionAdmin={this.props.onSuspensionAdmin}
                                onShowBlockModal={this.props.onShowBlockModal}
                                onShowDeleteModal={this.props.onShowDeleteModal}
                                onShowAmpModal={this.props.onShowAmpModal}
                            />
                        )
                    };
                });

                content = this.renderTable(rows);
                break;
            case 'ghost-accounts':
                rows = artists.map((item, i) => {
                    return {
                        model: item,
                        value: (
                            <ArtistItem
                                key={`${i}:${item.id}`}
                                artist={item}
                                onVerifyArtist={this.props.onVerifyArtist}
                                onSuspensionWhitelist={
                                    this.props.onSuspensionWhitelist
                                }
                                onSuspensionAdmin={this.props.onSuspensionAdmin}
                                onShowBlockModal={this.props.onShowBlockModal}
                                onShowDeleteModal={this.props.onShowDeleteModal}
                                onShowAmpModal={this.props.onShowAmpModal}
                            />
                        )
                    };
                });
                content = this.renderTable(rows);
                break;
            case 'authentication-application':
                content = (
                    <div className="u-spacing-top">
                        <AuthenticationPending />
                    </div>
                );
                break;
            case 'tastemaker':
                rows = artists.map((item, i) => {
                    return {
                        model: item,
                        value: (
                            <ArtistItem
                                key={`${i}:${item.id}`}
                                artist={item}
                                onVerifyArtist={this.props.onVerifyArtist}
                                onSuspensionWhitelist={
                                    this.props.onSuspensionWhitelist
                                }
                                onSuspensionAdmin={this.props.onSuspensionAdmin}
                                onShowBlockModal={this.props.onShowBlockModal}
                                onShowDeleteModal={this.props.onShowDeleteModal}
                                onShowAmpModal={this.props.onShowAmpModal}
                            />
                        )
                    };
                });
                content = this.renderTable(rows);
                break;
            default:
                break;
        }

        const table = (
            <Fragment>
                <FeedBar
                    className="dashboard-feed-bar"
                    activeMarker={context}
                    items={contextItems}
                    onContextSwitch={this.props.onGetLists}
                    showBorderMarker
                />
                {loader}
                {content ? content : emptyOrError}
                {!search && (
                    <div className={styles.pages}>
                        <button
                            onClick={this.props.onPreviousPage}
                            disabled={offset === 0}
                        >
                            &laquo; Previous
                        </button>
                        {offset / limit + 1}
                        <button
                            onClick={this.props.onNextPage}
                            disabled={errorContent !== null}
                        >
                            Next &raquo;
                        </button>
                    </div>
                )}
                <BlockModal />
            </Fragment>
        );

        return table;
    }

    renderTable(rows) {
        return (
            <Table
                className={styles.table}
                headerCells={this.getHeaderCells()}
                bodyRows={rows}
                ignoreTbody
            />
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Artists'}
                    topPaper={this.renderTop()}
                    contentPaper={this.renderContent()}
                />
            </Fragment>
        );
    }
}

export default ArtistAdminDashboard;
