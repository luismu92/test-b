import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { previewFile } from 'utils/index';

import ArtistEdit from './ArtistEdit';
import { addToast } from '../../redux/modules/toastNotification';
import {
    getArtist,
    newArtist,
    updateArtist
} from '../../redux/modules/theBackwoods/artist';

class ArtistsAdminEditContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        history: PropTypes.object,
        adminArtists: PropTypes.object,
        id: PropTypes.number
    };

    constructor(props) {
        super(props);

        this.state = {
            editableFieldValues: {},
            artist: {}
        };
    }

    componentDidMount() {
        if (typeof this.props.id !== 'undefined') {
            this.handleGetArtist();
        }
    }

    handleGetArtist = () => {
        const { dispatch, id } = this.props;

        dispatch(getArtist(id));
    };

    handleEditBanner = (imageData) => {
        const { editableFieldValues } = this.state;
        editableFieldValues.image_banner = imageData;

        this.setState({
            editableFieldValues: editableFieldValues
        });
    };

    handleRemoveBanner = () => {
        const { editableFieldValues } = this.state;
        editableFieldValues.image_banner = '';

        this.setState({
            editableFieldValues: editableFieldValues
        });
    };

    handleEditFieldInput = (e) => {
        const { dispatch } = this.props;
        const input = e.currentTarget;
        const { name, type } = input;
        let { value } = input;

        if (name === 'image') {
            previewFile(e.target.files[0])
                .then((result) => {
                    this.setState((prevState) => {
                        const newState = {
                            ...prevState
                        };

                        newState.editableFieldValues[name] = result;
                        return newState;
                    });
                    return;
                })
                .catch((err) => {
                    dispatch(
                        addToast({
                            action: 'message',
                            message: `The image could not be obtained: ${
                                err.message
                            }`
                        })
                    );
                });
            return;
        }

        if (type === 'select-one') {
            value = input.options[input.selectedIndex].value;
        }

        const newState = {
            ...this.state
        };

        newState.editableFieldValues[name] = value;

        this.setState(newState);
    };

    handleUpdateArtist = (artistData) => {
        const {
            adminArtists: { editableArtist },
            dispatch
        } = this.props;

        // Merging artist data with new one
        artistData = {
            ...editableArtist,
            ...artistData
        };

        dispatch(updateArtist(artistData))
            .then(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'Artist successfully updated'
                    })
                );
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'Artist could not be updated'
                    })
                );
            });
    };

    handleNewArtist = (artistData) => {
        const { dispatch, history } = this.props;

        dispatch(newArtist(artistData))
            .then(() => {
                history.push('/the-backwoods/accounts/dashboard');
                return;
            })
            .catch(() => {
                dispatch(
                    addToast({
                        action: 'message',
                        message: 'Artist could not be added'
                    })
                );
            });
    };

    render() {
        const {
            adminArtists: { editableArtist, loading }
        } = this.props;
        const { editableFieldValues } = this.state;
        const isEdit = typeof this.props.id !== 'undefined';

        return (
            <ArtistEdit
                artist={isEdit && !loading ? editableArtist : {}}
                isEdit={isEdit}
                loading={loading}
                editableFieldValues={editableFieldValues}
                onEditFieldInput={this.handleEditFieldInput}
                onUpdateArtist={this.handleUpdateArtist}
                onNewArtist={this.handleNewArtist}
                onBannerEditApply={this.handleEditBanner}
                onBannerRemove={this.handleRemoveBanner}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        adminArtists: state.adminArtists
    };
}

export default connect(mapStateToProps)(ArtistsAdminEditContainer);
