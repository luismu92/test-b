import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import ArtistDashboard from './ArtistDashboardContainer';
import ArtistEdit from './ArtistEditContainer';

class ArtistsAdminContainer extends Component {
    static propTypes = {
        detail: PropTypes.string,
        history: PropTypes.object
    };

    render() {
        const { history } = this.props;
        let { detail } = this.props;
        const id = parseInt(detail, 10);
        let content;

        // Check if is a number
        detail = isNaN(id) ? detail : 'edit';

        switch (detail) {
            case 'dashboard':
                content = <ArtistDashboard />;
                break;

            case 'new':
                content = <ArtistEdit history={history} />;
                break;

            case 'edit':
                content = <ArtistEdit id={id} history={history} />;
                break;

            default:
                history.replace({
                    pathname: '/the-backwoods/accounts/dashboard'
                });
                break;
        }

        return <Fragment>{content}</Fragment>;
    }
}

export default ArtistsAdminContainer;
