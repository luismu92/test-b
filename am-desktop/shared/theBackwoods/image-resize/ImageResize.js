import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import PaperTable from '../components/AdminPaper';
import Add from '../../icons/plus';
import AndroidLoader from '../../loaders/AndroidLoader';
import styles from './ImageResize.module.scss';

class ImageResize extends Component {
    static propTypes = {
        imageUri: PropTypes.string,
        onHandleUpdateImageResize: PropTypes.func,
        onChangeInput: PropTypes.func,
        error: PropTypes.string,
        showMessage: PropTypes.bool,
        loading: PropTypes.bool
    };

    renderLoader() {
        const { loading } = this.props;
        let loader = null;

        if (loading) {
            loader = (
                <div className="u-overlay-loader">
                    <AndroidLoader />
                </div>
            );
        }

        return loader;
    }

    renderForm() {
        const { imageUri, error, showMessage } = this.props;
        let message = '';
        let messageClass = '';

        if (error) {
            messageClass = styles.errorMessage;
            message = error;
        }

        if (showMessage) {
            messageClass = styles.successMessage;
            message = 'Successfully updated image resource';
        }
        return (
            <div className={styles.imageResizeTop}>
                <h5>Update broken and blurry images</h5>
                <p>
                    Update and repair wong size and blurry images by entering
                    their full URL in the entry box
                </p>
                <form
                    onSubmit={this.props.onHandleUpdateImageResize}
                    className={styles.searchBox}
                >
                    <input
                        type="url"
                        placeholder="https://assets.audiomack.com/artist-name/75e4c2f8b19dc003868a981da359fe6ef76a0986259ab2b338af8aa28b1b6bff.jpeg?width=1500&height=1500&max=true"
                        value={imageUri}
                        onChange={this.props.onChangeInput}
                    />
                    <button type="submit" disabled={!imageUri}>
                        <Add />
                    </button>
                </form>

                <p className={messageClass}>{message}</p>
            </div>
        );
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Admin Audiomack</title>
                    <meta name="robots" content="nofollow,noindex" />
                </Helmet>
                <PaperTable
                    title={'Image resize '}
                    topPaper={this.renderForm()}
                    contentPaper={this.renderLoader()}
                />
            </Fragment>
        );
    }
}

export default ImageResize;
