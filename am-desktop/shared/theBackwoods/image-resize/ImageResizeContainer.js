import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ImageResize from './ImageResize';
import { updateImageResizeResource } from '../../redux/modules/theBackwoods/imageResize';

class ImageResizeContainer extends Component {
    static propTypes = {
        dispatch: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            imageUri: '',
            error: '',
            showMessage: false,
            loading: false
        };
    }

    handleUpdateImageResize = async (event) => {
        event.preventDefault();
        const { dispatch } = this.props;
        const { imageUri } = this.state;

        const body = { imageUri: imageUri };

        try {
            this.setState({ loading: true });
            await dispatch(updateImageResizeResource(body));
            this.setState({
                loading: false,
                imageUri: '',
                showMessage: true,
                error: ''
            });
        } catch (err) {
            this.setState({
                loading: false,
                error: err.message,
                showMessage: false,
                imageUri: ''
            });
        }
    };

    handleInput = (e) => {
        this.setState({
            imageUri: e.target.value
        });
    };

    render() {
        const { imageUri, error, showMessage, loading } = this.state;
        return (
            <ImageResize
                imageUri={imageUri}
                error={error}
                showMessage={showMessage}
                loading={loading}
                onHandleUpdateImageResize={this.handleUpdateImageResize}
                onChangeInput={this.handleInput}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        imageResize: state.imageResize
    };
}

export default compose(connect(mapStateToProps))(ImageResizeContainer);
