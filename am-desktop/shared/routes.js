/* eslint react/no-multi-comp: 0, react/prop-types: 0 */
import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import universal from 'react-universal-component';
import {
    COLLECTION_TYPE_TRENDING,
    COLLECTION_TYPE_RECENTLY_ADDED,
    COLLECTION_TYPE_SONG,
    COLLECTION_TYPE_ALBUM,
    COLLECTION_TYPE_ARTIST,
    COLLECTION_TYPE_PLAYLIST,
    GENRE_TYPE_ALL,
    timeMap,
    liveGenres,
    liveGenresExcludedFromCharts
} from 'constants/index';
import { SECTIONS } from 'constants/stats/section';

import analytics from 'utils/analytics';

// import { showMessage, MESSAGE_TYPE_ROUTE_ERROR } from './redux/modules/message';

function noop() {
    return null;
}

function DefaultError({ error }) {
    if (process.env.NODE_ENV === 'development') {
        return (
            <div>
                Error: <p>{error.message}</p>
                <p>{error.stack}</p>
            </div>
        );
    }

    analytics.error(error);

    return null;
}

const universalComponentOptions = {
    loading: noop,
    error: DefaultError
};
const UniversalComponent = universal(
    (props) => import(`./${props.page}`),
    universalComponentOptions
);

function renderComponent(props) {
    if (props.isServer) {
        const Component = universal(
            import(`./${props.page}`),
            universalComponentOptions
        );

        return Component.preload(props).then(() => {
            return {
                Component,
                props
            };
        });
    }

    return <UniversalComponent {...props} />;
}
// If we're here that probably means we have failed to load a chunk.
// i.e. something like /static/dist/0.44lfadsf9430fasf.js doesnt exist anymore
//
// This can happen by the following steps:
// 1. A user loads the site
// 2. A deploy happens where new chunks are generated in the new docker container
// 3. A user tries to navigate to another page where the initially loaded javascript
// will try to load the associated javascript chunk for the new page. Spoiler
// alert: it probably doesnt exist anymore since the new docker container has newly
// build chunks

// function onRouteError(err, store, callback, is404) {
//     if (!is404) {
//         store.dispatch(showMessage(MESSAGE_TYPE_ROUTE_ERROR));
//     }
//     return callback(err);
// }

// You can test your routes here
// https://forbeslindesay.github.io/express-route-tester/

const timePeriodRegex = `(${Object.values(timeMap)
    .filter(Boolean)
    .join('|')})`;
const genreChartPages = liveGenres.reduce((acc, genre) => {
    const genreSlug = genre === GENRE_TYPE_ALL ? '' : `/${genre}`;
    const genreRoutes = [
        {
            path: `${genreSlug}/${COLLECTION_TYPE_TRENDING}/:pageString(page)?/:page(\\d+)?`,
            sitemap: `${genreSlug}/${COLLECTION_TYPE_TRENDING}`,
            context: COLLECTION_TYPE_TRENDING,
            section: SECTIONS.trending
        },
        liveGenresExcludedFromCharts.includes(genre)
            ? null
            : {
                  path: `${genreSlug}/${COLLECTION_TYPE_SONG}/:timePeriod?/:pageString(page)?/:page(\\d+)?`,
                  sitemap: `${genreSlug}/${COLLECTION_TYPE_SONG}`,
                  context: COLLECTION_TYPE_SONG,
                  section: SECTIONS.trendingSongs
              },
        liveGenresExcludedFromCharts.includes(genre)
            ? null
            : {
                  path: `${genreSlug}/${COLLECTION_TYPE_ALBUM}/:timePeriod?/:pageString(page)?/:page(\\d+)?`,
                  sitemap: `${genreSlug}/${COLLECTION_TYPE_ALBUM}`,
                  context: COLLECTION_TYPE_ALBUM,
                  section: SECTIONS.trendingAlbums
              },
        {
            path: `${genreSlug}/${COLLECTION_TYPE_RECENTLY_ADDED}/:timePeriod${timePeriodRegex}?/:pageString(page)?/:page(\\d+)?`,
            sitemap: `${genreSlug}/${COLLECTION_TYPE_RECENTLY_ADDED}`,
            context: COLLECTION_TYPE_RECENTLY_ADDED,
            section: SECTIONS.recentlyAdded
        }
    ]
        .filter(Boolean)
        .map(({ path, context, sitemap, section }) => {
            return {
                path,
                sitemap,
                render(props) {
                    const route = {
                        title: 'Browse New Music',
                        context: context,
                        genre: genre,
                        section: section
                    };
                    const allProps = {
                        page: 'browse/BrowseContainer',
                        ...props,
                        route
                    };

                    return renderComponent(allProps);
                }
            };
        });

    acc = acc.concat(genreRoutes);

    return acc;
}, []);

const genreArtistPages = liveGenres.reduce((acc, genre) => {
    const genreSlug = genre === GENRE_TYPE_ALL ? '' : `/${genre}`;

    acc.push({
        exact: true,
        path: `${genreSlug}/artists/popular`,
        render(props) {
            const route = {
                title: "Browse Audiomack's Top Artists",
                context: COLLECTION_TYPE_ARTIST,
                section: SECTIONS.popularArtists,
                genre
            };
            const allProps = {
                ...props,
                page: 'browse/BrowseArtistsContainer',
                route
            };

            return renderComponent(allProps);
        }
    });

    return acc;
}, []);

export const routeConfig = [
    // Redirect joe's old handle because he's special
    {
        from: '/embed/:musicType(song|album|playlist)/fuckjoevango/:musicSlug',
        to: '/embed/:musicType(song|album|playlist)/joevango/:musicSlug'
    },
    {
        path: '/embed/:musicType(song|album|playlist)/:artistId/:musicSlug',
        render(props) {
            const route = {
                section: SECTIONS.embed
            };

            const allProps = {
                page: 'embed/EmbedPageContainer',
                route,
                ...props
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/embed(\\d+)?(-large|-thin)?/:artistId/:songSlug',
        render({ match }) {
            return (
                <Redirect
                    to={`/embed/song/${match.params.artistId}/${
                        match.params.songSlug
                    }`}
                />
            );
        }
    },

    /* Embed albums */
    {
        path: '/embed(\\d+)?-album/:artistId/:albumSlug',
        render({ match }) {
            return (
                <Redirect
                    to={`/embed/album/${match.params.artistId}/${
                        match.params.albumSlug
                    }`}
                />
            );
        }
    },

    /* Embed playlists */
    {
        path: '/embed(\\d+)?-playlist/:artistId/:playlistSlug',
        render({ match }) {
            return (
                <Redirect
                    to={`/embed/playlist/${match.params.artistId}/${
                        match.params.playlistSlug
                    }`}
                />
            );
        }
    },

    /* Oauth */
    {
        path: '/oauth/authenticate',
        render(props) {
            const route = {
                title: 'Audiomack | Authorize An Application',
                section: SECTIONS.authenticate
            };

            const allProps = {
                route,
                ...props,
                page: 'oauth/AuthorizePageContainer'
            };

            return renderComponent(allProps);
        }
    },

    /* Main app */

    {
        exact: true,
        sitemap: true,
        path: '/',
        render(props) {
            const route = {
                section: SECTIONS.homePage
            };
            const allProps = {
                page: 'home/HomePageContainer',
                ...props,
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/am-shell',
        render(props) {
            const route = {
                title: 'Audiomack'
            };
            const allProps = {
                ...props,
                page: 'Shell',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        sitemap: '/playlists/browse',
        path: '/playlists/browse/:urlSlug?',
        render(props) {
            const route = {
                title: 'Browse New Playlists',
                context: COLLECTION_TYPE_PLAYLIST,
                genre: GENRE_TYPE_ALL,
                section: SECTIONS.newPlaylists
            };
            const allProps = {
                ...props,
                page: 'browse/BrowsePlaylistsContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    ...genreArtistPages,

    // Redirect old recent urls before parsing for new ones
    { from: '/recent/date/:date', to: '/recent' },
    ...genreChartPages,

    /* Individual music pages */

    // Redirect joe's old handle because he's special
    { from: '/song/fuckjoevango/:songSlug', to: '/song/joevango/:songSlug' },
    {
        exact: true,
        path: '/song/:artistId/:songSlug',
        render(props) {
            const route = {
                section: SECTIONS.playSong
            };
            const allProps = {
                ...props,
                page: 'song/SongPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    {
        from: '/album/fuckjoevango/:albumSlug',
        to: '/album/joevango/:albumSlug'
    },
    {
        exact: true,
        path: '/album/:artistId/:albumSlug',
        render(props) {
            const route = {
                section: SECTIONS.playAlbum
            };
            const allProps = {
                ...props,
                page: 'album/AlbumPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    {
        from: '/playlist/fuckjoevango/:playlistSlug',
        to: '/playlist/joevango/:playlistSlug'
    },
    {
        exact: true,
        path: '/playlist/:artistId/:playlistSlug',
        render(props) {
            const route = {
                section: SECTIONS.playlist
            };
            const allProps = {
                ...props,
                page: 'playlist/PlaylistPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/edit/pins',
        render(props) {
            const route = {
                section: SECTIONS.highlightedItems
            };
            const allProps = {
                ...props,
                page: 'edit/PinEditPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/playground/:type',
        render(props) {
            const route = {
                section: SECTIONS.editMusic
            };
            const allProps = {
                ...props,
                page: 'playground/PlaygroundPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/edit/:musicType(song|album|playlist)/:musicId(\\d+)',
        render(props) {
            const route = {
                section: SECTIONS.editMusic
            };
            const allProps = {
                ...props,
                page: 'edit/MusicEditPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path:
            '/edit/:musicType(song|album|playlist)/:musicId(\\d+)/steps/:step(\\d+)',
        render(props) {
            const route = {
                section: SECTIONS.editMusic
            };
            const allProps = {
                ...props,
                page: 'edit/MusicEditPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/edit/profile/:tab?',
        render(props) {
            const route = {
                section: SECTIONS.editProfile
            };
            const allProps = {
                ...props,
                page: 'edit/ProfileEditPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Artist things */

    {
        path: '/artist/:artistId/claim/:code/:ee',
        render(props) {
            const route = {
                section: SECTIONS.claimArtist
            };
            const allProps = {
                ...props,
                page: 'artist/ArtistPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    // Redirect joe's old handle because he's special
    {
        from: '/artist/fuckjoevango/:context?/:pageString(page)?/:page(\\d+)?',
        to: '/artist/joevango/:context?/:pageString(page)?/:page(\\d+)?'
    },
    {
        path: '/artist/:artistId/:context?/:pageString(page)?/:page(\\d+)?',
        render(props) {
            const route = {
                section: SECTIONS.artist
            };
            const allProps = {
                ...props,
                page: 'artist/ArtistPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Upload */

    {
        exact: true,
        sitemap: true,
        path: '/upload',
        render(props) {
            const route = {
                title: 'Choose your upload type',
                section: SECTIONS.upload
            };
            const allProps = {
                ...props,
                page: 'upload/UploadStartPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/upload/songs',
        render(props) {
            const route = {
                title: 'Upload Songs',
                section: SECTIONS.upload
            };
            const allProps = {
                ...props,
                page: 'upload/UploadSongsPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/upload/podcast',
        render(props) {
            const route = {
                title: 'Upload Podcast',
                section: SECTIONS.upload
            };
            const allProps = {
                ...props,
                page: 'upload/UploadSongsPageContainer',
                route,
                uploadType: 'podcast'
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/upload/albums',
        render(props) {
            const route = {
                title: 'Upload New Album',
                section: SECTIONS.upload
            };
            const allProps = {
                ...props,
                page: 'upload/UploadAlbumsPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Search */

    {
        exact: true,
        path: '/search',
        render(props) {
            const route = {
                title: 'Search',
                section: SECTIONS.search
            };
            const allProps = {
                ...props,
                page: 'search/SearchPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Auth */

    {
        exact: true,
        path: '/login',
        render(props) {
            const route = {
                activePage: 'login',
                title: 'Sign in',
                section: SECTIONS.login
            };
            const allProps = {
                ...props,
                page: 'auth/AuthPageContainer',
                activePage: 'login',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/join',
        render(props) {
            const route = {
                activePage: 'join',
                title: 'Sign up',
                section: SECTIONS.signUp
            };
            const allProps = {
                ...props,
                page: 'auth/AuthPageContainer',
                activePage: 'join',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/forgot-password',
        render(props) {
            const route = {
                activePage: 'forgot',
                title: 'Forgot password',
                section: SECTIONS.forgotPassword
            };
            const allProps = {
                ...props,
                page: 'auth/AuthPageContainer',
                activePage: 'forgot',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/switch-account',
        render(props) {
            const route = {
                activePage: 'forgot',
                title: 'Forgot password',
                section: SECTIONS.forgotPassword
            };
            const allProps = {
                ...props,
                page: 'auth/SwitchAccountPageContainer',
                activePage: 'forgot',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* World */
    {
        exact: true,
        sitemap: true,
        path: '/world',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/MainPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        exact: true,
        path: '/world/globe',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/GlobePageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/world/post/:slug',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/PostPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/world/tag/:slug',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/MainPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    {
        path: '/world/:page',
        render(props) {
            const route = {
                title: 'Audiomack World',
                section: SECTIONS.world
            };
            const allProps = {
                ...props,
                page: 'world/MainPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Dashboard */
    {
        path: '/dashboard/:context?/user/:userId(\\d+)?',
        render(props) {
            const route = {
                title: 'Audiomack Artist Dashboard',
                section: SECTIONS.artistDashboard
            };
            const allProps = {
                ...props,
                page: 'dashboard/DashboardPageContainer',
                route
            };

            return <UniversalComponent {...allProps} />;
        }
    },

    {
        path: '/dashboard/:context?/:subcontext?',
        render(props) {
            const route = {
                title: 'Audiomack Artist Dashboard',
                section: SECTIONS.artistDashboard
            };
            const allProps = {
                ...props,
                page: 'dashboard/DashboardPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* The-Backwoods Dashboard */
    {
        path: '/the-backwoods/:context?/:detail?',
        render(props) {
            const route = {
                title: 'Admin Dashboard'
            };
            const allProps = {
                ...props,
                page: 'theBackwoods/AdminPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Validate Artist */
    {
        path: '/validate/artist',
        render(props) {
            const route = {
                title: 'Validate Artist'
            };
            const allProps = {
                ...props,
                page: 'validate/ValidateArtistContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/validate/artist/steps/:step(\\d+)',
        render(props) {
            const route = {
                title: 'Validate Artist'
            };
            const allProps = {
                ...props,
                page: 'validate/ValidateArtistContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Validate Artist */
    {
        path: '/creators',
        render(props) {
            const route = {
                title: 'Validate Artist'
            };
            const allProps = {
                ...props,
                page: 'validate/ValidationLandingContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Monetization */
    {
        exact: true,
        path: '/amp',
        render(props) {
            const route = {
                title: 'Audiomack AMP Program',
                section: SECTIONS.ampProgram
            };
            const allProps = {
                ...props,
                page: 'monetization/AmpPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/amp-code',
        render(props) {
            const route = {
                title: 'Audiomack AMP Code Generator',
                section: SECTIONS.ampCode
            };
            const allProps = {
                ...props,
                page: 'monetization/AmpCodeGeneratorContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/monetization/info',
        render(props) {
            const route = {
                title: 'Payment Info',
                section: SECTIONS.ampProgram
            };
            const allProps = {
                ...props,
                page: 'monetization/PaymentInfoPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    // {
    //     exact: true,
    //     path: '/monetization/royalties',
    //     render(props) {
    //         const route = {
    //             title: 'Payment Royalties',
    //             section: SECTIONS.ampProgram
    //         };
    //         const allProps = {
    //             ...props,
    //             page: 'monetization/RoyaltiesPageContainer',
    //             route
    //         };

    //         return renderComponent(allProps);
    //     }
    // },
    {
        exact: true,
        path: '/monetization/history',
        render(props) {
            const route = {
                title: 'Payment History',
                section: SECTIONS.ampProgram
            };
            const allProps = {
                ...props,
                page: 'monetization/PaymentHistoryPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/monetization/faq',
        render(props) {
            const route = {
                title: 'Monetization FAQ',
                section: SECTIONS.ampProgram
            };
            const allProps = {
                ...props,
                page: 'monetization/MonetizationFaqPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/monetization/:year(\\d+)?/:month(\\d+)?',
        render(props) {
            const route = {
                title: 'Monetization Summary',
                section: SECTIONS.ampProgram
            };
            const allProps = {
                ...props,
                page: 'monetization/MonetizationPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* About */
    {
        exact: true,
        sitemap: true,
        path: '/about',
        render(props) {
            const route = {
                title: 'Create a Free Account',
                section: SECTIONS.about
            };
            const allProps = {
                ...props,
                page: 'about/AboutPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/about/terms-of-service',
        render(props) {
            const route = {
                title: 'Terms of Service',
                section: SECTIONS.tos
            };
            const allProps = {
                ...props,
                page: 'about/TosPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/about/legal',
        render(props) {
            const route = {
                title: 'Legal & DMCA',
                section: SECTIONS.legal
            };
            const allProps = {
                ...props,
                page: 'about/LegalPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/about/privacy-policy',
        render(props) {
            const route = {
                title: 'Privacy Policy',
                section: SECTIONS.privacyPolicy
            };
            const allProps = {
                ...props,
                page: 'about/PrivacyPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/stats/music/:musicId(\\d+)/:context',
        render(props) {
            const route = {
                title: 'Music Stats',
                section: SECTIONS.stats
            };
            const allProps = {
                ...props,
                page: 'stats/MusicStatsDetailContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/stats/music/:musicId(\\d+)',
        render(props) {
            const route = {
                title: 'Music Stats',
                section: SECTIONS.stats
            };
            const allProps = {
                ...props,
                page: 'stats/MusicStatsPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/about/wordpress',
        render(props) {
            const route = {
                title: 'Audiomack Wordpress Plugin',
                section: SECTIONS.wordpress
            };
            const allProps = {
                ...props,
                page: 'about/WordpressPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        sitemap: true,
        path: '/premium-partner-agreement',
        render(props) {
            const route = {
                title: 'Premium Partner Agreement',
                section: SECTIONS.partnerAgreement
            };
            const allProps = {
                ...props,
                page: 'about/PremiumPartnerContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/contact-us/:initialForm?',
        sitemap: '/contact-us',
        render(props) {
            const route = {
                title: 'Contact Us',
                section: SECTIONS.contactUs
            };
            const allProps = {
                ...props,
                page: 'about/ContactPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/notifications',
        render(props) {
            const route = {
                title: 'Your Notifications',
                section: SECTIONS.notifications
            };
            const allProps = {
                ...props,
                page: 'user/NotificationPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        exact: true,
        path: '/premium',
        render(props) {
            const route = {
                title: 'Audiomack Premium',
                section: SECTIONS.premium
            };
            const allProps = {
                ...props,
                page: 'user/PremiumPageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },
    {
        path: '/email/unsubscribe/:hash',
        render(props) {
            const route = {
                title: 'Unsubscribe From Email',
                section: SECTIONS.emailUnsubscribe
            };
            const allProps = {
                ...props,
                page: 'auth/EmailUnsubscribePageContainer',
                route
            };

            return renderComponent(allProps);
        }
    },

    /* Catch all redirects */

    { from: '/rap/playlists', to: '/playlists/browse' },
    { from: '/rap/playlists', to: '/playlists/browse' },
    { from: '/electronic/playlists', to: '/playlists/browse' },
    { from: '/afropop/playlists', to: '/playlists/browse' },
    { from: '/afrobeats/playlists', to: '/playlists/browse' },
    { from: '/dancehall/playlists', to: '/playlists/browse' },
    { from: '/pop/playlists', to: '/playlists/browse' },
    { from: '/podcast/playlists', to: '/playlists/browse' },
    { from: '/playlist', to: '/playlists/browse' },

    { from: '/trending', to: '/trending-now' },
    { from: '/rap', to: '/rap/trending-now' },
    { from: '/electronic', to: '/electronic/trending-now' },
    { from: '/afrobeats', to: '/afrobeats/trending-now' },
    { from: '/afropop', to: '/afrobeats/trending-now' },
    { from: '/dancehall', to: '/dancehall/trending-now' },
    { from: '/pop', to: '/pop/trending-now' },
    { from: '/podcast', to: '/podcast/trending-now' },

    { from: '/manage/playlists/add-to-playlist', to: '/playlists/browse' },
    { from: '/manage/playlists/new', to: '/playlists/browse' },
    { from: '/account/create', to: '/about' },
    { from: '/create', to: '/join' },
    { from: '/upload', to: '/upload' },
    { from: '/manage/albums/create', to: '/upload/albums' },
    { from: '/privacy-policy', to: '/about/privacy-policy' },
    { from: '/legal', to: '/about/legal' },
    { from: '/about/terms', to: '/about/terms-of-service' },
    { from: '/about/privacy', to: '/about/privacy-policy' },

    /* Catch all */
    {
        exact: true,
        path: '*',
        render(props) {
            const route = {
                status: 404,
                section: SECTIONS.notfound
            };
            const allProps = {
                ...props,
                route,
                page: 'NotFound'
            };

            if (props.staticContext) {
                props.staticContext.status = route.status;
            }

            return renderComponent(allProps);
        }
    }
];

export default () => {
    const AppContainer = require('./AppContainer').default;

    return (
        <AppContainer>
            <Switch>
                {routeConfig.map((config, i) => {
                    if (config.to && config.from) {
                        return <Redirect key={i} {...config} />;
                    }

                    if (config.sitemap === true && config.path.includes(':')) {
                        throw new Error(`You have specified "sitemap: true" but the path "${
                            config.path
                        }"
contains a dynamic url param. This will force the exact path, including the dynamic
param, to be included in the sitemap which is not useful to crawlers. Firstly, remove
"sitemap: true" for the path ${
                            config.path
                        }. Secondly, if you think these dynamic
paths should to be included in the sitemap, check out the sitemap worker and add
your custom partial to take care of these dynamic routes. The last option is just to specify an exact string for the "sitemap" key. An example of this was done for the /trending-now path that has optional timePeriod/genre params.`);
                    }

                    return <Route key={i} {...config} />;
                })}
            </Switch>
        </AppContainer>
    );
};
