/* global test, expect */

import { routeConfig } from '../routes';

test('should contain all the necessary upload routes', () => {
    const hasUpload = routeConfig.find((route) => route.path === '/upload');

    expect(hasUpload).toBeTruthy();

    const hasSongUpload = routeConfig.find(
        (route) => route.path === '/upload/songs'
    );

    expect(hasSongUpload).toBeTruthy();

    const hasAlbumUpload = routeConfig.find(
        (route) => route.path === '/upload/albums'
    );

    expect(hasAlbumUpload).toBeTruthy();

    const hasEditAlbumUpload = routeConfig.find(
        (route) =>
            route.path ===
            '/edit/:musicType(song|album|playlist)/:musicId(\\d+)/steps/:step(\\d+)'
    );

    expect(hasEditAlbumUpload).toBeTruthy();
});
