/* global test, expect */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { StaticRouter } from 'react-router';
import Enzyme, { render } from 'enzyme';

import NotFound from '../NotFound';

Enzyme.configure({ adapter: new Adapter() });

test('should render 404 page correctly', () => {
    const store = {
        dispatch() {},
        subscribe() {},
        getState() {
            return {
                musicBrowse: {
                    loading: true,
                    list: []
                }
            };
        }
    };
    const component = (
        <StaticRouter context={{}}>
            <NotFound store={store} />
        </StaticRouter>
    );
    const wrapper = render(component);

    expect(wrapper.text()).toEqual(expect.stringMatching(/^404/));
});
