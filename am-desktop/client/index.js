// @todo upgrade to core-js3 and use the below
// import 'core-js/stable';
// import 'regenerator-runtime/runtime';

import React from 'react';
import { hydrate, render } from 'react-dom';
import { configureStore, history } from 'redux/configureStore';
import { ConnectedRouter } from 'connected-react-router';
import Cookie from 'js-cookie';

import { loadCss } from 'utils/index';
import analytics from 'utils/analytics';
import { HotRoot } from 'components/Root';
import { setConfig } from 'api/index';
import { cookies } from 'constants/index';

import rootReducer from '../shared/redux/reducer';
import routes, { routeConfig } from '../shared/routes';

const initialState = window.__INITIAL_STATE__;
const rootNode = document.getElementById('react-view');
const store = configureStore(initialState, rootReducer, routeConfig);

window.__lastVisitedLocation = null;
history.listen((location) => {
    if (!window.__clientHasBeenRendered) {
        return;
    }

    const { pathname, search } = location;

    window.__lastVisitedLocation = location;
    analytics.pageView(`${pathname}${search}`);
});

function start(appRoutes) {
    const routerProps = {
        history
    };
    const fn = process.env.NODE_ENV === 'development' ? render : hydrate;

    fn(
        <HotRoot
            store={store}
            Router={ConnectedRouter}
            routerProps={routerProps}
            routes={appRoutes}
        />,
        rootNode
    );

    // Setting this flag so we don't fire off a request to fetch data
    // when the page loads since that data will have already been fetched
    // by the server when sending down the HTML with initial state
    //
    // @see connectDataFetchers
    window.__clientHasBeenRendered = true;
}

function fixHover() {
    document.body.addEventListener('keydown', (e) => {
        // Tab
        if (e.which === 9) {
            document.body.classList.add('showfocus');
        }
    });

    document.body.addEventListener('click', () => {
        document.body.classList.remove('showfocus');
    });
}

function polyfillPreloadLinks() {
    document.addEventListener('DOMContentLoaded', function handleDOMloaded() {
        document.removeEventListener('DOMContentLoaded', handleDOMloaded);

        Array.from(
            document.querySelectorAll('link[rel="preload"][as="style"]')
        ).forEach((link) => {
            loadCss(link.href, {
                callback: () => {
                    const isMainStylesheet =
                        link.href.indexOf('/audiomack-desktop.css') !== -1;

                    if (isMainStylesheet) {
                        window.requestAnimationFrame(() => {
                            document.body.classList.remove('css-loading');
                        });
                    }
                },
                existingEl: link,
                appendLink: false
            });
        });
    });
}

function removeTransitionClass() {
    window.addEventListener('load', function handleDOMloaded() {
        document.body.classList.remove('preloading');

        // For non supporting link.onload devices
        document.body.classList.remove('css-loading');
        window.removeEventListener('load', handleDOMloaded);
    });
}

function configureServiceWorker() {
    navigator.serviceWorker
        .register(process.env.SERVICE_WORKER_PATH)
        .then((reg) => {
            // Update the service worker as its possible for a user to
            // stay on the site without reloading for a while
            // https://developers.google.com/web/fundamentals/primers/service-workers/lifecycle#manual_updates
            window.setInterval(() => reg.update(), 1000 * 3600);
            return;
        })
        .catch((err) => {
            console.error(err);
        });
}

function run() {
    const context = 'desktop';

    analytics.release(context, process.env.GIT_COMMIT);

    fixHover();
    removeTransitionClass();
    polyfillPreloadLinks();

    let customApiOptions = {};

    try {
        customApiOptions = JSON.parse(Cookie.get(cookies.apiConfig));
    } catch (e) {} // eslint-disable-line

    setConfig(customApiOptions);

    start(routes);

    window.AM_VERSION = process.env.GIT_COMMIT;
    window.AM_CONTEXT = context;

    if (process.env.NODE_ENV === 'development') {
        window.store = store;
    }

    if (
        process.env.ENABLE_SERVICE_WORKERS === 'true' &&
        'serviceWorker' in navigator
    ) {
        window.addEventListener('load', function registerServiceWorker() {
            configureServiceWorker();

            window.removeEventListener('load', registerServiceWorker);
        });
    }

    const { currentUser } = initialState;

    // Allow admins to enable during runtime if they want
    // Will use for testing
    if (currentUser.isLoggedIn && currentUser.isAdmin) {
        window.configureServiceWorker = configureServiceWorker;
    }
}

run();

if (module.hot) {
    module.hot.accept('../shared/routes', () => {
        const newRoutes = require('../shared/routes').default;

        start(newRoutes);
    });

    module.hot.accept('../shared/redux/reducer', () => {
        const newReducer = require('../shared/redux/reducer').default;

        store.replaceReducer(newReducer);
    });
}
