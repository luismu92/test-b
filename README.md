# test-b

Welcome to the test-b web experience

## Technologies & Concepts

This is a ["universal"](https://medium.com/@mjackson/universal-javascript-4761051b7ae9#.p02wsn180) React app. You should be familiar with the following technologies & patterns if doing any type of work here.

- [React.js](https://facebook.github.io/react/)
- [Redux](https://github.com/reactjs/redux) using the ["Ducks"](https://github.com/erikras/ducks-modular-redux) pattern
- [Dumb & Smart](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0#.48gzhx8m4) components & containers
- [React Portals](https://www.youtube.com/watch?v=z5e7kWSHWTg&feature=youtu.be&t=15m17s) for modals
- <strike>[BEM CSS](http://getbem.com/introduction/) syntax (There are still things that should be converted over)</strike>
- [CSS Modules](https://www.javascriptstuff.com/css-modules-by-example/) <sup style="color: red">New!</sup> This is modeled off of the way CSS/SCSS modules work in [Gatsby](https://www.gatsbyjs.org/tutorial/part-two/#css-modules)
- [Promises](https://developers.google.com/web/fundamentals/primers/promises) Make sure to avoid the [rookie mistakes](https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html)

## Overview

Below are common things that come up during the development of the site. However if you're looking for a high level overview of how everything works together, see [How the Site Works](https://github.com/audiomack/audiomack-js/wiki/How-the-Site-Works).

### Environment Setup

[Check the wiki](https://github.com/audiomack/audiomack-js/wiki/Noob-Setup)

### Icons and SVGs
If you want to add a new icon, add the svg to `public/static/images/(mobile|desktop)/icons/` and then run `npm run icons`. This will generate the icons as React components in the `components/icons` folder which will in turn reference the newly generated SVG sprite sheet. Generally speaking, SVGs should have a square viewbox with the icon taking up either the full width and/or height (i.e. a "minus" icon would take up the full width of the viewbox but have space at the top and bottom). This will ensure icons will have proper line height and it will eliminate the need for some icons to require weird margins for proper adjusting.

### Images
At the time of writing imageotim doesn't run automatically on build so just run `npm run optimize-images` whenever adding new images to the repo.

## Other things I'd probably consider looking into and replacing:

- Replacing connectDataFetchers the currently added [redux-connect](https://github.com/makeomatic/redux-connect)
- Implementing [selectors](https://github.com/reactjs/reselect) to comb through business logic & and memoize actions within the reducers
- Implementing [TypeScript](https://www.typescriptlang.org/)

## Running locally

### Create your own env file

There is already a file in config/env called `.sample.env.yourname`. You should rename this to `.env.yourname` if you have a need for custom ENV variables. Make sure your custom env file lives in the same folder such that the resulting file path would be `config/env/.env.yourname`.

### Local setup

- Clone repo `git clone git@github.com:audiomack/audiomack-js.git`
- Install Node & NPM (using LTS version) https://nodejs.org/en/
- [Install yarn](https://yarnpkg.com/en/docs/install)
- `cd audiomack-js`
- `yarn`

#### Running
- `npm run dev`

or in separate terminal windows:

- `npm run start-dev`
- <strike>`npm run watch:css`</strike> Start using CSS/SCSS modules as described above.
- `npm run watch:test`

> Note: If you're testing on a browser that doesn't support javascript modules ie IE11, you'll need to run `BUILD_TYPE=legacy npm run start-dev`

#### Building css
- <strike>`npm run scss`</strike> Start using CSS/SCSS modules as described above.

You can also lint your files by running `npm run lint`. There isn't a watch command for this at the time of writing but maybe that could be added to `npm run watch`

## Running on dev server

- path: /var/www/audiomack-js
- Upstart script: /etc/init/node.conf
