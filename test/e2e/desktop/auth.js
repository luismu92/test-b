/* global global, describe, beforeAll, afterAll, beforeEach, afterEach, test */

import { uuid } from 'utils/index';
import {
    login,
    logout,
    registerAccount,
    deleteAccount
} from '../../helpers/desktop/auth';
import { InflightRequests, ignoreSpecialRequests } from '../../helpers/index';

const timeout = 20000;
const user = {
    username: 'headlessuser',
    password: 'password',
    email: `headlessuser-${uuid()}@gmail.com`
};

describe('Authentication', () => {
    let page;
    let tracker;

    beforeAll(async () => {
        page = await global.__BROWSER__.newPage();
        await page.emulate({
            viewport: {
                width: 1000,
                height: 800,
                isMobile: false
            },
            userAgent:
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        });
        await ignoreSpecialRequests(page, [
            '/v1/chart',
            '/v1/tags',
            '/blog',
            'www.facebook.com',
            '/static/favicon-16x16.png'
        ]);

        page.on('error', function(err) {
            console.log('page error', err);
        });
    });

    beforeEach(async () => {
        tracker = new InflightRequests(page);
    });

    afterEach(async () => {
        const inflight = tracker.inflightRequests();

        if (inflight.length) {
            //     console.log(inflight.map((request) => `inflight:  ${request.url()}`).join('\n'));
        }

        tracker.dispose();
    });

    afterAll(async () => {
        await page.close();
    });

    test(
        'registration',
        async () => {
            await registerAccount(page, user);
            await logout(page);
        },
        timeout
    );

    test(
        'login',
        async () => {
            await login(page, user);
            await logout(page);
        },
        timeout
    );

    test(
        'delete account',
        async () => {
            await login(page, user);
            await deleteAccount(page, user);
        },
        timeout
    );
});
