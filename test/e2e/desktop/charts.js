/* global global, describe, beforeAll, afterAll, expect, test */

const timeout = 10000;

describe('Chart pages', () => {
    let page;

    beforeAll(async() => {
        page = await global.__BROWSER__.newPage();
        await page.emulate({
            viewport: {
                width: 800,
                height: 600,
                isMobile: false
            },
            userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        });
    });

    afterAll(async() => {
        await page.close();
    });

    test('should load /trending-now successfully', async() => {
        const musicDetailContainer = '[data-testid="browseContainer"]';
        const musicDetailSelector = '[data-testid="musicDetail"]';

        await page.goto(`${process.env.AM_URL}/trending-now`);
        await page.waitForSelector(musicDetailSelector);

        const [
            musicItems,
            childrenCount
        ] = await Promise.all([
            page.$$(musicDetailSelector),
            page.$$eval(`${musicDetailContainer} > *`, (el) => el.length)
        ]);

        const musicItemsCount = musicItems.length;

        expect(musicItemsCount).toBeGreaterThan(0);

        // Test infinite scroll
        const nextMusicItemSelector = `${musicDetailContainer} :nth-child(${childrenCount + 1})`;

        // Scroll all the way down
        await page.evaluate(() => {
            window.scrollBy(0, document.body.scrollHeight);
        });

        await page.waitForSelector(nextMusicItemSelector);

        const newMusicItems = await page.$$(musicDetailSelector);

        expect(newMusicItems.length).toBeGreaterThan(musicItemsCount);
    }, timeout);

    test('should load ranking numbers correctly on /songs successfully', async() => {
        const musicDetailContainer = '[data-testid="browseContainer"]';
        const musicDetailSelector = '[data-testid="musicDetail"]';
        const rankingSelector = '[data-testid="ranking"]';

        await page.goto(`${process.env.AM_URL}/songs`);
        await page.waitForSelector(musicDetailSelector);

        const [
            musicItems,
            childrenCount
        ] = await Promise.all([
            page.$$(musicDetailSelector),
            page.$$eval(`${musicDetailContainer} > *`, (el) => el.length)
        ]);

        const musicItemsCount = musicItems.length;

        expect(musicItemsCount).toBeGreaterThan(0);

        // Test infinite scroll
        const nextMusicItemSelector = `${musicDetailContainer} :nth-child(${childrenCount + 1})`;

        // Scroll all the way down
        await page.evaluate(() => {
            window.scrollBy(0, document.body.scrollHeight);
        });

        await page.waitForSelector(nextMusicItemSelector);

        const newMusicItems = await page.$$(musicDetailSelector);

        expect(newMusicItems.length).toBeGreaterThan(musicItemsCount);

        const rankingHandles = await page.$$(rankingSelector);

        let i = 1;

        for (const handle of rankingHandles) {
            const rankHandle = await handle.getProperty('innerHTML');
            const rank = await rankHandle.jsonValue();

            expect(parseInt(rank, 10)).toBe(i);

            i += 1;
        }
    }, timeout);


    test('should load ranking numbers correctly on /albums successfully', async() => {
        const musicDetailContainer = '[data-testid="browseContainer"]';
        const musicDetailSelector = '[data-testid="musicDetail"]';
        const rankingSelector = '[data-testid="ranking"]';

        await page.goto(`${process.env.AM_URL}/albums`);
        await page.waitForSelector(musicDetailSelector);

        const [
            musicItems,
            childrenCount
        ] = await Promise.all([
            page.$$(musicDetailSelector),
            page.$$eval(`${musicDetailContainer} > *`, (el) => el.length)
        ]);

        const musicItemsCount = musicItems.length;

        expect(musicItemsCount).toBeGreaterThan(0);

        // Test infinite scroll
        const nextMusicItemSelector = `${musicDetailContainer} :nth-child(${childrenCount + 1})`;

        // Scroll all the way down
        await page.evaluate(() => {
            window.scrollBy(0, document.body.scrollHeight);
        });

        await page.waitForSelector(nextMusicItemSelector);

        const newMusicItems = await page.$$(musicDetailSelector);

        expect(newMusicItems.length).toBeGreaterThan(musicItemsCount);

        const rankingHandles = await page.$$(rankingSelector);

        let i = 1;

        for (const handle of rankingHandles) {
            const rankHandle = await handle.getProperty('innerHTML');
            const rank = await rankHandle.jsonValue();

            expect(parseInt(rank, 10)).toBe(i);

            i += 1;
        }
    }, timeout);
});
