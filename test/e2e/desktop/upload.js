/* global global, describe, beforeAll, afterAll, expect, beforeEach, afterEach, test */
import path from 'path';
import {
    generateUserObj,
    registerAccount,
    deleteAccount
} from '../../helpers/desktop/auth';
import { InflightRequests, ignoreSpecialRequests } from '../../helpers/index';
import {
    choosePreviousSongs,
    enterDetails,
    goToUploadPage
} from '../../helpers/desktop/upload';

const timeout = 60000;

describe('Upload', () => {
    let page;
    let tracker;
    let user;

    beforeAll(async () => {
        page = await global.__BROWSER__.newPage();
        await page.emulate({
            viewport: {
                width: 1000,
                height: 800,
                isMobile: false
            },
            userAgent:
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        });
        await ignoreSpecialRequests(page, [
            '/v1/chart',
            '/v1/tags',
            '/blog',
            'www.facebook.com',
            '/static/favicon-16x16.png'
        ]);

        page.on('error', function(err) {
            console.log('page error', err);
        });
    });

    beforeEach(async () => {
        tracker = new InflightRequests(page);
        user = generateUserObj();

        await registerAccount(page, user);
    }, timeout);

    afterEach(async () => {
        // const inflight = tracker.inflightRequests();

        // if (inflight.length) {
        //     console.log(inflight.map((request) => `inflight:  ${request.url()}`).join('\n'));
        // }
        await deleteAccount(page, user);

        tracker.dispose();
    }, timeout);

    afterAll(async () => {
        await page.close();
    });

    test(
        'single song upload',
        async () => {
            const nextStepButton = '[data-testid="nextUploadStep"]';

            await goToUploadPage(page, '[data-testid="song"]', nextStepButton);

            // Upload file
            const uploadInputSelector = '[data-testid="uploadInput"]';

            await page.waitForSelector(uploadInputSelector);
            const uploadingItems = await page.$$(uploadInputSelector);

            expect(uploadingItems.length).toBe(1);

            const uploadInput = await page.$(uploadInputSelector);
            const songPath = path.resolve(
                __dirname,
                '../../helpers/files/1.mp3'
            );
            await uploadInput.uploadFile(songPath);

            // Expect to see uploading item
            const uploadItem = '[data-testid="uploadItem"]';

            await page.waitForSelector(uploadItem);

            // Expect to see completed song upload component
            const completedSongUpload = '[data-testid="songUpload"]';
            const deleteButton = '[data-testid="deleteSongUpload"]';

            await Promise.all([
                page.waitForSelector(completedSongUpload),
                page.waitForSelector(deleteButton)
            ]);

            // Delete song
            await page.click(deleteButton);

            const confirmModalButton = '[data-testid="deleteConfirmButton"]';

            await page.waitForSelector(confirmModalButton);
            await page.click(confirmModalButton);
        },
        timeout
    );

    test(
        'existing song upload',
        async () => {
            const nextStepButton = '[data-testid="nextUploadStep"]';

            await goToUploadPage(page, '[data-testid="song"]', nextStepButton);

            // Upload file
            const uploadInputSelector = '[data-testid="uploadInput"]';

            await page.waitForSelector(uploadInputSelector);
            const uploadingItems = await page.$$(uploadInputSelector);

            expect(uploadingItems.length).toBe(1);

            const uploadInput = await page.$(uploadInputSelector);
            const songPath = path.resolve(
                __dirname,
                '../../helpers/files/1.mp3'
            );
            await uploadInput.uploadFile(songPath);

            // Expect to see uploading item
            const uploadItem = '[data-testid="uploadItem"]';

            await page.waitForSelector(uploadItem);

            // Expect to see completed song upload component
            const completedSongUpload = '[data-testid="songUpload"]';
            const deleteButton = '[data-testid="deleteSongUpload"]';

            await Promise.all([
                page.waitForSelector(completedSongUpload),
                page.waitForSelector(deleteButton)
            ]);

            // Finish song upload & go to song page
            await enterDetails(page, 'song', nextStepButton);
            await page.click(nextStepButton);
            await page.click(nextStepButton);
            await page.click(nextStepButton);

            // Reupload file
            await goToUploadPage(page, '[data-testid="song"]', nextStepButton);

            await page.waitForSelector(uploadInputSelector);
            const reuploadingItems = await page.$$(uploadInputSelector);

            expect(reuploadingItems.length).toBe(1);

            const reuploadInput = await page.$(uploadInputSelector);
            await reuploadInput.uploadFile(songPath);

            // Expect to see uploading item
            await page.waitForSelector(uploadItem);

            // Expect to see already existing status
            const existingSongUpload = '[data-testid="existingSongUpload"]';

            await Promise.all([
                page.waitForSelector(existingSongUpload),
                page.waitForSelector(deleteButton)
            ]);

            // Delete song
            await page.click(deleteButton);

            const confirmDeleteButton = '[data-testid="deleteConfirmButton"]';

            await page.waitForSelector(confirmDeleteButton);
            await page.click(confirmDeleteButton);
        },
        timeout
    );

    test(
        'single zip album upload',
        async () => {
            const albumButton = '[data-testid="album"]';
            const nextStepButton = '[data-testid="nextUploadStep"]';

            await goToUploadPage(page, albumButton, nextStepButton);
            await enterDetails(page, 'album', nextStepButton);
            await choosePreviousSongs(page, { skip: true });

            // Upload file
            const uploadInputSelector = '[data-testid="uploadInput"]';

            await page.waitForSelector(uploadInputSelector);
            const uploadingItems = await page.$$(uploadInputSelector);

            expect(uploadingItems.length).toBe(1);

            const uploadInput = await page.$(uploadInputSelector);
            const albumPath = path.resolve(
                __dirname,
                '../../helpers/files/album.zip'
            );

            await uploadInput.uploadFile(albumPath);

            // Expect to see uploading item
            const uploadItem = '[data-testid="uploadItem"]';
            const uploadItemCompleted = '[data-testid="uploadItemCompleted';

            await Promise.all([
                page.waitForSelector(uploadItem),
                page.waitForSelector(uploadItemCompleted)
            ]);

            await page.click(nextStepButton);

            // Expect to see completed song upload component
            const completedAlbumTracks = '[data-testid="albumTrackUpload"]';
            const deleteButton = '[data-testid="deleteAlbumUpload"]';

            await Promise.all([
                page.waitForSelector(completedAlbumTracks),
                page.waitForSelector(deleteButton)
            ]);

            // Should see 3 tracks in the zip file
            const tracks = await page.$$(completedAlbumTracks);

            expect(tracks.length).toBe(3);

            // Delete album
            await page.click(deleteButton);
            const confirmModalButton = '[data-testid="deleteConfirmButton"]';

            await page.waitForSelector(confirmModalButton);
            await page.click(confirmModalButton);

            // After deleting, we should be back on the upload start page
            await page.waitForNavigation({
                waitUntil: ['domcontentloaded', 'networkidle2']
            });
            await page.waitForSelector(albumButton);
        },
        timeout
    );

    test(
        'multiple song file album upload',
        async () => {
            const albumButton = '[data-testid="album"]';
            const nextStepButton = '[data-testid="nextUploadStep"]';

            await goToUploadPage(page, albumButton, nextStepButton);
            await enterDetails(page, 'album', nextStepButton);
            await choosePreviousSongs(page, { skip: true });

            // Upload file
            const uploadInputSelector = '[data-testid="uploadInput"]';

            await page.waitForSelector(uploadInputSelector);
            const uploadingItems = await page.$$(uploadInputSelector);

            expect(uploadingItems.length).toBe(1);

            const uploadInput = await page.$(uploadInputSelector);
            const songFiles = [1, 2, 3].map((num) => {
                return path.resolve(
                    __dirname,
                    `../../helpers/files/${num}.mp3`
                );
            });

            await uploadInput.uploadFile(...songFiles);

            // Expect to see uploading item
            const uploadItem = '[data-testid="uploadItem"]';
            const uploadItemCompleted = '[data-testid="uploadItemCompleted';

            await Promise.all([
                page.waitForSelector(uploadItem),
                page.waitForSelector(uploadItemCompleted)
            ]);

            // we need to use a timeout because we use data attributes instead of css selectors like '.uploadItemCompleted:nth-of-type(3)'
            await page.waitFor(5000);

            const uploads = await page.$$(uploadItemCompleted);

            expect(uploads.length).toBe(3);

            await page.click(nextStepButton);

            // Expect to see completed song upload component
            const completedAlbumTracks = '[data-testid="albumTrackUpload"]';
            const deleteButton = '[data-testid="deleteAlbumUpload"]';

            await Promise.all([
                page.waitForSelector(completedAlbumTracks),
                page.waitForSelector(deleteButton)
            ]);

            // Should see 3 tracks
            const tracks = await page.$$(completedAlbumTracks);

            expect(tracks.length).toBe(3);

            // Delete album
            await page.click(deleteButton);

            const confirmModalButton = '[data-testid="deleteConfirmButton"]';

            await page.waitForSelector(confirmModalButton);
            await page.click(confirmModalButton);

            // After deleting, we should be back on the upload start page
            await page.waitForNavigation({
                waitUntil: ['domcontentloaded', 'networkidle2']
            });
            await page.waitForSelector(albumButton);
        },
        timeout
    );

    test(
        'Add previous songs to new album upload',
        async () => {
            const albumButton = '[data-testid="album"]';
            const songButton = '[data-testid="song"]';
            const nextStepButton = '[data-testid="nextUploadStep"]';

            // Upload song
            await goToUploadPage(page, songButton, nextStepButton);

            // Upload file
            const uploadInputSelector = '[data-testid="uploadInput"]';

            await page.waitForSelector(uploadInputSelector);
            const uploadingItems = await page.$$(uploadInputSelector);

            expect(uploadingItems.length).toBe(1);

            const uploadInput = await page.$(uploadInputSelector);
            const songPath = path.resolve(
                __dirname,
                '../../helpers/files/1.mp3'
            );
            await uploadInput.uploadFile(songPath);

            // Expect to see uploading item
            const uploadItem = '[data-testid="uploadItem"]';

            await page.waitForSelector(uploadItem);

            // Expect to see completed song upload component
            const completedSongUpload = '[data-testid="songUpload"]';

            await page.waitForSelector(completedSongUpload);

            // Enter song details
            await enterDetails(page, 'song', nextStepButton);

            // Finish steps 2,3, and 4
            await page.click(nextStepButton);
            await page.click(nextStepButton);
            await page.click(nextStepButton);
            const songUrl = page.url();

            // Upload album
            await goToUploadPage(page, albumButton, nextStepButton);
            await enterDetails(page, 'album', nextStepButton);
            await choosePreviousSongs(page);

            // Delete Song
            await page.goto(songUrl, {
                waitUntil: ['domcontentloaded', 'networkidle2']
            });

            const editSongButton = '[title="Edit"]';
            const link = await page.$(editSongButton);
            const hrefValueHandle = await link.getProperty('href');
            const editSongUrl = await hrefValueHandle.jsonValue();

            await link.dispose();

            await page.goto(editSongUrl, {
                waitUntil: ['domcontentloaded', 'networkidle2']
            });

            await page.waitFor(5000);

            const confirmModalButton = '[data-testid="deleteConfirmButton"]';
            const deleteButton = '[data-testid="deleteSongUpload"]';

            await page.waitForSelector(deleteButton);
            await page.click(deleteButton);

            await page.waitForSelector(confirmModalButton);
            await page.click(confirmModalButton);
        },
        timeout
    );
});
