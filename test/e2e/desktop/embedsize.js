/* global global, describe, beforeAll, afterAll, expect, test */
/* eslint no-sync: 0 */

import fs from 'fs';
import path from 'path';
import { PNG } from 'pngjs';
import pixelmatch from 'pixelmatch';
import {
    sizes,
    queryStringVariants,
    urls,
    getFileName,
    getTestName
} from '../../helpers/embed';
import { InflightRequests, ignoreSpecialRequests } from '../../helpers/index';

describe('Embed sizes look okay', function() {
    const testDir = path.join(__dirname, './actualScreenshots');
    const correctDir = path.join(__dirname, './expectedScreenshots');
    const diffDir = path.join(__dirname, './diffScreenshots');
    let page;
    let tracker;

    function parseImage(filePath) {
        return new Promise((resolve) => {
            const img = fs
                .createReadStream(filePath)
                .pipe(new PNG())
                .on('parsed', () => resolve(img));
        });
    }

    function writeDiff(diff, filePath) {
        return new Promise((resolve, reject) => {
            const writeStream = fs.createWriteStream(filePath);

            writeStream.once('finish', () => {
                resolve();
            });

            writeStream.once('error', (err) => {
                reject(err);
            });

            diff.pack().pipe(writeStream);
        });
    }

    function compareScreenshots(fileName) {
        return Promise.all([
            parseImage(`${testDir}/${fileName}.png`),
            parseImage(`${correctDir}/${fileName}.png`)
        ]).then(async ([actualImage, expectedImage]) => {
            const diff = new PNG({
                width: expectedImage.width,
                height: expectedImage.height
            });

            expect(actualImage.width).toEqual(expectedImage.width);
            expect(actualImage.height).toEqual(expectedImage.height);

            const numDiffPixels = pixelmatch(
                actualImage.data,
                expectedImage.data,
                diff.data,
                actualImage.width,
                actualImage.height,
                { threshold: 0.1 }
            );
            // On the linux box vs my mac, This was showing a diff of about 130 pixels
            // and it was due to font discrepancies. Here I give the tests a little more leeway
            const customThreshold =
                numDiffPixels / (expectedImage.width * expectedImage.height);

            if (process.env.DEBUG) {
                await writeDiff(diff, `${diffDir}/${fileName}.png`);
            }

            expect(customThreshold).toBeLessThan(0.3);
            return;
        });
    }

    async function takeAndCompareScreenshot(url, fileName) {
        await page.goto(url, {
            waitUntil: ['domcontentloaded', 'networkidle2']
        });

        const inflight = tracker.inflightRequests();

        if (inflight.length) {
            // console.log(inflight.map((request) => `inflight:  ${request.url()}`).join('\n'));
        }

        // await page.waitForSelector('[data-testid="playButton"]');
        await page.waitFor(1000);
        await page.screenshot({ path: `${testDir}/${fileName}.png` });

        return compareScreenshots(fileName);
    }

    beforeAll(async () => {
        page = await global.__BROWSER__.newPage();

        await ignoreSpecialRequests(page);

        tracker = new InflightRequests(page);

        await page.emulate({
            viewport: {
                width: 800,
                height: 600,
                isMobile: false
            },
            userAgent:
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        });

        // Create necessary directories
        [testDir, correctDir, diffDir].forEach((dir) => {
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
        });
    });

    afterAll(async () => {
        await page.close();
        tracker.dispose();
    });

    async function testSize(obj, size, qs) {
        const [width, height] = size;
        const fileName = getFileName(obj, size, qs);

        await page.setViewport({ width, height });

        await takeAndCompareScreenshot(`${obj.url}${qs}`, fileName);
    }

    for (const obj of urls) {
        for (const size of sizes) {
            for (const qs of queryStringVariants) {
                test(
                    getTestName(obj, size, qs),
                    testSize.bind(null, obj, size, qs)
                );
            }
        }
    }
});
