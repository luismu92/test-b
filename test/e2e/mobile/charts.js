/* global global, describe, beforeAll, afterAll, expect, test */
import devices from 'puppeteer/DeviceDescriptors';

const iPhone = devices['iPhone 6'];
const timeout = 10000;

describe('Chart pages', () => {
    let page;

    beforeAll(async() => {
        page = await global.__BROWSER__.newPage();
        await page.emulate(iPhone);
    });

    afterAll(async() => {
        await page.close();
    });

    test('should load /trending-now successfully', async() => {
        const musicDetailContainer = '[data-testid="browseContainer"]';
        const musicDetailSelector = '[data-testid="listItem"]';

        await page.goto(`${process.env.AM_URL}/songs/total`);
        await page.waitForSelector(musicDetailSelector);

        const [
            musicItems,
            childrenCount
        ] = await Promise.all([
            page.$$(musicDetailSelector),
            page.$$eval(`${musicDetailContainer} > *`, (el) => el.length)
        ]);

        const musicItemsCount = musicItems.length;

        expect(musicItemsCount).toBeGreaterThan(0);

        // Test infinite scroll
        const loaderPlusAnotherItem = 2;
        const nextMusicItemSelector = `${musicDetailContainer} :nth-child(${childrenCount + loaderPlusAnotherItem})`;

        // Scroll all the way down
        await page.evaluate(() => {
            window.scrollBy(0, document.body.scrollHeight);
        });

        await page.waitForSelector(nextMusicItemSelector);

        const newMusicItemsCount = await page.$$(musicDetailSelector);

        expect(newMusicItemsCount.length).toBeGreaterThan(musicItemsCount);
    }, timeout);
});
