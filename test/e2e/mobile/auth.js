/* global global, describe, beforeEach, afterEach, beforeAll, test, expect */

import devices from 'puppeteer/DeviceDescriptors';

import { uuid } from 'utils/index';
import { clickAndWaitForNavigation } from '../../helpers/puppeteer';

import { InflightRequests, ignoreSpecialRequests } from '../../helpers/index';

const iPhone = devices['iPhone 6'];
const timeout = 15000;

describe('Authentication', () => {
    let page;
    let tracker;
    let user;

    async function logout() {
        const artistAvatar = '[data-testid="artistAvatar"]';
        const logoutButton = '[data-testid="logoutButton"]';
        const listItem = '[data-testid="listItem"]';
        const settingsButton = '[data-testid="settingsButton"]';

        await clickAndWaitForNavigation(page, artistAvatar);

        await page.waitForSelector(settingsButton);
        await page.click(settingsButton);

        await page.waitForSelector(logoutButton);
        await page.evaluate((selector) => {
            document.querySelector(selector).click();
        }, logoutButton);

        await page.waitForSelector(listItem);
    }

    beforeAll(async () => {
        user = {
            username: 'headlessuser',
            password: 'password',
            email: `headlessuser-${uuid()}@gmail.com`
        };
    });

    beforeEach(async () => {
        page = await global.__BROWSER__.newPage();
        await page.emulate(iPhone);
        await ignoreSpecialRequests(page);

        tracker = new InflightRequests(page);

        page.on('error', function(err) {
            console.log('page error', err);
        });
    });

    afterEach(async () => {
        const inflight = tracker.inflightRequests();

        if (inflight.length) {
            //     console.log(inflight.map((request) => `inflight:  ${request.url()}`).join('\n'));
        }
        await page.close();
    });

    test(
        'registration',
        async () => {
            const usernameInput = '[data-testid="username"]';
            const passwordInput = '[data-testid="password"]';
            const emailInput = '[data-testid="email"]';
            const submitButton = '[data-testid="registerSubmit"]';
            const birthdayInput = '[data-testid="birthday"]';
            const genderInput = '[data-testid="gender"]';
            const finishButton = '[data-testid="demographicsSubmit"]';
            const listItem = '[data-testid="listItem"]';

            await page.goto(`${process.env.AM_URL}/join`, {
                waitUntil: ['domcontentloaded', 'networkidle2']
            });

            // Set flag to let us register without captcha
            await page.evaluate(() => {
                window.__UA__ = window.__UA__ || {};
                window.__UA__.isHeadless = true;
            });

            await page.waitForSelector(usernameInput);

            await page.type(usernameInput, user.username);
            await page.type(emailInput, user.email);
            await page.type(passwordInput, user.password);

            // await clickAndWaitForNavigation(page, submitButton);
            await page.click(submitButton);

            await page.waitForSelector(birthdayInput);

            await page.type(birthdayInput, '01/01/2000');
            await page.select(genderInput, 'non-binary');
            await clickAndWaitForNavigation(page, finishButton);

            await page.waitForSelector(listItem);
            await logout();
        },
        timeout
    );

    async function login() {
        const emailInput = '[data-testid="email"]';
        const passwordInput = '[data-testid="password"]';
        const submitButton = '[data-testid="loginSubmit"]';
        const artistName = '[data-testid="artistName"]';
        const listItem = '[data-testid="listItem"]';
        const continueToLoginButton = '[data-testid="continueToLogin"]';

        await page.goto(`${process.env.AM_URL}/login`, {
            waitUntil: ['domcontentloaded', 'networkidle2']
        });

        // Set flag to let us register without captcha
        await page.evaluate(() => {
            window.__UA__ = window.__UA__ || {};
            window.__UA__.isHeadless = true;
        });

        await page.waitForSelector(emailInput);
        await page.type(emailInput, user.email);

        // Press continue button
        await page.waitForSelector(continueToLoginButton);
        await page.click(continueToLoginButton);

        await page.waitForSelector(passwordInput);
        await page.type(passwordInput, user.password);

        await page.click(submitButton);

        await Promise.all([
            page.waitForSelector(artistName),
            page.waitForSelector(listItem)
        ]);
    }

    test(
        'login',
        async () => {
            await login();
            await logout();
        },
        timeout
    );

    test(
        'delete account',
        async () => {
            await login();

            const artistAvatar = '[data-testid="artistAvatar"]';

            const settingsButton = '[data-testid="settingsButton"]';
            const editProfileButton = '[data-testid="editProfile"]';
            const deleteButton = '[data-testid="deleteAccount"]';
            const listItem = '[data-testid="listItem"]';

            const deletePasswordInput = '[data-testid="deletePassword"]';
            const submitButton = '[data-testid="deletePasswordSubmit"]';

            await clickAndWaitForNavigation(page, artistAvatar);

            await page.waitForSelector(settingsButton);
            await page.click(settingsButton);

            await page.waitForSelector(editProfileButton);
            await page.evaluate((selector) => {
                document.querySelector(selector).click();
            }, editProfileButton);

            await page.waitForSelector(deleteButton);
            await page.evaluate((selector) => {
                document.querySelector(selector).click();
            }, deleteButton);

            await page.waitForSelector(deletePasswordInput);
            await page.type(deletePasswordInput, user.password);
            await page.click(submitButton);

            // After deleting you go to home page
            await clickAndWaitForNavigation(page, listItem);

            const link = await page.$(artistAvatar);
            const hrefValueHandle = await link.getProperty('href');
            const href = await hrefValueHandle.jsonValue();

            await link.dispose();

            expect(href).toBe(`${process.env.AM_URL}/login`);
        },
        timeout
    );
});
