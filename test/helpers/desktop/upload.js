/* global expect */

// General selectors
const uploadButton = '[data-testid="uploadButton"]';
const uploadItem = '[data-testid="uploadItem"]';
const genreValue = '[data-testid="genre-rap"]';

// Album selectors
const albumArtistInput = '[data-testid="albumArtist"]';
const albumGenreInput = '[data-testid="albumGenre"]';
const albumTitleInput = '[data-testid="albumTitle"]';

// Song selectors
const songArtistInput = '[data-testid="songArtist"]';
const songGenreInput = '[data-testid="songGenre"]';
const songTitleInput = '[data-testid="songTitle"]';

// Modal selectors
const addPreExistingTracksModal = '[data-choice="choose"]';
const addPreExistingTracksButton = '[data-testid="addPreExistingTracks';
const closeChoosePreviousSongsModal = '[data-choice="close"]';
const selectPreviousUpload = '[data-testid="selectPreviousUpload"]';

export async function choosePreviousSongs(page, { skip } = { skip: false }) {
    const modalTimeout = 2000;

    if (skip) {
        try {
            // close ChoosePreviousSongsModal
            await page.waitForSelector(closeChoosePreviousSongsModal, {
                timeout: modalTimeout
            });
            await page.click(closeChoosePreviousSongsModal);
        } catch (err) {
            // No modal due to no uploads
            console.log(
                'ChoosePreviousSongsModal did not appear due to the user having no previous uploads'
            );
        } finally {
            return;
        }
    }

    await page.waitForSelector(addPreExistingTracksModal, {
        timeout: modalTimeout
    });
    await page.click(addPreExistingTracksModal);

    await Promise.all([
        page.waitForSelector(addPreExistingTracksButton),
        page.waitForSelector(selectPreviousUpload)
    ]);

    await page.click(selectPreviousUpload);
    await page.click(addPreExistingTracksButton);

    await page.waitForSelector(uploadItem);

    const previousUpload = await page.$$(uploadItem);
    expect(previousUpload.length).toBe(1);
}

export async function enterDetails(page, type, next) {
    let artistInput;
    let genreInput;
    let titleInput;

    switch (type) {
        case 'album':
            artistInput = albumArtistInput;
            genreInput = albumGenreInput;
            titleInput = albumTitleInput;
            break;

        case 'song':
            artistInput = songArtistInput;
            genreInput = songGenreInput;
            titleInput = songTitleInput;
            break;

        default:
            break;
    }

    // Fill out initial album details
    await Promise.all([
        page.waitForSelector(artistInput),
        page.waitForSelector(genreInput),
        page.waitForSelector(titleInput)
    ]);

    await page.type(artistInput, 'Artist');
    await page.type(titleInput, 'Title');
    await page.click(genreInput);

    await page.waitForSelector(genreValue);
    await page.click(genreValue);

    await page.click(next);
}

export async function goToUploadPage(page, uploadTypeButton, next) {
    // Navigate to UploadStartPage
    await page.waitForSelector(uploadButton);
    const link = await page.$(uploadButton);
    const hrefValueHandle = await link.getProperty('href');
    const href = await hrefValueHandle.jsonValue();
    await link.dispose();
    await page.goto(href, {
        waitUntil: ['domcontentloaded', 'networkidle2']
    });

    // Select upload type
    await page.waitForSelector(uploadTypeButton);
    await page.click(uploadTypeButton);
    await page.click(next);
}
