export function generateUserObj() {
    const id = Date.now();

    return {
        email: `ayo+test_e2e_${id}@audiomack.com`,
        password: 'password',
        username: `${id}`
    };
}

export async function login(page, userObj) {
    // userObj.email = 'headlessuser-7a95e297-60ba-4fdf-b852-2ecec18b273d@gmail.com';

    const signinButton = '[data-testid="signin"]';
    const loginButton = '[data-testid="loginSubmit"]';
    const continueToLoginButton = '[data-testid="continueToLogin"]';
    const emailInput = '[data-testid="email"]';
    const passwordInput = '[data-testid="password"]';
    const forgotPasswordLink = '[data-testid="forgot-password"]';
    const headerAvatar = '[data-testid="headerAvatar"]';

    await page.goto(process.env.AM_URL, {
        waitUntil: ['domcontentloaded', 'networkidle2']
    });

    // Click signin button on header
    await page.waitForSelector(signinButton);
    await page.click(signinButton);

    // Wait for email input in modal
    await page.waitForSelector(emailInput);

    // Fill out email
    await page.type(emailInput, userObj.email);

    // Press continue button
    await page.click(continueToLoginButton);

    // Wait for final submit form elements before logging in
    await Promise.all([
        page.waitForSelector(passwordInput),
        page.waitForSelector(loginButton),
        page.waitForSelector(forgotPasswordLink)
    ]);

    await page.type(passwordInput, userObj.password);

    // Proceed
    await Promise.all([
        page.waitForNavigation({
            waitUntil: ['domcontentloaded', 'networkidle2']
        }),
        page.click(loginButton)
    ]);

    await page.waitForSelector(headerAvatar);
}

export async function logout(page) {
    const artistAvatar = '[data-testid="headerAvatar"]';
    const logoutButton = '[data-testid="logoutButton"]';
    const signinButton = '[data-testid="signin"]';

    await page.waitForSelector(artistAvatar);
    await page.evaluate((selector) => {
        document.querySelector(selector).click();
    }, artistAvatar);

    await page.waitForSelector(logoutButton);
    await page.evaluate((selector) => {
        document.querySelector(selector).click();
    }, logoutButton);

    await page.waitForSelector(signinButton);
}

export async function registerAccount(page, user) {
    const signupButton = '[data-testid="signup"]';
    const usernameInput = '[data-testid="username"]';
    const passwordInput = '[data-testid="password"]';
    const emailInput = '[data-testid="email"]';
    const submitButton = '[data-testid="registerSubmit"]';
    // This has to be name because there is no way to add data-testid to
    // React Datepicker
    const birthdayInput = '[name="birthday"]';
    const genderInput = '[data-testid="gender"]';
    const finishButton = '[data-testid="demographicsSubmit"]';
    const headerAvatar = '[data-testid="headerAvatar"]';

    await page.goto(process.env.AM_URL, {
        waitUntil: ['domcontentloaded', 'networkidle2']
    });

    // Set flag to let us register without captcha
    await page.evaluate(() => {
        window.__UA__ = window.__UA__ || {};
        window.__UA__.isHeadless = true;
    });

    await page.click(signupButton);
    await page.waitForSelector(usernameInput);

    await page.type(emailInput, user.email);
    await page.type(usernameInput, user.username);
    await page.type(passwordInput, user.password);

    await page.click(submitButton);

    await page.waitForSelector(birthdayInput);

    await page.click(birthdayInput, { clickCount: 3 });
    await page.type(birthdayInput, '01/01/2000');
    await page.select(genderInput, 'non-binary');
    await page.click(finishButton);

    await page.waitForSelector(headerAvatar);
}

export async function deleteAccount(page, user) {
    const headerAvatar = '[data-testid="headerAvatar"]';

    const editProfileButton = '[data-testid="editProfile"]';
    const deleteButton = '[data-testid="deleteAccount"]';

    const deletePasswordInput = '[data-testid="deletePassword"]';
    const submitButton = '[data-testid="deletePasswordSubmit"]';
    const signinButton = '[data-testid="signin"]';

    await page.click(headerAvatar);

    await page.waitForSelector(editProfileButton);

    const link = await page.$(editProfileButton);
    const hrefValueHandle = await link.getProperty('href');
    const href = await hrefValueHandle.jsonValue();

    await link.dispose();

    await page.goto(href);

    await page.waitFor(200);
    await page.waitForSelector(deleteButton);
    await page.click(deleteButton);

    await page.waitForSelector(deletePasswordInput);
    await page.type(deletePasswordInput, user.password);
    await page.click(submitButton);

    await page.waitForSelector(signinButton);
}
