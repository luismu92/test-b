// Debug pupeteer requests to see which may have failed the test
module.exports.InflightRequests = class InflightRequests {
    constructor(page) {
        this._page = page;
        this._requests = new Set();
        this._onStarted = this._onStarted.bind(this);
        this._onFinished = this._onFinished.bind(this);
        this._page.on('request', this._onStarted);
        [
            'error',
            'pageerror',
            'close',
            'requestfinished',
            'requestfailed'
        ].forEach((evt) => {
            this._page.on(evt, this._onFinished);
        });
    }

    _onStarted(request) {
        this._requests.add(request);
    }
    _onFinished(request) {
        this._requests.delete(request);
    }

    inflightRequests() {
        return Array.from(this._requests);
    }

    dispose() {
        this._page.removeListener('request', this._onStarted);
        this._page.removeListener('requestfinished', this._onFinished);
        this._page.removeListener('requestfailed', this._onFinished);
    }
};

module.exports.ignoreSpecialRequests = function ignoreSpecialRequests(page, additionalReqests = []) {
    return page.setRequestInterception(true).then(() => {
        page.on('request', (interceptedRequest) => {
            const url = interceptedRequest.url();
            // These are requests we dont need to wait for
            // a response for as to not hang puppeteer
            const unncessaryRequest = [
                '__webpack_hmr',
                '/account/login/modal',
                '/account/logout',
                'crwdcntrl.net',
                'amazon-adsystem.com',
                'doubleclick.net',
                'casalemedia.com',
                'googlesyndication.com',
                'secure.adnxs.com',
                'facebook.com',
                'fastlane.rubiconproject.com',
                'google-analytics.com',
                'quantserve',
                'googleapis.com',
                'newrelic.com',
                'google.com/recaptcha',
                'googletagservices.com',
                'gstatic.com/recaptcha',
                'about:blank',
                'bam.nr-data.net',
                'favicon',
                'quantcast',
                'scorecardresearch.com',
                'zopim.com',
                'zdassets.com'
            ].concat(additionalReqests).some((str) => {
                return url.includes(str);
            });

            if (unncessaryRequest) {
                interceptedRequest.abort();
                return;
            }

            interceptedRequest.continue();
        });
        return;
    });
};
