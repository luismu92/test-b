module.exports.sizes = [
    [316, 80],
    [266, 191],
    [450, 50],
    [600, 250],
    [705, 144],
    [545, 105],
    // Smino embed here https://www.hotnewhiphop.com/smino-and-mick-jenkins-link-up-on-new-coupe-who-dis-new-song.1978629.html
    [355, 252],
    // So we can see album/playlist tracks
    [500, 500]
];

module.exports.urls = [
    {
        type: 'song',
        url: `${process.env.AM_URL}/embed/song/tester/way-back`
    },
    {
        type: 'album',
        url: `${process.env.AM_URL}/embed/album/tester/audiomack-ep`
    },
    {
        type: 'playlist',
        url: `${process.env.AM_URL}/embed/playlist/tester/test-playlist`
    }
];

const queryStringVariants = ['?background=1', '?color=ff0000'];

module.exports.queryStringVariants = queryStringVariants;

module.exports.getFileName = function getFileName(urlObj, size, queryString) {
    const [width, height] = size;
    const index = queryStringVariants.indexOf(queryString);

    return `${urlObj.type}${index}-${width}x${height}`;
};
module.exports.getTestName = function getFileName(urlObj, size, queryString) {
    const [width, height] = size;
    const index = queryStringVariants.indexOf(queryString);

    return `${urlObj.type}${index} - ${width}x${height} - ${queryString.substr(
        1
    )}`;
};
