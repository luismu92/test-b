global.document = require('jsdom').jsdom('<body></body>');
global.window = document.defaultView;
global.navigator = window.navigator;
global.Image = window.Image;
global.Event = window.Event;

const testElement = document.createElement('div');
testElement.id = 'test';
document.body.appendChild(testElement);
