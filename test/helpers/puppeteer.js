module.exports.clickAndWaitForNavigation = async function clickAndWaitForNavigation(
    page,
    selector
) {
    // If a click causes navigation, we have to wait for the
    // navigation as well using this pattern to avoid race condition
    await page.waitForSelector(selector);
    await Promise.all([
        page.waitForNavigation({
            waitUntil: ['domcontentloaded', 'networkidle2']
        }),
        page.click(selector)
    ]);
};
