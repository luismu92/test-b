#! /bin/bash

# Install yarn. At the time of writing `yarn` is still faster than npm install
npm i -g yarn

# Add githook that runs `yarn` when doing a `git pull` and the package.json changes
curl -O https://gist.githubusercontent.com/okcoker/0222c1c5e0b53711c0d566d47ad59c47/raw/11f6136910128c0d01eb85c00d12110f3e9e676f/post-merge && chmod +x post-merge && mv post-merge .git/hooks/
