const path = require('path');

/* eslint camelcase: 0 */
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */

module.exports.config = {
    /**
     * Array of application names.
     */
    app_name: ['audiomack-js'],
    /**
     * Your New Relic license key.
     */
    license_key: process.env.NEW_RELIC_KEY,

    filepath: path.resolve(__dirname, '../logs', 'newrelic.log'),

    logging: {
        /**
         * Level at which to log. 'trace' is most useful to New Relic when diagnosing
         * issues with the agent, 'info' and higher will impose the least overhead on
         * production applications.
         */
        level: 'info'
    },

    error_collector: {
        ignore_status_codes: [404, 410, 451, 501]
    }
};
