const path = require('path');
const CLASSNAME = '{CLASSNAME}';
const ICON_FILE_NAME = '{ICON_FILE_NAME}';

const template = `/**
 * Generated by inline-svg-template
 */

import React, { Component } from 'react';

export default class ${CLASSNAME} extends Component {
    render() {
        return (
            {IST_SVG}
        );
    }
}
`;

/**
 * Parse SVG contents before SVG is inserted into template
 * @param  {String} contents SVG contents to be inserted
 * @return {String}          Parsed SVG contents
 */
function parse() {
    const svg = `<svg role="img" title="${ICON_FILE_NAME}" {...this.props}>
                <use xlinkHref="/static/images/am-mobile-sprite-sheet.svg#${ICON_FILE_NAME}" />
            </svg>`;

    return svg;
}

function camelCaseClass(str) {
    const formatted = str.replace(/^([A-Z])|[\s-_](\w)/g, function(match, p1, p2) {
        if (p2) {
            return p2.toUpperCase();
        }
        return p1.toLowerCase();
    });

    return formatted.charAt(0).toUpperCase() + formatted.substr(1);
}

/**
 * Optional replacement functions keyed by the string to replace. The string
 * key will be passed to a global RegExp constructor. Each function will be passed
 * the current template contents and filepath. It should return the string to
 * replace the key with. Order matters.
 * @type {Object}
 */
const replacements = {
    [CLASSNAME](contents, filepath) {
        const basePath = path.basename(filepath, '.svg');

        return camelCaseClass(basePath);
    },
    [ICON_FILE_NAME](contents, filepath) {
        const name = path.basename(filepath, '.svg');

        return name;
    }
};

module.exports = {
    template,
    parse,
    replacements
};
