const webpack = require('webpack');
// const path = require('path');
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');

const buildAppConfigs = require('./buildAppConfigs');
const configureCssChunkPlugin = require('./buildAppConfigs')
    .configureCssChunkPlugin;
const getBundleAnalyzerPlugin = require('./buildAppConfigs')
    .getBundleAnalyzerPlugin;

// Allow you to build for only one app if you want by passing something
// like BUILD_FOR=mobile webpack --config ...
const appFromEnv = (process.env.BUILD_FOR || '').toLowerCase();
const apps = appFromEnv ? [appFromEnv] : undefined;

function buildConfig(types = apps) {
    return buildAppConfigs(types).map((data) => {
        const { app, definedEnv, common } = data;
        const config = merge.strategy({
            plugins: 'prepend'
        })(common, {
            output: {
                filename: '[id].[chunkhash].js',
                chunkFilename: '[id].[chunkhash].chunk.js'
            },
            devtool: 'hidden-source-map',
            mode: 'production',
            bail: true,
            plugins: [
                configureCssChunkPlugin(),
                new webpack.HashedModuleIdsPlugin(),
                new webpack.DefinePlugin({
                    DEBUG: false,
                    'process.env': Object.assign({}, definedEnv, {
                        BROWSER: JSON.stringify('true')
                    })
                }),
                new TerserPlugin({
                    sourceMap: true,
                    terserOptions: {
                        compress: {
                            drop_console: true
                        }
                    }
                })
            ]
        });

        if (process.env.BUNDLE_ANALYZER === 'true') {
            config.plugins.push(getBundleAnalyzerPlugin());
        }

        config.entry[app].unshift('eventsource-polyfill');

        // IE11 doesnt support adding multiple classes at once
        config.entry[app].unshift('classlist-polyfill');
        config.entry[app].unshift('@babel/polyfill');

        return config;
    });
}

module.exports = buildConfig;
