function time(fn, options = {}) {
    const displayTimesOver = options.displayTimesOver || 500;

    return function() {
        const startTime = Date.now();
        const ret = fn.apply(fn, arguments);
        const endTime = Date.now();
        const duration = endTime - startTime;

        if (duration > displayTimesOver) {
            const fnToString = fn.toString().replace(/\n+|\r+/g, '');
            const fnString = fn.name || fn.displayName || fnToString;

            console.log(`${duration}ms`, fnString);
        }

        return ret;
    };
}
class TimeAllPlugin {
    constructor(options = {}) {
        this.displayTimesOver = options.displayTimesOver || 500;
    }

    apply(compiler) {
        compiler.plugin('entry-option', function() {
            Object.keys(this._plugins).forEach((eventHook) => {
                this._plugins[eventHook] = this._plugins[eventHook].map((fn) => {
                    return time(fn, {
                        displayTimesOver: this.displayTimesOver
                    });
                });
            });
        });
    }
}

module.exports = TimeAllPlugin;
