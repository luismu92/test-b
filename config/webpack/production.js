const buildClientConfigs = require('./client.production');
const buildServerConfig = require('./server.production');

module.exports = [buildServerConfig()].concat(buildClientConfigs());
