/* eslint no-sync: 0 */
const webpack = require('webpack');
const merge = require('webpack-merge');
const fs = require('fs');
const path = require('path');
const buildAppConfigs = require('./buildAppConfigs');
const webappPlugins = require('./buildAppConfigs').webappPlugins;
const WriteFilePlugin = require('write-file-webpack-plugin');
const isServer = true;

function buildConfig() {
    // App argument doesnt matter here
    const { definedEnv, common, rootPath } = buildAppConfigs(
        ['desktop'],
        isServer
    )[0];
    const outputPath = path.join(rootPath, '_devServer');

    const parsedEnv = Object.keys(process.env).reduce((acc, key) => {
        acc[key] = JSON.stringify(process.env[key]);
        return acc;
    }, {});
    const filename = 'server.js';
    const bundlePath = `${outputPath}/${filename}`;
    const externalModules = fs
        .readdirSync(path.join(rootPath, 'node_modules'))
        .filter(
            (x) =>
                !/\.bin|react-universal-component|webpack-flush-chunks/.test(x)
        )
        .reduce((acc, mod) => {
            acc[mod] = `commonjs ${mod}`;
            return acc;
        }, {});

    externalModules['react-dom/server'] = 'commonjs react-dom/server';

    const config = merge.strategy({
        plugins: 'append',
        entry: 'replace',
        output: 'replace',
        optimization: 'replace'
    })(common, {
        name: 'server',
        devtool: 'cheap-module-source-map',
        entry: {
            server: [path.resolve(__dirname, '../../server/server')]
        },
        target: 'node',
        node: {
            __filename: true,
            __dirname: true
        },
        externals: externalModules,
        output: {
            filename: filename,
            libraryTarget: 'commonjs2',
            path: outputPath,
            pathinfo: false
        },
        optimization: {
            removeAvailableModules: false,
            removeEmptyChunks: false,
            nodeEnv: false
        },
        // profile: true,
        // stats: 'verbose',
        plugins: [
            // new webpack.optimize.LimitChunkCountPlugin({
            //     maxChunks: 1
            // }),
            // new webpack.ProgressPlugin({
            //     entries: true,
            //     modules: true,
            //     modulesCount: 100,
            //     profile: true
            //     // handler: (percentage, message, ...args) => {
            //     //     // custom logic
            //     // }
            // }),
            // new webpack.debug.ProfilingPlugin({
            //     outputPath: `build/server-${Date.now()}.json`
            // }),
            new webpack.DefinePlugin({
                'process.env': Object.assign({}, parsedEnv, definedEnv, {
                    IS_WEBPACK: JSON.stringify('true'),
                    ROOT_PATH: JSON.stringify(rootPath),
                    BUNDLE_PATH: JSON.stringify(bundlePath)
                })
            }),
            ...webappPlugins
        ]
    });

    if (process.env.LOCAL_DEV === 'true') {
        config.plugins.push(new WriteFilePlugin());
    }

    return config;
}

module.exports = buildConfig;
