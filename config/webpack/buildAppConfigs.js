require('../../am-shared/utils/setEnv');

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const baseStyles = new ExtractTextPlugin({
//     filename: 'app.css',
//     allChunks: true
// });
// import AssetsPlugin from 'assets-webpack-plugin';

const APP_CHROMECAST = 'chromecast';
const APP_DESKTOP = 'desktop';
const APP_MOBILE = 'mobile';
const validApps = [APP_MOBILE, APP_DESKTOP, APP_CHROMECAST];

const pkg = require('../../package.json');
const getBabelConfig = require('../../babel.config').getBabelConfig;

// When the server bundle is built, root path will be provided and we can
// build our paths using it since __dirname is different after webpack
// builds the bundle. There's probably a better way of handling this...
const rootPath = process.env.ROOT_PATH || path.join(__dirname, '../../');

// Configure Babel loader
function configureBabelLoader(browserList, isServer) {
    const isProd = process.env.NODE_ENV === 'production';
    const config = {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader',
            options: {
                cacheDirectory: true,
                ...getBabelConfig({
                    isProd,
                    browserList,
                    modern: false,
                    isServer
                })
            }
        }
    };

    config.exclude = function(modulePath) {
        return (
            /node_modules/.test(modulePath) &&
            // These modules need compilation for IE11
            !/node_modules\/query-string/.test(modulePath) &&
            !/node_modules\/strict-uri-encode/.test(modulePath) &&
            !/node_modules\/split-on-first/.test(modulePath)
        );
    };

    return config;
}

// Configure Manifest
function configureManifest() {
    return {
        fileName: 'manifest.json',
        basePath: '',
        map(file) {
            file.name = file.name.replace(/(\.[a-f0-9]{32})(\..*)$/, '$2');
            return file;
        }
    };
}

let ident = 0;

// Configure css loaders
function configureCssLoader(options = {}) {
    return {
        loader: require.resolve('css-loader'),
        options: {
            sourceMap: process.env.NODE_ENV !== 'production',
            camelCase: 'dashesOnly',
            localIdentName: '[name]__[local]--[hash:base64:5]',
            ...options
        }
    };
}

function configureCssRule(isServer) {
    const styleLoader = {
        loader: require.resolve('style-loader'),
        options: {
            sourceMap: process.env.NODE_ENV !== 'production'
        }
    };
    const cssModuleLoader = configureCssLoader({
        modules: true,
        importLoaders: 1
    });
    const cssLoader = configureCssLoader({ importLoaders: 1 });
    const cssModuleLoaderForSass = configureCssLoader({
        modules: true,
        importLoaders: 2
    });
    const cssLoaderForSass = configureCssLoader({ importLoaders: 2 });
    const postcssLoader = {
        loader: require.resolve('postcss-loader'),
        options: {
            ident: `postcss-${++ident}`,
            sourceMap: process.env.NODE_ENV !== 'production',
            config: {
                path: path.resolve(__dirname, '../')
            }
        }
    };
    const sassLoader = {
        loader: require.resolve('sass-loader'),
        options: {
            sourceMap: process.env.NODE_ENV !== 'production'
        }
    };
    const cssModuleTest = /\.module\.css$/;
    const cssTest = /\.css$/;
    const sassModuleTest = /\.module\.scss$/;
    const sassTest = /\.scss$/;

    const styleOrExtractLoader = isServer
        ? ExtractCssChunks.loader
        : styleLoader;
    const styleOrExtractLoaderModules = isServer
        ? ExtractCssChunks.loader
        : {
              ...styleLoader,
              options: {
                  ...styleLoader.options,
                  hmr: false
              }
          };

    if (process.env.NODE_ENV === 'development') {
        return {
            oneOf: [
                {
                    test: sassModuleTest,
                    exclude: /node_modules/,
                    use: [
                        styleOrExtractLoaderModules,
                        cssModuleLoaderForSass,
                        postcssLoader,
                        sassLoader
                    ].filter(Boolean)
                },
                {
                    test: sassTest,
                    use: [
                        styleOrExtractLoader,
                        cssLoaderForSass,
                        postcssLoader,
                        sassLoader
                    ]
                },
                {
                    test: cssModuleTest,
                    exclude: /node_modules/,
                    use: [
                        styleOrExtractLoaderModules,
                        cssModuleLoader,
                        postcssLoader
                    ].filter(Boolean)
                },
                {
                    test: cssTest,
                    use: [styleOrExtractLoader, cssLoader, postcssLoader]
                }
            ]
        };
    }

    return {
        oneOf: [
            {
                test: sassModuleTest,
                exclude: /node_modules/,
                use: [
                    ExtractCssChunks.loader,
                    cssModuleLoaderForSass,
                    postcssLoader,
                    sassLoader
                ]
            },
            {
                test: sassTest,
                use: [
                    ExtractCssChunks.loader,
                    cssLoaderForSass,
                    postcssLoader,
                    sassLoader
                ]
            },
            {
                test: cssModuleTest,
                exclude: /node_modules/,
                use: [ExtractCssChunks.loader, cssModuleLoader, postcssLoader]
            },
            {
                test: cssTest,
                use: [ExtractCssChunks.loader, cssLoader, postcssLoader]
            }
        ]
    };
}

function configureCssChunkPlugin() {
    const options = {
        // This may not be the safest thing to do but the warnings are annoying
        // and it doesnt seem like we can do anything (reasonable) about it:
        //
        // https://github.com/facebook/create-react-app/issues/5372#issuecomment-455665081
        // https://github.com/webpack-contrib/mini-css-extract-plugin/issues/250#issuecomment-447346852
        ignoreOrder: true
    };

    if (process.env.NODE_ENV === 'production') {
        options.filename = '[id].[chunkhash].css';
        options.chunkFilename = '[id].[chunkhash].chunk.css';
    }

    return new ExtractCssChunks(options);
}

function configureEslintRule() {
    return {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
            {
                loader: 'eslint-loader',
                options: {
                    quiet: true
                }
            }
        ]
    };
}

function getBundleAnalyzerPlugin() {
    return new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        reportFilename: 'report.html',
        logLevel: 'silent'
    });
}

// Configure terser
function configureTerser() {
    const options = {
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
            compress: {
                drop_console: true
            }
        }
    };

    return options;
}

// Configure optimization
function configureOptimization(isServer = false) {
    const optimization = {
        nodeEnv: false,
        runtimeChunk: {
            name: 'manifest'
        },
        splitChunks: {
            chunks: 'initial',
            cacheGroups: {
                vendors: {
                    // test: /[\\/]node_modules[\\/]/,
                    test(module /* , chunk */) {
                        // Only node_modules are needed
                        if (!module.context.includes('/node_modules/')) {
                            return false;
                        }

                        // Modules that are big, used in one part of the app,
                        // and dont need to be included in the main vendor bundle
                        const excludedModules = [
                            'transliteration',
                            'chart.js',
                            'react-color',
                            'react-datepicker',
                            'branch-sdk',
                            'cropperjs',
                            'react-virtualized',
                            'react-sortable-hoc'
                        ];
                        // But not node modules that contain these key words in the path
                        const exclude = excludedModules.some((str) => {
                            return module.context.includes(str);
                        });

                        if (exclude) {
                            return false;
                        }

                        return true;
                    },
                    name: 'common.js',
                    chunks: 'all'
                },
                // Break out the main app code from node modules so cachibility
                // on node modules is a bit better
                app: {
                    test: /[\\/]am-desktop[\\/]|[\\/]am-mobile[\\/]/
                    // test(module) {
                    //     if (!module.context.includes('/am-desktop/') && !module.context.includes('/am-mobile/')) {
                    //         return false;
                    //     }
                    // @todo figure out how to break out components that seem
                    // to be common amongst many chunks
                    //     // Modules that are big, used in one part of the app,
                    //     // and dont need to be included in the main app bundle
                    //     const excludedModules = [
                    //         'CustomDatePicker'
                    //     ];
                    //     const exclude = excludedModules.some((str) => {
                    //         return module.rawRequest && module.rawRequest.includes(str);
                    //     });

                    //     console.log('moduleRequest', module.rawRequest);

                    //     if (exclude) {
                    //         console.log(module.rawRequest);
                    //         return false;
                    //     }

                    //     return true;
                    // }
                },
                shared: {
                    test: /[\\/]am-shared[\\/]/,
                    name: 'shared.js'
                }

                // @todo we can eventually make a music page cache group
                // that includes the popular music page dependencies as it
                // seems those pages are the biggest chunks along with the
                // MusicEditPageContainer
            }
        }
    };

    if (process.env.NODE_ENV === 'production') {
        optimization.minimizer = [];

        if (!isServer) {
            optimization.minimizer.push(new TerserPlugin(configureTerser()));
        }

        optimization.minimizer.push(
            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: {
                    map: {
                        inline: false,
                        annotation: true
                    },
                    safe: true,
                    discardComments: true
                }
            })
        );
    }

    return optimization;
}

function configureCommonWebpackConfig(app, isServer) {
    const browserList = pkg.browserslist.legacyBrowsers;
    const name = `client-${app}`;
    const common = {
        name: name,
        mode: process.env.NODE_ENV,
        entry: {
            [app]: [`./am-${app}/client/index`]
        },
        target: 'web',
        output: {
            filename: '[name].js',
            chunkFilename: '[name].chunk.js',
            path: path.join(rootPath, `public/static/dist/${app}`),
            publicPath: `/static/dist/${app}/`,
            pathinfo: false
        },
        resolve: {
            modules: [
                path.resolve(__dirname, '../../am-shared'),
                path.resolve(__dirname, '../../node_modules')
            ]
        },
        // stats: 'verbose',
        module: {
            rules: [
                configureBabelLoader(browserList, isServer),
                configureCssRule(isServer)
            ]
        },
        optimization: configureOptimization(isServer),
        plugins: [
            // https://medium.com/@kudochien/a-react-webpack-optimization-case-27da392fb3ec
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            new webpack.IgnorePlugin(/__tests__/),
            isServer ? configureCssChunkPlugin() : undefined
        ].filter(Boolean)
    };

    return common;
}

// Configure Webapp webpack
function getWebappPlugins() {
    return [
        new WebappWebpackPlugin({
            logo: path.resolve(__dirname, './favicon.png'),
            prefix: '',
            cache: true,
            publicPath: '/static/',
            outputPath: '../public/static',
            favicons: {
                appName: pkg.name,
                appDescription: pkg.description,
                developerName: pkg.name,
                developerURL: pkg.homepage,
                background: '#161616',
                // 'theme_color': '#ffa200'
                theme_color: '#161616'
            }
        }),
        // Generate a favicons.js file that has all of the output favicon HTML.
        // There's a .rule() method on the plugin itself but I think it may only
        // work with browser as the compiled server code does not replace the
        // require(favicon/path) does not convert to the html
        new (class {
            constructor() {
                // So we can filter this out during worker builds.
                // This is also a bit hacky
                this.name = 'WebappWebpackPluginHelper';
            }

            apply(compiler) {
                compiler.hooks.make.tapAsync('A', (compilation, callback) => {
                    compilation.hooks.webappWebpackPluginBeforeEmit.tapAsync(
                        'B',
                        (result, cb) => {
                            const faviconPath = path.join(
                                compilation.outputOptions.path,
                                '../public/static/favicons.html'
                            );
                            const contents = result.tags.join('\n');

                            // The result of favicons library can be modified here
                            // and it will be returned to WebApp Plugin to be emitted.
                            // Add your custom functions below
                            fs.writeFile(faviconPath, contents, (err) => {
                                if (err) {
                                    cb(err);
                                    return;
                                }

                                cb(null, result);
                            });
                        }
                    );
                    return callback();
                });
            }
        })()
    ];
}

function getServiceWorkerPlugin(app) {
    if (app !== APP_DESKTOP && app !== APP_MOBILE) {
        return null;
    }

    const staticDir = '../../';

    return new WorkboxPlugin.GenerateSW({
        swDest: `${staticDir}service-worker-${app}.js`,
        // swDest: `${staticDir}service-worker.js`,
        // precacheManifestFilename: `${app}-precache-manifest.[manifestHash].js`,
        // importsDirectory: path.join(__dirname, '../../public/static/'),
        precacheManifestFilename: 'precache-manifest.[manifestHash].js',
        importScripts: [`/static/workbox-catch-handler-${app}.js`],
        exclude: [
            /\.(png|jpe?g|gif|svg|webp)$/i,
            /\.map$/,
            /^manifest.*\.js(?:on)?$/
        ],
        // navigateFallback doesnt add the html file to the precache so
        // im manually adding that inside the workbox-catch-handler
        // globPatterns: ['static/*.html'],
        // navigateFallback: `/static/shell-${app}.html`,
        offlineGoogleAnalytics: true,
        runtimeCaching: [
            {
                urlPattern: /\.(?:png|jpe?g|gif|svg|webp)$/i,
                handler: 'CacheFirst',
                options: {
                    cacheName: 'images',
                    expiration: {
                        maxEntries: 20
                    }
                }
            }
        ]
    });
}

module.exports = function buildAppConfigs(apps = validApps, isServer) {
    if (!Array.isArray(apps)) {
        throw new Error(
            `Config builder requires an array of apps as a single argument but got: ${apps}`
        );
    }

    return apps.map(function(app) {
        if (validApps.indexOf(app) === -1) {
            throw new Error(
                `App name must be one of "${validApps.join(
                    ', '
                )}". You passed ${app}`
            );
        }

        const config = {
            app,
            rootPath,
            pkg,
            isServer,
            definedEnv: {
                // Variables that should be visible to the browser
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
                AM_URL: JSON.stringify(process.env.AM_URL),
                AM_NAME: JSON.stringify(process.env.AM_NAME),
                AM_DOMAIN: JSON.stringify(process.env.AM_DOMAIN),
                HOT_LOADER_LOG_LEVEL: JSON.stringify(
                    process.env.HOT_LOADER_LOG_LEVEL
                ),
                FACEBOOK_APP_ID: JSON.stringify(process.env.FACEBOOK_APP_ID),
                GOOGLE_AUTH_ID: JSON.stringify(process.env.GOOGLE_AUTH_ID),
                GOOGLE_MAPS_API_KEY: JSON.stringify(
                    process.env.GOOGLE_MAPS_API_KEY
                ),
                INSTAGRAM_APP_ID: JSON.stringify(process.env.INSTAGRAM_APP_ID),
                INSTAGRAM_REDIRECT_URI: JSON.stringify(
                    process.env.INSTAGRAM_REDIRECT_URI
                ),
                APPLE_CLIENT_ID: JSON.stringify(process.env.APPLE_CLIENT_ID),
                APPLE_REDIRECT_URI: JSON.stringify(
                    process.env.APPLE_REDIRECT_URI
                ),
                API_VERSION: JSON.stringify(process.env.API_VERSION || 'v1'),
                STATIC_URL: JSON.stringify(process.env.STATIC_URL),
                API_URL: JSON.stringify(process.env.API_URL),
                AUDIOMACK_DEV: JSON.stringify(process.env.AUDIOMACK_DEV),
                API_CONSUMER_KEY: JSON.stringify(process.env.API_CONSUMER_KEY),
                API_CONSUMER_SECRET: JSON.stringify(
                    process.env.API_CONSUMER_SECRET
                ),
                GIT_COMMIT: JSON.stringify(process.env.GIT_COMMIT),
                ANDROID_APP_NAME: JSON.stringify(process.env.ANDROID_APP_NAME),
                ANDROID_APP_PACKAGE: JSON.stringify(
                    process.env.ANDROID_APP_PACKAGE
                ),
                IOS_APP_NAME: JSON.stringify(process.env.IOS_APP_NAME),
                IOS_APP_ID: JSON.stringify(process.env.IOS_APP_ID),
                PHP_BACKEND_URL: JSON.stringify(process.env.PHP_BACKEND_URL),
                BRANCH_URL: JSON.stringify(process.env.BRANCH_URL),
                BRANCH_KEY: JSON.stringify(process.env.BRANCH_KEY),
                COMSCORE_ID: JSON.stringify(process.env.COMSCORE_ID),
                CAPTCHA_SITEKEY: JSON.stringify(process.env.CAPTCHA_SITEKEY),
                CHROMECAST_RECEIVER_ID: JSON.stringify(
                    process.env.CHROMECAST_RECEIVER_ID
                ),
                SONGTRUST_CLIENT_ID: JSON.stringify(
                    process.env.SONGTRUST_CLIENT_ID
                ),
                CHROMECAST_URN: JSON.stringify(process.env.CHROMECAST_URN),
                SERVICE_WORKER_PATH: JSON.stringify(
                    `/service-worker-${app}.js`
                ),
                ENABLE_SERVICE_WORKERS: JSON.stringify(
                    process.env.ENABLE_SERVICE_WORKERS
                ),
                MIXPANEL_TOKEN: JSON.stringify(process.env.MIXPANEL_TOKEN),
                ZENDESK_SDK_KEY: JSON.stringify(process.env.ZENDESK_SDK_KEY),
                STRIPE_KEY: JSON.stringify(process.env.STRIPE_KEY),
                BLOG_URL: JSON.stringify(process.env.BLOG_URL),
                BLOG_CONTENT_KEY: JSON.stringify(process.env.BLOG_CONTENT_KEY),
                GA_MEASUREMENT_ID: JSON.stringify(
                    process.env.GA_MEASUREMENT_ID
                ),
                GA_PROPERTY_ID: JSON.stringify(process.env.GA_PROPERTY_ID),
                GA_PROPERTY_ID_EMBED: JSON.stringify(
                    process.env.GA_PROPERTY_ID_EMBED
                ),
                MAINTENANCE_MODE: JSON.stringify(process.env.MAINTENANCE_MODE),
                ARTIST_VALIDATION_REQUIRED_TOTAL_PLAYS: JSON.stringify(
                    process.env.ARTIST_VALIDATION_REQUIRED_TOTAL_PLAYS
                ),
                ARTIST_VALIDATION_REQUIRED_MUSIC_UPLOADS: JSON.stringify(
                    process.env.ARTIST_VALIDATION_REQUIRED_MUSIC_UPLOADS
                )
            },
            common: configureCommonWebpackConfig(app, isServer)
        };

        return config;
    });
};

module.exports.configureCssRule = configureCssRule;
module.exports.configureEslintRule = configureEslintRule;
module.exports.getBundleAnalyzerPlugin = getBundleAnalyzerPlugin;
module.exports.configureOptimization = configureOptimization;
module.exports.configureManifest = configureManifest;
module.exports.getServiceWorkerPlugin = getServiceWorkerPlugin;
module.exports.webappPlugins = getWebappPlugins();
module.exports.configureCssChunkPlugin = configureCssChunkPlugin;
module.exports.validApps = validApps;
module.exports.APP_CHROMECAST = APP_CHROMECAST;
module.exports.APP_DESKTOP = APP_DESKTOP;
