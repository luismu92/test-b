const buildClientConfigs = require('./client.development');
const buildServerConfig = require('./server.development');

// module.exports = buildClientConfigs();
module.exports = [buildServerConfig()].concat(buildClientConfigs());
