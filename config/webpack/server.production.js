const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');

const buildAppConfigs = require('./buildAppConfigs');
const devConfig = require('./server.development')();

function buildConfig() {
    // App argument doesnt matter here
    const { definedEnv, rootPath } = buildAppConfigs(['desktop'])[0];
    const outputPath = path.join(rootPath, 'dist/server');
    const parsedEnv = Object.keys(process.env).reduce((acc, key) => {
        acc[key] = JSON.stringify(process.env[key]);
        return acc;
    }, {});
    const filename = 'server.js';
    const bundlePath = `${outputPath}/${filename}`;

    const config = merge.strategy({
        entry: 'replace',
        plugins: 'append'
    })(devConfig, {
        output: {
            path: outputPath
        },
        devtool: 'source-map',
        mode: 'production',
        watch: false,
        bail: true,
        entry: {
            server: [path.join(rootPath, 'server/server')]
        },
        stats: 'errors-only',
        // stats: 'verbose',
        plugins: [
            new webpack.LoaderOptionsPlugin({
                // Indicates to our loaders that they should minify their output
                // if they have the capability to do so.
                // minimize: true,
                // Indicates to our loaders that they should enter into debug mode
                // should they support it.
                debug: false
            }),
            // new webpack.optimize.ModuleConcatenationPlugin(),
            // new webpack.ProgressPlugin({
            //     entries: true,
            //     modules: true,
            //     modulesCount: 100,
            //     profile: true
            //     // handler: (percentage, message, ...args) => {
            //     //     // custom logic
            //     // }
            // }),
            new webpack.DefinePlugin({
                'process.env': Object.assign({}, parsedEnv, definedEnv, {
                    NODE_ENV: JSON.stringify('production'),
                    ROOT_PATH: JSON.stringify(rootPath),
                    BUNDLE_PATH: JSON.stringify(bundlePath)
                })
            }),
            new webpack.HashedModuleIdsPlugin()

            // new webpack.debug.ProfilingPlugin({
            //     outputPath: 'profiling/profileEvents.json'
            // }),
        ]
    });

    return config;
}

module.exports = buildConfig;
