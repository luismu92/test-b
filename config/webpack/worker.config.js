/* eslint no-sync: 0 */
require('../../am-shared/utils/setEnv');

const merge = require('webpack-merge');
const serverConfig = require(`./server.${process.env.NODE_ENV}`)();

const config = merge.strategy({
    entry: 'replace',
    plugins: 'append'
})(serverConfig, {
    devtool: false,
    entry: []
});

config.plugins = config.plugins.filter((plugin) => {
    return (
        plugin.constructor.name !== 'WebappWebpackPlugin' &&
        plugin.name !== 'WebappWebpackPluginHelper'
    );
});

module.exports = config;
