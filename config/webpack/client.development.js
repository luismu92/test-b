const webpack = require('webpack');
const merge = require('webpack-merge');
const ManifestPlugin = require('webpack-manifest-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const buildAppConfigs = require('./buildAppConfigs');
const configureManifest = require('./buildAppConfigs').configureManifest;
const configureEslintRule = require('./buildAppConfigs').configureEslintRule;
const getServiceWorkerPlugin = require('./buildAppConfigs')
    .getServiceWorkerPlugin;
// const configureBundleAnalyzer = require('./buildAppConfigs').configureBundleAnalyzer;
// Use to debug compiled code if necessary
// const TimeAllPlugins = require('./plugins/TimeAllPlugins');

// Allow you to build for only one app if you want by passing something
// like BUILD_FOR=mobile webpack --config ...
const appFromEnv = (process.env.BUILD_FOR || '').toLowerCase();
const apps = appFromEnv ? [appFromEnv] : undefined;

function buildConfig(types = apps) {
    return buildAppConfigs(types).map((data) => {
        const { app, definedEnv, common } = data;
        const config = merge.strategy({
            'module.rules': 'append',
            plugins: 'prepend'
        })(common, {
            output: {
                // Avoid React cross-origin errors
                // See https://reactjs.org/docs/cross-origin-errors.html
                crossOriginLoading: 'anonymous'
            },
            devtool: 'cheap-module-source-map',
            module: {
                rules: [configureEslintRule()]
            },
            resolve: {
                alias: {
                    'react-dom': '@hot-loader/react-dom'
                }
            },
            // profile: true,
            // stats: 'verbose',
            plugins: [
                new webpack.HotModuleReplacementPlugin(),
                new ManifestPlugin(configureManifest()),
                // new webpack.ProgressPlugin({
                //     entries: true,
                //     modules: true,
                //     modulesCount: 100,
                //     profile: true
                //     // handler: (percentage, message, ...args) => {
                //     //     // custom logic
                //     // }
                // }),
                // new webpack.debug.ProfilingPlugin({
                //     outputPath: `build/${app}-${Date.now()}.json`
                // }),
                // new BundleAnalyzerPlugin(configureBundleAnalyzer()),
                // new TimeAllPlugins(),
                // new webpack.WatchIgnorePlugin(getWatchIgnoreArg(app)),
                new webpack.DefinePlugin({
                    DEBUG: true,
                    'process.env': Object.assign({}, definedEnv, {
                        BROWSER: JSON.stringify('true')
                    })
                })
            ]
        });

        config.entry[app].unshift(
            `webpack-hot-middleware/client?noInfo=false&name=${config.name}`
        );

        config.entry[app].unshift('eventsource-polyfill');

        // IE11 doesnt support adding multiple classes at once
        config.entry[app].unshift('classlist-polyfill');
        config.entry[app].unshift('@babel/polyfill');

        const plugin = getServiceWorkerPlugin(app);

        if (plugin) {
            config.plugins.unshift(plugin);
        }

        return config;
    });
}

module.exports = buildConfig;
