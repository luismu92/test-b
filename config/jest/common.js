const glob = require('glob');
const path = require('path');

require('utils/setEnv');

// Explicitly set to "test" so the appropriate babel settings
// take effect as expected
process.env.NODE_ENV = 'test';

const roots = ['am-desktop', 'am-mobile', 'am-shared', 'server'].reduce(
    (acc, folder) => {
        const files = glob.sync(`./${folder}/**/__tests__`).map((p) => {
            return path.resolve(p);
        });

        acc = acc.concat(files);
        return acc;
    },
    []
);

module.exports = {
    notify: true,
    verbose: true,
    roots: roots,
    testPathIgnorePatterns: ['__tests__/__snapshots__']
};
