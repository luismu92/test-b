const common = require('./common');

require('utils/setEnv');

// Explicitly set to "test" so the appropriate babel settings
// take effect as expected
process.env.NODE_ENV = 'test';

const preDeployTestsOnly = process.env.TEST_TYPE === 'predeploy';
const postDeployTestsOnly = process.env.TEST_TYPE === 'postdeploy';
const pattern = process.env.FILES || '';
let testRegex;

if (preDeployTestsOnly) {
    testRegex = `^(?!.*__tests__/.*${pattern}.postdeploy.js).*`;
} else if (postDeployTestsOnly) {
    testRegex = `__tests__/.*${pattern}.postdeploy.js$`;
}

if (process.env.TEST_REGEX) {
    testRegex = `__tests__/${process.env.TEST_REGEX}`;
}

module.exports = Object.assign({}, common, {
    testRegex: testRegex,
    transform: {
        '^.+\\.js?$': './transform.js'
    },
    // https://medium.com/trabe/testing-css-modules-in-react-components-with-jest-enzyme-and-a-custom-modulenamemapper-8ff86c7d18a2
    moduleNameMapper: {
        '\\.s?css$': 'identity-obj-proxy'
    }
});
