const glob = require('glob');
const path = require('path');
const common = require('../common');

const e2eFiles = glob.sync('./test/e2e').map((p) => {
    return path.resolve(p);
});

const regex = process.env.FILES || process.env.E2E_REGEX || '';

// https://github.com/xfumihiro/jest-puppeteer-example
module.exports = Object.assign({}, common, {
    roots: e2eFiles,
    globalSetup: './setup.js',
    globalTeardown: './teardown.js',
    testEnvironment: './PuppeteerEnvironment.js',
    testRegex: `test/e2e/.*${regex}.js$`
});
