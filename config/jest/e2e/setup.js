/* global global */
const chalk = require('chalk');
const puppeteer = require('puppeteer');
const fs = require('fs');
const mkdirp = require('mkdirp');
const os = require('os');
const path = require('path');

const DIR = path.join(os.tmpdir(), 'jest_puppeteer_global_setup');

module.exports = async function() {
    console.log(chalk.green('Setup Puppeteer'));
    const port = process.env.PORT || 3000;
    const config = {
        args: []
    };

    if (process.env.NO_SANDBOX === 'true') {
        config.args.push('--no-sandbox');
    }

    if (process.env.HEADLESS === 'false') {
        config.headless = false;
        config.slowMo = 100;

        config.args.push(
            '--disable-web-security',
            '--remote-debugging-address=0.0.0.0',
            `--remote-debugging-port=${port}`
        );
    }

    const browser = await puppeteer.launch(config);

    global.__BROWSER__ = browser;
    mkdirp.sync(DIR);
    fs.writeFileSync(path.join(DIR, 'wsEndpoint'), browser.wsEndpoint()); // eslint-disable-line
};
