export default function handler(err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }

    if (process.env.NODE_ENV !== 'development') {
        return res.status(500).end('Internal server error');
    }

    if (err.data) {
        return res.status(500).end(err.data);
    }

    return res.status(500).end(err.stack);
}
