import React from 'react';
import Helmet from 'react-helmet';
import fs from 'fs';
import path from 'path';

import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import serializeJS from 'serialize-javascript';
import { flushChunkNames } from 'react-universal-component/server';
import flushChunks from 'webpack-flush-chunks';

import fetchComponentsData from 'lib/fetchComponentsData';
import api from 'api/index';
import { configureStore } from '../../am-shared/redux/configureStore';

import { parseSongUrl, parseAlbumUrl, parseArtistUrl } from 'utils/index';
import Root from 'components/Root';

import mobileRoutes, {
    routeConfig as mobileRouteConfig
} from '../../am-mobile/shared/routes';
import mobileReducer from '../../am-mobile/shared/redux/reducer';
import desktopRoutes, {
    routeConfig as desktopRouteConfig
} from '../../am-desktop/shared/routes';
import desktopReducer from '../../am-desktop/shared/redux/reducer';

import { LOG_IN } from '../../am-shared/redux/modules/user/index';
import mobileUserReducer from '../../am-mobile/shared/redux/modules/user/index';

import desktopUserReducer from '../../am-desktop/shared/redux/modules/user/index';
import {
    getMusicIdByUrl,
    getMusicKeyById,
    getArtistIdBySlug
} from '../helpers/redis';
import {
    getLayoutVars,
    getNewRelic,
    getBuildPath,
    addNonceToScript,
    getQuantcast,
    getComscore,
    getFacebookPixel,
    getAdmiralScript,
    getGdprTag,
    getMixpanel,
    getGa,
    getFirebaseScript,
    getLiveRamp
} from '../helpers/index';

const inlineDesktopCss = '/static/css/audiomack-desktop-critical.css';
const inlineMobileCss = '/static/css/audiomack-mobile-critical.css';

/* eslint-disable no-sync */
const desktopCriticalCss = fs.readFileSync(
    getBuildPath(__dirname, path.join('../../public', inlineDesktopCss)),
    'utf-8'
);
const mobileCriticalCss = fs.readFileSync(
    getBuildPath(__dirname, path.join('../../public', inlineMobileCss)),
    'utf-8'
);
// This file gets generated when the server gets built by webpack.
const favicons = fs.readFileSync(
    getBuildPath(__dirname, path.join('../../public/static/favicons.html')),
    'utf-8'
);
/* eslint-enable no-sync */

const inlineCssMap = {
    [inlineDesktopCss]: desktopCriticalCss,
    [inlineMobileCss]: mobileCriticalCss
};

function isDown(status) {
    return (
        status === 'takedown' ||
        status === 'suspended' ||
        status === 'unplayable' ||
        !status
    );
}

function isItemSuspended(value) {
    return (
        !!value &&
        !!value.resolved &&
        !!value.resolved.results &&
        !!value.resolved.results.status &&
        isDown(value.resolved.results.status)
    );
}

// Checks to see if a given action has a "force404OnError" key when
// there is an error and then checks the request url against that "force404OnError"
// value to determine if a 404 should be thrown
function _getStatusWithFetchedData(requestUrl, fetchedValues = []) {
    const filtered = fetchedValues.filter(Boolean);

    for (let i = 0; i < filtered.length; i++) {
        const values = filtered[i];

        if (Array.isArray(values)) {
            for (let k = 0; k < values.length; k++) {
                const value = values[k];

                if (
                    value.force404OnError &&
                    requestUrl.match(value.force404OnError)
                ) {
                    if (value.error) {
                        return Promise.resolve(404);
                    }

                    if (isItemSuspended(value)) {
                        return Promise.resolve(410);
                    }
                }
            }
        } else if (
            values.error &&
            values.force404OnError &&
            requestUrl.match(values.force404OnError)
        ) {
            return Promise.resolve(404);
        }
    }

    return Promise.resolve(200);
}

async function getStatus(requestUrl, fetchedValues) {
    const songResults = parseSongUrl(requestUrl);
    const albumResults = parseAlbumUrl(requestUrl);
    const artistResults = parseArtistUrl(requestUrl);

    try {
        let results;
        let type;

        if (songResults) {
            results = songResults;
            type = 'song';
        } else if (albumResults) {
            results = albumResults;
            type = 'album';
        } else if (artistResults) {
            results = artistResults;
            type = 'artist';
        }

        if (results) {
            if (type === 'artist') {
                const artistId = await getArtistIdBySlug(results.artistSlug);

                return Promise.resolve(artistId ? 200 : 404);
            }

            const musicId = await getMusicIdByUrl(
                type,
                results.artistSlug,
                results.musicSlug
            );

            if (!musicId) {
                return Promise.resolve(404);
            }

            const status = await getMusicKeyById(musicId, 'status');

            if (isDown(status)) {
                return Promise.resolve(410);
            }

            return Promise.resolve(200);
        }
    } catch (err) {
        if (process.env.NODE_ENV === 'development') {
            console.log(
                'Since there is no local redis instance running, you may be getting a false 200 error. You can safely ignore the following error if you dont care about status codes.'
            );
            console.log(err);
        }
    }

    return _getStatusWithFetchedData(requestUrl, fetchedValues);
}

function getResourceHints() {
    // Break off /v1
    const apiUrl = process.env.API_URL.split('/')
        .slice(0, -1)
        .join('/');

    return `
        <link rel="preconnect" href="${process.env.ASSETS_URL}" crossorigin>
        <link rel="preconnect" href="${apiUrl}" crossorigin>
        <link rel="preconnect" href="https://bam.nr-data.net" crossorigin>
        <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="dns-prefetch" href="https://tpc.googlesyndication.com">
        <link rel="dns-prefetch" href="https://unequalbrake.com">
        <link rel="dns-prefetch" href="https://imasdk.googleapis.com">
        <link rel="dns-prefetch" href="https://www.googletagservices.com">
        <link rel="dns-prefetch" href="http://sync.crwdcntrl.net">
        <link rel="dns-prefetch" href="http://pixel.quantserve.com">
        <link rel="dns-prefetch" href="https://googleads.g.doubleclick.net">
        <link rel="dns-prefetch" href="https://securepubads.g.doubleclick.net">
        <link rel="dns-prefetch" href="http://imasdk.googleapis.com">
        <link rel="dns-prefetch" href="https://stats.g.doubleclick.net">
        <link rel="dns-prefetch" href="http://rules.quantcount.com">
        <link rel="dns-prefetch" href="http://tpc.googlesyndication.com">
        <link rel="dns-prefetch" href="https://js-agent.newrelic.com">
        <link rel="dns-prefetch" href="https://pixel.quantserve.com">
        <link rel="dns-prefetch" href="https://secure.quantserve.com">
        <link rel="dns-prefetch" href="https://adservice.google.com">
        <link rel="dns-prefetch" href="https://static.quantcast.mgr.consensu.org">
        <link rel="dns-prefetch" href="https://ad.doubleclick.net">
        <link rel="dns-prefetch" href="https://www.facebook.com">
        <link rel="dns-prefetch" href="https://ap.lijit.com">
        <link rel="dns-prefetch" href="https://www.google.com">
        <link rel="dns-prefetch" href="https://pagead2.googlesyndication.com">
        <link rel="dns-prefetch" href="https://api.mixpanel.com">
        <link rel="dns-prefetch" href="https://ssl.google-analytics.com">
        <link rel="dns-prefetch" href="https://aax.amazon-adsystem.com">
        <link rel="dns-prefetch" href="https://c.amazon-adsystem.com">
        <link rel="dns-prefetch" href="https://cms.quantserve.com">
        <link rel="dns-prefetch" href="https://rules.quantcount.com">
        <link rel="dns-prefetch" href="https://ib.adnxs.com">
        <link rel="dns-prefetch" href="https://px.moatads.com">
        <link rel="dns-prefetch" href="https://ad.turn.com">
        <link rel="dns-prefetch" href="https://cm.g.doubleclick.net">
        <link rel="dns-prefetch" href="https://fastlane.rubiconproject.com">
        <link rel="dns-prefetch" href="https://quantcast.mgr.consensu.org">
        <link rel="dns-prefetch" href="https://sb.scorecardresearch.com">
        <link rel="dns-prefetch" href="https://secure.quantserve.com">
        <link rel="dns-prefetch" href="https://connect.facebook.net">
        <link rel="dns-prefetch" href="https://unequalbrake.com">
        <link rel="dns-prefetch" href="https://www.google-analytics.com">
        <link rel="dns-prefetch" href="https://pixel.rubiconproject.com">
        <link rel="dns-prefetch" href="https://secure-assets.rubiconproject.com">
        <link rel="dns-prefetch" href="https://sync.mookie1.cn">
        <link rel="dns-prefetch" href="https://s0.2mdn.net">
        <link rel="dns-prefetch" href="https://js-sec.indexww.com">
        <link rel="dns-prefetch" href="https://ads.pubmatic.com">
        <link rel="dns-prefetch" href="https://sync.1rx.io">
        <link rel="dns-prefetch" href="https://e1.emxdgt.com">
        <link rel="dns-prefetch" href="https://ads.w55c.net">
    `;
}

function lazyloadStyles(hrefs = []) {
    // const noscripts = hrefs.map((href) => `<noscript><link rel="stylesheet" href="${href}"></noscript>`).join('\n');
    const noscripts = '';

    // loadCss
    return `<script>["${hrefs.join(
        '","'
    )}"].forEach(function(e){"use strict";function t(){for(var s,n=0;n<i.length;n++)i[n].href&&i[n].href.indexOf(e)>-1&&(s=!0);s?r.media="all":setTimeout(t)}var s=window.document,r=s.createElement("link"),n=s.getElementsByTagName("script")[0],i=s.styleSheets;r.rel="stylesheet",r.href=e,r.media="only x",n.parentNode.insertBefore(r,n),t()});</script>
        ${noscripts}
    `;
}

function renderInlineStyles(href) {
    const shouldInline = process.env.NODE_ENV !== 'development';

    // Render critical css in dev mode as href so we can just refresh the
    // page for style changes
    if (shouldInline && inlineCssMap[href]) {
        return `<style>${inlineCssMap[href]}</style>`;
    }

    return `<link rel="stylesheet" href="${href}" type="text/css" />`;
}

function transformModuleJS(flushed) {
    return flushed
        .map((assets) => {
            return assets.js.toString();
        })
        .join('\n');
}

function transformModuleCSS(flushed) {
    return flushed
        .reduce((acc, assets) => {
            const styles = assets.styles.toString();

            if (!acc.includes(styles)) {
                acc.push(styles);
            }

            return acc;
        }, [])
        .join('\n');
}

function renderCSSHash(flushed, cspNonce) {
    const css = flushed.reduce((obj, assets) => {
        Object.keys(assets.cssHashRaw).forEach((key) => {
            obj[key] = assets.cssHashRaw[key];
        });

        return obj;
    }, {});

    return addNonceToScript(
        `<script type='text/javascript'>window.__CSS_CHUNKS__= ${JSON.stringify(
            css
        )};</script>`,
        cspNonce
    );
}

function renderEmbedView({
    innerHTML,
    req,
    initialState,
    helmet,
    isCrawler,
    ua,
    flushedAssets,
    cspNonce
}) {
    const reqPath = req.originalUrl;
    const reqReferer = req.get('Referer');
    const referer = reqReferer ? `'${reqReferer}'` : null;
    const HTML = `
    <!DOCTYPE html>
    <html ${helmet ? helmet.htmlAttributes.toString() : ''} lang="en">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            ${addNonceToScript(getNewRelic(), cspNonce)}
            ${favicons}
            ${getResourceHints()}
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet" type="text/css">
            <link href="/static/css/audiomack-embed.css" rel="stylesheet">
            ${helmet ? helmet.title.toString() : ''}
            ${helmet ? helmet.meta.toString() : ''}
            ${helmet ? helmet.link.toString() : ''}
            ${helmet ? helmet.script.toString() : ''}
            ${transformModuleJS(flushedAssets)}
            ${transformModuleCSS(flushedAssets)}
        </head>
        <body class="preloading">
            <div class="react-view" id="react-view">${innerHTML}</div>
            ${
                process.env.NODE_ENV === 'development'
                    ? renderCSSHash(flushedAssets, cspNonce)
                    : ''
            }
            <script nonce="${cspNonce}">
            ${isCrawler ? 'window.__IS_CRAWLER__ = true;' : ''}
            ${
                process.env.NODE_ENV === 'production'
                    ? '(window.__REACT_DEVTOOLS_GLOBAL_HOOK__ || {}).inject = function () {};'
                    : ''
            }
            window.__UA__ = ${serializeJS(
                {
                    browser: ua.browser,
                    version: parseInt(ua.version, 10),
                    os: ua.os,
                    platform: ua.platform
                },
                { isJSON: true }
            )}
            window.__INITIAL_STATE__ = ${serializeJS(initialState, {
                isJSON: true
            })};
            window.__REFERER__ = ${referer};
            </script>
            ${addNonceToScript(
                getGa(process.env.GA_PROPERTY_ID_EMBED),
                cspNonce
            )}
            ${addNonceToScript(getQuantcast(), cspNonce)}
            ${addNonceToScript(getComscore(reqPath), cspNonce)}
            ${addNonceToScript(getLiveRamp(initialState.currentUser), cspNonce)}
        </body>
    </html>
    `;

    return HTML;
}

function renderMobileView({
    innerHTML,
    initialState,
    helmet,
    isCrawler,
    ua,
    flushedAssets,
    cspNonce,
    req
}) {
    const reqPath = req.originalUrl;
    const styles = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap',
        '/static/css/audiomack-mobile.css'
    ];
    const HTML = `
    <!DOCTYPE html>
    <html ${helmet ? helmet.htmlAttributes.toString() : ''} lang="en">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            ${addNonceToScript(getNewRelic(), cspNonce)}
            ${favicons}
            ${getResourceHints()}
            ${renderInlineStyles(inlineMobileCss)}
            ${helmet ? helmet.title.toString() : ''}
            ${helmet ? helmet.meta.toString() : ''}
            ${helmet ? helmet.link.toString() : ''}
            ${helmet ? helmet.script.toString() : ''}
            ${transformModuleJS(flushedAssets)}
            ${transformModuleCSS(flushedAssets)}
        </head>
        <body>
            <div class="react-view" id="react-view">${innerHTML}</div>
            ${
                process.env.NODE_ENV === 'development'
                    ? renderCSSHash(flushedAssets, cspNonce)
                    : ''
            }
            ${addNonceToScript(lazyloadStyles(styles), cspNonce)}
            <script nonce="${cspNonce}">
            ${isCrawler ? 'window.__IS_CRAWLER__ = true;' : ''}
            ${
                process.env.NODE_ENV === 'production'
                    ? '(window.__REACT_DEVTOOLS_GLOBAL_HOOK__ || {}).inject = function () {};'
                    : ''
            }
            window.__UA__ = ${serializeJS(
                {
                    browser: ua.browser,
                    version: parseInt(ua.version, 10),
                    os: ua.os,
                    platform: ua.platform
                },
                { isJSON: true }
            )}
            window.__INITIAL_STATE__ = ${serializeJS(initialState, {
                isJSON: true
            })};
            </script>
            ${addNonceToScript(
                getGa([
                    process.env.GA_PROPERTY_ID,
                    process.env.GA_MEASUREMENT_ID
                ]),
                cspNonce
            )}
            ${addNonceToScript(getAdmiralScript(), cspNonce)}
            ${addNonceToScript(getFacebookPixel(), cspNonce)}
            ${addNonceToScript(getQuantcast(), cspNonce)}
            ${addNonceToScript(getComscore(reqPath), cspNonce)}
            ${addNonceToScript(getGdprTag(), cspNonce)}
            ${addNonceToScript(getFirebaseScript(), cspNonce)}
            ${addNonceToScript(getLiveRamp(initialState.currentUser), cspNonce)}
        </body>
    </html>
    `;

    return HTML;
}

function renderDesktopView({
    innerHTML,
    initialState,
    helmet,
    ua,
    isMobileDevice,
    isCrawler,
    flushedAssets,
    cspNonce,
    req,
    isLoggedIn
}) {
    const reqPath = req.originalUrl;
    const HTML = `
    <!DOCTYPE html>
    <html ${helmet ? helmet.htmlAttributes.toString() : ''} lang="en">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            ${addNonceToScript(getNewRelic(), cspNonce)}
            ${favicons}
            ${getResourceHints()}
            ${renderInlineStyles(inlineDesktopCss)}
            <link ${
                !isCrawler ? 'rel="preload" as="style"' : 'rel="stylesheet"'
            } href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Montserrat:700&display=swap" />
            <link ${
                !isCrawler ? 'rel="preload" as="style"' : 'rel="stylesheet"'
            } href="/static/css/audiomack-desktop.css" />
            ${helmet ? helmet.title.toString() : ''}
            ${helmet ? helmet.meta.toString() : ''}
            ${helmet ? helmet.link.toString() : ''}
            ${helmet ? helmet.script.toString() : ''}
            ${transformModuleJS(flushedAssets)}
            ${transformModuleCSS(flushedAssets)}
        </head>
        <body class="no-js${isMobileDevice ? ' is-mobile' : ''}${
        isCrawler ? ' is-crawler' : ' preloading css-loading'
    }${
        req.path === '/' && !isLoggedIn
            ? ' has-hidden-sidebar no-header-border'
            : ''
    }">
            <script>document.body.classList.remove('no-js');</script>
            <div class="react-view" id="react-view">${innerHTML}</div>
            ${
                process.env.NODE_ENV === 'development'
                    ? renderCSSHash(flushedAssets, cspNonce)
                    : ''
            }
            <script nonce="${cspNonce}">
            ${isCrawler ? 'window.__IS_CRAWLER__ = true;' : ''}
            ${
                process.env.NODE_ENV === 'production'
                    ? '(window.__REACT_DEVTOOLS_GLOBAL_HOOK__ || {}).inject = function () {};'
                    : ''
            }
            window.__UA__ = ${serializeJS(
                {
                    browser: ua.browser,
                    version: parseInt(ua.version, 10),
                    os: ua.os,
                    platform: ua.platform
                },
                { isJSON: true }
            )}
            window.__INITIAL_STATE__ = ${serializeJS(initialState, {
                isJSON: true
            })};
            </script>
            ${addNonceToScript(
                getGa([
                    process.env.GA_PROPERTY_ID,
                    process.env.GA_MEASUREMENT_ID
                ]),
                cspNonce
            )}
            ${addNonceToScript(getAdmiralScript(), cspNonce)}
            ${addNonceToScript(getFacebookPixel(), cspNonce)}
            ${addNonceToScript(getQuantcast(), cspNonce)}
            ${addNonceToScript(getComscore(reqPath), cspNonce)}
            ${addNonceToScript(getGdprTag(), cspNonce)}
            ${addNonceToScript(getMixpanel(), cspNonce)}
            ${addNonceToScript(getFirebaseScript(), cspNonce)}
            ${addNonceToScript(getLiveRamp(initialState.currentUser), cspNonce)}
        </body>
    </html>
    `;

    return HTML;
}

function sendCacheHeaders(res, url, isLoggedIn) {
    let maxAge;

    // don't cache url's with query parameters
    if (url.includes('?') || isLoggedIn) {
        return;
    }

    if (
        url === '/' ||
        url.match(/\/(trending|songs|albums|playlist|recent)/i)
    ) {
        maxAge = 5 * 60; // 5 mins
    } else if (url.match(/^\/(song|album|playlist|artist)\//i)) {
        maxAge = 15 * 60; // 15 mins
    }

    if (maxAge) {
        res.setHeader('Cache-Control', `public, max-age=${maxAge}`);
    }
}

function handleMobileDesktopCookie(req, res) {
    if (req.query.showDesktop) {
        if (
            req.query.showDesktop === 'true' ||
            req.query.showDesktop === 'false'
        ) {
            res.cookie(
                'showDesktop',
                req.query.showDesktop === 'true' ? 1 : 0,
                {
                    path: '/',
                    expires: new Date(Date.now() + 900000),
                    sameSite: 'Strict'
                }
            );
        } else {
            res.clearCookie('showDesktop');
        }
    }
}

async function renderComponentHTML({
    req,
    res,
    store,
    routes,
    routeConfig,
    shouldFetchData = false
}) {
    let fetchedValues = [];
    const context = {};
    const routerProps = {
        context: context,
        location: req.url
    };

    if (shouldFetchData) {
        res.locals.timings.start('API: fetching component data');
        fetchedValues = await fetchComponentsData({ req, routeConfig, store });
        res.locals.timings.end('API: fetching component data');
    }

    res.locals.timings.start('Rendering root');

    const rootComponent = (
        <Root
            routes={routes}
            store={store}
            Router={StaticRouter}
            routerProps={routerProps}
        />
    );
    const innerHTML = renderToString(rootComponent);
    const helmet = Helmet.renderStatic();

    res.locals.timings.end('Rendering root');

    return {
        innerHTML,
        helmet,
        context,
        fetchedValues
    };
}

function isBot(ua) {
    if (ua.isBot) {
        return true;
    }

    if (ua.source.match(/branch-passthrough|branch metrics api/i)) {
        return true;
    }

    return false;
}

async function getHTML({
    req,
    res,
    layoutVars,
    store,
    routes,
    routeConfig,
    isLoggedIn
}) {
    const ua = req.useragent;
    const isCrawler = isBot(ua);
    const { isMobile, isEmbed, isMobileDevice } = layoutVars;
    const cspNonce = res.locals.nonce;
    const renderProps = await renderComponentHTML({
        req,
        res,
        store,
        routes,
        routeConfig,
        shouldFetchData: isCrawler
    });
    const redirect = renderProps.context.url;
    const helmet = renderProps.helmet;
    const innerHTML = renderProps.innerHTML;
    const fetchedValues = renderProps.fetchedValues;
    const reqPath = req.originalUrl;
    let status = renderProps.context.status;

    if (!status) {
        res.locals.timings.start('Getting status');
        status = await getStatus(reqPath, fetchedValues);
        res.locals.timings.end('Getting status');
    }

    const webpackStats = res.locals.webpackStats.filter((obj) => {
        // Return stats for either mobile or desktop
        const isMobileStats = obj.name.includes('mobile');
        const isChromecastStats = obj.name.includes('chromecast');
        const isServerStats = obj.name.includes('server');

        if (isMobile) {
            return isMobileStats;
        }

        return !isMobileStats && !isChromecastStats && !isServerStats;
    });

    const chunkNames = flushChunkNames();
    const flushedAssets = webpackStats.map((stats) => {
        const mobileStats = ['mobile'];
        const desktopStats = ['desktop'];
        const flushed = flushChunks(stats, {
            chunkNames,
            before: ['manifest', 'common', 'shared'],
            after: isMobile ? mobileStats : desktopStats
        });

        return flushed;
    });

    // console.log('flushedAssets.styles.toString');
    // console.log(flushedAssets.styles.toString());

    // const {
    //     js,
    //     styles,
    //     cssHash,
    //     scripts,
    //     stylesheets
    // } = flushedAssets;

    // console.log('PATH', req.path);
    // console.log('JS', js);
    // console.log('STYLES', styles);
    // console.log('CSSHASH', cssHash);
    // console.log('DYNAMIC CHUNK NAMES RENDERED', chunkNames);
    // console.log('SCRIPTS SERVED', scripts);
    // console.log('STYLESHEETS SERVED', stylesheets);

    const initialState = store.getState();
    let html;

    if (isEmbed) {
        res.removeHeader('X-Frame-Options');
        html = renderEmbedView({
            innerHTML,
            req,
            initialState,
            ua,
            helmet,
            isCrawler,
            flushedAssets,
            cspNonce
        });
    } else if (isMobile) {
        html = renderMobileView({
            innerHTML,
            initialState,
            ua,
            helmet,
            isCrawler,
            flushedAssets,
            cspNonce,
            req
        });
    } else {
        html = renderDesktopView({
            innerHTML,
            initialState,
            ua,
            helmet,
            isMobileDevice,
            isCrawler,
            flushedAssets,
            cspNonce,
            req,
            isLoggedIn
        });
    }

    return {
        html,
        status,
        redirect
    };
}

async function getUserStateTree(res, token, secret, isMobile) {
    const initialState = {};
    let user = null;

    if (token && secret) {
        res.locals.timings.start('API: fetching user');
        user = await api.user.get(token, secret).catch((err) => {
            if (process.env.NODE_ENV === 'development') {
                // When using dev environment this fails because of a cert
                // error so you might not be able to load a page that
                // requires a user unless you load the home page and navigate
                // there via some Link
                console.log(err);
            }
            // Return null for any user retreival errors
            return null;
        });
        res.locals.timings.end('API: fetching user');
    }

    let reducer;

    if (isMobile) {
        reducer = mobileUserReducer;
    } else {
        reducer = desktopUserReducer;
    }

    if (user) {
        // Emulate "LOG_IN" action
        const logInAction = {
            type: LOG_IN,
            resolved: {
                user,
                token: token,
                secret: secret
            }
        };

        const userStateTree = reducer(undefined, logInAction);

        initialState.currentUser = userStateTree;
    }

    return initialState;
}

export default async function handler(req, res, next) {
    try {
        // Redirect old m. urls
        if (req.get('host').indexOf('m.audiomack.com') === 0) {
            const newUrl = `${process.env.AM_URL}${req.originalUrl}`;

            res.redirect(301, newUrl);
            return;
        }

        // Redirect old www. urls to non www
        if (req.get('host').indexOf('www.audiomack.com') === 0) {
            const newUrl = `https://audiomack.com${req.originalUrl}`;

            res.redirect(301, newUrl);
            return;
        }

        // Redirect old devcf.aws. urls to non dcf.aws
        if (req.get('host').indexOf('devcf.aws.audiomack.com') === 0) {
            const newUrl = `https://dcf.aws.audiomack.com${req.originalUrl}`;

            res.redirect(301, newUrl);
            return;
        }

        handleMobileDesktopCookie(req, res);

        const layoutVars = getLayoutVars(req);
        const { isMobile } = layoutVars;
        const token = req.cookies.otoken;
        const secret = req.cookies.osecret;

        const initialState = await getUserStateTree(
            res,
            token,
            secret,
            isMobile
        );

        let store;
        let routes;
        let routeConfig;

        if (isMobile) {
            routes = mobileRoutes;
            routeConfig = mobileRouteConfig;
            store = configureStore(initialState, mobileReducer);
        } else {
            routes = desktopRoutes;
            routeConfig = desktopRouteConfig;
            store = configureStore(initialState, desktopReducer);
        }

        const isLoggedIn = store.getState().currentUser.isLoggedIn;

        sendCacheHeaders(res, req.originalUrl, isLoggedIn);

        const { html, status, redirect } = await getHTML({
            req,
            res,
            layoutVars,
            store,
            routes,
            routeConfig,
            isLoggedIn
        });

        if (redirect) {
            res.redirect(301, redirect);
            return;
        }

        res.set('Content-Type', 'text/html');
        res.set('Vary', 'User-Agent');
        res.status(status).end(html);
    } catch (err) {
        next(err);
        return;
    }
}
