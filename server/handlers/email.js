import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import htmlToText from 'html-to-text';

import emailMap from '../../workers/email/emails';
import trendingMusic from '../../workers/email/emails/trendingMusic';
import statBenchmark from '../../workers/email/emails/statBenchmark';

function template(title, html, text = '') {
    let textVersion = '';

    if (text) {
        textVersion = <pre style="width: 580px;margin: 50px auto;">{text}</pre>;
    }

    return `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>${title}</title>
            </head>
            <body style="margin: 0; padding: 0; background:#f4f4f4">
                ${html}
                ${textVersion}
            </body>
        </html>
    `;
}

export default async function handler(req, res) {
    res.set('Content-Type', 'text/html');

    try {
        const emailTemplate = req.params.name;
        const { component: Email, extraProps } = emailMap[emailTemplate];
        const props = {
            ...req.query,
            ...(extraProps || {})
        };

        if (!Email) {
            throw new Error(`Invalid email type: ${emailTemplate}`);
        }

        const title = Email.subject(props);
        let markup;

        // Test with something like:
        // http://localhost:3000/email/trending-music?musicArtist=Lil%20Pump&uploaderSlug=lil-pump&genreUrl=/rap/trending-now&genreName=Rap/Hip-Hop&name=Lil%20Pump&musicUrl=/album/lil-pump/harverd-dropout&musicTitle=Harverd%20Dropout&musicBaseImage=https://assets.audiomack.com/lil-pump/f4c5d5680b9767d071733c3f1c21ec512c3499803a84287d158916596af63996.jpeg&musicImage=https://assets.audiomack.com/lil-pump/f4c5d5680b9767d071733c3f1c21ec512c3499803a84287d158916596af63996.jpeg
        const dontSendEmail = true;

        if (emailTemplate === 'trending-music') {
            markup = await trendingMusic(props, dontSendEmail);
        }

        // Test with something like:
        // http://localhost:3000/email/stat-benchmark?name=Nicki%20Minaj&uploaderSlug=nicki-minaj&number=1%20Million&stat=streams&musicBaseImage=https://assets.audiomack.com/nicki-minaj/86ae1aa178417016e079ff6ca17a0a7598fafa7ceca6dba1ba87eb73fd0fe1b7.jpeg&musicImage=https://assets.audiomack.com/nicki-minaj/86ae1aa178417016e079ff6ca17a0a7598fafa7ceca6dba1ba87eb73fd0fe1b7.jpeg&featuring=DaBaby&artist=Nicki%20Minaj&title=Suge%20(Remix)
        else if (emailTemplate === 'stat-benchmark') {
            markup = await statBenchmark(props, dontSendEmail);
        } else {
            markup = renderToStaticMarkup(<Email {...props} />);
        }

        const text = htmlToText.fromString(markup);
        const html = template(title, markup, req.query.showText ? text : '');

        res.send(html);
    } catch (err) {
        res.status(500).end(`<h1>${err.message}</h1>`);
        return;
    }
}
