/**
 * @jest-environment node
 */

/* global describe, test, expect, beforeAll */

// Note make sure you're using the dev API endpoint when running these tests.
// You don't necessarily have to rerun your dev server with the new environment
// You'll just need to make sure your .env files are set before running these tests

import request from 'supertest';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import useragent from 'express-useragent';
import timings from 'server-timings';
import cheerio from 'cheerio';

import mainHandler from '../main';
// If you ever need to generate a new json output, run:
//
// node scripts/deploy/webpackBuild/createTestWebpackStats.js
import stats from '../../../test/helpers/stats-desktop.json';

const timeout = 15000;

function createServer() {
    const app = express();

    app.use(timings);
    app.use(bodyParser.json({ limit: '5mb' }));
    app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));
    app.use(cookieParser());
    app.use(useragent.express());
    app.use((req, res, next) => {
        res.locals.webpackStats = stats;

        next();
    });
    app.get('*', mainHandler);
    app.use((err, req, res, next) => {
        console.log(err);
        res.status(500).send(err.toString());
        next();
    });

    return app;
}

describe('Server side crawler rendering', () => {
    const ua =
        'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';
    const songUrl = '/song/tester/drop-out';
    const albumUrl = '/album/tester/audiomack-ep';
    const playlistUrl = '/playlist/tester/test-playlist';
    let app;

    beforeAll(() => {
        app = createServer();
    });

    test(
        'Render full logged out home page',
        async function() {
            const response = await request(app)
                .get('/')
                .set('User-Agent', ua);

            expect(response.statusCode).toBe(200);

            const $ = cheerio.load(response.text);

            expect($('body').hasClass('is-crawler')).toBe(true);
            expect($('body').find('.music-card').length).toBeGreaterThan(0);
        },
        timeout
    );

    // These few tests will fail without a local redis running
    test(
        'Render song description meta tags',
        async function() {
            const response = await request(app)
                .get(songUrl)
                .set('User-Agent', ua);

            const $ = cheerio.load(response.text);

            expect($('meta[name="description"]').attr('content')).toBeTruthy();
        },
        timeout
    );

    test(
        'Render song twitter properties',
        async function() {
            const response = await request(app)
                .get(songUrl)
                .set('User-Agent', ua);

            const $ = cheerio.load(response.text);

            expect(
                $('meta[name="twitter:description"]').attr('content')
            ).toBeTruthy();
            expect($('meta[name="twitter:card"]').attr('content')).toBe(
                'player'
            );
            expect($('meta[name="twitter:domain"]').attr('content')).toBe(
                process.env.AM_URL
            );
            expect($('meta[name="twitter:site"]').attr('content')).toBe(
                '@audiomack'
            );
            expect($('meta[name="twitter:title"]').attr('content')).toBe(
                'Lil Pump - Drop Out'
            );
            expect(
                $('meta[name="twitter:image"]')
                    .attr('content')
                    .startsWith('https://assets.dev.audiomack.com/tester')
            ).toBe(true);
            expect($('meta[name="twitter:player"]').attr('content')).toBe(
                `${process.env.AM_URL}/embed${songUrl}?twitterEmbed=true`
            );
            expect($('meta[name="twitter:player:width"]').attr('content')).toBe(
                '400'
            );
            expect(
                $('meta[name="twitter:player:height"]').attr('content')
            ).toBe('200');
            expect($('meta[name="twitter:creator"]').attr('content')).toBe(
                '@okcoker'
            );
        },
        timeout
    );

    test(
        'Render song facebook properties',
        async function() {
            const response = await request(app)
                .get(songUrl)
                .set('User-Agent', ua);

            const $ = cheerio.load(response.text);

            expect(
                $('meta[property="og:description"]').attr('content')
            ).toBeTruthy();
            expect(
                $('meta[property="og:image"]')
                    .attr('content')
                    .startsWith('https://assets.dev.audiomack.com/tester')
            ).toBe(true);
            expect($('meta[property="og:title"]').attr('content')).toBe(
                'Lil Pump - Drop Out'
            );
            expect($('meta[property="og:url"]').attr('content')).toBe(
                `${process.env.AM_URL}${songUrl}`
            );
            expect($('meta[property="og:type"]').attr('content')).toBe(
                'music.song'
            );
            expect($('meta[property="fb:app_id"]').attr('content')).toBe(
                process.env.FACEBOOK_APP_ID
            );
        },
        timeout
    );

    test(
        'Render album metatags',
        async function() {
            const response = await request(app)
                .get(albumUrl)
                .set('User-Agent', ua);

            const $ = cheerio.load(response.text);
            const title = 'Audiomack EP';
            const artist = 'Various Artists';
            const featuring = 'Magdalena, Riff Raff, Levi Carter';

            expect($('title').text()).toContain(title);
            expect($('title').text()).toContain(artist);
            expect($('meta[name="description"]').attr('content')).toContain(
                artist
            );
            expect($('meta[name="description"]').attr('content')).toContain(
                featuring
            );
            expect($('meta[name="twitter:card"]').attr('content')).toBe(
                'player'
            );
            expect($('meta[name="twitter:domain"]').attr('content')).toBe(
                process.env.AM_URL
            );
            expect($('meta[name="twitter:site"]').attr('content')).toBe(
                '@audiomack'
            );
            expect($('meta[name="twitter:title"]').attr('content')).toContain(
                title
            );
            expect($('meta[name="twitter:title"]').attr('content')).toContain(
                artist
            );
            expect(
                $('meta[name="twitter:description"]').attr('content')
            ).toContain(artist);
            expect(
                $('meta[name="twitter:description"]').attr('content')
            ).toContain(featuring);
            expect(
                $('meta[name="twitter:image"]')
                    .attr('content')
                    .startsWith('https://assets.dev.audiomack.com')
            ).toBe(true);
            expect($('meta[name="twitter:player"]').attr('content')).toBe(
                `${process.env.AM_URL}/embed${albumUrl}?twitterEmbed=true`
            );
            expect($('meta[name="twitter:player:width"]').attr('content')).toBe(
                '400'
            );
            expect(
                $('meta[name="twitter:player:height"]').attr('content')
            ).toBe('300');
            expect($('meta[name="twitter:creator"]').attr('content')).toBe(
                '@okcoker'
            );

            expect(
                $('meta[property="og:image"]')
                    .attr('content')
                    .startsWith('https://assets.dev.audiomack.com')
            ).toBe(true);
            expect($('meta[property="og:title"]').attr('content')).toContain(
                title
            );
            expect($('meta[property="og:title"]').attr('content')).toContain(
                artist
            );
            expect(
                $('meta[property="og:description"]').attr('content')
            ).toContain(artist);
            expect(
                $('meta[property="og:description"]').attr('content')
            ).toContain(featuring);
            expect($('meta[property="og:url"]').attr('content')).toBe(
                `${process.env.AM_URL}${albumUrl}`
            );
            expect($('meta[property="og:type"]').attr('content')).toBe(
                'music.album'
            );
            expect($('meta[property="fb:app_id"]').attr('content')).toBe(
                process.env.FACEBOOK_APP_ID
            );
        },
        timeout
    );

    test(
        'Render playlist metatags',
        async function() {
            const response = await request(app)
                .get(playlistUrl)
                .set('User-Agent', ua);
            const artist = 'Automated Test Account';

            const $ = cheerio.load(response.text);

            expect($('title').text()).toContain(artist);

            expect($('meta[name="description"]').attr('content')).toContain(
                artist
            );
            expect($('meta[name="twitter:card"]').attr('content')).toBe(
                'player'
            );
            expect($('meta[name="twitter:domain"]').attr('content')).toBe(
                process.env.AM_URL
            );
            expect($('meta[name="twitter:site"]').attr('content')).toBe(
                '@audiomack'
            );
            expect($('meta[name="twitter:title"]').attr('content')).toBe(
                'Automated Test Account - Test Playlist'
            );
            expect(
                $('meta[name="twitter:description"]').attr('content')
            ).toContain(artist);
            expect(
                $('meta[name="twitter:image"]')
                    .attr('content')
                    .startsWith(
                        'https://assets.dev.audiomack.com/test-playlist'
                    )
            ).toBe(true);
            expect($('meta[name="twitter:player"]').attr('content')).toBe(
                `${
                    process.env.AM_URL
                }/embed/playlist/tester/test-playlist?twitterEmbed=true`
            );
            expect($('meta[name="twitter:player:width"]').attr('content')).toBe(
                '400'
            );
            expect(
                $('meta[name="twitter:player:height"]').attr('content')
            ).toBe('250');

            expect(
                $('meta[property="og:image"]')
                    .attr('content')
                    .startsWith(
                        'https://assets.dev.audiomack.com/test-playlist'
                    )
            ).toBe(true);
            expect($('meta[property="og:title"]').attr('content')).toContain(
                artist
            );
            expect(
                $('meta[property="og:description"]').attr('content')
            ).toContain(artist);
            expect($('meta[property="og:url"]').attr('content')).toBe(
                `${process.env.AM_URL}/playlist/tester/test-playlist`
            );
            expect($('meta[property="og:type"]').attr('content')).toBe(
                'music.playlist'
            );
            expect($('meta[property="fb:app_id"]').attr('content')).toBe(
                process.env.FACEBOOK_APP_ID
            );
        },
        timeout
    );

    test(
        'Render 404 page from a not found song correctly',
        async function() {
            const response = await request(app)
                .get('/song/tester/take-it-wrong-2-asdf-asdfasdf')
                .set('User-Agent', ua);

            const $ = cheerio.load(response.text);
            const pageText = $('.l-container').text();

            expect(response.statusCode).toBe(404);
            expect(pageText).toEqual(expect.stringMatching(/^404/));
        },
        timeout
    );

    test(
        'Render 404 page correctly',
        async function() {
            const response = await request(app)
                .get('/adsf-asdgas-gsdgasdgasdg-asdg')
                .set('User-Agent', ua);

            const $ = cheerio.load(response.text);
            const pageText = $('.l-container').text();

            expect(response.statusCode).toBe(404);
            expect(pageText).toEqual(expect.stringMatching(/^404/));
        },
        timeout
    );
});
