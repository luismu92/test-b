/* global test, expect, beforeAll */

import request from 'supertest';
import express from 'express';

import { buildQueryString } from 'utils/index';

import oembedHandler from '../oembed';

function createServer() {
    const app = express();

    app.get('/oembed', oembedHandler);
    app.use((err, req, res, next) => {
        res.status(500).send(err.toString());
        next();
    });

    return app;
}

let app;

beforeAll(() => {
    app = createServer();
});

test('Should return an embed with 252 height on songs', async function() {
    const query = buildQueryString({
        url: `${process.env.AM_URL}/song/tester/drop-out`
    });
    const response = await request(app).get(`/oembed?${query}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.height).toBe(252);
});

test('Should return an embed with 352 height on albums', async function() {
    const query = buildQueryString({
        url: `${process.env.AM_URL}/album/tester/audiomack-ep`
    });
    const response = await request(app).get(`/oembed?${query}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.height).toBe(352);
});

test('Should return an embed with 352 height on playlists', async function() {
    const query = buildQueryString({
        url: `${process.env.AM_URL}/playlist/tester/test-playlist`
    });
    const response = await request(app).get(`/oembed?${query}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.height).toBe(352);
});
