import fs from 'fs';
import path from 'path';

export default function handler(req, res, next) {
    const robotsFilepath = path.join(__dirname, '../../public/static/ads.txt');

    fs.readFile(robotsFilepath, function(err, data) {
        if (err) {
            res.status(404).end('Not found');
            next(err);
            return;
        }

        res.contentType('text/plain');
        res.end(data);
    });
}
