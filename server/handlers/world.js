import { request } from 'utils/index';

const paragraphTypeToElement = {
    3: 'h1',
    1: 'p',
    4: 'img',
    6: 'blockquote'
};

const markupTypeToElement = {
    1: 'strong',
    2: 'em',
    3: 'a'
};

function validateOrigin(req, res) {
    const origin = req.get('origin');
    const validCorsOrigins = [
        'http://localhost:3002',
        'http://audiomack.world.s3-website-us-east-1.amazonaws.com',
        'https://audiomack.world.s3-website-us-east-1.amazonaws.com',
        'https://audiomack.world',
        'https://www.audiomack.world'
    ];

    if (validCorsOrigins.includes(origin)) {
        res.set('Access-Control-Allow-Origin', origin);
        res.set('Access-Control-Allow-Credentials', 'true');
        res.set('Vary', 'Origin');
        return true;
    }

    return false;
}

function getImageSrcFromId(id) {
    return `https://cdn-images-1.medium.com/max/1024/${id}`;
}

function wrapTextWithTag(text, markupObj, start, end) {
    const tag = markupTypeToElement[markupObj.type];

    if (!tag || end < start) {
        return {
            text
        };
    }

    let openTag = `<${tag}>`;

    if (tag === 'a') {
        openTag = `<a href="${markupObj.href}" title="${
            markupObj.title
        }" rel="${markupObj.rel}">`;
    }

    return {
        openTag,
        text: `${text.slice(0, start)}${openTag}${text.slice(
            start,
            end
        )}</${tag}>${text.slice(end)}`
    };
}

async function getHTMLFromParagraph({
    markups = [],
    text,
    type,
    iframe,
    metadata
}) {
    if (iframe) {
        const data = await request(
            `${process.env.BLOG_URL}/media/${iframe.mediaResourceId}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }
            }
        );
        const parsed = JSON.parse(data.substr(data.indexOf('{'), data.length));
        const {
            iframeSrc,
            iframeHeight,
            iframeWidth,
            title
        } = parsed.payload.value;

        let embedAttr = ' class="u-iframe-embed"';

        const isYoutube = decodeURIComponent(iframeSrc).match(
            /https?:\/\/(www.)?youtube.com/
        );
        const isVimeo = decodeURIComponent(iframeSrc).match(
            /https?:\/\/(www.)?vimeo.com/
        );

        if (isYoutube || isVimeo) {
            embedAttr = ' class="u-video-embed"';
        }

        return `<div${embedAttr}><iframe frameborder="0" scrolling="no" title="${title}" width="${iframeWidth}" height="${iframeHeight}" src="${iframeSrc}"></iframe></div>`;
    }

    if (paragraphTypeToElement[type] === 'img') {
        const landscape =
            metadata.originalWidth > metadata.originalHeight
                ? 'landscape'
                : null;
        const portrait =
            metadata.originalHeight > metadata.originalWidth
                ? 'portrait'
                : null;
        let attribute = '';

        if (landscape || portrait) {
            attribute = `data-layout="${landscape || portrait}"`;
        }

        return `<div class="image-wrap" style="background-image: url(${getImageSrcFromId(
            metadata.id
        )})"${attribute}><img src="${getImageSrcFromId(metadata.id)}" /></div>`;
    }

    const wrapTag = paragraphTypeToElement[type] || 'p';
    const newMarkups = Array.from(markups).sort((x, y) => {
        return x.start - y.start;
    });
    const innerText = newMarkups.reduce((str, obj, i) => {
        const markupTag = markupTypeToElement[obj.type];
        const { start, end } = obj;
        const nextIndex = i + 1;
        const { openTag, text: wrappedText } = wrapTextWithTag(
            str,
            obj,
            start,
            end
        );

        if (markupTag && openTag) {
            // Adjust every subsequent markup to account for our newly wrapped text
            for (let k = nextIndex, len = newMarkups.length; k < len; k++) {
                const nextObj = newMarkups[k];

                if (!nextObj) {
                    continue;
                }

                const { start: nextStart, end: nextEnd } = nextObj;

                if (start < nextStart) {
                    nextObj.start = nextStart + openTag.length;
                }

                if (start < nextEnd) {
                    nextObj.end = nextEnd + openTag.length;
                }

                if (end < nextEnd) {
                    nextObj.end = nextEnd + `</${markupTag}>`.length;
                }
            }
        }

        return wrappedText;
    }, text);

    return `<${wrapTag}>${innerText}</${wrapTag}>`;
}

async function getContentFromBodyModel(item) {
    const model = item.content.bodyModel;
    let paragraphs = model.paragraphs;

    // Skip title paragraph as we already have that
    if (paragraphs[0] && paragraphTypeToElement[paragraphs[0].type] === 'h1') {
        paragraphs = paragraphs.slice(1);
    }

    const values = await Promise.all(paragraphs.map(getHTMLFromParagraph));

    let imageContainerOpen = false;

    const newValues = values.reduce((arr, element, i) => {
        const imageTag = '<div class="image-wrap"';
        const portraitAttr = 'data-layout="portrait"';
        const landscapeAttr = 'data-layout="landscape"';
        const isImage = element.startsWith(imageTag);

        if (isImage && !imageContainerOpen) {
            imageContainerOpen = true;
            let adjacentImageCount = 1;
            let allPortrait = element.includes(portraitAttr);
            let allLandscape = element.includes(landscapeAttr);
            let layout = '';

            for (let k = i + 1, len = values.length; k < len; k++) {
                const nextElement = values[k];

                if (!nextElement.startsWith(imageTag)) {
                    break;
                }

                allPortrait = allPortrait && nextElement.includes(portraitAttr);
                allLandscape =
                    allLandscape && nextElement.includes(landscapeAttr);
                adjacentImageCount += 1;
            }

            if (allPortrait) {
                layout = 'data-layout="portrait"';
            }

            if (allLandscape) {
                layout = 'data-layout="landscape"';
            }

            arr.push(
                `<div data-image-count="${adjacentImageCount}" ${layout}>`
            );
        } else if (!isImage && imageContainerOpen) {
            imageContainerOpen = false;
            arr.push('</div>');
        }

        arr.push(element);

        return arr;
    }, []);

    return newValues.join('');
}

function getTypeForItem(item, tags) {
    if (item.virtuals.imageCount >= 5) {
        return 'photo';
    }

    if (tags.includes('playlist')) {
        return 'playlist';
    }

    if (tags.includes('videos')) {
        return 'video';
    }

    return 'music';
}

async function getResponseObjectForItem(item) {
    const tags = item.virtuals.tags.map((obj) => obj.slug);
    const ret = {
        id: item.id,
        title: item.title,
        url: `${process.env.BLOG_URL}/${item.uniqueSlug}`,
        created: item.createdAt,
        updated: item.updatedAt,
        image: getImageSrcFromId(item.virtuals.previewImage.imageId),
        tags: tags,
        claps: item.virtuals.totalClapCount || 0,
        type: getTypeForItem(item, tags)
    };

    if (item.content && item.content.bodyModel) {
        ret.content = await getContentFromBodyModel(item);
    }

    return ret;
}
// The lesser known, more flexible way to get post results
// https://github.com/Medium/medium-api-docs/issues/30#issuecomment-200145562
async function getAllPosts(req) {
    const tagRequest = req.query.tag;

    const data = await request(`${process.env.BLOG_URL}/latest?count=1000`, {
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    });
    const parsed = JSON.parse(data.substr(data.indexOf('{'), data.length));
    let items = parsed.payload.posts || [];

    if (tagRequest) {
        items = items.filter((item) => {
            return item.virtuals.tags
                .map((obj) => obj.slug)
                .includes(tagRequest);
        });
    }

    const results = await Promise.all(items.map(getResponseObjectForItem));
    const ret = {
        data: results
    };

    if (req.query.debug) {
        ret.debug = items;
    }

    return ret;
}

async function getSinglePost(req) {
    const id = req.query.id;

    const data = await request(`${process.env.BLOG_URL}/${id}`, {
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    });

    const parsed = JSON.parse(data.substr(data.indexOf('{'), data.length));
    const item = parsed.payload.value || {};
    const results = await getResponseObjectForItem(item);
    const ret = {
        data: results
    };

    if (req.query.debug) {
        ret.debug = item;
    }

    return ret;
}

export default async function handler(req, res, next) {
    try {
        const isValid = validateOrigin(req, res);

        if (!isValid) {
            res.status(403).json({
                error: 'Permission denied.'
            });
            return;
        }

        let results = {};

        switch (req.query.action) {
            case 'posts': {
                results = await getAllPosts(req, res);
                break;
            }

            case 'post':
                results = await getSinglePost(req, res);
                break;

            default:
                throw new Error('No action provided');
        }

        res.json(results);
    } catch (err) {
        next(err);
        return;
    }
}
