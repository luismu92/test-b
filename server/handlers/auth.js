import api from 'api/index';
import { AuthenticationFailedException } from 'utils/errors';

// This handler serves as the fallback if for some reason the API
// calls fail from the client

export default async function handler(req, res, next) {
    try {
        const authType = req.params.type;

        switch (authType) {
            case 'login': {
                const email = req.body.email;
                const password = req.body.password;
                const oauthToken = req.body.oauth_token;

                if (oauthToken) {
                    await api.oauth
                        .login(email, password, oauthToken)
                        .then((action) => {
                            res.redirect(action.resolved);
                            return;
                        })
                        .catch((err) => {
                            throw new AuthenticationFailedException(
                                err.message
                            );
                        });
                    return;
                }

                const response = await api.user
                    .login(email, password)
                    .catch((err) => {
                        throw new AuthenticationFailedException(err.message);
                    });

                const {
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expires
                } = response;
                const maxAgeInSeconds = (expires - Date.now() / 1000) * 1000;

                res.cookie('otoken', token, {
                    maxAge: maxAgeInSeconds,
                    secure:
                        process.env.NODE_ENV !== 'development' &&
                        process.env.AUDIOMACK_DEV !== 'true',
                    sameSite: 'Lax'
                });
                res.cookie('osecret', secret, {
                    maxAge: maxAgeInSeconds,
                    secure:
                        process.env.NODE_ENV !== 'development' &&
                        process.env.AUDIOMACK_DEV !== 'true',
                    sameSite: 'Lax'
                });

                res.redirect('/');
                break;
            }

            case 'join': {
                const username = req.body.username;
                const email = req.body.email;
                const password = req.body.password;
                const password2 = req.body.password2;
                const captcha = req.body['g-recaptcha-response'];

                const response = await api.user
                    .register(email, username, [password, password2], captcha)
                    .catch((err) => {
                        throw new AuthenticationFailedException(err.message);
                    });
                const {
                    oauth_token: token,
                    oauth_token_secret: secret,
                    oauth_token_expiration: expires
                } = response;
                const maxAgeInSeconds = (expires - Date.now() / 1000) * 1000;

                res.cookie('otoken', token, {
                    maxAge: maxAgeInSeconds,
                    secure:
                        process.env.NODE_ENV !== 'development' &&
                        process.env.AUDIOMACK_DEV !== 'true',
                    sameSite: 'Lax'
                });
                res.cookie('osecret', secret, {
                    maxAge: maxAgeInSeconds,
                    secure:
                        process.env.NODE_ENV !== 'development' &&
                        process.env.AUDIOMACK_DEV !== 'true',
                    sameSite: 'Lax'
                });

                res.redirect('/');

                break;
            }

            case 'forgot-password': {
                const email = req.body.email;

                await api.user.forgotpw(email);

                res.status(200).end(
                    `<h1>Password reset instructions have been sent to ${email}</h1>`
                );
                break;
            }

            // Not tested yet
            case 'verify-hash': {
                const hash = req.body.hash;
                const { otoken: token, osecret: secret } = res.cookies || {};

                await api.user.verifyHash(hash, token, secret).catch((err) => {
                    throw new AuthenticationFailedException(err.message);
                });

                res.redirect('/#verifyHashSuccess');
                break;
            }

            // Not tested yet
            case 'recover-account': {
                const { token, password } = req.body;

                await api.user
                    .recoverAccount(token, password, password)
                    .catch((err) => {
                        throw new AuthenticationFailedException(err.message);
                    });

                res.redirect('/login#recoverAccountSuccess');
                break;
            }

            default:
                res.status(404).end('Not found');
                break;
        }
    } catch (err) {
        res.set('Content-Type', 'text/html');

        if (err instanceof AuthenticationFailedException) {
            res.status(401).end(`<h1>${err.message}</h1>`);
        }

        next(err);
        return;
    }
}
