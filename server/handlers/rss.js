import moment from 'moment';
import api from 'api/index';
import {
    getPodcastUrl,
    getMusicUrl,
    convertSecondsToTimecode,
    request,
    yesBool,
    buildDynamicImage
} from 'utils/index';
import { RssException } from 'utils/errors';
import xmlescape from 'xml-escape';

const DATE_RFC2822 = 'ddd, DD MMM YYYY HH:mm:ss ZZ';

function getSummary(text, length = 50) {
    let summary = text || '';

    if (summary.length > length - 1) {
        summary = `${summary.substr(0, length - 1)}…`;
    }

    return xmlescape(summary);
}

function getCategory(category, subcategory) {
    let str = '';

    if (category) {
        str = `<itunes:category text="${xmlescape(category)}"`;

        if (subcategory) {
            str += `>
<itunes:category text="${xmlescape(subcategory)}" />
</itunes:category>`;
        } else {
            str += ' />';
        }
    }

    return str;
}

const imageOptions = {
    min: true,
    height: 1400,
    width: 1400
};

export default async function handler(req, res, next) {
    try {
        const { artistSlug } = req.params;
        const values = await Promise.all([
            api.artist.get(artistSlug).catch((e) => {
                throw new RssException(e.message);
            }),
            api.artist.podcasts(artistSlug).catch((e) => {
                throw new RssException(e.message);
            })
        ]);
        const [artistResponse, podcastResponse] = values;
        const artist = artistResponse.results;
        const podcasts = Array.from(podcastResponse.results).reverse();

        if (!podcasts.length) {
            throw new RssException('This user has no podcasts');
        }

        const title = xmlescape(artist.name);
        const author = xmlescape(artist.name);
        const podcastEmail = 'podcasts@audiomack.com';
        const authorEmail = xmlescape(artist.email || podcastEmail);
        const artistUrl = `${process.env.AM_URL}/artist/${artistSlug}`;
        const podcastCategory = getCategory(artist.podcast_category);

        let globalExplicitValue = false;

        res.writeHead(200, {
            'Content-Type': 'application/rss+xml;charset=utf-8;charset=utf-8'
        });
        res.write('<?xml version="1.0" encoding="utf-8"?>\n');
        res.write(
            '<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">\n'
        );
        res.write('<channel>\n');
        res.write(
            `<atom:link href="${getPodcastUrl(
                artistSlug
            )}" rel="self" type="application/rss+xml" />\n`
        );
        // if (page > 1) {
        //     res.write(`<atom:link href="${getPodcastUrl(artistSlug, page - 1)}" rel="prev" type="application/rss+xml" />\n`);
        // }
        // res.write(`<atom:link href="${getPodcastUrl(artistSlug, page + 1)}" rel="next" type="application/rss+xml" />\n`);

        res.write(`<title>${title}</title>
<link>${artistUrl}</link>
<ttl>60</ttl>
<language>${xmlescape(artist.podcast_language || 'en')}</language>
<copyright>All rights reserved</copyright>
<webMaster>${podcastEmail} (Audiomack Podcasts)</webMaster>
<description>${xmlescape(artist.bio)}</description>
<itunes:subtitle>${getSummary(artist.bio)}</itunes:subtitle>
<itunes:owner>
<itunes:name>${author}</itunes:name>
<itunes:email>${authorEmail}</itunes:email>
</itunes:owner>
<itunes:author>${author}</itunes:author>
<itunes:image href="${xmlescape(
            buildDynamicImage(artist.image_base, imageOptions)
        )}" />
<image>
<url>${artist.image}</url>
<title>${title}</title>
<link>${artistUrl}</link>
</image>\n`);

        if (podcastCategory) {
            res.write(`${podcastCategory}\n`);
        }

        let podcastPublishDate = moment();
        let podcastUpdatedDate = moment('1970-01-30');

        const getRawResponse = true;

        // Have to loop twice since we are streaming the response
        // yet according to the validator.w3 service, item
        // elements need to be the last things within the <channel>
        for (const podcast of podcasts) {
            const pubDate = moment(podcast.released * 1000);

            if (pubDate.isAfter(podcastUpdatedDate)) {
                podcastUpdatedDate = pubDate;
            }

            if (pubDate.isBefore(podcastPublishDate)) {
                podcastPublishDate = pubDate;
            }

            const explicit =
                yesBool(podcast.explicit) ||
                typeof podcast.explicit === 'undefined';

            if (explicit) {
                globalExplicitValue = true;
            }
        }

        res.write(`<pubDate>${podcastPublishDate.format(DATE_RFC2822)}</pubDate>
<lastBuildDate>${podcastUpdatedDate.format(DATE_RFC2822)}</lastBuildDate>
<itunes:explicit>${globalExplicitValue ? 'yes' : 'no'}</itunes:explicit>\n`);

        for (const podcast of podcasts) {
            const contentLength = await request(
                podcast.streaming_url,
                { method: 'HEAD' },
                getRawResponse
            ).then((response) => {
                if (response.ok) {
                    return response.headers.get('content-length');
                }

                return 0;
            });
            const pubDate = moment(podcast.released * 1000);

            const subtitle = getSummary(podcast.description);
            const description = xmlescape(podcast.description);
            const category = getCategory(podcast.category, podcast.subcategory);
            const explicit =
                yesBool(podcast.explicit) ||
                typeof podcast.explicit === 'undefined';

            res.write(`<item>
<guid isPermaLink="false">podcast:${podcast.id}</guid>
<title>${xmlescape(podcast.title)}</title>
<pubDate>${pubDate.format(DATE_RFC2822)}</pubDate>
<link>${getMusicUrl(podcast, { host: process.env.AM_URL })}</link>
<itunes:duration>${convertSecondsToTimecode(podcast.duration)}</itunes:duration>
<itunes:author>${author}</itunes:author>
<itunes:explicit>${explicit ? 'yes' : 'no'}</itunes:explicit>
<itunes:summary>${description}</itunes:summary>
<itunes:subtitle>${subtitle}</itunes:subtitle>
<description>${description}</description>
<enclosure type="audio/mpeg" url="${xmlescape(
                podcast.streaming_url
            )}" length="${contentLength}" />
<itunes:image href="${xmlescape(
                buildDynamicImage(podcast.image_base, imageOptions)
            )}" />
${category ? `${category}\n` : ''}</item>
`);
        }

        res.write('</channel>\n');
        res.write('</rss>');
        res.end();
    } catch (e) {
        res.set('Content-Type', 'text/html');

        if (e instanceof RssException) {
            res.status(404).end(`<h1>${e.message}</h1>`);
        }

        next(e);
        return;
    }
}
