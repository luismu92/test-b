import newrelic from 'newrelic';

export default function handler(req, res) {
    const hasBody = Object.keys(req.body).length;

    if (newrelic && hasBody && req.body) {
        newrelic.recordCustomEvent('csp', req.body);
    }

    res.status(204).end();
}
