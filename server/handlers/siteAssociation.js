import siteAssociation from '../../public/static/apple-app-site-association.json';

export default function handler(req, res) {
    res.json(siteAssociation);
}
