import assetLinks from '../../public/.well-known/assetlinks.json';

export default function handler(req, res) {
    res.json(assetLinks);
}
