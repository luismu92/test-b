import fs from 'fs';
import path from 'path';

export default function handler(req, res, next) {
    const suffix =
        process.env.NODE_ENV === 'production' &&
        process.env.AUDIOMACK_DEV !== 'true'
            ? 'prod'
            : 'dev';
    const robotsFilepath = path.join(
        __dirname,
        `../../public/static/robots.txt.${suffix}`
    );

    fs.readFile(robotsFilepath, function(err, data) {
        if (err) {
            res.status(404).end('Not found');
            next(err);
            return;
        }

        res.contentType('text/plain');
        res.end(data);
    });
}
