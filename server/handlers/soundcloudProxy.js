import { request } from 'utils/index';
import api from 'api/index';

export default async function handler(req, res, next) {
    try {
        const streamingUrl = req.query.streamingUrl;
        const consumerKey = 'e8d4a4460406f85186559062901c8a33';
        const url = `https://api.soundcloud.com/resolve?url=${streamingUrl}&format=json&consumer_key=${consumerKey}`;

        const track = await request(url);

        res.json(track);
    } catch (e) {
        const artistSlug = req.query.artistSlug;
        const songSlug = req.query.songSlug;

        api.music.flagUnplayableSong(artistSlug, songSlug).finally(() => {
            next(e);
        });
    }
}
