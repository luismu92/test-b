import React from 'react';
import { renderToString } from 'react-dom/server';
import { flushChunkNames } from 'react-universal-component/server';
import flushChunks from 'webpack-flush-chunks';

import App from '../../am-chromecast/shared/App';

import { getNewRelic } from '../helpers/index';

export default function handler(req, res) {
    // Uncomment to test default cast-media-player example
    // res.redirect(302, '/static/chromecast/player.html');
    // return;

    let webpackStats = res.locals.webpackStats;

    webpackStats = webpackStats.find((obj) => {
        return obj.name.includes('chromecast');
    });

    const rootComponent = <App />;
    const innerHTML = renderToString(rootComponent);

    // Since webpack replaces the "/" in chunk names with a hyphen we need
    // to do that here when flushing chunks
    const chunkNames = flushChunkNames().map((name) =>
        name.replace(/\//g, '-')
    );
    const flushedAssets = flushChunks(webpackStats, {
        chunkNames,
        before: ['manifest', 'vendor'],
        after: ['chromecast']
    });

    const html = `<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
        <meta charset="utf-8">
        ${getNewRelic()}
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
        <link rel="stylesheet" href="/static/css/audiomack-chromecast.css" />
        <script src="//www.gstatic.com/cast/sdk/libs/caf_receiver/v3/cast_receiver_framework.js" defer></script>
        ${flushedAssets.js}
    </head>
    <body>
        <div class="react-view" id="react-view">${innerHTML}</div>
    </body>
</html>
`;

    res.set('Content-Type', 'text/html');
    res.end(html);
}
