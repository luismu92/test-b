/* eslint camelcase: 0 */
import url from 'url';

import api, { getConfig, setConfig } from 'api/index';
import { buildDynamicImage, getFeaturing } from 'utils/index';
import { getImageData } from '../../workers/email/emails/trendingMusic';

export async function generateImage(amUrl) {
    // Not completely sure if we need to reset back to this but
    // doing it anyway for testing
    const originalConfig = getConfig();

    if (!amUrl) {
        throw new Error('URL parameter is missing.');
    }

    const reqPath = url.parse(amUrl).pathname;

    if (!reqPath) {
        throw new Error('Not Implemented.');
    }

    // Set to production urls
    setConfig({
        baseUrl: 'https://api.audiomack.com/v1',
        consumerKey: 'audiomack-js',
        consumerSecret: 'f3ac5b086f3eab260520d8e3049561e6'
    });

    const songsRegex = /song\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
    const songMatches = reqPath.match(songsRegex);
    const albumsRegex = /album\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
    const albumMatches = reqPath.match(albumsRegex);
    const playlistsRegex = /playlist\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
    const playlistMatches = reqPath.match(playlistsRegex);
    let musicType = null;
    let artistSlug = null;
    let musicSlug = null;

    if (songMatches) {
        musicType = 'song';
    } else if (albumMatches) {
        musicType = 'album';
    } else if (playlistMatches) {
        musicType = 'playlist';
    }

    let artist;
    let title;
    let image;
    let featuring;

    switch (musicType) {
        case 'song': {
            artistSlug = songMatches[1];
            musicSlug = songMatches[2];

            const { results: song } = await api.music.song(
                artistSlug,
                musicSlug
            );

            if (!song) {
                throw new Error('The page you requested could not be found.');
            }

            artist = song.artist;
            title = song.title;
            image = song.image_base || song.image;
            featuring = getFeaturing(song);
            break;
        }

        case 'album': {
            artistSlug = albumMatches[1];
            musicSlug = albumMatches[2];

            const { results: album } = await api.music.album(
                artistSlug,
                musicSlug
            );

            if (!album) {
                throw new Error('The page you requested could not be found.');
            }

            artist = album.artist;
            title = album.title;
            image = album.image_base || album.image;
            featuring = getFeaturing(album);
            break;
        }

        case 'playlist': {
            artistSlug = playlistMatches[1];
            musicSlug = playlistMatches[2];

            const { results: playlist } = await api.playlist.getBySlugs(
                artistSlug,
                musicSlug
            );

            if (!playlist) {
                throw new Error(
                    'The page you requested could not be found.',
                    404
                );
            }

            artist = playlist.artist.name;
            title = playlist.title;
            image = playlist.image_base || playlist.image;
            featuring = getFeaturing(playlist);
            break;
        }

        // Unsupported.
        default:
            throw new Error('Url not found.');
    }

    // For some reason large artwork such as:
    // https://assets.audiomack.com/lil-pump/f4c5d5680b9767d071733c3f1c21ec512c3499803a84287d158916596af63996.jpeg
    // Doesnt register when grabbing the main colors for some reason
    // Adding a smaller version to work around this for now
    const imageSize = 260;
    const thumb = buildDynamicImage(image, {
        width: imageSize,
        height: imageSize,
        max: true
    });
    const imageData = await getImageData(
        { artist, title, featuring },
        image,
        thumb
    );

    setConfig(originalConfig);

    return imageData;
}

export default async function handler(req, res) {
    // Not completely sure if we need to reset back to this but
    // doing it anyway for testing
    const originalConfig = getConfig();

    try {
        const amUrl = req.query.url;
        const imageData = await generateImage(amUrl);

        res.type('png');
        res.send(imageData);
    } catch (e) {
        res.set('Content-Type', 'text/html');

        setConfig(originalConfig);

        res.status(404).end(`<h1>${e.message}</h1>`);
    }
}
