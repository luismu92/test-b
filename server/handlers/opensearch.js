export default function handler(req, res) {
    res.contentType('application/xml');

    const xml = `
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">
    <ShortName>${process.env.AM_NAME}</ShortName>
    <Description>Audiomack is the place for artists to effortlessly share their music and for fans to discover and download free songs and albums.</Description>
    <Tags>audiomack music audio mp3 hip-hop electronic reggae afrobeats latin</Tags>
    <Image width="16" height="16" type="image/x-icon">
        ${process.env.AM_URL}/static/favicon-16x16.ico
    </Image>
    <Url method="get" type="text/html" template="${
        process.env.AM_URL
    }/search?q={searchTerms}"/>
    <moz:SearchForm>${process.env.AM_URL}/search</moz:SearchForm>
</OpenSearchDescription>
    `;

    res.end(xml);
}
