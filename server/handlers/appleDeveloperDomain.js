import fs from 'fs';
import path from 'path';

export default function handler(req, res) {
    const suffix =
        process.env.NODE_ENV === 'production' &&
        process.env.AUDIOMACK_DEV !== 'true'
            ? 'prod'
            : 'dev';
    const filePath = path.join(
        __dirname,
        `../../public/.well-known/apple-developer-domain-association.txt.${suffix}`
    );

    fs.readFile(filePath, function(err, data) {
        if (err) {
            res.status(404).end('Not found');
            return;
        }

        res.contentType('text/plain');
        res.end(data);
    });
}
