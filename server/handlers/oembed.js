/* eslint camelcase: 0 */
import url from 'url';

import {
    OembedNotImplementedException,
    OembedNotFoundException
} from 'utils/errors';
import api from 'api/index';
import { getIframe, absoluteUrl } from 'utils/index';

/**
 * This method is the central point.
 * URL scheme: URL scheme: http://audiomack.com/song/*
 * URL scheme: URL scheme: http://audiomack.com/album/*
 */
export default async function handler(req, res, next) {
    /*
    Example: calling: http://www.youtube.com/oembed?url=http%3A//www.youtube.com/watch%3Fv%3DbDOYN-6gdRE&format=json
    Returns this (formatted for easy reading):
    {
       "width":459,
       "version":"1.0",
       "thumbnail_width":480,
       "title":"Auto-Tune the News #8: dragons. geese. Michael Vick. (ft. T-Pain)",
       "height":344,
       "provider_url":"http:\/\/www.youtube.com\/",
       "thumbnail_url":"http:\/\/i1.ytimg.com\/vi\/bDOYN-6gdRE\/hqdefault.jpg",
       "author_name":"schmoyoho",
       "type":"video",
       "author_url":"http:\/\/www.youtube.com\/user\/schmoyoho",
       "provider_name":"YouTube",
       "html":"\u003ciframe width=\"459\" height=\"344\" src=\"http:\/\/www.youtube.com\/embed\/bDOYN-6gdRE?feature=oembed\" frameborder=\"0\" allowfullscreen\u003e\u003c\/iframe\u003e",
       "thumbnail_height":360
    }
    */

    try {
        // http://oembed.com/
        const ret = {
            msg: '',
            status: 1,
            type: 'rich', // audio is not defined yet (2014-06)
            title: '',
            description: '',
            version: '1.0',
            provider_name: 'Audiomack',
            provider_url: 'https://www.audiomack.com/',

            // type=rich requires the following
            html: '', // @todo generate the embed code iframe
            width: 649,
            height: 252
        };

        res.setHeader('Access-Control-Allow-Origin', '*');

        const amUrl = req.query.url;

        if (!amUrl) {
            throw new OembedNotFoundException('URL parameter is missing.');
        }

        if (req.query.format && req.query.format !== 'json') {
            throw new OembedNotImplementedException(
                `Format "${
                    req.query.format
                }" is not supported. Only JSON is supported (append &format=json).`
            );
        }

        const reqPath = url.parse(amUrl).pathname;

        if (!reqPath) {
            throw new OembedNotImplementedException('Not Implemented.');
        }

        const queryWidth = parseInt(req.query.maxwidth, 10);
        const queryHeight = parseInt(req.query.maxheight, 10);
        const widthBreakpoints = [481, 649];
        const heightBreakpoints = [62, 110];

        const songsRegex = /song\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
        const songMatches = reqPath.match(songsRegex);
        const albumsRegex = /album\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
        const albumMatches = reqPath.match(albumsRegex);
        const playlistsRegex = /playlist\/([-_a-zA-Z0-9.]+)\/([-_a-zA-Z0-9.]+)/i;
        const playlistMatches = reqPath.match(playlistsRegex);
        let musicType = null;
        let artistSlug = null;
        let musicSlug = null;

        if (songMatches) {
            musicType = 'song';
            heightBreakpoints.push(252);
        } else if (albumMatches) {
            musicType = 'album';
            heightBreakpoints.push(352);
        } else if (playlistMatches) {
            musicType = 'playlist';
            heightBreakpoints.push(352);
        }

        let height = heightBreakpoints[heightBreakpoints.length - 1];
        let width = widthBreakpoints[widthBreakpoints.length - 1];

        switch (musicType) {
            case 'song': {
                artistSlug = songMatches[1];
                musicSlug = songMatches[2];

                const { results: song } = await api.music.song(
                    artistSlug,
                    musicSlug
                );

                if (!song) {
                    throw new OembedNotFoundException(
                        'The page you requested could not be found.'
                    );
                }

                if (
                    song.status === 'deleted' ||
                    song.status === 'incomplete' ||
                    song.private === 'yes'
                ) {
                    throw new OembedNotFoundException(
                        'The page you requested could not be found.'
                    );
                }

                // Song
                ret.title = song.title;
                ret.description = song.description;
                ret.thumbnail_url = absoluteUrl(song.image);
                ret.url = `${process.env.AM_URL}/song/${song.url_slug}`;

                // Album
                ret.album = song.album;
                ret.producer = song.producer;
                ret.buy_link = song.buy_link;

                // Author
                ret.author_name = song.artist;
                ret.author_url = `${process.env.AM_URL}/artist/${
                    song.uploader.url_slug
                }`;

                if (req.query.detailed) {
                    ret.song = song;
                }
                break;
            }

            case 'album': {
                artistSlug = albumMatches[1];
                musicSlug = albumMatches[2];

                const { results: album } = await api.music.album(
                    artistSlug,
                    musicSlug
                );

                if (!album) {
                    throw new OembedNotFoundException(
                        'The page you requested could not be found.'
                    );
                }

                // Album
                ret.title = album.title;
                ret.description = album.description;
                ret.thumbnail_url = absoluteUrl(album.image);
                ret.url = `${process.env.AM_URL}/album/${
                    album.uploader.url_slug
                }/${album.url_slug}`;

                // Author
                ret.author_name = album.artist;
                ret.author_url = `${process.env.AM_URL}/artist/${
                    album.uploader.url_slug
                }`;

                artistSlug = album.uploader.url_slug;
                musicSlug = album.url_slug;

                if (req.query.detailed) {
                    ret.album = album;
                }
                break;
            }

            case 'playlist': {
                artistSlug = playlistMatches[1];
                musicSlug = playlistMatches[2];

                const { results: playlist } = await api.playlist.getBySlugs(
                    artistSlug,
                    musicSlug
                );

                if (!playlist) {
                    throw new OembedNotFoundException(
                        'The page you requested could not be found.',
                        404
                    );
                }

                // Playlist
                ret.title = playlist.name;
                ret.description = `Listen to ${playlist.title} Playlist from ${
                    playlist.artist.name
                }.`;
                ret.thumbnail_url = absoluteUrl(playlist.image);
                ret.url = `${process.env.AM_URL}/playlist/${
                    playlist.artist.url_slug
                }/${playlist.url_slug}`;

                // Author
                ret.author_name = playlist.artist.name;
                ret.author_url = `${process.env.AM_URL}/artist/${
                    playlist.artist.url_slug
                }`;

                artistSlug = playlist.artist.url_slug;
                musicSlug = playlist.url_slug;

                if (req.query.detailed) {
                    ret.playlist = playlist;
                }
                break;
            }

            // Unsupported.
            default:
                throw new OembedNotFoundException('Url not found.');
        }

        if (queryWidth) {
            Array.from(widthBreakpoints)
                .reverse()
                .some((w) => {
                    width = w;
                    return w <= queryWidth;
                });

            if (queryWidth < widthBreakpoints[0]) {
                width = queryWidth;
            }
        }

        if (queryHeight) {
            Array.from(heightBreakpoints)
                .reverse()
                .some((h) => {
                    height = h;
                    return h <= queryHeight;
                });

            if (queryHeight < heightBreakpoints[0]) {
                height = queryHeight;
            }
        }

        ret.html = getIframe(musicType, artistSlug, musicSlug, width, height);
        ret.width = width;
        ret.height = height;

        // JSONP?
        if (req.query.callback) {
            res.json(`${req.query.callback}(${JSON.stringify(ret)})`);
            return;
        }

        res.json(ret);
    } catch (e) {
        res.set('Content-Type', 'text/html');

        if (e instanceof OembedNotImplementedException) {
            res.status(501).end(`<h1>${e.message}</h1>`);
        } else if (e instanceof OembedNotFoundException) {
            res.status(404).end(`<h1>${e.message}</h1>`);
        } else {
            // We'll treat this as 404. When the album can't be found that's what we'll get
            res.status(404).end(`<h1>${e.message}</h1>`);
        }

        next(e);
        return;
    }
}
