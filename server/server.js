// @todo upgrade to core-js3 and use the below
// import 'core-js/stable';

import path from 'path';
import fs from 'fs';

import express from 'express';
import mime from 'mime-types';
// import compression from 'compression';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import hpp from 'hpp';
import helmet from 'helmet';
import useragent from 'express-useragent';
import timings from 'server-timings';
import favicon from 'serve-favicon';

import { nonce } from 'utils/index';
import { cookies } from 'constants/index';
import { setConfig } from 'api/index';

import mainHandler from './handlers/main';
import robotsHandler from './handlers/robots';
import adsHandler from './handlers/ads';
import appAdsHandler from './handlers/appAds';
import soundcloudProxyHandler from './handlers/soundcloudProxy';
import oembedHandler from './handlers/oembed';
import errorHandler from './handlers/error';
import siteAssociationHandler from './handlers/siteAssociation';
import appleDeveloperDomainHandler from './handlers/appleDeveloperDomain';
import assetLinksHandler from './handlers/assetLinks';
import emailHandler from './handlers/email';
import authHandler from './handlers/auth';
import cspLogHandler from './handlers/cspLog';
import openSearchHandler from './handlers/opensearch';
import chromecastHandler from './handlers/chromecast';
// import worldHandler from './handlers/world';
import trendingHandler from './handlers/trending';
import rssHandler from './handlers/rss';

const app = express();

app.use(timings);
app.disable('x-powered-by');

app.use(favicon(path.join(__dirname, '../public/static', 'favicon.ico')));

// I'm seeing this acting kinda slow. Can we make nginx or aws handle compression?
// app.use(compression());
app.use((req, res, next) => {
    res.locals.nonce = nonce();
    next();
});
app.use(bodyParser.json({ type: 'application/csp-report' }));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(hpp());
app.use(useragent.express());
app.use(
    helmet({
        hsts: {
            force: process.env.NODE_ENV !== 'development'
        },
        frameguard: {
            action: 'sameorigin'
        }
        // CSP is essential useless with complex on our site. Revisit
        // later
        // contentSecurityPolicy: {
        //     // I've annotated the scripts/images/frames that get injected
        //     // after including one complex CMU script below
        //     directives: {
        //         defaultSrc: [
        //             "'self'",
        //             process.env.ASSETS_URL,
        //             process.env.MUSIC_URL,
        //             process.env.API_URL,
        //             process.env.PHP_BACKEND_URL
        //         ],
        //         scriptSrc: [
        //             "'self'",
        //             "'unsafe-eval'",
        //             'https://js-agent.newrelic.com',
        //             'https://cdn.polyfill.io',
        //             'https://bam.nr-data.net',
        //             'tag.crsspxl.com',
        //             'media.complex.com',
        //             'https://www.google-analytics.com',
        //             'https://ssl.google-analytics.com',
        //             'https://www.gstatic.com',
        //             'http://www.gstatic.com',
        //             'https://www.googletagservices.com',
        //             'https://imasdk.googleapis.com',
        //             'https://pagead2.googlesyndication.com',

        //             /* start bullshit scripts that complex injects */
        //             // We have to allow unsafe inline scripts with complex
        //             // ads enabled which defeats a lot of the purpose for CSP
        //             "'unsafe-inline'",
        //             'http://www.google-analytics.com',
        //             'http://c.amazon-adsystem.com',
        //             'http://js-sec.indexww.com',
        //             'http://www.googletagservices.com',
        //             'https://ad.crwdcntrl.net',
        //             'https://tags.crwdcntrl.net',
        //             'https://securepubads.g.doubleclick.net',
        //             'http://secure.adnxs.com',
        //             'http://adserver-us.adtech.advertising.com',
        //             'http://fastlane.rubiconproject.com',
        //             'http://as.casalemedia.com',
        //             'http://tpc.googlesyndication.com',
        //             'https://tpc.googlesyndication.com',
        //             'https://ads.complex.com',
        //             'https://adservice.google.com',
        //             'http://pagead2.googlesyndication.com',
        //             'https://z.moatads.com'
        //             /* end bullshit scripts that complex injects */

        //             // We have to comment this out because we need
        //             // unsafe-inline enabled for complex scripts
        //             // function(req, res) {
        //             //     return `'nonce-${res.locals.nonce}'`;
        //             // }
        //         ],
        //         imgSrc: [
        //             "'self'",
        //             'https://*',
        //             'data:',
        //             'blob:',
        //             /* start bullshit that complex injects */
        //             'http://media.complex.com',
        //             'http://pagead2.googlesyndication.com'
        //             /* end bullshit that complex injects */
        //         ],
        //         frameSrc: [
        //             "'self'",
        //             'https://*',
        //             /* start bullshit scripts that complex injects */
        //             'http://us-u.openx.net',
        //             'http://bcp.crwdcntrl.net',
        //             'http://tpc.googlesyndication.com'
        //             /* end bullshit scripts that complex injects */
        //         ],
        //         connectSrc: [
        //             "'self'",
        //             'https://*',
        //             process.env.API_URL,
        //             process.env.PHP_BACKEND_URL,
        //             /* start bullshit scripts that complex injects */
        //             'http://as.casalemedia.com',
        //             'http://aax.amazon-adsystem.com',
        //             'http://complex-media-d.openx.net',
        //             'http://adserver-us.adtech.advertising.com',
        //             'http://secure.adnxs.com',
        //             'http://fastlane.rubiconproject.com',
        //             'http://media.complex.com'
        //             /* end bullshit scripts that complex injects */
        //         ],
        //         styleSrc: [
        //             "'self'",
        //             "'unsafe-inline'",
        //             'https://fonts.googleapis.com'
        //         ],
        //         fontSrc: [
        //             "'self'",
        //             'https://fonts.gstatic.com',
        //             'https://fonts.googleapis.com'
        //         ],
        //         reportUri: '/csp'
        //     },
        //     reportOnly: true
        // }
    })
);

const oneYearInSeconds = 1 * 60 * 60 * 24 * 365;
const staticPath = path.join(__dirname, '../public/static');

app.use(
    '/static',
    express.static(staticPath, {
        maxAge: 0,
        setHeaders(res, assetPath) {
            const isHashedJavascript =
                mime.lookup(assetPath) === 'application/javascript' &&
                assetPath.includes('/static/dist/');

            res.removeHeader('x-content-type-options');

            if (isHashedJavascript) {
                res.setHeader(
                    'Cache-Control',
                    `public, max-age=${oneYearInSeconds}`
                );
            }
        }
    })
);

// Add Access Logs
if (process.env.EXPRESS_ACCESS_LOG_PATH) {
    const accessLogStream = fs.createWriteStream(
        process.env.EXPRESS_ACCESS_LOG_PATH,
        { flags: 'a' }
    );

    app.use(
        morgan('combined', {
            stream: accessLogStream
        })
    );
}

// Set custom API variables, if set
app.use((req, res, next) => {
    if (
        process.env.NODE_ENV === 'development' ||
        process.env.AUDIOMACK_DEV === 'true'
    ) {
        let customApiOptions = {};

        try {
            customApiOptions = JSON.parse(req.cookies[cookies.apiConfig]);
        } catch (e) {} // eslint-disable-line

        setConfig(customApiOptions);
    }

    setConfig({
        customHeaders: {
            referer: `${process.env.AM_URL}${req.path}`
        }
    });

    next();
});
app.get('/service-worker-(mobile|desktop)(-legacy)?.js$', (req, res) => {
    res.sendFile(req.path, {
        root: staticPath
    });
});
app.post('/csp', cspLogHandler);
app.get('/oembed', oembedHandler);
app.get('/robots.txt', robotsHandler);
app.get('/ads.txt', adsHandler);
app.get('/app-ads.txt', appAdsHandler);
app.get('/apple-app-site-association', siteAssociationHandler);
app.get('/.well-known/apple-app-site-association', siteAssociationHandler);
app.get(
    '/.well-known/apple-developer-domain-association.txt',
    appleDeveloperDomainHandler
);
app.get('/.well-known/assetlinks.json', assetLinksHandler);
app.get('/opensearch.xml', openSearchHandler);

if (
    process.env.NODE_ENV === 'development' ||
    process.env.AUDIOMACK_DEV === 'true'
) {
    app.get('/email/:name', emailHandler);
    app.get('/trending-email', trendingHandler);
    app.get('/design', (req, res) =>
        res.redirect(301, 'https://forms.gle/Z7vaeyuGHSB7L7tX8')
    );
}

app.post('/auth/:type', authHandler);
app.get('/rss/:artistSlug/podcast.rss', rssHandler);
app.get('/chromecast/player.html', chromecastHandler);

// Proxy this soundcloud request because of safari not working
// @see https://github.com/soundcloud/soundcloud-javascript/issues/27
//
// edit 4/18/17 This may be good to go from the client but we would need
// to include the soundcloud SDK which is pretty big for the one small thing
// we need it for (It includes the promise polyfill and who knows what else
// which is unncessary)
// https://jsfiddle.net/ko4wcswx/
app.get('/soundcloud', soundcloudProxyHandler);
app.get('/ig', (req, res) =>
    res.redirect(301, 'https://www.instagram.com/audiomack')
);
app.get('/sitemap.xml', (req, res) =>
    res.redirect(301, '/sitemaps/sitemap-index.xml.gz')
);
app.get('*', mainHandler);
app.use(errorHandler);

export default ({ clientStats } = {}) => {
    return (req, res, next) => {
        if (clientStats) {
            res.locals.webpackStats = clientStats;
        }

        return app.handle(req, res, next);
    };
};

export { app };
