require('../am-shared/utils/setEnv');

const isDev = process.env.NODE_ENV === 'development';
const path = require('path');

process.env.NEW_RELIC_HOME = path.resolve(__dirname, '../config');
require('newrelic');

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
let isBuilt = false;
const startTime = Date.now();

function showStartMessage(ip, https = false) {
    const proto = https ? 'https' : 'http';

    return `
${new Date().toString()}

Total build time: ${(Date.now() - startTime) / 1000}s

Localhost: ⌨  ${proto}://localhost:${port}
LAN: ☎️  ${proto}://${ip.address()}:${port}

Press 'CTRL-C' to stop
`;
}

function done() {
    if (isBuilt) {
        return;
    }

    isBuilt = true;

    if (isDev && process.env.LOCAL_DEV === 'true') {
        const https = require('https');
        const fs = require('fs');
        const mainHandler = require('../_devServer/server').default();

        app.use(mainHandler);

        if (process.env.USE_HTTPS) {
            const server = https.createServer(
                {
                    // eslint-disable-next-line no-sync
                    cert: fs.readFileSync(
                        path.join(__dirname, '../config/ssl/cert.pem')
                    ),
                    // eslint-disable-next-line no-sync
                    key: fs.readFileSync(
                        path.join(__dirname, '../config/ssl/cert.key')
                    )
                },
                app
            );

            server.listen(port, () => {
                const ip = require('ip');

                console.log(showStartMessage(ip, true));
            });

            return;
        }
    }

    app.listen(port, () => {
        if (isDev) {
            const ip = require('ip');

            console.log(showStartMessage(ip));
        }
    });
}

if (isDev && process.env.LOCAL_DEV === 'true') {
    // Allow us to fetch to the PHP backend from the server without any issues
    // like the self-signed cert error that gets thrown
    //
    // https://github.com/request/request/issues/418#issuecomment-17149236
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    const webpack = require('webpack');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const useragent = require('express-useragent');
    const clientConfigs = require('../config/webpack/client.development')();
    const serverConfig = require('../config/webpack/server.development')();
    const devMiddlewareOptions = {
        // logLevel: 'warn',
        logLevel: 'trace',
        // logLevel: 'silent',
        logTime: true,
        stats: {
            colors: true
        },
        serverSideRender: true
    };
    const hotMiddlewareOptions = {
        // log: null
    };
    const getAlteredDevConfig = (config) => {
        // eslint-disable-line func-style
        // Assign watch: true here so HMR plugin in config works
        // with subsequent hot-update.json requests. I'm putting it here
        // so you can run webpack:dev without it being in watch mode
        config.watch = true;
        config.stats = false;

        return config;
    };

    const configs = clientConfigs.map(getAlteredDevConfig);
    const compiler = webpack([serverConfig].concat(configs));
    const devMiddleware = webpackDevMiddleware(compiler, devMiddlewareOptions);
    const hotMiddleware = webpackHotMiddleware(compiler, hotMiddlewareOptions);

    app.use(useragent.express());
    app.use(devMiddleware);
    app.use(hotMiddleware);

    app.use((req, res, next) => {
        res.locals.webpackStats = res.locals.webpackStats.toJson({
            all: false,
            // This is needed when in production mode where chunks are ids
            // instead of named
            // chunks: true
            chunkGroups: true,
            chunkModules: true,
            modules: true,
            publicPath: true,
            assets: true
        }).children;
        next();
    });

    devMiddleware.waitUntilValid(done);

    console.log(`
${new Date().toString()}
🚧  Webpack is building…
`);
} else {
    const json = require('./stats.json');

    if (!json) {
        throw new Error('No webpack stats json found');
    }

    if (json.errors.length) {
        throw new Error(json.errors.join('\n'));
    }

    const stats = json.children.reduce(
        function(obj, stat) {
            if (Object.keys(stat.publicPath).indexOf('server') !== -1) {
                obj.serverStats = stat;
            } else {
                obj.clientStats.push(stat);
            }

            return obj;
        },
        {
            clientStats: [],
            serverStats: null
        }
    );
    const serverRender = require('./server').default;
    const serverRenderStats = {
        clientStats: stats.clientStats,
        serverStats: stats.serverStats
    };

    app.use(serverRender(serverRenderStats));

    done();
}
