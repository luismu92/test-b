export default function() {
    return `
        <noscript style="text-align: center;">
            <h3>JavaScript Required</h3>
            <p>Audiomack doesn't work properly without JavaScript enabled.</p>
        </noscript>
    `;
}
