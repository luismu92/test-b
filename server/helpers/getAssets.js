let mobileAssets = {};
let desktopAssets = {};

try {
    const assets = require('../mobile-assets');

    mobileAssets = assets && assets.default ? assets.default : assets;
} catch (e) {} // eslint-disable-line

try {
    const assets = require('../desktop-assets');

    desktopAssets = assets && assets.default ? assets.default : assets;
} catch (e) {} // eslint-disable-line

// This takes the assets.json file that was output by the webpack AssetPlugin
// for our client bundle and converts it into an object that contains all the
// paths to our javascript and css files.  Doing this is required as for production
// configurations we add a hash to our filenames, therefore we won't know the
// paths of the output by webpack unless we read them from the assets.json file.

export function parseAssets(assets) {
    const chunks = Object.keys(assets).map((key) => assets[key]);

    return chunks.reduce(
        (acc, chunk) => {
            if (chunk.js && !chunk.text) {
                acc.javascript.push(chunk.js);
            }

            if (chunk.css) {
                acc.css.push(chunk.css);
            }

            if (chunk.text) {
                acc.manifest = chunk.text;
            }

            return acc;
        },
        { javascript: [], css: [], manifest: '' }
    );
}

export default {
    mobile: parseAssets(mobileAssets),
    desktop: parseAssets(desktopAssets),
    mobileUnparsed: mobileAssets,
    desktopUnparsed: desktopAssets
};
