import Redis from 'ioredis';

let redisClient;

if (process.env.NODE_ENV !== 'test') {
    if (process.env.REDIS_CLUSTER_ENABLED === 'true') {
        const hosts = process.env.REDIS_CLUSTER_HOSTS.split(',').map((uri) => {
            const [host, port] = uri.split(':');

            return {
                port: parseInt(port, 10),
                host
            };
        });

        redisClient = new Redis.Cluster(hosts);
    } else {
        redisClient = new Redis(process.env.REDIS_MAIN_URL);
    }

    redisClient.on('error', (err) => {
        if (process.env.NODE_ENV === 'production') {
            const host =
                process.env.REDIS_CLUSTER_ENABLED === 'true'
                    ? process.env.REDIS_CLUSTER_HOSTS
                    : process.env.REDIS_MAIN_URL;

            console.log(`Error connecting to ${host}.`);
            console.log(err);
        }

        if (redisClient) {
            redisClient.quit();
        }

        redisClient = null;
    });
}

export function getMusicIdByUrl(type, artistSlug, musicSlug) {
    if (!redisClient) {
        return Promise.reject('no redis client initialized');
    }

    return new Promise((resolve, reject) => {
        redisClient.hget(
            'music_urls',
            `${type}/${artistSlug}/${musicSlug}`,
            (err, reply) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(reply);
            }
        );
    });
}

export function getMusicKeyById(id, key) {
    if (!redisClient) {
        return Promise.reject('no redis client initialized');
    }

    return new Promise((resolve, reject) => {
        redisClient.hget(`music:${id}`, key, (err, reply) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(reply);
        });
    });
}

export function getArtistIdBySlug(slug) {
    if (!redisClient) {
        return Promise.reject('no redis client initialized');
    }

    return new Promise((resolve, reject) => {
        redisClient.hget('artist_slugs', slug, (err, reply) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(reply);
        });
    });
}
