/* global test, expect */
import { isDesktopLayout } from '../index';

function getMockRequest(data = {}) {
    return {
        get() {},
        originalUrl: '/',
        path: '/',
        url: '/',
        cookies: {
            showDesktop: '1'
        },
        query: {
            showDesktop: 'false'
        },
        ...data
    };
}

test('isDesktopLayout() should return true when showDesktop query is "true"', () => {
    const req = getMockRequest({
        query: {
            showDesktop: 'true'
        }
    });

    expect(isDesktopLayout(req)).toBe(true);
});

test('isDesktopLayout() should return false when showDesktop query is "false"', () => {
    const req = getMockRequest({
        query: {
            showDesktop: 'false'
        }
    });

    expect(isDesktopLayout(req)).toBe(false);
});

test('isDesktopLayout() should return false when no query string is present but cookie is', () => {
    const req = getMockRequest({
        query: {
            showDesktop: 'false'
        },
        cookies: {
            showDesktop: '0'
        }
    });

    expect(isDesktopLayout(req)).toBe(false);
});

test('isDesktopLayout() should return true when no query string is present but cookie is', () => {
    const req = getMockRequest({
        query: {
            showDesktop: 'false'
        },
        cookies: {
            showDesktop: '1'
        }
    });

    expect(isDesktopLayout(req)).toBe(false);
});

test('isDesktopLayout() should return true when cloudfront says its a tablet', () => {
    const req = getMockRequest({
        query: {},
        cookies: {},
        get(val) {
            if (val === 'cloudfront-is-tablet-viewer') {
                return 'true';
            }

            return 'false';
        }
    });

    expect(isDesktopLayout(req)).toBe(true);
});

test('isDesktopLayout() should return false when its a mobile device', () => {
    const req = getMockRequest({
        query: {},
        cookies: {}
    });
    const isMobile = true;

    expect(isDesktopLayout(req, isMobile)).toBe(false);
});

test('isDesktopLayout() should return false when its not a mobile device', () => {
    const req = getMockRequest({
        query: {},
        cookies: {}
    });
    const isMobile = false;

    expect(isDesktopLayout(req, isMobile)).toBe(true);
});
