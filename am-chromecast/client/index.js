import React from 'react';
import { hydrate, render } from 'react-dom';

import analytics from 'utils/analytics';
import App from '../shared/App';

const rootNode = document.getElementById('react-view');

function captureErrors() {
    if (
        typeof window !== 'undefined' &&
        typeof window.onerror !== 'undefined'
    ) {
        const original = window.onerror;
        window.onerror = function(msg, url, line, col, error) {
            original.apply(this, arguments);
            analytics.error(error);
        };
    }
}

function run() {
    const fn = process.env.NODE_ENV === 'development' ? render : hydrate;

    captureErrors();
    analytics.release('chromecast', process.env.GIT_COMMIT);

    fn(<App />, rootNode);
}

run();
