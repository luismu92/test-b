import React, { Component } from 'react';
import { setConfig } from 'react-hot-loader';
import { hot } from 'react-hot-loader/root';

import ErrorBoundary from 'components/ErrorBoundary';

import Receiver from './Receiver';
import AmMark from './icons/am-mark';

setConfig({ logLevel: process.env.HOT_LOADER_LOG_LEVEL || undefined });

class App extends Component {
    render() {
        const displayError = (
            <div
                className="u-flex-center flex-dir-column"
                style={{ height: '100%' }}
            >
                <AmMark
                    className="u-text-orange"
                    style={{ width: '60px', height: '60px' }}
                />
                <p>
                    There was an error loading the Audiomack Chromecast app.
                    Please try again later or email us at support@audiomack.com.
                </p>
            </div>
        );

        return (
            <ErrorBoundary
                boundaryName="chromecast-receiver"
                displayError={displayError}
            >
                <Receiver />
            </ErrorBoundary>
        );
    }
}

export default hot(App);
