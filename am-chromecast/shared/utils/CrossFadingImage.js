import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { uuid } from 'utils/index';

export default class CrossFadingImage extends Component {
    static propTypes = {
        src: PropTypes.string,
        className: PropTypes.string,
        imageClassName: PropTypes.string,
        activeImageClassName: PropTypes.string
    };

    constructor(props) {
        super(props);

        const state = {
            imageQueue: [],
            activeImageId: null
        };

        if (props.src) {
            state.currentImage = {
                src: props.src,
                id: uuid()
            };
        }

        this.state = state;
    }

    ///////////////////////
    // Lifecycle methods //
    ///////////////////////

    componentDidUpdate(prevProps) {
        if (prevProps.src !== this.props.src) {
            this.pushImage(this.props.src);
        }
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleBackgroundTransitionEnd = (e) => {
        if (e.propertyName !== 'opacity') {
            return;
        }

        const image = e.currentTarget;
        const id = image.getAttribute('data-id');
        const active = image.getAttribute('data-active') === 'true';

        if (active && this.state.currentImage.id !== id) {
            this.updateCurrentImage(image.src, id);
        }
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    updateCurrentImage(src, id = null) {
        const newState = {
            currentImage: {
                src: src,
                id: id !== null ? id : uuid()
            }
        };

        // If id is passed that means we're using an image
        // from the queue to update here, which means we can
        // get rid of all the images in the queue after this image
        if (id !== null) {
            const queue = this.state.imageQueue;
            const index = queue.findIndex((info) => {
                return info.id === id;
            });
            const newImageQueue = queue.slice(0, Math.max(0, index));

            newState.imageQueue = newImageQueue;
        }

        this.setState(newState);
    }

    pushImage(src) {
        if (!this.state.currentImage) {
            this.updateCurrentImage(src);
            return;
        }

        this.setState(
            (prev) => {
                return {
                    ...prev,
                    imageQueue: [
                        {
                            src: src,
                            id: uuid()
                        }
                    ].concat(prev.imageQueue)
                };
            },
            () => {
                clearTimeout(this._transitionImageTimer);

                // Hack to delay the transition of the background.
                // Chromecast is a pretty low powered device so this
                // makes sure the image--active class doesnt get added
                // until about 1.5s after the setState call above
                // causes a re-render (which is the slow part since
                // about all elements in the UI are updating at this
                // point)
                this._transitionImageTimer = setTimeout(() => {
                    this.transitionActiveImage();
                }, 1000);
            }
        );
    }

    transitionActiveImage() {
        if (!this.state.imageQueue.length) {
            return;
        }

        this.setState({
            activeImageId: this.state.imageQueue[0].id
        });
    }

    renderImage(info, zIndex, active = false) {
        const isActive = active || this.state.activeImageId === info.id;
        const klass = classnames('cross-fading-image__image', {
            [this.props.imageClassName]: this.props.imageClassName,
            [this.props.activeImageClassName]: isActive
        });

        return (
            <img
                key={info.id}
                src={info.src}
                className={klass}
                onTransitionEnd={this.handleBackgroundTransitionEnd}
                style={{ zIndex: zIndex }}
                data-active={isActive}
                data-id={info.id}
                alt={info.alt || ''}
            />
        );
    }

    renderImages(current, imageQueue = []) {
        if (!current) {
            return null;
        }

        const transitioningImages = imageQueue.map((info) => {
            return this.renderImage(info);
        });

        return (
            <Fragment>
                {transitioningImages}
                {this.renderImage(current, 0, true)}
            </Fragment>
        );
    }

    render() {
        const klass = classnames('cross-fading-image', {
            [this.props.className]: this.props.className
        });

        return (
            <div className={klass}>
                {this.renderImages(
                    this.state.currentImage,
                    this.state.imageQueue
                )}
            </div>
        );
    }
}
