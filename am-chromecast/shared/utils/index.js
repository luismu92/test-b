export function codeToErrorMessage(code) {
    switch (code) {
        case 100:
            return "MEDIA_UNKNOWN: The media element encountered an unknown error fired from platform. The media element encountered an error that did not indicate it's one of MediaError.MEDIA_ERR_*. This should be rare.";
        case 101:
            return "MEDIA_ABORTED: The media element fired MediaError.MEDIA_ERR_ABORTED error. The fetching process for the media resource was aborted at the user's request. This is usually due to aborting play.";
        case 102:
            return "MEDIA_DECODE: The media element fired MediaError.MEDIA_ERR_DECODE error.  Developer should validate their stream's encoding parameters.";
        case 103:
            return "MEDIA_NETWORK: The media element fired MediaError.MEDIA_ERR_NETWORK error  Download of media data failed because Cast doesn't use media element to download. This issue can be with your app; if necessary, report through Cast issue tracker.";
        case 104:
            return 'MEDIA_SRC_NOT_SUPPORTED: The media element fired MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED error.   Validate developer data segment, ensure the codecs are included in Cast supported formats.';
        case 110:
            return 'SOURCE_BUFFER_FAILURE: Cast is unable to add a source buffer to the existing Media source. See Web Crypto. Often times this could be incorrect codecs specified on the codecs stream on the manifest in your app. If not, report through Cast issue tracker.';
        case 201:
            return 'MEDIAKEYS_NETWORK: When using XhrIo in Media Keys for posting a license request, we encountered a network error.   Developer validates their license server.';
        case 202:
            return `MEDIAKEYS_UNSUPPORTED: This covers two cases:
When using the media session to generate a request on EME, we encountered a key error.
Failed to initialize EME media keys.
Download of media data failed, but because Cast doesn't use media element to download, please report through Cast issue tracker.`;
        case 203:
            return 'MEDIAKEYS_WEBCRYPTO: When using built-in Web Crypto supported by the browser, we ran into an error decrypting. See Web Crypto API.   Please report through Cast issue tracker.';
        case 301:
            return 'SEGMENT_NETWORK:*    Failed to retrieve any segment from any bitrate (with three retries of exponential backoffs).   Developers need to validate that their segments are indeed available. It could be the case that a user that cannot reach these segments as well.';
        case 311:
            return 'HLS_NETWORK_MASTER_PLAYLIST:*    Failed to retrieve the master playlist m3u8 file with three retries.    Developers need to validate that their playlists are indeed available. It could be the case that a user that cannot reach the playlist as well.';
        case 312:
            return 'HLS_NETWORK_PLAYLIST:*    Failed to retrieve the media (bitrated) playlist m3u8 file with three retries.  Developers need to validate that their playlists are indeed available. It could be the case that a user that cannot reach the playlist as well.';
        case 313:
            return 'HLS_NETWORK_NO_KEY_RESPONSE: The request for decryption key did not return a response.   Developers need to validate their decryption key service.';
        case 314:
            return 'HLS_NETWORK_KEY_LOAD:*    The XhrIO used to request HLS decryption key failed.    Developers need to validate their decryption key service.';
        case 315:
            return 'HLS_NETWORK_INVALID_SEGMENT: The HLS segment received for processing is neither a TS nor an mp4 AAC segment. Could be MPL bug or configuration on receiver app bug.  Developers need to validate that their segments are either TS or AAC.';
        case 321:
            return 'DASH_NETWORK:*    The XHR request to get the DASH Manifest failed with no response.   See star section for network-related error diagnosis.';
        case 322:
            return 'DASH_NO_INIT: We cannot extract initialization data from the first DASH init segment. Developers need to validate their DASH init segment.';
        case 331:
            return 'SMOOTH_NETWORK:*    The XHR request to get the DASH Manifest failed with no response.   See star section for network related error diagnosis.';
        case 332:
            return 'SMOOTH_NO_MEDIA_DATA: The segment downloaded for processing contains no media data.   Developers need to validate their Smooth segments.';
        case 411:
            return 'HLS_MANIFEST_MASTER: Parsing of the HLS manifest file failed. Or something MPL does not understand yet in the m3u8   Examine the content of the manifest url.';
        case 412:
            return 'HLS_MANIFEST_PLAYLIST: Parsing of the media playlist file failed. Or something MPL does not understand yet in the m3u8 Examine the content of the media playlist URL.';
        case 421:
            return 'DASH_MANIFEST_NO_PERIODS: When normalizing the Dash manifest, we found not periods in it. This is abnormal.   Developers need to validate their DASH manifest.';
        case 422:
            return 'DASH_MANIFEST_NO_MIMETYPE: There is no mimetype for a representation in the manifest.  Developers need to specify Audio/Video/Text mimetype for their representations.';
        case 423:
            return 'DASH_INVALID_SEGMENT_INFO: MPL is requesting a segment index that is beyond the length of available segments as specified by the representation in the manifest.   Report through Cast issue tracker.';
        case 431:
            return `SMOOTH_MANIFEST: The smooth manifest does not conform to standard.
Developer needs to resolve any of the following issues:
no sps
invalid video quality
invalid audio quality
invalid audio codec private data
invalid protection info
no quality level
unknown media`;
        default:
            return null;
    }
}

export function preloadImages(images = []) {
    const promises = images.map((image) => {
        return new Promise((resolve) => {
            const img = new Image();

            img.onerror = resolve;
            img.onload = resolve;
            img.src = image;
        });
    });

    return Promise.all(promises);
}
