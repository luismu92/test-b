/* global cast, YourServer */
// @todo use for v2 party mode
export default class extends cast.framework.QueueBase {
    constructor() {
        /** @private {} */
        super();
        YourServer.onSomeEvent = this.updateEntireQueue_;
    }

    /**
     * Initializes the queue.
     * @param {!cast.framework.messages.LoadRequestData} loadRequestData
     * @return {!cast.framework.messages.QueueData}
     */
    initialize(loadRequestData) {
        // Put the first set of items into the queue
        const items = this.nextItems();
        const queueData =
            loadRequestData.queueData ||
            new cast.framework.messages.QueueData();

        queueData.name = 'Your Playlist';
        queueData.description = 'Your Playlist Description';
        queueData.items = items;
        return queueData;
    }

    /**
     * Picks a set of items from remote server after the reference item id and
     * return as the next items to be inserted into the queue. When
     * referenceItemId is omitted, items are simply appended to the end of the
     * queue.
     * @param {number} referenceItemId
     * @return {!Array<cast.framework.QueueItem>}
     */
    nextItems(referenceItemId) {
        // Assume your media has a itemId and the media url
        return this.constructQueueList_(
            YourServer.getNextMedias(referenceItemId)
        );
    }

    /**
     * Picks a set of items from remote server before the reference item id and
     * return as the items to be inserted into the queue. When
     * referenceItemId is omitted, items are simply appended to beginning of the
     * queue.
     * @param {number} referenceItemId
     * @return {!Array<cast.framework.QueueItem>}
     */
    prevItems(referenceItemId) {
        return this.constructQueueList_(
            YourServer.getPrevMedias(referenceItemId)
        );
    }

    /**
     * Constructs a list of QueueItems based on the media information containing
     * the item id and the media url.
     * @param {number} referenceItemId
     * @return {!Array<cast.framework.QueueItem>}
     */
    constructQueueList_(medias) {
        const items = [];

        for (const media of medias) {
            const item = new cast.framework.messages.QueueItem(media.itemId);

            item.media = new cast.framework.messages.MediaInformation();
            item.media.contentId = media.url;
            items.push(item);
        }

        return items;
    }

    /**
     * Logs the currently playing item.
     * @param {number} itemId The unique id for the item.
     * @export
     */
    onCurrentItemIdChanged(itemId) {
        console.log(`We are now playing video ${itemId}`);

        YourServer.trackUsage(itemId);
    }

    /** Inserts items to the queue. */
    onSomeEventTriggeringInsertionToQueue() {
        const twoMoreUrls = ['http://url1', 'http://url2'];
        const items = [];

        for (const mediaUrl of twoMoreUrls) {
            const item = new cast.framework.QueueItem();

            item.media = new cast.framework.messages.MediaInformation();
            item.media.contentId = mediaUrl;
            items.push(item);
        }
        // Insert two more items after the current playing item.
        const allItems = this.queueManager_.getItems();
        const currentItem = this.queueManager_.getCurrentItem();
        const nextItemIndex = allItems.indexOf(currentItem) + 1;
        const insertBefore =
            nextItemIndex < allItems.length
                ? allItems[nextItemIndex].itemId
                : undefined;

        this.queueManager_.insertItems(items, insertBefore);
    }

    /** Removes a particular item from the queue. */
    onSomeEventTriggeringRemovalFromQueue() {
        this.queueManager_.removeItems([2]);
    }

    /** Removes all the ads from all the items across the entire queue. */
    onUserBoughtAdFreeVersion() {
        const items = this.queueManager_.getItems();

        this.queueManager_.updateItems(
            items.map((item) => {
                item.media.breaks = undefined;
                return item;
            })
        );
    }
}
