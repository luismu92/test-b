/* global cast */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import api, { setConfig } from 'api/index';

import {
    convertSecondsToTimecode,
    humanizeNumber,
    renderFeaturingLinks,
    getDominantColors,
    buildDynamicImage,
    ucfirst,
    uuid,
    getUploader
} from 'utils/index';
import PlaySession from 'utils/PlaySession';
import { chromecastMessageTypes } from 'constants/index';
import { ENVIRONMENT_CHROMECAST } from 'constants/stats/environment';
import Waveform from 'components/Waveform';
import PlayerAudio from 'components/player/PlayerAudio';
import Verified from 'components/Verified';
import HeartIcon from 'icons/heart';
// import AndroidLoader from 'components/loaders/AndroidLoader';

import { preloadImages } from './utils/index';
import CrossFadingImage from './utils/CrossFadingImage';
import PlayButton from '../../am-desktop/shared/components/PlayButton';
import PlayIcon from './icons/play';
import PlusIcon from './icons/plus';
import RetweetIcon from './icons/retweet';
import AmLogo from './icons/am-logo-new';
import { codeToErrorMessage } from './utils/index';
// import Queue from './Queue';

/**
 * Lessons in doing this Chromecast app
 *
 * - Keep anything held in memory very slim. For example, its too
 * much to hold a whole playlist object in memory
 *
 * - Keep css transitions to a minimum, especially filter: blur.
 * That will crash things.
 *
 *
 */
// let firstSenderConnected = false;

const playSession = new PlaySession();

export default class Receiver extends Component {
    static propTypes = {
        title: PropTypes.string,
        artist: PropTypes.string,
        artwork: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            activeMusicItemCanBePaused: false,
            trackId: null,
            parentId: null,
            parentType: null,
            parentTitle: null,
            key: null,
            chromecastSessionToken: null,
            backgroundImage: null,
            firstItemCanPlay: false,
            playerSrc: null,
            playerLoading: false,
            playerReady: false,
            playerPaused: true,
            playerCurrentTime: 0,
            playerDuration: 0,
            playerVolume: 1,
            playerMuted: false,
            title: props.title || '',
            artist: props.artist || '',
            artwork: props.artwork || null,
            artworkTint: props.artwork || null,
            released: null,
            uploader: null,
            stats: null,
            album: null,
            featuring: null,
            producer: null,
            volumeData: null
        };
    }

    componentDidMount() {
        const playerManager = this.instance.getPlayerManager();

        console.info(
            `Listening to all messages on channel ${process.env.CHROMECAST_URN}`
        );
        this.instance.addCustomMessageListener(
            process.env.CHROMECAST_URN,
            this.handleCustomMessage
        );

        // Listen to all events
        console.info('Listening to all player events');
        playerManager.addEventListener(
            cast.framework.events.category.REQUEST,
            this.handlePlayerEvent
        );
        // playerManager.addEventListener(cast.framework.events.category.CORE, this.handlePlayerEvent);

        console.info('Listening to all system events');
        Object.keys(cast.framework.system.EventType).forEach((key) => {
            this.instance.addEventListener(
                cast.framework.system.EventType[key],
                this.handleSystemEvent
            );
        });

        // this.instance.setLoggerLevel(cast.framework.LoggerLevel.DEBUG);

        this.instance.start({
            // queue: new Queue(),
            // playbackConfig: playbackConfig,
            maxInactivity:
                process.env.NODE_ENV === 'development' ? 3600 : undefined,
            mediaElement: document.getElementById('audio'),
            version: process.env.GIT_COMMIT || 'development'
        });

        this.loadStatsToken();
    }

    ////////////////////
    // Event handlers //
    ////////////////////

    handleSystemEvent = (e) => {
        console.log('system event', e);
        console.log(
            this.instance.getPlayerManager().getMediaInformation(),
            this.instance.getSenders()
        );

        const {
            READY,
            SENDER_CONNECTED,
            SENDER_DISCONNECTED
        } = cast.framework.system.EventType;

        switch (e.type) {
            case READY: {
                this.setState({
                    playerReady: true
                });
                break;
            }

            case SENDER_CONNECTED: {
                // When a sender connects make sure they have the current chromecast queue
                // if (!firstSenderConnected) {
                //     const firstSender = this.instance.getSenders()[0];

                //     this.instance.sendCustomMessage(process.env.CHROMECAST_URN, firstSender.id, {
                //         type: 'getTrack'
                //     });

                //     firstSenderConnected = true;
                // }
                break;
            }

            case SENDER_DISCONNECTED: {
                this.maybeShowIdleScreen();
                break;
            }

            default:
                break;
        }
    };

    handlePlayerEvent = (e) => {
        console.log('player event', e);

        const {
            REQUEST_FOCUS_STATE,
            ERROR,
            REQUEST_LOAD,
            REQUEST_SEEK
        } = cast.framework.events.EventType;

        switch (e.type) {
            case REQUEST_LOAD: {
                this.handleRequestLoad(e.requestData);
                break;
            }

            case REQUEST_SEEK: {
                console.warn('request seek', e);
                this.handleSeek(e.requestData);
                break;
            }

            case REQUEST_FOCUS_STATE: {
                break;
            }

            case ERROR: {
                const detailedMessage = codeToErrorMessage(e.detailedErrorCode);

                if (detailedMessage) {
                    console.error(detailedMessage);
                } else if (e.error) {
                    console.error(e.error);
                } else {
                    console.error(`Unknown error code: ${e.detailedErrorCode}`);
                }
                break;
            }

            default:
                break;
        }
    };

    handleRequestLoad = (requestData) => {
        const { media, autoplay, currentTime } = requestData;
        const {
            id,
            type,
            token,
            secret,
            key,
            apiKey,
            apiSecret,
            apiUrl
        } = media.customData;
        const playerManager = this.instance.getPlayerManager();

        this.setState({
            playerLoading: true,
            activeMusicItemCanBePaused: false
        });

        console.warn(requestData, media.customData);

        setConfig({
            baseUrl: apiUrl.replace(/\/+$/, ''),
            consumerKey: apiKey,
            consumerSecret: apiSecret
        });

        playerManager.pause();

        this.getMusic(id, type, { token, secret, key })
            .then((musicItem) => {
                return this.setStateWithMusicItem(
                    requestData,
                    media.customData,
                    musicItem
                ).then(() => {
                    playSession.reset();
                    playSession.play(currentTime);
                    playSession.once(0, this.handlePlayTrackCall);
                    playSession.once(30, this.handlePlayTrackCall);

                    if (currentTime) {
                        playerManager.seek(currentTime);

                        this.setState({
                            playerCurrentTime: currentTime
                        });
                    }

                    if (autoplay) {
                        playerManager.play();
                    } else {
                        playerManager.pause();
                    }

                    this.setState({
                        playerLoading: false
                    });
                    return;
                });
            })
            .catch((err) => {
                console.error(err);
                this.setState({
                    playerLoading: false
                });
            });
    };

    handleCustomMessage = ({ type, senderId, data }) => {
        console.log('custom message', type, senderId, data);

        switch (type) {
            case chromecastMessageTypes.idle: {
                const ignoreSenders = true;

                this.maybeShowIdleScreen(ignoreSenders);
                break;
            }

            default:
                break;
        }
    };

    handleSeek = (requestData) => {
        const currentTime = this.state.playerCurrentTime;
        const time = requestData.currentTime;

        console.warn('Seeking from', currentTime, 'to', time);
        playSession.seek(time);

        this.setState({
            playerCurrentTime: time
        });
    };

    handleTimeUpdate = (e) => {
        clearTimeout(this._playTimer);
        this._playTimer = null;

        const time = e.target.currentTime;

        playSession.timeupdate(time);

        this.setState({
            playerCurrentTime: time
        });
    };

    handleLoadedMetadata = (e) => {
        console.log('loaded metadata', e);
        this.setState({
            playerDuration: e.target.duration
        });
    };

    handleEnded = (e) => {
        console.log('audio ended', e);

        this.sendMessage(chromecastMessageTypes.ended);
        this.setState(
            {
                activeMusicItemCanBePaused: false
            },
            () => this.maybeShowIdleScreen()
        );
    };

    handlePause = () => {
        console.log('audio paused');
        this.setState({
            playerPaused: true
        });
    };

    handleCanPlay = () => {
        this.setState({
            firstItemCanPlay: true
        });
    };

    handlePlay = () => {
        console.log('audio playing');

        this.setState({
            playerPaused: false,
            firstItemCanPlay: true,
            activeMusicItemCanBePaused: true
        });
        // If we press play and there's no subsequent timeupdate
        // event, then we've probably left a song in the player
        // for too long and it expired.
        this._playTimer = setTimeout(() => {
            console.log('error playing within 4 seconds');
        }, 4000);
    };

    handleError = (e) => {
        const target = e.currentTarget || {};
        const error = target.error || {};
        const {
            code,
            MEDIA_ERR_ABORTED,
            MEDIA_ERR_NETWORK,
            MEDIA_ERR_DECODE,
            MEDIA_ERR_SRC_NOT_SUPPORTED
        } = error;

        switch (code) {
            case MEDIA_ERR_ABORTED:
                console.warn(code, 'Aborted playback.');
                break;

            case MEDIA_ERR_NETWORK:
                console.warn(
                    code,
                    'A network error caused the audio download to fail.'
                );
                break;

            case MEDIA_ERR_DECODE:
                console.warn(
                    code,
                    'The audio playback was aborted due to a corruption problem or because the video used features your browser did not support.'
                );
                break;

            case MEDIA_ERR_SRC_NOT_SUPPORTED: {
                console.warn(
                    code,
                    'The audio could not be loaded, either because the server or network failed or because the format is not supported.'
                );
                break;
            }

            default:
                console.warn(code, 'An unknown error occurred.', e);
                break;
        }
    };

    handlePlayTrackCall = (time) => {
        const options = {
            session: this.state.chromecastSessionToken,
            environment: ENVIRONMENT_CHROMECAST,
            key: this.state.key,
            token: this.state.token,
            secret: this.state.secret,
            hq: this.state.hq
        };

        if (time === 30) {
            options.time = time;
        }

        api.music.play(this.state.trackId, options);
    };

    ////////////////////
    // Helper methods //
    ////////////////////

    get instance() {
        return cast.framework.CastReceiverContext.getInstance();
    }

    /**
     *  This is purposely refetching possibly the same data
     *  over and over if its a playlist on purpose. The reason
     *  being some playlists that have 50+ tracks, it's a bit much
     *  to hold that data in memory.
     */
    getMusic(id, type, { token, secret, key } = {}) {
        console.log(`Fetching ${type} with id ${id} and key ${key}`);

        if (type === 'playlist') {
            return api.playlist.id(id, token, secret).then((data) => {
                return data.results;
            });
        }

        return api.music.id(id, { token, secret, key }).then((data) => {
            return data.results;
        });
    }

    getArtwork() {
        return (
            this.state.artwork ||
            'https://assets.dev.audiomack.com/default-album-image.jpg'
        );
    }

    /**
     * Set state for the app UI from the musicItem. We
     * want to keep this as low in memory as possible
     */
    setStateWithMusicItem(requestData, customData, musicItem) {
        const { key, token, secret, hq } = customData;
        const isTrackItem = ['album', 'playlist'].includes(musicItem.type);

        let songOrTrack = musicItem;

        if (isTrackItem) {
            songOrTrack =
                musicItem.tracks[requestData.media.metadata.trackNumber - 1];
        }

        const artworkOptions = {
            width: 330,
            height: 330,
            max: true
        };
        const backgroundOptions = {
            width: 250,
            height: 250,
            max: true,
            blur: 30
        };

        const baseImage =
            songOrTrack.image_base ||
            musicItem.image_base ||
            songOrTrack.image ||
            musicItem.image;
        const image = buildDynamicImage(baseImage, artworkOptions);
        const backgroundImage = buildDynamicImage(baseImage, backgroundOptions);

        return preloadImages([image, backgroundImage]).then(() => {
            const newState = {
                trackId: songOrTrack.song_id || songOrTrack.id,
                parentId: null,
                parentType: null,
                parentTitle: null,
                playerSrc: requestData.media.contentId,
                playerDuration: requestData.media.duration,
                title: requestData.media.metadata.title,
                artist: requestData.media.metadata.artist,
                artwork: image,
                backgroundImage,
                released: requestData.media.metadata.releaseDate || null,
                uploader: getUploader(musicItem) || null,
                stats: musicItem.stats || null,
                // Only show album if the parent details were
                // not sent. This could be a single track that
                // has an album that hasnt been released yet
                album:
                    !isTrackItem && requestData.media.metadata.albumName
                        ? requestData.media.metadata.albumName
                        : null,
                featuring: songOrTrack.featuring || null,
                key: key || null,
                hq: hq || null,
                token: token || null,
                secret: secret || null,
                producer: songOrTrack.producer || null,
                volumeData: songOrTrack.volume_data || null
            };

            if (isTrackItem) {
                newState.parentId = musicItem.id || null;
                newState.parentType = musicItem.type || null;
                newState.parentTitle = musicItem.title || null;
            }

            let promise = Promise.resolve([`#${musicItem.dominant_color}`]);

            if (!musicItem.dominant_color) {
                promise = getDominantColors(newState.artwork).then((color) => {
                    return `rgb(${color})`;
                });
            }

            return promise.then((colors) => {
                console.log('Setting state', newState);

                return new Promise((resolve) => {
                    this.setState(
                        {
                            ...newState,
                            artworkTint: colors[0] || null
                        },
                        resolve
                    );
                });
            });
        });
    }

    maybeShowIdleScreen(ignoreSenders = false) {
        const sendersExist = this.instance.getSenders().length;

        if (sendersExist && !ignoreSenders) {
            return;
        }

        if (this.state.activeMusicItemCanBePaused) {
            return;
        }

        console.warn('@todo Show idle screen');
    }

    loadStatsToken() {
        api.music
            .getStatsToken(uuid())
            .then((token) => {
                this.setState({
                    chromecastSessionToken: token
                });
                return;
            })
            .catch((err) => {
                console.warn('Error getting chromecast token', err);
            });
    }

    isAppReady() {
        return this.state.playerReady && this.state.firstItemCanPlay;
    }

    sendMessage(type, { senderId = null, data = {} } = {}) {
        let senders = this.instance.getSenders().map((s) => s.id);

        if (senderId) {
            senders = [senderId];
        }

        senders.forEach((id) => {
            this.instance.sendCustomMessage(process.env.CHROMECAST_URN, id, {
                ...data,
                type
            });
        });
    }

    renderSplash() {
        const appReady = this.isAppReady();
        const klass = classnames('splash', {
            'splash--inactive': appReady
        });

        // Runs too slow and choppy
        // let loader;

        // if (!appReady) {
        //     loader = <AndroidLoader />;
        // }

        return <div className={klass}>{/* loader */}</div>;
    }

    renderBg() {
        return (
            <div className="bg">
                <CrossFadingImage
                    src={this.state.backgroundImage}
                    className="bg__layer bg__image-container"
                    imageClassName="bg__image"
                    activeImageClassName="bg__image--active"
                />
                <div
                    className="bg__tint bg__layer"
                    style={{ backgroundColor: this.state.artworkTint }}
                />
                <div className="bg__black bg__layer" />
                <div className="bg__gradient bg__layer" />
            </div>
        );
    }

    renderArtwork() {
        const src = this.getArtwork();
        let pauseButton;

        if (
            (this.state.playerPaused &&
                this.state.activeMusicItemCanBePaused) ||
            this.state.playerLoading
        ) {
            pauseButton = (
                <PlayButton
                    onlySizeLoader
                    className="artwork__play-button"
                    size={40}
                    // loading={this.state.playerLoading}
                    // This is choppy on chromecast
                    loading={false}
                    // This has to be backwards because thats the convention on TVs...
                    paused={!this.state.playerPaused}
                />
            );
        }

        return (
            <div className="artwork">
                {pauseButton}
                <img src={src} className="artwork" alt="" />
            </div>
        );
    }

    renderStats(stats) {
        if (!stats) {
            return null;
        }

        const plays = stats['plays-raw'];
        const favorites = stats['favorites-raw'];
        const reposts = stats['reposts-raw'];
        const playlists = stats['playlists-raw'];
        const isPlaylist = this.state.parentType === 'playlist';
        const isAlbum = this.state.parentType === 'album';

        return (
            <div className="stats">
                <span className="stat">
                    <PlayIcon className="stat__icon" />
                    <span className="stat__count">{humanizeNumber(plays)}</span>
                </span>
                <span className="stat">
                    <HeartIcon className="stat__icon" />
                    <span className="stat__count">
                        {humanizeNumber(favorites)}
                    </span>
                </span>
                {!isPlaylist && (
                    <span className="stat">
                        <RetweetIcon className="stat__icon stat__icon--reposts" />
                        <span className="stat__count">
                            {humanizeNumber(reposts)}
                        </span>
                    </span>
                )}
                {!isPlaylist && !isAlbum && (
                    <span className="stat">
                        <PlusIcon className="stat__icon" />
                        <span className="stat__count">
                            {humanizeNumber(playlists)}
                        </span>
                    </span>
                )}
            </div>
        );
    }

    renderFeaturing(featuring) {
        if (!featuring) {
            return null;
        }

        return (
            <p className="featuring">
                {renderFeaturingLinks(featuring, { removeLinks: true })}
            </p>
        );
    }

    renderProducer(producer) {
        if (!producer) {
            return null;
        }

        return (
            <p className="producer">
                <strong>Producer:</strong> {producer}
            </p>
        );
    }

    renderAlbum(album) {
        if (!album) {
            return null;
        }

        return (
            <p className="album">
                <strong>Album:</strong> {album}
            </p>
        );
    }

    renderAddedOn(released, uploader) {
        if (!released) {
            return null;
        }

        let uploaderName;
        let check;

        if (uploader) {
            uploaderName = (
                <span>
                    by{' '}
                    <strong className="u-text-orange">{uploader.name}</strong>
                </span>
            );
            check = <Verified user={uploader} small />;
        }

        return (
            <p className="released">
                <strong>Release Date: </strong>
                {moment(released).format('MMM DD, YYYY')} {uploaderName} {check}
            </p>
        );
    }

    renderAlbumPlaylistAttribution(type, title) {
        if (!title) {
            return null;
        }

        return (
            <p className="parent-attribution">
                From {ucfirst(type)}:{' '}
                <span className="u-text-orange">{title}</span>
            </p>
        );
    }

    renderWaveForm() {
        const currentTime = this.state.playerCurrentTime;
        const duration = this.state.playerDuration;
        const progress =
            this.state.playerCurrentTime /
            Math.max(this.state.playerDuration, 1);
        const elapsedDisplay = currentTime || 0;
        const dataAsString = this.state.volumeData || '';
        let musicVolumeData = [];

        try {
            musicVolumeData = JSON.parse(dataAsString) || [];
        } catch (e) {} //eslint-disable-line

        return (
            <div className="waveform-wrap">
                <Waveform
                    fill={progress}
                    dataAsString={dataAsString}
                    data={musicVolumeData}
                    color="rgba(199, 199, 199, 0.4)"
                    fillColor="#ffa200"
                />
                <span className="waveform__elapsed waveform__time waveform__time--dark">
                    {convertSecondsToTimecode(elapsedDisplay)}
                </span>
                <span className="waveform__duration waveform__time waveform__time--dark">
                    {convertSecondsToTimecode(duration)}
                </span>
            </div>
        );
    }

    renderContent() {
        if (!this.isAppReady()) {
            return null;
        }

        return (
            <Fragment>
                {this.renderBg()}
                <AmLogo className="logo" />
                <div className="main-content media-item media-item--center">
                    <div className="media-item__figure artwork-container">
                        {this.renderArtwork()}
                        {this.renderStats(this.state.stats)}
                    </div>

                    <div className="media-item__body">
                        <h3 className="artist">{this.state.artist}</h3>
                        <h1 className="title">{this.state.title}</h1>
                        {this.renderFeaturing(this.state.featuring)}
                        {this.renderProducer(this.state.producer)}
                        {this.renderAlbum(this.state.album)}
                        {this.renderAddedOn(
                            this.state.released,
                            this.state.uploader
                        )}
                    </div>
                </div>
                {this.renderAlbumPlaylistAttribution(
                    this.state.parentType,
                    this.state.parentTitle
                )}
                {this.renderWaveForm()}
            </Fragment>
        );
    }

    render() {
        return (
            <Fragment>
                <PlayerAudio
                    ref={(c) => (this._playerAudio = c)}
                    src={this.state.playerSrc}
                    volume={this.state.playerVolume}
                    currentTime={this.state.playerCurrentTime}
                    paused={this.state.playerPaused}
                    muted={this.state.playerMuted}
                    onTimeUpdate={this.handleTimeUpdate}
                    onLoadedMetadata={this.handleLoadedMetadata}
                    onEnded={this.handleEnded}
                    onError={this.handleError}
                    onCanPlay={this.handleCanPlay}
                    onPlay={this.handlePlay}
                    onPause={this.handlePause}
                />
                {this.renderSplash()}
                {this.renderContent()}
            </Fragment>
        );
    }
}
